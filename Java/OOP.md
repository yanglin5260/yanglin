<h2>目录</h2>

<details open>
  <summary><a href="#1-面向对象编程">1. 面向对象编程</a></summary>
  <ul>
  <details open>
    <summary><a href="#11-简介">1.1. 简介</a>  </summary>
    <ul>
      <a href="#111-class和instance">1.1.1. class和instance</a><br>
      <a href="#112-定义class">1.1.2. 定义class</a><br>
      <a href="#113-创建实例">1.1.3. 创建实例</a><br>
      <a href="#114-不通过构造函数也能创建对象吗需要看第二遍">1.1.4. 不通过构造函数也能创建对象吗?(需要看第二遍)</a><br>
      <a href="#115-小结">1.1.5. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#12-方法">1.2. 方法</a>  </summary>
    <ul>
      <a href="#121-定义方法">1.2.1. 定义方法</a><br>
      <a href="#122-private方法">1.2.2. private方法</a><br>
      <a href="#123-this变量">1.2.3. this变量</a><br>
      <a href="#124-方法参数">1.2.4. 方法参数</a><br>
      <a href="#125-可变参数">1.2.5. 可变参数</a><br>
      <a href="#126-参数绑定">1.2.6. 参数绑定</a><br>
      <a href="#127-小结">1.2.7. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#13-构造方法">1.3. 构造方法</a>  </summary>
    <ul>
      <a href="#131-默认构造方法">1.3.1. 默认构造方法</a><br>
      <a href="#132-多构造方法">1.3.2. 多构造方法</a><br>
      <a href="#133-小结">1.3.3. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#14-方法重载">1.4. 方法重载</a>  </summary>
    <ul>
      <a href="#141-小结">1.4.1. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#15-继承">1.5. 继承</a>  </summary>
    <ul>
      <a href="#151-继承树">1.5.1. 继承树</a><br>
      <a href="#152-protected">1.5.2. protected</a><br>
      <a href="#153-super">1.5.3. super</a><br>
      <a href="#154-super关键字的用法">1.5.4. super关键字的用法</a><br>
      <a href="#155-this与super的区别">1.5.5. this与super的区别</a><br>
      <a href="#156-阻止继承">1.5.6. 阻止继承</a><br>
      <a href="#157-向上转型">1.5.7. 向上转型</a><br>
      <a href="#158-向下转型">1.5.8. 向下转型</a><br>
      <a href="#159-区分继承和组合">1.5.9. 区分继承和组合</a><br>
      <a href="#1510-小结">1.5.10. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#16-多态">1.6. 多态</a>  </summary>
    <ul>
      <a href="#161-多态">1.6.1. 多态</a><br>
      <a href="#162-覆写object方法">1.6.2. 覆写Object方法</a><br>
      <a href="#163-调用super">1.6.3. 调用super</a><br>
      <a href="#164-final">1.6.4. final</a><br>
      <a href="#165-小结">1.6.5. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#17-抽象类">1.7. 抽象类</a>  </summary>
    <ul>
      <a href="#171-抽象类">1.7.1. 抽象类</a><br>
      <a href="#172-面向抽象编程">1.7.2. 面向抽象编程</a><br>
      <a href="#173-小结">1.7.3. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#18-接口">1.8. 接口</a>  </summary>
    <ul>
      <a href="#181-术语">1.8.1. 术语</a><br>
      <a href="#182-接口继承">1.8.2. 接口继承</a><br>
      <a href="#183-继承关系">1.8.3. 继承关系</a><br>
      <a href="#184-default方法">1.8.4. default方法</a><br>
      <a href="#185-小结">1.8.5. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#19-静态字段和静态方法">1.9. 静态字段和静态方法</a>  </summary>
    <ul>
      <a href="#191-静态方法">1.9.1. 静态方法</a><br>
      <a href="#192-接口的静态字段">1.9.2. 接口的静态字段</a><br>
      <a href="#193-静态变量和实例变量的区别？">1.9.3. 静态变量和实例变量的区别？</a><br>
      <a href="#194-static应用场景">1.9.4. static应用场景</a><br>
      <a href="#195-static注意事项">1.9.5. static注意事项</a><br>
      <a href="#196-static的独特之处">1.9.6. static的独特之处</a><br>
      <a href="#197-小结">1.9.7. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#110-包">1.10. 包</a>  </summary>
    <ul>
      <a href="#1101-包作用域">1.10.1. 包作用域</a><br>
      <a href="#1102-import">1.10.2. import</a><br>
      <a href="#1103-import-java和javax有什么区别">1.10.3. import java和javax有什么区别</a><br>
      <a href="#1104-jdk-中常用的包有哪些">1.10.4. JDK 中常用的包有哪些</a><br>
      <a href="#1105-最佳实践">1.10.5. 最佳实践</a><br>
      <a href="#1106-小结">1.10.6. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#111-作用域">1.11. 作用域</a>  </summary>
    <ul>
      <a href="#1111-public">1.11.1. public</a><br>
      <a href="#1112-private">1.11.2. private</a><br>
      <a href="#1113-protected">1.11.3. protected</a><br>
      <a href="#1114-package">1.11.4. package</a><br>
      <a href="#1115-局部变量">1.11.5. 局部变量</a><br>
      <a href="#1116-final">1.11.6. final</a><br>
      <a href="#1117-最佳实践">1.11.7. 最佳实践</a><br>
      <a href="#1118-小结">1.11.8. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#112-内部类">1.12. 内部类</a>  </summary>
    <ul>
      <a href="#1121-inner-class">1.12.1. Inner Class</a><br>
      <a href="#1122-anonymous-class">1.12.2. Anonymous Class</a><br>
      <a href="#1123-static-nested-class">1.12.3. Static Nested Class</a><br>
      <a href="#1124-小结">1.12.4. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#113-classpath和jar">1.13. classpath和jar</a>  </summary>
    <ul>
      <a href="#1131-jar包">1.13.1. jar包</a><br>
      <a href="#1132-小结">1.13.2. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#114-模块">1.14. 模块</a>  </summary>
    <ul>
      <a href="#1141-编写模块">1.14.1. 编写模块</a><br>
      <a href="#1142-运行模块">1.14.2. 运行模块</a><br>
      <a href="#1143-打包jre">1.14.3. 打包JRE</a><br>
      <a href="#1144-访问权限">1.14.4. 访问权限</a><br>
      <a href="#1145-小结">1.14.5. 小结</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#2-java核心类">2. Java核心类</a></summary>
  <ul>
  <details open>
    <summary><a href="#21-字符串和编码">2.1. 字符串和编码</a>  </summary>
    <ul>
      <a href="#211-string">2.1.1. String</a><br>
      <a href="#212-字符串比较">2.1.2. 字符串比较</a><br>
      <a href="#213-去除首尾空白字符">2.1.3. 去除首尾空白字符</a><br>
      <a href="#214-替换子串">2.1.4. 替换子串</a><br>
      <a href="#215-分割字符串">2.1.5. 分割字符串</a><br>
      <a href="#216-拼接字符串">2.1.6. 拼接字符串</a><br>
      <a href="#217-格式化字符串">2.1.7. 格式化字符串</a><br>
      <a href="#218-类型转换">2.1.8. 类型转换</a><br>
      <a href="#219-转换为char[]">2.1.9. 转换为char[]</a><br>
      <a href="#2110-字符编码">2.1.10. 字符编码</a><br>
      <a href="#2111-延伸阅读需要看第二遍">2.1.11. 延伸阅读(需要看第二遍)</a><br>
      <a href="#2112-小结">2.1.12. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#22-stringbuilder">2.2. StringBuilder</a>  </summary>
    <ul>
      <a href="#221-小结">2.2.1. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#23-stringjoiner">2.3. StringJoiner</a>  </summary>
    <ul>
      <a href="#231-stringjoin">2.3.1. String.join()</a><br>
      <a href="#232-小结">2.3.2. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#24-包装类型">2.4. 包装类型</a>  </summary>
    <ul>
      <a href="#241-auto-boxing">2.4.1. Auto Boxing</a><br>
      <a href="#242-不变类">2.4.2. 不变类</a><br>
      <a href="#243-进制转换">2.4.3. 进制转换</a><br>
      <a href="#244-处理无符号整型">2.4.4. 处理无符号整型</a><br>
      <a href="#245-小结">2.4.5. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#25-javabean">2.5. JavaBean</a>  </summary>
    <ul>
      <a href="#251-javabean的作用">2.5.1. JavaBean的作用</a><br>
      <a href="#252-枚举javabean属性">2.5.2. 枚举JavaBean属性</a><br>
      <a href="#253-小结">2.5.3. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#26-枚举类">2.6. 枚举类</a>  </summary>
    <ul>
      <a href="#261-enum">2.6.1. enum</a><br>
      <a href="#262-enum的比较">2.6.2. enum的比较</a><br>
    <details open>
      <summary><a href="#263-enum类型">2.6.3. enum类型</a>    </summary>
      <ul>
        <a href="#2631-name">2.6.3.1. name()</a><br>
        <a href="#2632-ordinal">2.6.3.2. ordinal()</a><br>
      </ul>
    </details>
      <a href="#264-switch">2.6.4. switch</a><br>
      <a href="#265-小结">2.6.5. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#27-记录类">2.7. 记录类</a>  </summary>
    <ul>
      <a href="#271-record">2.7.1. record</a><br>
      <a href="#272-构造方法">2.7.2. 构造方法</a><br>
      <a href="#273-小结">2.7.3. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#28-biginteger">2.8. BigInteger</a>  </summary>
    <ul>
      <a href="#281-biginteger">2.8.1. BigInteger</a><br>
      <a href="#282-小结">2.8.2. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#29-bigdecimal">2.9. BigDecimal</a>  </summary>
    <ul>
      <a href="#291-比较bigdecimal">2.9.1. 比较BigDecimal</a><br>
      <a href="#292-小结">2.9.2. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#210-常用工具类">2.10. 常用工具类</a>  </summary>
    <ul>
      <a href="#2101-math">2.10.1. Math</a><br>
      <a href="#2102-random">2.10.2. Random</a><br>
      <a href="#2103-securerandom">2.10.3. SecureRandom</a><br>
      <a href="#2104-小结">2.10.4. 小结</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#3-面试题">3. 面试题</a></summary>
  <ul>
    <a href="#31-overload-和override-的区别？overloaded的方法是否可以改变返回值的类型需要看第二遍">3.1. Overload 和Override 的区别？Overloaded的方法是否可以改变返回值的类型?(需要看第二遍)</a><br>
    <a href="#32-java-中，什么是构造函数？什么是构造函数重载？什么是复制构造函数？">3.2. Java 中，什么是构造函数？什么是构造函数重载？什么是复制构造函数？</a><br>
    <a href="#33-构造器-constructor-是否可被-override">3.3. 构造器 Constructor 是否可被 Override?</a><br>
    <a href="#34-java-支持多继承么？">3.4. Java 支持多继承么？</a><br>
    <a href="#35-接口和抽象类的区别是什么？需要看第二遍">3.5. 接口和抽象类的区别是什么？(需要看第二遍)</a><br>
    <a href="#36-java-接口的修饰符可以为">3.6. Java 接口的修饰符可以为?</a><br>
    <a href="#37-下面是-people-和-child-类的定义和构造方法，每个构造方法都输出编号。在执行-new-childmike-的时候都有哪些构造方法被顺序调用？请选择输出结果">3.7. 下面是 People 和 Child 类的定义和构造方法，每个构造方法都输出编号。在执行 new Child("mike") 的时候都有哪些构造方法被顺序调用？请选择输出结果</a><br>
    <a href="#38-两个对象值相同xequalsy--true，但却可有不同的-hash-code，这句话对不对？">3.8. 两个对象值相同(x.equals(y) == true)，但却可有不同的 hash code，这句话对不对？</a><br>
    <a href="#39-接口是否可继承（extends）接口-抽象类是否可实现（implements）接口-抽象类是否可继承具体类（concrete-class）需要看第二遍">3.9. 接口是否可继承（extends）接口? 抽象类是否可实现（implements）接口? 抽象类是否可继承具体类（concrete class）?(需要看第二遍)</a><br>
    <a href="#310-classforname（string-classname）这个方法的作用">3.10. Class.forName（String className）这个方法的作用</a><br>
    <a href="#311-什么是aop和oop，ioc和di有什么不同">3.11. 什么是AOP和OOP，IOC和DI有什么不同?</a><br>
    <a href="#312-判断下列语句是否正确，如果有错误，请指出错误所在？">3.12. 判断下列语句是否正确，如果有错误，请指出错误所在？</a><br>
    <a href="#313-static关键字是什么意思？java-中是否可以覆盖override-一个-private或者是-static-的方法？">3.13. static关键字是什么意思？Java 中是否可以覆盖(override) 一个 private或者是 static 的方法？</a><br>
    <a href="#314-访问修饰符-public-private-protected-以及不写（默认）时的区别？需要看第二遍">3.14. 访问修饰符 public, private, protected, 以及不写（默认）时的区别？(需要看第二遍)</a><br>
    <a href="#315-volatile关键字是否能保证线程安全？需要看第二遍">3.15. volatile关键字是否能保证线程安全？(需要看第二遍)</a><br>
    <a href="#316-lock和synchronized的区别需要看第二遍">3.16. Lock和Synchronized的区别(需要看第二遍)</a><br>
    <a href="#317-synchronized锁的底层实现需要看第二遍">3.17. synchronized锁的底层实现(需要看第二遍)</a><br>
    <a href="#318-java-有没有-goto">3.18. Java 有没有 goto?</a><br>
    <a href="#319-final-finally-finalize-的区别需要看第二遍">3.19. final, finally, finalize 的区别(需要看第二遍)</a><br>
    <a href="#320-用最有效率的方法算出2乘以8等於几">3.20. 用最有效率的方法算出2乘以8等於几?</a><br>
    <a href="#321-存在使-i--1--i的数吗">3.21. 存在使 i + 1 < i的数吗?</a><br>
    <a href="#322-06332的数据类型是（）">3.22. 0.6332的数据类型是（）</a><br>
    <a href="#323-systemoutprintln5--2的输出结果应该是（）">3.23. System.out.println("5" + 2);的输出结果应该是（）</a><br>
    <a href="#324-float-f34是否正确">3.24. float f=3.4;是否正确?</a><br>
    <a href="#325-char型变量中能不能存贮一个中文汉字为什么">3.25. char型变量中能不能存贮一个中文汉字?为什么?</a><br>
    <a href="#326-string-是最基本的数据类型吗">3.26. String 是最基本的数据类型吗?</a><br>
    <a href="#327-数组有没有-length-方法-string-有没有-length-方法？">3.27. 数组有没有 length() 方法? String 有没有 length() 方法？</a><br>
    <a href="#328-是否可以继承string类">3.28. 是否可以继承String类</a><br>
    <a href="#329-string-和stringbuilder、stringbuffer-的区别">3.29. String 和StringBuilder、StringBuffer 的区别?</a><br>
    <a href="#330-string-s--new-stringxyz创建了几个字符串对象？">3.30. String s = new String(“xyz”);创建了几个字符串对象？</a><br>
    <a href="#331-将字符-12345-转换成-long-型">3.31. 将字符 “12345” 转换成 long 型</a><br>
    <a href="#332-为了显示-mystr--23-这样的结果，写出在控制台输入的命令">3.32. 为了显示 myStr = 23 这样的结果，写出在控制台输入的命令</a><br>
    <a href="#333-string-s--hellos--s---world这两行代码执行后，原始的string对象中的内容到底变了没有？为什么不可变？不可变有什么好处？">3.33. String s = "Hello";s = s + " world!”;这两行代码执行后，原始的String对象中的内容到底变了没有？为什么不可变？不可变有什么好处？</a><br>
    <a href="#334-如何把一段逗号分割的字符串转换成一个数组需要看第二遍">3.34. 如何把一段逗号分割的字符串转换成一个数组?(需要看第二遍)</a><br>
    <a href="#335-下面这条语句一共创建了多少个对象：string-sabcd">3.35. 下面这条语句一共创建了多少个对象：String s=“a”+”b”+”c”+”d”;</a><br>
    <a href="#336-string和stringbuffer的区别">3.36. String和StringBuffer的区别?</a><br>
    <a href="#337-string-stringbuffer-stringbuilder的区别">3.37. String, StringBuffer StringBuilder的区别</a><br>
    <a href="#338-面向对象五大基本原则是什么（可选）">3.38. 面向对象五大基本原则是什么（可选）</a><br>
    <a href="#339-普通类和抽象类有哪些区别？">3.39. 普通类和抽象类有哪些区别？</a><br>
    <a href="#340-抽象类能使用-final-修饰吗？">3.40. 抽象类能使用 final 修饰吗？</a><br>
  <details open>
    <summary><a href="#341-成员变量与局部变量的区别有哪些需要看第二遍">3.41. 成员变量与局部变量的区别有哪些(需要看第二遍)</a>  </summary>
    <ul>
      <a href="#3411-作用域">3.41.1. 作用域</a><br>
      <a href="#3412-存储位置">3.41.2. 存储位置</a><br>
      <a href="#3413-生命周期">3.41.3. 生命周期</a><br>
      <a href="#3414-初始值">3.41.4. 初始值</a><br>
      <a href="#3415-使用原则">3.41.5. 使用原则</a><br>
    </ul>
  </details>
    <a href="#342-在java中定义一个不做事且没有参数的构造方法的作用需要看第二遍">3.42. 在Java中定义一个不做事且没有参数的构造方法的作用(需要看第二遍)</a><br>
    <a href="#343-在调用子类构造方法之前会先调用父类没有参数的构造方法，其目的是？需要看第二遍">3.43. 在调用子类构造方法之前会先调用父类没有参数的构造方法，其目的是？(需要看第二遍)</a><br>
    <a href="#344-一个类的构造方法的作用是什么？若一个类没有声明构造方法，改程序能正确执行吗？为什么？">3.44. 一个类的构造方法的作用是什么？若一个类没有声明构造方法，改程序能正确执行吗？为什么？</a><br>
    <a href="#345-构造方法有哪些特性？">3.45. 构造方法有哪些特性？</a><br>
    <a href="#346-静态变量和实例变量区别">3.46. 静态变量和实例变量区别</a><br>
    <a href="#347-静态变量与普通变量区别需要看第二遍">3.47. 静态变量与普通变量区别(需要看第二遍)</a><br>
    <a href="#348-静态方法和实例方法有何不同？">3.48. 静态方法和实例方法有何不同？</a><br>
    <a href="#349-什么是方法的返回值？返回值的作用是什么？">3.49. 什么是方法的返回值？返回值的作用是什么？</a><br>
    <a href="#350-什么是内部类？">3.50. 什么是内部类？</a><br>
  <details open>
    <summary><a href="#351-内部类的分类有哪些需要看第二遍">3.51. 内部类的分类有哪些(需要看第二遍)</a>  </summary>
    <ul>
      <a href="#3511-静态内部类">3.51.1. 静态内部类</a><br>
      <a href="#3512-成员内部类">3.51.2. 成员内部类</a><br>
      <a href="#3513-局部内部类">3.51.3. 局部内部类</a><br>
      <a href="#3514-匿名内部类">3.51.4. 匿名内部类</a><br>
    </ul>
  </details>
    <a href="#352-内部类的优点">3.52. 内部类的优点</a><br>
    <a href="#353-内部类有哪些应用场景">3.53. 内部类有哪些应用场景</a><br>
    <a href="#354-局部内部类和匿名内部类访问局部变量的时候，为什么变量必须要加上final？">3.54. 局部内部类和匿名内部类访问局部变量的时候，为什么变量必须要加上final？</a><br>
    <a href="#355-内部类相关，看程序说出运行结果">3.55. 内部类相关，看程序说出运行结果</a><br>
  <details open>
    <summary><a href="#356-重写与重载">3.56. 重写与重载</a>  </summary>
    <ul>
      <a href="#3561-构造器（constructor）是否可被重写（override）">3.56.1. 构造器（constructor）是否可被重写（override）</a><br>
      <a href="#3562-重载（overload）和重写（override）的区别。重载的方法能否根据返回类型进行区分？">3.56.2. 重载（Overload）和重写（Override）的区别。重载的方法能否根据返回类型进行区分？</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#357-对象相等判断需要看第二遍">3.57. 对象相等判断(需要看第二遍)</a>  </summary>
    <ul>
      <a href="#3571--和-equals-的区别是什么需要看第二遍">3.57.1. == 和 equals 的区别是什么(需要看第二遍)</a><br>
    <details open>
      <summary><a href="#3572-hashcode-与-equals-重要">3.57.2. hashCode 与 equals (重要)</a>    </summary>
      <ul>
        <a href="#35721-hashset如何检查重复">3.57.2.1. HashSet如何检查重复</a><br>
        <a href="#35722-hashcode介绍">3.57.2.2. hashCode()介绍</a><br>
        <a href="#35723-为什么要有-hashcode">3.57.2.3. 为什么要有 hashCode</a><br>
        <a href="#35724-hashcode与equals的相关规定">3.57.2.4. hashCode()与equals()的相关规定</a><br>
        <a href="#35725-对象的相等与指向他们的引用相等，两者有什么不同？">3.57.2.5. 对象的相等与指向他们的引用相等，两者有什么不同？</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#3573-值传递">3.57.3. 值传递</a>    </summary>
      <ul>
        <a href="#35731-当一个对象被当作参数传递到一个方法后，此方法可改变这个对象的属性，并可返回变化后的结果，那么这里到底是值传递还是引用传递？">3.57.3.1. 当一个对象被当作参数传递到一个方法后，此方法可改变这个对象的属性，并可返回变化后的结果，那么这里到底是值传递还是引用传递？</a><br>
        <a href="#35732-为什么-java-中只有值传递">3.57.3.2. 为什么 Java 中只有值传递</a><br>
        <a href="#35733-总结">3.57.3.3. 总结</a><br>
      </ul>
    </details>
      <a href="#3574-值传递和引用传递有什么区别">3.57.4. 值传递和引用传递有什么区别</a><br>
    </ul>
  </details>
    <a href="#358-字符型常量和字符串常量的区别">3.58. 字符型常量和字符串常量的区别</a><br>
    <a href="#359-什么是字符串常量池？需要看第二遍">3.59. 什么是字符串常量池？(需要看第二遍)</a><br>
    <a href="#360-string-是最基本的数据类型吗">3.60. String 是最基本的数据类型吗</a><br>
    <a href="#361-string有哪些特性需要看第二遍">3.61. String有哪些特性(需要看第二遍)</a><br>
    <a href="#362-string为什么是不可变的吗？">3.62. String为什么是不可变的吗？</a><br>
  <details open>
    <summary><a href="#363-string真的是不可变的吗？">3.63. String真的是不可变的吗？</a>  </summary>
    <ul>
      <a href="#3631-string不可变但不代表引用不可以变">3.63.1. String不可变但不代表引用不可以变</a><br>
      <a href="#3632-通过反射是可以修改所谓的不可变对象">3.63.2. 通过反射是可以修改所谓的“不可变”对象</a><br>
    </ul>
  </details>
    <a href="#364-是否可以继承-string-类">3.64. 是否可以继承 String 类</a><br>
    <a href="#365-string-stri与-string-strnew-stringi一样吗？">3.65. String str="i"与 String str=new String(“i”)一样吗？</a><br>
    <a href="#366-string-s--new-stringxyz创建了几个字符串对象">3.66. String s = new String(“xyz”);创建了几个字符串对象</a><br>
    <a href="#367-如何将字符串反转？">3.67. 如何将字符串反转？</a><br>
    <a href="#368-数组有没有-length方法？string-有没有-length方法">3.68. 数组有没有 length()方法？String 有没有 length()方法</a><br>
    <a href="#369-string-类的常用方法都有那些？">3.69. String 类的常用方法都有那些？</a><br>
    <a href="#370-在使用-hashmap-的时候，用-string-做-key-有什么好处？">3.70. 在使用 HashMap 的时候，用 String 做 key 有什么好处？</a><br>
  <details open>
    <summary><a href="#371-string和stringbuffer、stringbuilder的区别是什么？string为什么是不可变的需要看第二遍">3.71. String和StringBuffer、StringBuilder的区别是什么？String为什么是不可变的(需要看第二遍)</a>  </summary>
    <ul>
      <a href="#3711-可变性（关于string的优化）">3.71.1. 可变性（关于String的优化）</a><br>
      <a href="#3712-线程安全性">3.71.2. 线程安全性</a><br>
      <a href="#3713-性能">3.71.3. 性能</a><br>
      <a href="#3714-对于三者使用的总结">3.71.4. 对于三者使用的总结</a><br>
    </ul>
  </details>
  </ul>
</details>


<h1>面向对象编程</h1>

# 1. 面向对象编程

## 1.1. 简介

面向对象编程，是一种通过对象的方式，把现实世界映射到计算机模型的一种编程方法。

### 1.1.1. class和instance

所以，只要理解了class和instance的概念，基本上就明白了什么是面向对象编程。

class是一种对象模版，它定义了如何创建实例，因此，class本身就是一种数据类型。

而instance是对象实例，instance是根据class创建的实例，可以创建多个instance，每个instance类型相同，但各自属性可能不相同。

### 1.1.2. 定义class

在Java中，创建一个类，例如，给这个类命名为`Person`，就是定义一个`class`：

```java
class Person {
    public String name;
    public int age;
}
```

一个`class`可以包含多个字段（`field`），字段用来描述一个类的特征。上面的`Person`类，我们定义了两个字段，一个是`String`类型的字段，命名为`name`，一个是`int`类型的字段，命名为`age`。因此，通过`class`，把一组数据汇集到一个对象上，实现了数据封装。

`public`是用来修饰字段的，它表示这个字段可以被外部访问。

### 1.1.3. 创建实例

定义了class，只是定义了对象模版，而要根据对象模版创建出真正的对象实例，必须用new操作符。

有了指向这个实例的变量，我们就可以通过这个变量来操作实例。

### 1.1.4. 不通过构造函数也能创建对象吗?(需要看第二遍)

是的。

Java 创建对象的几种方式（重要）：

1. 用new语句创建对象，这是最常见的创建对象的方法
2. 运用反射手段,调用java.lang.Class或者java.lang.reflect.Constructor类的newInstance()实例方法
3. 调用对象的clone()方法
4. 运用反序列化手段，调用java.io.ObjectInputStream对象的readObject()方法。

(1)和(2)都会明确的显式的调用构造函数；(3)是在内存上对已有对象的影印，所以不会调用构造函数；(4)是从文件中还原类的对象，也不会调用构造函数。

### 1.1.5. 小结

在OOP中，`class`和`instance`是“模版”和“实例”的关系；

定义`class`就是定义了一种数据类型，对应的`instance`是这种数据类型的实例；

`class`定义的`field`，在每个`instance`都会拥有各自的`field`，且互不干扰；

通过`new`操作符创建新的`instance`，然后用变量指向它，即可通过变量来引用这个`instance`；

访问实例字段的方法是`变量名.字段名`；

指向`instance`的变量都是引用变量。

## 1.2. 方法

一个`class`可以包含多个`field`，但是，直接把`field`用`public`暴露给外部可能会破坏封装性。为了避免外部代码直接去访问`field`，我们可以用`private`修饰`field`，拒绝外部访问。

虽然外部代码不能直接修改`private`字段，但是，外部代码可以调用方法`setName()`和`setAge()`来间接修改`private`字段。在方法内部，我们就有机会检查参数对不对。比如，`setAge()`就会检查传入的参数，参数超出了范围，直接报错。这样，外部代码就没有任何机会把`age`设置成不合理的值。

外部代码不能直接读取`private`字段，但可以通过`getName()`和`getAge()`间接获取`private`字段的值。

所以，一个类通过定义方法，就可以给外部代码暴露一些操作的接口，同时，内部自己保证逻辑一致性。

### 1.2.1. 定义方法

从上面的代码可以看出，定义方法的语法是：

```
修饰符 方法返回类型 方法名(方法参数列表) {
    若干方法语句;
    return 方法返回值;
}
```

方法返回值通过`return`语句实现，如果没有返回值，返回类型设置为`void`，可以省略`return`。

### 1.2.2. private方法

有`public`方法，自然就有`private`方法。和`private`字段一样，`private`方法不允许外部调用。

### 1.2.3. this变量

在方法内部，可以使用一个隐含的变量`this`，它始终指向当前实例。

如果没有命名冲突，可以省略`this`。

但是，如果有局部变量和字段重名，那么局部变量优先级更高，就必须加上`this`。

**this关键字的用法**

this是自身的一个对象，代表对象本身，可以理解为：指向对象本身的一个指针。

this的用法在java中大体可以分为3种：

1. 普通的直接引用，this相当于是指向当前对象本身。

2. 形参与成员名字重名，用this来区分：

3. 引用本类的构造函数    

### 1.2.4. 方法参数

方法可以包含0个或任意个参数。方法参数用于接收传递给方法的变量值。调用方法时，必须严格按照参数的定义一一传递。

### 1.2.5. 可变参数

可变参数用`类型...`定义，可变参数相当于数组类型。

可变参数可以保证无法传入`null`，因为传入0个参数时，接收到的实际值是一个空数组而不是`null`。

### 1.2.6. 参数绑定

调用方把参数传递给实例方法时，调用时传递的值会按参数位置一一绑定。

那什么是参数绑定？

结论：基本类型参数的传递，是调用方值的复制。双方各自的后续修改，互不影响。

结论：引用类型参数的传递，调用方的变量，和接收方的参数变量，指向的是同一个对象。双方任意一方对这个对象的修改，都会影响对方（因为指向同一个对象嘛）。

### 1.2.7. 小结

- 方法可以让外部代码安全地访问实例字段；
- 方法是一组执行语句，并且可以执行任意逻辑；
- 方法内部遇到return时返回，void表示不返回任何值（注意和返回null不同）；
- 外部代码通过public方法操作实例，内部代码可以调用private方法；
- 理解方法的参数绑定。

## 1.3. 构造方法

创建实例的时候，实际上是通过构造方法来初始化实例的。

由于构造方法是如此特殊，所以构造方法的名称就是类名。构造方法的参数没有限制，在方法内部，也可以编写任意语句。但是，和普通方法相比，构造方法没有返回值（也没有`void`），调用构造方法，必须用`new`操作符。

### 1.3.1. 默认构造方法

是不是任何`class`都有构造方法？是的。

那前面我们并没有为`Person`类编写构造方法，为什么可以调用`new Person()`？

原因是如果一个类没有定义构造方法，编译器会自动为我们生成一个默认构造方法，它没有参数，也没有执行语句。

要特别注意的是，如果我们自定义了一个构造方法，那么，编译器就**不再**自动创建默认构造方法：

如果既要能使用带参数的构造方法，又想保留不带参数的构造方法，那么只能把两个构造方法都定义出来：

没有在构造方法中初始化字段时，引用类型的字段默认是`null`，数值类型的字段用默认值，`int`类型默认值是`0`，布尔类型默认值是`false`。

也可以对字段直接进行初始化。

在Java中，创建对象实例的时候，按照如下顺序进行初始化：

1. 先初始化字段，例如，`int age = 10;`表示字段初始化为`10`，`double salary;`表示字段默认初始化为`0`，`String name;`表示引用类型字段默认初始化为`null`；
2. 执行构造方法的代码进行初始化。

因此，构造方法的代码由于后运行，所以，`new Person("Xiao Ming", 12)`的字段值最终由构造方法的代码确定。

### 1.3.2. 多构造方法

可以定义多个构造方法，在通过`new`操作符调用的时候，编译器通过构造方法的参数数量、位置和类型自动区分。

一个构造方法可以调用其他构造方法，这样做的目的是便于代码复用。调用其他构造方法的语法是`this(…)`。

### 1.3.3. 小结

实例在创建时通过`new`操作符会调用其对应的构造方法，构造方法用于初始化实例；

没有定义构造方法时，编译器会自动创建一个默认的无参数构造方法；

可以定义多个构造方法，编译器根据参数自动判断；

可以在一个构造方法内部调用另一个构造方法，便于代码复用。

## 1.4. 方法重载

在一个类中，我们可以定义多个方法。如果有一系列方法，它们的功能都是类似的，只有参数有所不同，那么，可以把这一组方法名做成**同名**方法。这种方法名相同，但各自的参数不同，称为**方法重载**（`Overload`）。

**注意**：方法重载的返回值类型通常都是相同的。

方法重载的目的是，功能类似的方法使用同一名字，更容易记住，因此，调用起来更简单。

举个例子，`String`类提供了多个重载方法`indexOf()`，可以查找子串：

- `int indexOf(int ch)`：根据字符的Unicode码查找；
- `int indexOf(String str)`：根据字符串查找；
- `int indexOf(int ch, int fromIndex)`：根据字符查找，但指定起始位置；
- `int indexOf(String str, int fromIndex)`根据字符串查找，但指定起始位置。

### 1.4.1. 小结

方法重载是指多个方法的方法名相同，但各自的参数不同；

重载方法应该完成类似的功能，参考`String`的`indexOf()`；

重载方法返回值类型应该相同。

## 1.5. 继承

继承是面向对象编程中非常强大的一种机制，它首先可以复用代码。

Java使用`extends`关键字来实现继承。

可见，通过继承，`Student`只需要编写额外的功能，不再需要重复代码。

**注意**：子类自动获得了父类的所有字段，严禁定义与父类重名的字段！

在OOP的术语中，我们把`Person`称为超类（super class），父类（parent class），基类（base class），把`Student`称为子类（subclass），扩展类（extended class）。

### 1.5.1. 继承树

注意到我们在定义`Person`的时候，没有写`extends`。在Java中，没有明确写`extends`的类，编译器会自动加上`extends Object`。所以，任何类，除了`Object`，都会继承自某个类。

Java只允许一个class继承自一个类，因此，一个类有且仅有一个父类。只有`Object`特殊，它没有父类。

### 1.5.2. protected

继承有个特点，就是子类无法访问父类的`private`字段或者`private`方法。

这使得继承的作用被削弱了。为了让子类可以访问父类的字段，我们需要把`private`改为`protected`。用`protected`修饰的字段可以被子类访问。

因此，`protected`关键字可以把字段和方法的访问权限控制在继承树内部，一个`protected`字段和方法可以被其子类，以及子类的子类所访问。

### 1.5.3. super

`super`关键字表示父类（超类）。

实际上，这里使用`super.name`，或者`this.name`，或者`name`，效果都是一样的。编译器会自动定位到父类的`name`字段。

但是，在某些时候，就必须使用`super`。

运行上面的代码，会得到一个编译错误，大意是在`Student`的构造方法中，无法调用`Person`的构造方法。

这是因为在Java中，任何`class`的构造方法，第一行语句必须是调用父类的构造方法。如果没有明确地调用父类的构造方法，编译器会帮我们自动加一句`super();`，`Person`类并没有无参数的构造方法，因此，编译失败。

解决方法是调用`Person`类存在的某个构造方法。

因此我们得出结论：如果父类没有默认的构造方法，子类就必须显式调用`super()`并给出参数以便让编译器定位到父类的一个合适的构造方法。

这里还顺带引出了另一个问题：即子类**不会继承**任何父类的构造方法。子类默认的构造方法是编译器自动生成的，不是继承的。

### 1.5.4. super关键字的用法

super可以理解为是指向自己超（父）类对象的一个指针，而这个超类指的是离自己最近的一个父类。

super也有三种用法：

1. 普通的直接引用：与this类似，super相当于是指向当前对象的父类的引用，这样就可以用super.xxx来引用父类的成员。

2. 子类中的成员变量或方法与父类中的成员变量或方法同名时，用super进行区分

3. 引用父类构造函数

### 1.5.5. this与super的区别

- super:　它引用当前对象的直接父类中的成员（用来访问直接父类中被隐藏的父类中成员数据或函数，基类与派生类中有相同成员定义时如：super.变量名 super.成员函数据名（实参）
- this：它代表当前对象名（在程序中易产生二义性之处，应使用this来指明当前对象；如果函数的形参与类中的成员数据同名，这时需用this来指明成员变量名）
- super()和this()类似，区别是，super()在子类中调用父类的构造方法，this()在本类内调用本类的其它构造方法。
- super()和this()均需放在构造方法内第一行。
- 尽管可以用this调用一个构造器，但却不能调用两个。
- this和super不能同时出现在一个构造函数里面，因为this必然会调用其它的构造函数，其它的构造函数必然也会有super语句的存在，所以在同一个构造函数里面有相同的语句，就失去了语句的意义，编译器也不会通过。
- this()和super()都指的是对象，所以，均不可以在static环境中使用。包括：static变量,static方法，static语句块。
- 从本质上讲，this是一个指向本对象的指针，然而super是一个Java关键字。

### 1.5.6. 阻止继承

正常情况下，只要某个class没有`final`修饰符，那么任何类都可以从该class继承。

从Java 15开始，允许使用`sealed`修饰class，并通过`permits`明确写出能够从该class继承的子类名称。

例如，定义一个`Shape`类：

```java
public sealed class Shape permits Rect, Circle, Triangle {
    ...
}
```

上述`Shape`类就是一个`sealed`类，它只允许指定的3个类继承它。如果写：

```java
public final class Rect extends Shape {...}
```

是没问题的，因为`Rect`出现在`Shape`的`permits`列表中。但是，如果定义一个`Ellipse`就会报错：

```java
public final class Ellipse extends Shape {...}
// Compile error: class is not allowed to extend sealed class: Shape
```

原因是`Ellipse`并未出现在`Shape`的`permits`列表中。这种`sealed`类主要用于一些框架，防止继承被滥用。

`sealed`类在Java 15中目前是预览状态，要启用它，必须使用参数`--enable-preview`和`--source 15`。

### 1.5.7. 向上转型

这种把一个子类类型安全地变为父类类型的赋值，被称为向上转型（upcasting）。

向上转型实际上是把一个子类型安全地变为更加抽象的父类型：

```java
Student s = new Student();
Person p = s; // upcasting, ok
Object o1 = p; // upcasting, ok
Object o2 = s; // upcasting, ok
```

注意到继承树是`Student > Person > Object`，所以，可以把`Student`类型转型为`Person`，或者更高层次的`Object`。

### 1.5.8. 向下转型

和向上转型相反，如果把一个父类类型强制转型为子类类型，就是向下转型（downcasting）。

```java
Person p1 = new Student(); // upcasting, ok
Person p2 = new Person();
Student s1 = (Student) p1; // ok
Student s2 = (Student) p2; // runtime error! ClassCastException!
```

如果测试上面的代码，可以发现：

`Person`类型`p1`实际指向`Student`实例，`Person`类型变量`p2`实际指向`Person`实例。在向下转型的时候，把`p1`转型为`Student`会成功，因为`p1`确实指向`Student`实例，把`p2`转型为`Student`会失败，因为`p2`的实际类型是`Person`，不能把父类变为子类，因为子类功能比父类多，多的功能无法凭空变出来。

因此，向下转型很可能会失败。失败的时候，Java虚拟机会报`ClassCastException`。

为了避免向下转型出错，Java提供了`instanceof`操作符，可以先判断一个实例究竟是不是某种类型：

```java
Person p = new Person();
System.out.println(p instanceof Person); // true
System.out.println(p instanceof Student); // false

Student s = new Student();
System.out.println(s instanceof Person); // true
System.out.println(s instanceof Student); // true

Student n = null;
System.out.println(n instanceof Student); // false
```

`instanceof`实际上判断一个变量所指向的实例是否是**指定类型，或者这个类型的子类**。如果一个引用变量为`null`，那么对任何`instanceof`的判断都为`false`。

利用`instanceof`，在向下转型前可以先判断：

```java
Person p = new Student();
if (p instanceof Student) {
    // 只有判断成功才会向下转型:
    Student s = (Student) p; // 一定会成功
}
```

从Java 14开始，判断`instanceof`后，可以直接转型为指定变量，避免再次强制转型。例如，对于以下代码：

```java
Object obj = "hello";
if (obj instanceof String) {
    String s = (String) obj;
    System.out.println(s.toUpperCase());
}
```

可以改写如下：

`// instanceof variable: ` 

```java
public class Main {
    public static void main(String[] args) {
        Object obj = "hello";
        if (obj instanceof String s) {
            // 可以直接使用变量s:
            System.out.println(s.toUpperCase());
        }
    }
}
```

这种使用`instanceof`的写法更加简洁。

### 1.5.9. 区分继承和组合

在使用继承时，我们要注意逻辑一致性。继承是is关系，组合是has关系。

### 1.5.10. 小结

- 继承是面向对象编程的一种强大的代码复用方式；
- Java只允许单继承，所有类最终的根类是`Object`；
- `protected`允许子类访问父类的字段和方法；
- 子类的构造方法可以通过`super()`调用父类的构造方法；
- 可以安全地向上转型为更抽象的类型；
- 可以强制向下转型，最好借助`instanceof`判断；
- 子类和父类的关系是is，has关系不能用继承。

## 1.6. 多态

在继承关系中，子类如果定义了一个与父类方法签名完全相同的方法，被称为覆写（Override）。

Override和Overload不同的是，如果方法签名如果不同，就是Overload，Overload方法是一个新方法；如果方法签名相同，并且返回值也相同，就是`Override`。

注意：方法名相同，方法参数相同，但方法返回值不同，也是不同的方法。在Java程序中，出现这种情况，编译器会报错。

```java
class Person {
    public void run() { … }
}

class Student extends Person {
    // 不是Override，因为参数不同:
    public void run(String s) { … }
    // 不是Override，因为返回值不同:
    public int run() { … }
}
```

加上`@Override`可以让编译器帮助检查是否进行了正确的覆写。希望进行覆写，但是不小心写错了方法签名，编译器会报错。

`// override ` 

```java
public class Main {
    public static void main(String[] args) {
    }
}

class Person {
    public void run() {}
}

public class Student extends Person {
    @Override // Compile error!
    public void run(String s) {}
}
```

但是`@Override`不是必需的。

在上一节中，我们已经知道，引用变量的声明类型可能与其实际类型不符，例如：

```java
Person p = new Student();
```

现在，我们考虑一种情况，如果子类覆写了父类的方法：

`// override ` 

```java
public class Main {
    public static void main(String[] args) {
        Person p = new Student();
        p.run(); // 应该打印Person.run还是Student.run?
    }
}

class Person {
    public void run() {
        System.out.println("Person.run");
    }
}

class Student extends Person {
    @Override
    public void run() {
        System.out.println("Student.run");
    }
}
```

那么，一个实际类型为`Student`，引用类型为`Person`的变量，调用其`run()`方法，调用的是`Person`还是`Student`的`run()`方法？

运行一下上面的代码就可以知道，实际上调用的方法是`Student`的`run()`方法。因此可得出结论：

Java的实例方法调用是基于运行时的实际类型的动态调用，而非变量的声明类型。

这个非常重要的特性在面向对象编程中称之为多态。它的英文拼写非常复杂：Polymorphic。

### 1.6.1. 多态

多态是指，针对某个类型的方法调用，其真正执行的方法取决于运行时期实际类型的方法。例如：

```java
Person p = new Student();
p.run(); // 无法确定运行时究竟调用哪个run()方法
```

有童鞋会问，从上面的代码一看就明白，肯定调用的是`Student`的`run()`方法啊。

但是，假设我们编写这样一个方法：

```java
public void runTwice(Person p) {
    p.run();
    p.run();
}
```

它传入的参数类型是`Person`，我们是无法知道传入的参数实际类型究竟是`Person`，还是`Student`，还是`Person`的其他子类，因此，也无法确定调用的是不是`Person`类定义的`run()`方法。

所以，多态的特性就是，运行期才能动态决定调用的子类方法。对某个类型调用某个方法，执行的实际方法可能是某个子类的覆写方法。多态具有一个非常强大的功能，就是允许添加更多类型的子类实现功能扩展，却不需要修改基于父类的代码。

### 1.6.2. 覆写Object方法

因为所有的`class`最终都继承自`Object`，而`Object`定义了几个重要的方法：

- `toString()`：把instance输出为`String`；
- `equals()`：判断两个instance是否逻辑相等；
- `hashCode()`：计算一个instance的哈希值。

在必要的情况下，我们可以覆写`Object`的这几个方法。

### 1.6.3. 调用super

在子类的覆写方法中，如果要调用父类的被覆写的方法，可以通过`super`来调用。

### 1.6.4. final

继承可以允许子类覆写父类的方法。

如果一个父类不允许子类对它的某个方法进行覆写，可以把该方法标记为`final`。

如果一个类不希望任何其他类继承自它，那么可以把这个类本身标记为`final`。用`final`修饰的类不能被继承。

对于一个类的实例字段，同样可以用`final`修饰。用`final`修饰的字段在初始化后不能被修改。

可以在构造方法中初始化final字段：

```java
class Person {
    public final String name;
    public Person(String name) {
        this.name = name;
    }
}
```

这种方法更为常用，因为可以保证实例一旦创建，其`final`字段就不可修改。

### 1.6.5. 小结

- 子类可以覆写父类的方法（Override），覆写在子类中改变了父类方法的行为；
- Java的方法调用总是作用于运行期对象的实际类型，这种行为称为多态；
- `final`修饰符有多种作用：
  - `final`修饰的方法可以阻止被覆写；
  - `final`修饰的class可以阻止被继承；
  - `final`修饰的field必须在创建对象时初始化，随后不可修改。

## 1.7. 抽象类

如果父类的方法本身不需要实现任何功能，仅仅是为了定义方法签名，目的是让子类去覆写它，那么，可以把父类的方法声明为抽象方法。

把一个方法声明为`abstract`，表示它是一个抽象方法，本身没有实现任何方法语句。因为这个抽象方法本身是无法执行的，所以，`Person`类也无法被实例化。编译器会告诉我们，无法编译`Person`类，因为它包含抽象方法。

必须把`Person`类本身也声明为`abstract`，才能正确编译它。

### 1.7.1. 抽象类

如果一个`class`定义了方法，但没有具体执行代码，这个方法就是抽象方法，抽象方法用`abstract`修饰。

因为无法执行抽象方法，因此这个类也必须申明为抽象类（abstract class）。

使用`abstract`修饰的类就是抽象类。

**无法实例化的抽象类有什么用**？

因为抽象类本身被设计成只能用于被继承，因此，抽象类可以强迫子类实现其定义的抽象方法，否则编译会报错。因此，抽象方法实际上相当于定义了“规范”。

### 1.7.2. 面向抽象编程

这种尽量引用高层类型，避免引用实际子类型的方式，称之为面向抽象编程。

面向抽象编程的本质就是：

- 上层代码只定义规范（例如：`abstract class Person`）；
- 不需要子类就可以实现业务逻辑（正常编译）；
- 具体的业务逻辑由不同的子类实现，调用者并不关心。

### 1.7.3. 小结

- 通过`abstract`定义的方法是抽象方法，它只有定义，没有实现。抽象方法定义了子类必须实现的接口规范；
- 定义了抽象方法的class必须被定义为抽象类，从抽象类继承的子类必须实现抽象方法；
- 如果不实现抽象方法，则该子类仍是一个抽象类；
- 面向抽象编程使得调用者只关心抽象方法的定义，不关心子类的具体实现。

## 1.8. 接口

在抽象类中，抽象方法本质上是定义接口规范：即规定高层类的接口，从而保证所有子类都有相同的接口实现，这样，多态就能发挥出威力。

如果一个抽象类没有字段，所有方法全部都是抽象方法：

```java
abstract class Person {
    public abstract void run();
    public abstract String getName();
}
```

就可以把该抽象类改写为接口：`interface`。

在Java中，使用`interface`可以声明一个接口：

```java
interface Person {
    void run();
    String getName();
}
```

所谓`interface`，就是比抽象类还要抽象的纯抽象接口，因为它连实例字段（可以有常量字段public static final）都不能有。因为接口定义的所有方法默认都是`public abstract`的，所以这两个修饰符不需要写出来（写不写效果都一样）。

当一个具体的`class`去实现一个`interface`时，需要使用`implements`关键字。

我们知道，在Java中，一个类只能继承自另一个类，不能从多个类继承。但是，一个类可以实现多个`interface`。

### 1.8.1. 术语

注意区分术语：

Java的接口特指`interface`的定义，表示一个接口类型和一组方法签名，而编程接口泛指接口规范，如方法签名，数据格式，网络协议等。

抽象类和接口的对比如下：

|            | abstract class       | interface                   |
| :--------- | :------------------- | :-------------------------- |
| 继承       | 只能extends一个class | 可以implements多个interface |
| 字段       | 可以定义实例字段     | 不能定义实例字段            |
| 抽象方法   | 可以定义抽象方法     | 可以定义抽象方法            |
| 非抽象方法 | 可以定义非抽象方法   | 可以定义default方法         |

### 1.8.2. 接口继承

一个`interface`可以继承自另一个`interface`。`interface`继承自`interface`使用`extends`，它相当于扩展了接口的方法。

### 1.8.3. 继承关系

合理设计`interface`和`abstract class`的继承关系，可以充分复用代码。一般来说，公共逻辑适合放在`abstract class`中，具体逻辑放到各个子类，而接口层次代表抽象程度。可以参考Java的集合类定义的一组接口、抽象类以及具体子类的继承关系：

```ascii
┌───────────────┐
│   Iterable    │
└───────────────┘
        ▲                ┌───────────────────┐
        │                │      Object       │
┌───────────────┐        └───────────────────┘
│  Collection   │                  ▲
└───────────────┘                  │
        ▲     ▲          ┌───────────────────┐
        │     └──────────│AbstractCollection │
┌───────────────┐        └───────────────────┘
│     List      │                  ▲
└───────────────┘                  │
              ▲          ┌───────────────────┐
              └──────────│   AbstractList    │
                         └───────────────────┘
                                ▲     ▲
                                │     │
                                │     │
                     ┌────────────┐ ┌────────────┐
                     │ ArrayList  │ │ LinkedList │
                     └────────────┘ └────────────┘
```

在使用的时候，实例化的对象永远只能是某个具体的子类，但总是通过接口去引用它，因为接口比抽象类更抽象：

```java
List list = new ArrayList(); // 用List接口引用具体子类的实例
Collection coll = list; // 向上转型为Collection接口
Iterable it = coll; // 向上转型为Iterable接口
```

### 1.8.4. default方法

在接口中，可以定义`default`方法。实现类可以不必覆写`default`方法。`default`方法的目的是，当我们需要给接口新增一个方法时，会涉及到修改全部子类。如果新增的是`default`方法，那么子类就不必全部修改，只需要在需要覆写的地方去覆写新增方法。

`default`方法和抽象类的普通方法是有所不同的。因为`interface`没有字段，`default`方法无法访问字段，而抽象类的普通方法可以访问实例字段。

### 1.8.5. 小结

- Java的接口（interface）定义了纯抽象规范，一个类可以实现多个接口；

- 接口也是数据类型，适用于向上转型和向下转型；

- 接口的所有方法都是抽象方法，接口不能定义实例字段；

- 接口可以定义`default`方法（JDK>=1.8）。

## 1.9. 静态字段和静态方法

在一个`class`中定义的字段，我们称之为实例字段。实例字段的特点是，每个实例都有独立的字段，各个实例的同名字段互不影响。

还有一种字段，是用`static`修饰的字段，称为静态字段：`static field`。

实例字段在每个实例中都有自己的一个独立“空间”，但是静态字段只有一个共享“空间”，所有实例都会共享该字段。

对于静态字段，无论修改哪个实例的静态字段，效果都是一样的：所有实例的静态字段都被修改了，原因是静态字段并不属于实例。

虽然实例可以访问静态字段，但是它们指向的其实都是`Person class`的静态字段。所以，所有实例共享一个静态字段。

因此，不推荐用`实例变量.静态字段`去访问静态字段，因为在Java程序中，实例对象并没有静态字段。在代码中，实例对象能访问静态字段只是因为编译器可以根据实例类型自动转换为`类名.静态字段`来访问静态对象。

推荐用类名来访问静态字段。

### 1.9.1. 静态方法

有静态字段，就有静态方法。用`static`修饰的方法称为静态方法。

调用实例方法必须通过一个实例变量，而调用静态方法则不需要实例变量，通过类名就可以调用。

因为静态方法属于`class`而不属于实例，因此，静态方法内部，无法访问`this`变量，也无法访问实例字段，它只能访问静态字段。

通过实例变量也可以调用静态方法，但这只是编译器自动帮我们把实例改写成类名而已。

通常情况下，通过实例变量访问静态字段和静态方法，会得到一个编译警告。

### 1.9.2. 接口的静态字段

因为`interface`是一个纯抽象类，所以它不能定义实例字段。但是，`interface`是可以有静态字段的，并且静态字段必须为`final`类型：

```java
public interface Person {
    public static final int MALE = 1;
    public static final int FEMALE = 2;
}
```

实际上，因为`interface`的字段只能是`public static final`类型，所以我们可以把这些修饰符都去掉，上述代码可以简写为：

```java
public interface Person {
    // 编译器会自动加上public statc final:
    int MALE = 1;
    int FEMALE = 2;
}
```

编译器会自动把该字段变为`public static final`类型。

### 1.9.3. 静态变量和实例变量的区别？

在语法定义上的区别：静态变量前要加static关键字，而实例变量前则不加。

在程序运行时的区别：

- 实例变量属于某个对象的属性，必须创建了实例对象，其中的实例变量才会被分配空间，才能使用这个实例变量。
- 静态变量不属于某个实例对象，而是属于类，所以也称为类变量，只要程序加载了类的字节码，不用创建任何实例对象，静态变量就会被分配空间，静态变量就可以被使用了。总之，实例变量必须创建对象后才可以通过这个对象来使用，静态变量则可以直接使用类名来引用。

### 1.9.4. static应用场景

因为static是被类的实例对象所共享，因此如果某个成员变量是被所有对象所共享的，那么这个成员变量就应该定义为静态变量。

因此比较常见的static应用场景有：

1. 修饰成员变量
2. 修饰成员方法
3. 静态代码块
4. 修饰类【只能修饰内部类也就是静态内部类】
5. 静态导包

### 1.9.5. static注意事项

1. 静态只能访问静态
2. 非静态既可以访问非静态的，也可以访问静态的。

### 1.9.6. static的独特之处

1. 被static修饰的变量或者方法是独立于该类的任何对象，也就是说，这些变量和方法不属于任何一个实例对象，而是被类的实例对象所共享。

2. 在该类被第一次加载的时候，就会去加载被static修饰的部分，而且只在类第一次使用时加载并进行初始化，注意这是第一次用就要初始化，后面根据需要是可以再次赋值的。

3. static变量值在类加载的时候分配空间，以后创建类对象的时候不会重新分配。赋值的话，是可以任意赋值的！

4. 被static修饰的变量或者方法是优先于对象存在的，也就是说当一个类加载完毕之后，即便没有创建对象，也可以去访问。

### 1.9.7. 小结

- 静态字段属于所有实例“共享”的字段，实际上是属于`class`的字段；
- 调用静态方法不需要实例，无法访问`this`，但可以访问静态字段和其他静态方法；
- 静态方法常用于工具类和辅助方法。

## 1.10. 包

在Java中，我们使用`package`来解决名字冲突。

Java定义了一种名字空间，称之为包：`package`。

在Java虚拟机执行的时候，JVM只看完整类名，因此，只要包名不同，类就不同。

包可以是多层结构，用`.`隔开。

 **要特别注意**：包没有父子关系。java.util和java.util.zip是不同的包，两者没有任何继承关系。

没有定义包名的`class`，它使用的是默认包，非常容易引起名字冲突，因此，不推荐不写包名的做法。

### 1.10.1. 包作用域

位于同一个包的类，可以访问包作用域的字段和方法。不用`public`、`protected`、`private`修饰的字段和方法就是包作用域。

### 1.10.2. import

在一个`class`中，我们总会引用其他的`class`。

第一种，直接写出完整类名。

第二种写法是用`import`语句，导入小军的`Arrays`，然后写简单类名。

在写`import`的时候，可以使用`*`，表示把这个包下面的所有`class`都导入进来（但不包括子包的`class`）。

我们一般不推荐这种写法，因为在导入了多个包后，很难看出`Arrays`类属于哪个包。

还有一种`import static`的语法，它可以导入可以导入一个类的静态字段和静态方法。

`import static`很少使用。

Java编译器最终编译出的`.class`文件只使用完整类名，因此，在代码中，当编译器遇到一个`class`名称时：

- 如果是完整类名，就直接根据完整类名查找这个`class`；
- 如果是简单类名，按下面的顺序依次查找：
  - 查找当前`package`是否存在这个`class`；
  - 查找`import`的包是否包含这个`class`；
  - 查找`java.lang`包是否包含这个`class`。

如果按照上面的规则还无法确定类名，则编译报错。

因此，编写class的时候，编译器会自动帮我们做两个import动作：

- 默认自动`import`当前`package`的其他`class`；
- 默认自动`import java.lang.*`。

 注意：自动导入的是java.lang包，但类似java.lang.reflect这些包仍需要手动导入。

如果有两个`class`名称相同，例如，`mr.jun.Arrays`和`java.util.Arrays`，那么只能`import`其中一个，另一个必须写完整类名。

### 1.10.3. import java和javax有什么区别

刚开始的时候 JavaAPI 所必需的包是 java 开头的包，javax 当时只是扩展 API 包来说使用。然而随着时间的推移，javax 逐渐的扩展成为 Java API 的组成部分。但是，将扩展从 javax 包移动到 java 包将是太麻烦了，最终会破坏一堆现有的代码。因此，最终决定 javax 包将成为标准API的一部分。

所以，实际上java和javax没有区别。这都是一个名字。

### 1.10.4. JDK 中常用的包有哪些

1. java.lang：这个是系统的基础类；
2. java.io：这里面是所有输入输出有关的类，比如文件操作等；
3. java.nio：为了完善 io 包中的功能，提高 io 包中性能而写的一个新包；
4. java.net：这里面是与网络有关的类；
5. java.util：这个是系统辅助类，特别是集合类；
6. java.sql：这个是数据库操作的类。

### 1.10.5. 最佳实践

为了避免名字冲突，我们需要确定唯一的包名。推荐的做法是使用倒置的域名来确保唯一性。例如：

- org.apache
- org.apache.commons.log
- com.liaoxuefeng.sample

子包就可以根据功能自行命名。

要注意不要和`java.lang`包的类重名，即自己的类不要使用这些名字：

- String
- System
- Runtime
- ...

要注意也不要和JDK常用类重名：

- java.util.List
- java.text.Format
- java.math.BigInteger
- ...

### 1.10.6. 小结

Java内建的`package`机制是为了避免`class`命名冲突；

JDK的核心类使用`java.lang`包，编译器会自动导入；

JDK的其它常用类定义在`java.util.*`，`java.math.*`，`java.text.*`，……；

包名推荐使用倒置的域名，例如`org.apache`。

## 1.11. 作用域

在Java中，我们经常看到`public`、`protected`、`private`这些修饰符。在Java中，这些修饰符可以用来限定访问作用域。

### 1.11.1. public

定义为`public`的`class`、`interface`可以被其他任何类访问。

上面的`Hello`是`public`，因此，可以被其他包的类访问：

定义为`public`的`field`、`method`可以被其他类访问，前提是首先有访问`class`的权限：

### 1.11.2. private

定义为`private`的`field`、`method`无法被其他类访问。

`private`访问权限被限定在`class`的内部，而且与方法声明顺序*无关*。推荐把`private`方法放到后面，因为`public`方法定义了类对外提供的功能，阅读代码的时候，应该先关注`public`方法。

由于Java支持嵌套类，如果一个类内部还定义了嵌套类，那么，嵌套类拥有访问`private`的权限。

### 1.11.3. protected

`protected`作用于继承关系。定义为`protected`的字段和方法可以被子类访问，以及子类的子类。

### 1.11.4. package

最后，包作用域是指一个类允许访问同一个`package`的没有`public`、`private`修饰的`class`，以及没有`public`、`protected`、`private`修饰的字段和方法。

### 1.11.5. 局部变量

在方法内部定义的变量称为局部变量，局部变量作用域从变量声明处开始到对应的块结束。方法参数也是局部变量。

使用局部变量时，应该尽可能把局部变量的作用域缩小，尽可能延后声明局部变量。

### 1.11.6. final

Java还提供了一个`final`修饰符。`final`与访问权限不冲突，它有很多作用。

- 用`final`修饰`class`可以阻止被继承。

- 用`final`修饰`method`可以阻止被子类覆写。

- 用`final`修饰`field`可以阻止被重新赋值。

- 用`final`修饰局部变量可以阻止被重新赋值。


### 1.11.7. 最佳实践

如果不确定是否需要`public`，就不声明为`public`，即尽可能少地暴露对外的字段和方法。

把方法定义为`package`权限有助于测试，因为测试类和被测试类只要位于同一个`package`，测试代码就可以访问被测试类的`package`权限方法。

一个`.java`文件只能包含一个`public`类，但可以包含多个非`public`类。如果有`public`类，文件名必须和`public`类的名字相同。

### 1.11.8. 小结

- Java内建的访问权限包括`public`、`protected`、`private`和`package`权限；

- Java在方法内部定义的变量是局部变量，局部变量的作用域从变量声明开始，到一个块结束；

- `final`修饰符不是访问权限，它可以修饰`class`、`field`和`method`；

- 一个`.java`文件只能包含一个`public`类，但可以包含多个非`public`类。

## 1.12. 内部类

有一种类，它被定义在另一个类的内部，所以称为内部类（Nested Class）。Java的内部类分为好几种，通常情况用得不多，但也需要了解它们是如何使用的。

### 1.12.1. Inner Class

如果一个类定义在另一个类的内部，这个类就是Inner Class：

```java
class Outer {
    class Inner {
        // 定义了一个Inner Class
    }
}
```

上述定义的`Outer`是一个普通类，而`Inner`是一个Inner Class，它与普通类有个最大的不同，就是Inner Class的实例不能单独存在，必须依附于一个Outer Class的实例。

要实例化一个`Inner`，我们必须首先创建一个`Outer`的实例，然后，调用`Outer`实例的`new`来创建`Inner`实例：

```java
Outer.Inner inner = outer.new Inner();
```

这是因为Inner Class除了有一个`this`指向它自己，还隐含地持有一个Outer Class实例，可以用`Outer.this`访问这个实例。所以，实例化一个Inner Class不能脱离Outer实例。

Inner Class和普通Class相比，除了能引用Outer实例外，还有一个额外的“特权”，就是可以修改Outer Class的`private`字段，因为Inner Class的作用域在Outer Class内部，所以能访问Outer Class的`private`字段和方法。

观察Java编译器编译后的`.class`文件可以发现，`Outer`类被编译为`Outer.class`，而`Inner`类被编译为`Outer$Inner.class`。

### 1.12.2. Anonymous Class

还有一种定义Inner Class的方法，它不需要在Outer Class中明确地定义这个Class，而是在方法内部，通过匿名类（Anonymous Class）来定义。

匿名类和Inner Class一样，可以访问Outer Class的`private`字段和方法。之所以我们要定义匿名类，是因为在这里我们通常不关心类名，比直接定义Inner Class可以少写很多代码。

观察Java编译器编译后的`.class`文件可以发现，`Outer`类被编译为`Outer.class`，而匿名类被编译为`Outer$1.class`。如果有多个匿名类，Java编译器会将每个匿名类依次命名为`Outer$1`、`Outer$2`、`Outer$3`……

除了接口外，匿名类也完全可以继承自普通类。观察以下代码：

`// Anonymous Class ` 

```java
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        HashMap<String, String> map1 = new HashMap<>();
        HashMap<String, String> map2 = new HashMap<>() {}; // 匿名类!
        HashMap<String, String> map3 = new HashMap<>() {
            {
                put("A", "1");
                put("B", "2");
            }
        };
        System.out.println(map3.get("A"));
    }
}
```

`map1`是一个普通的`HashMap`实例，但`map2`是一个匿名类实例，只是该匿名类继承自`HashMap`。`map3`也是一个继承自`HashMap`的匿名类实例，并且添加了`static`代码块来初始化数据。观察编译输出可发现`Main$1.class`和`Main$2.class`两个匿名类文件。

### 1.12.3. Static Nested Class

最后一种内部类和Inner Class类似，但是使用`static`修饰，称为静态内部类（Static Nested Class）：

`// Static Nested Class ` 

```java
public class Main {
    public static void main(String[] args) {
        Outer.StaticNested sn = new Outer.StaticNested();
        sn.hello();
    }
}

class Outer {
    private static String NAME = "OUTER";

    private String name;

    Outer(String name) {
        this.name = name;
    }

    static class StaticNested {
        void hello() {
            System.out.println("Hello, " + Outer.NAME);
        }
    }
}
```

用`static`修饰的内部类和Inner Class有很大的不同，它不再依附于`Outer`的实例，而是一个完全独立的类，因此无法引用`Outer.this`，但它可以访问`Outer`的`private`静态字段和静态方法。如果把`StaticNested`移到`Outer`之外，就失去了访问`private`的权限。



**内部类可以引用它的包含类（外部类）的成员吗？有没有什么限制？**

答：一个内部类对象可以访问创建它的外部类对象的成员，包括私有成员

**完全可以。如果不是静态内部类，那没有什么限制！**

### 1.12.4. 小结

Java的内部类可分为Inner Class、Anonymous Class和Static Nested Class三种：

- Inner Class和Anonymous Class本质上是相同的，都必须依附于Outer Class的实例，即隐含地持有`Outer.this`实例，并拥有Outer Class的`private`访问权限；
- Static Nested Class是独立类，但拥有Outer Class的`private`访问权限。

## 1.13. classpath和jar

`classpath`是JVM用到的一个环境变量，它用来指示JVM如何搜索`class`。

因为Java是编译型语言，源码文件是`.java`，而编译后的`.class`文件才是真正可以被JVM执行的字节码。因此，JVM需要知道，如果要加载一个`abc.xyz.Hello`的类，应该去哪搜索对应的`Hello.class`文件。

所以，`classpath`就是一组目录的集合，它设置的搜索路径与操作系统相关。例如，在Windows系统上，用`;`分隔，带空格的目录用`""`括起来，可能长这样：

```
C:\work\project1\bin;C:\shared;"D:\My Documents\project1\bin"
```

在Linux系统上，用`:`分隔，可能长这样：

```
/usr/shared:/usr/local/bin:/home/liaoxuefeng/bin
```

现在我们假设`classpath`是`.;C:\work\project1\bin;C:\shared`，当JVM在加载`abc.xyz.Hello`这个类时，会依次查找：

- <当前目录>\abc\xyz\Hello.class
- C:\work\project1\bin\abc\xyz\Hello.class
- C:\shared\abc\xyz\Hello.class

注意到`.`代表当前目录。如果JVM在某个路径下找到了对应的`class`文件，就不再往后继续搜索。如果所有路径下都没有找到，就报错。

`classpath`的设定方法有两种：

- 在系统环境变量中设置`classpath`环境变量，不推荐；

- 在启动JVM时设置`classpath`变量，推荐。


我们强烈**不推荐**在系统环境变量中设置`classpath`，那样会污染整个系统环境。在启动JVM时设置`classpath`才是推荐的做法。实际上就是给`java`命令传入`-classpath`或`-cp`参数：

```
java -classpath .;C:\work\project1\bin;C:\shared abc.xyz.Hello
```

或者使用`-cp`的简写：

```
java -cp .;C:\work\project1\bin;C:\shared abc.xyz.Hello
```

没有设置系统环境变量，也没有传入`-cp`参数，那么JVM默认的`classpath`为`.`，即当前目录：

```
java abc.xyz.Hello
```

上述命令告诉JVM只在当前目录搜索`Hello.class`。



在IDE中运行Java程序，IDE自动传入的`-cp`参数是当前工程的`bin`目录和引入的jar包。

通常，我们在自己编写的`class`中，会引用Java核心库的`class`，例如，`String`、`ArrayList`等。这些`class`应该上哪去找？

有很多“如何设置classpath”的文章会告诉你把JVM自带的`rt.jar`放入`classpath`，但事实上，根本不需要告诉JVM如何去Java核心库查找`class`，JVM怎么可能笨到连自己的核心库在哪都不知道？

 不要把任何Java核心库添加到classpath中！JVM根本不依赖classpath加载核心库！

更好的做法是，不要设置`classpath`！默认的当前目录`.`对于绝大多数情况都够用了。

JVM根据classpath设置的`.`在当前目录下查找`com.example.Hello`，即实际搜索文件必须位于`com/example/Hello.class`。如果指定的`.class`文件不存在，或者目录结构和包名对不上，均会报错。

### 1.13.1. jar包

如果有很多`.class`文件，散落在各层目录中，肯定不便于管理。如果能把目录打一个包，变成一个文件，就方便多了。

jar包就是用来干这个事的，它可以把`package`组织的目录层级，以及各个目录下的所有文件（包括`.class`文件和其他文件）都打成一个jar文件，这样一来，无论是备份，还是发给客户，就简单多了。

jar包实际上就是一个zip格式的压缩文件，而jar包相当于目录。如果我们要执行一个jar包的`class`，就可以把jar包放到`classpath`中：

```
java -cp ./hello.jar abc.xyz.Hello
```



**如何创建jar包**？

因为jar包就是zip包，所以，直接在资源管理器中，找到正确的目录，点击右键，在弹出的快捷菜单中选择“发送到”，“压缩(zipped)文件夹”，就制作了一个zip文件。然后，把后缀从`.zip`改为`.jar`，一个jar包就创建成功。

假设编译输出的目录结构是这样：

```ascii
package_sample
└─ bin
   ├─ hong
   │  └─ Person.class
   │  ming
   │  └─ Person.class
   └─ mr
      └─ jun
         └─ Arrays.class
```

这里需要特别注意的是，jar包里的第一层目录，不能是`bin`，而应该是`hong`、`ming`、`mr`。

JVM仍然无法从jar包中查找正确的`class`，原因是`hong.Person`必须按`hong/Person.class`存放，而不是`bin/hong/Person.class`。

jar包还可以包含一个特殊的`/META-INF/MANIFEST.MF`文件，`MANIFEST.MF`是纯文本，可以指定`Main-Class`和其它信息。JVM会自动读取这个`MANIFEST.MF`文件，如果存在`Main-Class`，我们就不必在命令行指定启动的类名，而是用更方便的命令：

```
java -jar hello.jar
```

jar包还可以包含其它jar包，这个时候，就需要在`MANIFEST.MF`文件里配置`classpath`了。

在大型项目中，不可能手动编写`MANIFEST.MF`文件，再手动创建zip包。Java社区提供了大量的开源构建工具，例如[Maven](https://www.liaoxuefeng.com/wiki/1252599548343744/1255945359327200)，可以非常方便地创建jar包。

### 1.13.2. 小结

JVM通过环境变量`classpath`决定搜索`class`的路径和顺序；

不推荐设置系统环境变量`classpath`，始终建议通过`-cp`命令传入；

jar包相当于目录，可以包含很多`.class`文件，方便下载和使用；

`MANIFEST.MF`文件可以提供jar包的信息，如`Main-Class`，这样可以直接运行jar包。

## 1.14. 模块

从Java 9开始，JDK又引入了模块（Module）。

什么是模块？这要从Java 9之前的版本说起。

我们知道，`.class`文件是JVM看到的最小可执行文件，而一个大型程序需要编写很多Class，并生成一堆`.class`文件，很不便于管理，所以，`jar`文件就是`class`文件的容器。

在Java 9之前，一个大型Java程序会生成自己的jar文件，同时引用依赖的第三方jar文件，而JVM自带的Java标准库，实际上也是以jar文件形式存放的，这个文件叫`rt.jar`，一共有60多M。

如果是自己开发的程序，除了一个自己的`app.jar`以外，还需要一堆第三方的jar包，运行一个Java程序，一般来说，命令行写这样：

```bash
java -cp app.jar:a.jar:b.jar:c.jar com.liaoxuefeng.sample.Main
```

**注意**：JVM自带的标准库rt.jar不要写到classpath中，写了反而会干扰JVM的正常运行。

如果漏写了某个运行时需要用到的jar，那么在运行期极有可能抛出`ClassNotFoundException`。

所以，jar只是用于存放class的容器，它并不关心class之间的依赖。

从Java 9开始引入的模块，主要是为了解决“依赖”这个问题。如果`a.jar`必须依赖另一个`b.jar`才能运行，那我们应该给`a.jar`加点说明啥的，让程序在编译和运行的时候能自动定位到`b.jar`，这种自带“依赖关系”的class容器就是模块。

为了表明Java模块化的决心，从Java 9开始，原有的Java标准库已经由一个单一巨大的`rt.jar`分拆成了几十个模块，这些模块以`.jmod`扩展名标识，可以在`$JAVA_HOME/jmods`目录下找到它们：

- java.base.jmod
- java.compiler.jmod
- java.datatransfer.jmod
- java.desktop.jmod
- ...

这些`.jmod`文件每一个都是一个模块，模块名就是文件名。模块之间的依赖关系已经被写入到模块内的`module-info.class`文件了。所有的模块都直接或间接地依赖`java.base`模块，只有`java.base`模块不依赖任何模块，它可以被看作是“根模块”，好比所有的类都是从`Object`直接或间接继承而来。

把一堆class封装为jar仅仅是一个打包的过程，而把一堆class封装为模块则不但需要打包，还需要写入依赖关系，并且还可以包含二进制代码（通常是JNI扩展）。此外，模块支持多版本，即在同一个模块中可以为不同的JVM提供不同的版本。

### 1.14.1. 编写模块

那么，我们应该如何编写模块呢？还是以具体的例子来说。首先，创建模块和原有的创建Java项目是完全一样的，以`oop-module`工程为例，它的目录结构如下：

```ascii
oop-module
├── bin
├── build.sh
└── src
    ├── com
    │   └── itranswarp
    │       └── sample
    │           ├── Greeting.java
    │           └── Main.java
    └── module-info.java
```

其中，`bin`目录存放编译后的class文件，`src`目录存放源码，按包名的目录结构存放，仅仅在`src`目录下多了一个`module-info.java`这个文件，这就是模块的描述文件。在这个模块中，它长这样：

```java
module hello.world {
	requires java.base; // 可不写，任何模块都会自动引入java.base
	requires java.xml;
}
```

其中，`module`是关键字，后面的`hello.world`是模块的名称，它的命名规范与包一致。花括号的`requires xxx;`表示这个模块需要引用的其他模块名。除了`java.base`可以被自动引入外，这里我们引入了一个`java.xml`的模块。

当我们使用模块声明了依赖关系后，才能使用引入的模块。例如，`Main.java`代码如下：

```java
package com.itranswarp.sample;

// 必须引入java.xml模块后才能使用其中的类:
import javax.xml.XMLConstants;

public class Main {
	public static void main(String[] args) {
		Greeting g = new Greeting();
		System.out.println(g.hello(XMLConstants.XML_NS_PREFIX));
	}
}
```

如果把`requires java.xml;`从`module-info.java`中去掉，编译将报错。可见，模块的重要作用就是声明依赖关系。

下面，我们用JDK提供的命令行工具来编译并创建模块。

首先，我们把工作目录切换到`oop-module`，在当前目录下编译所有的`.java`文件，并存放到`bin`目录下，命令如下：

```bash
$ javac -d bin src/module-info.java src/com/itranswarp/sample/*.java
```

如果编译成功，现在项目结构如下：

```ascii
oop-module
├── bin
│   ├── com
│   │   └── itranswarp
│   │       └── sample
│   │           ├── Greeting.class
│   │           └── Main.class
│   └── module-info.class
└── src
    ├── com
    │   └── itranswarp
    │       └── sample
    │           ├── Greeting.java
    │           └── Main.java
    └── module-info.java
```

注意到`src`目录下的`module-info.java`被编译到`bin`目录下的`module-info.class`。

下一步，我们需要把bin目录下的所有class文件先打包成jar，在打包的时候，注意传入`--main-class`参数，让这个jar包能自己定位`main`方法所在的类：

```bash
$ jar --create --file hello.jar --main-class com.itranswarp.sample.Main -C bin .
```

现在我们就在当前目录下得到了`hello.jar`这个jar包，它和普通jar包并无区别，可以直接使用命令`java -jar hello.jar`来运行它。但是我们的目标是创建模块，所以，继续使用JDK自带的`jmod`命令把一个jar包转换成模块：

```bash
$ jmod create --class-path hello.jar hello.jmod
```

于是，在当前目录下我们又得到了`hello.jmod`这个模块文件，这就是最后打包出来的传说中的模块！

### 1.14.2. 运行模块

要运行一个jar，我们使用`java -jar xxx.jar`命令。要运行一个模块，我们只需要指定模块名。试试：

```bash
$ java --module-path hello.jmod --module hello.world
```

结果是一个错误：

```bash
Error occurred during initialization of boot layer
java.lang.module.FindException: JMOD format not supported at execution time: hello.jmod
```

原因是`.jmod`不能被放入`--module-path`中。换成`.jar`就没问题了：

```bash
$ java --module-path hello.jar --module hello.world
Hello, xml!
```

那我们辛辛苦苦创建的`hello.jmod`有什么用？答案是我们可以用它来打包JRE。

### 1.14.3. 打包JRE

前面讲了，为了支持模块化，Java 9首先带头把自己的一个巨大无比的`rt.jar`拆成了几十个`.jmod`模块，原因就是，**运行Java程序的时候，实际上我们用到的JDK模块，并没有那么多。不需要的模块，完全可以删除**。

现在，JRE自身的标准库已经分拆成了模块，只需要带上程序用到的模块，其他的模块就可以被裁剪掉。怎么裁剪JRE呢？并不是说把系统安装的JRE给删掉部分模块，而是“复制”一份JRE，但只带上用到的模块。为此，JDK提供了`jlink`命令来干这件事。命令如下：

```bash
$ jlink --module-path hello.jmod --add-modules java.base,java.xml,hello.world --output jre/
```

我们在`--module-path`参数指定了我们自己的模块`hello.jmod`，然后，在`--add-modules`参数中指定了我们用到的3个模块`java.base`、`java.xml`和`hello.world`，用`,`分隔。最后，在`--output`参数指定输出目录。

现在，在当前目录下，我们可以找到`jre`目录，这是一个完整的并且带有我们自己`hello.jmod`模块的JRE。试试直接运行这个JRE：

```bash
$ jre/bin/java --module hello.world
Hello, xml!
```

要分发我们自己的Java应用程序，只需要把这个`jre`目录打个包给对方发过去，对方直接运行上述命令即可，既不用下载安装JDK，也不用知道如何配置我们自己的模块，极大地方便了分发和部署。

### 1.14.4. 访问权限

前面我们讲过，Java的class访问权限分为public、protected、private和默认的包访问权限。引入模块后，这些访问权限的规则就要稍微做些调整。

确切地说，class的这些访问权限只在一个模块内有效，模块和模块之间。

举个例子：我们编写的模块`hello.world`用到了模块`java.xml`的一个类`javax.xml.XMLConstants`，我们之所以能直接使用这个类，是因为模块`java.xml`的`module-info.java`中声明了若干导出：

```java
module java.xml {
    exports java.xml;
    exports javax.xml.catalog;
    exports javax.xml.datatype;
    ...
}
```

只有它声明的导出的包，外部代码才被允许访问。换句话说，如果外部代码想要访问我们的`hello.world`模块中的`com.itranswarp.sample.Greeting`类，我们必须将其导出：

```java
module hello.world {
    exports com.itranswarp.sample;

    requires java.base;
    requires java.xml;
}
```

因此，模块进一步隔离了代码的访问权限。

### 1.14.5. 小结

- Java 9引入的模块目的是为了管理依赖；

- 使用模块可以按需打包JRE；

- 使用模块对类的访问权限有了进一步限制。

# 2. Java核心类

## 2.1. 字符串和编码

### 2.1.1. String

在Java中，`String`是一个引用类型，它本身也是一个`class`。但是，Java编译器对`String`有特殊处理，即可以直接用`"..."`来表示一个字符串：

```java
String s1 = "Hello!";
```

实际上字符串在`String`内部是通过一个`char[]`数组表示的，因此，按下面的写法也是可以的：

```java
String s2 = new String(new char[] {'H', 'e', 'l', 'l', 'o', '!'});
```

因为`String`太常用了，所以Java提供了`"..."`这种字符串字面量表示方法。

Java字符串的一个重要特点就是字符串**不可变**。这种不可变性是通过内部的`private final char[]`字段，以及没有任何修改`char[]`的方法实现的。

### 2.1.2. 字符串比较

当我们想要比较两个字符串是否相同时，要特别注意，我们实际上是想比较字符串的内容是否相同。必须使用`equals()`方法而不能用`==`。

两个字符串用`==`和`equals()`比较都为`true`，但实际上那只是Java编译器在编译期，会自动把所有相同的字符串当作一个对象放入常量池，自然`s1`和`s2`的引用就是相同的。

所以，这种`==`比较返回`true`纯属巧合。换一种写法，`==`比较就会失败：

结论：两个字符串比较，必须总是使用`equals()`方法。

要忽略大小写比较，使用`equalsIgnoreCase()`方法。



`String`类还提供了多种方法来搜索子串、提取子串。常用的方法有：

```java
// 是否包含子串:
"Hello".contains("ll"); // true
```

注意到`contains()`方法的参数是`CharSequence`而不是`String`，因为`CharSequence`是`String`的父类。



搜索子串的更多的例子：

```java
"Hello".indexOf("l"); // 2
"Hello".lastIndexOf("l"); // 3
"Hello".startsWith("He"); // true
"Hello".endsWith("lo"); // true
```

提取子串的例子：

```java
"Hello".substring(2); // "llo"
"Hello".substring(2, 4); "ll"
```

注意索引号是从`0`开始的。

### 2.1.3. 去除首尾空白字符

使用`trim()`方法可以移除字符串首尾空白字符。空白字符包括空格，`\t`，`\r`，`\n`。

**注意**：`trim()`并没有改变字符串的内容，而是返回了一个新字符串。

另一个`strip()`方法也可以移除字符串首尾空白字符。它和`trim()`不同的是，类似中文的空格字符`\u3000`也会被移除。

`String`还提供了`isEmpty()`和`isBlank()`来判断字符串是否为空和空白字符串：

```java
"".isEmpty(); // true，因为字符串长度为0
"  ".isEmpty(); // false，因为字符串长度不为0
"  \n".isBlank(); // true，因为只包含空白字符
" Hello ".isBlank(); // false，因为包含非空白字符
```

### 2.1.4. 替换子串

要在字符串中替换子串，有两种方法。一种是根据字符或字符串替换：

```java
String s = "hello";
s.replace('l', 'w'); // "hewwo"，所有字符'l'被替换为'w'
s.replace("ll", "~~"); // "he~~o"，所有子串"ll"被替换为"~~"
```

另一种是通过正则表达式替换：

```java
String s = "A,,B;C ,D";
s.replaceAll("[\\,\\;\\s]+", ","); // "A,B,C,D"
```

### 2.1.5. 分割字符串

要分割字符串，使用`split()`方法，并且传入的也是正则表达式：

```java
String s = "A,B,C,D";
String[] ss = s.split("\\,"); // {"A", "B", "C", "D"}
```

### 2.1.6. 拼接字符串

拼接字符串使用静态方法`join()`，它用指定的字符串连接字符串数组：

```java
String[] arr = {"A", "B", "C"};
String s = String.join("***", arr); // "A***B***C"
```

### 2.1.7. 格式化字符串

字符串提供了`formatted()`方法和`format()`静态方法，可以传入其他参数，替换占位符，然后生成新的字符串：

`// String ` 

```java
public class Main {
    public static void main(String[] args) {
        String s = "Hi %s, your score is %d!";
        System.out.println(s.formatted("Alice", 80));
        System.out.println(String.format("Hi %s, your score is %.2f!", "Bob", 59.5));
    }
}
```

有几个占位符，后面就传入几个参数。参数类型要和占位符一致。我们经常用这个方法来格式化信息。常用的占位符有：

- `%s`：显示字符串；
- `%d`：显示整数；
- `%x`：显示十六进制整数；
- `%f`：显示浮点数。

占位符还可以带格式，例如`%.2f`表示显示两位小数。如果你不确定用啥占位符，那就始终用`%s`，因为`%s`可以显示任何数据类型。

### 2.1.8. 类型转换

要把任意基本类型或引用类型转换为字符串，可以使用静态方法`valueOf()`。

要把字符串转换为其他类型，就需要根据情况。例如，把字符串转换为`int`类型：

```java
int n1 = Integer.parseInt("123"); // 123
int n2 = Integer.parseInt("ff", 16); // 按十六进制转换，255
```

把字符串转换为`boolean`类型：

```java
boolean b1 = Boolean.parseBoolean("true"); // true
boolean b2 = Boolean.parseBoolean("FALSE"); // false
```

要特别注意，`Integer`有个`getInteger(String)`方法，它不是将字符串转换为`int`，而是把该字符串对应的系统变量转换为`Integer`：

```java
Integer.getInteger("java.version"); // 版本号，11
```

### 2.1.9. 转换为char[]

`String`和`char[]`类型可以互相转换，方法是：

```java
char[] cs = "Hello".toCharArray(); // String -> char[]
String s = new String(cs); // char[] -> String
```

如果修改了`char[]`数组，`String`并不会改变，是因为通过`new String(char[])`创建新的`String`实例时，它并不会直接引用传入的`char[]`数组，而是会复制一份，所以，修改外部的`char[]`数组不会影响`String`实例内部的`char[]`数组，因为这是两个不同的数组。

从`String`的不变性设计可以看出，如果传入的对象有可能改变，我们需要复制而不是直接引用。

### 2.1.10. 字符编码

在早期的计算机系统中，为了给字符编码，美国国家标准学会（American National Standard Institute：ANSI）制定了一套英文字母、数字和常用符号的编码，它占用一个字节，编码范围从`0`到`127`，最高位始终为`0`，称为`ASCII`编码。例如，字符`'A'`的编码是`0x41`，字符`'1'`的编码是`0x31`。

如果要把汉字也纳入计算机编码，很显然一个字节是不够的。`GB2312`标准使用两个字节表示一个汉字，其中第一个字节的最高位始终为`1`，以便和`ASCII`编码区分开。例如，汉字`'中'`的`GB2312`编码是`0xd6d0`。

类似的，日文有`Shift_JIS`编码，韩文有`EUC-KR`编码，这些编码因为标准不统一，同时使用，就会产生冲突。

为了统一全球所有语言的编码，全球统一码联盟发布了`Unicode`编码，它把世界上主要语言都纳入同一个编码，这样，中文、日文、韩文和其他语言就不会冲突。

`Unicode`编码需要两个或者更多字节表示，我们可以比较中英文字符在`ASCII`、`GB2312`和`Unicode`的编码：

英文字符`'A'`的`ASCII`编码和`Unicode`编码：

```ascii
         ┌────┐
ASCII:   │ 41 │
         └────┘
         ┌────┬────┐
Unicode: │ 00 │ 41 │
         └────┴────┘
```

英文字符的`Unicode`编码就是简单地在前面添加一个`00`字节。

中文字符`'中'`的`GB2312`编码和`Unicode`编码：

```ascii
         ┌────┬────┐
GB2312:  │ d6 │ d0 │
         └────┴────┘
         ┌────┬────┐
Unicode: │ 4e │ 2d │
         └────┴────┘
```

那我们经常使用的`UTF-8`又是什么编码呢？因为英文字符的`Unicode`编码高字节总是`00`，包含大量英文的文本会浪费空间，所以，出现了`UTF-8`编码，它是一种变长编码，用来把固定长度的`Unicode`编码变成1～4字节的变长编码。通过`UTF-8`编码，英文字符`'A'`的`UTF-8`编码变为`0x41`，正好和`ASCII`码一致，而中文`'中'`的`UTF-8`编码为3字节`0xe4b8ad`。

`UTF-8`编码的另一个好处是容错能力强。如果传输过程中某些字符出错，不会影响后续字符，因为`UTF-8`编码依靠高字节位来确定一个字符究竟是几个字节，它经常用来作为传输编码。

在Java中，`char`类型实际上就是两个字节的`Unicode`编码。如果我们要手动把字符串转换成其他编码，可以这样做：

```java
byte[] b1 = "Hello".getBytes(); // 按系统默认编码转换，不推荐
byte[] b2 = "Hello".getBytes("UTF-8"); // 按UTF-8编码转换
byte[] b2 = "Hello".getBytes("GBK"); // 按GBK编码转换
byte[] b3 = "Hello".getBytes(StandardCharsets.UTF_8); // 按UTF-8编码转换
```

**注意**：转换编码后，就不再是`char`类型，而是`byte`类型表示的数组。

如果要把已知编码的`byte[]`转换为`String`，可以这样做：

```java
byte[] b = ...
String s1 = new String(b, "GBK"); // 按GBK转换
String s2 = new String(b, StandardCharsets.UTF_8); // 按UTF-8转换
```

**始终牢记**：Java的`String`和`char`在内存中总是以Unicode编码表示。

### 2.1.11. 延伸阅读(需要看第二遍)

对于不同版本的JDK，`String`类在内存中有不同的优化方式。具体来说，早期JDK版本的`String`总是以`char[]`存储，它的定义如下：

```java
public final class String {
    private final char[] value;
    private final int offset;
    private final int count;
}
```

而较新的JDK版本的`String`则以`byte[]`存储：如果`String`仅包含ASCII字符，则每个`byte`存储一个字符，否则，每两个`byte`存储一个字符，这样做的目的是为了节省内存，因为大量的长度较短的`String`通常仅包含ASCII字符：

```java
public final class String {
    private final byte[] value;
    private final byte coder; // 0 = LATIN1, 1 = UTF16
```

对于使用者来说，`String`内部的优化不影响任何已有代码，因为它的`public`方法签名是不变的。

### 2.1.12. 小结

- Java字符串`String`是不可变对象；
- 字符串操作不改变原字符串内容，而是返回新字符串；
- 常用的字符串操作：提取子串、查找、替换、大小写转换等；
- Java使用Unicode编码表示`String`和`char`；
- 转换编码就是将`String`和`byte[]`转换，需要指定编码；
- 转换为`byte[]`时，始终优先考虑`UTF-8`编码。

## 2.2. StringBuilder

Java编译器对`String`做了特殊处理，使得我们可以直接用`+`拼接字符串。

虽然可以直接拼接字符串，但是，在循环中，每次循环都会创建新的字符串对象，然后扔掉旧的字符串。这样，绝大部分字符串都是临时对象，不但浪费内存，还会影响GC效率。

为了能高效拼接字符串，Java标准库提供了`StringBuilder`，它是一个可变对象，可以预分配缓冲区，这样，往`StringBuilder`中新增字符时，不会创建新的临时对象：

```java
StringBuilder sb = new StringBuilder(1024);
for (int i = 0; i < 1000; i++) {
    sb.append(',');
    sb.append(i);
}
String s = sb.toString();
```

`StringBuilder`还可以进行链式操作：

`// 链式操作 ` 

```java
public class Main {
    public static void main(String[] args) {
        var sb = new StringBuilder(1024);
        sb.append("Mr ")
          .append("Bob")
          .append("!")
          .insert(0, "Hello, ");
        System.out.println(sb.toString());
    }
}
```

如果我们查看`StringBuilder`的源码，可以发现，进行链式操作的关键是，定义的`append()`方法会返回`this`，这样，就可以不断调用自身的其他方法。

注意：对于普通的字符串`+`操作，并不需要我们将其改写为`StringBuilder`，因为Java编译器在编译时就自动把多个连续的`+`操作编码为`StringConcatFactory`的操作。在运行期，`StringConcatFactory`会自动把字符串连接操作优化为数组复制或者`StringBuilder`操作。

你可能还听说过`StringBuffer`，这是Java早期的一个`StringBuilder`的线程安全版本，它通过同步来保证多个线程操作`StringBuffer`也是安全的，但是同步会带来执行速度的下降。

`StringBuilder`和`StringBuffer`接口完全相同，现在完全没有必要使用`StringBuffer`。

### 2.2.1. 小结

- `StringBuilder`是可变对象，用来高效拼接字符串；

- `StringBuilder`可以支持链式操作，实现链式操作的关键是返回实例本身；

- `StringBuffer`是`StringBuilder`的线程安全版本，现在很少使用。

## 2.3. StringJoiner

类似用分隔符拼接数组的需求很常见，所以Java标准库还提供了一个`StringJoiner`来干这个事：

```java
var sj = new StringJoiner(", ");
```

慢着！用`StringJoiner`的结果少了前面的`"Hello "`和结尾的`"!"`！遇到这种情况，需要给`StringJoiner`指定“开头”和“结尾”：

```java
var sj = new StringJoiner(", ", "Hello ", "!");
```

### 2.3.1. String.join()

`String`还提供了一个静态方法`join()`，这个方法在内部使用了`StringJoiner`来拼接字符串，在不需要指定“开头”和“结尾”的时候，用`String.join()`更方便：

```java
String[] names = {"Bob", "Alice", "Grace"};
var s = String.join(", ", names);
```

### 2.3.2. 小结

- 用指定分隔符拼接字符串数组时，使用`StringJoiner`或者`String.join()`更方便；

- 用`StringJoiner`拼接字符串时，还可以额外附加一个“开头”和“结尾”。


## 2.4. 包装类型

Java的数据类型分两种：

- 基本类型：`byte`，`short`，`int`，`long`，`boolean`，`float`，`double`，`char`
- 引用类型：所有`class`和`interface`类型

引用类型可以赋值为`null`，表示空，但基本类型不能赋值为`null`：

```java
String s = null;
int n = null; // compile error!
```

那么，如何把一个基本类型视为对象（引用类型）？

想要把`int`基本类型变成一个引用类型，我们可以定义一个`Integer`类，它只包含一个实例字段`int`，这样，`Integer`类就可以视为`int`的包装类（Wrapper Class）：

```java
public class Integer {
    private int value;

    public Integer(int value) {
        this.value = value;
    }

    public int intValue() {
        return this.value;
    }
}
```

因为包装类型非常有用，Java核心库为每种基本类型都提供了对应的包装类型：

| 基本类型 | 对应的引用类型      |
| :------- | :------------------ |
| boolean  | java.lang.Boolean   |
| byte     | java.lang.Byte      |
| short    | java.lang.Short     |
| int      | java.lang.Integer   |
| long     | java.lang.Long      |
| float    | java.lang.Float     |
| double   | java.lang.Double    |
| char     | java.lang.Character |

### 2.4.1. Auto Boxing

直接把`int`变为`Integer`的赋值写法，称为自动装箱（Auto Boxing），反过来，把`Integer`变为`int`的赋值写法，称为自动拆箱（Auto Unboxing）。

**注意**：自动装箱和自动拆箱只发生在编译阶段，目的是为了少写代码。

装箱和拆箱会影响代码的执行效率，因为编译后的`class`代码是严格区分基本类型和引用类型的。并且，自动拆箱执行时可能会报`NullPointerException`。

### 2.4.2. 不变类

所有的包装类型都是不变类。我们查看`Integer`的源码可知，它的核心代码如下：

```java
public final class Integer {
    private final int value;
}
```

因此，一旦创建了`Integer`对象，该对象就是不变的。

对两个`Integer`实例进行比较要特别注意：绝对不能用`==`比较，因为`Integer`是引用类型，必须使用`equals()`比较。



仔细观察结果的童鞋可以发现，`==`比较，较小的两个相同的`Integer`返回`true`，较大的两个相同的`Integer`返回`false`，这是因为`Integer`是不变类，编译器把`Integer x = 127;`自动变为`Integer x = Integer.valueOf(127);`，为了节省内存，`Integer.valueOf()`对于较小的数，始终返回相同的实例，因此，`==`比较“恰好”为`true`，但我们**绝不能**因为Java标准库的`Integer`内部有缓存优化就用`==`比较，必须用`equals()`方法比较两个`Integer`。

按照语义编程，而不是针对特定的底层实现去“优化”。

因为`Integer.valueOf()`可能始终返回同一个`Integer`实例，因此，在我们自己创建`Integer`的时候，以下两种方法：

- 方法1：`Integer n = new Integer(100);`
- 方法2：`Integer n = Integer.valueOf(100);`

方法2更好，因为方法1总是创建新的`Integer`实例，方法2把内部优化留给`Integer`的实现者去做，即使在当前版本没有优化，也有可能在下一个版本进行优化。



我们把能创建“新”对象的静态方法称为静态工厂方法。`Integer.valueOf()`就是静态工厂方法，它尽可能地返回缓存的实例以节省内存。

创建新对象时，优先选用静态工厂方法而不是new操作符。

如果我们考察`Byte.valueOf()`方法的源码，可以看到，标准库返回的`Byte`实例全部是缓存实例，但调用者并不关心静态工厂方法以何种方式创建新实例还是直接返回缓存的实例。

### 2.4.3. 进制转换

`Integer`类本身还提供了大量方法，例如，最常用的静态方法`parseInt()`可以把字符串解析成一个整数：

```java
int x1 = Integer.parseInt("100"); // 100
int x2 = Integer.parseInt("100", 16); // 256,因为按16进制解析
```

`Integer`还可以把整数格式化为指定进制的字符串：

`// Integer: ` 

```java
public class Main {
    public static void main(String[] args) {
        System.out.println(Integer.toString(100)); // "100",表示为10进制
        System.out.println(Integer.toString(100, 36)); // "2s",表示为36进制
        System.out.println(Integer.toHexString(100)); // "64",表示为16进制
        System.out.println(Integer.toOctalString(100)); // "144",表示为8进制
        System.out.println(Integer.toBinaryString(100)); // "1100100",表示为2进制
    }
}
```

注意：上述方法的输出都是`String`，在计算机内存中，只用二进制表示，不存在十进制或十六进制的表示方法。`int n = 100`在内存中总是以4字节的二进制表示：

```ascii
┌────────┬────────┬────────┬────────┐
│00000000│00000000│00000000│01100100│
└────────┴────────┴────────┴────────┘
```

我们经常使用的`System.out.println(n);`是依靠核心库自动把整数格式化为10进制输出并显示在屏幕上，使用`Integer.toHexString(n)`则通过核心库自动把整数格式化为16进制。

这里我们注意到程序设计的一个重要原则：数据的存储和显示要分离。

Java的包装类型还定义了一些有用的静态变量

```java
// boolean只有两个值true/false，其包装类型只需要引用Boolean提供的静态字段:
Boolean t = Boolean.TRUE;
Boolean f = Boolean.FALSE;
// int可表示的最大/最小值:
int max = Integer.MAX_VALUE; // 2147483647
int min = Integer.MIN_VALUE; // -2147483648
// long类型占用的bit和byte数量:
int sizeOfLong = Long.SIZE; // 64 (bits)
int bytesOfLong = Long.BYTES; // 8 (bytes)
```

最后，所有的整数和浮点数的包装类型都继承自`Number`，因此，可以非常方便地直接通过包装类型获取各种基本类型：

```java
// 向上转型为Number:
Number num = new Integer(999);
// 获取byte, int, long, float, double:
byte b = num.byteValue();
int n = num.intValue();
long ln = num.longValue();
float f = num.floatValue();
double d = num.doubleValue();
```

### 2.4.4. 处理无符号整型

在Java中，并没有无符号整型（Unsigned）的基本数据类型。无符号整型和有符号整型的转换在Java中就需要借助包装类型的静态方法完成。

例如，byte是有符号整型，范围是`-128`~`+127`，但如果把`byte`看作无符号整型，它的范围就是`0`~`255`。我们把一个负的`byte`按无符号整型转换为`int`：

`// Byte ` 

```java
public class Main {
    public static void main(String[] args) {
        byte x = -1;
        byte y = 127;
        System.out.println(Byte.toUnsignedInt(x)); // 255
        System.out.println(Byte.toUnsignedInt(y)); // 127
    }
}
```

因为`byte`的`-1`的二进制表示是`11111111`，以无符号整型转换后的`int`就是`255`。

### 2.4.5. 小结

- Java核心库提供的包装类型可以把基本类型包装为`class`；

- 自动装箱和自动拆箱都是在编译期完成的（JDK>=1.5）；

- 装箱和拆箱会影响执行效率，且拆箱时可能发生`NullPointerException`；

- 包装类型的比较必须使用`equals()`；

- 整数和浮点数的包装类型都继承自`Number`；

- 包装类型提供了大量实用方法。


## 2.5. JavaBean

在Java中，有很多`class`的定义都符合这样的规范：

- 若干`private`实例字段；
- 通过`public`方法来读写实例字段。

如果读写方法符合以下这种命名规范：

```java
// 读方法:
public Type getXyz()
// 写方法:
public void setXyz(Type value)
```

那么这种`class`被称为`JavaBean`。

上面的字段是`xyz`，那么读写方法名分别以`get`和`set`开头，并且后接大写字母开头的字段名`Xyz`，因此两个读写方法名分别是`getXyz()`和`setXyz()`。

`boolean`字段比较特殊，它的读方法一般命名为`isXyz()`：

```java
// 读方法:
public boolean isChild()
// 写方法:
public void setChild(boolean value)
```

我们通常把一组对应的读方法（`getter`）和写方法（`setter`）称为属性（`property`）。

只有`getter`的属性称为只读属性（read-only）。

- 对应的读方法是`int getAge()`
- 无对应的写方法`setAge(int)`

类似的，只有`setter`的属性称为只写属性（write-only）。

`getter`和`setter`也是一种数据封装的方法。

### 2.5.1. JavaBean的作用

JavaBean主要用来传递数据，即把一组数据组合成一个JavaBean便于传输。

### 2.5.2. 枚举JavaBean属性

要枚举一个JavaBean的所有属性，可以直接使用Java核心库提供的`Introspector`：

```java
import java.beans.*;

public class Main {
    public static void main(String[] args) throws Exception {
        BeanInfo info = Introspector.getBeanInfo(Person.class);
        for (PropertyDescriptor pd : info.getPropertyDescriptors()) {
            System.out.println(pd.getName());
            System.out.println("  " + pd.getReadMethod());
            System.out.println("  " + pd.getWriteMethod());
        }
    }
}

class Person {
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
```

运行上述代码，可以列出所有的属性，以及对应的读写方法。注意`class`属性是从`Object`继承的`getClass()`方法带来的。

### 2.5.3. 小结

- JavaBean是一种符合命名规范的`class`，它通过`getter`和`setter`来定义属性；

- 属性是一种通用的叫法，并非Java语法规定；

- 可以利用IDE快速生成`getter`和`setter`；

- 使用`Introspector.getBeanInfo()`可以获取属性列表。


## 2.6. 枚举类

### 2.6.1. enum

为了让编译器能自动检查某个值在枚举的集合内，并且，不同用途的枚举需要不同的类型来标记，不能混用，我们可以使用`enum`来定义枚举类：

`// enum ` 

```java
public class Main {
    public static void main(String[] args) {
        Weekday day = Weekday.SUN;
        if (day == Weekday.SAT || day == Weekday.SUN) {
            System.out.println("Work at home!");
        } else {
            System.out.println("Work at office!");
        }
    }
}

enum Weekday {
    SUN, MON, TUE, WED, THU, FRI, SAT;
}
```

注意到定义枚举类是通过关键字`enum`实现的，我们只需依次列出枚举的常量名。

和`int`定义的常量相比，使用`enum`定义枚举有如下好处：

首先，`enum`常量本身带有类型信息，即`Weekday.SUN`类型是`Weekday`，编译器会自动检查出类型错误。例如，下面的语句不可能编译通过：

```java
int day = 1;
if (day == Weekday.SUN) { // Compile error: bad operand types for binary operator '=='
}
```

其次，不可能引用到非枚举的值，因为无法通过编译。

最后，不同类型的枚举不能互相比较或者赋值，因为类型不符。例如，不能给一个`Weekday`枚举类型的变量赋值为`Color`枚举类型的值。

这就使得编译器可以在编译期自动检查出所有可能的潜在错误。

### 2.6.2. enum的比较

使用`enum`定义的枚举类是一种引用类型。前面我们讲到，引用类型比较，要使用`equals()`方法，如果使用`==`比较，它比较的是两个引用类型的变量是否是同一个对象。因此，引用类型比较，要始终使用`equals()`方法，但`enum`类型可以例外。

这是因为`enum`类型的每个常量在JVM中只有一个唯一实例，所以可以直接用`==`比较：

```java
if (day == Weekday.FRI) { // ok!
}
if (day.equals(Weekday.SUN)) { // ok, but more code!
}
```

### 2.6.3. enum类型

通过`enum`定义的枚举类，和其他的`class`有什么区别？

答案是没有任何区别。`enum`定义的类型就是`class`，只不过它有以下几个特点：

- 定义的`enum`类型总是继承自`java.lang.Enum`，且无法被继承；
- 只能定义出`enum`的实例，而无法通过`new`操作符创建`enum`的实例；
- 定义的每个实例都是引用类型的唯一实例；
- 可以将`enum`类型用于`switch`语句。

例如，我们定义的`Color`枚举类：

```java
public enum Color {
    RED, GREEN, BLUE;
}
```

编译器编译出的`class`大概就像这样：

```java
public final class Color extends Enum { // 继承自Enum，标记为final class
    // 每个实例均为全局唯一:
    public static final Color RED = new Color();
    public static final Color GREEN = new Color();
    public static final Color BLUE = new Color();
    // private构造方法，确保外部无法调用new操作符:
    private Color() {}
}
```

所以，编译后的`enum`类和普通`class`并没有任何区别。但是我们自己无法按定义普通`class`那样来定义`enum`，必须使用`enum`关键字，这是Java语法规定的。

因为`enum`是一个`class`，每个枚举的值都是`class`实例，因此，这些实例有一些方法：

#### 2.6.3.1. name()

返回常量名，例如：

```java
String s = Weekday.SUN.name(); // "SUN"
```

#### 2.6.3.2. ordinal()

返回定义的常量的顺序，从0开始计数，例如：

```java
int n = Weekday.MON.ordinal(); // 1
```

改变枚举常量定义的顺序就会导致`ordinal()`返回值发生变化。例如：

```java
public enum Weekday {
    SUN, MON, TUE, WED, THU, FRI, SAT;
}
```

和

```java
public enum Weekday {
    MON, TUE, WED, THU, FRI, SAT, SUN;
}
```

的`ordinal`就是不同的。

有些童鞋会想，`Weekday`的枚举常量如果要和`int`转换，使用`ordinal()`不是非常方便？比如这样写：

```java
String task = Weekday.MON.ordinal() + "/ppt";
saveToFile(task);
```

但是，如果不小心修改了枚举的顺序，编译器是无法检查出这种逻辑错误的。要编写健壮的代码，就不要依靠`ordinal()`的返回值。因为`enum`本身是`class`，所以我们可以定义`private`的构造方法，并且，给每个枚举常量添加字段：

`// enum ` 

```java
public class Main {
    public static void main(String[] args) {
        Weekday day = Weekday.SUN;
        if (day.dayValue == 6 || day.dayValue == 0) {
            System.out.println("Work at home!");
        } else {
            System.out.println("Work at office!");
        }
    }
}

enum Weekday {
    MON(1), TUE(2), WED(3), THU(4), FRI(5), SAT(6), SUN(0);

    public final int dayValue;

    private Weekday(int dayValue) {
        this.dayValue = dayValue;
    }
}
```

这样就无需担心顺序的变化，新增枚举常量时，也需要指定一个`int`值。

**注意**：枚举类的字段也可以是非final类型，即可以在运行期修改，但是不推荐这样做！

默认情况下，对枚举常量调用`toString()`会返回和`name()`一样的字符串。但是，`toString()`可以被覆写，而`name()`则不行。我们可以给`Weekday`添加`toString()`方法。

**注意**：判断枚举常量的名字，要始终使用name()方法，绝不能调用toString()！

### 2.6.4. switch

最后，枚举类可以应用在`switch`语句中。因为枚举类天生具有类型信息和有限个枚举常量，所以比`int`、`String`类型更适合用在`switch`语句中：

`// switch ` 

```java
public class Main {
    public static void main(String[] args) {
        Weekday day = Weekday.SUN;
        switch(day) {
            case MON:
            case TUE:
            case WED:
            case THU:
            case FRI:
                System.out.println("Today is " + day + ". Work at office!");
                break;
            case SAT:
            case SUN:
                System.out.println("Today is " + day + ". Work at home!");
                break;
            default:
                throw new RuntimeException("cannot process " + day);
        }
    }
}

enum Weekday {
    MON, TUE, WED, THU, FRI, SAT, SUN;
}
```

加上`default`语句，可以在漏写某个枚举常量时自动报错，从而及时发现错误。

### 2.6.5. 小结

- Java使用`enum`定义枚举类型，它被编译器编译为`final class Xxx extends Enum { … }`；

- 通过`name()`获取常量定义的字符串，注意不要使用`toString()`；

- 通过`ordinal()`返回常量定义的顺序（无实质意义）；

- 可以为`enum`编写构造方法、字段和方法

- `enum`的构造方法要声明为`private`，字段强烈建议声明为`final`；

- `enum`适合用在`switch`语句中。

## 2.7. 记录类

使用`String`、`Integer`等类型的时候，这些类型都是不变类，一个不变类具有以下特点：

1. 定义class时使用`final`，无法派生子类；
2. 每个字段使用`final`，保证创建实例后无法修改任何字段。

假设我们希望定义一个`Point`类，有`x`、`y`两个变量，同时它是一个不变类，可以这么写：

```
public final class Point {
    private final int x;
    private final int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int x() {
        return this.x;
    }

    public int y() {
        return this.y;
    }
}
```

为了保证不变类的比较，还需要正确覆写`equals()`和`hashCode()`方法，这样才能在集合类中正常使用。

### 2.7.1. record

从Java 14开始，引入了新的`Record`类。我们定义`Record`类时，使用关键字`record`。把上述`Point`类改写为`Record`类，代码如下：

`// Record ` 

```java
public class Main {
    public static void main(String[] args) {
        Point p = new Point(123, 456);
        System.out.println(p.x());
        System.out.println(p.y());
        System.out.println(p);
    }
}

public record Point(int x, int y) {}
```

仔细观察`Point`的定义：

```java
public record Point(int x, int y) {}
```

把上述定义改写为class，相当于以下代码：

```java
public final class Point extends Record {
    private final int x;
    private final int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int x() {
        return this.x;
    }

    public int y() {
        return this.y;
    }

    public String toString() {
        return String.format("Point[x=%s, y=%s]", x, y);
    }

    public boolean equals(Object o) {
        ...
    }

    public int hashCode() {
        ...
    }
}
```

除了用`final`修饰class以及每个字段外，编译器还自动为我们创建了构造方法，和字段名同名的方法，以及覆写`toString()`、`equals()`和`hashCode()`方法。

换句话说，使用`record`关键字，可以一行写出一个不变类。

和`enum`类似，我们自己不能直接从`Record`派生，只能通过`record`关键字由编译器实现继承。

### 2.7.2. 构造方法

编译器默认按照`record`声明的变量顺序自动创建一个构造方法，并在方法内给字段赋值。那么问题来了，如果我们要检查参数，应该怎么办？

假设`Point`类的`x`、`y`不允许负数，我们就得给`Point`的构造方法加上检查逻辑：

```java
public record Point(int x, int y) {
    public Point {
        if (x < 0 || y < 0) {
            throw new IllegalArgumentException();
        }
    }
}
```

注意到方法`public Point {...}`被称为Compact Constructor，它的目的是让我们编写检查逻辑，编译器最终生成的构造方法如下：

```java
public final class Point extends Record {
    public Point(int x, int y) {
        // 这是我们编写的Compact Constructor:
        if (x < 0 || y < 0) {
            throw new IllegalArgumentException();
        }
        // 这是编译器继续生成的赋值代码:
        this.x = x;
        this.y = y;
    }
    ...
}
```

作为`record`的`Point`仍然可以添加静态方法。一种常用的静态方法是`of()`方法，用来创建`Point`：

```java
public record Point(int x, int y) {
    public static Point of() {
        return new Point(0, 0);
    }
    public static Point of(int x, int y) {
        return new Point(x, y);
    }
}
```

这样我们可以写出更简洁的代码：

```java
var z = Point.of();
var p = Point.of(123, 456);
```

### 2.7.3. 小结

从Java 14开始，提供新的`record`关键字，可以非常方便地定义Data Class：

- 使用`record`定义的是不变类；
- 可以编写Compact Constructor对参数进行验证；
- 可以定义静态方法。

## 2.8. BigInteger

### 2.8.1. BigInteger

在Java中，由CPU原生提供的整型最大范围是64位`long`型整数。使用`long`型整数可以直接通过CPU指令进行计算，速度非常快。

如果我们使用的整数范围超过了`long`型怎么办？这个时候，就只能用软件来模拟一个大整数。`java.math.BigInteger`就是用来表示任意大小的整数。`BigInteger`内部用一个`int[]`数组来模拟一个非常大的整数：

```java
BigInteger bi = new BigInteger("1234567890");
System.out.println(bi.pow(5)); // 2867971860299718107233761438093672048294900000
```

对`BigInteger`做运算的时候，只能使用实例方法，例如，加法运算：

```java
BigInteger i1 = new BigInteger("1234567890");
BigInteger i2 = new BigInteger("12345678901234567890");
BigInteger sum = i1.add(i2); // 12345678902469135780
```

和`long`型整数运算比，`BigInteger`不会有范围限制，但缺点是速度比较慢。

也可以把`BigInteger`转换成`long`型：

```java
BigInteger i = new BigInteger("123456789000");
System.out.println(i.longValue()); // 123456789000
System.out.println(i.multiply(i).longValueExact()); // java.lang.ArithmeticException: BigInteger out of long range
```

使用`longValueExact()`方法时，如果超出了`long`型的范围，会抛出`ArithmeticException`。

`BigInteger`和`Integer`、`Long`一样，也是不可变类，并且也继承自`Number`类。因为`Number`定义了转换为基本类型的几个方法：

- 转换为`byte`：`byteValue()`
- 转换为`short`：`shortValue()`
- 转换为`int`：`intValue()`
- 转换为`long`：`longValue()`
- 转换为`float`：`floatValue()`
- 转换为`double`：`doubleValue()`

因此，通过上述方法，可以把`BigInteger`转换成基本类型。

如果`BigInteger`表示的范围超过了基本类型的范围，转换时将丢失高位信息，即结果不一定是准确的。

如果需要准确地转换成基本类型，可以使用`intValueExact()`、`longValueExact()`等方法，在转换时如果超出范围，将直接抛出`ArithmeticException`异常。

如果`BigInteger`的值超过了`float`的最大范围，返回 Infinity 。

### 2.8.2. 小结

- `BigInteger`用于表示任意大小的整数；
- `BigInteger`是不变类，并且继承自`Number`；

- 将`BigInteger`转换成基本类型时可使用`longValueExact()`等方法保证结果准确。

## 2.9. BigDecimal

和`BigInteger`类似，`BigDecimal`可以表示一个任意大小且精度完全准确的浮点数。

`BigDecimal`用`scale()`表示小数位数。

通过`BigDecimal`的`stripTrailingZeros()`方法，可以将一个`BigDecimal`格式化为一个相等的，但去掉了末尾0的`BigDecimal`：

```java
BigDecimal d1 = new BigDecimal("123.4500");
BigDecimal d2 = d1.stripTrailingZeros();
System.out.println(d1.scale()); // 4
System.out.println(d2.scale()); // 2,因为去掉了00

BigDecimal d3 = new BigDecimal("1234500");
BigDecimal d4 = d3.stripTrailingZeros();
System.out.println(d3.scale()); // 0
System.out.println(d4.scale()); // -2
```

如果一个`BigDecimal`的`scale()`返回负数，例如，`-2`，表示这个数是个整数，并且末尾有2个0。

可以对一个`BigDecimal`设置它的`scale`，如果精度比原始值低，那么按照指定的方法进行四舍五入或者直接截断。

对`BigDecimal`做加、减、乘时，精度不会丢失，但是做除法时，存在无法除尽的情况，这时，就必须指定精度以及如何进行截断。

### 2.9.1. 比较BigDecimal

在比较两个`BigDecimal`的值是否相等时，要特别注意，使用`equals()`方法不但要求两个`BigDecimal`的值相等，还要求它们的`scale()`相等：

```java
BigDecimal d1 = new BigDecimal("123.456");
BigDecimal d2 = new BigDecimal("123.45600");
System.out.println(d1.equals(d2)); // false,因为scale不同
System.out.println(d1.equals(d2.stripTrailingZeros())); // true,因为d2去除尾部0后scale变为2
System.out.println(d1.compareTo(d2)); // 0
```

必须使用`compareTo()`方法来比较，它根据两个值的大小分别返回负数、正数和`0`，分别表示小于、大于和等于。

总是使用compareTo()比较两个BigDecimal的值，不要使用equals()！

如果查看`BigDecimal`的源码，可以发现，实际上一个`BigDecimal`是通过一个`BigInteger`和一个`scale`来表示的，即`BigInteger`表示一个完整的整数，而`scale`表示小数位数：

```java
public class BigDecimal extends Number implements Comparable<BigDecimal> {
    private final BigInteger intVal;
    private final int scale;
}
```

`BigDecimal`也是从`Number`继承的，也是不可变对象。

### 2.9.2. 小结

- `BigDecimal`用于表示精确的小数，常用于财务计算；
- 比较`BigDecimal`的值是否相等，必须使用`compareTo()`而不能使用`equals()`。


## 2.10. 常用工具类

### 2.10.1. Math

顾名思义，`Math`类就是用来进行数学计算的，它提供了大量的静态方法来便于我们实现数学计算：

求绝对值：

```java
Math.abs(-100); // 100
Math.abs(-7.8); // 7.8
```

取最大或最小值：

```java
Math.max(100, 99); // 100
Math.min(1.2, 2.3); // 1.2
```

计算xy次方：

```java
Math.pow(2, 10); // 2的10次方=1024
```

计算√x：

```java
Math.sqrt(2); // 1.414...
```

计算e的x次方：

```java
Math.exp(2); // 7.389...
```

计算以e为底的对数：

```java
Math.log(4); // 1.386...
```

计算以10为底的对数：

```java
Math.log10(100); // 2
```

三角函数：

```java
Math.sin(3.14); // 0.00159...
Math.cos(3.14); // -0.9999...
Math.tan(3.14); // -0.0015...
Math.asin(1.0); // 1.57079...
Math.acos(1.0); // 0.0
```

Math还提供了几个数学常量：

```java
double pi = Math.PI; // 3.14159...
double e = Math.E; // 2.7182818...
Math.sin(Math.PI / 6); // sin(π/6) = 0.5
```

生成一个随机数x，x的范围是`0 <= x < 1`：

```java
Math.random(); // 0.53907... 每次都不一样
```

有些童鞋可能注意到Java标准库还提供了一个`StrictMath`，它提供了和`Math`几乎一模一样的方法。这两个类的区别在于，由于浮点数计算存在误差，不同的平台（例如x86和ARM）计算的结果可能不一致（指误差不同），因此，`StrictMath`保证所有平台计算结果都是完全相同的，而`Math`会尽量针对平台优化计算速度，所以，绝大多数情况下，使用`Math`就足够了。

### 2.10.2. Random

`Random`用来创建伪随机数。所谓伪随机数，是指只要给定一个初始的种子，产生的随机数序列是完全一样的。

要生成一个随机数，可以使用`nextInt()`、`nextLong()`、`nextFloat()`、`nextDouble()`：

```java
Random r = new Random();
r.nextInt(); // 2071575453,每次都不一样
r.nextInt(10); // 5,生成一个[0,10)之间的int
r.nextLong(); // 8811649292570369305,每次都不一样
r.nextFloat(); // 0.54335...生成一个[0,1)之间的float
r.nextDouble(); // 0.3716...生成一个[0,1)之间的double
```

有童鞋问，每次运行程序，生成的随机数都是不同的，没看出**伪随机数**的特性来。

这是因为我们创建`Random`实例时，如果不给定种子，就使用系统当前时间戳作为种子，因此每次运行时，种子不同，得到的伪随机数序列就不同。

如果我们在创建`Random`实例时指定一个种子，就会得到完全确定的随机数序列。

我们使用的`Math.random()`实际上内部调用了`Random`类，所以它也是伪随机数，只是我们无法指定种子。

### 2.10.3. SecureRandom

有伪随机数，就有真随机数。实际上真正的真随机数只能通过量子力学原理来获取，而我们想要的是一个不可预测的安全的随机数，`SecureRandom`就是用来创建安全的随机数的：

```java
SecureRandom sr = new SecureRandom();
System.out.println(sr.nextInt(100));
```

`SecureRandom`无法指定种子，它使用RNG（random number generator）算法。JDK的`SecureRandom`实际上有多种不同的底层实现，有的使用安全随机种子加上伪随机数算法来产生安全的随机数，有的使用真正的随机数生成器。实际使用的时候，可以优先获取高强度的安全随机数生成器，如果没有提供，再使用普通等级的安全随机数生成器：

```java
import java.util.Arrays;
import java.security.SecureRandom;
import java.security.NoSuchAlgorithmException;

public class Main {
    public static void main(String[] args) {
        SecureRandom sr = null;
        try {
            sr = SecureRandom.getInstanceStrong(); // 获取高强度安全随机数生成器
        } catch (NoSuchAlgorithmException e) {
            sr = new SecureRandom(); // 获取普通的安全随机数生成器
        }
        byte[] buffer = new byte[16];
        sr.nextBytes(buffer); // 用安全随机数填充buffer
        System.out.println(Arrays.toString(buffer));
    }
}

```

`SecureRandom`的安全性是通过操作系统提供的安全的随机种子来生成随机数。这个种子是通过CPU的热噪声、读写磁盘的字节、网络流量等各种随机事件产生的“熵”。

在密码学中，安全的随机数非常重要。如果使用不安全的伪随机数，所有加密体系都将被攻破。因此，时刻牢记必须使用`SecureRandom`来产生安全的随机数。

需要使用安全随机数的时候，必须使用SecureRandom，绝不能使用Random！

### 2.10.4. 小结

Java提供的常用工具类有：

- Math：数学计算
- Random：生成伪随机数
- SecureRandom：生成安全的随机数

# 3. 面试题

## 3.1. Overload 和Override 的区别？Overloaded的方法是否可以改变返回值的类型?(需要看第二遍)

**重载Overload**表示同一个类中可以有多个名称相同的方法，但这些方法的参数列表各不相同（即参数个数或类型不同）。

**重写Override**表示子类中的方法可以与父类中的某个方法的名称和参数完全相同，通过子类创建的实例对象调用这个方法时，将调用子类中的定义方法，这相当于把父类中定义的那个完全相同的方法给覆盖了，这也是面向对象编程的多态性的一种表现。子类覆盖父类的方法时，只能比父类抛出更少的异常，或者是抛出父类抛出的异常的子异常，因为子类可以解决父类的一些问题，不能比父类有更多的问题。子类方法的访问权限只能比父类的更大，不能更小。如果父类的方法是 private 类型，那么，子类则不存在覆盖的限制，相当于子类中增加了一个全新的方法。

 

至于 Overloaded 的方法是否可以改变返回值的类型这个问题，要看你倒底想问什么呢？

这个题目很模糊。如果几个 Overloaded 的方法的参数列表不一样，它们的返回者类型当然也可以不一样。但我估计你想问的问题是：如果两个方法的参数列表完全一样，是否可以让它们的返回值不同来实现重载 Overload。这是不行的，我们可以用反证法来说明这个问题，因为我们有时候调用一个方法时也可以不定义返回结果变量，即不要关心其返回结果，例如，我们调用 map.remove(key) 方法时，虽然 remove 方法有返回值，但是我们通常都不会定义接收返回结果的变量，这时候假设**该类中有两个名称和参数列表完全相同的方法，仅仅是返回类型不同，Java 就无法确定编程者倒底是想调用哪个方法了，因为它无法通过返回结果类型来判断**。

## 3.2. Java 中，什么是构造函数？什么是构造函数重载？什么是复制构造函数？

当新对象被创建的时候，构造函数会被调用。每一个类都有构造函数。在程序员没有给类提供构造函数的情况下，Java 编译器会为这个类创建一个默认的构造函数。

Java 中构造函数重载和方法重载很相似。可以为一个类创建多个构造函数。每一个构造函数必须有它自己唯一的参数列表。

Java 不支持像 C++ 中那样的复制构造函数，这个不同点是因为如果你不自己写构造函数的情况下，Java不会创建默认的复制构造函数。

## 3.3. 构造器 Constructor 是否可被 Override?

构造器 Constructor 不能被继承，因此不能重写 Override，但可以被重载 Overload。

## 3.4. Java 支持多继承么？

不支持，Java 不支持多继承。每个类都只能继承一个类，但是可以实现多个接口。

## 3.5. 接口和抽象类的区别是什么？(需要看第二遍)

抽象类是用来捕捉子类的通用特性的。接口是抽象方法的集合。

从设计层面来说，抽象类是对类的抽象，是一种模板设计，接口是行为的抽象，是一种行为的规范。

**相同点**：

1. 接口和抽象类都不能实例化
2. 都位于继承的顶端，用于被其他实现或继承
3. 都包含抽象方法，其子类都必须覆写这些抽象方法

**不同点**：

| 参数             | 抽象类                                                       | 接口                                                         |
| ---------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 声明             | 抽象类使用abstract关键字声明                                 | 接口使用interface关键字声明                                  |
| 实现             | 子类使用extends关键字来继承抽象类。类可以不实现抽象类声明的所有方法 | 子类使用implements关键字来实现接口。它需要提供接口中所有声明的方法的实现 |
| 构造器           | 抽象类可以有构造器                                           | 接口不能有构造器                                             |
| 访问修饰符       | 抽象类中的方法可以是任意访问修饰符，可以是 private，protected 或者是 public | 接口方法默认修饰符是public。并且不允许定义为 private 或者 protected |
| 多继承，实现数量 | 一个类最多只能继承一个抽象类                                 | 一个类可以实现多个接口                                       |
| 字段声明         | 抽象类的字段声明可以是任意的                                 | 接口的字段默认都是 static 和 final 的                        |
| main 方法        | 抽象类可以有 main 方法，并且我们能运行它                     | 接口不能有 main 方法                                         |
| final变量        | 抽象类可以包含非final的变量                                  | Java接口中声明的变量默认都是final的                          |
| 方法是否抽象     | 抽象类则可以同时包含抽象和非抽象的方法                       | 接口中所有的方法隐含的都是抽象的                             |

**备注**：Java8中接口中引入默认方法和静态方法，以此来减少抽象类和接口之间的差异。

现在，我们可以为接口提供默认实现的方法了，并且不用强制子类来实现它。

接口和抽象类各有优缺点，在接口和抽象类的选择上，必须遵守下面的原则：

- 行为模型应该总是通过接口而不是抽象类定义，所以通常是优先选用接口，尽量少用抽象类。
- 选择抽象类的时候通常是如下情况：需要定义子类的行为，又要为子类提供通用的功能。

## 3.6. Java 接口的修饰符可以为?

A. private

B. protected

C. final

D. abstract

答案：CD



通俗的讲，你认为是要变化的东西，就放在你自己的实现中，不能放在接口中去，接口只是对一类事物的属性和行为更高层次的抽象。对修改关闭，对扩展（不同的实现 implements）开放，接口是对开闭原则的一种体现。

所以：

接口的方法默认是 public abstract；

接口中不可以定义变量即只能定义常量(加上final修饰就会变成常量)。

所以接口的属性默认是 public static final常量，**且必须赋初值**。

**注意：final 和 abstract不能同时出现**

## 3.7. 下面是 People 和 Child 类的定义和构造方法，每个构造方法都输出编号。在执行 new Child("mike") 的时候都有哪些构造方法被顺序调用？请选择输出结果

```java
class People {
  
    String name;
  
    public People() {
    		System.out.print(1);
    }

    public People(String name) {
        System.out.print(2);
        this.name = name;
    }
}

class Child extends People {

    People father;

    public Child(String name) {
        System.out.print(3);
        this.name = name;
        father = new People(name + ":F");
    }

    public Child() {
    		System.out.print(4);
    }

}
```

A. 312

B. 32

C. 432

D. 132

答案：D

解析：考察的又是父类与子类的构造函数调用次序。

 

**第一个规则**：子类的构造过程中，必须调用其父类的构造方法。一个类，如果我们不写构造方法，那么编译器会帮我们加上一个默认的构造方法（就是没有参数的构造方法），但是如果你自己写了构造方法，那么编译器就不会给你添加了，所以有时候当你 new一个子类对象的时候，肯定调用了子类的构造方法，但是如果在子类构造方法中我们并没有显示的调用基类的构造方法，如：super(); 这样就会调用父类没有参数的构造方法。

**第二个规则**：如果子类的构造方法中既没有显示的调用基类构造方法，而基类中又没有无参的构造方法，则编译出错，所以，通常我们需要显示的：super(参数列表)，来调用父类有参数的构造函数，此时无参的构造函数就不会被调用。

**总之，一句话**：子类没有显示调用父类构造函数，不管子类构造函数是否带参数都默认调用父类无参的构造函数，若父类没有则编译出错。



**父类静态块初始化---->子类静态块初始化---->父类非静态块初始化---->父类构造方法---->子类非静态块初始化---->子类构造方法。(先静后动，先父后子)**(需要看第二遍)

## 3.8. 两个对象值相同(x.equals(y) == true)，但却可有不同的 hash code，这句话对不对？

答：**不对**，如果两个对象 x 和 y 满足 x.equals(y) == true，它们的哈希码（hash code）应当相同。Java 对于eqauls 方法和 hashCode 方法是这样规定的：

1. 如果两个对象相同（equals 方法返回 true ），那么它们的 hashCode 值一定要相同；
2. 如果两个对象的 hashCode 相同，它们并不一定相同。当然，你未必要按照要求去做，但是如果你违背了上述原则就会发现在使用容器时，相同的对象可以出现在 Set 集合中，同时增加新元素的效率会大大下降（对于使用哈希存储的系统，如果哈希码频繁的冲突将会造成存取性能急剧下降）。

**补充**：关于 equals 和 hashCode 方法中是这样介绍 equals 方法的：首先 equals 方法必须满足

1. 自反性（x.equals(x) 必须返回true）
2. 对称性（x.equals(y) 返回true时，y.equals(x) 也必须返回 true）
3. 传递性（x.equals(y)和y.equals(z)都返回 true 时，x.equals(z)也必须返回true）
4. 一致性（当x和y引用的对象信息没有被修改时，多次调用x.equals(y)应该得到同样的返回值）
5. 非空性（对于任何非 null 值的引用 x，x.equals(null) 必须返回 false）。



实现高质量的equals方法的诀窍包括：

1. 使用 == 操作符检查“参数是否为这个对象的引用”；
2. 使用 instanceof 操作符检查“参数是否为正确的类型”；
3. 对于类中的关键属性，检查参数传入对象的属性是否与之相匹配；
4. 编写完 equals 方法后，问自己它是否满足对称性、传递性、一致性；
5. 重写 equals 时总是要重写 hashCode；
6. 不要将 equals 方法参数中的 Object 对象替换为其他的类型，在重写时不要忘掉 @Override 注解。

## 3.9. 接口是否可继承（extends）接口? 抽象类是否可实现（implements）接口? 抽象类是否可继承具体类（concrete class）?(需要看第二遍)

接口可以继承接口。

抽象类可以实现(implements)接口。

抽象类可继承具体类，但前提是具体类必须有明确的构造函数。

## 3.10. Class.forName（String className）这个方法的作用

答：通过类的全名获得该类的类对象

## 3.11. 什么是AOP和OOP，IOC和DI有什么不同?

- OOP（Object Oriented Programming，OOP，面向对象编程）是一种计算机编程架构。
- AOP是OOP 的延续，是Aspect Oriented Programming的缩写，意思是面向切面编程。将通用需求功能从不相关类之中分离出来；同时，能够使得很多类共享一个行为，一旦行为发生变化，不必修改很多类，只要修改这个行为就可以。AOP 就是这种实现分散关注的编程方法，它将“关注”封装在“切面”中
- 控制反转IOC(Inversion of Control)，控制指的就是程序相关类之间的依赖关系。传统观念设计中,通常由调用者来创建被调用者的实例，在Spring里，创建被调用者的工作不再由调用者来完成，而是由Spring容器完成，依赖关系被反转了，称为控制反转，目的是为了获得更好的扩展性和良好的可维护性。
- 依赖注入(Dependency injection)创建被调用者的工作由Spring容器完成，然后注入调用者，因此也称依赖注入。控制反转和依赖注入是同一个概念

## 3.12. 判断下列语句是否正确，如果有错误，请指出错误所在？

```java
interface A{
		int add(final A a);
}

class B implements A {
    long add(final A a){
        return this.hashCode() + a.hashCode();
    }
}
```

答：返回值不是 long 类型

```java
public native int hashCode();
```

native修饰方法，native修饰的方法简单来说就是：**一个Java方法调用了一个非Java代码的接口**。

定义navtive方法时，并不提供实现体，因为其实现体是用非Java语言在外面实现的。native可以和任何修饰符连用，abstract除外。因为native暗示这个方法时有实现体的，而abstract却显式指明了这个方法没有实现体。但是abstract方法和Interface方法不能用native来修饰。

## 3.13. static关键字是什么意思？Java 中是否可以覆盖(override) 一个 private或者是 static 的方法？

static关键字表明一个成员变量或者是成员方法可以在没有所属的类的实例变量的情况下被访问。

Java中static方法不能被覆盖，因为方法覆盖是基于运行时动态绑定的，而static方法是编译时静态绑定的。static方法跟类的任何实例都不相关，所以概念上不适用。

**静态方法的覆盖是方法隐藏**--方法隐藏只有一种形式，就是父类和子类存在相同的静态方法

## 3.14. 访问修饰符 public, private, protected, 以及不写（默认）时的区别？(需要看第二遍)

| **修饰符** | **当前类** | **同包** | **子类** | **其它包** |
| ---------- | ---------- | -------- | -------- | ---------- |
| private    | √          | ×        | ×        | ×          |
| default    | √          | √        | ×        | ×          |
| protected  | √          | √        | √        | ×          |
| public     | √          | √        | √        | √          |

## 3.15. volatile关键字是否能保证线程安全？(需要看第二遍)

答案：不能

解析：volatile 关键字用在多线程同步中，可保证读取的可见性，JVM只是保证从主内存加载到线程工作内存的值是最新的读取值，而非 cache 中。但多个线程对volatile的写操作，无法保证线程安全。例如假如线程 1，线程 2 在进行 read,load 操作中，发现主内存中 count 的值都是 5，那么都会加载这个最新的值，在线程 1 堆 count 进行修改之后，会write到主内存中，主内存中的count变量就会变为6；线程2由于已经进行 read,load 操作，在进行运算之后，也会更新主内存 count 的变量值为6；导致两个线程即使用了volatile关键字修改之后，还是会存在并发的情况。（修饰变量，变量自增时候会出现线程安全问题）

 

**需要加锁保证线程同步**（原子性）



**Volatile语义1**

简而言之就是用关键字修饰的就是修改后及时写回主存，对其他线程可见可理解为其他线程探嗅到自己缓存中的变量是过期的（不同线程在同一核心道理相同）。

**Volatile语义2**

Volatile第二层意思就是禁止指令重排序。

Jvm为了优化性能会采用指令重排序

Int a=1;

Int b=2;

Jvm的执行顺序可能是先b=2；然后再a=1，因为当代码执行到a=1时a对象可能被加锁了，这时如果等待锁释放显然浪费了时间，所以先执行b=2，但是用volatile修饰的关键字在操作的时候必须是在指定位置的，即如果这个变量的操作在第五行那会保证前四行都执行完才会执行第五行。

## 3.16. Lock和Synchronized的区别(需要看第二遍)

**相同点**
synchronized和lock都是锁的意思，都是为了线程安全性，应用合理性和运行效率的。

可以简单理解lock比前置更加优秀和合理，是前者的优化版。



**不同点**：

1. 首先synchronized是java内置关键字，在jvm层面，Lock是个java类；
2. synchronized无法判断是否获取锁的状态，Lock可以判断是否获取到锁；
3. **拥有锁的状态**：synchronized会自动释放锁（**线程执行完同步代码会释放锁** ；**线程执行过程中发生异常会释放锁**），Lock需在finally中手工释放锁（unlock()方法释放锁），否则容易造成线程死锁；
4. **抢夺锁**：用synchronized关键字的两个线程1和线程2，如果当前线程1获得锁，线程2线程等待。如果线程1**阻塞**，线程2则会一直等待下去，而Lock锁就不一定会等待下去，如果尝试获取不到锁，线程可以不用一直等待就结束了；
5. synchronized的锁可重入、不可中断、非公平，而Lock锁可重入、可中断、可公平（两者皆可）
6. Lock锁适合大量同步的代码的同步问题，synchronized锁适合代码少量的同步问题。

## 3.17. synchronized锁的底层实现(需要看第二遍)

在理解锁实现原理之前先了解一下Java的对象头和Monitor，在JVM中，**对象实例**是分成三部分存在的：**对象头、实例数据、对其填充**。

实例数据和对其填充与synchronized无关，这里简单说一下（我也是阅读《深入理解Java虚拟机》学到的，读者可仔细阅读该书相关章节学习）。**实例数据**存放类的属性数据信息，包括父类的属性信息，如果是数组的实例部分还包括数组的长度，这部分内存**按4字节对齐**；**对其填充**不是必须部分，由于虚拟机要求对象起始地址必须是**8字节的整数倍**，**对齐填充仅仅是为了使字节对齐**。

对象头是我们需要关注的重点，它是synchronized实现锁的基础，因为synchronized申请锁、上锁、释放锁都与对象头有关。对象头主要结构是由`Mark Word` 和 `Class Metadata Address`组成，**其中`Mark Word`存储对象的hashCode、锁信息或分代年龄或GC标志等信息**，**Class Metadata Address是类型指针指向对象的类元数据，JVM通过该指针确定该对象是哪个类的实例**。

锁也分不同状态，JDK6之前只有两个状态：无锁、有锁（重量级锁），而在JDK6之后对synchronized进行了优化，新增了两种状态，总共就是四个状态：**无锁状态、偏向锁、轻量级锁、重量级锁**，其中无锁就是一种状态了。锁的类型和状态在对象头`Mark Word`中都有记录，在申请锁、锁升级等过程中JVM都需要读取对象的`Mark Word`数据。

每一个锁都对应一个**monitor**对象，在HotSpot虚拟机中它是由ObjectMonitor实现的（C++实现）。每个对象都存在着一个monitor与之关联，对象与其monitor之间的关系有存在多种实现方式，如monitor可以与对象一起创建销毁或当线程试图获取对象锁时自动生成，但当一个monitor被某个线程持有后，它便处于锁定状态。

## 3.18. Java 有没有 goto?

答：goto 是 Java 中的保留字，在目前版本的 Java 中没有使用。

根据James Gosling（Java 之父）编写的《The Java Programming Language》一书的附录中给出了一个 Java 关键字列表，其中有 goto 和 const，但是这两个是目前无法使用的关键字，因此有些地方将其称之为保留字，其实保留字这个词应该有更广泛的意义，因为熟悉 C 语言的程序员都知道，在系统类库中使用过的有特殊意义的单词或单词的组合都被视为保留字。

## 3.19. final, finally, finalize 的区别(需要看第二遍)

**final**：修饰符（关键字）有三种用法：

1. 修饰类：表示该类不能被继承；
2. 修饰方法：表示方法不能被重写；
3. 修饰变量：表示变量只能一次赋值以后值不能被修改（常量）。（基本类型和引用类型有区别）

**finally**：通常放在 try…catch 的后面构造总是执行代码块，这就意味着程序无论正常执行还是发生异常，这里的代码只要 JVM 不关闭都能执行，可以将释放外部资源的代码写在 finally 块中。

**finalize**：Object类中定义的方法，Java中允许使用finalize()方法在垃圾收集器将对象从内存中清除出去之前做必要的清理工作。这个方法是由垃圾收集器**在销毁对象时调用**的，通过重写finalize()方法可以整理系统资源或者执行其他清理工作。



## 3.20. 用最有效率的方法算出2乘以8等於几?

2 << 3，因为将一个数左移n位，就相当于乘以了2的n次方，那么，一个数乘以8只要将其左移3位即可，而位运算cpu直接支持的，效率最高，所以，2乘以8等於几的最效率的方法是2 << 3

## 3.21. 存在使 i + 1 < i的数吗?

答案：存在

解析：如果 i 为 int 型，那么当 i 为 int 能表示的最大整数时， i+1 就溢出变成负数了，此时不就 <i 了吗。

 

**扩展**：存在使 i > j || i <= j 不成立的数吗?

答案：存在

解析：比如Double.NaN或Float.NaN

```java
public class  Test
{
    public static void main(String[] args) 
    {

        if (Double.NaN == Double.NaN)
        {
            System.out.println("=");
        }
        else
        		System.out.println("this");// 输出this
    }
}
```



```java
Integer i = new Integer(0);
Integer j = new Integer(0);

System.out.println(i <= j && j <= i && i != j);// true
```

## 3.22. 0.6332的数据类型是（）

A. float

B. double

C. Float

D. Double

答案：B

解析：默认为 double 型，如果为 float 型需要加上f显示说明，即0.6332f

## 3.23. System.out.println("5" + 2);的输出结果应该是（）

A. 52

B. 7

C. 2

D. 5

答案：A

解析：没啥好说的，Java会自动将2转换为字符串。

## 3.24. float f=3.4;是否正确?

答:不正确。

3.4是双精度数，将双精度型（double）赋值给浮点型（float）属于下转型（down-casting，也称为窄化）会造成精度损失，因此需要强制类型转换float f =(float)3.4; 或者写成 float f =3.4F;。

答：char 类型可以存储一个中文汉字，因为 Java中使用的编码是 Unicode（不选择任何特定的编码，直接使用字符在字符集中的编号，这是统一的唯一方法），一个 char 类型占 2 个字节（16bit），所以放一个中文是没问题的。

## 3.25. char型变量中能不能存贮一个中文汉字?为什么?

答：char 类型可以存储一个中文汉字，因为 Java中使用的编码是 Unicode（不选择任何特定的编码，直接使用字符在字符集中的编号，这是统一的唯一方法），一个 char 类型占 2 个字节（16bit），所以放一个中文是没问题的。

## 3.26. String 是最基本的数据类型吗?

答：不是。

Java 中的基本数据类型只有8个：byte、short、int、long、float、double、char、boolean；除了基本类型（primitive type）和枚举类型（enumeration type），剩下的都是引用类型（reference type）。

## 3.27. 数组有没有 length() 方法? String 有没有 length() 方法？

答：数组没有length()方法，有length的属性。String有length()方法。JavaScript中，获得字符串的长度是通过 length属性得到的，这一点容易和 Java 混淆。

## 3.28. 是否可以继承String类

答：String 类是 final 类，不可以被继承。

补充：继承 String 本身就是一个错误的行为，对 String 类型最好的重用方式是关联（HAS-A）而不是继承（IS-A）

## 3.29. String 和StringBuilder、StringBuffer 的区别?

答：Java 平台提供了两种类型的字符串：String 和StringBuffer / StringBuilder，它们可以储存和操作字符串。

其中 String 是只读字符串，也就意味着 String 引用的字符串内容是不能被改变的。

而 StringBuffer 和 StringBuilder 类表示的字符串对象可以直接进行修改。StringBuilder 是 JDK 1.5 中引入的，它和 StringBuffer 的方法完全相同，区别在于它是在单线程环境下使用的，因为它的所有方面都没有被 synchronized 修饰，因此它的效率也比 StringBuffer 略高。

 

有一个面试题问：有没有哪种情况用 + 做字符串连接比调用 StringBuffer / StringBuilder 对象的 append 方法性能更好？如果连接后得到的字符串在静态存储区中是早已存在的，那么用+做字符串连接是优于 StringBuffer /StringBuilder 的 append 方法的。

## 3.30. String s = new String(“xyz”);创建了几个字符串对象？

答：两个对象，一个是静态存储区的"xyz",一个是用new创建在堆上的对象。

## 3.31. 将字符 “12345” 转换成 long 型

解答：

```java
String s=”12345”;

long num=Long.valueOf(s).longValue();
```

## 3.32. 为了显示 myStr = 23 这样的结果，写出在控制台输入的命令

```java
public class MyClass {

    public static void main(String args[]) {
        String s1 = args[0];
        String s2 = args[1];
        String myStr = args[2]; 
        System.out.printin(“myStr =” + s2 + myStr);
    }

}
```

答：java MyClass 1 2 3 4

**数字不一定是1 2 3 4，个数也不一定是4个，只要第二个和第三了字符分别是2和3就行**

## 3.33. String s = "Hello";s = s + " world!”;这两行代码执行后，原始的String对象中的内容到底变了没有？为什么不可变？不可变有什么好处？

没有。因为String被设计成不可变(immutable)类，所以它的所有对象都是不可变对象。在这段代码中，s原先指向一个String对象，内容是"Hello"，然后我们对s进行了+操作，那么s所指向的那个对象是否发生了改变呢？答案是没有。这时，s不指向原来那个对象了，而指向了另一个String对象，内容为"Hello world!"，原来那个对象还存在于内存之中，只是s这个引用变量不再指向它了。

 

**String的实体存在于方法区的常量池**

 

**为什么String对象是不可变的**？

要理解String的不可变性，首先看一下String类中都有哪些成员变量。在JDK1.6中，String的成员变量有以下几个：

```java
public final class String implements java.io.Serializable, Comparable<String>, CharSequence
{
  /** The value is used for character storage. */
  private final char value[];

  /** The offset is the first index of the storage that is used. */
  private final int offset;

  /** The count is the number of characters in the String. */
  private final int count;

  /** Cache the hash code for the string */
	private int hash; // Default to 0
```

在JDK1.7中，String类做了一些改动，主要是改变了substring方法执行时的行为，这和本文的主题不相关。JDK1.7中String类的主要成员变量就剩下了两个：

```java
public final class String implements java.io.Serializable, Comparable<String>, CharSequence {

  /** The value is used for character storage. */
  private final char value[];
  
  /** Cache the hash code for the string */
	private int hash; // Default to 0
```

 

由以上的代码可以看出，在Java中String类其实就是对字符数组的封装。JDK6中，value是String封装的数组，offset是String在这个value数组中的起始位置，count是String所占的字符的个数。在JDK7中，只有一个value变量，也就是value中的所有字符都是属于String这个对象的。这个改变不影响本文的讨论。除此之外还有一个hash成员变量，是该String对象的哈希值的缓存，这个成员变量也和本文的讨论无关。在Java中，数组也是对象。所以value也只是一个引用，它指向一个真正的数组对象。其实执行了String s = “ABCabc”; 这句代码之后，真正的内存布局应该是这样的：

![img](./images/OOP-1.jpg)

value，offset和count这三个变量都是private的，并且没有提供setValue，setOffset和setCount等公共方法来修改这些值，所以在String类的外部无法修改String。也就是说一旦初始化就不能修改，并且在String类的外部不能访问这三个成员。此外，value，offset和count这三个变量都是final的， 也就是说在String类内部，一旦这三个值初始化了， 也不能被改变。所以可以认为String对象是不可变的了

**相对于可变对象，不可变对象有很多优势**。

1. 不可变对象可以提高String Pool的效率和安全性。如果你知道一个对象是不可变的，那么需要拷贝这个对象的内容时，就不用复制它的本身而只是复制它的地址，复制地址（通常一个指针的大小）需要很小的内存效率也很高。对于同时引用这个“ABC”的其他变量也不会造成影响。

2. 不可变对象对于多线程是安全的，因为在多线程同时进行的情况下，一个可变对象的值很可能被其他进程改变，这样会造成不可预期的结果，而使用不可变对象就可以避免这种情况。

## 3.34. 如何把一段逗号分割的字符串转换成一个数组?(需要看第二遍)

如果不查 jdk api，我很难写出来！我可以说说我的思路：

- 用正则表达式，代码大概为： String [] result = orgStr.split(“,”);
- 用 StringTokenizer ,代码为：

```java
StringTokenizer tokener = StringTokenizer(orgStr,”,”);

String [] result = new String[tokener.countTokens()];
int i = 0;
while (tokener.hasNext()) {
		result[i++] = toker.nextToken();
}
```

## 3.35. 下面这条语句一共创建了多少个对象：String s=“a”+”b”+”c”+”d”;

答：对于如下代码：

```java
String s1 = "a";
String s2 = s1 + "b";
String s3 = "a" + "b";
System.out.println(s2 == "ab");
System.out.println(s3 == "ab");
```

第一条语句打印的结果为 false ，第二条语句打印的结果为 true，这说明 Javac 编译可以对字符串常量直接相加的表达式进行优化，不必要等到运行期去进行加法运算处理，而是在编译时去掉其中的加号，直接将其编译成一个这些常量相连的结果。

题目中的第一行代码被编译器在编译时优化后，相当于直接定义了一个”abcd”的字符串，所以，**上面的代码应该只创建了一个String对象**。写如下两行代码，

```java
String s ="a" + "b" + "c" + "d";
System.out.println(s== "abcd");
```

最终打印的结果应该为 true。

## 3.36. String和StringBuffer的区别?

答：

JAVA 平台提供了两个类：String 和 StringBuffer，它们可以储存和操作字符串，即包含多个字符的字符数据。

这个 String 类提供了数值不可改变的字符串。

而这个 StringBuffer 类提供的字符串进行修改。

当你知道字符数据要改变 的时候你就可以使用 StringBuffer。典型地，你可以使用 StringBuffer来动态构造字符数据。

## 3.37. String, StringBuffer StringBuilder的区别

答：

- String 的长度是不可变的；
- StringBuffer的长度是可变的，如果你对字符串中的内容经常进行操作，特别是内容要修改时，那么使用 StringBuffer，如果最后需要String，那么使用 StringBuffer的toString()方法；线程安全；
- StringBuilder是从JDK 5开始，为StringBuffer该类补充了一个单个线程使用的等价类；

通常应该优先使用 StringBuilder类，因为它支持所有相同的操作，但由于它不执行同步，所以速度更快。

## 3.38. 面向对象五大基本原则是什么（可选）

1. 单一职责原则SRP(Single Responsibility Principle)
   类的功能要单一，不能包罗万象，跟杂货铺似的。
2. 开放封闭原则OCP(Open－Close Principle)
   一个模块对于拓展是开放的，对于修改是封闭的。
3. 里式替换原则LSP(the Liskov Substitution Principle LSP)
   子类可以替换父类出现在父类能够出现的任何地方。
4. 依赖倒置原则DIP(the Dependency Inversion Principle DIP)
   高层次的模块不应该依赖于低层次的模块，他们都应该依赖于抽象。抽象不应该依赖于具体实现，具体实现应该依赖于抽象。就是你出国要说你是中国人，而不能说你是哪个村子的。比如说中国人是抽象的，下面有具体的xx省，xx市，xx县。你要依赖的抽象是中国人，而不是你是xx村的。
5. 接口分离原则ISP(the Interface Segregation Principle ISP)
   设计时采用多个与特定客户类有关的接口比采用一个通用的接口要好。就比如一个手机拥有打电话，看视频，玩游戏等功能，把这几个功能拆分成不同的接口，比在一个接口里要好的多。

## 3.39. 普通类和抽象类有哪些区别？

- 普通类不能包含抽象方法，抽象类可以包含抽象方法。
- 抽象类不能直接实例化，普通类可以直接实例化。

## 3.40. 抽象类能使用 final 修饰吗？

不能，定义抽象类就是让其他类继承的，如果定义为 final 该类就不能被继承，这样彼此就会产生矛盾，所以 final 不能修饰抽象类

## 3.41. 成员变量与局部变量的区别有哪些(需要看第二遍)

变量：在程序执行的过程中，在某个范围内其值可以发生改变的量。从本质上讲，变量其实是内存中的一小块区域

成员变量：方法外部，类内部定义的变量

局部变量：类的方法中的变量。

### 3.41.1. 作用域

成员变量：针对整个类有效。

局部变量：只在某个范围内有效。(一般指的就是方法,语句体内)

### 3.41.2. 存储位置

成员变量：随着对象的创建而存在，随着对象的消失而消失，存储在堆内存中。

局部变量：在方法被调用，或者语句被执行的时候存在，存储在栈内存中。当方法调用完，或者语句结束后，就自动释放。

### 3.41.3. 生命周期

成员变量：随着对象的创建而存在，随着对象的消失而消失

局部变量：当方法调用完，或者语句结束后，就自动释放。

### 3.41.4. 初始值

成员变量：有默认初始值。

局部变量：没有默认初始值，使用前必须赋值。

### 3.41.5. 使用原则

在使用变量时需要遵循的原则为：就近原则

首先在局部范围找，有就使用；接着在成员位置找。

## 3.42. 在Java中定义一个不做事且没有参数的构造方法的作用(需要看第二遍)

Java程序在执行子类的构造方法之前，如果没有用super()来调用父类特定的构造方法，则会调用父类中“没有参数的构造方法”。因此，如果父类中只定义了有参数的构造方法，而在子类的构造方法中又没有用super()来调用父类中特定的构造方法，则编译时将发生错误，因为Java程序在父类中找不到没有参数的构造方法可供执行。

**解决办法**是在父类里加上一个不做事且没有参数的构造方法。

## 3.43. 在调用子类构造方法之前会先调用父类没有参数的构造方法，其目的是？(需要看第二遍)

帮助子类做初始化工作。

## 3.44. 一个类的构造方法的作用是什么？若一个类没有声明构造方法，改程序能正确执行吗？为什么？

主要作用是完成对类对象的初始化工作。可以执行。因为一个类即使没有声明构造方法也会有默认的不带参数的构造方法。

## 3.45. 构造方法有哪些特性？

- 名字与类名相同；
- 没有返回值，但不能用void声明构造函数；
- 生成类的对象时自动执行，无需调用。

## 3.46. 静态变量和实例变量区别

- 静态变量： 静态变量由于不属于任何实例对象，属于类的，所以在内存中只会有一份，在类的加载过程中，JVM只为静态变量分配一次内存空间。
- 实例变量： 每次创建对象，都会为每个对象分配成员变量内存空间，实例变量是属于实例对象的，在内存中，创建几次对象，就有几份成员变量。

## 3.47. 静态变量与普通变量区别(需要看第二遍)

static变量也称作静态变量，静态变量和非静态变量的区别是：

- 静态变量被所有的对象所共享，在内存中只有一个副本，它当且仅当在类初次加载时会被初始化。
- 非静态变量是对象所拥有的，在创建对象的时候被初始化，存在多个副本，各个对象拥有的副本互不影响。

还有一点就是**static成员变量的初始化顺序按照定义的顺序进行初始化**。

## 3.48. 静态方法和实例方法有何不同？

静态方法和实例方法的区别主要体现在两个方面：

在外部调用静态方法时，可以使用"类名.方法名"的方式，也可以使用"对象名.方法名"的方式。而实例方法只有后面这种方式。也就是说，调用静态方法可以无需创建对象。

静态方法在访问本类的成员时，只允许访问静态成员（即静态成员变量和静态方法），而不允许访问实例成员变量和实例方法；实例方法则无此限制

## 3.49. 什么是方法的返回值？返回值的作用是什么？

方法的返回值是指我们获取到的某个方法体中的代码执行后产生的结果！（前提是该方法可能产生结果）。返回值的作用:接收出结果，使得它可以用于其他的操作！

## 3.50. 什么是内部类？

在Java中，可以将一个类的定义放在另外一个类的定义内部，这就是内部类。**内部类本身就是类的一个属性**，与其他属性定义方式一致。

## 3.51. 内部类的分类有哪些(需要看第二遍)

内部类可以分为四种：

1. 成员内部类
2. 局部内部类（方法内定义的类）
3. 匿名内部类
4. 静态内部类。

### 3.51.1. 静态内部类

定义在类内部的静态类，就是静态内部类。

```java
public class Outer {

    private static int radius = 1;

    static class StaticInner {
        public void visit() {
            System.out.println("visit outer static  variable:" + radius);
        }
    }
}
```

静态内部类可以访问外部类所有的静态变量，而不可访问外部类的非静态变量；静态内部类的创建方式，new 外部类.静态内部类()，如下：

```java
Outer.StaticInner inner = new Outer.StaticInner();
inner.visit();
```

### 3.51.2. 成员内部类

定义在类内部，成员位置上的非静态类，就是成员内部类。

```java
public class Outer {

    private static  int radius = 1;
    private int count =2;
    
     class Inner {
        public void visit() {
            System.out.println("visit outer static  variable:" + radius);
            System.out.println("visit outer   variable:" + count);
        }
    }
}
```

成员内部类可以访问外部类所有的变量和方法，包括静态和非静态，私有和公有。成员内部类依赖于外部类的实例，它的创建方式外部类实例.new 内部类()，如下：

```java
Outer outer = new Outer();
Outer.Inner inner = outer.new Inner();
inner.visit();
```

### 3.51.3. 局部内部类

定义在方法中的内部类，就是局部内部类。

```java
public class Outer {

    private  int out_a = 1;
    private static int STATIC_b = 2;

    public void testFunctionClass(){
        int inner_c =3;
        class Inner {
            private void fun(){
                System.out.println(out_a);
                System.out.println(STATIC_b);
                System.out.println(inner_c);
            }
        }
        Inner  inner = new Inner();
        inner.fun();
    }
    public static void testStaticFunctionClass(){
        int d =3;
        class Inner {
            private void fun(){
                // System.out.println(out_a); 编译错误，定义在静态方法中的局部类不可以访问外部类的实例变量
                System.out.println(STATIC_b);
                System.out.println(d);
            }
        }
        Inner inner = new Inner();
        inner.fun();
    }
}
```

定义在实例方法中的局部类可以访问外部类的所有变量和方法，定义在静态方法中的局部类只能访问外部类的静态变量和方法。局部内部类的创建方式，在对应方法内，new 内部类()，如下：

```java
 public static void testStaticFunctionClass(){
    class Inner {
    }
    Inner inner = new Inner();
 }
```

### 3.51.4. 匿名内部类

匿名内部类就是没有名字的内部类，日常开发中使用的比较多。

```java
public class Outer {

    private void test(final int i) {
        new Service() {
            public void method() {
                for (int j = 0; j < i; j++) {
                    System.out.println("匿名内部类" );
                }
            }
        }.method();
    }
 }
 //匿名内部类必须继承或实现一个已有的接口 
 interface Service{
    void method();
}
```

除了没有名字，匿名内部类还有以下特点：

- 匿名内部类必须继承一个抽象类或者实现一个接口。
- 匿名内部类不能定义任何静态成员和静态方法。
- 当所在的方法的形参需要被匿名内部类使用时，必须声明为 final。
- 匿名内部类不能是抽象的，它必须要实现继承的类或者实现的接口的所有抽象方法。

匿名内部类创建方式：

```java
new 类/接口{ 
  //匿名内部类实现部分
}
```

## 3.52. 内部类的优点

我们为什么要使用内部类呢？因为它有以下优点：

1. 一个内部类对象可以访问创建它的外部类对象的内容，包括私有数据！
2. 内部类不为同一包的其他类所见，具有很好的封装性；
3. 内部类有效实现了“多重继承”，优化 java 单继承的缺陷。
4. 匿名内部类可以很方便的定义回调。

## 3.53. 内部类有哪些应用场景

1. 一些多算法场合
2. 解决一些非面向对象的语句块。
3. 适当使用内部类，使得代码更加灵活和富有扩展性。
4. 当某个类除了它的外部类，不再被其他的类使用时。

## 3.54. 局部内部类和匿名内部类访问局部变量的时候，为什么变量必须要加上final？

局部内部类和匿名内部类访问局部变量的时候，为什么变量必须要加上final呢？它内部原理是什么呢？

先看这段代码：

```java
public class Outer {

    void outMethod(){
        final int a =10;
        class Inner {
            void innerMethod(){
                System.out.println(a);
            }

        }
    }
}
```

以上例子，为什么要加final呢？是**因为生命周期不一致， 局部变量直接存储在栈中，当方法执行结束后，非final的局部变量就被销毁**。而局部内部类对局部变量的引用依然存在，如果局部内部类要调用局部变量时，就会出错。加了final，可以确保局部内部类使用的变量与外层的局部变量区分开，解决了这个问题。

## 3.55. 内部类相关，看程序说出运行结果

```java
public class Outer {
    private int age = 12;

    class Inner {
        private int age = 13;
        public void print() {
            int age = 14;
            System.out.println("局部变量：" + age);
            System.out.println("内部类变量：" + this.age);
            System.out.println("外部类变量：" + Outer.this.age);
        }
    }

    public static void main(String[] args) {
        Outer.Inner in = new Outer().new Inner();
        in.print();
    }

}
```

运行结果：

```
局部变量：14
内部类变量：13
外部类变量：12
```

## 3.56. 重写与重载

### 3.56.1. 构造器（constructor）是否可被重写（override）

构造器不能被继承，因此不能被重写，但可以被重载。

### 3.56.2. 重载（Overload）和重写（Override）的区别。重载的方法能否根据返回类型进行区分？

方法的重载和重写都是实现多态的方式，区别在于前者实现的是编译时的多态性，而后者实现的是运行时的多态性。

**重载**：发生在同一个类中，方法名相同参数列表不同（参数类型不同、个数不同、顺序不同），与方法返回值和访问修饰符无关，即重载的方法不能根据返回类型进行区分

**重写**：发生在父子类中，方法名、参数列表必须相同，返回值小于等于父类，**抛出的异常小于等于父类**，**访问修饰符大于等于父类**（里氏代换原则）；如果父类方法访问修饰符为private则子类中就不是重写。

## 3.57. 对象相等判断(需要看第二遍)

### 3.57.1. == 和 equals 的区别是什么(需要看第二遍)

**==**：它的作用是判断两个对象的地址是不是相等。即，判断两个对象是不是同一个对象。(基本数据类型 == 比较的是值，引用数据类型 == 比较的是内存地址)

**equals()** : 它的作用也是判断两个对象是否相等。但它一般有两种使用情况：

- 情况1：类没有覆盖 equals() 方法。则通过 equals() 比较该类的两个对象时，等价于通过“==”比较这两个对象。
- 情况2：类覆盖了 equals() 方法。一般，我们都覆盖 equals() 方法来两个对象的内容相等；若它们的内容相等，则返回 true (即，认为这两个对象相等)。

举个例子：

```java
public class test1 {
    public static void main(String[] args) {
        String a = new String("ab"); // a 为一个引用
        String b = new String("ab"); // b为另一个引用,对象的内容一样
        String aa = "ab"; // 放在常量池中
        String bb = "ab"; // 从常量池中查找
        if (aa == bb) // true
            System.out.println("aa==bb");
        if (a == b) // false，非同一对象
            System.out.println("a==b");
        if (a.equals(b)) // true
            System.out.println("aEQb");
        if (42 == 42.0) { // true
            System.out.println("true");
        }
    }
}
```

说明：

String中的equals方法是被重写过的，因为object的equals方法是比较的对象的内存地址，而String的equals方法比较的是对象的值。
当创建String类型的对象时，虚拟机会在常量池中查找有没有已经存在的值和要创建的值相同的对象，如果有就把它**赋给当前引用**。如果没有就在常量池中重新创建一个String对象。

### 3.57.2. hashCode 与 equals (重要)

#### 3.57.2.1. HashSet如何检查重复

两个对象的 hashCode() 相同，则 equals() 也一定为 true，对吗？

hashCode和equals方法的关系

面试官可能会问你：“你重写过 hashcode 和 equals 么，为什么重写equals时必须重写hashCode方法？”

#### 3.57.2.2. hashCode()介绍

hashCode() 的作用是获取哈希码，也称为散列码；它实际上是返回一个int整数。这个哈希码的作用是确定该对象在哈希表中的索引位置。hashCode() 定义在JDK的Object.java中，这就意味着Java中的任何类都包含有hashCode()函数。

散列表存储的是键值对(key-value)，它的特点是：能根据“键”快速的检索出对应的“值”。这其中就利用到了散列码！（可以快速找到所需要的对象）

#### 3.57.2.3. 为什么要有 hashCode

我们以“HashSet 如何检查重复”为例子来说明为什么要有 hashCode：

当你把对象加入 HashSet 时，HashSet 会先计算对象的 hashcode 值来判断对象加入的位置，同时也会与其他已经加入的对象的 hashcode 值作比较，如果没有相符的hashcode，HashSet会假设对象没有重复出现。但是如果发现有相同 hashcode 值的对象，这时会调用 equals()方法来检查 hashcode 相等的对象是否真的相同。如果两者相同，HashSet 就不会让其加入操作成功。如果不同的话，就会重新散列到其他位置。（摘自我的Java启蒙书《Head first java》第二版）。这样我们就大大减少了 equals 的次数，相应就大大提高了执行速度。

#### 3.57.2.4. hashCode()与equals()的相关规定

如果两个对象相等，则hashcode一定也是相同的

两个对象相等，对两个对象分别调用equals方法都返回true

两个对象有相同的hashcode值，它们也不一定是相等的

**因此，equals 方法被覆盖过，则 hashCode 方法也必须被覆盖**

hashCode() 的默认行为是对堆上的对象产生独特值。如果没有重写 hashCode()，则该 class 的两个对象无论如何都不会相等（即使这两个对象指向相同的数据）

#### 3.57.2.5. 对象的相等与指向他们的引用相等，两者有什么不同？

**对象的相等**比的是内存中存放的内容是否相等，而**引用相等**比较的是他们指向的内存地址是否相等。

### 3.57.3. 值传递

#### 3.57.3.1. 当一个对象被当作参数传递到一个方法后，此方法可改变这个对象的属性，并可返回变化后的结果，那么这里到底是值传递还是引用传递？

是**值传递**。**Java 语言的方法调用只支持参数的值传递**。当一个对象实例作为一个参数被传递到方法中时，参数的值就是对该对象的引用。对象的属性可以在被调用过程中被改变，但对对象引用的改变是不会影响到调用者的

#### 3.57.3.2. 为什么 Java 中只有值传递

首先回顾一下在程序设计语言中有关将参数传递给方法（或函数）的一些专业术语。

按值调用(call by value)表示方法接收的是调用者提供的值，而按引用调用（call by reference)表示方法接收的是调用者提供的变量地址。一个方法可以修改传递引用所对应的变量值，而不能修改传递值调用所对应的变量值。 它用来描述各种程序设计语言（不只是Java)中方法参数传递方式。

**Java程序设计语言总是采用按值调用。也就是说，方法得到的是所有参数值的一个拷贝，也就是说，方法不能修改传递给它的任何参数变量的内容**。

下面通过 3 个例子来给大家说明

example 1

```java
public static void main(String[] args) {
    int num1 = 10;
    int num2 = 20;

    swap(num1, num2);

    System.out.println("num1 = " + num1);
    System.out.println("num2 = " + num2);
}

public static void swap(int a, int b) {
    int temp = a;
    a = b;
    b = temp;

    System.out.println("a = " + a);
    System.out.println("b = " + b);
}
```

结果：

```
a = 20
b = 10
num1 = 10
num2 = 20
```

解析：

![img](./images/OOP-2.png)

在swap方法中，a、b的值进行交换，并不会影响到 num1、num2。因为，a、b中的值，只是从 num1、num2 的复制过来的。也就是说，a、b相当于num1、num2 的副本，副本的内容无论怎么修改，都不会影响到原件本身。

通过上面例子，我们已经知道了一个方法不能修改一个基本数据类型的参数，而对象引用作为参数就不一样，请看 example2.

example 2

```java
public static void main(String[] args) {
    int[] arr = { 1, 2, 3, 4, 5 };
    System.out.println(arr[0]);
    change(arr);
    System.out.println(arr[0]);
}

public static void change(int[] array) {
    // 将数组的第一个元素变为0
    array[0] = 0;
}
```

结果：

```
1
0
1
2
```

解析：

![img](./images/OOP-3.png)

array 被初始化 arr 的拷贝也就是一个对象的引用，也就是说 array 和 arr 指向的时同一个数组对象。 因此，外部对引用对象的改变会反映到所对应的对象上。

**通过 example2 我们已经看到，实现一个改变对象参数状态的方法并不是一件难事。理由很简单，方法得到的是对象引用的拷贝，对象引用及其他的拷贝同时引用同一个对象**。

**很多程序设计语言（特别是，C++和Pascal)提供了两种参数传递的方式：值调用和引用调用。有些程序员（甚至本书的作者）认为Java程序设计语言对对象采用的是引用调用，实际上，这种理解是不对的。由于这种误解具有一定的普遍性，所以下面给出一个反例来详细地阐述一下这个问题**。

example 3

```java
public class Test {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Student s1 = new Student("小张");
        Student s2 = new Student("小李");
        Test.swap(s1, s2);
        System.out.println("s1:" + s1.getName());
        System.out.println("s2:" + s2.getName());
    }

    public static void swap(Student x, Student y) {
        Student temp = x;
        x = y;
        y = temp;
        System.out.println("x:" + x.getName());
        System.out.println("y:" + y.getName());
    }
}
```

结果：

```
x:小李
y:小张
s1:小张
s2:小李
```

解析：

交换之前：

![img](./images/OOP-4.png)

交换之后：

![img](./images/OOP-5.png)

通过上面两张图可以很清晰的看出：**方法并没有改变存储在变量 s1 和 s2 中的对象引用。swap方法的参数x和y被初始化为两个对象引用的拷贝，这个方法交换的是这两个拷贝**

#### 3.57.3.3. 总结

Java程序设计语言对对象采用的不是引用调用，实际上，**对象引用是按值传递**的。

下面再总结一下Java中方法参数的使用情况：

1. 一个方法不能修改一个基本数据类型的参数（即数值型或布尔型）
2. 一个方法可以改变一个对象参数的状态。
3. 一个方法不能让对象参数引用一个新的对象。

### 3.57.4. 值传递和引用传递有什么区别

值传递：指的是在方法调用时，传递的参数是按值的拷贝传递，传递的是值的拷贝，也就是说传递后就互不相关了。

引用传递：指的是在方法调用时，传递的参数是按引用进行传递，其实传递的是**引用的地址，也就是变量所对应的内存空间的地址**。传递的是值的引用，也就是说传递前和传递后都指向同一个引用（也就是同一个内存空间）。

## 3.58. 字符型常量和字符串常量的区别

- 形式上：字符常量是单引号引起的一个字符；字符串常量是双引号引起的若干个字符
- 含义上：字符常量相当于一个整形值(ASCII值)，可以参加表达式运算；字符串常量代表一个地址值(该字符串在内存中存放位置)
- 占内存大小：字符常量只占一个字节；字符串常量占若干个字节(至少一个字符结束标志)

## 3.59. 什么是字符串常量池？(需要看第二遍)

字符串常量池位于堆内存中，专门用来存储字符串常量，可以提高内存的使用率，避免开辟多块空间存储相同的字符串，在创建字符串时 JVM 会首先检查字符串常量池，如果该字符串已经存在池中，则返回它的引用，如果不存在，则实例化一个字符串放到池中，并返回其引用。

## 3.60. String 是最基本的数据类型吗

不是。Java 中的基本数据类型只有 8 个 ：byte、short、int、long、float、double、char、boolean；除了基本类型（primitive type），剩下的都是引用类型（referencetype），Java 5 以后引入的枚举类型也算是一种比较特殊的引用类型。

这是很基础的东西，但是很多初学者却容易忽视，Java 的 8 种基本数据类型中不包括 String，基本数据类型中用来描述文本数据的是 char，但是它只能表示单个字符，比如 ‘a’,‘好’ 之类的，如果要描述一段文本，就需要用多个 char 类型的变量，也就是一个 char 类型数组，比如“你好” 就是长度为2的数组 char[] chars = {‘你’,‘好’};

但是使用数组过于麻烦，所以就有了 String，String 底层就是一个 char 类型的数组，只是使用的时候开发者不需要直接操作底层数组，用更加简便的方式即可完成对字符串的使用。

## 3.61. String有哪些特性(需要看第二遍)

- 不变性：String 是只读字符串，是一个典型的 immutable 对象，对它进行任何操作，其实都是创建一个新的对象，再把引用指向该对象。不变模式的主要作用在于当一个对象需要被多线程共享并频繁访问时，可以保证数据的一致性。

- 常量池优化：String 对象创建之后，会在字符串常量池中进行缓存，如果下次创建同样的对象时，会直接返回缓存的引用。

- final：使用 final 来定义 String 类，表示 String 类不能被继承，提高了系统的安全性。


## 3.62. String为什么是不可变的吗？

简单来说就是String类利用了final修饰的char类型数组存储字符，源码如下图所以：

```java
/** The value is used for character storage. */
private final char value[];
```

## 3.63. String真的是不可变的吗？

我觉得如果别人问这个问题的话，回答不可变就可以了。 下面只是给大家看两个有代表性的例子：

### 3.63.1. String不可变但不代表引用不可以变

```java
String str = "Hello";
str = str + " World";
System.out.println("str=" + str);
```

结果：

```
str=Hello World
```

解析：

实际上，原来String的内容是不变的，只是str由原来指向"Hello"的内存地址转为指向"Hello World"的内存地址而已，也就是说多开辟了一块内存区域给"Hello World"字符串。

### 3.63.2. 通过反射是可以修改所谓的“不可变”对象

```java
// 创建字符串"Hello World"， 并赋给引用s
String s = "Hello World";

System.out.println("s = " + s); // Hello World

// 获取String类中的value字段
Field valueFieldOfString = String.class.getDeclaredField("value");

// 改变value属性的访问权限
valueFieldOfString.setAccessible(true);

// 获取s对象上的value属性的值
char[] value = (char[]) valueFieldOfString.get(s);

// 改变value所引用的数组中的第5个字符
value[5] = '_';

System.out.println("s = " + s); // Hello_World
```

结果：

```
s = Hello World
s = Hello_World
```

解析：

用反射可以访问私有成员， 然后反射出String对象中的value属性， 进而改变通过获得的value引用改变数组的结构。但是一般我们不会这么做，这里只是简单提一下有这个东西。

## 3.64. 是否可以继承 String 类

String 类是 final 类，不可以被继承。

## 3.65. String str="i"与 String str=new String(“i”)一样吗？

不一样，因为内存的分配方式不一样。String str="i"的方式，java 虚拟机会将其分配到常量池中；而 String str=new String(“i”) 则会被分到堆内存中。

## 3.66. String s = new String(“xyz”);创建了几个字符串对象

两个对象，一个是静态区的"xyz"，一个是用new创建在堆上的对象。

```java
String str1 = "hello"; //str1指向静态区
String str2 = new String("hello");  //str2指向堆上的对象
String str3 = "hello";
String str4 = new String("hello");
System.out.println(str1.equals(str2)); //true
System.out.println(str2.equals(str4)); //true
System.out.println(str1 == str3); //true
System.out.println(str1 == str2); //false
System.out.println(str2 == str4); //false
System.out.println(str2 == "hello"); //false
str2 = str1;
System.out.println(str2 == "hello"); //true
```

## 3.67. 如何将字符串反转？

使用 StringBuilder 或者 stringBuffer 的 reverse() 方法。

示例代码：

```java
// StringBuffer reverse
StringBuffer stringBuffer = new StringBuffer();
stringBuffer.append("abcdefg");
System.out.println(stringBuffer.reverse()); // gfedcba
// StringBuilder reverse
StringBuilder stringBuilder = new StringBuilder();
stringBuilder.append("abcdefg");
System.out.println(stringBuilder.reverse()); // gfedcba
```

## 3.68. 数组有没有 length()方法？String 有没有 length()方法

数组没有 length()方法 ，有 length 的属性。String 有 length()方法。JavaScript中，获得字符串的长度是通过 length 属性得到的，这一点容易和 Java 混淆。

## 3.69. String 类的常用方法都有那些？

- indexOf()：返回指定字符的索引。
- charAt()：返回指定索引处的字符。
- replace()：字符串替换。
- trim()：去除字符串两端空白。
- split()：分割字符串，返回一个分割后的字符串数组。
- getBytes()：返回字符串的 byte 类型数组。
- length()：返回字符串长度。
- toLowerCase()：将字符串转成小写字母。
- toUpperCase()：将字符串转成大写字符。
- substring()：截取字符串。
- equals()：字符串比较。

## 3.70. 在使用 HashMap 的时候，用 String 做 key 有什么好处？

HashMap 内部实现是通过 key 的 hashcode 来确定 value 的存储位置，因为字符串是不可变的，所以当创建字符串时，它的 hashcode 被缓存下来，不需要再次计算，所以相比于其他对象更快。

## 3.71. String和StringBuffer、StringBuilder的区别是什么？String为什么是不可变的(需要看第二遍)

### 3.71.1. 可变性（关于String的优化）

String类中使用字符数组保存字符串，`private　final　char　value[]`，所以string对象是不可变的。

StringBuilder与StringBuffer都继承自AbstractStringBuilder类，在AbstractStringBuilder中也是使用字符数组保存字符串，char[] value，这两种对象都是可变的。

### 3.71.2. 线程安全性

String中的对象是不可变的，也就可以理解为常量，线程安全。

AbstractStringBuilder是StringBuilder与StringBuffer的公共父类，定义了一些字符串的基本操作，如expandCapacity、append、insert、indexOf等公共方法。

StringBuffer对方法加了同步锁或者对调用的方法加了同步锁，所以是线程安全的。

StringBuilder并没有对方法进行加同步锁，所以是非线程安全的。

### 3.71.3. 性能

每次对String 类型进行改变的时候，都会生成一个新的String对象，然后将指针指向新的String 对象。

StringBuffer每次都会对StringBuffer对象本身进行操作，而不是生成新的对象并改变对象引用。相同情况下使用StirngBuilder 相比使用StringBuffer 仅能获得10%~15% 左右的性能提升，但却要冒多线程不安全的风险。

### 3.71.4. 对于三者使用的总结

如果要操作少量的数据用 = String

**单线程操作字符串缓冲区**下操作大量数据 = StringBuilder

**多线程操作字符串缓冲区**下操作大量数据 = StringBuffer



