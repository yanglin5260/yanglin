package com.yang.integrated.sap;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSONObject;

public class JiraAPIForQRayUtil {

  private final static String BASIC_AUTH_JIRA = "Basic "
      + Base64.getUrlEncoder().encodeToString(("C5323535:MUMUmumu52602109").getBytes());

  public static void main(String[] args) throws Exception {

    File file = new File("idAndKey.txt");
    if (!file.exists()) {
      file.createNewFile();
    }
    // get JIRA ticket id or key
    List<String> content = Files.lines(Path.of("idAndKey.txt"), StandardCharsets.UTF_8)
        .filter(m -> !"".equals(m.trim())).collect(
            Collectors.toList());

//    // Change to test's Test Repository Path(fields.customfield_24694) to "Deprecated by DIT" by issue id，Folder must exist
//    for (String id : content) {
//      String result = sendHTTPRequest(
//          RequestBuilder.get("https://jira.successfactors.com/rest/api/2/issue/" + id).addHeader("Authorization",
//                  BASIC_AUTH_JIRA)
//              .build());
//      String customfield_24694 = JSONObject.parseObject(result).getJSONObject("fields").getString("customfield_24694");
//
//      Map<String, Object> test = new HashMap<>();
//      Map<String, Object> fields = new HashMap<>();
//      fields.put("customfield_24694", customfield_24694.replaceAll("/Deprecated by DIT", "")
//          .replaceAll("/Services", "/Services/Deprecated by DIT"));
//      test.put("fields", fields);
//
//      sendHTTPRequest(
//          RequestBuilder.put("https://jira.successfactors.com/rest/api/2/issue/" + id)
//              .setEntity(new StringEntity(JSONObject.toJSONString(test))).addHeader("Authorization",
//                  BASIC_AUTH_JIRA)
//              .build());
//    }

//    // Change test's Test Repository Path(fields.customfield_24694) to "Deprecated Invalid" by issue id, Folder must exist
//    for (String id : content) {
//      String result = sendHTTPRequest(
//          RequestBuilder.get("https://jira.successfactors.com/rest/api/2/issue/" + id).addHeader("Authorization",
//                  BASIC_AUTH_JIRA)
//              .build());
//      String customfield_24694 = JSONObject.parseObject(result).getJSONObject("fields").getString("customfield_24694");
//
//      Map<String, Object> test = new HashMap<>();
//      Map<String, Object> fields = new HashMap<>();
//      fields.put("customfield_24694", customfield_24694.replaceAll("/Deprecated Invalid", "")
//          .replaceAll("/Services", "/Services/Deprecated Invalid"));
//      test.put("fields", fields);
//
//      sendHTTPRequest(
//          RequestBuilder.put("https://jira.successfactors.com/rest/api/2/issue/" + id)
//              .setEntity(new StringEntity(JSONObject.toJSONString(test))).addHeader("Authorization",
//                  BASIC_AUTH_JIRA)
//              .build());
//    }

//    // Change test's Status to "Deprecated"(id=41) by issue key, but it can be modified only once
//    for (String key : content) {
//      Map<String, Object> params = new HashMap<>();
//      Map<String, Object> transition = new HashMap<>();
//      transition.put("id", "41");
//      params.put("transition", transition);
//
//      sendHTTPRequest(
//          RequestBuilder.post("https://jira.successfactors.com/rest/api/2/issue/" + key + "/transitions")
//              .setEntity(new StringEntity(JSONObject.toJSONString(params)))
//              .addHeader("Authorization", BASIC_AUTH_JIRA)
//              .build());
//    }

    // Change test's Test Automation Type(fields.customfield_26580) to "Not Automated (Manual)"(id=81760) by issue id
    for (String id : content) {
      Map<String, Object> params = new HashMap<>();
      Map<String, Object> fields = new HashMap<>();
      Map<String, Object> customfield_26580 = new HashMap<>();
      customfield_26580.put("id", "81760");
      fields.put("customfield_26580", customfield_26580);
      params.put("fields", fields);

      sendHTTPRequest(
          RequestBuilder.put("https://jira.successfactors.com/rest/api/2/issue/" + id)
              .setEntity(new StringEntity(JSONObject.toJSONString(params))).addHeader("Authorization",
                  BASIC_AUTH_JIRA).build());
    }

  }

  private static String sendHTTPRequest(HttpUriRequest request) {
    request.addHeader("Content-Type", "application/json; charset=utf-8");
    String result = null;
    try (CloseableHttpClient closeableHttpClient = HttpClientBuilder.create().build();) {
      CloseableHttpResponse response = closeableHttpClient.execute(request);
      System.out.println(request.getURI() + "---Response Code is " + response.getStatusLine().getStatusCode());
      result = response.getEntity() == null ? null : EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return result;
  }

}
