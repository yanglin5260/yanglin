package com.yang.integrated.controller;

import com.yang.integrated.controller.common.BaseHibernateController;
import com.yang.integrated.pojo.CategoryBrand;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * Category Brand
 * </p>
 *
 * @author yanglin
 * @date 2020-06-21 17:58:25
 */
@RestController
@RequestMapping("/category-brand")
public class CategoryBrandController extends BaseHibernateController<CategoryBrand> {
}
