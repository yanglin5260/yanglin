package com.yang.integrated.utils;

import com.github.houbb.markdown.toc.constant.TocConstant;
import com.github.houbb.markdown.toc.core.MarkdownToc;
import com.github.houbb.markdown.toc.exception.MarkdownTocRuntimeException;
import com.github.houbb.markdown.toc.support.IncreaseMap;
import com.github.houbb.markdown.toc.util.FileUtil;
import com.github.houbb.markdown.toc.util.StringUtil;
import com.github.houbb.markdown.toc.vo.TocGen;
import com.github.houbb.markdown.toc.vo.TocVo;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * <p>
 * AtxMarkdownToc 重写，实现MarkdownToc，自定义目录生成
 * </p>
 *
 * @author yanglin
 * @date 2020-12-25 17:10:41
 */
public class MarkdownUtil implements MarkdownToc {

  private static String currentPath = null;

  private static int imageIndex = 1;

  private static List<String> existImages = new ArrayList<>();

  static {
    // 获取当前文件夹路径
    currentPath = MarkdownUtil.class.getResource("/").getPath();
  }

  private List<String> fileContentList = new LinkedList();

  private List<String> tocStrList = new LinkedList();

  private List<TocVo> tocVoList = new LinkedList();

  private List<String> resultList = new LinkedList();

  private TocVo previous;

  private Charset charset = Charset.forName("UTF-8");

  private boolean subTree = true;

  private boolean write = true;

  private Integer codeMark = 0;

  public MarkdownUtil() {
  }

  public static MarkdownUtil newInstance() {
    return new MarkdownUtil();
  }

  /**
   * <p>
   * 主测试方法
   * </p>
   *
   * @param args
   * @author yanglin
   * @date 2020-12-25 17:11:39
   */
  public static void main(String[] args) {
    // 记录一个文档中存在的图片
    existImages.clear();
    // 让主文件夹yanglin下面的所有Markdown文件生成目录，并且规范化图片以及图片引用
    traverseFolder(URLDecoder.decode(
        new File(currentPath).getParentFile().getParentFile().getParentFile().getParentFile().getPath(),
        StandardCharsets.UTF_8));
    // 统一删除未用到的图片
    if (!existImages.isEmpty()) {
      // 图片路径去重并排序
      Collections.sort(new ArrayList<>(new HashSet<>(existImages)));
      Set<String> imageFolder = new HashSet<>();
      existImages.stream().forEach(m -> imageFolder.add(new File(m).getParent()));
      for (String folder : imageFolder) {
        File f = new File(folder);
        for (String i : f.list()) {
          File m = new File(f, i);
          if (!existImages.contains(m.toString())) {
            m.delete();
          }
        }
      }
    }
  }

  /**
   * <p>
   * 递归遍历文件夹下面的Markdown文件，并自动生成目录
   * </p>
   *
   * @param path
   * @author yanglin
   * @date 2020-12-29 01:01:17
   */
  public static void traverseFolder(String path) {
    File file = new File(path);
    if (file.exists()) {
      File[] files = file.listFiles();
      if (null == files || files.length == 0) {
        System.out.println("文件夹是空的!");
        return;
      } else {
        for (File file2 : files) {
          if (file2.isDirectory()) {
            traverseFolder(file2.getAbsolutePath());
          } else {
            String absolutePath = file2.getAbsolutePath();
            if (absolutePath.endsWith(".md")) {
              System.out.println("生成目录文件:" + absolutePath);
              // 格式化两次，由于在生成标题编号时候有Bug，后面有时间修正
              MarkdownUtil.newInstance()
                  .charset(StandardCharsets.UTF_8.toString())
                  .write(Boolean.TRUE)
                  .subTree(Boolean.TRUE)
                  .genTocFile(absolutePath);
              MarkdownUtil.newInstance()
                  .charset(StandardCharsets.UTF_8.toString())
                  .write(Boolean.TRUE)
                  .subTree(Boolean.TRUE)
                  .genTocFile(absolutePath);
            }
          }
        }
      }
    } else {
      System.out.println("文件不存在!");
    }
  }

  /**
   * <p>
   * 下载并存储图片
   * </p>
   *
   * @param urlString
   * @param filename
   * @param savePath
   * @author yanglin
   * @date 2020-12-31 01:31:08
   */
  public static String download(String urlString, String filename, String savePath) {
    // 输出的文件流
    File sf = new File(savePath);
    if (!sf.exists()) {
      sf.mkdirs();
    }

    HttpEntity he = null;
    try {
      he = HttpClientBuilder.create().build().execute(RequestBuilder.get(urlString).build()).getEntity();
    } catch (IOException e) {
      e.printStackTrace();
    }
    String imgSrcExtension = "." + he.getContentType().getValue().replaceAll("image/", "");
    try (InputStream is = he.getContent(); OutputStream os = new FileOutputStream(
        Path.of(sf.getPath(), filename + imgSrcExtension).toString());) {
      // 1K的数据缓冲
      byte[] bs = new byte[1024];
      // 读取到的数据长度
      int len;
      // 开始读取
      while ((len = is.read(bs)) != -1) {
        os.write(bs, 0, len);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    // 返回文件类型，图片后缀名
    return imgSrcExtension;
  }

  public TocGen genTocFile(String filePath) {
    this.checkPath(filePath);
    Path path = Paths.get(filePath);
    return this.genTocForFile(path, this.charset);
  }

  public List<TocGen> genTocDir(String dirPath) {
    this.checkPath(dirPath);
    List<TocGen> tocGens = new ArrayList();
    Path path = Paths.get(dirPath);
    if (!Files.isDirectory(path, new LinkOption[0])) {
      String msg = String.format("%s 不是文件夹目录", dirPath);
      throw new MarkdownTocRuntimeException(msg);
    } else {
      List<Path> paths = FileUtil.getMdFilePathList(path, this.subTree);
      Iterator var5 = paths.iterator();

      while (var5.hasNext()) {
        Path filePath = (Path) var5.next();
        TocGen tocGen = this.genTocForFile(filePath, this.charset);
        tocGens.add(tocGen);
      }

      return tocGens;
    }
  }

  public MarkdownUtil charset(String charset) {
    this.charset = Charset.forName(charset);
    return this;
  }

  public MarkdownUtil subTree(boolean subTree) {
    this.subTree = subTree;
    return this;
  }

  public MarkdownUtil write(boolean write) {
    this.write = write;
    return this;
  }

  private void checkPath(String path) {
    if (StringUtil.isEmpty(path)) {
      throw new MarkdownTocRuntimeException("文件路径不可为空！");
    }
  }

  private synchronized TocGen genTocForFile(Path path, Charset charset) {
    try {
      if (!FileUtil.isMdFile(path.toString())) {
        throw new MarkdownTocRuntimeException("当前只支持 markdown 文件");
      } else {
        this.initFileContentList(path, charset);
        this.initToc();
        TocGen tocGen = new TocGen(path.toString(), this.resultList);
        this.resultList.addAll(this.fileContentList);
        // 处理文档中的图片，并先把图片编号设置为1
        imageIndex = 1;
        handlePictureInMarkDown(path);
        if (this.write) {
          Files.write(path, this.resultList, charset);
        }
        return tocGen;
      }
    } catch (IOException var4) {
      throw new MarkdownTocRuntimeException(var4);
    }
  }

  private void handlePictureInMarkDown(Path path) {
    // 设置上一个标题编号
    List<Integer> titleNumber = new ArrayList<>(16);
    titleNumber.add(0);
    for (int i = 0, size = this.resultList.size(); i < size; i++) {
      String s = this.resultList.get(i);

      if (s.startsWith("#") && codeMark % 2 == 0) {
        String[] titleArr = s.trim().split("[\\s]+");
        // 生成标题
        char[] titleMark = titleArr[0].toCharArray();
        int markLen = titleMark.length;
        int numberSize = titleNumber.size();
        if (numberSize == markLen) {
          titleNumber.set(markLen - 1, new AtomicInteger(titleNumber.get(markLen - 1)).incrementAndGet());
        } else if (markLen > numberSize) {
          for (int j = markLen - numberSize; j > 0; j--) {
            titleNumber.add(1);
          }
        } else {
          titleNumber = titleNumber.subList(0, markLen);
          titleNumber.set(markLen - 1, new AtomicInteger(titleNumber.get(markLen - 1)).incrementAndGet());
        }
        String standardNumber = titleNumber.stream().map(m -> m + ".").collect(Collectors.joining(""));
        // 判断是否格式化过标题号
        if (titleArr[1] != null) {
          if (titleArr[1].matches("([\\d]+\\.)+?") && !standardNumber.equals(titleArr[1])) {
            // 原先格式化内容不对
            s = s.replaceAll(titleArr[0] + " " + titleArr[1], titleArr[0] + " " + standardNumber);
          }
          if (!titleArr[1].matches("([\\d]+\\.)+?")) {
            // 原先格式化内容不对
            s = s.replaceAll(titleArr[0], titleArr[0] + " " + standardNumber);
          }
        } else {
          s = s.replaceAll(titleArr[0], titleArr[0] + " " + standardNumber);
        }
      }
      if (s.trim().startsWith("```")) {
        this.codeMark++;
      }
      this.resultList.set(i, matchAndReplace(s, path));
    }
  }

  /**
   * <p>
   * 匹配Markdown中的图片url，![...](...)
   * </p>
   *
   * @param content
   * @return java.lang.String
   * @author yanglin
   * @date 2020-12-30 23:59:49
   */
  private synchronized String matchAndReplace(String content, Path path) {
    Pattern pattern = Pattern.compile("!\\[[^\\]]*\\](\\([^\\)]*\\))");
    Matcher matcher = pattern.matcher(content);
    while (matcher.find()) {
      try {
        String[] imageString = matcher.group().split("\\(");
        // 新图片文件夹名字
        String newImageDirectory = "images";
        // 图片url之前
        String imgSrcPre = imageString[0];
        // 图片url
        String imgSrc = imageString[1].substring(0, imageString[1].length() - 1);
        // 图片扩展名
        String imgSrcExtension = imgSrc.substring(imgSrc.lastIndexOf("."));
        // Markdown 文件名
        String pathFileName = path.getFileName().toString();
        String mdFileName = pathFileName.substring(0, pathFileName.lastIndexOf("."));
        // 目标存储路径
        Path tarfetPathDir = Path.of(path.getParent().toString(), newImageDirectory);
        while (Path.of(tarfetPathDir.toString(), mdFileName + "-" + imageIndex + imgSrcExtension).toFile().exists()) {
          imageIndex++;
        }
        Path targetFile = Path.of(tarfetPathDir.toString(), mdFileName + "-" + imageIndex + imgSrcExtension);
        // 如果之前规范化过图片，就略过
        if (new File(imgSrc).getName().startsWith(mdFileName) && imgSrc.startsWith("./" + newImageDirectory + "/")) {
          existImages.add(Path.of(tarfetPathDir.toString(), imgSrc.replaceAll("./images", "")).toString());
          break;
        }
        // 不是图片，也要略过
        if (!imgSrc.contains(".")) {
          break;
        }
        // 不存在新图片文件夹时候，创建文件夹
        if (!tarfetPathDir.toFile().exists()) {
          Files.createDirectories(tarfetPathDir);
        }
        // 处理下载的路径
        if (imgSrc.startsWith("http")) {//图片url是远程路径
          imgSrcExtension = download(imgSrc, mdFileName + "-" + imageIndex, tarfetPathDir.toString());
          existImages.add(
              Path.of(tarfetPathDir.toString(), mdFileName + "-" + imageIndex + imgSrcExtension).toString());
        } else {//图片url是本地路径
          try {
            // 记录存在的图片路径
            existImages.add(targetFile.toString());
            Path localImage = null;
            if (imgSrc.startsWith("\\./")) {
              localImage = Path.of(path.getParent().toString(), imgSrc.replaceFirst("\\./", "/"));
              // 重命名文件，相当于剪切
//                        localImage.toFile().renameTo(targetFile.toFile());
              Files.move(localImage, targetFile, StandardCopyOption.ATOMIC_MOVE);
            } else {
              localImage = Path.of(imgSrc);
              // 重命名文件，相当于剪切
//                        localImage.toFile().renameTo(targetFile.toFile());
              Files.copy(localImage, targetFile);
            }
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
        content = imgSrcPre + "(./" + newImageDirectory + "/" + mdFileName + "-" + imageIndex + imgSrcExtension + ")";
      } catch (Exception e) {
        //e.printStackTrace();
      }
    }
    return content;
  }

  /**
   * <p>
   * 清空旧的目录
   * </p>
   *
   * @param path
   * @param charset
   * @author yanglin
   * @date 2020-12-25 21:21:59
   */
  private void initFileContentList(Path path, Charset charset) throws IOException {
    this.fileContentList.clear();
    this.fileContentList = Files.readAllLines(path, charset);
    String firstLine = this.fileContentList.get(0);
    if (firstLine.startsWith("<h2>目录</h2>")) {
      Iterator<String> stringIterator = this.fileContentList.iterator();
      this.nextAndRemove(stringIterator, 2);

      while (stringIterator.hasNext()) {
        String contentTrim = stringIterator.next().trim();
        if (!contentTrim.startsWith("<")) {
          break;
        }
        stringIterator.remove();
      }

      stringIterator.remove();
      this.nextAndRemove(stringIterator, 1);
    }
  }

  private void nextAndRemove(Iterator<String> stringIterator, int times) {
    for (int i = 0; i < times; ++i) {
      String content = stringIterator.next();
      stringIterator.remove();
    }
  }

  private void initToc() {
    this.resultList.clear();
    this.tocStrList.clear();
    this.tocVoList.clear();
    this.resultList.add("<h2>目录</h2>" + TocConstant.RETURN_LINE);
    Iterator var1 = this.fileContentList.iterator();

    while (var1.hasNext()) {
      String string = (String) var1.next();
      String trim = string.trim();
      if (trim.startsWith("#") && codeMark % 2 == 0) {
        this.tocStrList.add(trim);
      }
      if (trim.startsWith("```")) {
        this.codeMark++;
      }
    }

    IncreaseMap increaseMap = new IncreaseMap();
    TocVo root = TocVo.rootToc(increaseMap);
    root.setParent((TocVo) null);
    this.tocVoList.add(root);
    this.previous = root;
    Iterator var7 = this.tocStrList.iterator();

    while (var7.hasNext()) {
      String string = (String) var7.next();
      this.addNewToc(string, increaseMap);
    }
    // 移除重复的第一个项
    try {
      this.tocVoList.get(0).getChildren().remove(0);
    } catch (Exception e) {
      e.printStackTrace();
    }
//        this.showToc(root.getChildren());
    this.resultList.addAll(Arrays.asList(this.showToc(root.getChildren()).split("\\n")));
    this.resultList.add(TocConstant.RETURN_LINE);
  }

  private String showToc(List<TocVo> tocVoList) {
    if (!this.tocStrList.isEmpty()) {
      String result = "";
      Iterator var2 = tocVoList.iterator();

      while (var2.hasNext()) {
        TocVo tocVo = (TocVo) var2.next();
        boolean noChildren = tocVo.getChildren().isEmpty();
        String suffix = this.getSuffix(tocVo.getLevel());

        if (noChildren) {// 没有子节点时，加前缀

        } else {// 有子节点时，加前缀，折叠，展开
          result += suffix + "<details open>\n" + suffix + "  <summary>";
        }

        String tocVoContent =
            suffix + String.format("  <a href=\"%s\">%s</a>", tocVo.getTocHref(), tocVo.getTocTitle());
        if (noChildren) {// 没有子节点时，加后缀
          //添加本节点内容
          result += tocVoContent;
        } else {// 有子节点时，加后缀
          //添加本节点内容
          result += tocVoContent.trim();
        }
        if (noChildren) {// 没有子节点时，加后缀

        } else {// 有子节点时，加后缀
          result += suffix + "</summary>\n" + suffix + "  <ul>\n";
        }
        // 添加子节点内容
        result += this.showToc(tocVo.getChildren());
        //                this.resultList.add(tocVoContent);
        if (noChildren) {// 没有子节点时，加后缀
          // 添加子节点内容后缀
          result += "<br>\n";
        } else {// 有子节点时，加后缀
          // 添加子节点内容后缀
          result += suffix + "  </ul>\n" + suffix + "</details>\n";
        }
      }
      return result;
    } else {
      return "";
    }
  }

  private String getSuffix(int level) {
    StringBuilder result = new StringBuilder();
    for (int i = 0; i < level - 1; ++i) {
      result.append("  ");
    }
    return result.toString();
  }

  private void addNewToc(String tocTrimStr, IncreaseMap increaseMap) {
    TocVo current = new TocVo(tocTrimStr, increaseMap);
    TocVo previousParent;
    if (current.getLevel() == 1) {
      previousParent = this.tocVoList.get(0);
      current.setParent(previousParent);
      previousParent.getChildren().add(current);
    } else if (current.getLevel() == this.previous.getLevel()) {
      previousParent = this.previous.getParent();
      current.setParent(previousParent);
      previousParent.getChildren().add(current);
    } else if (current.getLevel() < this.previous.getLevel()) {
      try {
        previousParent = this.previous.getParent();
        int n = this.previous.getLevel();
        while (--n >= current.getLevel()) {
          previousParent = previousParent.getParent();
        }
        current.setParent(previousParent);
        previousParent.getChildren().add(current);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    if (current.getLevel() > this.previous.getLevel()) {
      current.setParent(this.previous);
      this.previous.getChildren().add(current);
    }
    this.previous = current;
  }
}
