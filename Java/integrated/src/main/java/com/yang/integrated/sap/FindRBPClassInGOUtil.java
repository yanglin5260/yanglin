package com.yang.integrated.sap;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

/**
 * 查找RBP的类，在GO项目里
 */
public class FindRBPClassInGOUtil {

  public static void main(String[] args) throws IOException {

    File file = new File("RBPClasses.txt");
    if (!file.exists()) {
      file.createNewFile();
    } else {
      file.delete();
      file.createNewFile();
    }
    traverseFolder("/Users/C5323535/Work/workspace/au-genericobject");
  }

  public static void traverseFolder(String path) throws FileNotFoundException {
    File file = new File(path);
    if (file.exists()) {
      File[] files = file.listFiles();
      if (null == files || files.length == 0) {
        System.out.println("文件夹是空的!");
        return;
      } else {
        for (File file2 : files) {
          if (file2.isDirectory()) {
            traverseFolder(file2.getAbsolutePath());
          } else {
            String absolutePath = file2.getAbsolutePath();
            if (absolutePath.endsWith(".java") && absolutePath.toLowerCase().contains("rbp") && !absolutePath.contains(
                "uitestbase") && !absolutePath.contains("testbase") && !absolutePath.contains("/test/")) {
              //追加写模式
              try (BufferedWriter writer =
                  Files.newBufferedWriter(Path.of("RBPClasses.txt"),
                      StandardCharsets.UTF_8,
                      StandardOpenOption.APPEND)) {
                writer.write(absolutePath);
                writer.write("\n");
              } catch (IOException e) {
                e.printStackTrace();
              }
              System.out.println(absolutePath);
            }
          }
        }
      }
    } else {
      System.out.println("文件不存在!");
    }
  }

}
