package com.yang.integrated.controller;

import com.yang.integrated.controller.common.BaseHibernateController;
import com.yang.integrated.pojo.Spu;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * Spu
 * </p>
 *
 * @author yanglin
 * @date 2020-07-05 23:43:13
 */
@RestController
@RequestMapping("/spu")
public class SpuController extends BaseHibernateController<Spu> {
}
