package com.yang.integrated.sap;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

import com.yang.integrated.utils.FileUtil;

/**
 * <p>
 * 公司任务：测试方法相关统计，DataIO(Sunny)
 * </p>
 *
 * @author yanglin
 * @date 2021-6-15 15:22:44
 */
public class TestMethodNameStatisticsDataIO {

  public static final String BASE_PATH = "C:\\workspace\\au-mdfdataio\\au-mdfdataio-testbase\\src\\test\\java\\com\\successfactors\\genericobject";

  public static final String BASE_PATH_VIEW = "C:\\workspace\\au-mdfdataio\\au-mdfdataio-testbase\\src\\test\\java\\com\\successfactors\\genericobject";

  public static final String TARGET_PATH = "C:\\Users\\C5323535\\Desktop\\Kelly\\TestMethodNameStatisticsDataIO.txt";

  public static void main(String[] args) throws IOException {
    File file = new File(TARGET_PATH);
    file.delete();
    file.getParentFile().mkdirs();
    file.createNewFile();
    FileUtil.appendContentInFile(TARGET_PATH,
        "Path\t" + "TestMethodName\t" + "Broken or Pass\t" + "On Class\t" + "On Method");
    traverseFolder(BASE_PATH);
  }

  /**
   * <p>
   * 递归遍历文件夹下面的Markdown文件，并自动生成目录
   * </p>
   *
   * @param path
   * @author yanglin
   * @date 2020-12-29 01:01:17
   */
  public static void traverseFolder(String path) {
    File file = new File(path);
    if (file.exists()) {
      File[] files = file.listFiles();
      if (null == files || files.length == 0) {
        System.out.println("文件夹是空的!");
        return;
      } else {
        for (File file2 : files) {
          if (file2.isDirectory()) {
            traverseFolder(file2.getAbsolutePath());
          } else {
            String absolutePath = file2.getAbsolutePath();
            if (absolutePath.endsWith(".java")) {
              System.out.println("处理文件:" + absolutePath);
              try {
                List<String> fileContentList = Files.readAllLines(file2.toPath(), StandardCharsets.UTF_8);
                boolean intoClassed = false;
                String testConfigContent = "No @TestConfig on class";
                for (int i = 0, size = fileContentList.size(); i < size; i++) {
                  // 判断类上是否有相应@TestConfig注解
                  if (fileContentList.get(i).indexOf(" class ") != -1
                      && fileContentList.get(i).indexOf(file2.getName().replaceAll(".java", "")) != -1
                      && intoClassed == false) {
                    intoClassed = true;
                    int n = 0;
                    do {
                      n++;
                    } while (fileContentList.get(i - n).indexOf("import ") == -1
                        && fileContentList.get(i - n).indexOf("package ") == -1
                        && fileContentList.get(i - n).indexOf("@TestConfig") == -1);
                    if (fileContentList.get(i - n).indexOf("@TestConfig") != -1) {
                      testConfigContent = fileContentList.get(i - n).replaceAll("\\s+", "");
                    }
                  }

                  if (fileContentList.get(i).indexOf("@Test") != -1
                      && fileContentList.get(i).indexOf("@TestConfig") == -1) {
                    String content =
                        file2.getAbsolutePath().replaceAll(BASE_PATH_VIEW.replaceAll("\\\\", "\\\\\\\\"), "") + "\t";
                    int n = 0;
                    do {
                      n++;
                    } while (fileContentList.get(i + n) != null && fileContentList.get(i + n).indexOf(" void ") == -1);
                    String[] s = fileContentList.get(i + n).split("\\s+");
                    for (int j = 0; j < s.length; j++) {
                      if ("void".equals(s[j])) {
                        content += s[j + 1].split("\\(")[0].replaceAll("\\s+", "") + "\t";
                        break;
                      }
                    }
                    String broken = fileContentList.get(i);
                    for (int j = 1; j < n; j++) {
                      broken += fileContentList.get(i + j);
                    }
                    if (broken.indexOf("broken") != -1) {
                      content += "broken\t";
                    } else {
                      content += "Pass\t";
                    }

//                    if (fileContentList.get(i + n + 1).indexOf("setEntities(\"") != -1) {
//                      content += "Only old save\t";
//                    } else {
//                      content += "Both\t";
//                    }
                    content += testConfigContent + "\t";
                    String tc = "No @TestConfig on method";
                    for (int j = i + n; j >= i - 1; j--) {
                      if (fileContentList.get(j).indexOf("@TestConfig") != -1) {
                        tc = fileContentList.get(j).replaceAll("\\s+", "").trim();
                        break;
                      }
                    }
                    content += tc + "\t";
                    FileUtil.appendContentInFile(TARGET_PATH, content.trim());
                  }
                }
              } catch (IOException e) {
                e.printStackTrace();
              }
            }
          }
        }
      }
    } else {
      System.out.println("文件不存在!");
    }
  }
}
