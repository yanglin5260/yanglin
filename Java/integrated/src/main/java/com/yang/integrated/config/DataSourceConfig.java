package com.yang.integrated.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * <p>
 * Common DataSource Config
 * 在同样的DataSource中，首先使用被标注的DataSource
 * </p>
 *
 * @author yanglin
 * @date 2020-07-02 00:28:47
 */
@Configuration
@Primary
public class DataSourceConfig {

    @Value("${spring.datasource.url}")
    private String datasourceUrl;
    @Value("${spring.datasource.driver-class-name}")
    private String driverClassName;
    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String password;

    /**
     * <p>
     * Init Data Source
     * </p>
     *
     * @return javax.sql.DataSource
     * @author yanglin
     * @date 2020-07-02 00:20:43
     */
    @Bean
    public DataSource dataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl(datasourceUrl);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setDriverClassName(driverClassName);

        String url01 = datasourceUrl.substring(0, datasourceUrl.indexOf("?"));
        String url02 = url01.substring(0, url01.lastIndexOf("/"));
        String datasourceName = url01.substring(url01.lastIndexOf("/") + 1);
        try (Connection connection = DriverManager.getConnection(url02, username, password); Statement statement = connection.createStatement()) {
            Class.forName(driverClassName);
            // 创建数据库
            statement.executeUpdate("CREATE DATABASE IF NOT EXISTS `" + datasourceName + "` DEFAULT CHARACTER SET UTF8 COLLATE UTF8_GENERAL_CI");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return dataSource;
    }

}
