package com.yang.integrated.sap;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.yang.integrated.utils.FileUtil;

/**
 * <p>
 * 公司任务：筛选出本地批量网页文中的异常数据
 * </p>
 *
 * @author yanglin
 * @date 2020-06-21 00:34:31
 */
public class MyPageProcessor {

  public static void main(String[] args) {
    // 让主文件夹yanglin下面的所有Markdown文件生成目录，并且规范化图片以及图片引用
    traverseFolder("C:\\Users\\C5323535\\Desktop\\Kelly");
  }

  /**
   * <p>
   * 递归遍历文件夹下面的Markdown文件，并自动生成目录
   * </p>
   *
   * @param path
   * @author yanglin
   * @date 2020-12-29 01:01:17
   */
  public static void traverseFolder(String path) {
    File file = new File(path);
    if (file.exists()) {
      File[] files = file.listFiles();
      if (null == files || files.length == 0) {
        System.out.println("文件夹是空的!");
        return;
      } else {
        for (File file2 : files) {
          if (file2.isDirectory()) {
            traverseFolder(file2.getAbsolutePath());
          } else {
            String absolutePath = file2.getAbsolutePath();
            if (absolutePath.endsWith(".html")) {
              System.out.println("处理文件:" + absolutePath);
              Document doc = null;//productDesc为前台提交的html，以字符串的形式存在
              try {
                doc = Jsoup.parse(file2, StandardCharsets.UTF_8.name());
                // 处理表元素
                Elements trs = doc.select("table tbody tr");
                for (Element element : trs) {
                  Elements tds = element.select("td");
                  if ("SF_FIE".equals(tds.get(4).html()) && "SF_FIEL".equals(tds.get(5).html())) {
                    continue;
                  }
                  if ("SF_FIELD1".equals(tds.get(4).html()) && "SF_FIELD2".equals(tds.get(5).html())) {
                    continue;
                  }
                  if (!"userId".equals(tds.get(4).html()) || !"endDate".equals(tds.get(5).html())) {
                    String content = "";
                    for (Element e : element.select("td")) {
                      content += e.html() + "\t";
                    }
                    content += absolutePath;
                    // 把不符合要求的数据放到txt
                    FileUtil.appendContentInFile("C:\\Users\\C5323535\\Desktop\\Kelly\\ErrorData.txt", content);
//                    FileUtil.appendContentInFile("C:\\Users\\C5323535\\Desktop\\Kelly\\ErrorData.txt",
//                        element.select("td").stream().forEach(s->{return s.html();}).toString());
                    System.out.println(content);
                  }
                }
              } catch (IOException e) {
                e.printStackTrace();
              }
            }
          }
        }
      }
    } else {
      System.out.println("文件不存在!");
    }
  }
}
