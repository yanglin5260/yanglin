package com.yang.integrated.sap;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.util.Strings;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yang.integrated.utils.FileUtil;

public class MDFRBPDependencyAnalyzeUtil {

  private final static List<String> components = new ArrayList<>();

  static {
    components.add("au-permissionadmin");
    components.add("au-permissionapp");
    components.add("au-permissionist");
    components.add("idl-permission");
    components.add("au-genericobject");
  }

  public static void main(String[] args) throws Exception {
    File file = new File("MDF-RBP-classes.txt");
    if (!file.exists()) {
      file.createNewFile();
    }

    File file2 = new File("MDF-RBP-Analyze.txt");
    if (file2.exists()) {
      file2.delete();
    }
    file2.createNewFile();

    // get Classes
    List<String> content = Files.lines(Path.of("MDF-RBP-classes.txt"), StandardCharsets.UTF_8)
        .filter(m -> !"".equals(m.trim())).collect(Collectors.toList());

    for (String clazz : content) {
      if (!clazz.endsWith("Test")) {
        try {
          List<String> result = new ArrayList<>();
          JSONArray array = JSONArray.parseArray(sendHTTPRequest(RequestBuilder.get(
              "https://dependency-tool.saf-service.sap.corp/api/v1/request/singleClassDependencyFrom?className="
                  + clazz).build()));
          for (Object a : array) {
            String[] l = a.toString().split(",");
            if (components.stream().anyMatch(m -> l[0].startsWith(m)) && !content.contains(l[1])) {
              result.add(l[0] + "," + l[1]);
            }
          }
          FileUtil.appendContentInFile(file2, clazz + "\t" + "".join(" || ", result));
        } catch (Exception e) {
          e.printStackTrace();
          FileUtil.appendContentInFile(file2, clazz + "\t" + "Exception---" + e.getMessage());
        }
      } else {
        FileUtil.appendContentInFile(file2, clazz + "\tTest");
      }
    }

  }

  private static String sendHTTPRequest(HttpUriRequest request) {
    request.addHeader("Content-Type", "application/json; charset=utf-8");
    String result = null;
    try (CloseableHttpClient closeableHttpClient = HttpClientBuilder.create().setSSLSocketFactory(
        new SSLConnectionSocketFactory(
            SSLContexts.custom().loadTrustMaterial(null, new TrustSelfSignedStrategy()).build())).build();) {
      CloseableHttpResponse response = closeableHttpClient.execute(request);
      System.out.println(request.getURI() + "---Response Code is " + response.getStatusLine().getStatusCode());
      result = response.getEntity() == null ? null : EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return result;
  }

}
