package com.yang.integrated.controller;

import com.yang.integrated.controller.common.BaseHibernateController;
import com.yang.integrated.pojo.Sku;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * Sku
 * </p>
 *
 * @author yanglin
 * @date 2020-07-05 23:43:13
 */
@RestController
@RequestMapping("/sku")
public class SkuController extends BaseHibernateController<Sku> {
}
