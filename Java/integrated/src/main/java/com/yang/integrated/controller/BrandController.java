package com.yang.integrated.controller;

import com.yang.integrated.controller.common.BaseHibernateController;
import com.yang.integrated.pojo.Brand;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * Brand
 * </p>
 *
 * @author yanglin
 * @date 2020-06-21 17:58:25
 */
@RestController
@RequestMapping("/brand")
public class BrandController extends BaseHibernateController<Brand> {
}
