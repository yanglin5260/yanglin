<h2>目录</h2>

<details open>
  <summary><a href="#1-springboot基础初始化">1. SpringBoot基础初始化</a></summary>
  <ul>
    <a href="#11-pomxml基本信息">1.1. pom.xml基本信息</a><br>
    <a href="#12-基础启动类">1.2. 基础启动类</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#2-springboot打war包和jar包">2. SpringBoot打war包和jar包</a></summary>
  <ul>
    <a href="#21-添加依赖">2.1. 添加依赖</a><br>
    <a href="#22-新增servletinitializer类">2.2. 新增ServletInitializer类</a><br>
    <a href="#23-生成war包或jar">2.3. 生成war包或jar</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#3-swagger2">3. Swagger2</a></summary>
  <ul>
    <a href="#31-添加依赖">3.1. 添加依赖</a><br>
    <a href="#32-自定义swagger2通用配置类">3.2. 自定义Swagger2通用配置类</a><br>
    <a href="#33-自定义swagger2通用配置properties">3.3. 自定义Swagger2通用配置properties</a><br>
    <a href="#34-引用swagger2通用配置类">3.4. 引用Swagger2通用配置类</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#4-数据库连接配置（mysql）">4. 数据库连接配置（MySQL）</a></summary>
  <ul>
    <a href="#41-添加依赖">4.1. 添加依赖</a><br>
    <a href="#42-通用mysql数据库配置">4.2. 通用MySQL数据库配置</a><br>
    <a href="#43-自动创建数据库配置类">4.3. 自动创建数据库配置类</a><br>
    <a href="#44-引用通用mysql数据库配置">4.4. 引用通用MySQL数据库配置</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#5-通用增删改查（jpa和mybatis）">5. 通用增删改查（JPA和MyBatis）</a></summary>
  <ul>
  <details open>
    <summary><a href="#51-controller，mapper，pojo，service-and-impl">5.1. Controller，Mapper，Pojo，Service and Impl</a>  </summary>
    <ul>
      <a href="#511-controller">5.1.1. Controller</a><br>
      <a href="#512-mapper">5.1.2. Mapper</a><br>
      <a href="#513-pojo">5.1.3. Pojo</a><br>
      <a href="#514-service-and-impl">5.1.4. Service and Impl</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#52-编写增删改查实例">5.2. 编写增删改查实例</a>  </summary>
    <ul>
      <a href="#521-pojo">5.2.1. Pojo</a><br>
      <a href="#522-controller">5.2.2. Controller</a><br>
    </ul>
  </details>
    <a href="#53-测试">5.3. 测试</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#6-springboot测试类">6. Springboot测试类</a></summary>
  <ul>
    <a href="#61-添加依赖">6.1. 添加依赖</a><br>
    <a href="#62-主测试类">6.2. 主测试类</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#7-常用工具类">7. 常用工具类</a></summary>
  <ul>
    <a href="#71-snowflakeidworker分布式id生成器">7.1. SnowflakeIdWorker分布式ID生成器</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#8-配置文件">8. 配置文件</a></summary>
  <ul>
    <a href="#81-springboot启动springprofilesactive和springprofilesinclude影响的区别">8.1. springboot启动spring.profiles.active和spring.profiles.include影响的区别</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#9-markdown工具">9. Markdown工具</a></summary>
  <ul>
  <details open>
    <summary><a href="#91-自动生成可折叠目录">9.1. 自动生成可折叠目录</a>  </summary>
    <ul>
      <a href="#911-添加依赖">9.1.1. 添加依赖</a><br>
      <a href="#912-使用">9.1.2. 使用</a><br>
    </ul>
  </details>
  </ul>
</details>


<h1>Integrated</h1>

# 1. SpringBoot基础初始化

## 1.1. pom.xml基本信息

[pom.xml内容](./pom.xml)

```xml
<!-- modelVersion描述这个POM文件是遵从哪个版本的项目描述符 -->
<!-- modelVersion指定了当前POM模型的版本，对于Maven2及Maven3来说，它只能是4.0.0 -->
<modelVersion>4.0.0</modelVersion>
<!-- parent项目中不存放任何代码，只是管理多个项目之间公共的依赖 -->
<parent>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-parent</artifactId>
  <version>2.4.1</version>
  <relativePath/> <!-- lookup parent from repository -->
</parent>
<!-- 定义了项目属于哪个组，举个例子，如果你的公司是mycom，有一个项目为myapp，那么groupId就应该是com.mycom.myapp. -->
<groupId>com.yang</groupId>
<!-- 定义了当前maven项目在组中唯一的ID,比如，myapp-util,myapp-domain,myapp-web等 -->
<artifactId>integrated</artifactId>
<version>0.0.1-SNAPSHOT</version>
<name>integrated</name>
<description>Integrating a wide variety of tools and frameworks</description>
```

## 1.2. 基础启动类

[IntegratedApplication内容](./src/main/java/com/yang/integrated/IntegratedApplication.java)

# 2. SpringBoot打war包和jar包

## 2.1. 添加依赖

[pom.xml内容](./pom.xml)

```xml
<!--默认jar-->
<packaging>war</packaging>

<!-- SpringBoot Tomcat -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-tomcat</artifactId>
    <scope>provided</scope>
</dependency>

<build>
  <plugins>
    <!--构建工具-->
    <plugin>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-maven-plugin</artifactId>
    </plugin>
  </plugins>
</build>
```

## 2.2. 新增ServletInitializer类

[ServletInitializer内容](./src/main/java/com/yang/integrated/ServletInitializer.java)

## 2.3. 生成war包或jar

使用maven的package命令生成war包或jar，生成成功后，可在下面的路径中看到：

[生成war包或jar的路径，在本地可以看到，在远程上没有](./target/)

# 3. Swagger2

## 3.1. 添加依赖

[pom.xml内容](./pom.xml)

```xml
<!-- Swagger2 -->
<!-- https://mvnrepository.com/artifact/io.springfox/springfox-swagger2 -->
<dependency>
  <groupId>io.springfox</groupId>
  <artifactId>springfox-swagger2</artifactId>
  <version>2.9.2</version>
</dependency>
<!-- https://mvnrepository.com/artifact/io.springfox/springfox-swagger-ui -->
<dependency>
  <groupId>io.springfox</groupId>
  <artifactId>springfox-swagger-ui</artifactId>
  <version>2.9.2</version>
</dependency>
```

## 3.2. 自定义Swagger2通用配置类

[Swagger2配置类](./src/main/java/com/yang/integrated/config/Swagger2.java)

## 3.3. 自定义Swagger2通用配置properties

[application-swagger2.properties](./src/main/resources/application-swagger2.properties)

## 3.4. 引用Swagger2通用配置类

在主配置文件中配置如下的的内容

[application.properties](./src/main/resources/application.properties)

```properties
spring.profiles.include=swagger2
```

# 4. 数据库连接配置（MySQL）

## 4.1. 添加依赖

[pom.xml内容](./pom.xml)

```xml
<!-- MySQL -->
<dependency>
  <groupId>mysql</groupId>
  <artifactId>mysql-connector-java</artifactId>
  <scope>runtime</scope>
</dependency>

<!-- Druid -->
<!-- https://mvnrepository.com/artifact/com.alibaba/druid -->
<dependency>
  <groupId>com.alibaba</groupId>
  <artifactId>druid</artifactId>
  <version>1.1.23</version>
</dependency>
```

## 4.2. 通用MySQL数据库配置

可变参数配置文件，作用是控制可变数据，比如IP地址，账号密码等：[application-args.properties](./src/main/resources/application-args.properties)

通用MySQL数据库配置：[application-mysql.properties](./src/main/resources/application-mysql.properties)

## 4.3. 自动创建数据库配置类

[DataSourceConfig.java](./src/main/java/com/yang/integrated/config/DataSourceConfig.java)

## 4.4. 引用通用MySQL数据库配置

在主配置文件中配置如下的的内容

[application.properties](./src/main/resources/application.properties)

```properties
spring.profiles.include=mysql
```


# 5. 通用增删改查（JPA和MyBatis）

## 5.1. Controller，Mapper，Pojo，Service and Impl

### 5.1.1. Controller

[/src/main/java/com/yang/integrated/controller/common/BaseHibernateController.java](./src/main/java/com/yang/integrated/controller/common/BaseHibernateController.java)

[/src/main/java/com/yang/integrated/controller/common/BaseMyBatisController.java](./src/main/java/com/yang/integrated/controller/common/BaseMyBatisController.java)

### 5.1.2. Mapper

[/src/main/java/com/yang/integrated/mapper/BaseMyBatisMapper.java](./src/main/java/com/yang/integrated/mapper/BaseMyBatisMapper.java)

[/src/main/java/com/yang/integrated/mapper/MyBatisMapper.java](./src/main/java/com/yang/integrated/mapper/MyBatisMapper.java)

### 5.1.3. Pojo

[/src/main/java/com/yang/integrated/pojo/common/CommonPo.java](./src/main/java/com/yang/integrated/pojo/common/CommonPo.java)

### 5.1.4. Service and Impl

[/src/main/java/com/yang/integrated/service/impl/BaseHibernateServiceImpl.java](./src/main/java/com/yang/integrated/service/impl/BaseHibernateServiceImpl.java)

[/src/main/java/com/yang/integrated/service/impl/BaseMyBatisServiceImpl.java](./src/main/java/com/yang/integrated/service/impl/BaseMyBatisServiceImpl.java)

[/src/main/java/com/yang/integrated/service/BaseHibernateService.java](./src/main/java/com/yang/integrated/service/BaseHibernateService.java)

[/src/main/java/com/yang/integrated/service/BaseMyBatisService.java](./src/main/java/com/yang/integrated/service/BaseMyBatisService.java)

## 5.2. 编写增删改查实例

### 5.2.1. Pojo

[/src/main/java/com/yang/integrated/pojo/Brand.java](./src/main/java/com/yang/integrated/pojo/Brand.java)

### 5.2.2. Controller

[/src/main/java/com/yang/integrated/controller/BrandController.java](./src/main/java/com/yang/integrated/controller/BrandController.java)

## 5.3. 测试

启动项目后，访问下面的Swagger2在线接口文档网站，可以测试响应的接口。

[本地Swagger2地址](http://localhost:8080//swagger-ui.html)

# 6. Springboot测试类

## 6.1. 添加依赖

[pom.xml内容](./pom.xml)

```xml
<!-- SpringBoot Test -->
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-test</artifactId>
  <scope>test</scope>
</dependency>
```

## 6.2. 主测试类

[IntegratedApplicationTests.java](./src/test/java/com/yang/integrated/IntegratedApplicationTests.java)

# 7. 常用工具类

## 7.1. SnowflakeIdWorker分布式ID生成器

[Java/integrated/src/main/java/com/yang/integrated/utils/SnowflakeIdWorker.java](./src/main/java/com/yang/integrated/utils/SnowflakeIdWorker.java)

# 8. 配置文件

## 8.1. springboot启动spring.profiles.active和spring.profiles.include影响的区别

多个配置文件中有同一个值，以下情况获取值的效果： 

1. 启动命令不带--spring.profiles.active参数以application.properties首先启动。按顺序所有文件第一个配置的spring.profiles.active属性中指定的最后一个文件中含有该属性的值为准，如果所有文件都没有spring.profiles.active，那么以spring.profiles.include配置的最后一个属性文件中的值为准。
2. 启动命令带--spring.profiles.active参数以参数指定的属性文件首先启动。此情况，已命令指定的配置文件中的值为准，其他文件中再配置spring.profiles.active也不会生效，如果不存在值，那么会以spring.profiles.include指定的最后一个文件中的值为准。

简要说，启动命令spring.profiles.active指定文件中的值 **>** 文件中spring.profiles.active指定的文件列表中最后一次出现的值 **>** 文件中spring.profiles.include指定的文件列表中最后一次出现的值。

**注意：无论是否配置启动命令参数指定文件，最后都会加载application.properties，它里边配置的信息也很关键。**

# 9. Markdown工具

## 9.1. 自动生成可折叠目录

### 9.1.1. 添加依赖

[pom.xml内容](./pom.xml)

```xml
<!-- markdown-toc -->
<dependency>
  <groupId>com.github.houbb</groupId>
  <artifactId>markdown-toc</artifactId>
  <version>1.0.2</version>
</dependency>
```

### 9.1.2. 使用

修改下面类中的主测试方法的文件路径，需要生成目录的markdown文件。配合着自动成成多级目录更完美。

[MarkdownUtil.java](./src/main/java/com/yang/integrated/utils/MarkdownUtil.java)





