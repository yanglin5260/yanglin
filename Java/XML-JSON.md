<h2>目录</h2>

<details open>
  <summary><a href="#1-xml和json">1. XML和JSON</a></summary>
  <ul>
  <details open>
    <summary><a href="#11-xml简介">1.1. XML简介</a>  </summary>
    <ul>
      <a href="#111-xml的结构">1.1.1. XML的结构</a><br>
      <a href="#112-小结">1.1.2. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#12-使用dom">1.2. 使用DOM</a>  </summary>
    <ul>
      <a href="#121-小结">1.2.1. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#13-使用sax">1.3. 使用SAX</a>  </summary>
    <ul>
      <a href="#131-小结">1.3.1. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#14-使用jackson">1.4. 使用Jackson</a>  </summary>
    <ul>
      <a href="#141-小结">1.4.1. 小结</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#15-使用json">1.5. 使用JSON</a>  </summary>
    <ul>
      <a href="#151-小结">1.5.1. 小结</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#2-面试题">2. 面试题</a></summary>
  <ul>
    <a href="#21-xml-包括哪些解释技术，区别是什么？">2.1. XML 包括哪些解释技术，区别是什么？</a><br>
    <a href="#22-xml文档定义有几种形式？它们之间有何本质区别？解析xml-文档有哪几种方式？">2.2. XML文档定义有几种形式？它们之间有何本质区别？解析XML 文档有哪几种方式？</a><br>
    <a href="#23-你在项目中哪些地方用到了xml？">2.3. 你在项目中哪些地方用到了XML？</a><br>
    <a href="#24-谈谈对xml的理解？说明web应用中webxml文件的作用？">2.4. 谈谈对XML的理解？说明Web应用中Web.xml文件的作用？</a><br>
    <a href="#25-dtd-与-xml-schema都是xml文档。选择1项">2.5. DTD 与 XML Schema都是XML文档。(选择1项)</a><br>
  </ul>
</details>


<h1>XML和JSON</h1>

# 1. XML和JSON

## 1.1. XML简介

XML是可扩展标记语言（eXtensible Markup Language）的缩写，它是是一种数据表示格式，可以描述非常复杂的数据结构，常用于传输和存储数据。

XML有几个特点：一是纯文本，默认使用UTF-8编码，二是可嵌套，适合表示结构化数据。如果把XML内容存为文件，那么它就是一个XML文件。此外，XML内容经常通过网络作为消息传输。

### 1.1.1. XML的结构

XML有固定的结构，首行必定是`<?xml version="1.0"?>`，可以加上可选的编码。紧接着，如果以类似`<!DOCTYPE note SYSTEM "book.dtd">`声明的是文档定义类型（DTD：Document Type Definition），DTD是可选的。接下来是XML的文档内容，一个XML文档有且仅有一个根元素，根元素可以包含任意个子元素，元素可以包含属性。如果是空元素，可以用`<tag/>`表示。

由于使用了`<`、`>`以及引号等标识符，如果内容出现了特殊符号，需要使用`&???;`表示转义。

常见的特殊字符如下：

| 字符 | 表示   |
| :--- | :----- |
| <    | &lt;   |
| >    | &gt;   |
| &    | &amp;  |
| "    | &quot; |
| '    | &apos; |

格式正确的XML（Well Formed）是指XML的格式是正确的，可以被解析器正常读取。而合法的XML是指，不但XML格式正确，而且它的数据结构可以被DTD或者XSD验证。

### 1.1.2. 小结

XML使用嵌套结构的数据表示方式，支持格式验证；

XML常用于配置文件、网络消息传输等。

## 1.2. 使用DOM

因为XML是一种树形结构的文档，它有两种标准的解析API：

- DOM：一次性读取XML，并在内存中表示为树形结构；
- SAX：以流的形式读取XML，使用事件回调。

DOM是Document Object Model的缩写，DOM模型就是把XML结构作为一个树形结构处理，从根节点开始，每个节点都可以包含任意个子节点。

我们以下面的XML为例：

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<book id="1">
    <name>Java核心技术</name>
    <author>Cay S. Horstmann</author>
    <isbn lang="CN">1234567</isbn>
    <tags>
        <tag>Java</tag>
        <tag>Network</tag>
    </tags>
    <pubDate/>
</book>
```

如果解析为DOM结构，它大概长这样：

```ascii
                      ┌─────────┐
                      │document │
                      └─────────┘
                           │
                           ▼
                      ┌─────────┐
                      │  book   │
                      └─────────┘
                           │
     ┌──────────┬──────────┼──────────┬──────────┐
     ▼          ▼          ▼          ▼          ▼
┌─────────┐┌─────────┐┌─────────┐┌─────────┐┌─────────┐
│  name   ││ author  ││  isbn   ││  tags   ││ pubDate │
└─────────┘└─────────┘└─────────┘└─────────┘└─────────┘
                                      │
                                 ┌────┴────┐
                                 ▼         ▼
                             ┌───────┐ ┌───────┐
                             │  tag  │ │  tag  │
                             └───────┘ └───────┘
```

注意到最顶层的document代表XML文档，它是真正的“根”，而`<book>`虽然是根元素，但它是`document`的一个子节点。

Java提供了DOM API来解析XML，它使用下面的对象来表示XML的内容：

- Document：代表整个XML文档；
- Element：代表一个XML元素；
- Attribute：代表一个元素的某个属性。

使用DOM API解析一个XML文档的代码如下：

```java
InputStream input = Main.class.getResourceAsStream("/book.xml");
DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
DocumentBuilder db = dbf.newDocumentBuilder();
Document doc = db.parse(input);
```

`DocumentBuilder.parse()`用于解析一个XML，它可以接收InputStream，File或者URL，如果解析无误，我们将获得一个Document对象，这个对象代表了整个XML文档的树形结构，需要遍历以便读取指定元素的值。

对于DOM API解析出来的结构，我们从根节点Document出发，可以遍历所有子节点，获取所有元素、属性、文本数据，还可以包括注释，这些节点被统称为Node，每个Node都有自己的Type，根据Type来区分一个Node到底是元素，还是属性，还是文本，等等。

### 1.2.1. 小结

- Java提供的DOM API可以将XML解析为DOM结构，以Document对象表示；
- DOM可在内存中完整表示XML数据结构；
- DOM解析速度慢，内存占用大。

## 1.3. 使用SAX

使用DOM解析XML的优点是用起来省事，但它的主要缺点是内存占用太大。

另一种解析XML的方式是SAX。SAX是Simple API for XML的缩写，它是一种基于流的解析方式，边读取XML边解析，并以事件回调的方式让调用者获取数据。因为是一边读一边解析，所以无论XML有多大，占用的内存都很小。

SAX解析会触发一系列事件：

- startDocument：开始读取XML文档；
- startElement：读取到了一个元素，例如`<book>`；
- characters：读取到了字符；
- endElement：读取到了一个结束的元素，例如`</book>`；
- endDocument：读取XML文档结束。

如果我们用SAX API解析XML，Java代码如下：

```java
InputStream input = Main.class.getResourceAsStream("/book.xml");
SAXParserFactory spf = SAXParserFactory.newInstance();
SAXParser saxParser = spf.newSAXParser();
saxParser.parse(input, new MyHandler());
```

如果要读取`<name>`节点的文本，我们就必须在解析过程中根据`startElement()`和`endElement()`定位当前正在读取的节点，可以使用栈结构保存，每遇到一个`startElement()`入栈，每遇到一个`endElement()`出栈，这样，读到`characters()`时我们才知道当前读取的文本是哪个节点的。

### 1.3.1. 小结

- SAX是一种流式解析XML的API；
- SAX通过事件触发，读取速度快，消耗内存少；
- 调用方必须通过回调方法获得解析过程中的数据。

## 1.4. 使用Jackson

前面我们介绍了DOM和SAX两种解析XML的标准接口。但是，无论是DOM还是SAX，使用起来都不直观。

一个名叫Jackson的开源的第三方库可以轻松做到XML到JavaBean的转换。

如果要解析的数据格式不是Jackson内置的标准格式，那么需要编写一点额外的扩展来告诉Jackson如何自定义解析。

### 1.4.1. 小结

使用Jackson解析XML，可以直接把XML解析为JavaBean，十分方便。

## 1.5. 使用JSON

JSON是JavaScript Object Notation的缩写，它去除了所有JavaScript执行代码，只保留JavaScript的对象格式。

JSON作为数据传输的格式，有几个显著的优点：

- JSON只允许使用UTF-8编码，不存在编码问题；
- JSON只允许使用双引号作为key，特殊字符用`\`转义，格式简单；
- 浏览器内置JSON支持，如果把数据用JSON发送给浏览器，可以用JavaScript直接处理。

因此，JSON适合表示层次结构，因为它格式简单，仅支持以下几种数据类型：

- 键值对：`{"key": value}`
- 数组：`[1, 2, 3]`
- 字符串：`"abc"`
- 数值（整数和浮点数）：`12.34`
- 布尔值：`true`或`false`
- 空值：`null`

浏览器直接支持使用JavaScript对JSON进行读写：

```javascript
// JSON string to JavaScript object:
jsObj = JSON.parse(jsonStr);

// JavaScript object to JSON string:
jsonStr = JSON.stringify(jsObj);
```

所以，开发Web应用的时候，使用JSON作为数据传输，在浏览器端非常方便。因为JSON天生适合JavaScript处理，所以，绝大多数REST API都选择JSON作为数据传输格式。

在Java中，针对JSON也有标准的JSR 353 API，但是我们在前面讲XML的时候发现，如果能直接在XML和JavaBean之间互相转换是最好的。类似的，如果能直接在JSON和JavaBean之间转换，那么用起来就简单多了。

常用的用于解析JSON的第三方库有：

- Jackson
- Gson
- Fastjson
- ...

### 1.5.1. 小结

- JSON是轻量级的数据表示方式，常用于Web应用；

- Jackson可以实现JavaBean和JSON之间的转换；

- 可以通过Module扩展Jackson能处理的数据类型；

- 可以自定义`JsonSerializer`和`JsonDeserializer`来定制序列化和反序列化。

# 2. 面试题

## 2.1. XML 包括哪些解释技术，区别是什么？

包括：DOM（Document Object Modal）文档对象模型，SAX（Simple API for XML）。

DOM 是一次性将整个文档读入内存操作，如果是文档比较小，读入内存，可以极大提高操作的速度，但如果文档比较大，那么这个就吃力了。

所以此时 SAX 应用而生，它不是一次性的将整个文档读入内存，这对于处理大型文档就比较省力了。

## 2.2. XML文档定义有几种形式？它们之间有何本质区别？解析XML 文档有哪几种方式？

答：XML文档定义分为 DTD和Schema两种形式；其本质区别在于Schema本身也是一个XML文件，可以被XML 解析器解析。

对XML的解析主要有DOM（文档对象模型）、SAX、StAX（JDK 1.6中引入的新的解析 XML 的方式，Streaming API for XML）等，

**DOM**处理大型文件时其性能下降的非常厉害，这个问题是由 DOM 的树结构所造成的，这种结构占用的内存较多，而且DOM 必须在解析文件之前把整个文档装入内存，适合对 XML 的随机访问（典型的用空间换取时间的策略）；

**SAX**是事件驱动型的XML 解析方式，它顺序读取 XML 文件，不需要一次全部装载整个文件。当遇到像文件开头，文档结束，或者标签开头与标签结束时，它会触发一个事件，用户通过在其回调事件中写入处理代码来处理 XML 文件，适合对 XML 的顺序访问；如其名称所暗示的那样，StAX把重点放在流上。实际上，StAX与其他方法的区别就在于应用程序能够把XML作为一个事件流来处理。将XML作为一组事件来处理的想法并不新颖（事实上SAX已经提出来了），但不同之处在于StAX允许应用程序代码把这些事件逐个拉出来，而不用提供在解析器方便时从解析器中接收事件的处理程序。

## 2.3. 你在项目中哪些地方用到了XML？

XML 的主要作用有两个方面：

- 数据交换：在做数据交换时，XML将数据用标签组装成起来，然后压缩打包加密后通过网络传送给接收者，接收解密与解压缩后再从 XML 文件中还原相关信息进行处理。
- 信息配置：目前很多软件都使用 XML 来存储配置信息，很多项目中我们通常也会将作为配置的硬代码（hard code）写在 XML 文件中，Java 的很多框架也是这么做的。

## 2.4. 谈谈对XML的理解？说明Web应用中Web.xml文件的作用？

解答：XML（Extensible Markup Language）即可扩展标记语言，它与 HTML 一样，都是 SGML(Standard Generalized Markup Language，标准通用标记语言)。XML是Internet环境中跨平台的，依赖于内容的技术，是当前处理结构化文档信息的有力工具。扩展标记语言XML是一种简单的数据存储语言，使用一系列简单的标记描述数据，而这些标记可以用方便的方式建立，虽然XML占用的空间比二进制数据要占用更多的空间，但XML极其简单易于掌握和使用。

web.xml 的作用是配置欢迎页，servlet，filter，listener等的。

## 2.5. DTD 与 XML Schema都是XML文档。(选择1项)

A．正确

B．不正确

解答：DTD 不是XML文件，但是可以嵌入在XML文档中。schema是XML文档




