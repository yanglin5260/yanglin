<h2>目录</h2>

<details open>
  <summary><a href="#1-spring源码解析---spring容器加载源码beandefinition封装和注册过程">1. spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)</a></summary>
  <ul>
    <a href="#11-前言">1.1. 前言</a><br>
    <a href="#12-容器加载流程">1.2. 容器加载流程</a><br>
  <details open>
    <summary><a href="#13-xml配置解析">1.3. xml配置解析</a>  </summary>
    <ul>
      <a href="#131-容器入口">1.3.1. 容器入口</a><br>
      <a href="#132-容器上下文刷新refresh入口">1.3.2. 容器上下文刷新refresh()入口</a><br>
      <a href="#133-abstractrefreshableapplicationcontextrefreshbeanfactory模板方法">1.3.3. AbstractRefreshableApplicationContext.refreshBeanFactory()模板方法</a><br>
      <a href="#134-abstractxmlapplicationcontextloadbeandefinitions解析配置核心方法">1.3.4. AbstractXmlApplicationContext.loadBeanDefinitions()解析配置核心方法</a><br>
      <a href="#135-将配置资源封装成inputstream流对象">1.3.5. 将配置资源封装成InputStream流对象</a><br>
      <a href="#136-beandefinitiondocumentreaderregisterbeandefinitions解析和注册beandefinition入口方法">1.3.6. BeanDefinitionDocumentReader.registerBeanDefinitions()解析和注册beandefinition入口方法</a><br>
      <a href="#137-defaultbeandefinitiondocumentreaderparsebeandefinitions核心解析方法">1.3.7. DefaultBeanDefinitionDocumentReader.parseBeanDefinitions()核心解析方法</a><br>
      <a href="#138-defaultbeandefinitiondocumentreaderparsedefaultelement默认标签解析过程">1.3.8. DefaultBeanDefinitionDocumentReader.parseDefaultElement()默认标签解析过程</a><br>
      <a href="#139-解析中间结果包装beandefinitionholder包装对象">1.3.9. 解析中间结果包装BeanDefinitionHolder包装对象</a><br>
      <a href="#1310-beandefinitionparserdelegateparsebeandefinitionelement解析bean标签的各种属性和子元素">1.3.10. BeanDefinitionParserDelegate.parseBeanDefinitionElement()解析bean标签的各种属性和子元素</a><br>
      <a href="#1311-通用beandefinition数据结构">1.3.11. 通用beandefinition数据结构</a><br>
      <a href="#1312-beandefinitionparserdelegatedecoratebeandefinitionifrequired装饰beandefinition其它功能">1.3.12. BeanDefinitionParserDelegate.decorateBeanDefinitionIfRequired()装饰beandefinition其它功能</a><br>
      <a href="#1313-这种装饰器模式在自定义标签解析有使用beandefinitionreaderutilsregisterbeandefinition注册beandefinition工具类方法">1.3.13. 这种装饰器模式在自定义标签解析有使用BeanDefinitionReaderUtils.registerBeanDefinition()注册beandefinition工具类方法</a><br>
      <a href="#1314-beandefinition注册的目标接口beanfactory---defaultlistablebeanfactory对象">1.3.14. BeanDefinition注册的目标接口BeanFactory--->DefaultListableBeanFactory对象</a><br>
      <a href="#1315-beandefinitionparserdelegateparsecustomelement解析自定义标签">1.3.15. BeanDefinitionParserDelegate.parseCustomElement()解析自定义标签</a><br>
      <a href="#1316-以上的handlers的解析对于spring来源说有很多，我们也可以自定义标签对象，以内置的context标签为例，包含以下部分">1.3.16. 以上的handlers的解析对于spring来源说有很多，我们也可以自定义标签对象，以内置的context标签为例，包含以下部分</a><br>
      <a href="#1317-componentscanbeandefinitionparserparse解析contextcomponent-scan标签解析过程">1.3.17. ComponentScanBeanDefinitionParser.parse()解析\<context:component-scan/>标签解析过程</a><br>
      <a href="#1318-创建classpathbeandefinitionscanner扫描器">1.3.18. 创建ClassPathBeanDefinitionScanner扫描器</a><br>
      <a href="#1319-classpathscanningcandidatecomponentproviderregisterdefaultfilters注册默认的注解类型过滤器">1.3.19. ClassPathScanningCandidateComponentProvider.registerDefaultFilters()注册默认的注解类型过滤器</a><br>
      <a href="#1320-classpathbeandefinitionscannerdoscan包class文件扫描">1.3.20. ClassPathBeanDefinitionScanner.doScan()包class文件扫描</a><br>
      <a href="#1321-classpathscanningcandidatecomponentproviderfindcandidatecomponents查找符合条件class的过程">1.3.21. ClassPathScanningCandidateComponentProvider.findCandidateComponents()查找符合条件class的过程</a><br>
      <a href="#1322-componentscanbeandefinitionparserregistercomponents注册核心三大组件">1.3.22. ComponentScanBeanDefinitionParser.registerComponents()注册核心三大组件</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#14-注解扫描">1.4. 注解扫描</a>  </summary>
    <ul>
      <a href="#141-annotationconfigapplicationcontext构造函数注解扫描入口">1.4.1. AnnotationConfigApplicationContext构造函数注解扫描入口</a><br>
      <a href="#142-annotationconfigutilsregisterannotationconfigprocessors注册三大组件，过程同xml解析中组件注册相同">1.4.2. AnnotationConfigUtils.registerAnnotationConfigProcessors()注册三大组件，过程同xml解析中组件注册相同</a><br>
      <a href="#143-classpathbeandefinitionscanner扫描器注册和namespaceuri处理器">1.4.3. ClassPathBeanDefinitionScanner扫描器注册和NamespaceUri处理器</a><br>
      <a href="#144-classpathbeandefinitionscannerscan执行包扫描">1.4.4. ClassPathBeanDefinitionScanner.scan()执行包扫描</a><br>
      <a href="#145-abstractapplicationcontextrefresh容器上下文刷新-同xml配置解析加载类似">1.4.5. AbstractApplicationContext.refresh()容器上下文刷新, 同xml配置解析加载类似</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#15-调用时序">1.5. 调用时序</a>  </summary>
    <ul>
      <a href="#151-xml配置解析和注册beandefinition调用时序">1.5.1. xml配置解析和注册beandefinition调用时序</a><br>
      <a href="#152-注解扫描和注册beandefinition调用时序">1.5.2. 注解扫描和注册beandefinition调用时序</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#2-spring源码解析---spring容器加载源码bean实例化过程">2. spring源码解析 - spring容器加载源码(bean实例化过程)</a></summary>
  <ul>
    <a href="#21-前言">2.1. 前言</a><br>
  <details open>
    <summary><a href="#22-容器加载流程">2.2. 容器加载流程</a>  </summary>
    <ul>
      <a href="#221-abstractapplicationcontextfinishbeanfactoryinitialization实例化bean入口方法">2.2.1. AbstractApplicationContext.finishBeanFactoryInitialization()实例化bean入口方法</a><br>
      <a href="#222-defaultlistablebeanfactorypreinstantiatesingletons开始实例化流程">2.2.2. DefaultListableBeanFactory.preInstantiateSingletons()开始实例化流程</a><br>
      <a href="#223-abstractbeanfactorydogetbean抽象实例化bean骨架方法">2.2.3. AbstractBeanFactory.doGetBean()抽象实例化bean骨架方法</a><br>
      <a href="#224-defaultsingletonbeanregistrygetsingleton获取三个级别缓存的流程">2.2.4. DefaultSingletonBeanRegistry.getSingleton()获取三个级别缓存的流程</a><br>
      <a href="#225-返回factorybean中定义的对象实例getobjectforbeaninstance">2.2.5. 返回factorybean中定义的对象实例,getObjectForBeanInstance()</a><br>
      <a href="#226-多例情况scopeprototype下，有循环依赖则直接抛出异常">2.2.6. 多例情况(Scope=ProtoType)下，有循环依赖则直接抛出异常</a><br>
      <a href="#227-检测是否抽象beandefinition对象，则直接抛出异常">2.2.7. 检测是否抽象beandefinition对象，则直接抛出异常</a><br>
      <a href="#228-缓存依赖的depend的bean对象">2.2.8. 缓存依赖的depend的bean对象</a><br>
      <a href="#229-创建并缓存单例bean实例">2.2.9. 创建并缓存单例bean实例</a><br>
      <a href="#2210-创建bean实例createbean核心封装">2.2.10. 创建bean实例createBean()核心封装</a><br>
      <a href="#2211-反射创建bean实例">2.2.11. 反射创建bean实例</a><br>
      <a href="#2212-bean的依赖对象ioc注入：abstractautowirecapablebeanfactorypopulatebean">2.2.12. bean的依赖对象IOC注入：AbstractAutowireCapableBeanFactory.populateBean()</a><br>
      <a href="#2213-bean实例化后置处理，主要是aop动态代理的创建，这个后面会详细展开分享：initializebean">2.2.13. bean实例化后置处理，主要是AOP动态代理的创建，这个后面会详细展开分享：initializeBean()</a><br>
      <a href="#2214-bean实例销毁注册，供外部servlet容器调用：registerdisposablebeanifnecessary">2.2.14. bean实例销毁注册，供外部servlet容器调用：registerDisposableBeanIfNecessary()</a><br>
    </ul>
  </details>
    <a href="#23-调用时序">2.3. 调用时序</a><br>
    <a href="#24-总结">2.4. 总结</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#3-spring源码解析---spring缓存和循环依赖问题">3. spring源码解析 - spring缓存和循环依赖问题</a></summary>
  <ul>
    <a href="#31-前言">3.1. 前言</a><br>
  <details open>
    <summary><a href="#32-缓存场景">3.2. 缓存场景</a>  </summary>
    <ul>
      <a href="#321-场景1、首次getbean时">3.2.1. 场景1、首次getBean()时</a><br>
    <details open>
      <summary><a href="#322-场景2、首次createbean时">3.2.2. 场景2、首次createBean()时</a>    </summary>
      <ul>
        <a href="#3221-在docreatebean过程中，若bean设置可提前暴露（默认开启），则会创建三级缓存beanfactory对象">3.2.2.1. 在doCreateBean()过程中，若bean设置可提前暴露（默认开启），则会创建三级缓存(beanFactory对象)</a><br>
      </ul>
    </details>
    </ul>
  </details>
    <a href="#33-循环依赖">3.3. 循环依赖</a><br>
    <a href="#34-解决方案">3.4. 解决方案</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#4-spring源码解析---spring后置处理器埋点设计">4. spring源码解析 - spring后置处理器埋点设计</a></summary>
  <ul>
    <a href="#41-前言">4.1. 前言</a><br>
  <details open>
    <summary><a href="#42-spring中的后置处理器">4.2. Spring中的后置处理器</a>  </summary>
    <ul>
      <a href="#421-beandefinition注册阶段">4.2.1. BeanDefinition注册阶段</a><br>
      <a href="#422-触发beandefinition干预阶段">4.2.2. 触发BeanDefinition干预阶段</a><br>
      <a href="#423-bean实例化前置阶段">4.2.3. Bean实例化前置阶段</a><br>
      <a href="#424-bean实例化后置阶段">4.2.4. Bean实例化后置阶段</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#43-后置处理器的应">4.3. 后置处理器的应</a>  </summary>
    <ul>
      <a href="#431-beandefinitionregistrypostprocessor扩展">4.3.1. BeanDefinitionRegistryPostProcessor扩展</a><br>
      <a href="#432-beanfactorypostprocesser扩展">4.3.2. BeanFactoryPostProcesser扩展</a><br>
      <a href="#433-initializingbeanbean实例化扩展">4.3.3. InitializingBean,Bean实例化扩展</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#5-spring源码解析---aop相关源码分析">5. spring源码解析 - aop相关源码分析</a></summary>
  <ul>
    <a href="#51-前言">5.1. 前言</a><br>
  <details open>
    <summary><a href="#52-aop的基本要素">5.2. AOP的基本要素</a>  </summary>
    <ul>
      <a href="#521-切面（aspect）-----spring中叫advisor">5.2.1. 切面（Aspect）-----Spring中叫Advisor</a><br>
      <a href="#522-切点（pointcut）-----一类joinpoint的集合">5.2.2. 切点（Pointcut）-----一类JoinPoint的集合</a><br>
      <a href="#523-增强（advice）-----目标类方法的代理方法实现">5.2.3. 增强（Advice）-----目标类方法的代理方法实现</a><br>
      <a href="#524-连接点（joinpoint）-----被拦截的某个目标方法">5.2.4. 连接点（Joinpoint）-----被拦截的某个目标方法</a><br>
      <a href="#525-目标对象（target）">5.2.5. 目标对象（Target）</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#53-springaop的整体架构">5.3. SpringAOP的整体架构</a>  </summary>
    <ul>
      <a href="#531-abstractautoproxycreator">5.3.1. AbstractAutoProxyCreator</a><br>
      <a href="#532-advisor切面设计部分">5.3.2. Advisor切面设计部分</a><br>
      <a href="#533-aopproxy动态代理部分">5.3.3. AopProxy动态代理部分</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#54-springaop的源码实现">5.4. SpringAOP的源码实现</a>  </summary>
    <ul>
      <a href="#541-aop源码入口">5.4.1. AOP源码入口</a><br>
      <a href="#542-通过advisor的准备，对当前bean创建动态拦截代理类">5.4.2. 通过Advisor的准备，对当前bean创建动态拦截代理类</a><br>
      <a href="#543-abstractautoproxycreatorbuildadvisors封装最终的各种切面对象，为后续的链式调用做准备">5.4.3. AbstractAutoProxyCreator.buildAdvisors()封装最终的各种切面对象，为后续的链式调用做准备</a><br>
    <details open>
      <summary><a href="#544-动态代理对象生成">5.4.4. 动态代理对象生成</a>    </summary>
      <ul>
        <a href="#5441-jdk内置代理创建">5.4.4.1. JDK内置代理创建</a><br>
        <a href="#5442-cglib动态代理创建">5.4.4.2. Cglib动态代理创建</a><br>
      </ul>
    </details>
      <a href="#545-核心增强业务实现">5.4.5. 核心增强业务实现</a><br>
    </ul>
  </details>
    <a href="#55-总结">5.5. 总结</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#6-spring源码解析---spring事务相关源码分析">6. Spring源码解析 - spring事务相关源码分析</a></summary>
  <ul>
    <a href="#61-前言">6.1. 前言</a><br>
    <a href="#62-注解事务运行流程">6.2. 注解事务运行流程</a><br>
  <details open>
    <summary><a href="#63-核心对象关系">6.3. 核心对象关系</a>  </summary>
    <ul>
      <a href="#631-事务配置相关">6.3.1. 事务配置相关</a><br>
      <a href="#632-事务运行拦截相关">6.3.2. 事务运行拦截相关</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#64-源码实现">6.4. 源码实现</a>  </summary>
    <ul>
      <a href="#641-事务配置">6.4.1. 事务配置</a><br>
      <a href="#642-事务创建">6.4.2. 事务创建</a><br>
      <a href="#643-事务回滚">6.4.3. 事务回滚</a><br>
      <a href="#644-事务提交">6.4.4. 事务提交</a><br>
    </ul>
  </details>
    <a href="#65-总结">6.5. 总结</a><br>
  </ul>
</details>


<h1>Spring源码解析</h1>

# 1. spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)

## 1.1. 前言

一直听说spring对java进行了重定义，设计和封装体系比较宏大；加上最近遇到了spring的问题，为了更好地定位问题，最近一段啃了一下spring源码。我用的源码版本是5.2.28，下面就把最近的研究成果做一下分享。

我的源码阅读目标主要是IOC和AOP这两块，先来看看spring容器加载这块
![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/10bbe756eaff51f80a55391715c9881d.png)
我们重点来分析一下前两种容器加载方式。至于嵌入式容器加载方式，后续接触到SpringBoot时再介绍。

虽然现在spring boot大行其道，但就底层的实现来讲很多都是基于springframework来实现的。所以我们由必要分析一下通过xml方式加载容器方式。注解方式容器加载方式作为当前主流同样也是分析的重点（其实它们在执行流程上有很多相同的处理环节）。
先来整体看看这两种方式的容器加载流程

## 1.2. 容器加载流程

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/3c1b8f4333fa6205167f07a691a9b83f.png)
![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/7c3bf1c5b153b5b7b4c14f1013d0aaaf.png)
注：流程中标记的环节是接下来要陆续分析的重点

从以上两种方式容器加载的流程我们可以看到，从整体上讲，xml配置解析方式只比注解扫描方式多了一个xml解析的环节，解析的结果最终还是要封装和注册beandefinition对象。另外注解扫描方式会把三大组件的注册、包的扫描环节提前完成。OK, 接下来我们一个一个来看。

## 1.3. xml配置解析

### 1.3.1. 容器入口

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/b59c755030fb5799d292c1373e77c65b.png)

### 1.3.2. 容器上下文刷新refresh()入口

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/7bca0366bba198b3b5f9b6472a9b8560.png)

### 1.3.3. AbstractRefreshableApplicationContext.refreshBeanFactory()模板方法

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/4f25dbc194e312fc7c35410033648493.png)
![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/b17cb86888994afa7eea17ae8cf5095c.png)

### 1.3.4. AbstractXmlApplicationContext.loadBeanDefinitions()解析配置核心方法

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/7f23cf5a25d4c27d547d76ebf4b0ed2a.png)
![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/e2a6c1904fa149afcc519a28ba536f24.png)
![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/be3410bae06ab6d51804517a1f5f4832.png)

### 1.3.5. 将配置资源封装成InputStream流对象

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/3669dcbe55c5597112ae401a30b7a30b.png)
![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/06faca877b48af8eb52d8fb90be17575.png)
![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/b810452457fc7b077d1948cceb5935f2.png)
![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/85f1366a9d7595a98dd767be5e9dee8a.png)

### 1.3.6. BeanDefinitionDocumentReader.registerBeanDefinitions()解析和注册beandefinition入口方法

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/ec5984f72fae8596e8078f4e2c3e1851.png)
![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/df4f5984b0fe9eef0039fcbb14ff9aa6.png)
![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/88e7d765d5a6b6dcd40a5dfc8f252aba.png)
![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/a678a923254cb7c482f063403da2bc64.png)

### 1.3.7. DefaultBeanDefinitionDocumentReader.parseBeanDefinitions()核心解析方法

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/8297635e33b31fa75c607ffa69db2ff3.png)

### 1.3.8. DefaultBeanDefinitionDocumentReader.parseDefaultElement()默认标签解析过程

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/c035d5ed8a1816c682b2fbb1a5153dde.png)

### 1.3.9. 解析中间结果包装BeanDefinitionHolder包装对象

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/bce5fa5323688a2dac32dbf275846f2d.png)
![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/21890d7afcfd7e2dd07f404774d3602c.png)

### 1.3.10. BeanDefinitionParserDelegate.parseBeanDefinitionElement()解析bean标签的各种属性和子元素

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/16c5eb6f7ebc8fda03bac7b7f44575d0.png)

### 1.3.11. 通用beandefinition数据结构

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/a7c4deaa30edabc98b936daf9431c496.png)

### 1.3.12. BeanDefinitionParserDelegate.decorateBeanDefinitionIfRequired()装饰beandefinition其它功能

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/767aa3d67e7fe5ece2bf54ddc7c6b562.png)
![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/1e0415b5622abeb917d002ff1a2b9a20.png)

### 1.3.13. 这种装饰器模式在自定义标签解析有使用BeanDefinitionReaderUtils.registerBeanDefinition()注册beandefinition工具类方法

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/a452c29d32de439b51de3c46ea3d7583.png)

### 1.3.14. BeanDefinition注册的目标接口BeanFactory--->DefaultListableBeanFactory对象

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/939c5ab66d6839cc1d8cdb00b4d7a865.png)

### 1.3.15. BeanDefinitionParserDelegate.parseCustomElement()解析自定义标签

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/0c798dea662e6011faad06a8df58b6f9.png)

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/d5cba219c61e90ffa7fe1517a3ac4429.png)

### 1.3.16. 以上的handlers的解析对于spring来源说有很多，我们也可以自定义标签对象，以内置的context标签为例，包含以下部分

 ![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/adfec8d449d7bb233a4b0fa35ddeab7d.png)

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/16d5eefc444e74981d167a87094c2f3c.png)
![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/f4a766e52de672a7c761f3aea14d6380.png)

### 1.3.17. ComponentScanBeanDefinitionParser.parse()解析\<context:component-scan/>标签解析过程

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/5092783cbd861f57109beb1356dbf4c1.png)

### 1.3.18. 创建ClassPathBeanDefinitionScanner扫描器

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/0bc9642dfbc944624936da5bf96ab1be.png)

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/2d28e58f9f5af6fc8f85b969afc46c8a.png)

### 1.3.19. ClassPathScanningCandidateComponentProvider.registerDefaultFilters()注册默认的注解类型过滤器

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/aed5fa3aa11ef4eafb930ba76d761662.png)

### 1.3.20. ClassPathBeanDefinitionScanner.doScan()包class文件扫描

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/efcb2b2cb217697f33a08e2a8b20de76.png)

### 1.3.21. ClassPathScanningCandidateComponentProvider.findCandidateComponents()查找符合条件class的过程

<img src="https://s4.51cto.com/images/blog/202009/12/2cceaec66cf067bee5ae63f22c918fec.png" alt="spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)" style="zoom:80%;" />

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/a5beb9c8b2020ba86dd8446f9e5b3ad0.png)
![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/28e842db6f12b89576b380f497cbf2ab.png)
![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/fe9074e96b0635066df15a590f15889c.png)

### 1.3.22. ComponentScanBeanDefinitionParser.registerComponents()注册核心三大组件

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/981e0e898a3b75c0281ce5466e33244f.png)

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/ecb72e5365fd99c0160365544cbc7413.png)

## 1.4. 注解扫描

### 1.4.1. AnnotationConfigApplicationContext构造函数注解扫描入口

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/c516939e679440e83f0b76350aaab14a.png)
![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/475c4fd6d535f60d739e1599baac222b.png)

### 1.4.2. AnnotationConfigUtils.registerAnnotationConfigProcessors()注册三大组件，过程同xml解析中组件注册相同

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/d4f27d039c97f27d340021845629065d.png)

### 1.4.3. ClassPathBeanDefinitionScanner扫描器注册和NamespaceUri处理器

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/3ada9026d374c75e95fed3ec921a7607.png)

### 1.4.4. ClassPathBeanDefinitionScanner.scan()执行包扫描

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/7a43002d5374a616cb39a7fe10fdfb57.png)

### 1.4.5. AbstractApplicationContext.refresh()容器上下文刷新, 同xml配置解析加载类似

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/5970c5b9d625e501b32af24bc30c9ed7.png)
![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/8decd8f32677a799af4151673a47167f.png)
![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/12b2ef0ab029d18555797be76da01d9b.png)

注解扫描重写GenericApplicationContext.refreshBeanFactory()方法(没有重要实现)；

xml配置解析重写的是：AbstractRefreshableApplicationContext.refreshBeanFactory()方法(核心实现就在此)；

refresh()容器刷新的后续步骤，两种以上两种容器加载方式都相同，这里就不再赘述 。

## 1.5. 调用时序

最后，让我们来梳理一下xml配置解析和注解扫描两种方式的beandefinition封装和注解过程

### 1.5.1. xml配置解析和注册beandefinition调用时序

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/759c5e1d64810b5bba7f5f17cdb2de50.jpg)

### 1.5.2. 注解扫描和注册beandefinition调用时序

![spring源码解析 - spring容器加载源码(beandefinition封装和注册过程)](https://s4.51cto.com/images/blog/202009/12/64f6ac75f7e561582d2638fc6499b577.jpg)

# 2. spring源码解析 - spring容器加载源码(bean实例化过程)

## 2.1. 前言

上篇我们介绍了spring容器加载的方式，并重点介绍了基于xml配置解析和注解扫描两种容器加载的方式，封装和注册beandefinition的过程。今天我们分享BeanDefinition注册后的另一个重要过程 ---bean的实例化过程的源码。

## 2.2. 容器加载流程

![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/f1cddc37f4ed609b0b09e599da5ea69c.png)
在整个bean实例化的过程中，spring主要干了以下几件大事：

1. bean的实例化过程算法；
2. IOC依赖注入；
3. 注解支撑；
4. 系列BeanPostProcessors的接口执行；
5. AOP动态代理构建；

### 2.2.1. AbstractApplicationContext.finishBeanFactoryInitialization()实例化bean入口方法

![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/2eea35ea8779a16ce77d862dc95d621b.png)

### 2.2.2. DefaultListableBeanFactory.preInstantiateSingletons()开始实例化流程

![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/70b8bb837b7d8b146c72d59aeee2e034.png)

### 2.2.3. AbstractBeanFactory.doGetBean()抽象实例化bean骨架方法

![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/31dfa67d66f5b3e3c4e2982c37628300.png)

### 2.2.4. DefaultSingletonBeanRegistry.getSingleton()获取三个级别缓存的流程

![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/c73e1a04678cb304081b2947886eba02.png)

### 2.2.5. 返回factorybean中定义的对象实例,getObjectForBeanInstance()

![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/80af2e9a40dab769c3952e85fb061d55.png)
![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/2492b592f3916a6018dea4dc5fe7e4db.png)

### 2.2.6. 多例情况(Scope=ProtoType)下，有循环依赖则直接抛出异常

![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/e5fa6f9d9fb3db835199e78a0eac67d4.png)

### 2.2.7. 检测是否抽象beandefinition对象，则直接抛出异常

![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/e10efef81dcf16d147f8ac6435c77e08.png)

### 2.2.8. 缓存依赖的depend的bean对象

![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/bf153199fa7b91d8e5bb7a38f492be7e.png)

### 2.2.9. 创建并缓存单例bean实例

![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/b81f336a944bd45befe19c318c496d0e.png)
![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/740a1c6f1d2fbd9e5786bd3724e4b566.png)

### 2.2.10. 创建bean实例createBean()核心封装

![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/78d4fdf8dcaccf6d1fcb8a75b8572d21.png)
![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/3e3e5bb1777858214b85684f80f70feb.png)

### 2.2.11. 反射创建bean实例

![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/0dea2a7dc0a27edfe7c153daee10083c.png?x-oss%3Cbr/%3E-process=image/watermark,size_16,text_QDUxQ1RP5Y2a5a6i,color_FFFFFF,t_100,g_se,x_10,y_10,shadow_90,type_ZmFuZ3poZW5naGVpdGk=)
![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/a1746cd488821a4ce5d00575a9effdcf.png)
![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/6ae702784eb8daf91b7e5039469f401b.png)
![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/4e0aac2c83b54e4a55d21153290b497f.png)
![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/13cbfb6935ee17ba1155df065423ba2c.png)
![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/482f28a012c5cccc61d93cc2896f302e.png)

### 2.2.12. bean的依赖对象IOC注入：AbstractAutowireCapableBeanFactory.populateBean()

![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/5312d0a7a3c40f6bf253a7098b3bf489.png)

### 2.2.13. bean实例化后置处理，主要是AOP动态代理的创建，这个后面会详细展开分享：initializeBean()

![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/d9c8c53aba64ef9ddf783e22791a1150.png)

### 2.2.14. bean实例销毁注册，供外部servlet容器调用：registerDisposableBeanIfNecessary()

![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/cd13aad75b876906123b1752d4d43af2.png)
![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/51f9722847c12058399c6680c31d8a06.png)

## 2.3. 调用时序

![spring源码解析 - spring容器加载源码(bean实例化过程)](https://s4.51cto.com/images/blog/202009/12/2ff5cfc535327fea6305d54437582aea.png)

## 2.4. 总结

OK，bean实例化的整个过程就先分享到这里，后面我们将会分享spring源码是如何解决循环依赖的问题，AOP源码，Spring事务源码等诸多干货。

# 3. spring源码解析 - spring缓存和循环依赖问题

## 3.1. 前言

由上篇的分析可知，spring创建和实例化bean的过程的环节是比较多并且包装比较深的，那么如果每次getBean时都需要走这么多环节的话，那么不但会产生很多内存对象和计算逻辑，而且更重要的是无法解决对象在一些场景中的依赖问题，尤其是循环依赖的问题。因此spring本身也考虑到了这个问题，在创建bean的过程中会有一些相关的缓存设计。今天我们就一起来看一下它是如何用缓存来解决的。

## 3.2. 缓存场景

### 3.2.1. 场景1、首次getBean()时

![spring源码解析 - spring缓存和循环依赖问题](./images/SpringSourceCode-1.png)

首次获取bean时，根据上图的流程当从一级或二级缓存中获取，若拿到则直接返回。若没有则直接从三级缓存中拿，当三级缓存中有缓存对象时，则通过缓存的beanFactory.getObject()直接拿到bean，同时移除三级缓存将拿到的bean写入二级缓存中，然后返回对象。

![spring源码解析 - spring缓存和循环依赖问题](./images/SpringSourceCode-2.png)
![spring源码解析 - spring缓存和循环依赖问题](./images/SpringSourceCode-3.png)

### 3.2.2. 场景2、首次createBean()时

![spring源码解析 - spring缓存和循环依赖问题](./images/SpringSourceCode-4.png)
![spring源码解析 - spring缓存和循环依赖问题](./images/SpringSourceCode-5.png)

#### 3.2.2.1. 在doCreateBean()过程中，若bean设置可提前暴露（默认开启），则会创建三级缓存(beanFactory对象)

![spring源码解析 - spring缓存和循环依赖问题](./images/SpringSourceCode-6.png)
![spring源码解析 - spring缓存和循环依赖问题](./images/SpringSourceCode-7.png)

为什么会在创建三级缓存时，同时移除二级缓存？因为二级缓存中的对象是从三级缓存中获取的，所以当三级缓存更新时会同时移除老旧的二级缓存数据，避免产生缓存数据不一致问题。

## 3.3. 循环依赖

在A对象中，持有对B对象的引用；同理在B对象中，也支持对A对象的引用。这种循环依赖分两种情况：

1. 在A对象中，B对象作为A对象的成员变量；
2. 在A对象中，B对象作为A对象的构造参数依赖

这两种方式一旦有相互成环的引用场景，就需要引起重视了。

![spring源码解析 - spring缓存和循环依赖问题](./images/SpringSourceCode-8.png)

以上截图属第一种情况，spring也只有对这种情况有考虑。这种情况spring视为正常引用，其它情况spring不支持且有此情况会直接抛出异常。

![spring源码解析 - spring缓存和循环依赖问题](./images/SpringSourceCode-9.png)
![spring源码解析 - spring缓存和循环依赖问题](./images/SpringSourceCode-10.png)

## 3.4. 解决方案

那么对于以上的第一种循环依赖情况，spring是如何解决的呢？通过构建三级缓存机制完美解决这个问题。具体的解决过程请看下面的流程图
![spring源码解析 - spring缓存和循环依赖问题](./images/SpringSourceCode-10.jpg)
根据上面的解决流程图和贴出的缓存源码, 详细的解释如下：

1. 当CircularRefA实例化时，先从缓存种拿实例bean；
2. 当三个级别的缓存中都不存在对象时调用getSingleton()创建实例；
3. 调用匿名类创建实例createBean();
4. 通过构造函数实例化CircularRefA对象（对象中的依赖属性和成员没有被实例化，也就是CircularRefB对象这时是null）；
5. 创建第三级缓存，缓存CircularRefA对应的objectFactory对象；
6. 对CircularRefA进行依赖注入，这时触发CircularRefB对象getBean()操作；
7. 程序递归进入步骤1....步骤6;
8. 当同理到步骤6时，在CircularRefB中对CircularRefA进行依赖注入, 触发getBean()操作；
9. 这时从第三级缓存中可以拿到CircularRefA的引用(虽然只是个骨架)，于是CircularRefA被注入；
10. 紧接着CircularRefB实例化完成，然后通过依赖注入赋值给CircularRefA中的CircularRefB成员变量；
11. 最终CircularRefA和CircularRefB两个对象都实例化完成（他们彼此持有对方的引用，这时也会被赋值上）；

OK，看到这里大家是不是明白了？spring的三个等级的缓存机制设计是很值得玩味的。感兴趣的可以深入去看下源码。

最后要明确一个结论：只有beandefintion的Scope=Singleton类型才能进行缓存和支持以上的第一种情况的循环依赖，其它类型的只

要出现循环依赖，spring直接抛出异常。至于其它情况为什么spring不支持大家看过源码，应该或多或少能想得到。

# 4. spring源码解析 - spring后置处理器埋点设计

## 4.1. 前言

spring容器在加载阶段，有一些非常重要类似于埋点的设计（这个概念叫法不统一，有的称为“后置处理器”）。本文暂且以接口的字面意思叫作后置处理器(PostProcessor)。这些后置处理器的设计扩展了spring容器的功能。它允许自定义不同类型的后置处理器，实现外部对bean和beandefinition的处理节点、加载时机等的干预，从而满足项目的个性化需求。

## 4.2. Spring中的后置处理器

![spring源码解析 - spring后置处理器埋点设计](./images/SpringSourceCode-11.png)

其中最重要的三大组件为：

1. ConfigurationClassPostProcessor
2. AutowiredAnnotationBeanPostProcessor
3. CommonAnnotationBeanPostProcessor

下面我们先来看下以上组件在spring源码中的埋点（处理节点设计）

### 4.2.1. BeanDefinition注册阶段

ClassPathXmlApplicationContext容器实现，对以上三大组件的注册点

![spring源码解析 - spring后置处理器埋点设计](./images/SpringSourceCode-12.png)

![spring源码解析 - spring后置处理器埋点设计](./images/SpringSourceCode-13.png)

AnnotationConfigApplicationContext容器实现，对以上三大组件的注册点

![spring源码解析 - spring后置处理器埋点设计](./images/SpringSourceCode-14.png)

![spring源码解析 - spring后置处理器埋点设计](./images/SpringSourceCode-15.png)

### 4.2.2. 触发BeanDefinition干预阶段

![spring源码解析 - spring后置处理器埋点设计](./images/SpringSourceCode-16.png)

![spring源码解析 - spring后置处理器埋点设计](./images/SpringSourceCode-17.png)

具体处理流程
![spring源码解析 - spring后置处理器埋点设计](./images/SpringSourceCode-18.png)

对应的源码实现

![spring源码解析 - spring后置处理器埋点设计](./images/SpringSourceCode-19.png)

### 4.2.3. Bean实例化前置阶段

![spring源码解析 - spring后置处理器埋点设计](./images/SpringSourceCode-20.png)

PostProcessorRegistrationDelegate.registerBeanPostProcessors()提前实例化BeanPostProcessor组件，以便后面实例化其它bean时使用

![spring源码解析 - spring后置处理器埋点设计](./images/SpringSourceCode-21.png)

### 4.2.4. Bean实例化后置阶段

AbstractAutowireCapableBeanFactory.resolveBeforeInstantiation()处理@Bean,factoryMethod等外部实例化埋点

![spring源码解析 - spring后置处理器埋点设计](./images/SpringSourceCode-22.png)

AbstractAutowireCapableBeanFactory.resolveBeforeInstantiation()处理@Bean,factoryMethod等外部实例化埋点

![spring源码解析 - spring后置处理器埋点设计](./images/SpringSourceCode-24.png)

AbstractAutowireCapableBeanFactory.applyMergedBeanDefinitionPostProcessors()收集bean对象的依赖属性和方法注入等信息

![spring源码解析 - spring后置处理器埋点设计](./images/SpringSourceCode-26.png)

AbstractAutowireCapableBeanFactory.populateBean()bean对象依赖注入赋值

![spring源码解析 - spring后置处理器埋点设计](./images/SpringSourceCode-28.png)

AbstractAutowireCapableBeanFactory.initializeBean()对bean对象生成Aop动态代理的相关处理

![spring源码解析 - spring后置处理器埋点设计](./images/SpringSourceCode-33.png)

## 4.3. 后置处理器的应

用三大后置处理器，在实际项目使用得并不是很多，但也有项目会使用到如下的场景：

1. 在beandefinition注册后，需要在运行时对容器中的beandefinition进行增强或干预；
2. 在bean实例化过程中，某个类不希望在spring中进行实例化；
3. 希望容器启动后，做一些全局的业务逻辑；
4. 开发spring的插件需要和spring进行集成或对接等。
5. 下面就举例看一些自定义后置处理器的场景：

### 4.3.1. BeanDefinitionRegistryPostProcessor扩展

![spring源码解析 - spring后置处理器埋点设计](./images/SpringSourceCode-34.png)

### 4.3.2. BeanFactoryPostProcesser扩展

![spring源码解析 - spring后置处理器埋点设计](./images/SpringSourceCode-35.png)

### 4.3.3. InitializingBean,Bean实例化扩展

![spring源码解析 - spring后置处理器埋点设计](./images/SpringSourceCode-36.png)

# 5. spring源码解析 - aop相关源码分析

## 5.1. 前言

我们如果善用spring框架的源码设计思路，其实可以写出低耦合、高内聚、兼顾灵活性和扩展性较好的优雅代码，尤其是在做框架或组件设计的时候。今天我们就来分享一个能让我们代码变得优雅的spring核心模块-AOP模块源码设计。

## 5.2. AOP的基本要素

### 5.2.1. 切面（Aspect）-----Spring中叫Advisor

切面由切点和增强（引介）组成，它既包括了横切逻辑的定义，也包括了连接点的定义，Spring AOP就是负责实施切面的框架，它将切面所定义的横切逻辑织入到切面所指定的连接点中。

### 5.2.2. 切点（Pointcut）-----一类JoinPoint的集合

每个程序类都拥有多个连接点，如一个拥有两个方法的类，这两个方法都是连接点，即连接点是程序类中客观存在的事物。AOP通过“切点”定位特定的连接点。连接点相当于数据库中的记录，而切点相当于查询条件。切点和连接点不是一对一的关系，一个切点可以匹配多个连接点。在Spring中，切点通过org.springframework.aop.Pointcut接口进行描述，它使用类和方法作为连接点的查询条件，Spring AOP的规则解析引擎负责切点所设定的查询条件，找到对应的连接点。其实确切地说，不能称之为查询连接点，因为连接点是方法执行前、执行后等包括方位信息的具体程序执行点，而切点只定位到某个方法上，所以如果希望定位到具体连接点上，还需要提供方位信息。

### 5.2.3. 增强（Advice）-----目标类方法的代理方法实现

增强是织入到目标类连接点上的一段程序代码，在Spring中，增强除用于描述一段程序代码外，还拥有另一个和连接点相关的信息，这便是执行点的方位。结合执行点方位信息和切点信息，我们就可以找到特定的连接点。

### 5.2.4. 连接点（Joinpoint）-----被拦截的某个目标方法

程序执行的某个特定位置：如类开始初始化前、类初始化后、类某个方法调用前、调用后、方法抛出异常后。一个类或一段程序代码拥有一些具有边界性质的特定点，这些点中的特定点就称为“连接点”。Spring仅支持方法的连接点，即仅能在方法调用前、方法调用后、方法抛出异常时以及方法调用前后这些程序执行点织入增强。连接点由两个信息确定：第一是用方法表示的程序执行点；第二是用相对点表示的方位。

### 5.2.5. 目标对象（Target）

增强逻辑的织入目标类。如果没有AOP，目标业务类需要自己实现所有逻辑，而在AOP的帮助下，目标业务类只实现那些非横切逻辑的程序逻辑，而性能监视和事务管理等这些横切逻辑则可以使用AOP动态织入到特定的连接点上。

## 5.3. SpringAOP的整体架构

![spring源码解析 - aop相关源码分析](./images/SpringSourceCode-37.png)

### 5.3.1. AbstractAutoProxyCreator

整个Spring AOP模块的协作组件，它作为AOP的指挥官角色，统一协作Advisor(切面)和AopProxy(动态代理)这两大核心组件。

它的本质是BeanPostProcessor(后置处理器)的一种实现，通过Spring IOC模块来启动运行。它针对的主体是Spring的Bean对象。

通过扫描有@Aspect注解的类，构建候选的Advisor切面集合；

在Aspect注解类中循环查找没有@PointCut注解的方法封装成Advice对象；

通过方法上的注解找到对应的PointCut拦截目标，并把Advisor上的表达式封装到PointCut对象中；

然后结合当前的Bean对象的元数据，过滤找到有效的Advisor集合。

最后通过beanClass创建针对当前bean的动态代理对象；

在代理对象中，动态运行Advisor调用链，完成对当前bean中匹配目标的拦截和增强。

### 5.3.2. Advisor切面设计部分

1. Advisor(切面)： 作为AOP中最重要的数据结构，它是aop所有的基础数据的归宿，负责所有的Advice和Pointcut的封装；
2. PointcutAdvisor：Advisor的扩展组件，它封装Advisor具有的Pointcut能力；
3. Pointcut(切点)：配置Advisor的拦截增强目标，Spring内部最终通过模糊匹配算法，最终定位到具体的拦截目标；
4. Advice(增强)：负责Advisor切面的具体增强业务，它包含AspectJAroundAdvice、AspectJAfterAdvice、AspectJAfterThrowingAdvice、AspectJMethodBeforeAdvice、
   AspectJAfterReturningAdvice等具体的增强部件。它们分别一 一对应不同增强类型Around、After、AfterThrowing、Before、AfterReturning 等；
5. MethodBeforeAdvice：注解Before增强的顶层设计，最终会统一包装成MethodInterceptor对象，提供统一的链式调用能力；
6. MethodInterceptor：前三种增强类型的基接口，它扩展的invoke方法让Advisor具有了基础的拦截处理能力；
7. AfterReturningAdvice：注解AfterReturning增强的顶层设计，最终会统一包装成MethodInterceptor对象，提供统一的链式调用能力；

### 5.3.3. AopProxy动态代理部分

1. BeanPostProcessor：所有bean后置处理器的基接口，负责定义基础的前、后置处理行为能力；
2. SmartInstantiationAwareBeanPostProcessor：继承至BeanPostProcessor基接口，扩展了对bean的属性、引用、类型等处理能力；
3. ProxyFactory：动态代理工厂接口，负责生产创建指定的AopProxy代理对象；
4. AopProxy：动态代理的基接口，它扩展了JDK内置的动态代理能力和Cglib类代理能力；
5. JdkDynamicAopProxy：对JDK内置Proxy动态代理的二次封装；
6. CglibAopProxy：对cglib类动态增强的二次封装；

## 5.4. SpringAOP的源码实现

### 5.4.1. AOP源码入口

AbstractAutowireCapableBeanFactory.initializeBean()实例化bean完成后的相关后置处理

![spring源码解析 - aop相关源码分析](./images/SpringSourceCode-38.png)

这里是各种BeanPostProcessor后置处理器，针对当前bean做各种扩展和增强的执行入口，包括AbstractAutoProxyCreator、ServletContextAwareProcessor、BootstrapContextAwareProcessor、InitDestroyAnnotationBeanPostProcessor、InstantiationAwareBeanPostProcessorAdapter等。

![spring源码解析 - aop相关源码分析](./images/SpringSourceCode-39.png)

幂等模式，确保一个增强面只做一次

![spring源码解析 - aop相关源码分析](./images/SpringSourceCode-40.png)
![spring源码解析 - aop相关源码分析](./images/SpringSourceCode-41.png)

对候选的Advisor进行过滤，筛选出作用于当前bean上的相关Advisor，并对最终的Advisor的执行顺序进行排序

![spring源码解析 - aop相关源码分析](./images/SpringSourceCode-42.png)
![spring源码解析 - aop相关源码分析](./images/SpringSourceCode-43.png)
![spring源码解析 - aop相关源码分析](./images/SpringSourceCode-44.png)

### 5.4.2. 通过Advisor的准备，对当前bean创建动态拦截代理类

![spring源码解析 - aop相关源码分析](./images/SpringSourceCode-45.png)

在选用代理模式的时候，spring内部采用这种方式：

1. 当proxyTargetClass为true时，目标bean实现了接口或只有实现类都采用Cglib做动态代理；
2. 当proxyTargetClass为false时，目标bean若实现了接口则采用JDK内置代理模式，若目标bean只有实现类，则采用Cglib类代理模式；
3. 默认proxyTargetClass为false；

![spring源码解析 - aop相关源码分析](./images/SpringSourceCode-46.png)

### 5.4.3. AbstractAutoProxyCreator.buildAdvisors()封装最终的各种切面对象，为后续的链式调用做准备

![spring源码解析 - aop相关源码分析](./images/SpringSourceCode-47.png)
![spring源码解析 - aop相关源码分析](./images/SpringSourceCode-48.png)

### 5.4.4. 动态代理对象生成

![spring源码解析 - aop相关源码分析](./images/SpringSourceCode-49.png)
![spring源码解析 - aop相关源码分析](./images/SpringSourceCode-50.png)

#### 5.4.4.1. JDK内置代理创建

![spring源码解析 - aop相关源码分析](./images/SpringSourceCode-51.png)

#### 5.4.4.2. Cglib动态代理创建

![spring源码解析 - aop相关源码分析](./images/SpringSourceCode-52.png)
![spring源码解析 - aop相关源码分析](./images/SpringSourceCode-53.png)

### 5.4.5. 核心增强业务实现

![spring源码解析 - aop相关源码分析](./images/SpringSourceCode-54.png)

bean代理对象中，未被拦截的方法直接反射调用，执行方法

![spring源码解析 - aop相关源码分析](./images/SpringSourceCode-55.png)

ReflectiveMethodInvocation.proceed()链式调用处理核心方法，所有本质AOP的实现(如：注解Transactional、Cacheable和自定义AOP拦截等)，都在这里被调用：

![spring源码解析 - aop相关源码分析](./images/SpringSourceCode-56.png)

**注：本文侧重点是分享Spring AOP在源码上的实现，对于使用层面的东西不是本文的重点 ~**

## 5.5. 总结

以上就是Spring AOP这块的源码分享，从以上的源码我们不看出几点：

1. AOP的本质其实就是指对一类功能增强；
2. Spring实现AOP的核心思路是采用动态代理实现；
3. 所有类似AOP的功能最终都被封装成MethodInterceptor接口，整体的调用链就是递归调用MethodInterceptor.invoke()方法执行相应的拦截处理；
4. 最后再强调一点，我们使用AOP的最终目的是解耦业务代码和公共的切面处理逻辑，让整个代码简洁、优雅、职责分明、更易于维护和可读。

# 6. Spring源码解析 - spring事务相关源码分析

## 6.1. 前言

今天我们分享一个和Spring AOP联系非常紧密的话题---Spring事务。很多人认为事务很简单，但是往往在工作中遇到一些事务的坑(尤其是事务方法中嵌套其它事务方法一起使用时)之后，我们却不知道问题产生的原因和如何有效的解决。

## 6.2. 注解事务运行流程

先来看Spring事务的底层运行流程

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-57.png)

## 6.3. 核心对象关系

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-58.png)

### 6.3.1. 事务配置相关

- TransactionManagementConfigurationSelector：配置启动事务启动(EnableTransactionManagement)时，导入注册的配置bean。它包括AutoProxyRegistrar和ProxyTransactionManagementConfiguration两大配置块。
- AutoProxyRegistrar：负责依赖注入事务的相关属性配置和注入事务入口类（InfrastructureAdvisorAutoProxyCreator类）；
- ProxyTransactionManagementConfiguration：负责注入事务相关的Bean, 包括：事务切面Bean(BeanFactoryTransactionAttributeSourceAdvisor)、TransactionAttributeSource（事务配置属性bean）和TransactionInterceptor（事务拦截器bean）等；

### 6.3.2. 事务运行拦截相关

1. AopProxy：Spring AOP 动态代理的基接口，负责定义Proxy的基础行为；
2. MethodInterceptor：Spring AOP调用链中拦截器的内部核心接口，所有类型的切面最终都会最终包装成此接口触发统一拦截；
3. TransactionInterceptor：Spring事务拦截器的核心业务实现，AOP调用链也最终触发它的invoke方法；
4. TransactionManager：Spring管理的基接口，作为子接口上层接口区分，并没有定义实际的事务行为能力；
5. PlatformTransactionManager：继承TransactionManager，定义事务和基础行为；
6. AbstractPlatformTransactionManager：负责实现整个事务管理和运行过程中的公共行为和通用实现逻辑，它继承至PlatformTransactionManager接口；

## 6.4. 源码实现

接下来我们来分块分析一下，Spring事务的源码，其中的一些坑和重要结论会在这个过程分享。

### 6.4.1. 事务配置

TransactionManagementConfigurationSelector.selectImports()负责定义外部加入spring容器的配置类

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-59.png)

此方法最终在ConfigurationClassParser中被解析并最终实例化为bean

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-60.png)

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-61.png)

AutoProxyRegistrar.registerBeanDefinitions()把InfrastructureAdvisorAutoProxyCreator注册beandefinition

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-62.png)

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-63.png)

ProxyTransactionManagementConfiguration.transactionAdvisor()注入事务切面

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-64.png)

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-65.png)

### 6.4.2. 事务创建

实际运行入口之一(还有cglib)：JdkDynamicAopProxy..invoke()

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-66.png)

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-67.png)

ReflectiveMethodInvocation.proceed()切面调用链处理核心方法

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-68.png)

TransactionInterceptor.invoke()从这里触发事务拦截

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-69.png)

TransactionAspectSupport.invokeWithinTransaction()实现Spring事务的核心业务

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-70.png)

TransactionAspectSupport.determineTransactionManager()定义指定的事务管理器

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-71.png)

对于事务管理器，默认使用DataSourceTransactionManager（可配置数据源的事务管理器），也可自定义事务管理器，然后配置数据源即可。

createTransactionIfNecessary()创建事务信息TransactionInfo：其中包括数据源、事务连接(ConnectionHolder)、事务状态、连接缓存等；

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-72.png)

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-73.png)

DataSourceTransactionManager.doGetTransaction()获取事务对象：里面包含连接包装和缓存

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-74.png)

TransactionSynchronizationManager.getResource()连接线程级缓存：确保当前线程拿到唯一的数据连接

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-75.png)

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-76.png)

AbstractPlatformTransactionManager.startTransaction()开启一个新事务

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-77.png)

只有newTransaction为true时，spring才会做实际的提交或回滚，这里是一个很重要的点。很多嵌套事务的坑，都是这里没有理解清楚。

DataSourceTransactionManager.doBegin()开启事务核心源码

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-78.png)

TransactionSynchronizationManager.bindResource()设置当前连接和数据源的线程级绑定

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-79.png)

### 6.4.3. 事务回滚

对于非编程式事务而言，Spring事务的核心实现其实就是用try/catch包裹事务提交和回滚的范式而已，但提交和回滚里面的包装大有讲究。

TransactionAspectSupport.completeTransactionAfterThrowing()事务回滚实现

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-80.png)

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-81.png)

以上截图中doSetRollbackOnly()，不会在连接上实际设置回滚点，只打个标记（当前事务需要回滚, commit时会使用该标记）！

### 6.4.4. 事务提交

AbstractPlatformTransactionManager.commit()事务实际提交源码这里

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-82.png)

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-83.png)

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-84.png)


cleanupAfterCompletion()回收连接或恢复事务，这个点耐人寻味：当事务传播属性为Require_New时，会暂时挂起之前的连接，然后创建新连接；当新连接提交或回滚后，通过这个方法恢复之前连接和状态！！！！

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-85.png)

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-86.png)

![spring源码解析 - spring事务相关源码分析](./images/SpringSourceCode-87.png)

OK事务到这里，我根据源码分享一些关于**嵌套子事务的甜点**：

1. 嵌套事务过程，中间有任何异常，只要没被屏蔽，则都会上抛，不会再执行后续代码；
2. 只要设置Require_New事务传播属性，则每个Transactional注解方法内部都会单独提交或回滚；
3. 嵌套事务会在除首个Transactional注解外的子事务中创建savepoint回滚点；
4. 设置savepoint回滚点后，回滚时会回滚包括当前方法之前的所有事务；若只需回滚当前方法，则在最外层事务方法加try{}catch(){//不抛出异常}；
5. 每个事务的对象都是不一样的，事务状态可能不一样，但连接可能是同一个；

## 6.5. 总结

今天的Spring事务源码就暂时分享到这里，事务这块只有一层的事务很简单，但当嵌套了多个(层)子事务（而且每个子事务的事务传播属性可能不一样的情况下）时就当另当别论了。需要我们根据源码的规律去分析每层子事务该如何流转！！



