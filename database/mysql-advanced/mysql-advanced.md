<h2>目录</h2>

<details open>
  <summary><a href="#1-索引">1. 索引</a></summary>
  <ul>
    <a href="#11-索引概述">1.1. 索引概述</a><br>
    <a href="#12-索引优势劣势">1.2. 索引优势劣势</a><br>
  <details open>
    <summary><a href="#13-索引结构">1.3. 索引结构</a>  </summary>
    <ul>
      <a href="#131-btree-结构">1.3.1. BTREE 结构</a><br>
      <a href="#132-btree-结构">1.3.2. B+TREE 结构</a><br>
      <a href="#133-mysql中的btree">1.3.3. MySQL中的B+Tree</a><br>
    </ul>
  </details>
    <a href="#14-索引分类">1.4. 索引分类</a><br>
  <details open>
    <summary><a href="#15-索引语法">1.5. 索引语法</a>  </summary>
    <ul>
      <a href="#151-创建索引">1.5.1. 创建索引</a><br>
      <a href="#152-查看索引">1.5.2. 查看索引</a><br>
      <a href="#153-删除索引">1.5.3. 删除索引</a><br>
      <a href="#154-alter命令">1.5.4. ALTER命令</a><br>
    </ul>
  </details>
    <a href="#16-索引设计原则">1.6. 索引设计原则</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#2-mysql的体系结构概览">2. Mysql的体系结构概览</a></summary>
  <ul>
    <a href="#21-连接层">2.1. 连接层</a><br>
    <a href="#22-服务层">2.2. 服务层</a><br>
    <a href="#23-引擎层">2.3. 引擎层</a><br>
    <a href="#24-存储层">2.4. 存储层</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#3-存储引擎">3. 存储引擎</a></summary>
  <ul>
    <a href="#31-存储引擎概述">3.1. 存储引擎概述</a><br>
  <details open>
    <summary><a href="#32-各种存储引擎特性">3.2. 各种存储引擎特性</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#321-innodb">3.2.1. InnoDB</a>    </summary>
      <ul>
        <a href="#3211-事务控制">3.2.1.1. 事务控制</a><br>
        <a href="#3212-外键约束">3.2.1.2. 外键约束</a><br>
        <a href="#3213-存储方式">3.2.1.3. 存储方式</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#322-myisam">3.2.2. MyISAM</a>    </summary>
      <ul>
        <a href="#3221-不支持事务">3.2.2.1. 不支持事务</a><br>
        <a href="#3222-文件存储方式">3.2.2.2. 文件存储方式</a><br>
      </ul>
    </details>
      <a href="#323-memory">3.2.3. MEMORY</a><br>
      <a href="#324-merge">3.2.4. MERGE</a><br>
    </ul>
  </details>
    <a href="#33-存储引擎的选择">3.3. 存储引擎的选择</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#4-优化sql步骤">4. 优化SQL步骤</a></summary>
  <ul>
    <a href="#41-查看sql执行频率（查看是以查为主还是以增删改为主）">4.1. 查看SQL执行频率（查看是以查为主还是以增删改为主）</a><br>
    <a href="#42-定位低效率执行sql">4.2. 定位低效率执行SQL</a><br>
  <details open>
    <summary><a href="#43-explain分析执行计划">4.3. explain分析执行计划</a>  </summary>
    <ul>
      <a href="#431-explain-之-id">4.3.1. explain 之 id</a><br>
      <a href="#432-explain-之-select_type">4.3.2. explain 之 select_type</a><br>
      <a href="#433-explain-之-table">4.3.3. explain 之 table</a><br>
      <a href="#434-explain-之-type">4.3.4. explain 之 type</a><br>
      <a href="#435-explain-之--key">4.3.5. explain 之  key</a><br>
      <a href="#436-explain-之-rows">4.3.6. explain 之 rows</a><br>
      <a href="#437-explain-之-extra">4.3.7. explain 之 extra</a><br>
    </ul>
  </details>
    <a href="#44-show-profile分析sql">4.4. show profile分析SQL</a><br>
    <a href="#45-trace分析优化器执行计划">4.5. trace分析优化器执行计划</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#5-索引的使用">5. 索引的使用</a></summary>
  <ul>
  <details open>
    <summary><a href="#51-验证索引提升查询效率">5.1. 验证索引提升查询效率</a>  </summary>
    <ul>
      <a href="#511-根据id查询">5.1.1. 根据ID查询</a><br>
      <a href="#512-根据-title-进行精确查询">5.1.2. 根据 title 进行精确查询</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#52-避免索引失效">5.2. 避免索引失效</a>  </summary>
    <ul>
      <a href="#521-全值匹配-，对索引中所有列都指定具体值。">5.2.1. 全值匹配 ，对索引中所有列都指定具体值。</a><br>
      <a href="#522-最左前缀法则">5.2.2. 最左前缀法则</a><br>
      <a href="#523-范围查询右边的列，不能使用索引">5.2.3. 范围查询右边的列，不能使用索引</a><br>
      <a href="#524-不要在索引列上进行运算操作，索引将失效">5.2.4. 不要在索引列上进行运算操作，索引将失效</a><br>
      <a href="#525-字符串不加单引号，造成索引失效。">5.2.5. 字符串不加单引号，造成索引失效。</a><br>
      <a href="#526-尽量使用覆盖索引，避免select-">5.2.6. 尽量使用覆盖索引，避免select *</a><br>
      <a href="#527-or前面的条件都会索引失效">5.2.7. or前面的条件都会索引失效</a><br>
      <a href="#528-以开头的like模糊查询，索引失效">5.2.8. 以%开头的Like模糊查询，索引失效</a><br>
      <a href="#529-如果mysql评估使用索引比全表更慢，则不使用索引">5.2.9. 如果MySQL评估使用索引比全表更慢，则不使用索引</a><br>
      <a href="#5210-is-null，is-not-null索引失效。">5.2.10. IS NULL，IS NOT NULL索引失效。</a><br>
      <a href="#5211-in-走索引一般符合数据少，not-in索引失效一般符合数据多。">5.2.11. in 走索引(一般符合数据少)，not in索引失效(一般符合数据多)。</a><br>
      <a href="#5212-单列索引和复合索引">5.2.12. 单列索引和复合索引</a><br>
    </ul>
  </details>
    <a href="#53-查看索引使用情况">5.3. 查看索引使用情况</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#6-sql优化">6. SQL优化</a></summary>
  <ul>
  <details open>
    <summary><a href="#61-优化大批量插入数据">6.1. 优化大批量插入数据</a>  </summary>
    <ul>
      <a href="#611-主键顺序插入">6.1.1. 主键顺序插入</a><br>
      <a href="#612-关闭唯一性校验">6.1.2. 关闭唯一性校验</a><br>
      <a href="#613-手动提交事务">6.1.3. 手动提交事务</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#62-优化insert语句">6.2. 优化insert语句</a>  </summary>
    <ul>
      <a href="#621-尽可能批量插入">6.2.1. 尽可能批量插入</a><br>
      <a href="#622-在事务中进行数据插入">6.2.2. 在事务中进行数据插入</a><br>
      <a href="#623-数据有序插入">6.2.3. 数据有序插入</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#63-优化order-by语句">6.3. 优化order by语句</a>  </summary>
    <ul>
      <a href="#631-两种排序方式">6.3.1. 两种排序方式</a><br>
      <a href="#632-filesort的优化">6.3.2. Filesort的优化</a><br>
    </ul>
  </details>
    <a href="#64-优化group-by语句">6.4. 优化group by语句</a><br>
    <a href="#65-优化嵌套查询">6.5. 优化嵌套查询</a><br>
    <a href="#66-优化or条件">6.6. 优化OR条件</a><br>
  <details open>
    <summary><a href="#67-优化分页查询">6.7. 优化分页查询</a>  </summary>
    <ul>
      <a href="#671-优化思路一">6.7.1. 优化思路一</a><br>
      <a href="#672-优化思路二">6.7.2. 优化思路二</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#68-使用sql提示使用索引">6.8. 使用SQL提示使用索引</a>  </summary>
    <ul>
      <a href="#681-use-index">6.8.1. USE INDEX</a><br>
      <a href="#682-ignore-index">6.8.2. IGNORE INDEX</a><br>
      <a href="#683-force-index">6.8.3. FORCE INDEX</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#7-应用优化">7. 应用优化</a></summary>
  <ul>
    <a href="#71-使用连接池">7.1. 使用连接池</a><br>
  <details open>
    <summary><a href="#72-减少对mysql的访问">7.2. 减少对MySQL的访问</a>  </summary>
    <ul>
      <a href="#721-避免对数据进行重复检索">7.2.1. 避免对数据进行重复检索</a><br>
      <a href="#722-增加cache层">7.2.2. 增加cache层</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#73-负载均衡">7.3. 负载均衡</a>  </summary>
    <ul>
      <a href="#731-利用mysql复制分流查询">7.3.1. 利用MySQL复制分流查询</a><br>
      <a href="#732-采用分布式数据库架构">7.3.2. 采用分布式数据库架构</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#8-mysql内存管理及优化">8. Mysql内存管理及优化</a></summary>
  <ul>
    <a href="#81-内存优化原则">8.1. 内存优化原则</a><br>
  <details open>
    <summary><a href="#82-myisam-内存优化">8.2. MyISAM 内存优化</a>  </summary>
    <ul>
      <a href="#821-key_buffer_size">8.2.1. key_buffer_size</a><br>
      <a href="#822-read_buffer_size">8.2.2. read_buffer_size</a><br>
      <a href="#823-read_rnd_buffer_size">8.2.3. read_rnd_buffer_size</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#83-innodb-内存优化">8.3. InnoDB 内存优化</a>  </summary>
    <ul>
      <a href="#831-innodb_buffer_pool_size">8.3.1. innodb_buffer_pool_size</a><br>
      <a href="#832-innodb_log_buffer_size">8.3.2. innodb_log_buffer_size</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#9-mysql并发参数调整">9. Mysql并发参数调整</a></summary>
  <ul>
    <a href="#91-max_connections">9.1. max_connections</a><br>
    <a href="#92-back_log">9.2. back_log</a><br>
    <a href="#93-table_open_cache">9.3. table_open_cache</a><br>
    <a href="#94-thread_cache_size">9.4. thread_cache_size</a><br>
    <a href="#95-innodb_lock_wait_timeout">9.5. innodb_lock_wait_timeout</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#10-mysql锁问题">10. Mysql锁问题</a></summary>
  <ul>
    <a href="#101-锁概述">10.1. 锁概述</a><br>
    <a href="#102-锁分类">10.2. 锁分类</a><br>
    <a href="#103-mysql-锁">10.3. Mysql 锁</a><br>
  <details open>
    <summary><a href="#104-myisam表锁">10.4. MyISAM表锁</a>  </summary>
    <ul>
      <a href="#1041-如何加表锁">10.4.1. 如何加表锁</a><br>
      <a href="#1042-查看锁的争用情况">10.4.2. 查看锁的争用情况</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#105-innodb-行锁">10.5. InnoDB 行锁</a>  </summary>
    <ul>
      <a href="#1051-行锁介绍">10.5.1. 行锁介绍</a><br>
    <details open>
      <summary><a href="#1052-背景知识">10.5.2. 背景知识</a>    </summary>
      <ul>
        <a href="#10521-事务及其acid属性">10.5.2.1. 事务及其ACID属性</a><br>
        <a href="#10522-并发事务处理带来的问题">10.5.2.2. 并发事务处理带来的问题</a><br>
        <a href="#10523-事务隔离级别">10.5.2.3. 事务隔离级别</a><br>
      </ul>
    </details>
      <a href="#1053-innodb-的行锁模式">10.5.3. InnoDB 的行锁模式</a><br>
      <a href="#1054-无索引行锁升级为表锁">10.5.4. 无索引行锁升级为表锁</a><br>
      <a href="#1055-间隙锁危害">10.5.5. 间隙锁危害</a><br>
      <a href="#1056-innodb-行锁争用情况">10.5.6. InnoDB 行锁争用情况</a><br>
      <a href="#1057-总结">10.5.7. 总结</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#11-mysql中常用工具">11. MySql中常用工具</a></summary>
  <ul>
  <details open>
    <summary><a href="#111-mysql">11.1. mysql</a>  </summary>
    <ul>
      <a href="#1111-连接选项">11.1.1. 连接选项</a><br>
      <a href="#1112-执行选项">11.1.2. 执行选项</a><br>
    </ul>
  </details>
    <a href="#112-mysqladmin">11.2. mysqladmin</a><br>
    <a href="#113-mysqlbinlog">11.3. mysqlbinlog</a><br>
  <details open>
    <summary><a href="#114-mysqldump">11.4. mysqldump</a>  </summary>
    <ul>
      <a href="#1141-连接选项">11.4.1. 连接选项</a><br>
      <a href="#1142-输出内容选项">11.4.2. 输出内容选项</a><br>
    </ul>
  </details>
    <a href="#115-mysqlimportsource">11.5. mysqlimport/source</a><br>
    <a href="#116-mysqlshow">11.6. mysqlshow</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#12-mysql-日志">12. Mysql 日志</a></summary>
  <ul>
    <a href="#121-错误日志">12.1. 错误日志</a><br>
  <details open>
    <summary><a href="#122-二进制日志">12.2. 二进制日志</a>  </summary>
    <ul>
      <a href="#1221-概述">12.2.1. 概述</a><br>
      <a href="#1222-日志格式">12.2.2. 日志格式</a><br>
    <details open>
      <summary><a href="#1223-日志读取">12.2.3. 日志读取</a>    </summary>
      <ul>
        <a href="#12231-查看statement格式日志">12.2.3.1. 查看STATEMENT格式日志</a><br>
        <a href="#12232-查看row格式日志">12.2.3.2. 查看ROW格式日志</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1224-日志删除">12.2.4. 日志删除</a>    </summary>
      <ul>
        <a href="#12241-方式一">12.2.4.1. 方式一</a><br>
        <a href="#12242-方式二">12.2.4.2. 方式二</a><br>
        <a href="#12243-方式三">12.2.4.3. 方式三</a><br>
        <a href="#12244-方式四">12.2.4.4. 方式四</a><br>
      </ul>
    </details>
    </ul>
  </details>
    <a href="#123-查询日志">12.3. 查询日志</a><br>
  <details open>
    <summary><a href="#124-慢查询日志">12.4. 慢查询日志</a>  </summary>
    <ul>
      <a href="#1241-文件位置和格式">12.4.1. 文件位置和格式</a><br>
    <details open>
      <summary><a href="#1242-日志的读取">12.4.2. 日志的读取</a>    </summary>
      <ul>
        <a href="#12421-查询long_query_time的值">12.4.2.1. 查询long_query_time的值</a><br>
        <a href="#12422-执行查询操作">12.4.2.2. 执行查询操作</a><br>
        <a href="#12423-查看慢查询日志文件">12.4.2.3. 查看慢查询日志文件</a><br>
      </ul>
    </details>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#13-mysql复制">13. Mysql复制</a></summary>
  <ul>
    <a href="#131-复制概述">13.1. 复制概述</a><br>
    <a href="#132-复制原理">13.2. 复制原理</a><br>
    <a href="#133-复制优势">13.3. 复制优势</a><br>
  <details open>
    <summary><a href="#134-搭建步骤">13.4. 搭建步骤</a>  </summary>
    <ul>
      <a href="#1341-master">13.4.1. master</a><br>
      <a href="#1342-slave">13.4.2. slave</a><br>
      <a href="#1343-验证同步操作">13.4.3. 验证同步操作</a><br>
    </ul>
  </details>
  </ul>
</details>


<h1>MySQL 5.7 高级</h1>

# 1. 索引

## 1.1. 索引概述

MySQL官方对索引的定义为：索引（index）是帮助MySQL高效获取数据的数据结构（有序）。在数据之外，数据库系统还维护者满足特定查找算法的数据结构，这些数据结构以某种方式引用（指向）数据， 这样就可以在这些数据结构上实现高级查找算法，这种数据结构就是索引。如下面的==示意图==所示 : 

![1555902055367](./images/mysql-advanced-1.png)

左边是数据表，一共有两列七条记录，最左边的是数据记录的物理地址（注意逻辑上相邻的记录在磁盘上也并不是一定物理相邻的）。为了加快Col2的查找，可以维护一个右边所示的二叉查找树，每个节点分别包含索引键值和一个指向对应数据记录物理地址的指针，这样就可以运用二叉查找快速获取到相应数据。

一般来说索引本身也很大，不可能全部存储在内存中，因此索引往往以索引文件的形式存储在磁盘上。索引是数据库中用来提高性能的最常用的工具。

## 1.2. 索引优势劣势

**优势**

- 类似于书籍的目录索引，**提高数据检索的效率，降低数据库的IO成本**。

- 通过索引列对数据进行排序，**降低数据排序的成本，降低CPU的消耗**。


**劣势**

- 实际上索引也是一张表，该表中保存了主键与索引字段，并指向实体类的记录，所以**索引列也是要占用空间**的。

- 虽然索引大大提高了查询效率，同时却也**降低更新表的速度**，如对表进行INSERT、UPDATE、DELETE。因为更新表时，MySQL 不仅要保存数据，还要保存一下索引文件每次更新添加了索引列的字段，都会调整因为更新所带来的键值变化后的索引信息。


## 1.3. 索引结构

索引是在MySQL的存储引擎层中实现的，而不是在服务器层实现的。所以每种存储引擎的索引都不一定完全相同，也不是所有的存储引擎都支持所有的索引类型的。MySQL目前提供了以下4种索引：

1. BTREE 索引 ： 最常见的索引类型，大部分索引都支持 B 树索引。
2. HASH 索引：只有Memory引擎支持 ， 使用场景简单 。
3. R-tree 索引（空间索引）：空间索引是MyISAM引擎的一个特殊索引类型，主要用于地理空间数据类型，通常使用较少，不做特别介绍。
4. Full-text （全文索引） ：全文索引也是MyISAM的一个特殊索引类型，主要用于全文索引，InnoDB从Mysql5.6版本开始支持全文索引。

<center><b>MyISAM、InnoDB、Memory三种存储引擎对各种索引类型的支持</b></center>

| 索引        | InnoDB引擎      | MyISAM引擎 | Memory引擎 |
| ----------- | --------------- | ---------- | ---------- |
| BTREE索引   | 支持            | 支持       | 支持       |
| HASH 索引   | 不支持          | 不支持     | 支持       |
| R-tree 索引 | 不支持          | 支持       | 不支持     |
| Full-text   | 5.6版本之后支持 | 支持       | 不支持     |

我们平常所说的索引，如果没有特别指明，都是指B+树（多路搜索树，并不一定是二叉的）结构组织的索引。其中聚集索引、复合索引、前缀索引、唯一索引默认都是使用 B+tree 索引，统称为**索引**。

### 1.3.1. BTREE 结构

BTree又叫多路平衡搜索树，一颗m叉的BTree特性如下：

- 树中每个节点最多包含m个孩子。
- 除根节点与叶子节点外，每个节点至少有[ceil(m/2)]个孩子。
- 若根节点不是叶子节点，则至少有两个孩子。
- 所有的叶子节点都在同一层。
- 每个非叶子节点由n个key与n+1个指针组成，其中[ceil(m/2)-1] <= n <= m-1 



以5叉BTree为例，key的数量：公式推导[ceil(m/2)-1] <= n <= m-1。所以 2 <= n <=4 。当n>4时，中间节点分裂到父节点，两边节点分裂。

插入 C N G A H E K Q M F W L T Z D P R X Y S 数据为例。

演变过程如下：

1). 插入前4个字母 C N G A 

![1555944126588](./images/mysql-advanced-2.png)

2). 插入H，n>4，中间元素G字母向上分裂到新的节点

![1555944549825](./images/mysql-advanced-3.png)

3). 插入E，K，Q不需要分裂

![1555944596893](./images/mysql-advanced-4.png)

4). 插入M，中间元素M字母向上分裂到父节点G

![1555944652560](./images/mysql-advanced-5.png)

5). 插入F，W，L，T不需要分裂

![1555944686928](./images/mysql-advanced-6.png)

6). 插入Z，中间元素T向上分裂到父节点中 

![1555944713486](./images/mysql-advanced-7.png)

7). 插入D，中间元素D向上分裂到父节点中。然后插入P，R，X，Y不需要分裂

![1555944749984](./images/mysql-advanced-8.png)

8). 最后插入S，NPQR节点n>5，中间节点Q向上分裂，但分裂后父节点DGMT的n>5，中间节点M向上分裂

![1555944848294](./images/mysql-advanced-9.png)

到此，该BTREE树就已经构建完成了， BTREE树 和 二叉树 相比， 查询数据的效率更高， 因为对于**相同的数据量来说，BTREE的层级结构比二叉树小，因此搜索速度快**。

### 1.3.2. B+TREE 结构

B+Tree为BTree的变种，B+Tree与BTree的区别为：

1. n叉B+Tree最多含有n个key，而BTree最多含有n-1个key。

2. B+Tree的叶子节点保存所有的key信息，依key大小顺序排列。

3. 所有的非叶子节点都可以看作是key的索引部分。


![1555906287178](./images/mysql-advanced-9.jpg)

由于B+Tree只有叶子节点保存key信息，查询任何key都要从root走到叶子。所以B+Tree的查询效率更加稳定。

### 1.3.3. MySQL中的B+Tree

MySql索引数据结构对经典的B+Tree进行了优化。在原B+Tree的基础上，增加一个指向相邻叶子节点的**链表指针（便于进行范围搜索）**，就形成了带有顺序指针的B+Tree，提高区间访问的性能。

MySQL中的 B+Tree 索引结构示意图: 

![1555906287178](./images/mysql-advanced-10.png)

## 1.4. 索引分类

1. 单值索引（单列索引） ：即一个索引只包含单个列，一个表可以有多个单列索引

2. 唯一索引 ：索引列的值必须唯一，但允许有空值

3. 复合索引 ：即一个索引包含多个列


## 1.5. 索引语法

### 1.5.1. 创建索引

语法 ： 	

```sql
CREATE 	[UNIQUE|FULLTEXT|SPATIAL] INDEX index_name 
[USING  index_type]
ON tbl_name(index_col_name,...)


index_col_name : column_name[(length)][ASC | DESC]
```

**index_type默认是BTREE索引，主键列默认有唯一索引(名字是PRIMARY)**

示例 ： 为city表中的city_name字段创建单列索引 ；

![1551438009843](./images/mysql-advanced-11.png)

### 1.5.2. 查看索引

语法： 

```
show index from table_na\G;
```

### 1.5.3. 删除索引

语法 ：

```
DROP INDEX index_name ON tbl_name;
```

### 1.5.4. ALTER命令

```mysql
# 1. 该语句添加一个主键，这意味着索引值必须是唯一的，且不能为NULL
alter table tb_name add primary key(column_list); 

# 2. 这条语句创建索引的值必须是唯一的（除了NULL外，NULL可能会出现多次）	
alter table tb_name add unique index_name(column_list);

# 3. 添加普通索引，索引值可以出现多次。
alter table tb_name add index index_name(column_list);

# 4. 该语句指定了索引为FULLTEXT， 用于全文索引
alter table tb_name add fulltext index_name(column_list);
```

## 1.6. 索引设计原则

​	索引的设计可以遵循一些已有的原则，创建索引的时候请尽量考虑符合这些原则，便于提升索引的使用效率，更高效的使用索引。

1. 对**查询频次较高**，且**数据量比较大**的表建立索引。
2. 索引字段的选择，最佳候选列应当**从where子句的条件中提取**，如果where子句中的组合比较多，那么应当挑选最常用、过滤效果最好的列的组合。
3. **尽可能使用唯一索引，区分度越高，使用索引的效率越高**。
4. 索引可以有效的提升查询数据的效率，但索引数量不是多多益善，索引越多，维护索引的代价自然也就水涨船高。对于插入、更新、删除等DML操作比较频繁的表来说，索引过多，会引入相当高的维护代价，降低DML操作的效率，增加相应操作的时间消耗。另外索引过多的话，MySQL也会犯选择困难病，虽然最终仍然会找到一个可用的索引，但无疑提高了选择的代价。
5. **尽可能使用短索引**，索引创建之后也是使用硬盘来存储的，因此提升索引访问的I/O效率，也可以提升总体的访问效率。假如构成索引的字段总长度比较短，那么在给定大小的存储块内可以存储更多的索引值，相应的可以有效的提升MySQL访问索引的I/O效率。
6. **利用最左前缀（查询时候条件中有最左边的第一个列名称条件被包含在复合索引中）**，N个列组合而成的组合索引，那么相当于是创建了N个索引，如果查询时where子句中使用了组成该索引的前几个字段，那么这条查询SQL可以利用组合索引来提升查询效率。


```mysql
# 创建复合索引:
CREATE INDEX idx_name_email_status ON tb_seller(NAME, email, status);

# 就相当于
# 对name 创建索引 ;
# 对name , email 创建了索引 ;
# 对name , email, status 创建了索引 ;
```

# 2. Mysql的体系结构概览

![171214401286615](./images/mysql-advanced-12.jpg)

整个MySQL Server由以下组成

- Connection Pool : 连接池组件
- Management Services & Utilities : 管理服务和工具组件
- SQL Interface : SQL接口组件
- Parser : 查询分析器组件
- Optimizer : 优化器组件
- Caches & Buffers : 缓冲池组件
- Pluggable Storage Engines : 存储引擎
- File System : 文件系统

## 2.1. 连接层

最上层是一些客户端和链接服务，包含本地sock 通信和大多数基于客户端/服务端工具实现的类似于 TCP/IP的通信。主要完成一些类似于连接处理、授权认证、及相关的安全方案。在该层上引入了线程池的概念，为通过认证安全接入的客户端提供线程。同样在该层上可以实现基于SSL的安全链接。服务器也会为安全接入的每个客户端验证它所具有的操作权限。

## 2.2. 服务层

第二层架构主要完成大多数的核心服务功能，如SQL接口，并完成缓存的查询，SQL的分析和优化，部分内置函数的执行。所有跨存储引擎的功能也在这一层实现，如 过程、函数等。在该层，服务器会解析查询并创建相应的内部解析树，并对其完成相应的优化如确定表的查询的顺序，是否利用索引等， 最后生成相应的执行操作。如果是select语句，服务器还会查询内部的缓存，如果缓存空间足够大，这样在解决大量读操作的环境中能够很好的提升系统的性能。

## 2.3. 引擎层

存储引擎层， 存储引擎真正的负责了MySQL中数据的存储和提取，服务器通过API和存储引擎进行通信。不同的存储引擎具有不同的功能，这样我们可以根据自己的需要，来选取合适的存储引擎。

## 2.4. 存储层

数据存储层， 主要是将数据存储在文件系统之上，并完成与存储引擎的交互。



和其他数据库相比，MySQL有点与众不同，它的架构可以在多种不同场景中应用并发挥良好作用。主要体现在存储引擎上，插件式的存储引擎架构，将查询处理和其他的系统任务以及数据的存储提取分离。这种架构可以根据业务的需求和实际需要选择合适的存储引擎。

# 3. 存储引擎

## 3.1. 存储引擎概述

和大多数的数据库不同, MySQL中有一个存储引擎的概念, 针对不同的存储需求可以选择最优的存储引擎。

**存储引擎**就是存储数据，建立索引，更新查询数据等等技术的实现方式 。存储引擎是基于表的，而不是基于库的。所以存储引擎也可被称为表类型。

Oracle，SqlServer等数据库只有一种存储引擎。MySQL提供了插件式的存储引擎架构。所以MySQL存在多种存储引擎，可以根据需要使用相应引擎，或者编写存储引擎。

MySQL5.0支持的存储引擎包含 ： InnoDB 、MyISAM 、BDB、MEMORY、MERGE、EXAMPLE、NDB Cluster、ARCHIVE、CSV、BLACKHOLE、FEDERATED等，其中InnoDB和BDB提供事务安全表，其他存储引擎是非事务安全表。

可以通过指定 show engines ， 来查询当前数据库支持的存储引擎。 

创建新表时如果不指定存储引擎，那么系统就会使用默认的存储引擎，MySQL5.5之前的默认存储引擎是MyISAM，5.5之后就改为了InnoDB。

查看Mysql数据库默认的存储引擎 ， 指令 ：

```mysql
 show variables like '%storage_engine%' ;  	 
```

## 3.2. 各种存储引擎特性

下面重点介绍几种常用的存储引擎， 并对比各个存储引擎之间的区别， 如下表所示 ： 

| 特点         | InnoDB               | MyISAM   | MEMORY | MERGE | NDB  |
| ------------ | -------------------- | -------- | ------ | ----- | ---- |
| 存储限制     | 64TB                 | 有       | 有     | 没有  | 有   |
| 事务安全     | ==支持==             |          |        |       |      |
| 锁机制       | ==行锁(适合高并发)== | ==表锁== | 表锁   | 表锁  | 行锁 |
| B树索引      | 支持                 | 支持     | 支持   | 支持  | 支持 |
| 哈希索引     |                      |          | 支持   |       |      |
| 全文索引     | 支持(5.6版本之后)    | 支持     |        |       |      |
| 集群索引     | 支持                 |          |        |       |      |
| 数据索引     | 支持                 |          | 支持   |       | 支持 |
| 索引缓存     | 支持                 | 支持     | 支持   | 支持  | 支持 |
| 数据可压缩   |                      | 支持     |        |       |      |
| 空间使用     | 高                   | 低       | N/A    | 低    | 低   |
| 内存使用     | 高                   | 低       | 中等   | 低    | 高   |
| 批量插入速度 | 低                   | 高       | 高     | 高    | 高   |
| 支持外键     | ==支持==             |          |        |       |      |

下面我们将重点介绍最长使用的两种存储引擎： InnoDB、MyISAM ， 另外两种 MEMORY、MERGE ， 了解即可。

### 3.2.1. InnoDB

InnoDB存储引擎是Mysql的默认存储引擎。InnoDB存储引擎提供了具有提交、回滚、崩溃恢复能力的事务安全。但是对比MyISAM的存储引擎，InnoDB写的处理效率差一些，并且会占用更多的磁盘空间以保留数据和索引。

InnoDB存储引擎不同于其他存储引擎的特点 ： 

#### 3.2.1.1. 事务控制

```mysql
create table goods_innodb(
  id int NOT NULL AUTO_INCREMENT,
  name varchar(20) NOT NULL,
    primary key(id)
) ENGINE=innodb DEFAULT CHARSET=utf8;
start transaction;

insert into goods_innodb(id,name)values(null,'Meta20');

commit; 
```

测试，发现在InnoDB中是存在事务的 ；

#### 3.2.1.2. 外键约束

MySQL支持外键的存储引擎只有InnoDB ， 在创建外键的时候， 要求父表必须有对应的索引 ，子表在创建外键的时候， 也会自动的创建对应的索引。

在创建索引时， 可以指定在删除、更新父表时，对子表进行的相应操作，包括 RESTRICT、CASCADE、SET NULL 和 NO ACTION。

- RESTRICT和NO ACTION相同， 是指限制在子表有关联记录的情况下， 父表不能更新；
- CASCADE表示父表在更新或者删除时，更新或者删除子表对应的记录；
- SET NULL 则表示父表在更新或者删除的时候，子表的对应字段被SET NULL 。

#### 3.2.1.3. 存储方式

InnoDB 存储表和索引有以下两种方式： 

1. 使用共享表空间存储， 这种方式创建的表的表结构保存在.frm文件中， 数据和索引保存在 innodb_data_home_dir 和 innodb_data_file_path定义的表空间中，可以是多个文件。
2. 使用多表空间存储， 这种方式创建的表的表结构仍然存在 .frm 文件中，但是每个表的数据和索引单独保存在 .ibd 中。 

### 3.2.2. MyISAM

MyISAM 不支持事务、也不支持外键，其优势是访问的速度快，对事务的完整性没有要求或者以SELECT、INSERT为主的应用基本上都可以使用这个引擎来创建表 。有以下两个比较重要的特点： 

#### 3.2.2.1. 不支持事务

```mysql
create table goods_myisam(
  id int NOT NULL AUTO_INCREMENT,
  name varchar(20) NOT NULL,
    primary key(id)
)ENGINE=myisam DEFAULT CHARSET=utf8; 
```

#### 3.2.2.2. 文件存储方式

每个MyISAM在磁盘上存储成3个文件，其文件名都和表名相同，但拓展名分别是 ： 

- .frm (存储表定义，from);
- .MYD(MYData , 存储数据);
- .MYI(MYIndex , 存储索引);

### 3.2.3. MEMORY

Memory存储引擎将表的数据存放在内存中。每个MEMORY表实际对应一个磁盘文件，格式是.frm ，该文件中只存储表的结构，而其数据文件，都是存储在内存中，这样有利于数据的快速处理，提高整个表的效率。

MEMORY 类型的表访问非常地快，因为他的数据是存放在内存中的，并且默认使用**HASH索引** ， 但是**服务一旦关闭，表中的数据就会丢失**。

### 3.2.4. MERGE

MERGE存储引擎是一组MyISAM表的组合，这些MyISAM表必须结构完全相同，MERGE表本身并没有存储数据，对MERGE类型的表可以进行查询、更新、删除操作，这些操作实际上是对内部的MyISAM表进行的。

对于MERGE类型表的插入操作，是通过INSERT_METHOD子句定义插入的表，可以有3个不同的值，使用FIRST 或 LAST 值使得插入操作被相应地作用在第一或者最后一个表上，不定义这个子句或者定义为NO，表示不能对这个MERGE表执行插入操作。

可以对MERGE表进行DROP操作，但是这个操作只是删除MERGE表的定义，对内部的表是没有任何影响的。

## 3.3. 存储引擎的选择

在选择存储引擎时，应该根据应用系统的特点选择合适的存储引擎。对于复杂的应用系统，还可以根据实际情况选择多种存储引擎进行组合。以下是几种常用的存储引擎的使用环境。

- InnoDB : 是Mysql的默认存储引擎，用于事务处理应用程序，支持外键。如果应用对事务的完整性有比较高的要求，在并发条件下要求数据的一致性，数据操作除了插入和查询意外，还包含很多的更新、删除操作，那么InnoDB存储引擎是比较合适的选择。InnoDB存储引擎除了有效的降低由于删除和更新导致的锁定， 还可以确保事务的完整提交和回滚，对于类似于计费系统或者财务系统等对数据准确性要求比较高的系统，InnoDB是最合适的选择。
- MyISAM ： 如果应用是以读操作和插入操作为主，只有很少的更新和删除操作，并且对事务的完整性、并发性要求不是很高，那么选择这个存储引擎是非常合适的。
- MEMORY：将所有数据保存在RAM中，在需要快速定位记录和其他类似数据环境下，可以提供几块的访问。MEMORY的缺陷就是对表的大小有限制，太大的表无法缓存在内存中，其次是要确保表的数据可以恢复，数据库异常终止后表中的数据是可以恢复的。MEMORY表通常用于更新不太频繁的小表，用以快速得到访问结果。
- MERGE：用于将一系列等同的MyISAM表以逻辑方式组合在一起，并作为一个对象引用他们。MERGE表的优点在于可以突破对单个MyISAM表的大小限制，并且通过将不同的表分布在多个磁盘上，可以有效的改善MERGE表的访问效率。这对于存储诸如数据仓储等VLDB（Very Large DB）环境十分合适。

# 4. 优化SQL步骤

当面对一个有 SQL 性能问题的数据库时，我们应该从何处入手来进行系统的分析，使得能够尽快定位问题 SQL 并尽快解决问题。

## 4.1. 查看SQL执行频率（查看是以查为主还是以增删改为主）

MySQL 客户端连接成功后，通过 **show [session|global] status** 命令可以提供服务器状态信息。**show [session|global] status**可以根据需要加上参数“session”或者“global”来显示 session 级（当前连接）的计结果和 global 级（自数据库上次启动至今）的统计结果。如果不写，默认使用参数是“session”。

下面的命令显示了当前 session 中所有统计参数的值：

```mysql
# 显示SQL增删改查的次数，这些参数对于所有存储引擎的表操作都会进行累计
show status like 'Com_______';

# 这几个参数只是针对InnoDB 存储引擎的，累加的算法也略有不同。
show status like 'Innodb_rows_%';
```

Com_xxx 表示每个 xxx 语句执行的次数，我们通常比较关心的是以下几个统计参数。

| 参数                 | 含义                                                         |
| :------------------- | ------------------------------------------------------------ |
| Com_select           | 执行 select 操作的次数，一次查询只累加 1。                   |
| Com_insert           | 执行 INSERT 操作的次数，对于批量插入的 INSERT 操作，只累加一次。 |
| Com_update           | 执行 UPDATE 操作的次数。                                     |
| Com_delete           | 执行 DELETE 操作的次数。                                     |
| Innodb_rows_read     | select 查询返回的行数。                                      |
| Innodb_rows_inserted | 执行 INSERT 操作插入的行数。                                 |
| Innodb_rows_updated  | 执行 UPDATE 操作更新的行数。                                 |
| Innodb_rows_deleted  | 执行 DELETE 操作删除的行数。                                 |
| Connections          | 试图连接 MySQL 服务器的次数。                                |
| Uptime               | 服务器工作时间。                                             |
| Slow_queries         | 慢查询的次数。                                               |

## 4.2. 定位低效率执行SQL

可以通过以下两种方式定位执行效率较低的SQL语句。

- **慢查询日志**：通过慢查询日志定位那些执行效率较低的 SQL 语句，用--log-slow-queries[=file_name]选项启动时，mysqld 写一个包含所有执行时间超过 long_query_time 秒的 SQL 语句的日志文件。具体可以查看本书第 26 章中日志管理的相关部分。
- **show processlist**：慢查询日志在查询结束以后才纪录，所以在应用反映执行效率出现问题的时候查询慢查询日志并不能定位问题，可以使用show processlist命令查看**当前MySQL在进行的线程**，包括线程的状态、是否锁表等，可以实时地查看 SQL 的执行情况，同时对一些锁表操作进行优化。

![1556098544349](./images/mysql-advanced-12.png)

1. id列，用户登录mysql时，系统分配的"connection_id"，可以使用函数connection_id()查看
2. user列，显示当前用户。如果不是root，这个命令就只显示用户权限范围的sql语句
3. host列，显示这个语句是从哪个ip的哪个端口上发的，可以用来跟踪出现问题语句的用户
4. db列，显示这个进程目前连接的是哪个数据库

5. command列，显示当前连接的执行的命令，一般取值为休眠（sleep），查询（query），连接（connect）等
6. time列，显示这个状态持续的时间，单位是秒
7. state列，显示使用当前连接的sql语句的状态，很重要的列。state描述的是语句执行中的某一个状态。一个sql语句，以查询为例，可能需要经过copying to tmp table、sorting result、sending data等状态才可以完成

8. info列，显示这个sql语句，是判断问题语句的一个重要依据

## 4.3. explain分析执行计划

通过以上步骤查询到效率低的 SQL 语句后，可以通过 **EXPLAIN**或者 **DESC**命令获取 MySQL如何执行 SELECT 语句的信息，包括在 SELECT 语句执行过程中表如何连接和连接的顺序。

查询SQL语句的执行计划 ： 

```sql
explain select * from tb_item where id = 1;
```

![1552487489859](./images/mysql-advanced-13.png)

```sql
explain  select * from tb_item where title = '阿尔卡特 (OT-979) 冰川白 联通3G手机3';
```

![1552487526919](./images/mysql-advanced-14.png)

| 字段          | 含义                                                         |
| ------------- | ------------------------------------------------------------ |
| id            | select查询的序列号，是一组数字，表示的是查询中执行select子句或者是操作表的顺序。 |
| select_type   | 表示 SELECT 的类型，常见的取值有 SIMPLE（简单表，即不使用表连接或者子查询）、PRIMARY（主查询，即外层的查询）、UNION（UNION 中的第二个或者后面的查询语句）、SUBQUERY（子查询中的第一个 SELECT）等 |
| table         | 输出结果集的表                                               |
| type          | 表示表的连接类型，性能由好到差的连接类型为( system  --->  const  ----->  eq_ref  ------>  ref  ------->  ref_or_null---->  index_merge  --->  index_subquery  ----->  range  ----->  index  ------> all ) |
| possible_keys | 表示查询时，可能使用的索引                                   |
| key           | 表示实际使用的索引                                           |
| key_len       | 索引字段的长度                                               |
| rows          | 扫描行的数量                                                 |
| extra         | 执行情况的说明和描述                                         |

### 4.3.1. explain 之 id

id 字段是 select查询的序列号，是一组数字，表示的是查询中**执行select子句或者是操作表的顺序**。id 情况有三种 ： 

1. **id相同**表示加载表的顺序是从上到下

```mysql
explain select * from t_role r, t_user u, user_role ur where r.id = ur.role_id and u.id = ur.user_id ;
```

![1556102471304](./images/mysql-advanced-15.png)



2. **id 不同id值越大**，优先级越高，越先被执行。 

``` SQL
EXPLAIN SELECT * FROM t_role WHERE id = (SELECT role_id FROM user_role WHERE user_id = (SELECT id FROM t_user WHERE username = 'stu1'))
```

![1556103009534](./images/mysql-advanced-16.png)



3. **id 有相同，也有不同，同时存在**。id相同的可以认为是一组，从上往下顺序执行；在所有的组中，id的值越大，优先级越高，越先执行。

```sql 
EXPLAIN SELECT * FROM t_role r , (SELECT * FROM user_role ur WHERE ur.`user_id` = '2') a WHERE r.id = a.role_id ; 
```

![1556103294182](./images/mysql-advanced-17.png)

### 4.3.2. explain 之 select_type

 表示 SELECT 的类型，常见的取值（select_type从上往下，效率越来越慢，也就是说SIMPLE效率最高），如下表所示：

| select_type  | 含义                                                         |
| ------------ | ------------------------------------------------------------ |
| SIMPLE       | 简单的select查询，查询中不包含子查询或者UNION                |
| PRIMARY      | 查询中若包含任何复杂的子查询，最外层查询标记为该标识         |
| SUBQUERY     | 在SELECT 或 WHERE 列表中包含了子查询                         |
| DERIVED      | 在FROM 列表中包含的子查询，被标记为 DERIVED（衍生） MYSQL会递归执行这些子查询，把结果放在临时表中 |
| UNION        | 若第二个SELECT出现在UNION之后，则标记为UNION ； 若UNION包含在FROM子句的子查询中，外层SELECT将被标记为 ： DERIVED |
| UNION RESULT | 从UNION表获取结果的SELECT                                    |

### 4.3.3. explain 之 table

展示这一行的数据是关于哪一张表的 

### 4.3.4. explain 之 type

type 显示的是访问类型，是较为重要的一个指标，可取值为： 

| type   | 含义                                                         |
| ------ | ------------------------------------------------------------ |
| NULL   | MySQL不访问任何表，索引，直接返回结果。例如Select now();     |
| system | 表(from后的表)**只有一行记录**(等于系统表)，这是const类型的特例，一般不会出现 |
| const  | 表示通过**primary key 或者 unique 索引**一次就查询到了。因为只匹配一行数据，所以很快。如将主键置于where列表中，MySQL 就能将该查询转换为一个常亮。const于将 "主键" 或 "唯一" 索引的所有部分与常量值进行比较 |
| eq_ref | 类似ref，区别在于使用的是**唯一索引，使用主键的关联查询，关联查询出的记录只有一条**。常见于主键或唯一索引扫描 |
| ref    | **非唯一性索引**扫描（可能需要创建非唯一索引），**返回匹配某个单独值的所有行**。本质上也是一种索引访问，返回所有匹配某个单独值的所有行（**多个**） |
| range  | 只检索给定返回的行，使用一个索引来选择行。 where 之后出现 between ， < , > , in 等操作。 |
| index  | index 与 ALL的区别为  **index 类型只是遍历了索引树**， 通常比ALL 快， ALL 是遍历数据文件。例如Select id from t_user，遍历了主键索引树。 |
| all    | 将遍历全表以找到匹配的行                                     |

结果值从最好到最坏以此是：

```
NULL > system > const > eq_ref > ref > fulltext > ref_or_null > index_merge > unique_subquery > index_subquery > range > index > ALL


system > const > eq_ref > ref > range > index > ALL
```

**一般来说， 我们需要保证查询至少达到 range 级别， 最好达到ref**。

### 4.3.5. explain 之  key

- possible_keys：显示可能应用在这张表的索引， 一个或多个。 

- key：实际使用的索引， 如果为NULL，则没有使用索引。

- key_len：表示索引中使用的字节数，该值为索引字段最大可能长度，并非实际使用长度，在不损失精确性的前提下，**长度越短越好，越短执行效率越高**。

### 4.3.6. explain 之 rows

扫描行的数量（有索引可能只扫描一行，效果高，没有索引一般会扫描全表）。

### 4.3.7. explain 之 extra

其他的额外的执行计划信息，在该列展示 。

| extra                          | 含义                                                         |
| ------------------------------ | ------------------------------------------------------------ |
| using  filesort（需要被优化）  | 说明mysql会对数据使用一个外部的索引排序，而不是按照表内的索引顺序进行读取， 称为 **“文件排序”, 效率低**。例如select password from user order by password; 其中password没有建立索引 |
| using  temporary（需要被优化） | 使用了临时表保存中间结果，MySQL在对查询结果排序时使用临时表。常见于 order by 和 group by； 效率低。例如select * from user order by password; 其中password没有建立索引 |
| using  index（需要保持）       | 表示相应的select操作使用了覆盖索引， 避免访问表的数据行， 效率不错。例如select name from user order by name; 其中name建立了索引 |

## 4.4. show profile分析SQL

Mysql从5.0.37版本开始增加了对 show profiles 和 show profile 语句的支持。show profiles 能够在做SQL优化时帮助我们了解时间都耗费到哪里去了。

通过 have_profiling 参数(`select @@have_profiling;`)，能够看到当前MySQL是否支持profile。 

默认profiling是关闭的(`select @@have_profiling;`)，可以**通过set语句在Session级别开启profiling**。 

```sql
set profiling=1; //开启profiling 开关(默认session级别)；
```



通过profile，我们能够更清楚地了解SQL执行的过程。

首先，我们可以执行一系列的操作，如下图所示：

```sql
show databases;
use db01;
show tables;

select * from tb_item where id < 5;
select count(*) from tb_item;
```

执行完上述命令之后，再执行show profiles 指令， 来查看SQL语句执行的耗时：

![1552489017940](./images/mysql-advanced-18.png)

通过**`show profile for query <query_id>`**语句可以查看到该SQL执行过程中**每个线程的状态（Status）**和**消耗的时间（Duration）**，query_id是上面查询得到的：

![1552489053763](./images/mysql-advanced-19.png)

**注意**：**Sending data**状态表示MySQL线程**开始访问数据行并把结果返回给客户端，而不仅仅是返回个客户端**。由于在Sending data状态下，MySQL线程往往需要做大量的磁盘读取操作，所以经常是整各查询中耗时最长的状态。



在获取到最消耗时间的线程状态后，MySQL支持进一步选择all、cpu、block io 、context switch、page faults等明细类型类查看MySQL在使用什么资源上耗费了过高的时间。例如，选择查看CPU的耗费时间（过**`show profile all for query <query_id>`**）：

![1552489671119](./images/mysql-advanced-20.png)

| 字段       | 含义                           |
| ---------- | ------------------------------ |
| Status     | sql 语句执行的状态             |
| Duration   | sql 执行过程中每一个步骤的耗时 |
| CPU_user   | **当前用户**占有的cpu          |
| CPU_system | **系统**占有的cpu              |

## 4.5. trace分析优化器执行计划

MySQL5.6提供了对SQL的跟踪trace, 通过trace文件能够进一步了解为什么优化器选择A计划，而不是选择B计划。

打开trace ， 设置格式为 JSON，并设置trace最大能够使用的内存大小，避免解析过程中因为默认内存过小而不能够完整展示。

```sql
# 执行下面的指令开启trace分析优化器
SET optimizer_trace="enabled=on", end_markers_in_json=on;
SET optimizer_trace_max_mem_size=1000000;
```

执行SQL语句 ：

```sql
select * from tb_item where id < 4;
```

最后， 检查information_schema.optimizer_trace就可以知道MySQL是如何执行SQL的 ：

```sql
select * from information_schema.optimizer_trace;

# 其中QUERY是原SQL语句，expanded_query是经过优化器优化后实际执行的SQL语句
```

```json
*************************** 1. row ***************************
QUERY: select * from tb_item where id < 4
TRACE: {
  "steps": [
    {
      "join_preparation": {
        "select#": 1,
        "steps": [
          {
            "expanded_query": "/* select#1 */ select `tb_item`.`id` AS `id`,`tb_item`.`title` AS `title`,`tb_item`.`price` AS `price`,`tb_item`.`num` AS `num`,`tb_item`.`categoryid` AS `categoryid`,`tb_item`.`status` AS `status`,`tb_item`.`sellerid` AS `sellerid`,`tb_item`.`createtime` AS `createtime`,`tb_item`.`updatetime` AS `updatetime` from `tb_item` where (`tb_item`.`id` < 4)"
          }
        ] /* steps */
      } /* join_preparation */
    },
    {
      "join_optimization": {
        "select#": 1,
        "steps": [
          {
            "condition_processing": {
              "condition": "WHERE",
              "original_condition": "(`tb_item`.`id` < 4)",
              "steps": [
                {
                  "transformation": "equality_propagation",
                  "resulting_condition": "(`tb_item`.`id` < 4)"
                },
                {
                  "transformation": "constant_propagation",
                  "resulting_condition": "(`tb_item`.`id` < 4)"
                },
                {
                  "transformation": "trivial_condition_removal",
                  "resulting_condition": "(`tb_item`.`id` < 4)"
                }
              ] /* steps */
            } /* condition_processing */
          },
          {
            "table_dependencies": [
              {
                "table": "`tb_item`",
                "row_may_be_null": false,
                "map_bit": 0,
                "depends_on_map_bits": [
                ] /* depends_on_map_bits */
              }
            ] /* table_dependencies */
          },
          {
            "ref_optimizer_key_uses": [
            ] /* ref_optimizer_key_uses */
          },
          {
            "rows_estimation": [
              {
                "table": "`tb_item`",
                "range_analysis": {
                  "table_scan": {
                    "rows": 9816098,
                    "cost": 2.04e6
                  } /* table_scan */,
                  "potential_range_indices": [
                    {
                      "index": "PRIMARY",
                      "usable": true,
                      "key_parts": [
                        "id"
                      ] /* key_parts */
                    }
                  ] /* potential_range_indices */,
                  "setup_range_conditions": [
                  ] /* setup_range_conditions */,
                  "group_index_range": {
                    "chosen": false,
                    "cause": "not_group_by_or_distinct"
                  } /* group_index_range */,
                  "analyzing_range_alternatives": {
                    "range_scan_alternatives": [
                      {
                        "index": "PRIMARY",
                        "ranges": [
                          "id < 4"
                        ] /* ranges */,
                        "index_dives_for_eq_ranges": true,
                        "rowid_ordered": true,
                        "using_mrr": false,
                        "index_only": false,
                        "rows": 3,
                        "cost": 1.6154,
                        "chosen": true
                      }
                    ] /* range_scan_alternatives */,
                    "analyzing_roworder_intersect": {
                      "usable": false,
                      "cause": "too_few_roworder_scans"
                    } /* analyzing_roworder_intersect */
                  } /* analyzing_range_alternatives */,
                  "chosen_range_access_summary": {
                    "range_access_plan": {
                      "type": "range_scan",
                      "index": "PRIMARY",
                      "rows": 3,
                      "ranges": [
                        "id < 4"
                      ] /* ranges */
                    } /* range_access_plan */,
                    "rows_for_plan": 3,
                    "cost_for_plan": 1.6154,
                    "chosen": true
                  } /* chosen_range_access_summary */
                } /* range_analysis */
              }
            ] /* rows_estimation */
          },
          {
            "considered_execution_plans": [
              {
                "plan_prefix": [
                ] /* plan_prefix */,
                "table": "`tb_item`",
                "best_access_path": {
                  "considered_access_paths": [
                    {
                      "access_type": "range",
                      "rows": 3,
                      "cost": 2.2154,
                      "chosen": true
                    }
                  ] /* considered_access_paths */
                } /* best_access_path */,
                "cost_for_plan": 2.2154,
                "rows_for_plan": 3,
                "chosen": true
              }
            ] /* considered_execution_plans */
          },
          {
            "attaching_conditions_to_tables": {
              "original_condition": "(`tb_item`.`id` < 4)",
              "attached_conditions_computation": [
              ] /* attached_conditions_computation */,
              "attached_conditions_summary": [
                {
                  "table": "`tb_item`",
                  "attached": "(`tb_item`.`id` < 4)"
                }
              ] /* attached_conditions_summary */
            } /* attaching_conditions_to_tables */
          },
          {
            "refine_plan": [
              {
                "table": "`tb_item`",
                "access_type": "range"
              }
            ] /* refine_plan */
          }
        ] /* steps */
      } /* join_optimization */
    },
    {
      "join_execution": {
        "select#": 1,
        "steps": [
        ] /* steps */
      } /* join_execution */
    }
  ] /* steps */
}
```

# 5. 索引的使用

索引是数据库优化最常用也是最重要的手段之一，通过索引通常可以帮助用户解决大多数的MySQL的性能优化问题。

## 5.1. 验证索引提升查询效率

在我们准备的表结构tb_item 中， 一共存储了 300 万记录；

### 5.1.1. 根据ID查询

```
select * from tb_item where id = 1999;
```

![1553261992653](./images/mysql-advanced-21.png)

查询速度很快， 接近0s ， 主要的原因是因为id为主键， 有索引；

![1553262044466](./images/mysql-advanced-22.png)

### 5.1.2. 根据 title 进行精确查询

```SQL
select * from tb_item where title = 'iphoneX 移动3G 32G941'; 
```

![1553262215900](./images/mysql-advanced-23.png)

查看SQL语句的执行计划 ： 

![1553262469785](./images/mysql-advanced-24.png)

处理方案 ， 针对title字段， 创建索引：

```SQL
create index idx_item_title on tb_item(title);
```

![1553263229523](./images/mysql-advanced-25.png)



索引创建完成之后，再次进行查询：

![1553263302706](./images/mysql-advanced-26.png)

通过explain， 查看执行计划，执行SQL时使用了刚才创建的索引 

![1553263355262](./images/mysql-advanced-27.png)

## 5.2. 避免索引失效

### 5.2.1. 全值匹配 ，对索引中所有列都指定具体值。

条件顺序未修改情况下，索引生效，执行效率高。

```sql
explain select * from tb_seller where name='小米科技' and status='1' and address='北京市'; 
```

### 5.2.2. 最左前缀法则

如果索引了多列，要遵守最左前缀法则。指的是**查询从索引的最左前列开始，并且不跳过索引中的列**。

如果符合最左法则，但是出现跳跃某一列，只有最左列索引生效： 

### 5.2.3. 范围查询右边的列，不能使用索引 

根据前面的两个字段name ， status查询是走索引的， 但是最后一个条件address 没有用到索引。

```mysql
explain select * from tb_seller where name='小米科技' and status>'1' and address='北京市';
```

### 5.2.4. 不要在索引列上进行运算操作，索引将失效

```mysql
explain select * from tb_seller where sbstring(name,3,2)='小米科技'; 
```

### 5.2.5. 字符串不加单引号，造成索引失效。 

由于，在查询是整数类型，**没有对字符串加单引号，MySQL的查询优化器，会自动的进行类型转换，造成索引失效**。

### 5.2.6. 尽量使用覆盖索引，避免select *

尽量使用覆盖索引（只访问索引的查询（索引列完全包含查询列）），减少select *（**因为可能需要返回表中再次查询数据，因为索引中可能没有覆盖返回的所有列**）。 

如果查询列，超出索引列，也会降低性能。 

- using index ：使用覆盖索引的时候就会出现
- using where：在查找使用索引的情况下，需要回表去查询所需的数据
- using index condition：**查找使用了索引，但是需要回表查询数据，因为索引没有覆盖所有返回列**
- using index ; using where：查找使用了索引，但是需要的数据都在索引列中能找到，**所以不需要回表查询数据**

### 5.2.7. or前面的条件都会索引失效

用or分割开的条件， 如果or前的条件中的列有索引，而后面的列中没有索引，那么涉及的索引都不会被用到。

示例，name字段是索引列 ， 而createtime不是索引列，中间是or进行连接是不走索引的 ： 

```sql
explain select * from tb_seller where name='黑马程序员' or createtime = '2088-01-01 12:00:00';
```

### 5.2.8. 以%开头的Like模糊查询，索引失效

like '%**'，%开头的Like索引失效

like '**'，不是%开头的Like索引不失效 

解决方案 ： 

通过**覆盖索引(返回列字段都要创建索引)**来解决 

### 5.2.9. 如果MySQL评估使用索引比全表更慢，则不使用索引

比如某一列中几乎全是符合筛选条件的，这时候查询（即使这一列创建了单列索引）不会使用到索引，MySQL会自己优化。

### 5.2.10. IS NULL，IS NOT NULL索引失效。

is  NULL或者is NOT NULL查询的列几乎都是满足条件，索引失效（MySQL自动优化，不如放弃索引，而使用全表扫描）；反之则索引生效

### 5.2.11. in 走索引(一般符合数据少)，not in索引失效(一般符合数据多)。  

### 5.2.12. 单列索引和复合索引

尽量使用复合索引，而少使用单列索引

创建复合索引 

```mysql
create index idx_name_sta_address on tb_seller(name, status, address);

就相当于创建了三个索引 ： 
	name
	name + status
	name + status + address
```

创建单列索引 

```mysql
create index idx_seller_name on tb_seller(name);
create index idx_seller_status on tb_seller(status);
create index idx_seller_address on tb_seller(address);
```

数据库会选择一个最优的索引（**辨识度最高索引，少有重复数据**）来使用，并不会使用全部索引 

## 5.3. 查看索引使用情况

```sql
# 当前Session索引使用情况
show status like 'Handler_read%';

# 全局MySQL的索引使用情况
show global status like 'Handler_read%';
```

![1552885364563](./images/mysql-advanced-28.png)

- Handler_read_first：索引中第一条被读的次数。如果较高，表示服务器正执行大量全索引扫描（这个值越低越好）。

- Handler_read_key：如果索引正在工作，这个值代表一个行被索引值读的次数，如果值越低，表示索引得到的性能改善不高，因为索引不经常使用（这个值越高越好）。

- Handler_read_next ：按照键顺序读下一行的请求数。如果你用范围约束或如果执行索引扫描来查询索引列，该值增加。

- Handler_read_prev：按照键顺序读前一行的请求数。该读方法主要用于优化ORDER BY ... DESC。

- Handler_read_rnd ：根据固定位置读一行的请求数。如果你正执行大量查询并需要对结果进行排序该值较高。你可能使用了大量需要MySQL扫描整个表的查询或你的连接没有正确使用键。这个值较高，意味着运行效率低，应该建立索引来补救。

- Handler_read_rnd_next：在数据文件中读下一行的请求数。如果你正进行大量的表扫描，该值较高。通常说明你的表索引不正确或写入的查询没有利用索引。

# 6. SQL优化

## 6.1. 优化大批量插入数据

对于 InnoDB 类型的表，有以下几种方式可以提高导入的效率：

### 6.1.1. 主键顺序插入

因为InnoDB类型的表是按照主键的顺序保存的，所以将导入的数据**按照主键的顺序排列，可以有效的提高导入数据的效率**。如果InnoDB表没有主键，那么系统会自动默认创建一个内部列作为主键，所以如果可以给表创建一个主键，将可以利用这点，来提高导入数据的效率。

```
脚本文件介绍 :
	sql1.log  ----> 主键有序
	sql2.log  ----> 主键无序
```

### 6.1.2. 关闭唯一性校验

在导入数据前执行SET UNIQUE_CHECKS=0，关闭唯一性校验；

在导入结束后执行SET UNIQUE_CHECKS=1，恢复唯一性校验，可以提高导入的效率。 

### 6.1.3. 手动提交事务

如果应用使用自动提交的方式，建议在导入前执行SET AUTOCOMMIT=0，关闭自动提交；

导入结束后再执行SET AUTOCOMMIT=1，打开自动提交，也可以提高导入的效率。

原因是没插入一条就会自动提交事务，不如等全部插入完，手动一次性提交全部事务。

## 6.2. 优化insert语句

当进行数据的insert操作的时候，可以考虑采用以下几种优化方案。

### 6.2.1. 尽可能批量插入

如果需要同时对一张表插入很多行数据时，应该尽量使用多个值表的insert语句，这种方式将大大的缩减客户端与数据库之间的连接、关闭等消耗。使得效率比分开执行的单个insert语句快。

示例， 原始方式为：

```sql
insert into tb_test values(1,'Tom');
insert into tb_test values(2,'Cat');
insert into tb_test values(3,'Jerry');
```

优化后的方案为 ： 

```sql
insert into tb_test values(1,'Tom'),(2,'Cat')，(3,'Jerry');
```

### 6.2.2. 在事务中进行数据插入

```sql
start transaction;
insert into tb_test values(1,'Tom');
insert into tb_test values(2,'Cat');
insert into tb_test values(3,'Jerry');
commit;
```

### 6.2.3. 数据有序插入

```sql
insert into tb_test values(4,'Tim');
insert into tb_test values(1,'Tom');
insert into tb_test values(3,'Jerry');
insert into tb_test values(5,'Rose');
insert into tb_test values(2,'Cat');
```

优化后

```sql
insert into tb_test values(1,'Tom');
insert into tb_test values(2,'Cat');
insert into tb_test values(3,'Jerry');
insert into tb_test values(4,'Tim');
insert into tb_test values(5,'Rose');
```

## 6.3. 优化order by语句

### 6.3.1. 两种排序方式

第一种是通过对返回数据进行排序，就是通常说的**FileSort排序**，所有不是通过索引直接返回排序结果的排序，通过文件系统排序。

第二种通过有序索引顺序扫描直接返回**有序数据**，这种情况即为**using index**，不需要额外排序，操作效率高。 



了解了MySQL的排序方式，优化目标就清晰了：尽量减少额外的排序，通过索引直接返回有序数据（返回的列数据都创建了索引）。where条件和Order by使用相同的索引，并且**Order By 的顺序和符合索引字段顺序相同**， 并且**Order  by 的字段都是升序，或者都是降序**。否则肯定需要额外的操作，这样就会出现FileSort。

### 6.3.2. Filesort的优化

通过创建合适的索引，能够减少 Filesort 的出现，但是在某些情况下，条件限制不能让Filesort消失，那就需要加快 Filesort的排序操作。对于Filesort ， MySQL 有两种排序算法：

1. 两次扫描算法 ：MySQL4.1 之前，使用该方式排序。首先根据条件取出排序字段和行指针信息，然后在排序区 sort buffer 中排序，如果sort buffer不够，则在临时表 temporary table 中存储排序结果。完成排序之后，再根据行指针回表读取记录，该操作可能会导致大量随机I/O操作。

2. 一次扫描算法：一次性取出满足条件的所有字段，然后在排序区 sort  buffer 中排序后直接输出结果集。排序时内存开销较大，但是排序效率比两次扫描算法要高。




MySQL4.1之后，MySQL 通过比较系统变量 max_length_for_sort_data 的大小和Query语句取出的字段总大小， 来判定是否那种排序算法，如果max_length_for_sort_data 更大，那么使用第二种优化之后的算法；否则使用第一种。

可以适当提高 sort_buffer_size  和 max_length_for_sort_data  系统变量，来增大排序区的大小，提高排序的效率。 

## 6.4. 优化group by语句

由于GROUP BY 实际上也同样会进行排序操作，而且与ORDER BY 相比，GROUP BY 主要只是多了排序之后的分组操作。当然，如果在分组的时候还使用了其他的一些聚合函数，那么还需要一些聚合函数的计算。所以，在GROUP BY 的实现过程中，与 ORDER BY 一样也可以利用到索引。

如果查询包含 group by 但是用户想要避免排序结果的消耗， 则**可以执行order by null禁止排序**。如下 ：

```SQL
drop index idx_emp_age_salary on emp;

explain select age,count(*) from emp group by age;
```

![1556339573979](./images/mysql-advanced-29.png)

优化后

```sql
explain select age,count(*) from emp group by age order by null;
```

![1556339633161](./images/mysql-advanced-30.png)

从上面的例子可以看出，第一个SQL语句需要进行"filesort"，而第二个SQL由于order  by  null不需要进行 "filesort"， 而上文提过Filesort往往非常耗费时间。



这时候Using temporary的效率也不高，可以通过创建索引来优化：

```SQL
create index idx_emp_age_salary on emp(age,salary)； 
```

## 6.5. 优化嵌套查询

Mysql4.1版本之后，开始支持SQL的子查询。这个技术可以使用SELECT语句来创建一个单列的查询结果，然后把这个结果作为过滤条件用在另一个查询中。使用子查询可以一次性的完成很多逻辑上需要多个步骤才能完成的SQL操作，同时也可以避免事务或者表锁死，并且写起来也很容易。但是，有些情况下，**子查询是可以被更高效的连接（JOIN）替代**。

连接(Join)查询之所以更有效率一些 ，是因为MySQL不需要在内存中创建临时表来完成这个逻辑上需要两个步骤的查询工作。

## 6.6. 优化OR条件

对于包含OR的查询子句，如果要利用索引，则OR之间的每个条件列都必须用到索引 ， 而且**不能使用到复合索引**； 如果没有索引，则应该考虑增加索引。

建议使用 union 替换并优化 or。 

![1556355027728](./images/mysql-advanced-31.png)

我们来比较下重要指标，发现主要差别是 type 和 ref 这两项

type 显示的是访问类型，是较为重要的一个指标，结果值从好到坏依次是：

```
system > const > eq_ref > ref > fulltext > ref_or_null > index_merge > unique_subquery > index_subquery > range > index > ALL
```

UNION 语句的 type 值为 ref，OR 语句的 type 值为 range，可以看到这是一个很明显的差距

这两项的差距就说明了 UNION 要优于 OR 。

## 6.7. 优化分页查询

一般分页查询时，通过创建**覆盖索引**能够比较好地提高性能。一个常见又非常头疼的问题就是 limit 2000000,10  ，此时需要MySQL排序前2000010 记录，仅仅返回2000000 - 2000010 的记录，其他记录丢弃，查询排序的代价非常大 。 

### 6.7.1. 优化思路一

在索引上完成排序分页操作，最后根据主键关联回原表查询所需要的其他列内容。

```mysql
# 通过主键索引排序
select * from t_table t, (select id from t_table order by 2000000,10) o where t.id=a.id; 
```

### 6.7.2. 优化思路二

该方案适用于**主键自增并且不能够出现断层的表**，可以把Limit 查询转换成某个位置的查询。

```mysql
# 通过主键索引排序
select * from t_table where id > '2000000' limit '10';
```

## 6.8. 使用SQL提示使用索引

### 6.8.1. USE INDEX

在查询语句中表名的后面，添加 use index 来**提供希望MySQL去参考的索引列表**，**就可以让MySQL不再考虑其他可用的索引**。

```mysql
create index idx_seller_name on tb_seller(name);
select * from t_table use index(idx_seller_name) where name='小米科技'; 
```

### 6.8.2. IGNORE INDEX

如果用户只是单纯的**想让MySQL忽略一个或者多个索引**，则可以使用 ignore index 作为 hint 。

```
 explain select * from tb_seller ignore index(idx_seller_name) where name = '小米科技'; 
```

### 6.8.3. FORCE INDEX

**强制让MySQL使用一个特定的索引**，可在查询中使用 force index 作为hint 。 

``` SQL
create index idx_seller_address on tb_seller(address);
select * from t_table force index(idx_seller_address) where address='北京市';
```

# 7. 应用优化

## 7.1. 使用连接池

对于访问数据库来说，建立连接的代价是比较昂贵的，因为我们频繁的创建关闭连接，是比较耗费资源的，我们有必要**建立数据库连接池，以提高访问的性能**。

## 7.2. 减少对MySQL的访问

### 7.2.1. 避免对数据进行重复检索

在编写应用代码时，需要能够理清对数据库的访问逻辑。能够一次连接就获取到结果的，就不用两次连接，这样可以大大减少对数据库无用的重复请求。

### 7.2.2. 增加cache层

在应用中，我们可以在应用中增加**缓存层**来达到减轻数据库负担的目的。缓存层有很多种，也有很多实现方式，只要能达到降低数据库的负担又能满足应用需求就可以。

## 7.3. 负载均衡 

负载均衡是应用中使用非常普遍的一种优化方法，它的机制就是利用某种均衡算法，将固定的负载量分布到不同的服务器上，以此来降低单台服务器的负载，达到优化的效果。

### 7.3.1. 利用MySQL复制分流查询

通过MySQL的主从复制，实现读写分离，使**增删改操作走主节点**，**查询操作走从节点**，从而可以降低单台服务器的读写压力。

### 7.3.2. 采用分布式数据库架构

分布式数据库架构适合大数据量、负载高的情况，它有良好的拓展性和高可用性。通过在多台服务器之间分布数据，可以实现在多台服务器之间的负载均衡，提高访问效率。

# 8. Mysql内存管理及优化

## 8.1. 内存优化原则

1. 将尽量多的内存分配给MySQL做缓存，但要给操作系统和其他程序预留足够内存。

2. MyISAM 存储引擎的数据文件读取依赖于操作系统自身的IO缓存，因此，如果有MyISAM表，就要预留更多的内存给操作系统做IO缓存。

3. 排序区、连接区等缓存是分配给每个数据库会话（session）专用的，其默认值的设置要根据最大连接数合理分配，如果设置太大，不但浪费资源，而且在并发连接较高时会导致物理内存耗尽。


## 8.2. MyISAM 内存优化

myisam存储引擎使用 key_buffer 缓存索引块，加速MyISAM索引的读写速度。

对于MyISAM表的数据块，mysql没有特别的缓存机制，完全依赖于操作系统的IO缓存。

### 8.2.1. key_buffer_size

key_buffer_size决定MyISAM索引块缓存区的大小，直接**影响到MyISAM表的存取效率**。可以在MySQL参数文件中设置key_buffer_size的值，对于一般MyISAM数据库，建议至少将1/4可用内存分配给key_buffer_size。

在/usr/my.cnf中做如下配置：

```
key_buffer_size=512M
```

### 8.2.2. read_buffer_size

如果**需要经常顺序扫描MyISAM表**，可以通过增大read_buffer_size的值来改善性能。但需要注意的是read_buffer_size是每个session独占的，如果默认值设置太大，就会造成内存浪费。

### 8.2.3. read_rnd_buffer_size

对于**需要做排序的MyISAM表的查询**，如带有order by子句的sql，适当增加read_rnd_buffer_size的值，可以改善此类的sql性能。但需要注意的是read_rnd_buffer_size是每个session独占的，如果默认值设置太大，就会造成内存浪费。

## 8.3. InnoDB 内存优化

innodb用一块内存区做IO缓存池，该缓存池不仅用来缓存innodb的索引块，而且也用来缓存innodb的数据块。

### 8.3.1. innodb_buffer_pool_size

该变量决定了 innodb 存储引擎表数据和索引数据的最大缓存区大小。在保证操作系统及其他程序有足够内存可用的情况下，**innodb_buffer_pool_size 的值越大，缓存命中率越高，访问InnoDB表需要的磁盘I/O 就越少，性能也就越高**。

```
innodb_buffer_pool_size=512M
```

### 8.3.2. innodb_log_buffer_size

决定了innodb重做日志缓存的大小，对于**可能产生大量更新记录的大事务，增加innodb_log_buffer_size的大小**，可以避免innodb在事务提交前就执行不必要的日志写入磁盘操作。

```
innodb_log_buffer_size=10M
```

# 9. Mysql并发参数调整

从实现上来说，MySQL Server 是多线程结构，包括后台线程和客户服务线程。多线程可以有效利用服务器资源，提高数据库的并发性能。在Mysql中，控制并发连接和线程的主要参数如下。

## 9.1. max_connections

采用**max_connections（select @@max_connections）**控制允许连接到MySQL数据库的最大数量，默认值是151。

如果状态变量**connection_errors_max_connections**不为零，并且一直增长，则说明不断有连接请求因数据库连接数已达到允许最大值而失败，这是可以考虑增大max_connections的值。

Mysql 最大可支持的连接数，取决于很多因素，包括给定操作系统平台的线程库的质量、内存大小、每个连接的负荷、CPU的处理速度，期望的响应时间等。在Linux 平台下，性能好的服务器，支持 500-1000 个连接不是难事，需要根据服务器性能进行评估设定。

## 9.2. back_log

back_log 参数控制MySQL监听TCP端口时设置的积压请求栈大小。**如果MySql的连接数达到max_connections时，新来的请求将会被存在堆栈中，以等待某一连接释放资源，该堆栈的数量即back_log**，如果等待连接的数量超过back_log，将不被授予连接资源，将会报错。5.6.6 版本之前默认值为 50 ， 之后的版本默认为 151。

如果需要数据库在较短的时间内处理大量连接请求， 可以考虑适当增大back_log 的值。

## 9.3. table_open_cache

该参数用来控制所有SQL语句执行线程可打开表缓存的数量（默认值是4000），而在执行SQL语句时，每一个SQL执行线程至少要打开 1 个表缓存。该参数的值应该根据设置的最大连接数 max_connections 以及每个连接执行关联查询中涉及的表的最大数量来设定 ：

```
max_connections*N;
```

## 9.4. thread_cache_size

为了加快连接数据库的速度，MySQL 会缓存一定数量的**客户服务线程**以备重用，通过参数 thread_cache_size（默认值是9）可控制 MySQL 缓存客户服务线程的数量。

## 9.5. innodb_lock_wait_timeout

该参数是用来设置InnoDB事务等待行锁的时间，默认值是50ms，可以根据需要进行动态设置。

对于**需要快速反馈的业务系统**来说，可以将行锁的等待时间调小，以避免事务长时间挂起； 

对于**后台运行的批量处理程序**来说，可以将行锁的等待时间调大，以避免发生大的回滚操作。

# 10. Mysql锁问题

## 10.1. 锁概述

锁是计算机协调多个进程或线程，并发访问某一资源的机制（避免争抢）。

在数据库中，除传统的计算资源（如 CPU、RAM、I/O 等）的争用以外，数据也是一种供许多用户共享的资源。如何保证数据并发访问的一致性、有效性是所有数据库必须解决的一个问题，锁冲突也是影响数据库并发访问性能的一个重要因素。从这个角度来说，锁对数据库而言显得尤其重要，也更加复杂。

## 10.2. 锁分类

从对数据操作的粒度分：

1. **表锁**：操作时，会锁定整个表。
2. **行锁**：操作时，会锁定当前操作行。

从对数据操作的类型分：

1. **读锁（共享锁）**：针对同一份数据，多个读操作可以同时进行而不会互相影响。
2. **写锁（排它锁）**：当前操作没有完成之前，它会阻断其他写锁和读锁。

## 10.3. Mysql 锁

相对其他数据库而言，MySQL的锁机制比较简单，其最显著的特点是不同的存储引擎支持不同的锁机制。下表中罗列出了各存储引擎对锁的支持情况：

| 存储引擎 | 表级锁 | 行级锁 | 页面锁 |
| -------- | ------ | ------ | ------ |
| MyISAM   | 支持   | 不支持 | 不支持 |
| InnoDB   | 支持   | 支持   | 不支持 |
| MEMORY   | 支持   | 不支持 | 不支持 |
| BDB      | 支持   | 不支持 | 支持   |

MySQL这3种锁的特性可大致归纳如下 ：

| 锁类型 | 特点                                                         |
| ------ | ------------------------------------------------------------ |
| 表级锁 | 偏向MyISAM存储引擎，开销小，加锁快；不会出现死锁；锁定粒度大，发生锁冲突的概率最高，并发度最低。 |
| 行级锁 | 偏向InnoDB存储引擎，开销大，加锁慢；会出现死锁；锁定粒度最小，发生锁冲突的概率最低，并发度也最高。 |
| 页面锁 | 开销和加锁时间界于表锁和行锁之间；会出现死锁；锁定粒度界于表锁和行锁之间，并发度一般。 |

具体应用的特点来来选择锁！仅从锁的角度来说：

表级锁更适合于以查询为主，只有少量按索引条件更新数据的应用，如Web应用；

而行级锁则更适合于有大量按索引条件并发更新少量不同数据，同时又有并查询的应用，如一些在线事务处理（OLTP）系统。


## 10.4. MyISAM表锁

MyISAM存储引擎只支持表锁，这也是MySQL开始几个版本中唯一支持的锁类型。

### 10.4.1. 如何加表锁

MyISAM 在执行查询语句（SELECT）前，会自动给涉及的所有表加读锁，在执行更新操作（UPDATE、DELETE、INSERT 等）前，会自动给涉及的表加写锁，这个过程并不需要用户干预，因此，用户一般不需要直接用 LOCK TABLE 命令给 MyISAM 表显式加锁。

显示加表锁语法：

```SQL
加读锁 ： lock table table_name read;
加写锁 ： lock table table_name write;
```

锁模式的相互兼容性如表中所示：

![1553905621992](./images/mysql-advanced-32.png)

由上表可见： 

1. 对MyISAM 表的读操作，不会阻塞其他用户对同一表的读请求，但会阻塞对同一表的写请求；

2. 对MyISAM 表的写操作，则会阻塞其他用户对同一表的读和写操作；


简而言之，就是**读锁会阻塞写，但是不会阻塞读**。而**写锁，则既会阻塞读，又会阻塞写**。

此外，MyISAM的读写锁调度是**写优先**，**这也是MyISAM不适合做写为主的表的存储引擎的原因**。因为写锁后，其他线程不能做任何操作，大量的更新会使查询很难得到锁，从而造成永远阻塞。

### 10.4.2. 查看锁的争用情况

``` mysql
show open tables;
```

![1556443073322](./images/mysql-advanced-33.png)

- In_user：表当前被查询使用的次数。如果该数为零，则表是打开的，但是当前没有被使用。

- Name_locked：**表名称**是否被锁定。名称锁定用于**取消表**或**对表进行重命名**等操作。


```mysql
# 查看表锁定情况
show status like 'Table_locks%';
```

![1556443170082](./images/mysql-advanced-34.png)

- Table_locks_immediate：指的是能够立即获得表级锁的次数（而不处于阻塞状态），每立即获取锁，值加1。

- Table_locks_waited：指的是不能立即获取表级锁而需要等待的次数，每等待一次，该值加1，此值高说明存在着较为严重的表级锁争用情况。


## 10.5. InnoDB 行锁

### 10.5.1. 行锁介绍

行锁特点 ：偏向InnoDB 存储引擎，开销大，加锁慢；会出现死锁；锁定粒度最小，发生锁冲突的概率最低，并发度也最高。

InnoDB 与MyISAM 的最大不同有两点：一是**支持事务**；二是采用了**行级锁**。

### 10.5.2. 背景知识

#### 10.5.2.1. 事务及其ACID属性

事务是由一组SQL语句组成的逻辑处理单元。

事务具有以下4个特性，简称为事务ACID属性。

| ACID属性             | 含义                                                         |
| -------------------- | ------------------------------------------------------------ |
| 原子性（Atomicity）  | 事务是一个原子操作单元，其对数据的修改，要么全部成功，要么全部失败。 |
| 一致性（Consistent） | 在事务开始和完成时，数据都必须保持一致状态。                 |
| 隔离性（Isolation）  | 数据库系统提供一定的隔离机制，保证事务在不受外部并发操作影响的 “独立” 环境下运行。 |
| 持久性（Durable）    | 事务完成之后，对于数据的修改是永久的。                       |

#### 10.5.2.2. 并发事务处理带来的问题

| 问题                               | 含义                                                         |
| ---------------------------------- | ------------------------------------------------------------ |
| 丢失更新（Lost Update）            | 当两个或多个事务选择同一行，最初的事务修改的值，会**被后面的事务修改的值覆盖**。 |
| 脏读（Dirty Reads）                | 当一个事务正在访问数据，并且对数据进行了修改，而这种修改还没有提交到数据库中，这时，另外一个事务也访问这个数据，然后使用了这个数据。 |
| 不可重复读（Non-Repeatable Reads） | 一个事务在读取某些数据后的某个时间，再次读取以前读过的数据，却发现和以前读出的数据不一致。 |
| 幻读（Phantom Reads）              | 一个事务按照相同的查询条件重新读取以前查询过的数据，却发现其他事务插入了满足其查询条件的新数据。 |

#### 10.5.2.3. 事务隔离级别

为了解决上述提到的事务并发问题，数据库提供一定的事务隔离机制来解决这个问题。

数据库的事务隔离越严格，并发副作用越小，但付出的代价也就越大，因为事务隔离实质上就是使用事务在一定程度上“串行化” 进行，这显然与“并发” 是矛盾的。 

数据库的隔离级别有4个，由低到高依次为Read uncommitted、Read committed、Repeatable read、Serializable，这四个级别可以逐个解决脏写、脏读、不可重复读、幻读这几类问题。

| 隔离级别                | 丢失更新 | 脏读 | 不可重复读 | 幻读 |
| ----------------------- | -------- | ---- | ---------- | ---- |
| Read uncommitted        | ×        | √    | √          | √    |
| Read committed          | ×        | ×    | √          | √    |
| Repeatable read（默认） | ×        | ×    | ×          | √    |
| Serializable            | ×        | ×    | ×          | ×    |

Mysql的数据库的默认隔离级别为Repeatable read，查看方式：

```mysql
show variables like '%transaction_isolation%';
```

### 10.5.3. InnoDB 的行锁模式

InnoDB  实现了以下两种类型的行锁。

- 共享锁（S，Share）：又称为读锁，简称S锁，共享锁就是多个事务对于同一数据可以共享一把锁，都能访问到数据，但是只能读不能修改。
- 排他锁（X，Exclusive）：又称为写锁，简称X锁，排他锁就是不能与其他锁并存，如一个事务获取了一个数据行的排他锁，其他事务就不能再获取该行的**其他锁**，包括共享锁和排他锁，但是获取排他锁的事务是可以对数据就行读取和修改。

对于UPDATE、DELETE和INSERT语句，InnoDB会**自动给涉及数据集加排他锁（X)**；

对于普通SELECT语句，InnoDB不会加任何锁；

可以通过以下语句**显示给记录集加共享锁或排他锁**。

```
共享锁（S）：SELECT * FROM table_name WHERE ... LOCK IN SHARE MODE
排他锁（X) ：SELECT * FROM table_name WHERE ... FOR UPDATE
```

### 10.5.4. 无索引行锁升级为表锁

如果不通过索引条件（比如条件值没加引号，导致失效）检索数据，那么InnoDB将对表中的所有记录加锁，实际效果跟表锁一样，**行锁就升级为了表锁**，这样会使使用效率下降。

### 10.5.5. 间隙锁危害

当我们用范围条件，而不是使用相等条件检索数据，并请求共享或排他锁时，InnoDB会**给符合条件的已有数据进行加锁**； **对于键值在条件范围内但并不存在的记录**，叫做 "**间隙（GAP）**" ， **InnoDB也会对这个 "间隙" 加锁**，这种锁机制就是所谓的**间隙锁**（Next-Key锁） 。

### 10.5.6. InnoDB 行锁争用情况

```sql
show status like 'innodb_row_lock%';
```

![1556455943670](./images/mysql-advanced-35.png)

- Innodb_row_lock_current_waits：当前正在等待锁定的数量

- Innodb_row_lock_time：从系统启动到现在锁定总时间长度

- Innodb_row_lock_time_avg：每次等待所花平均时长

- Innodb_row_lock_time_max：从系统启动到现在等待最长的一次所花的时间

- Innodb_row_lock_waits：系统启动后到现在总共等待的次数



当等待的次数很高，而且每次等待的时长也不小的时候，我们就需要分析系统中为什么会有如此多的等待，然后根据分析结果着手制定优化计划。

### 10.5.7. 总结

InnoDB存储引擎由于实现了行级锁定，虽然在锁定机制的实现方面带来了性能损耗可能比表锁会更高一些，但是在整体并发处理能力方面要远远高于MyISAM的表锁的。当系统并发量较高的时候，InnoDB的整体性能和MyISAM相比就会有比较明显的优势。

但是，InnoDB的行级锁同样也有其脆弱的一面，当我们使用不当的时候，可能会让InnoDB的整体性能表现不仅不能比MyISAM高，甚至可能会更差。

优化建议：

- 尽可能让所有数据检索都能通过索引来完成，避免无索引行锁升级为表锁。
- 合理设计索引，尽量缩小锁的范围
- 尽可能减少索引条件，及索引范围，避免间隙锁
- 尽量控制事务大小，减少锁定资源量和时间长度
- 尽可使用低级别事务隔离（但是需要业务层面满足需求）

# 11. MySql中常用工具

## 11.1. mysql

该mysql不是指mysql服务，而是指mysql的客户端工具。

语法 ：

```
mysql [options] [database]
```

### 11.1.1. 连接选项

```mysql
参数： 
	-u, --user=name			指定用户名
	-p, --password[=name]	指定密码
	-h, --host=name			指定服务器IP或域名
	-P, --port=#			指定连接端口

示例：
mysql -h 127.0.0.1 -P 3306 -u root -p
mysql -h127.0.0.1 -P3306 -uroot -p2143
```

### 11.1.2. 执行选项

```
-e, --execute=name		执行SQL语句并退出
```

此选项可以在Mysql客户端执行SQL语句，而不用连接到MySQL数据库再执行，对于一些批处理脚本，这种方式尤其方便。

```
mysql -uroot -p2143 db01 -e "select * from tb_book"; 
```

## 11.2. mysqladmin

mysqladmin 是一个执行管理操作的客户端程序。可以用它来检查服务器的配置和当前状态、创建并删除数据库等。

可以通过：mysqladmin --help指令查看帮助文档 

```mysql
mysqladmin -uroot -p2143 create 'test01';  
mysqladmin -uroot -p2143 drop 'test01';
mysqladmin -uroot -p2143 version;
```

## 11.3. mysqlbinlog

由于服务器生成的二进制日志文件以二进制格式保存，所以如果想要检查这些文本的文本格式，就会使用到mysqlbinlog 日志管理工具。

语法：

```
mysqlbinlog [options]  log-files1 log-files2 ...

选项：
	-d, --database=name : 指定数据库名称，只列出指定的数据库相关操作。
	-o, --offset=# : 忽略掉日志中的前n行命令。
	-r,--result-file=name : 将输出的文本格式日志输出到指定文件。
	-s, --short-form : 显示简单格式， 省略掉一些信息。
	--start-datatime=date1  --stop-datetime=date2 : 指定日期间隔内的所有日志。
	--start-position=pos1 --stop-position=pos2 : 指定位置间隔内的所有日志。
```

## 11.4. mysqldump

mysqldump 客户端工具用来备份数据库或在不同数据库之间进行数据迁移。备份内容包含创建表，及插入表的SQL语句。

语法：

```
mysqldump [options] db_name [tables]
mysqldump [options] --database/-B db1 [db2 db3...]
mysqldump [options] --all-databases/-A
```

### 11.4.1. 连接选项

```
参数 ： 
	-u, --user=name			指定用户名
	-p, --password[=name]	指定密码
	-h, --host=name			指定服务器IP或域名
	-P, --port=#			指定连接端口
```

### 11.4.2. 输出内容选项

```
参数:
	--add-drop-database		在每个数据库创建语句前加上 Drop database 语句
	--add-drop-table		在每个表创建语句前加上 Drop table 语句 , 默认开启 ; 不开启 (--skip-add-drop-table)

	-n, --no-create-db		不包含数据库的创建语句
	-t, --no-create-info	不包含数据表的创建语句
	-d --no-data			不包含数据
	
	 -T, --tab=name			自动生成两个文件：一个.sql文件，创建表结构的语句；
	 						一个.txt文件，数据文件，相当于select into outfile  
```

## 11.5. mysqlimport/source

mysqlimport 是客户端数据导入工具，用来导入mysqldump 加 -T 参数后导出的文本文件。

语法：

```
mysqlimport [options]  db_name  textfile1  [textfile2...]
```

如果需要导入sql文件，可以使用mysql中的source 指令 : 

```
source /root/tb_book.sql
```

## 11.6. mysqlshow

mysqlshow 客户端对象查找工具，用来很快地查找存在哪些数据库、数据库中的表、表中的列或者索引。

语法：

```
mysqlshow [options] [db_name [table_name [col_name]]]
```

参数：

```
--count		显示数据库及表的统计信息（数据库，表 均可以不指定）
-i			显示指定数据库或者指定表的状态信息
```

# 12. Mysql 日志

在 MySQL 中，有 4 种不同的日志，分别是错误日志、二进制日志（BINLOG 日志）、查询日志和慢查询日志，这些日志记录着数据库在不同方面的踪迹。

## 12.1. 错误日志

错误日志是 MySQL 中最重要的日志之一，它记录了当 mysqld 启动和停止时，以及服务器在运行过程中发生任何严重错误时的相关信息。当数据库出现任何故障导致无法正常使用时，可以首先查看此日志。

该日志是默认开启的 ， 默认存放目录为 mysql 的数据目录（var/lib/mysql）, 默认的日志文件名为  hostname.err（hostname是主机名）。

查看日志位置指令 ： 

```sql
show variables like 'log_error%'; 
```

查看日志内容 ： 

```shell
tail -f /var/lib/mysql/xaxh-server.err 
```

## 12.2. 二进制日志

### 12.2.1. 概述

二进制日志（BINLOG）记录了所有的 DDL（数据定义语言）语句和 DML（数据操纵语言）语句，但是不包括数据查询语句。此日志对于灾难时的数据恢复起着极其重要的作用，**MySQL的主从复制， 就是通过该binlog实现**的。

二进制日志，默认情况下是没有开启的，需要到MySQL的配置文件中开启，并配置MySQL日志的格式。 

配置文件位置 : /usr/my.cnf

日志存放位置 : 配置时，给定了文件名但是没有指定路径，日志默认写入Mysql的数据目录。

```properties
#配置开启binlog日志， 日志的文件前缀为 mysqlbin -----> 生成的文件名如 : mysqlbin.000001,mysqlbin.000002
log_bin=mysqlbin

#配置二进制日志的格式
binlog_format=STATEMENT
```

### 12.2.2. 日志格式

**STATEMENT**

该日志格式在日志文件中记录的都是SQL语句（statement），每一条对数据进行修改的SQL都会记录在日志文件中，通过Mysql提供的mysqlbinlog工具，可以清晰的查看到每条语句的文本。主从复制的时候，从库（slave）会将日志解析为原文本，并在从库重新执行一次。

**ROW**

该日志格式在日志文件中记录的是每一行的数据变更，而不是记录SQL语句。比如，执行SQL语句 ： update tb_book set status='1' , 如果是STATEMENT 日志格式，在日志中会记录一行SQL文件； 如果是ROW，由于是对全表进行更新，也就是每一行记录都会发生变更，ROW 格式的日志中会记录每一行的数据变更。

**MIXED**

这是目前MySQL默认的日志格式，即混合了STATEMENT 和 ROW两种格式。默认情况下采用STATEMENT，但是在一些特殊情况下采用ROW来进行记录。MIXED格式能尽量利用两种模式的优点，而避开他们的缺点。

### 12.2.3. 日志读取

由于日志以二进制方式存储，不能直接读取，需要用mysqlbinlog工具来查看，语法如下 ：

```
mysqlbinlog log-file;
```

#### 12.2.3.1. 查看STATEMENT格式日志 

执行插入语句 ：

```SQL
insert into tb_book values(null,'Lucene','2088-05-01','0');
```

 查看日志文件 ：

![1554079717375](./images/mysql-advanced-36.png)

mysqlbin.index：该文件是日志索引文件 ， 记录日志的文件名；

mysqlbing.000001：日志文件

查看日志内容：

```
mysqlbinlog mysqlbing.000001； 
```

#### 12.2.3.2. 查看ROW格式日志

配置 :

```properties
# 配置开启binlog日志， 日志的文件前缀为 mysqlbin -----> 生成的文件名如 : mysqlbin.000001, mysqlbin.000002
log_bin=mysqlbin

# 配置二进制日志的格式
binlog_format=ROW
```

插入数据：

```sql
insert into tb_book values(null,'SpringCloud实战','2088-05-05','0');
```

如果日志格式是 ROW，直接查看数据，是查看不懂的；可以在mysqlbinlog 后面加上参数 -vv  

```SQL
mysqlbinlog -vv mysqlbin.000002  
```

### 12.2.4. 日志删除

对于比较繁忙的系统，由于每天生成日志量大 ，这些日志如果长时间不清楚，将会占用大量的磁盘空间。下面我们将会讲解几种删除日志的常见方法 ：

#### 12.2.4.1. 方式一

通过 Reset Master 指令删除全部 binlog 日志，删除之后，日志编号，将从 xxxx.000001重新开始 。

查询之前 ，先查询下日志文件 ： 

![1554118609489](./images/mysql-advanced-37.png)

执行删除日志指令： 

```
Reset Master
```

执行之后， 查看日志文件 ：

![1554118675264](./images/mysql-advanced-38.png)

#### 12.2.4.2. 方式二

执行指令 ` purge master logs to 'mysqlbin.******'` ，该命令将删除  ` ******` 编号之前的所有日志。 

#### 12.2.4.3. 方式三

执行指令 ``` purge master logs before 'yyyy-mm-dd hh24:mi:ss'``` ，该命令将删除日志为 "yyyy-mm-dd hh24:mi:ss" 之前产生的所有日志 。

#### 12.2.4.4. 方式四

设置参数（在my.cnf中）--expire_logs_days=3，此参数的含义是设置日志的过期天数， 过了指定的天数后日志将会被自动删除，这样将有利于减少DBA 管理日志的工作量。 

## 12.3. 查询日志

查询日志中记录了客户端的所有操作语句，而二进制日志**不包含查询数据的SQL语句**。

默认情况下， 查询日志是未开启的。如果需要开启查询日志，可以在/usr/my.cnf以下配置 ：

```properties
#该选项用来开启查询日志 ， 可选值 ： 0 或者 1 ； 0 代表关闭， 1 代表开启 
general_log=1

#设置日志的文件名 ， 如果没有指定， 默认的文件名为 host_name.log 
general_log_file=file_name 
```

配置完毕之后，在数据库执行以下操作 ：

```
select * from tb_book;
select * from tb_book where id = 1;
update tb_book set name = 'lucene入门指南' where id = 5;
select * from tb_book where id < 8;
```

执行完毕之后， 再次来查询日志文件

## 12.4. 慢查询日志

慢查询日志记录了所有执行时间超过参数 long_query_time 设置值并且扫描记录数不小于 min_examined_row_limit 的所有的SQL语句的日志。long_query_time 默认为 10 秒，最小为 0， 精度可以到微秒。

### 12.4.1. 文件位置和格式

慢查询日志默认是关闭的 。可以通过两个参数来控制慢查询日志 ：

```properties
# 该参数用来控制慢查询日志是否开启， 可取值： 1 和 0 ， 1 代表开启， 0 代表关闭
slow_query_log=1

# 该参数用来指定慢查询日志的文件名
slow_query_log_file=slow_query.log

# 该选项用来配置查询的时间限制， 超过这个时间将认为值慢查询， 将需要进行日志记录， 默认10s
long_query_time=10
```

### 12.4.2. 日志的读取

和错误日志、查询日志一样，慢查询日志记录的格式也是纯文本，可以被直接读取。

#### 12.4.2.1. 查询long_query_time的值

![1554130333472](./images/mysql-advanced-39.png)

#### 12.4.2.2. 执行查询操作

```sql
select id, title, price, num, status from tb_item where id = 1;
```

由于该语句执行时间很短，为0s ， 所以不会记录在慢查询日志中。

```
select * from tb_item where title like '%阿尔卡特 (OT-927) 炭黑 联通3G手机 双卡双待165454%' ; 
```

该SQL语句 ， 执行时长为 26.77s ，超过10s ， 所以会记录在慢查询日志文件中。

#### 12.4.2.3. 查看慢查询日志文件

直接通过cat 指令查询该慢日志文件。 

如果慢查询日志内容很多， 直接查看文件，比较麻烦， 这个时候可以借助于mysql自带的mysqldumpslow工具，来对慢查询日志进行分类汇总。

# 13. Mysql复制

## 13.1. 复制概述

复制是指将主数据库的DDL 和 DML 操作通过二进制日志传到从库服务器中，然后在从库上对这些日志重新执行（也叫重做），从而使得从库和主库的数据保持同步。

MySQL支持一台主库同时向多台从库进行复制， 从库同时也可以作为其他从服务器的主库，实现链状复制。

## 13.2. 复制原理

MySQL 的主从复制原理如下。

![1554423698190](./images/mysql-advanced-39.jpg)

从上层来看，复制分成三步：

- Master主库在事务提交时，会把数据变更作为时间Events记录在二进制日志文件 Binlog 中。
- 主库推送二进制日志文件Binlog中的日志事件到从库的中继日志Relay Log 。

- slave重做中继日志中的事件，将改变反映它自己的数据。

## 13.3. 复制优势

MySQL 复制的有点主要包含以下三个方面：

- 主库出现问题，可以快速切换到从库提供服务。

- 可以在从库上执行查询操作，从主库中更新，实现读写分离，降低主库的访问压力。

- 可以在从库中执行备份，以避免备份期间影响主库的服务。

## 13.4. 搭建步骤

### 13.4.1. master

1） 在master 的配置文件（/usr/my.cnf）中，配置如下内容：

```properties
#mysql 服务ID,保证整个集群环境中唯一
server-id=1

#mysql binlog 日志的存储路径和文件名
log-bin=/var/lib/mysql/mysqlbin

#错误日志,默认已经开启
#log-err

#mysql的安装目录
#basedir

#mysql的临时目录
#tmpdir

#mysql的数据存放目录
#datadir

#是否只读,1 代表只读, 0 代表读写
read-only=0

#忽略的数据, 指不需要同步的数据库
binlog-ignore-db=mysql

#指定同步的数据库
#binlog-do-db=db01
```

2） 执行完毕之后，需要重启Mysql：

```sql
service mysql restart ；
```

3） 创建同步数据的账户，并且进行授权操作：

```sqlgit 
grant replication slave on *.* to 'itcast'@'192.168.192.131' identified by 'itcast';	

flush privileges;
```

4） 查看master状态：

```sql
show master status;
```

![1554477759735](./images/mysql-advanced-40.png)

字段含义：

```
File：从哪个日志文件开始推送日志文件 
Position：从哪个位置开始推送日志
Binlog_Ignore_DB：指定不需要同步的数据库
```

### 13.4.2. slave

1） 在 slave 端配置文件中，配置如下内容：

```properties
#mysql服务端ID,唯一
server-id=2

#指定binlog日志
log-bin=/var/lib/mysql/mysqlbin
```

2）  执行完毕之后，需要重启Mysql：

```
service mysql restart；
```

3） 执行如下指令 ：

```sql
change master to master_host= '192.168.192.130', master_user='itcast', master_password='itcast', master_log_file='mysqlbin.000001', master_log_pos=413;
```

指定当前从库对应的主库的IP地址，用户名，密码，从哪个日志文件开始的那个位置开始同步推送日志。

4） 开启同步操作

```mysql
start slave;
show slave status; 
```

5） 停止同步操作

```
stop slave;
```

### 13.4.3. 验证同步操作

可以对主库进行增删改查操作，然后在从库中查看是否成功同步



