<h2>目录</h2>

<details open>
  <summary><a href="#1-kafka概述">1. Kafka概述</a></summary>
  <ul>
    <a href="#11-定义">1.1. 定义</a><br>
  <details open>
    <summary><a href="#12-消息队列">1.2. 消息队列</a>  </summary>
    <ul>
      <a href="#121-传统消息队列的应用场景">1.2.1. 传统消息队列的应用场景</a><br>
    <details open>
      <summary><a href="#122-消息队列的两种模式">1.2.2. 消息队列的两种模式</a>    </summary>
      <ul>
        <a href="#1221-点对点模式">1.2.2.1. 点对点模式</a><br>
        <a href="#1222-发布订阅模式">1.2.2.2. 发布/订阅模式</a><br>
      </ul>
    </details>
    </ul>
  </details>
    <a href="#13-kafka-基础架构">1.3. Kafka 基础架构</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#2-kafka-快速入门">2. Kafka 快速入门</a></summary>
  <ul>
    <a href="#21-安装部署">2.1. 安装部署</a><br>
  <details open>
    <summary><a href="#22-kafka-命令行操作">2.2. Kafka 命令行操作</a>  </summary>
    <ul>
      <a href="#221-查看当前服务器中的所有-topic">2.2.1. 查看当前服务器中的所有 topic</a><br>
      <a href="#222-创建-topic">2.2.2. 创建 topic</a><br>
      <a href="#223-删除-topic">2.2.3. 删除 topic</a><br>
      <a href="#224-查看某个-topic-的详情">2.2.4. 查看某个 Topic 的详情</a><br>
      <a href="#225-修改分区数">2.2.5. 修改分区数</a><br>
    <details open>
      <summary><a href="#226-控制台生产者消费者测试">2.2.6. 控制台生产者消费者测试</a>    </summary>
      <ul>
        <a href="#2261-发送消息（在kafka1中）">2.2.6.1. 发送消息（在kafka1中）</a><br>
        <a href="#2262-消费消息（在kafka2中）">2.2.6.2. 消费消息（在kafka2中）</a><br>
      </ul>
    </details>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#3-kafka-架构深入">3. Kafka 架构深入</a></summary>
  <ul>
  <details open>
    <summary><a href="#31-kafka-工作流程及文件存储机制">3.1. Kafka 工作流程及文件存储机制</a>  </summary>
    <ul>
      <a href="#311-工作流程">3.1.1. 工作流程</a><br>
      <a href="#312-文件存储">3.1.2. 文件存储</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#32-kafka-生产者">3.2. Kafka 生产者</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#321-分区策略">3.2.1. 分区策略</a>    </summary>
      <ul>
        <a href="#3211-分区的原因">3.2.1.1. 分区的原因</a><br>
        <a href="#3212-分区的原则">3.2.1.2. 分区的原则</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#322-数据可靠性保证">3.2.2. 数据可靠性保证</a>    </summary>
      <ul>
        <a href="#3221-副本数据同步策略">3.2.2.1. 副本数据同步策略</a><br>
        <a href="#3222-isr">3.2.2.2. ISR</a><br>
        <a href="#3223-ack机制">3.2.2.3. ack机制</a><br>
        <a href="#3224-故障处理细节">3.2.2.4. 故障处理细节</a><br>
      </ul>
    </details>
      <a href="#323-exactly-once-语义">3.2.3. Exactly Once 语义</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#33-消费者">3.3. 消费者</a>  </summary>
    <ul>
      <a href="#331-消费方式">3.3.1. 消费方式</a><br>
    <details open>
      <summary><a href="#332-分区分配策略">3.3.2. 分区分配策略</a>    </summary>
      <ul>
        <a href="#3321-round-robin-assignor">3.3.2.1. Round Robin Assignor</a><br>
        <a href="#3322-range-assignor（默认）">3.3.2.2. Range Assignor（默认）</a><br>
        <a href="#3323-sticky-assignor分配策略">3.3.2.3. Sticky Assignor分配策略</a><br>
      </ul>
    </details>
      <a href="#333-offset的维护">3.3.3. offset的维护</a><br>
    <details open>
      <summary><a href="#334-消费者组案例">3.3.4. 消费者组案例</a>    </summary>
      <ul>
        <a href="#3341-需求">3.3.4.1. 需求</a><br>
        <a href="#3342-操作步骤">3.3.4.2. 操作步骤</a><br>
      </ul>
    </details>
    </ul>
  </details>
  <details open>
    <summary><a href="#34-kafka-高效读写数据">3.4. Kafka 高效读写数据</a>  </summary>
    <ul>
      <a href="#341-顺序写磁盘">3.4.1. 顺序写磁盘</a><br>
      <a href="#342-零复制技术">3.4.2. 零复制技术</a><br>
    </ul>
  </details>
    <a href="#35-zookeeper-在-kafka-中的作用">3.5. Zookeeper 在 Kafka 中的作用</a><br>
  <details open>
    <summary><a href="#36-kafka-事务">3.6. Kafka 事务</a>  </summary>
    <ul>
      <a href="#361-producer-事务">3.6.1. Producer 事务</a><br>
      <a href="#362-consumer-事务">3.6.2. Consumer 事务</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#4-kafka-api">4. Kafka API</a></summary>
  <ul>
  <details open>
    <summary><a href="#41-producer-api">4.1. Producer API</a>  </summary>
    <ul>
      <a href="#411-消息发送流程">4.1.1. 消息发送流程</a><br>
    <details open>
      <summary><a href="#412-异步发送api">4.1.2. 异步发送API</a>    </summary>
      <ul>
        <a href="#4121-导入依赖">4.1.2.1. 导入依赖</a><br>
        <a href="#4122-编写代码">4.1.2.2. 编写代码</a><br>
      </ul>
    </details>
      <a href="#413-同步发送api">4.1.3. 同步发送API</a><br>
    </ul>
  </details>
  </ul>
</details>


<h1>Kafka</h1>

# 1. Kafka概述

https://my.oschina.net/jallenkwong/blog/4449224

笔记资料：链接: https://pan.baidu.com/s/17XOeCBYdyfNqWJPxb_dMsA 提取码: 5sp9
from：尚硅谷官网



## 1.1. 定义

Kafka 是一个**分布式**的基于**发布/订阅模式**的**消息队列**（Message Queue），主要应用于大数据实时处理领域。

## 1.2. 消息队列

### 1.2.1. 传统消息队列的应用场景

![img](./images/Kafka-1.png)

**使用消息队列的好处**

1. **解耦**（类似Spring的IOC）：允许你独立的扩展或修改两边的处理过程，只要确保它们遵守同样的接口约束。
2. **可恢复性**：系统的一部分组件失效时，不会影响到整个系统。消息队列降低了进程间的耦合度，所以即使一个处理消息的进程挂掉，加入队列中的消息仍然可以在系统恢复后被处理。
3. **缓冲**：有助于控制和优化数据流经过系统的速度， **解决生产消息和消费消息的处理速度不一致的情况**。
4. **灵活性 & 峰值处理能力**（削峰）：在访问量剧增的情况下，应用仍然需要继续发挥作用，但是这样的突发流量并不常见。如果为以能处理这类峰值访问为标准来投入资源随时待命无疑是巨大的浪费。使用消息队列能够使关键组件顶住突发的访问压力，而不会因为突发的超负荷的请求而完全崩溃。
5. **异步通信**：很多时候，用户不想也不需要立即处理消息。消息队列提供了异步处理机制，允许用户把一个消息放入队列，但并不立即处理它。想向队列中放入多少消息就放多少，然后在需要的时候再去处理它们。

### 1.2.2. 消息队列的两种模式

#### 1.2.2.1. 点对点模式

**一对一，消费者主动拉取数据，消息收到后消息清除**

消息生产者生产消息发送到Queue中，然后消息消费者从Queue中取出并且消费消息。消息被消费以后， queue 中不再有存储，所以消息消费者不可能消费到已经被消费的消息。Queue 支持存在多个消费者，但是对一个消息而言，只会有一个消费者可以消费。

![img](./images/Kafka-2.png)

#### 1.2.2.2. 发布/订阅模式

**一对多，消费者消费数据之后不会清除消息**

消息生产者（发布）将消息发布到 topic 中，同时有多个消息消费者（订阅）消费该消息。和点对点方式不同，发布到 topic 的消息会被所有订阅者消费。

![img](./images/Kafka-3.png)

## 1.3. Kafka 基础架构

![img](./images/Kafka-4.png)

1. **Producer**：消息生产者，就是向 Kafka broker 发消息的客户端；
2. **Consumer**：消息消费者，向 Kafka broker 取消息的客户端；
3. **Consumer Group （CG）**：消费者组，由多个 consumer 组成。 消费者组内每个消费者负责消费不同分区的数据，一个分区只能由一个组内消费者消费；消费者组之间互不影响。 所有的消费者都属于某个消费者组，即消费者组是逻辑上的一个订阅者。
4. **Broker**：经纪人，一台 Kafka 服务器就是一个 broker。一个集群由多个 broker 组成。一个 broker可以容纳多个 topic。
5. **Topic**：话题，可以理解为一个队列， 生产者和消费者面向的都是一个topic；
6. **Partition**：为了实现扩展性，一个非常大的 topic 可以分布到多个 broker（即服务器）上，一个 topic 可以分为多个 partition，每个 partition 是一个有序的队列；
7. **Replica**：副本（Replication），为保证集群中的某个节点发生故障时， 该节点上的  partition 数据不丢失，且 Kafka仍然能够继续工作， Kafka 提供了副本机制，一个 topic 的每个分区都有若干个副本，一个  leader 和若干个 follower。
8. **Leader**：每个分区多个副本的“主”，生产者发送数据的对象，以及消费者消费数据的对象都是 leader。
9. **Follower**：每个分区多个副本中的“从”，实时从 leader 中同步数据，保持和 leader 数据的同步。 leader 发生故障时，某个 Follower 会成为新的 leader。

# 2. Kafka 快速入门

## 2.1. 安装部署

在[目录](./)下运行`docker-compose up`命令，启动Kafka集群，运行`docker-compose down`命令，关闭并清除Kafka集群。

## 2.2. Kafka 命令行操作

### 2.2.1. 查看当前服务器中的所有 topic

```bash
/opt/kafka_2.13-2.8.1/bin/kafka-topics.sh  --list --zookeeper zoo1:2181
```

### 2.2.2. 创建 topic

```bash
/opt/kafka_2.13-2.8.1/bin/kafka-topics.sh --create --zookeeper zoo1:2181 --replication-factor 3 --partitions 1 --topic my-replicated-topic
```

选项说明：

- --topic 定义 topic 名
- --replication-factor 定义副本数
- --partitions 定义分区数

> 为了实现扩展性，一个非常大的 topic 可以分布到多个 broker（即服务器）上，一个 topic 可以分为多个 partition，每个 partition 是一个有序的队列；

### 2.2.3. 删除 topic

```bash
/opt/kafka_2.13-2.8.1/bin/kafka-topics.sh --zookeeper zoo1:2181 --delete --topic my-replicated-topic
```

需要`/opt/kafka_2.13-2.8.1/config/server.properties`中设置 `delete.topic.enable=true` 否则只是标记删除。

### 2.2.4. 查看某个 Topic 的详情

```bash
/opt/kafka_2.13-2.8.1/bin/kafka-topics.sh --zookeeper zoo1:2181 --describe --topic my-replicated-topic
```

### 2.2.5. 修改分区数

```bash
/opt/kafka_2.13-2.8.1/bin/kafka-topics.sh --zookeeper zoo1:2181 --alter --topic my-replicated-topic --partitions 6
```

### 2.2.6. 控制台生产者消费者测试

#### 2.2.6.1. 发送消息（在kafka1中）

```bash
# kafka1
/opt/kafka_2.13-2.8.1/bin/kafka-console-producer.sh --broker-list kafka1:9092 --topic my-replicated-topic

>hello, kafka.
>what a nice day!
>to be or not to be. that' s a question.
```

#### 2.2.6.2. 消费消息（在kafka2中）

```bash
# kafka2
/opt/kafka_2.13-2.8.1/bin/kafka-console-consumer.sh --bootstrap-server kafka1:9092 --topic my-replicated-topic --from-beginning

hello, kafka.
what a nice day!
to be or not to be. that' s a question.
```

**--from-beginning**： 会把主题中以往所有的数据都读取出来。

# 3. Kafka 架构深入

## 3.1. Kafka 工作流程及文件存储机制

### 3.1.1. 工作流程

![img](./images/Kafka-5.png)

Kafka 中消息是以 topic 进行分类的， producer生产消息，consumer消费消息，都是面向 topic的。

topic 是逻辑上的概念，而 partition 是物理上的概念，每个 partition 对应于一个 log 文件，该 log 文件中存储的就是 producer 生产的数据。（topic = N partition，partition = log）

Producer 生产的数据会被不断追加到该log 文件末端，且每条数据都有自己的 offset。 consumer组中的每个consumer，都会实时记录自己消费到了哪个offset，以便出错恢复时，从上次的位置继续消费。（producer -> log with offset ->  consumer(s)）

### 3.1.2. 文件存储

![img](./images/Kafka-6.png)

由于生产者生产的消息会不断追加到 log 文件末尾， 为防止 log 文件过大导致数据定位效率低下， Kafka 采取了**分片**和**索引**机制，将每个 partition 分为多个 segment。

每个 segment对应两个文件——“.index”文件和“.log”文件。

这些文件位于一个文件夹下(/kafka/kafka-logs-kafka1/chat-0/)， 该文件夹的命名规则为：**topic 名称+分区序号**。

例如，first 这个 topic 有三个分区，则其对应的文件夹为  first-0,first-1,first-2。

```
00000000000000000000.index
00000000000000000000.log
00000000000000170410.index
00000000000000170410.log
00000000000000239430.index
00000000000000239430.log
```

index 和 log 文件**以当前 segment 的第一条消息的offset命名**，如上面的170410。下图为 index 文件和 log文件的结构示意图。

![img](./images/Kafka-7.png)

**“.index”文件存储大量的索引信息，“.log”文件存储大量的数据**，索引文件中的元数据指向对应数据文件中 message 的物理偏移地址。

## 3.2. Kafka 生产者

### 3.2.1. 分区策略

#### 3.2.1.1. 分区的原因

1. **方便在集群中扩展（负载均衡）**，每个 Partition 可以通过调整以适应它所在的机器，而一个 topic又可以有多个 Partition 组成，因此整个集群就可以适应适合的数据了；
2. **可以提高并发**，因为可以以 Partition 为单位读写了，提高吞吐量。

#### 3.2.1.2. 分区的原则

我们需要将 producer 发送的数据封装成一个 `ProducerRecord` 对象。

![img](./images/Kafka-8.png)

1. 指明 partition 的情况下，直接将指明的值直接作为 partiton 值；
2. 没有指明 partition 值但有 key 的情况下，将 key 的 hash 值与 topic 的 partition 数进行取余得到 partition 值；
3. 既没有 partition 值又没有 key 值的情况下，第一次调用时随机生成一个整数（后面每次调用在这个整数上自增），将这个值与 topic 可用的 partition 总数取余得到 partition值，也就是常说的 round-robin 算法。

### 3.2.2. 数据可靠性保证

为保证 producer 发送的数据，能可靠的发送到指定的 topic， topic 的每个partition 收到producer 发送的数据后，都需要向 producer 发送 ack（acknowledgement 确认收到），如果producer 收到 ack， 就会进行下一轮的发送，否则重新发送数据。

![img](./images/Kafka-9.png)



**何时发送ack？**

确保有follower与leader（**消息是发送给leader的，然后同步**）同步完成，leader再发送ack，这样才能保证leader挂掉之后，能在follower中选举出新的leader。



**多少个follower同步完成之后发送ack？**

1. 半数以上的follower同步完成，即可发送ack继续发送重新发送
2. 全部的follower同步完成，才可以发送ack

#### 3.2.2.1. 副本数据同步策略

| 序号 | 方案                          | 优点                                                         | 缺点                                                         |
| ---- | ----------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 1    | 半数以上完成同步， 就发送 ack | 延迟低                                                       | 选举新的 leader 时，容忍 n 台节点的故障，需要 2n+1 个副本。（如果集群有2n+1台机器，选举leader的时候至少需要半数以上即n+1台机器投票，那么能容忍的故障，最多就是n台机器发生故障）容错率：1/2 |
| 2    | 全部完成同步，才发送ack       | 选举新的 leader 时， 容忍 n 台节点的故障，需要 n+1 个副本（如果集群有n+1台机器，选举leader的时候只要有一个副本就可以了）容错率：1 | 延迟高                                                       |

Kafka 选择了第二种方案，原因如下：

1. 同样为了容忍 n 台节点的故障，第一种方案需要 2n+1 个副本，而第二种方案只需要 n+1 个副本，而 Kafka 的每个分区都有大量的数据， 第一种方案会造成大量数据的冗余。
2. 虽然第二种方案的网络延迟会比较高，但网络延迟对 Kafka 的影响较小。

#### 3.2.2.2. ISR

采用第二种方案之后，设想以下情景： leader 收到数据，所有 follower 都开始同步数据，但有一个 follower，因为某种故障，迟迟不能与 leader 进行同步，那 leader 就要一直等下去，直到它完成同步，才能发送 ack。这个问题怎么解决呢？

Leader 维护了一个动态的 **In-Sync Replica Set** (ISR，In-Sync Replicas，同步副本集合)，意为和 leader 保持同步的 follower 集合。当 ISR 中的 follower 完成数据的同步之后，就会给 leader 发送 ack。如果 follower长时间未向leader同步数据，则该follower将被踢出ISR，该时间阈值由`replica.lag.time.max.ms`参数设定。 Leader 发生故障之后，就会从 ISR 中选举新的 leader。

#### 3.2.2.3. ack机制

对于某些不太重要的数据，对数据的可靠性要求不是很高，能够容忍数据的少量丢失，所以没必要等 ISR 中的 follower 全部接收成功。

所以 Kafka 为用户提供了三种可靠性级别，用户根据对可靠性和延迟的要求进行权衡，选择以下的配置。

**acks 参数配置**：

- 0： producer 不等待 broker 的 ack，这一操作提供了一个最低的延迟， broker 一接收到还没有写入磁盘就已经返回，当 broker 故障时有可能**丢失数据**；
- 1： producer 等待 broker 的 ack， partition 的 leader 落盘成功后返回 ack，如果在 follower同步成功之前 leader 故障，那么将会**丢失数据**；

![img](./images/Kafka-10.png)

- -1（all） ： producer 等待 broker 的 ack， partition 的 leader 和 ISR 的follower 全部落盘成功后才返回 ack。但是如果在 follower 同步完成后， broker 发送 ack 之前， leader 发生故障，那么会造成**数据重复**。

![img](./images/Kafka-11.png)

**助记：返ACK前，0无落盘，1一落盘，-1全落盘，（落盘：消息存到本地）**

#### 3.2.2.4. 故障处理细节


![img](./images/Kafka-12.png)

- LEO：（Log End Offset）每个副本的最后一个offset
- HW：（High Watermark）高水位，指的是**消费者能见到的最大的 offset**，**ISR 队列中最小的 LEO**



- **follower 故障**：follower 发生故障后会被临时踢出 ISR，待该 follower 恢复后， follower 会读取本地磁盘记录的上次的 HW，并将 log 文件高于 HW 的部分截取掉，从 HW 开始向 leader 进行同步。等该 follower 的 LEO 大于等于该 Partition 的 HW，即 follower 追上 leader 之后，就可以重新加入 ISR 了。
- **leader 故障**：leader 发生故障之后，会从 ISR 中选出一个新的 leader，之后，为保证多个副本之间的数据一致性， 其余的 follower 会先将各自的 log 文件**高于 HW 的部分截掉**，然后从新的 leader同步数据。

**注意**： 这只能保证副本之间的数据一致性，并不能保证数据不丢失或者不重复。

### 3.2.3. Exactly Once 语义

将服务器的 ACK 级别设置为-1（all），可以保证 Producer 到 Server 之间不会丢失数据，即 **At Least Once** 语义。

相对的，将服务器 ACK 级别设置为 0，可以保证生产者每条消息只会被发送一次，即 **At Most Once** 语义。

At Least Once可以保证数据不丢失，但是不能保证数据不重复；

At Most Once可以保证数据不重复，但是不能保证数据不丢失。 

但是，对于一些非常重要的信息，下游数据消费者要求数据**既不重复也不丢失**，即 **Exactly Once** 语义。



在0.11版本以前的 Kafka，对此是无能为力的，只能保证数据不丢失，再在下游消费者对数据做全局去重。对于多个下游应用的情况，每个都需要单独做全局去重，这就对性能造成了很大影响。

0.11版本的 Kafka，引入了一项重大特性：**幂等性**。**所谓的幂等性就是指 Producer 不论向 Server 发送多少次重复数据， Server 端都只会持久化一条**。幂等性结合 At Least Once 语义，就构成了 Kafka 的 Exactly Once 语义。即：

```
At Least Once + 幂等性 = Exactly Once
```

要启用幂等性，只需要将 Producer 的参数中 `enable.idempotence` 设置为 true 即可。 

Kafka的幂等性实现其实就是将原来下游需要做的去重放在了数据上游。开启幂等性的 Producer 在初始化的时候会被分配一个 PID（Producer ID），发往同一 Partition 的消息会附带 Sequence Number。而Broker 端会对`<PID, Partition, SeqNumber>`做缓存，当具有相同主键的消息提交时， Broker 只会持久化一条。

但是生产的消息ID PID（Producer ID）重启就会变化，同时不同的 Partition 也具有不同主键，所以**幂等性无法保证跨分区跨会话的 Exactly Once**。

## 3.3. 消费者

### 3.3.1. 消费方式

**consumer 采用 pull（拉） 模式从 broker 中读取数据**。

**push（推）模式很难适应消费速率不同的消费者，因为消息发送速率是由 broker 决定的**。它的目标是尽可能以最快速度传递消息，但是这样很容易造成 consumer 来不及处理消息，典型的表现就是拒绝服务以及网络拥塞。而 pull 模式则可以根据 consumer 的消费能力以适当的速率消费消息。

**pull 模式不足之处**是，如果 kafka 没有数据，消费者可能会陷入循环中， 一直返回空数据。 针对这一点， Kafka 的消费者在消费数据时会传入一个时长参数 timeout，如果当前没有数据可供消费， consumer 会等待一段时间之后再返回，这段时长即为 timeout。

### 3.3.2. 分区分配策略

一个 consumer group 中有多个 consumer，一个 topic 有多个 partition，所以必然会涉及到 partition 的分配问题，即确定那个 partition 由哪个 consumer 来消费。

Kafka 有两种分配策略：

- Round Robin Assignor轮询
- range

#### 3.3.2.1. Round Robin Assignor

**Round Robin重分配策略**，其主要采用的是一种轮询的方式分配所有的分区，该策略主要实现的步骤如下。

这里我们首先假设有三个topic：t0、t1和t2，这三个topic拥有的分区数分别为1、2和3，那么总共有六个分区，这六个分区分别为：t0-0、t1-0、t1-1、t2-0、t2-1和t2-2。

这里假设我们有三个consumer：C0、C1和C2，它们订阅情况为：C0订阅t0，C1订阅t0和t1，C2订阅t0、t1和t2。

那么这些分区的分配步骤如下：

- 首先将所有的partition和consumer按照字典序进行排序，所谓的字典序，就是按照其名称的字符串顺序，那么上面的六个分区和三个consumer排序之后分别为：

![img](./images/Kafka-13.png)

- 然后依次以按顺序轮询的方式将这六个分区分配给三个consumer，如果当前consumer没有订阅当前分区所在的topic，则轮询的判断下一个consumer：
- 尝试将t0-0分配给C0，由于C0订阅了t0，因而可以分配成功；
- 尝试将t1-0分配给C1，由于C1订阅了t1，因而可以分配成功；
- 尝试将t1-1分配给C2，由于C2订阅了t1，因而可以分配成功；
- 尝试将t2-0分配给C0，由于C0没有订阅t2，因而会轮询下一个consumer；
- 尝试将t2-0分配给C1，由于C1没有订阅t2，因而会轮询下一个consumer；
- 尝试将t2-0分配给C2，由于C2订阅了t2，因而可以分配成功；
- 同理由于t2-1和t2-2所在的topic都没有被C0和C1所订阅，因而都不会分配成功，最终都会分配给C2。
- 按照上述的步骤将所有的分区都分配完毕之后，最终分区的订阅情况如下：

![img](./images/Kafka-14.png)

从上面的步骤分析可以看出，轮询的策略就是简单的将所有的partition和consumer按照字典序进行排序之后，然后依次将partition分配给各个consumer，**如果当前的consumer没有订阅当前的partition，那么就会轮询下一个consumer，直至最终将所有的分区都分配完毕**。但是从上面的分配结果可以看出，**轮询的方式会导致每个consumer所承载的分区数量不一致，从而导致各个consumer压力不均一**。

#### 3.3.2.2. Range Assignor（默认）

**Range重分配策略**，就是首先会计算各个consumer将会承载的分区数量，然后将指定数量的分区分配给该consumer。

这里我们假设有两个consumer：C0和C1，两个topic：t0和t1，这两个topic分别都有三个分区，那么总共的分区有六个：t0-0、t0-1、t0-2、t1-0、t1-1和t1-2。那么Range分配策略将会按照如下步骤进行分区的分配：

- 需要注意的是，Range策略是按照topic依次进行分配的，比如我们以t0进行讲解，其首先会获取t0的所有分区：t0-0、t0-1和t0-2，以及所有订阅了该topic的consumer：C0和C1，并且会将这些分区和consumer按照字典序进行排序；
- 然后按照平均分配的方式计算每个consumer会得到多少个分区，如果没有除尽，则会将多出来的分区依次计算到前面几个consumer。比如这里是三个分区和两个consumer，那么**每个consumer至少会得到1个分区，而3除以2后还余1，那么就会将多余的部分依次算到前面几个consumer，也就是这里的1会分配给第一个consumer**，总结来说，那么C0将会从第0个分区开始，分配2个分区，而C1将会从第2个分区开始，分配1个分区；
- 同理，按照上面的步骤依次进行后面的topic的分配。
- 最终上面六个分区的分配情况如下：

![img](./images/Kafka-15.png)

可以看到，如果按照`Range`分区方式进行分配，其本质上是**依次遍历每个topic，然后将这些topic的分区按照其所订阅的consumer数量进行平均的范围分配**。这种方式从计算原理上就**会导致排序在前面的consumer分配到更多的分区，从而导致各个consumer的压力不均衡**。

#### 3.3.2.3. Sticky Assignor分配策略

Kafka从0.11.x版本开始引入这种分配策略，它主要有两个目的：

1、分区的分配要尽可能的均匀；

2、分区的分配尽可能的与上次分配的保持相同。

当两者发生冲突时，第一个目标优先于第二个目标

假设消费组内有3个消费者：C0、C1和C2，它们都订阅了4个主题：t0、t1、t2、t3，并且每个主题有2个分区，也就是说整个消费组订阅了t0p0、t0p1、t1p0、t1p1、t2p0、t2p1、t3p0、t3p1这8个分区。最终的分配结果如下：

```
消费者C0：t0p0、t1p1、t3p0
消费者C1：t0p1、t2p0、t3p1
消费者C2：t1p0、t2p1
```

这样初看上去似乎与采用Round Robin Assignor策略所分配的结果相同，但事实是否真的如此呢？再假设此时消费者C1脱离了消费组，那么消费组就会执行再平衡操作，进而消费分区会重新分配。　　

如果采用Round Robin Assignor策略，那么此时的分配结果如下：

```
消费者C0：t0p0、t1p0、t2p0、t3p0
消费者C2：t0p1、t1p1、t2p1、t3p1
```

如分配结果所示，Round Robin Assignor策略会按照消费者C0和C2进行重新轮询分配。而如果此时使用的是Sticky Assignor策略，那么分配结果为：

```
消费者C0：t0p0、t1p1、t3p0、t2p0
消费者C2：t1p0、t2p1、t0p1、t3p1
```

**可以看到分配结果中保留了上一次分配中对于消费者C0和C2的所有分配结果，并将原来消费者C1的“负担”分配给了剩余的两个消费者C0和C2，最终C0和C2的分配还保持了均衡**。

如果发生分区重分配，那么对于同一个分区而言有可能之前的消费者和新指派的消费者不是同一个，对于之前消费者进行到一半的处理还要在新指派的消费者中再次复现一遍，这显然很浪费系统资源。StickyAssignor策略如同其名称中的“sticky”一样，让分配策略具备一定的“粘性”，尽可能地让前后两次分配相同，进而减少系统资源的损耗以及其它异常情况的发生。

### 3.3.3. offset的维护

由于 consumer 在消费过程中可能会出现断电宕机等故障， consumer 恢复后，需要从故障前的位置的继续消费，所以 **consumer 需要实时记录自己消费到了哪个 offset**，以便故障恢复后继续消费。

![img](./images/Kafka-16.png)

**Kafka 0.9 版本之前， consumer 默认将 offset 保存在 Zookeeper 中，从 0.9 版本开始，consumer 默认将 offset 保存在 Kafka 一个内置的 topic 中，该 topic 为__consumer_offsets**。

1. 修改配置文件 consumer.properties，`exclude.internal.topics=false`。
2. 读取 offset
   - 0.11.0.0 之前版本 - `bin/kafka-console-consumer.sh --topic __consumer_offsets --zookeeper hadoop102:2181 --formatter "kafka.coordinator.GroupMetadataManager\$OffsetsMessageFormatter" --consumer.config config/consumer.properties --from-beginning`
   - 0.11.0.0 及之后版本 - `bin/kafka-console-consumer.sh --topic __consumer_offsets --zookeeper hadoop102:2181 --formatter "kafka.coordinator.group.GroupMetadataManager\$OffsetsMessageFormatter" --consumer.config config/consumer.properties --from-beginning`

### 3.3.4. 消费者组案例

#### 3.3.4.1. 需求

测试同一个消费者组中的消费者， **同一时刻只能有一个**消费者消费。

#### 3.3.4.2. 操作步骤

1.修改`%KAFKA_HOME\config\consumer.properties%`文件中的`group.id`属性。

```properties
group.id=shan-kou-zu
```

2.打开两个cmd，分别启动两个消费者。（以`%KAFKA_HOME\config\consumer.properties%`作配置参数）

```bat
bin\windows\kafka-console-consumer.bat --zookeeper 127.0.0.1:2181 --topic test --consumer.config config\consumer.properties
```

3.再打开一个cmd，启动一个生产者。

```bat
bin\windows\kafka-console-producer.bat --broker-list 127.0.0.1:9092 --topic test
```

4.在生产者窗口输入消息，观察两个消费者窗口。**会发现两个消费者窗口中，只有一个才会弹出消息**。

## 3.4. Kafka 高效读写数据

### 3.4.1. 顺序写磁盘

Kafka 的 producer 生产数据，要写入到 log 文件中，写的过程是一直追加到文件末端，为顺序写。 官网有数据表明，同样的磁盘，顺序写能到 600M/s，而随机写只有 100K/s。这与磁盘的机械机构有关，顺序写之所以快，是因为其**省去了大量磁头寻址的时间**。

### 3.4.2. 零复制技术

![img](./images/Kafka-17.png)

- NIC network interface controller 网络接口控制器

## 3.5. Zookeeper 在 Kafka 中的作用

Kafka 集群中有一个 broker 会被选举为 Controller，负责管理集群 broker 的上下线，所有 topic 的分区副本分配和 leader 选举等工作。[Reference](https://www.oschina.net/action/GoToLink?url=http%3A%2F%2Fkafka.apache.org%2F0110%2Fdocumentation%2F%23design_replicamanagment)

Controller 的管理工作都是依赖于 Zookeeper 的。

以下为 partition 的 leader 选举过程：

![Leader选举流程](./images/Kafka-18.png)

## 3.6. Kafka 事务

Kafka 从 0.11 版本开始引入了事务支持。事务可以保证 Kafka 在 Exactly Once 语义的基础上，生产和消费可以跨分区和会话，要么全部成功，要么全部失败。

### 3.6.1. Producer 事务

为了实现跨分区跨会话的事务，需要引入一个全局唯一的 Transaction ID，并将 Producer 获得的PID 和Transaction ID 绑定。这样当Producer 重启后就可以通过正在进行的 TransactionID 获得原来的 PID。

为了管理 Transaction， Kafka 引入了一个新的组件 Transaction Coordinator。 Producer 就是通过和 Transaction Coordinator 交互获得 Transaction ID 对应的任务状态。 Transaction Coordinator 还负责将事务所有写入 Kafka 的一个内部 Topic，这样即使整个服务重启，由于事务状态得到保存，进行中的事务状态可以得到恢复，从而继续进行。

### 3.6.2. Consumer 事务

上述事务机制主要是从 Producer 方面考虑，对于 Consumer 而言，事务的保证就会相对较弱，尤其时无法保证 Commit 的信息被精确消费。这是由于 Consumer 可以通过 offset 访问任意信息，而且不同的 Segment File 生命周期不同，同一事务的消息可能会出现重启后被删除的情况。

# 4. Kafka API

## 4.1. Producer API

### 4.1.1. 消息发送流程

Kafka 的 Producer 发送消息采用的是异步发送的方式。在消息发送的过程中，涉及到了两个线程——main 线程和 Sender 线程，以及一个线程共享变量——RecordAccumulator。 main 线程将消息发送给 RecordAccumulator， Sender 线程不断从 RecordAccumulator 中拉取消息发送到 Kafka broker。

![img](./images/Kafka-19.png)

相关参数：

- **batch.size**： 只有数据积累到 batch.size 之后， sender 才会发送数据。
- **linger.ms**： 如果数据迟迟未达到 batch.size， sender 等待 linger.time 之后就会发送数据。

### 4.1.2. 异步发送API

#### 4.1.2.1. 导入依赖

[pom.xml](https://my.oschina.net/jallenkwong/blog/pom.xml)

```xml
<dependency>
	<groupId>org.apache.kafka</groupId>
	<artifactId>kafka-clients</artifactId>
	<version>0.11.0.0</version>
</dependency>
```

#### 4.1.2.2. 编写代码

需要用到的类：

- KafkaProducer：需要创建一个生产者对象，用来发送数据
- ProducerConfig：获取所需的一系列配置参数
- ProducerRecord：每条数据都要封装成一个 ProducerRecord 对象



**不带回调函数的API**

[CustomProducer.java](https://gitee.com/jallenkwong/LearnKafka/blob/master/src/main/java/com/lun/kafka/producer/CustomProducer.java)

```java
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class CustomProducer {

	public static void main(String[] args) {
		Properties props = new Properties();
		// kafka 集群， broker-list
		
		props.put("bootstrap.servers", "127.0.0.1:9092");
		
		//可用ProducerConfig.ACKS_CONFIG 代替 "acks"
		//props.put(ProducerConfig.ACKS_CONFIG, "all");
		props.put("acks", "all");
		// 重试次数
		props.put("retries", 1);
		// 批次大小
		props.put("batch.size", 16384);
		// 等待时间
		props.put("linger.ms", 1);
		// RecordAccumulator 缓冲区大小
		props.put("buffer.memory", 33554432);
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		Producer<String, String> producer = new KafkaProducer<>(props);
		for (int i = 0; i < 100; i++) {
			producer.send(new ProducerRecord<String, String>("test", "test-" + Integer.toString(i),
					"test-" + Integer.toString(i)));
		}
		producer.close();
	}

}
```



**带回调函数的生产者**

回调函数会在 producer 收到 ack 时调用，为异步调用， 该方法有两个参数，分别是 RecordMetadata 和 Exception，如果 Exception 为 null，说明消息发送成功，如果Exception 不为 null，说明消息发送失败。

**注意**：消息发送失败会自动重试，不需要我们在回调函数中手动重试。

[CallBackProducer.java](https://gitee.com/jallenkwong/LearnKafka/blob/master/src/main/java/com/lun/kafka/producer/CallBackProducer.java)

```java
import java.util.Properties;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

public class CallBackProducer {
	public static void main(String[] args) {
		Properties props = new Properties();
		props.put("bootstrap.servers", "127.0.0.1:9092");//kafka 集群， broker-list
		props.put("acks", "all");
		props.put("retries", 1);//重试次数
		props.put("batch.size", 16384);//批次大小
		props.put("linger.ms", 1);//等待时间
		props.put("buffer.memory", 33554432);//RecordAccumulator 缓冲区大小
		props.put("key.serializer",
		"org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer",
		"org.apache.kafka.common.serialization.StringSerializer");
		
		Producer<String, String> producer = new KafkaProducer<>(props);
		for (int i = 0; i < 100; i++) {
			producer.send(new ProducerRecord<String, String>("test",
				"test" + Integer.toString(i)), new Callback() {
			
				//回调函数， 该方法会在 Producer 收到 ack 时调用，为异步调用
				@Override
				public void onCompletion(RecordMetadata metadata, Exception exception) {
					if (exception == null) {
						System.out.println(metadata.partition() + " - " + metadata.offset());
					} else {
						exception.printStackTrace();
					}
				}
			});
		}
		
		producer.close();
	}
}
```

### 4.1.3. 同步发送API



