package com.mall.token;

import org.junit.jupiter.api.Test;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaVerifier;

/**
 * <p>
 * 使用公钥解密令牌数据
 * </p>
 *
 * @author yanglin
 * @date 2021-01-14 00:00:49
 */
public class ParseJwtTest {

    /***
     * 校验令牌
     */
    @Test
    public void testParseToken() {
        //令牌
        String token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlcyI6IlJPTEVfVklQLFJPTEVfVVNFUiIsIm5hbWUiOiJpdGhlaW1hIiwiaWQiOiIxIn0.BtmmKZVK0Ju0-tJ8cJOWHLJMmQDLut9K-30FntOnsoFIuBxEWfZy7BuN40qrTyD614fqMtAsl7FICpmWdLGiIiYq4vsY7rvh5gYEfZIhrthzhBe_1uZuAUPknOiPvEd59u6eVkT-61bFjTzedi-G3kvb_P0ncOU-I15PqvGHpwJoYInb4-fYdEck7MX9LXx-5HGMRmJy6FuSsxxUxjPQ1FN5R0V11WSIZEFFnGJg1TchHYOqrjT9S-MtawMASw2Zo1IBYKeHdYmastObHd3uDK4AorPBT5H2-t6HDpRVCFUD5Xqg1TKaHkqWHEW-kmbWUEW-Vy7WrXCBw4iJAGPrrg";
        //公钥
        String publicKey = "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqbNgffXW03mr64Cs3PHAOIvXWmiXRW2aMuC+AwrDJCgLGBNj0BcjIGQY2UmWQHGQkWZgW2w3B2kf5viqSwHX0iGE5qZJfTQrEulMA1DJ/oS60oxcKNEMzDulsOiPZJA/U9PszHNl9eigs6RwK0B4GIfqR3ImrUxrASLNHsI5vA29NboZ30JWoylVbKliwiUv+ZAz9oTWt7vT4rZV1u/9o2DDxG9K6Cx1ZXIZR3bUx3YEMQeUma5rquQ7BeyCXs2yb8TJ6RkISmq9PEzQEwFMp8jtvBrnodAr/6mKFpTxUO5F1VXUuotDXooXNhpxwJY6oMJ//zZaw94j4k8aY7bW/QIDAQAB-----END PUBLIC KEY-----";
        //校验Jwt
        Jwt jwt = JwtHelper.decodeAndVerify(token, new RsaVerifier(publicKey));

        //获取Jwt原始内容 载荷
        String claims = jwt.getClaims();
        System.out.println(claims);
        //jwt令牌
        String encoded = jwt.getEncoded();
        System.out.println(encoded);
    }
}
