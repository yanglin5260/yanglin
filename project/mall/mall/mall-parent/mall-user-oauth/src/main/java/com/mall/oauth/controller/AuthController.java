package com.mall.oauth.controller;

import com.mall.common.utils.ConstantUtil;
import com.mall.oauth.service.AuthService;
import com.mall.oauth.util.AuthToken;
import com.mall.oauth.util.CookieUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * AuthController
 * </p>
 *
 * @author yanglin
 * @date 2021-01-14 00:01:35
 */
@RestController
@RequestMapping(value = "/userx")
public class AuthController {

    @Autowired
    AuthService authService;
    //客户端ID
    @Value("${auth.clientId}")
    private String clientId;
    //秘钥
    @Value("${auth.clientSecret}")
    private String clientSecret;
    //Cookie存储的域名
    @Value("${auth.cookieDomain}")
    private String cookieDomain;
    //Cookie生命周期
    @Value("${auth.cookieMaxAge}")
    private int cookieMaxAge;

    @PostMapping("/login")
    public ResponseEntity<Map<String, Object>> login(String username, String password) {
        if (StringUtils.isEmpty(username)) {
            throw new RuntimeException("用户名不允许为空");
        }
        if (StringUtils.isEmpty(password)) {
            throw new RuntimeException("密码不允许为空");
        }
        //申请令牌
        AuthToken authToken = authService.login(username, password, clientId, clientSecret);

        //用户身份令牌
        String access_token = authToken.getAccessToken();
        //将令牌存储到cookie
        saveCookie(access_token);

        Map<String, Object> result = new HashMap<>(ConstantUtil.RESULT_MAP_INIT_COUNT);
        result.put("massage", "登录成功");
        return ResponseEntity.ok(result);
    }

    /***
     * 将令牌存储到cookie
     * @param token
     */
    private void saveCookie(String token) {
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        CookieUtil.addCookie(response, cookieDomain, "/", "Authorization", token, cookieMaxAge, false);
    }
}
