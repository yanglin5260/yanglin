package com.mall.oauth.service.impl;

import com.alibaba.fastjson.JSON;
import com.mall.oauth.service.LoginService;
import com.mall.oauth.util.AuthToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Base64;
import java.util.Map;

/**
 * <p>
 * LoginServiceImpl
 * </p>
 *
 * @author yanglin
 * @date 2021-01-14 00:01:52
 */
@Service
public class LoginServiceImpl implements LoginService {


    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private LoadBalancerClient loadBalancerClient;

    public static void main(String[] args) {
        byte[] decode = Base64.getDecoder().decode(new String("Y2hhbmdnb3UxOmNoYW5nZ291Mg==").getBytes());
        System.out.println(new String(decode));
    }

    @Override
    public AuthToken login(String username, String password, String clientId, String clientSecret, String grandType) {
        //1.定义url (申请令牌的url)
        //参数 : 微服务的名称spring.application指定的名称
        ServiceInstance choose = loadBalancerClient.choose("user-auth");
        String url = choose.getUri().toString() + "/oauth/token";
        //2.定义头信息 (有client id 和client secret)
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Basic " + Base64.getEncoder().encodeToString((clientId + ":" + clientSecret).getBytes()));
        //3. 定义请求体  有授权模式 用户的名称 和密码
        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
        formData.add("grant_type", grandType);
        formData.add("username", username);
        formData.add("password", password);
        //4.模拟浏览器 发送POST 请求 携带 头 和请求体 到认证服务器
        /**
         * 参数1  指定要发送的请求的url
         * 参数2  指定要发送的请求的方法 PSOT
         * 参数3 指定请求实体(包含头和请求体数据)
         */
        HttpEntity<MultiValueMap> requestEntity = new HttpEntity<>(formData, headers);
        ResponseEntity<Map> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, Map.class);
        //5.接收到返回的响应(就是:令牌的信息)
        Map body = responseEntity.getBody();
        //6.返回，使用FastJson将结果Map封装为AuthToken
        return JSON.parseObject(JSON.toJSONString(body), AuthToken.class);
    }
}
