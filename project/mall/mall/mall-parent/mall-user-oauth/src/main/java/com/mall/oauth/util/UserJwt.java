package com.mall.oauth.util;


import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * <p>
 * 丰富载荷信息
 * </p>
 *
 * @author yanglin
 * @date 2021-01-14 00:52:23
 */
@Getter
@Setter
public class UserJwt extends User {
    private String id;    //用户ID
    private String name;  //用户名字
    private String company; //设置公司

    public UserJwt(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }
}
