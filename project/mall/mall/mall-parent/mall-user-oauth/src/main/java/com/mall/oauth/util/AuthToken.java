package com.mall.oauth.util;

import lombok.Data;

/**
 * <p>
 * AuthToken
 * </p>
 *
 * @author yanglin
 * @date 2021-01-14 01:41:55
 */
@Data
public class AuthToken {
    //令牌信息
    String accessToken;
    // 令牌类型
    String tokenType;
    //刷新token(refresh_token)
    String refreshToken;
    // 过期时间（秒）
    Integer expiresIn;
    // 范围，与定义的客户端范围一致。
    String scope;
    //jwt短令牌
    String jti;
}