package com.mall.oauth.service;

import com.mall.oauth.util.AuthToken;

/**
 * <p>
 * AuthService
 * </p>
 *
 * @author yanglin
 * @date 2021-01-14 00:01:18
 */
public interface AuthService {

    /***
     * 授权认证方法
     */
    AuthToken login(String username, String password, String clientId, String clientSecret);
}
