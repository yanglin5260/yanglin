package com.mall.oauth.config;

import com.mall.oauth.util.UserJwt;
import com.mall.user.feign.UserFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.LinkedHashMap;
import java.util.List;

/*****
 * 自定义授权认证类
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    ClientDetailsService clientDetailsService;

    @Autowired
    private UserFeign userFeign;

    /****
     * 自定义授权认证
     * 这里最主要是实现了UserDetailsService接口中的loadUserByUsername方法，
     * 在执行登录的过程中，这个方法将根据用户名去查找用户，如果用户不存在，
     * 则抛出UsernameNotFoundException异常，否则直接将查到的Hr返回。
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //=========================客户端信息认证 start=========================
        //取出身份，如果身份为空说明没有认证
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        //没有认证统一采用httpbasic认证，httpbasic中存储了client_id和client_secret，开始认证client_id和client_secret
        if (authentication == null) {
            // 查询oauth_client_details表
            ClientDetails clientDetails = clientDetailsService.loadClientByClientId(username);
            if (clientDetails != null) {
                //秘钥
                String clientSecret = clientDetails.getClientSecret();
                //静态方式
//                return new User(
//                        username,// 客户端ID
//                        new BCryptPasswordEncoder().encode(clientSecret),// 客户端秘钥，加密操作
//                        AuthorityUtils.commaSeparatedStringToAuthorityList(""));// 权限

//                数据库查找方式
                return new User(
                        username,// 客户端ID
                        clientSecret, // 客户端秘钥，数据库查找的，已经加密
                        AuthorityUtils.commaSeparatedStringToAuthorityList(""));// 权限
            }
        }

        //=========================客户端信息认证 end=========================

        //=========================用户信息认证 start=========================
        if (StringUtils.isEmpty(username)) {
            return null;
        }

        //从数据库查询
        com.mall.user.pojo.User user = new com.mall.user.pojo.User();
        user.setUsername(username);
//        userFeign.getInfoById("1");
        List<com.mall.user.pojo.User> ul = userFeign.load(username).getBody();
        if (ul == null || ul.size() == 0) {
            return null;
        }

        //根据用户名查询用户信息
//        String pwd = new BCryptPasswordEncoder().encode("yanglin");
        //创建User对象
        String permissions = "ROLE_ADMIN,ROLE_USER";


        UserJwt userDetails = new UserJwt(username, ul.get(0).getPassword(), AuthorityUtils.commaSeparatedStringToAuthorityList(permissions));


        //=========================用户信息认证 end=========================

        //userDetails.setComy(songsi);
        return userDetails;
    }
}
