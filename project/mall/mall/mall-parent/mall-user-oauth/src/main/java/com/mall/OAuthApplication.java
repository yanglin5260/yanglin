package com.mall;

import com.mall.common.utils.ConstantUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.web.client.RestTemplate;

/**
 * <p>
 * TODO
 * </p>
 *
 * @author yanglin
 * @date 2021-01-13 12:24:54
 */
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScans(value = {
        @ComponentScan(basePackages = {ConstantUtil.COMMON_PACKAGE_NAME})
})
@EnableFeignClients(basePackages = {"com.mall.user.feign"})
public class OAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(OAuthApplication.class, args);
    }


    @Bean(name = "restTemplate")
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}