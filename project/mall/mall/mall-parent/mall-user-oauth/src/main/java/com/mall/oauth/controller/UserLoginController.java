package com.mall.oauth.controller;

import com.mall.common.utils.ConstantUtil;
import com.mall.oauth.service.LoginService;
import com.mall.oauth.util.AuthToken;
import com.mall.oauth.util.CookieUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * UserLoginController
 * </p>
 *
 * @author yanglin
 * @date 2021-01-14 00:02:15
 */
@RestController
@RequestMapping("/user")
public class UserLoginController {

    private static final String GRAND_TYPE = "password";//授权模式 密码模式
    @Autowired
    private LoginService loginService;
    @Value("${auth.clientId}")
    private String clientId;
    @Value("${auth.clientSecret}")
    private String clientSecret;
    @Value("${auth.cookieDomain}")
    private String cookieDomain;

    //Cookie生命周期
    @Value("${auth.cookieMaxAge}")
    private int cookieMaxAge;


    /**
     * 密码模式  认证.
     * 改造后，可以自己填充下面的数据，不用用户输入，更加人性化
     * clientId 客户端ID
     * clientSecret 客户端密码
     * GRAND_TYPE 授权模式 密码模式
     *
     * @param username
     * @param password
     * @return
     */
    @RequestMapping("/login")
    public ResponseEntity<Map<String, Object>> login(String username, String password) {
        Map<String, Object> result = new HashMap<>(ConstantUtil.RESULT_MAP_INIT_COUNT);
        //登录 之后生成令牌的数据返回
        AuthToken authToken = loginService.login(username, password, clientId, clientSecret, GRAND_TYPE);
        //设置到cookie中
        saveCookie(authToken.getAccessToken());

        result.put("massage", "令牌生成成功");
        result.put("authToken", authToken);
        return ResponseEntity.ok(result);
    }

    private void saveCookie(String token) {
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        CookieUtil.addCookie(response, cookieDomain, "/", "Authorization", token, cookieMaxAge, false);
    }
}
