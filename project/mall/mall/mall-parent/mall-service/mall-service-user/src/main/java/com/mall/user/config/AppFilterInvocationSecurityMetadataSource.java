package com.mall.user.config;

import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 该类的主要功能就是通过当前的请求地址，获取该地址需要的用户角色
 * </p>
 *
 * @author yanglin
 * @date 2021-01-15 00:46:50
 */
@Component
public class AppFilterInvocationSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

//    private FilterInvocationSecurityMetadataSource superMetadataSource;

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

//    public AppFilterInvocationSecurityMetadataSource(FilterInvocationSecurityMetadataSource expressionBasedFilterInvocationSecurityMetadataSource){
//        this.superMetadataSource = expressionBasedFilterInvocationSecurityMetadataSource;
//
//        // TODO 从数据库加载权限配置
//    }

    private final AntPathMatcher antPathMatcher = new AntPathMatcher();

    // 这里的需要从DB加载
    private final Map<String, String> urlRoleMap = new HashMap<String, String>() {{
        put("/open/**", "ROLE_ANONYMOUS");
        put("/health", "ROLE_ANONYMOUS");
        put("/address", "ROLE_ADMIN");

        // 过滤Swagger2
        put("/swagger-ui.html", "ROLE_LOGIN");
        put("/v2/api-docs", "ROLE_LOGIN");
        put("/definitions/**", "ROLE_LOGIN");
        put("/configuration/ui", "ROLE_LOGIN");
        put("/swagger-resources/**", "ROLE_LOGIN");
        put("/configuration/security", "ROLE_LOGIN");
        put("/webjars/**", "ROLE_LOGIN");
        put("/swagger-resources/configuration/ui", "ROLE_LOGIN");
    }};

    /**
     * <p>
     * getAuthorities，该方法用来获取当前用户所具有的角色
     * </p>
     *
     * @param object
     * @return java.util.Collection<org.springframework.security.access.ConfigAttribute>
     * @author yanglin
     * @date 2021-01-15 00:41:32
     */
    @Override
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        FilterInvocation fi = (FilterInvocation) object;
        String url = fi.getRequestUrl();

        for (Map.Entry<String, String> entry : urlRoleMap.entrySet()) {
            if (antPathMatcher.match(entry.getKey(), url)) {
                return SecurityConfig.createList(entry.getValue());
            }
        }
//        //没有匹配上的资源，都是登录访问
//        return SecurityConfig.createList("ROLE_LOGIN");
        return SecurityConfig.createList("ROLE_ANONYMOUS");
        //  返回代码定义的默认配置
//        return superMetadataSource.getAttributes(object);
    }


    @Override
    public boolean supports(Class<?> clazz) {
        return FilterInvocation.class.isAssignableFrom(clazz);
    }
}
