package com.mall.pay;

import com.mall.common.swagger2.Swagger2;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.core.env.Environment;

/**
 * 描述
 *
 * @author www.itheima.com
 * @version 1.0
 * @package com.changgou *
 * @since 1.0
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableEurekaClient
@ComponentScans(value = {
        @ComponentScan(basePackageClasses = {Swagger2.class})
})
public class WeixinPayApplication {
    public static void main(String[] args) {
        SpringApplication.run(WeixinPayApplication.class, args);
    }


    @Autowired
    private Environment env;

    //创建队列
    @Bean
    public Queue createQueue() {
        return new Queue(env.getProperty("mq.pay.queue.order"));
    }

    //创建交换机
    @Bean
    public DirectExchange basicExchange() {
        return new DirectExchange(env.getProperty("mq.pay.exchange.order"));
    }

    //绑定
    @Bean
    public Binding basicBinding() {
        return BindingBuilder.bind(createQueue()).to(basicExchange()).with(env.getProperty("mq.pay.routing.key"));
    }
}
