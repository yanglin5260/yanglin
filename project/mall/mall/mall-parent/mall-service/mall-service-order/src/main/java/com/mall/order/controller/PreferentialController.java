package com.mall.order.controller;

import com.mall.common.base.controller.BaseHibernateController;
import com.mall.order.pojo.Preferential;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * PreferentialController
 * </p>
 *
 * @author yanglin
 * @date 2021-01-19 03:02:42
 */
@RestController
@RequestMapping("/preferential")
public class PreferentialController extends BaseHibernateController<Preferential> {

}
