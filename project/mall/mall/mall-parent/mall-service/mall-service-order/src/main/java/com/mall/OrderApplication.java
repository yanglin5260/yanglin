package com.mall;

import com.mall.common.base.config.FeignInterceptor;
import com.mall.common.utils.ConstantUtil;
import com.mall.goods.feign.SkuFeign;
import com.mall.goods.feign.SpuFeign;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

/**
 * <p>
 * OrderApplication
 * </p>
 *
 * @author yanglin
 * @date 2021-01-19 03:11:32
 */
@SpringBootApplication
@EnableEurekaClient
@ComponentScans(value = {
        @ComponentScan(basePackages = {ConstantUtil.COMMON_PACKAGE_NAME})
})
@EntityScan(basePackages = {"com.mall.order.pojo"})
//@EnableFeignClients(basePackages = {"com.mall.goods.feign"})
@EnableFeignClients(basePackageClasses = {SkuFeign.class, SpuFeign.class})
public class OrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }

    /**
     * <p>
     * 开启服务器之间的验证，可以传递有效的Token
     * </p>
     *
     * @return com.mall.common.base.config.FeignInterceptor
     * @author yanglin
     * @date 2021-01-20 01:26:13
     */
    @Bean
    public FeignInterceptor feignInterceptor(){
        return new FeignInterceptor();
    }
}
