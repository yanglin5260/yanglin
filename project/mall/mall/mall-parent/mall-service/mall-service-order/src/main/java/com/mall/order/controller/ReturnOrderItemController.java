package com.mall.order.controller;

import com.mall.common.base.controller.BaseHibernateController;
import com.mall.order.pojo.ReturnOrderItem;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * ReturnOrderItemController
 * </p>
 *
 * @author yanglin
 * @date 2021-01-19 03:05:04
 */
@RestController
@RequestMapping("/returnOrder-item")
public class ReturnOrderItemController extends BaseHibernateController<ReturnOrderItem> {
}
