package com.mall.order.controller;

import com.mall.common.base.controller.BaseHibernateController;
import com.mall.order.pojo.OrderLog;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * TODO
 * </p>
 *
 * @author yanglin
 * @date 2021-01-19 03:01:47
 */
@RestController
@RequestMapping("/order-log")
public class OrderLogController extends BaseHibernateController<OrderLog> {

}
