package com.mall.order.controller;

import com.mall.common.base.controller.BaseHibernateController;
import com.mall.common.base.service.BaseHibernateService;
import com.mall.order.pojo.Order;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * <p>
 * OrderController
 * </p>
 *
 * @author yanglin
 * @date 2021-01-19 03:00:16
 */
@RestController
@RequestMapping("/order")
public class OrderController extends BaseHibernateController<Order> {


    @Autowired
    private BaseHibernateService<Order> baseHibernateService;


    @Autowired
    private RabbitTemplate rabbitTemplate;//主要是为了添加订单时候，保证订单的过期时间

    @PostMapping("mq-add-order")
    public ResponseEntity<Order> add(@RequestBody Order t) throws SecurityException, IllegalArgumentException {
        Order order = baseHibernateService.save(t);
        System.out.println("创建订单时间" + new Date());
        rabbitTemplate.convertAndSend("orderDelayQueue", (Object) order.getId(), new MessagePostProcessor() {
            @Override
            public Message postProcessMessage(Message message) throws AmqpException {
                //设置延时读取,10秒,可以自由更改订单过期时间，10秒后处罚监听执行过程，删除订单或者使订单失效，
                message.getMessageProperties().setExpiration("10000");
                return message;
            }
        });
        return ResponseEntity.ok(order);
    }
}
