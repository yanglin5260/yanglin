package com.mall.order.mq.queue;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QueueConfig {

    /**
     * <p>
     * 创建队列Queue1，延时队列，会过期，过期后会发送数据给Queue2
     * </p>
     *
     * @return org.springframework.amqp.core.Queue
     * @author yanglin
     * @date 2021-01-22 01:30:18
     */
    @Bean
    public Queue orderDelayQueue() {
//        return new Queue("orderListenerQueue");
        return QueueBuilder.durable("orderDelayQueue")
                // 死信队列：在RabbitMQ中的一个队列中的信息没有被读取，过期了之后，这个队列就叫死信队列
                // orderDelayQueue会过期，过期后进入到死信队列，会发送数据给Queue2，就路由到orderListenerQueue中，也就是当前队列
                .withArgument("x-dead-letter-exchange", "orderListenerExchange")
                .withArgument("x-dead-letter-routing-key", "orderListenerQueue")
                .build();
    }

    /**
     * <p>
     * 创建队列Queue2
     * </p>
     *
     * @return org.springframework.amqp.core.Queue
     * @author yanglin
     * @date 2021-01-22 01:31:12
     */
    @Bean
    public Queue orderListenerQueue() {
        return new Queue("orderListenerQueue", true);//true:是持久化的
    }

    //创建交换机
    @Bean
    public DirectExchange orderListenerExchange() {
        return new DirectExchange("orderListenerExchange");
    }

    //绑定
    @Bean
    public Binding orderListenerBinding(Queue orderListenerQueue, DirectExchange orderListenerExchange) {
        return BindingBuilder.bind(orderListenerQueue).to(orderListenerExchange).with("orderListenerQueue");
    }
}
