package com.mall.order.controller;

import com.mall.common.base.controller.BaseHibernateController;
import com.mall.order.pojo.OrderConfig;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * OrderConfigController
 * </p>
 *
 * @author yanglin
 * @date 2021-01-19 02:59:32
 */
@RestController
@RequestMapping("/order-config")
public class OrderConfigController extends BaseHibernateController<OrderConfig> {

}
