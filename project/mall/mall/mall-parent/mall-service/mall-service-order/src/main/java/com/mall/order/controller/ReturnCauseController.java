package com.mall.order.controller;

import com.mall.common.base.controller.BaseHibernateController;
import com.mall.order.pojo.ReturnCause;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * ReturnCauseController
 * </p>
 *
 * @author yanglin
 * @date 2021-01-19 03:03:32
 */
@RestController
@RequestMapping("/return-cause")
public class ReturnCauseController extends BaseHibernateController<ReturnCause> {

}
