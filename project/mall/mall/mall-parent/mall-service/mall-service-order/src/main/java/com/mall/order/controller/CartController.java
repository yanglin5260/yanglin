package com.mall.order.controller;

//import com.mall.order.config.TokenDecode;

import com.mall.order.config.TokenDecode;
import com.mall.order.pojo.OrderItem;
import com.mall.order.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 购物车
 * </p>
 *
 * @author yanglin
 * @date 2021-01-19 12:40:42
 */
@RestController
@RequestMapping("/cart")
public class CartController {

    @Autowired
    private CartService cartService;

    @Autowired
    private TokenDecode tokenDecode;

    /**
     * 添加购物车
     *
     * @param id  要购买的商品的SID
     * @param num 要购买的数量
     * @return
     */
    @PutMapping("/add")
    public ResponseEntity<Map<String, Object>> add(String id, Integer num) {
        //springsecurity 获取当前的用户名 传递service
        Map<String, Object> result = new HashMap<>();
        Map<String, String> userInfo = tokenDecode.getUserInfo();
        String username = userInfo.get("username");
//        String username = "yanglin";
//        String username = userInfo.get("username");

        System.out.println("哇塞::用户名:" + username);

        cartService.add(id, num, username);
        result.put("message", "添加成功");
        return ResponseEntity.ok(result);

    }

    @GetMapping("/list")
    public ResponseEntity<List<OrderItem>> list() {
        Map<String, String> userInfo = tokenDecode.getUserInfo();
        String username = userInfo.get("username");
//        String username = "yanglin";
        System.out.println("哇塞::用户名:" + username);
        List<OrderItem> orderItemList = cartService.list(username);
        return ResponseEntity.ok(orderItemList);


    }


}
