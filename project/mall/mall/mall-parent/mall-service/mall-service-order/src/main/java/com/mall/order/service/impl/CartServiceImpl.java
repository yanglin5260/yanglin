package com.mall.order.service.impl;

import com.mall.goods.feign.SkuFeign;
import com.mall.goods.feign.SpuFeign;
import com.mall.goods.pojo.Sku;
import com.mall.goods.pojo.Spu;
import com.mall.order.pojo.OrderItem;
import com.mall.order.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * 描述
 *
 * @author www.itheima.com
 * @version 1.0
 * @package com.changgou.order.service.impl *
 * @since 1.0
 */
@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private SkuFeign skuFeign;

    @Autowired
    private SpuFeign spuFeign;

    @Autowired
    private RedisTemplate redisTemplate;


    //创建feign (SKUfeign SPU的feign)

    @Override
    public void add(String id, Integer num, String username) {

        if (num <= 0) {
            //删除掉原来的商品
            redisTemplate.boundHashOps("Cart_" + username).delete(id);
            //如果购物车为空，连购物车一起移除
            Long size = redisTemplate.boundHashOps("Cart_" + username).size();
            if (size == null || size <= 0) {
                redisTemplate.delete("Cart_" + username);
            }
            return;
        }

        //1.根据商品的SKU的ID 获取sku的数据
        Sku skuResult = skuFeign.getInfoById(id).getBody();

        //
//        Sku data = skuResult.getData();
//
        if (skuResult != null) {

//            Spu spuResult = spuFeign.getInfoById(spuId);
            Spu spuResult = spuFeign.getInfoById(skuResult.getSpuId()).getBody();

            //3.将数据存储到 购物车对象(order_item)中

            OrderItem orderItem = new OrderItem();

            orderItem.setCategoryId1(spuResult.getCategory1Id());
            orderItem.setCategoryId2(spuResult.getCategory2Id());
            orderItem.setCategoryId3(spuResult.getCategory3Id());
            orderItem.setSpuId(spuResult.getId());
            orderItem.setSkuId(id);
            orderItem.setName(skuResult.getName());//商品的名称  sku的名称
            orderItem.setPrice(skuResult.getPrice());//sku的单价
            orderItem.setNum(num);//购买的数量
            orderItem.setMoney(orderItem.getPrice().multiply(BigDecimal.valueOf(orderItem.getNum())));//单价* 数量
            orderItem.setPayMoney(orderItem.getPrice().multiply(BigDecimal.valueOf(orderItem.getNum())));//单价* 数量
            orderItem.setImage(skuResult.getImage());//商品的图片dizhi
            //4.数据添加到redis中  key:用户名 field:sku的ID  value:购物车数据(order_item)

            redisTemplate.boundHashOps("Cart_" + username).put(id, orderItem);// hset key field value   hget key field
        }

    }

    @Override
    public List<OrderItem> list(String username) {
        List<OrderItem> orderItemList = redisTemplate.boundHashOps("Cart_" + username).values();
        return orderItemList;
    }
}
