package com.mall.order.controller;

import com.mall.common.base.controller.BaseHibernateController;
import com.mall.order.pojo.ReturnOrder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * ReturnOrderController
 * </p>
 *
 * @author yanglin
 * @date 2021-01-19 03:04:06
 */
@RestController
@RequestMapping("/returnOrder")
public class ReturnOrderController extends BaseHibernateController<ReturnOrder> {

}
