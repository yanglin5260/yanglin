package com.mall.order.controller;

import com.mall.common.base.controller.BaseHibernateController;
import com.mall.goods.pojo.Album;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * UndoLogController
 * </p>
 *
 * @author yanglin
 * @date 2021-01-19 03:05:34
 */
@RestController
@RequestMapping("/undo-log")
public class UndoLogController extends BaseHibernateController<Album> {

}
