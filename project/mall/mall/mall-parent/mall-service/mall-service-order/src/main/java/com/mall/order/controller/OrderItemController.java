package com.mall.order.controller;

import com.mall.common.base.controller.BaseHibernateController;
import com.mall.order.pojo.OrderItem;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * OrderItemController
 * </p>
 *
 * @author yanglin
 * @date 2021-01-19 03:00:58
 */
@RestController
@RequestMapping("/order-item")
public class OrderItemController extends BaseHibernateController<OrderItem> {

}
