package com.mall.order.mq.listener;

import com.alibaba.fastjson.JSON;
import com.mall.common.base.service.BaseHibernateService;
import com.mall.common.utils.ConstantUtil;
import com.mall.order.pojo.Order;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

/**
 * <p>
 * 订单支付状态监听，通过RabbitMQ监听
 * </p>
 *
 * @author yanglin
 * @date 2021-01-21 23:40:36
 */
@Component
@RabbitListener(queues = "${mq.pay.queue.order}")
public class OrderUpdateListener {


    @Autowired
    private BaseHibernateService<Order> orderService;

    @RabbitHandler
    public void handlerData(String msg) {
        //1.接收消息(有订单的ID  有transaction_id )
        Map<String, String> map = JSON.parseObject(msg, Map.class);
        //2.更新对营的订单的状态
        if (map != null) {
            if (map.get("return_code").equalsIgnoreCase("success")) {
                Order order = new Order();
                order.setId(map.get("out_trade_no"));
                order.setPayTime(new Date());//支付时间
                order.setTransactionId(map.get("transaction_id"));//设置交易流水号
                order.setPayStatus("1");//设置支付成功状态
                orderService.save(order);
            } else {
                // 关闭支付。库存回滚
                //删除订单 支付失败.....（假删除）
                Order order = new Order();
                order.setId(map.get("out_trade_no"));
                order.setIsDelete(ConstantUtil.IS_DELETE);
                order.setOrderStatus("2");//支付失败状态
                orderService.save(order);
            }
        }
    }
}
