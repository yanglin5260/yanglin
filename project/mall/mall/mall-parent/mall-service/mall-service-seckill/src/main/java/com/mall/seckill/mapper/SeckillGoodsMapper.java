package com.mall.seckill.mapper;

import com.mall.seckill.pojo.SeckillGoods;
import org.apache.ibatis.annotations.Mapper;


/**
 * <p>
 * SeckillGoodsMapper
 * </p>
 *
 * @author yanglin
 * @date 2021-01-23 00:32:40
 */
@Mapper
public interface SeckillGoodsMapper extends MyBatisMapper<SeckillGoods> {
}
