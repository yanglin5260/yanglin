package com.mall.seckill.controller;

import com.mall.common.base.controller.BaseHibernateController;
import com.mall.seckill.pojo.SeckillOrder;
import com.mall.seckill.pojo.SeckillStatus;
import com.mall.seckill.service.SeckillOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * SeckillOrder
 * </p>
 *
 * @author yanglin
 * @date 2021-01-22 21:50:43
 */
@RestController
@RequestMapping("/seckill-order")
public class SeckillOrderController extends BaseHibernateController<SeckillOrder> {

    @Autowired
    private SeckillOrderService seckillOrderService;

    /**
     * 下单
     *
     * @param time 时间段
     * @param id   秒杀商品的ID
     * @return
     */
    @GetMapping("/add")
    public ResponseEntity<Map<String, Object>> add(String time, String id) {
        Map<String, Object> result = new HashMap<>();

        //1.获取当前登录的用户的名称
        String username = "yanglin";//测试用写死
//        String username = System.currentTimeMillis() + "yanglin";//测试生成不同的用户名测试超卖问题，暂时的

        //2.调用service的方法创建订单
        boolean flag = seckillOrderService.add(id, time, username);
        if (flag) {
            result.put("message", "正在排队中.....");
            return ResponseEntity.ok(result);
        }
        result.put("message", "抢单失败");
        return ResponseEntity.ok(result);
    }

    /**
     * <p>
     * 抢单状态查询
     * </p>
     *
     * @return org.springframework.http.ResponseEntity<com.mall.seckill.pojo.SeckillStatus>
     * @author yanglin
     * @date 2021-01-23 13:50:32
     */
    @GetMapping("/query")
    public ResponseEntity<SeckillStatus> queryStatus() {
        try {
            //1.获取当前登录的用户的名称
            String username = "yanglin";//测试用写死

            SeckillStatus seckillStatus = seckillOrderService.queryStatus(username);

            return ResponseEntity.ok(seckillStatus);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


}
