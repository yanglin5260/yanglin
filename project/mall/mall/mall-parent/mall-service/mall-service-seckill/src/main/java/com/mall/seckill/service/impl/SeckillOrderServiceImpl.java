package com.mall.seckill.service.impl;

import com.mall.common.utils.SystemConstants;
import com.mall.seckill.pojo.SeckillStatus;
import com.mall.seckill.service.SeckillOrderService;
import com.mall.seckill.task.MultiThreadingCreateOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;

/****
 * @Author:admin
 * @Description:SeckillOrder业务层接口实现类
 * @Date 2019/6/14 0:16
 *****/
@Service
public class SeckillOrderServiceImpl implements SeckillOrderService {


    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private MultiThreadingCreateOrder multiThreadingCreateOrder;

    @Override
    public boolean add(String id, String time, String username) {

        //1. 验证是否重复下单
        // 该当前的用户加一个数字+1   incr         使用hash    key field  2
        Long userQueueCount = redisTemplate.boundHashOps(SystemConstants.SEC_KILL_QUEUE_REPEAT_KEY).increment(username, 1);

        //判断 是否大于1 如果是,返回 ,否则 就放行 重复了.
        if (userQueueCount > 1) {
            throw new RuntimeException("20006，重复下单了");
        }


        /**
         * username 抢单的用户是谁
         * status 1  表示抢单的状态 (1.排队中)
         * id 抢的商品的ID
         * time :抢的商品的所属时间段
         */
        SeckillStatus seckillStatus = new SeckillStatus();
        seckillStatus.setUsername(username);
        seckillStatus.setStatus(1);
        seckillStatus.setGoodsId(id);
        seckillStatus.setTime(time);
        seckillStatus.setCreateTime(new Date());

        //进入排队中，左边存储，进入队列leftPush
        redisTemplate.boundListOps(SystemConstants.SEC_KILL_USER_QUEUE_KEY).leftPush(seckillStatus);


        //进入排队标识
        redisTemplate.boundHashOps(SystemConstants.SEC_KILL_USER_STATUS_KEY).put(username, seckillStatus);


        //多线程下单
        multiThreadingCreateOrder.createrOrder();

        return true;
    }

    @Override
    public SeckillStatus queryStatus(String username) {
        return (SeckillStatus) redisTemplate.boundHashOps(SystemConstants.SEC_KILL_USER_STATUS_KEY).get(username);
    }
}
