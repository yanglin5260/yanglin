package com.mall.seckill.controller;

import com.mall.common.base.controller.BaseHibernateController;
import com.mall.common.utils.DateUtil;
import com.mall.seckill.pojo.SeckillGoods;
import com.mall.seckill.service.SeckillGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * SeckillGoodsController
 * </p>
 *
 * @author yanglin
 * @date 2021-01-23 01:29:44
 */
@RestController
@RequestMapping("/seckill-goods")
public class SeckillGoodsController extends BaseHibernateController<SeckillGoods> {

    @Autowired
    private SeckillGoodsService seckillGoodsService;

    /**
     * <p>
     * 获取当前的时间基准的5个时间段
     * 秒杀商品时间菜单获取
     * </p>
     *
     * @author yanglin
     * @date 2021-01-23 02:01:23
     */
    @GetMapping("/menus")
    public List<Date> datemenus() {
        List<Date> dateMenus = DateUtil.getDateMenus();
        for (Date date : dateMenus) {
            System.out.println(date);
        }
        return dateMenus;
    }

    /**
     * 根据时间段(2021012301) 查询该时间段的所有的秒品
     *
     * @param time
     * @return
     */
    @PostMapping("/list")
    public List<SeckillGoods> list(@RequestBody Date time) {
        List<SeckillGoods> seckillGoodsList = seckillGoodsService.list(new SimpleDateFormat("yyyyMMddHH").format(time));
        for (SeckillGoods seckillGoods : seckillGoodsList) {
            System.out.println("id是:" + seckillGoods.getId());
        }
        return seckillGoodsList;
    }


    /**
     * 根据时间段  和秒杀商品的ID 获取商的数据
     *
     * @param time
     * @param id
     * @return
     */
    @GetMapping("/one")
    public SeckillGoods one(String time, String id) {
//        time = "2021012218";
//        id = "802299553253806080";
        SeckillGoods seckillGoods = seckillGoodsService.one(time, id);
        return seckillGoods;
    }


}
