package com.mall.seckill.service;

import com.mall.seckill.pojo.SeckillGoods;
import com.github.pagehelper.PageInfo;

import java.util.List;

/****
 * @Author:admin
 * @Description:SeckillGoods业务层接口
 * @Date 2019/6/14 0:16
 *****/
public interface SeckillGoodsService {



    List<SeckillGoods> list(String time);


    SeckillGoods one(String time, String id);
}
