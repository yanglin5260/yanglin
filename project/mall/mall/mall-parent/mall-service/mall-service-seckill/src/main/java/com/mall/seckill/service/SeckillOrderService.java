package com.mall.seckill.service;

import com.mall.seckill.pojo.SeckillStatus;

/****
 * @Author:admin
 * @Description:SeckillOrder业务层接口
 * @Date 2019/6/14 0:16
 *****/
public interface SeckillOrderService {

    /**
     * 下单
     *
     * @param id
     * @param time
     * @param username
     * @return
     */
    boolean add(String id, String time, String username);

    /**
     * 查询状态
     *
     * @param username
     * @return
     */
    SeckillStatus queryStatus(String username);
}
