package com.mall.user.feign;

import com.mall.common.utils.ConstantUtil;
import com.mall.user.pojo.User;
import feign.Param;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * User Feign
 * </p>
 *
 * @author yanglin
 * @date 2020-11-25 22:37:26
 */
@FeignClient(name = "user")
@RequestMapping("/user")
public interface UserFeign {

    /**
     * <p>
     * 根据ID获取用户信息
     * </p>
     *
     * @param id
     * @return org.springframework.http.ResponseEntity<com.mall.user.pojo.User>
     * @author yanglin
     * @date 2020-11-25 22:32:18
     */
    @GetMapping("{id}")
    ResponseEntity<User> getInfoById(@PathVariable String id);

    /**
     * <p>
     * 修改用户信息
     * </p>
     *
     * @param id
     * @param t
     * @return org.springframework.http.ResponseEntity<com.mall.user.pojo.User>
     * @author yanglin
     * @date 2020-11-25 22:32:01
     */
    @PutMapping("{id}")
    ResponseEntity<User> update(@PathVariable String id, @RequestBody User t);

    /**
     * <p>
     * 根据名字查找账号
     * </p>
     *
     * @param username
     * @return org.springframework.http.ResponseEntity<java.util.List<com.mall.user.pojo.User>>
     * @author yanglin
     * @date 2021-01-19 01:47:16
     */
    @GetMapping("/load/{username}")
    ResponseEntity<List<User>> load(@PathVariable String username);

}
