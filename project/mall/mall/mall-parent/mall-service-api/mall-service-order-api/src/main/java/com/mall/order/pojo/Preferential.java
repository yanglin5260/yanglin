package com.mall.order.pojo;

import com.mall.common.base.pojo.CommonPo;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * <p>
 * Preferential
 * </p>
 *
 * @author yanglin
 * @date 2021-01-19 02:46:45
 */
@Data
@Entity
@Table(name = "preferential")
public class Preferential extends CommonPo {

    @Column(name = "buy_money")
    private Integer buyMoney;//消费金额

    @Column(name = "pre_money")
    private Integer preMoney;//优惠金额

    @Column(name = "category_id")
    private Long categoryId;//品类ID

    @Column(name = "start_time")
    private Date startTime;//活动开始日期

    @Column(name = "end_time")
    private Date endTime;//活动截至日期

    @Column(name = "state")
    private String state;//状态

    @Column(name = "type")
    private String type;//类型1不翻倍 2翻倍


}
