package com.mall.order.pojo;

import com.mall.common.base.pojo.CommonPo;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * <p>
 * ReturnCause
 * </p>
 *
 * @author yanglin
 * @date 2021-01-19 02:47:26
 */
@Data
@Entity
@Table(name = "return_cause")
public class ReturnCause extends CommonPo {

    @Column(name = "cause")
    private String cause;//原因

    @Column(name = "status")
    private String status;//是否启用


}
