package com.mall.order.pojo;

import com.mall.common.base.pojo.CommonPo;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * <p>
 * ReturnOrderItem
 * </p>
 *
 * @author yanglin
 * @date 2021-01-19 02:49:03
 */
@Data
@Entity
@Table(name = "return_order_item")
public class ReturnOrderItem extends CommonPo {

    @Column(name = "category_id")
    private Long categoryId;//分类ID

    @Column(name = "spu_id")
    private Long spuId;//SPU_ID

    @Column(name = "sku_id")
    private Long skuId;//SKU_ID

    @Column(name = "order_id")
    private Long orderId;//订单ID

    @Column(name = "order_item_id")
    private Long orderItemId;//订单明细ID

    @Column(name = "return_order_id")
    private Long returnOrderId;//退货订单ID

    @Column(name = "title")
    private String title;//标题

    @Column(name = "price")
    private Integer price;//单价

    @Column(name = "num")
    private Integer num;//数量

    @Column(name = "money")
    private Integer money;//总金额

    @Column(name = "pay_money")
    private Integer payMoney;//支付金额

    @Column(name = "image")
    private String image;//图片地址

    @Column(name = "weight")
    private Integer weight;//重量


}
