package com.mall.order.pojo;

import com.mall.common.base.pojo.CommonPo;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * <p>
 * CategoryReport
 * </p>
 *
 * @author yanglin
 * @date 2021-01-19 02:40:33
 */
@Data
@Entity
@Table(name = "category_report")
public class CategoryReport extends CommonPo {

    @Column(name = "category_id1")
    private Integer categoryId1;//1级分类

    @Column(name = "category_id2")
    private Integer categoryId2;//2级分类

    @Column(name = "category_id3")
    private Integer categoryId3;//3级分类

    @Column(name = "num")
    private Integer num;//销售数量

    @Column(name = "money")
    private Integer money;//销售额

}
