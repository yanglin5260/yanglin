package com.mall.order.pojo;

import com.mall.common.base.pojo.CommonPo;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * <p>
 * Order
 * </p>
 *
 * @author yanglin
 * @date 2021-01-19 02:41:51
 */
@Data
@Entity
@Table(name = "t_order")
public class Order extends CommonPo {
    @Column(name = "total_num")
    private Integer totalNum;//数量合计

    @Column(name = "total_money")
    private Integer totalMoney;//金额合计

    @Column(name = "pre_money")
    private Integer preMoney;//优惠金额

    @Column(name = "post_fee")
    private Integer postFee;//邮费

    @Column(name = "pay_money")
    private Integer payMoney;//实付金额

    @Column(name = "pay_type")
    private String payType;//支付类型，1、在线支付、0 货到付款

    @Column(name = "pay_time")
    private Date payTime;//付款时间

    @Column(name = "consign_time")
    private Date consignTime;//发货时间

    @Column(name = "end_time")
    private Date endTime;//交易完成时间

    @Column(name = "close_time")
    private Date closeTime;//交易关闭时间

    @Column(name = "shipping_name")
    private String shippingName;//物流名称

    @Column(name = "shipping_code")
    private String shippingCode;//物流单号

    @Column(name = "username")
    private String username;//用户名称

    @Column(name = "buyer_message")
    private String buyerMessage;//买家留言

    @Column(name = "buyer_rate")
    private String buyerRate;//是否评价

    @Column(name = "receiver_contact")
    private String receiverContact;//收货人

    @Column(name = "receiver_mobile")
    private String receiverMobile;//收货人手机

    @Column(name = "receiver_address")
    private String receiverAddress;//收货人地址

    @Column(name = "source_type")
    private String sourceType;//订单来源：1:web，2：app，3：微信公众号，4：微信小程序  5 H5手机页面

    @Column(name = "transaction_id")
    private String transactionId;//交易流水号

    @Column(name = "order_status")
    private String orderStatus;//订单状态,0:未完成,1:已完成，2：已退货

    @Column(name = "pay_status")
    private String payStatus;//支付状态,0:未支付，1：已支付，2：支付失败

    @Column(name = "consign_status")
    private String consignStatus;//发货状态,0:未发货，1：已发货，2：已收货

}
