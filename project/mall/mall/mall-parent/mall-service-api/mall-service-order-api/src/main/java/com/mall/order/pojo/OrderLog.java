package com.mall.order.pojo;

import com.mall.common.base.pojo.CommonPo;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * <p>
 * OrderLog
 * </p>
 *
 * @author yanglin
 * @date 2021-01-19 02:45:42
 */
@Data
@Entity
@Table(name = "order_log")
public class OrderLog extends CommonPo {

    @Column(name = "operater")
    private String operater;//操作员

    @Column(name = "operate_time")
    private Date operateTime;//操作时间

    @Column(name = "order_id")
    private String orderId;//订单ID

    @Column(name = "order_status")
    private String orderStatus;//订单状态,0未完成，1已完成，2，已退货

    @Column(name = "pay_status")
    private String payStatus;//付款状态

    @Column(name = "consign_status")
    private String consignStatus;//发货状态

    @Column(name = "remarks")
    private String remarks;//备注

    @Column(name = "money")
    private Integer money;//支付金额

    @Column(name = "username")
    private String username;//


}
