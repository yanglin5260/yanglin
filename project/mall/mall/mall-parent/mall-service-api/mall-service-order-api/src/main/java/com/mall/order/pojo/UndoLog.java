package com.mall.order.pojo;

import com.mall.common.base.pojo.CommonPo;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * <p>
 * UndoLog
 * </p>
 *
 * @author yanglin
 * @date 2021-01-19 02:49:53
 */
@Data
@Entity
@Table(name = "undo_log")
public class UndoLog extends CommonPo {

    @Column(name = "branch_id")
    private Long branchId;//

    @Column(name = "xid")
    private String xid;//

    @Column(name = "rollback_info")
    private String rollbackInfo;//

    @Column(name = "log_status")
    private Integer logStatus;//

    @Column(name = "log_created")
    private Date logCreated;//

    @Column(name = "log_modified")
    private Date logModified;//

    @Column(name = "ext")
    private String ext;//

}
