package com.mall.order.pojo;

import com.mall.common.base.pojo.CommonPo;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * <p>
 * OrderConfig
 * </p>
 *
 * @author yanglin
 * @date 2021-01-19 02:44:20
 */
@Data
@Entity
@Table(name = "order_config")
public class OrderConfig extends CommonPo {

    @Column(name = "order_timeout")
    private Integer orderTimeout;//正常订单超时时间（分）

    @Column(name = "seckill_timeout")
    private Integer seckillTimeout;//秒杀订单超时时间（分）

    @Column(name = "take_timeout")
    private Integer takeTimeout;//自动收货（天）

    @Column(name = "service_timeout")
    private Integer serviceTimeout;//售后期限

    @Column(name = "comment_timeout")
    private Integer commentTimeout;//自动五星好评


}
