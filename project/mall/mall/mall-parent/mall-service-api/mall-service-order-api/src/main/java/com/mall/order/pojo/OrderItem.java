package com.mall.order.pojo;

import com.mall.common.base.pojo.CommonPo;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * <p>
 * OrderItem
 * </p>
 *
 * @author yanglin
 * @date 2021-01-19 02:44:52
 */
@Data
@Entity
@Table(name = "order_item")
public class OrderItem extends CommonPo {

    @Column(name = "category_id1")
    private String categoryId1;//1级分类

    @Column(name = "category_id2")
    private String categoryId2;//2级分类

    @Column(name = "category_id3")
    private String categoryId3;//3级分类

    @Column(name = "spu_id")
    private String spuId;//SPU_ID

    @Column(name = "sku_id")
    private String skuId;//SKU_ID

    @Column(name = "order_id")
    private String orderId;//订单ID


    @Column(name = "price")
    private BigDecimal price;//单价

    @Column(name = "num")
    private Integer num;//数量

    @Column(name = "money")
    private BigDecimal money;//总金额

    @Column(name = "pay_money")
    private BigDecimal payMoney;//实付金额

    @Column(name = "image")
    private String image;//图片地址

    @Column(name = "weight")
    private Integer weight;//重量

    @Column(name = "post_fee")
    private Integer postFee;//运费

    @Column(name = "is_return")
    private String isReturn;//是否退货,0:未退货，1：已退货

}
