package com.mall.goods.feign;

import com.mall.goods.pojo.Spu;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 * Spu Feign
 * </p>
 *
 * @author yanglin
 * @date 2020-10-31 17:26:46
 */
@FeignClient(value = "goods")
//@FeignClient(value = "goods", url = "${eureka.server.host}")
@RequestMapping("/spu")
public interface SpuFeign {

//    @PostMapping
//    ResponseEntity<Spu> add(@RequestBody Spu t);
//
//    @PutMapping("{id}")
//    ResponseEntity<Spu> update(@PathVariable String id, @RequestBody Spu t);
//
//    @DeleteMapping("{id}")
//    ResponseEntity<Spu> deleteById(@PathVariable String id);

    @GetMapping("/{id}")
    ResponseEntity<Spu> getInfoById(@PathVariable String id);
//
//
//    @PostMapping("batchAdd")
//    ResponseEntity<List<Spu>> batchAdd(@RequestBody List<Spu> t);
//
//    @DeleteMapping("deleteByCond")
//    ResponseEntity<List<Spu>> deleteByCond(@RequestBody Spu t);
}
