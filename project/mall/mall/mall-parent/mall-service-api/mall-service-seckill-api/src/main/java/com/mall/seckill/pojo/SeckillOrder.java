package com.mall.seckill.pojo;

import com.mall.common.base.pojo.CommonPo;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * SeckillOrder
 * </p>
 *
 * @author yanglin
 * @date 2021-01-22 21:34:53
 */
@Data
@Entity
@Table(name = "seckill_order")
public class SeckillOrder extends CommonPo {

    @Column(name = "seckill_id")
    private String seckillId;//秒杀商品ID

    @Column(name = "money")
    private BigDecimal money;//支付金额

    @Column(name = "user_id")
    private String userId;//用户

    @Column(name = "pay_time")
    private Date payTime;//支付时间

    @Column(name = "status")
    private String status;//状态，0未支付，1已支付

    @Column(name = "receiver_address")
    private String receiverAddress;//收货人地址

    @Column(name = "receiver_mobile")
    private String receiverMobile;//收货人电话

    @Column(name = "receiver")
    private String receiver;//收货人

    @Column(name = "transaction_id")
    private String transactionId;//交易流水


}
