package com.mall.seckill.pojo;

import com.mall.common.base.pojo.CommonPo;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * TODO
 * </p>
 *
 * @author yanglin
 * @date 2021-01-22 21:35:31
 */
@Data
@Entity
@Table(name = "seckill_goods")
public class SeckillGoods extends CommonPo {

    @Column(name = "sup_id")
    private String supId;//spu ID

    @Column(name = "sku_id")
    private String skuId;//sku ID

    @Column(name = "small_pic")
    private String smallPic;//商品图片

    @Column(name = "price")
    private BigDecimal price;//原价格

    @Column(name = "cost_price")
    private BigDecimal costPrice;//秒杀价格

    @Column(name = "check_time")
    private Date checkTime;//审核日期

    @Column(name = "status")
    private String status;//审核状态，0未审核，1审核通过，2审核不通过

    @Column(name = "start_time")
    private Date startTime;//开始时间

    @Column(name = "end_time")
    private Date endTime;//结束时间

    @Column(name = "num")
    private Integer num;//秒杀商品数

    @Column(name = "stock_count")
    private Integer stockCount;//剩余库存数

    @Column(name = "introduction")
    private String introduction;//描述

}
