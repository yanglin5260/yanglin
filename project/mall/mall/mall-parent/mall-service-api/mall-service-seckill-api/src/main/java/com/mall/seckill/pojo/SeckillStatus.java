package com.mall.seckill.pojo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * SeckillStatus
 * </p>
 *
 * @author yanglin
 * @date 2021-01-22 21:48:46
 */
@Data
public class SeckillStatus implements Serializable {

    //秒杀用户名
    private String username;
    //创建时间
    private Date createTime;
    //秒杀状态  1:排队中，2:秒杀等待支付,3:支付超时，4:秒杀失败,5:支付完成
    private Integer status;
    //秒杀的商品ID
    private String goodsId;
    //应付金额
    private BigDecimal money;
    //订单号
    private String orderId;
    //时间段
    private String time;

}
