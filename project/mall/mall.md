<h2>目录</h2>

<details open>
  <summary><a href="#1-框架搭建">1. 框架搭建</a></summary>
  <ul>
  <details open>
    <summary><a href="#11-走进电商">1.1. 走进电商</a>  </summary>
    <ul>
      <a href="#111-电商行业分析">1.1.1. 电商行业分析</a><br>
      <a href="#112-电商系统技术特点">1.1.2. 电商系统技术特点</a><br>
      <a href="#113-主要电商模式">1.1.3. 主要电商模式</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#12-畅购-需求分析与系统设计">1.2. 畅购-需求分析与系统设计</a>  </summary>
    <ul>
      <a href="#121-需求分析">1.2.1. 需求分析</a><br>
    <details open>
      <summary><a href="#122-系统设计">1.2.2. 系统设计</a>    </summary>
      <ul>
        <a href="#1221-前后端分离">1.2.2.1. 前后端分离</a><br>
        <a href="#1222-技术架构">1.2.2.2. 技术架构</a><br>
        <a href="#1223-系统架构图">1.2.2.3. 系统架构图</a><br>
      </ul>
    </details>
    </ul>
  </details>
    <a href="#13-运行与测试">1.3. 运行与测试</a><br>
    <a href="#14-总结">1.4. 总结</a><br>
    <a href="#15-安装mysql8-in-docker-in-centos8">1.5. 安装MySQL8 in Docker in CentOS8</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#2-分布式文件存储fastdfs">2. 分布式文件存储FastDFS</a></summary>
  <ul>
  <details open>
    <summary><a href="#21-fastdfs">2.1. FastDFS</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#211-fastdfs简介">2.1.1. FastDFS简介</a>    </summary>
      <ul>
        <a href="#2111-fastdfs体系结构">2.1.1.1. FastDFS体系结构</a><br>
        <a href="#2112-上传流程">2.1.1.2. 上传流程</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#212-fastdfs搭建">2.1.2. FastDFS搭建</a>    </summary>
      <ul>
        <a href="#2121-安装fastdfs镜像">2.1.2.1. 安装FastDFS镜像</a><br>
        <a href="#2122-配置nginx">2.1.2.2. 配置Nginx</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#213-文件存储微服务">2.1.3. 文件存储微服务</a>    </summary>
      <ul>
        <a href="#2131-pomxml依赖">2.1.3.1. pom.xml依赖</a><br>
        <a href="#2132-fastdfs配置">2.1.3.2. FastDFS配置</a><br>
        <a href="#2133-applicationyml配置">2.1.3.3. application.yml配置</a><br>
        <a href="#2134-启动类">2.1.3.4. 启动类</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#214-文件上传">2.1.4. 文件上传</a>    </summary>
      <ul>
        <a href="#2141-文件信息封装">2.1.4.1. 文件信息封装</a><br>
        <a href="#2142-文件操作">2.1.4.2. 文件操作</a><br>
        <a href="#2143-文件上传">2.1.4.3. 文件上传</a><br>
      </ul>
    </details>
    </ul>
  </details>
    <a href="#22-运行与测试">2.2. 运行与测试</a><br>
    <a href="#23-分布式文件存储fastdfs-fastdfs在docker上的安装步骤和细节说明">2.3. 分布式文件存储FastDFS-FastDFS在Docker上的安装步骤和细节说明</a><br>
    <a href="#24-install-lua-in-centos8（在centos8上面安装lua语言环境）">2.4. Install Lua in CentOS8（在CentOS8上面安装Lua语言环境）</a><br>
    <a href="#25-install-openresty-in-centos8（在centos8上面安装openresty）">2.5. Install OpenResty in CentOS8（在CentOS8上面安装OpenResty）</a><br>
    <a href="#26-install-redis-in-docker（在docker上面安装redis）">2.6. Install Redis in Docker（在Docker上面安装Redis）</a><br>
    <a href="#27-test-openresty-connect-mysql8-via-lua（测试通过lua语言openresty连接mysql8）">2.7. Test Openresty connect MySQL8 via Lua（测试通过Lua语言Openresty连接MySQL8）</a><br>
    <a href="#28-nginx-current-limit（nginx限流）">2.8. Nginx Current Limit（Nginx限流）</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#3-elasticsearch">3. Elasticsearch</a></summary>
  <ul>
    <a href="#31-elasticsearch-安装">3.1. Elasticsearch 安装</a><br>
    <a href="#32-ik分词器安装">3.2. IK分词器安装</a><br>
  <details open>
    <summary><a href="#33-kibana">3.3. Kibana</a>  </summary>
    <ul>
      <a href="#331-kibana下载安装">3.3.1. Kibana下载安装</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#34-运行与测试">3.4. 运行与测试</a>  </summary>
    <ul>
      <a href="#341-准备工作">3.4.1. 准备工作</a><br>
      <a href="#342-运行">3.4.2. 运行</a><br>
      <a href="#343-测试">3.4.3. 测试</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#4-微服务网关和jwt令牌">4. 微服务网关和Jwt令牌</a></summary>
  <ul>
  <details open>
    <summary><a href="#41-微服务网关">4.1. 微服务网关</a>  </summary>
    <ul>
      <a href="#411-微服务网关的概述">4.1.1. 微服务网关的概述</a><br>
      <a href="#412-微服务网关技术">4.1.2. 微服务网关技术</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#42-网关系统使用">4.2. 网关系统使用</a>  </summary>
    <ul>
      <a href="#421-需求分析">4.2.1. 需求分析</a><br>
      <a href="#422-跨域配置">4.2.2. 跨域配置</a><br>
      <a href="#423-网关过滤配置">4.2.3. 网关过滤配置</a><br>
    <details open>
      <summary><a href="#424-网关限流">4.2.4. 网关限流</a>    </summary>
      <ul>
        <a href="#4241-思路分析">4.2.4.1. 思路分析</a><br>
        <a href="#4242-令牌桶算法">4.2.4.2. 令牌桶算法</a><br>
        <a href="#4243-使用令牌桶进行请求次数限流">4.2.4.3. 使用令牌桶进行请求次数限流</a><br>
      </ul>
    </details>
    </ul>
  </details>
    <a href="#43-用户登录">4.3. 用户登录</a><br>
  <details open>
    <summary><a href="#44-jwt讲解">4.4. JWT讲解</a>  </summary>
    <ul>
      <a href="#441-需求分析">4.4.1. 需求分析</a><br>
      <a href="#442-什么是jwt">4.4.2. 什么是JWT</a><br>
      <a href="#443-jwt的构成">4.4.3. JWT的构成</a><br>
    <details open>
      <summary><a href="#444-jjwt的介绍和使用">4.4.4. JJWT的介绍和使用</a>    </summary>
      <ul>
        <a href="#4441-创建token">4.4.4.1. 创建TOKEN</a><br>
        <a href="#4442-token解析">4.4.4.2. TOKEN解析</a><br>
      <details open>
        <summary><a href="#4443-设置过期时间">4.4.4.3. 设置过期时间</a>      </summary>
        <ul>
          <a href="#44431-解析token">4.4.4.3.1. 解析TOKEN</a><br>
        </ul>
      </details>
        <a href="#4444-自定义claims">4.4.4.4. 自定义claims</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#445-鉴权处理">4.4.5. 鉴权处理</a>    </summary>
      <ul>
        <a href="#4451-思路分析">4.4.5.1. 思路分析</a><br>
        <a href="#4452-用户登录签发token">4.4.5.2. 用户登录签发TOKEN</a><br>
        <a href="#4453-网关过滤器拦截请求处理">4.4.5.3. 网关过滤器拦截请求处理</a><br>
        <a href="#4454-自定义全局过滤器">4.4.5.4. 自定义全局过滤器</a><br>
        <a href="#4455-配置过滤规则">4.4.5.5. 配置过滤规则</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#446-会话保持">4.4.6. 会话保持</a>    </summary>
      <ul>
        <a href="#4461-登录封装cookie">4.4.6.1. 登录封装Cookie</a><br>
        <a href="#4462-过滤器获取令牌数据">4.4.6.2. 过滤器获取令牌数据</a><br>
      </ul>
    </details>
    </ul>
  </details>
  <details open>
    <summary><a href="#45-运行与测试">4.5. 运行与测试</a>  </summary>
    <ul>
      <a href="#451-准备工作">4.5.1. 准备工作</a><br>
      <a href="#452-运行">4.5.2. 运行</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#5-spring-security-oauth2-jwt">5. Spring Security Oauth2 JWT</a></summary>
  <ul>
    <a href="#51-学习目标">5.1. 学习目标</a><br>
  <details open>
    <summary><a href="#52-认证技术方案">5.2. 认证技术方案</a>  </summary>
    <ul>
      <a href="#521-单点登录技术方案">5.2.1. 单点登录技术方案</a><br>
    <details open>
      <summary><a href="#522-oauth2认证">5.2.2. Oauth2认证</a>    </summary>
      <ul>
        <a href="#5221-oauth2认证流程">5.2.2.1. Oauth2认证流程</a><br>
        <a href="#5222-oauth2在项目的应用">5.2.2.2. Oauth2在项目的应用</a><br>
      </ul>
    </details>
      <a href="#523-spring-security-oauth2认证解决方案">5.2.3. Spring security Oauth2认证解决方案</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#53-security-oauth20入门">5.3. Security Oauth2.0入门</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#531-oauth2授权模式">5.3.1. Oauth2授权模式</a>    </summary>
      <ul>
        <a href="#5311-oauth2授权模式">5.3.1.1. Oauth2授权模式</a><br>
      </ul>
    </details>
    </ul>
  </details>
  <details open>
    <summary><a href="#54-运行与测试">5.4. 运行与测试</a>  </summary>
    <ul>
      <a href="#541-运行">5.4.1. 运行</a><br>
    <details open>
      <summary><a href="#542-测试授权码模式">5.4.2. 测试授权码模式</a>    </summary>
      <ul>
        <a href="#5421-申请授权码">5.4.2.1. 申请授权码</a><br>
        <a href="#5422-申请令牌">5.4.2.2. 申请令牌</a><br>
        <a href="#5423-校验令牌">5.4.2.3. 校验令牌</a><br>
        <a href="#5424-刷新令牌">5.4.2.4. 刷新令牌</a><br>
      </ul>
    </details>
      <a href="#543-测试密码模式">5.4.3. 测试密码模式</a><br>
    <details open>
      <summary><a href="#544-生成私钥公钥">5.4.4. 生成私钥公钥</a>    </summary>
      <ul>
        <a href="#5441-生成私钥">5.4.4.1. 生成私钥</a><br>
        <a href="#5442-opensl-从私钥导出不带签名的公钥">5.4.4.2. opensl 从私钥导出不带签名的公钥</a><br>
        <a href="#5443-同时测试生成和解析令牌">5.4.4.3. 同时测试生成和解析令牌</a><br>
      </ul>
    </details>
      <a href="#545-用户密码登录测试">5.4.5. 用户密码登录测试</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#6-购物车">6. 购物车</a></summary>
  <ul>
    <a href="#61-学习目标">6.1. 学习目标</a><br>
  <details open>
    <summary><a href="#62-运行与测试">6.2. 运行与测试</a>  </summary>
    <ul>
      <a href="#621-运行">6.2.1. 运行</a><br>
      <a href="#622-测试">6.2.2. 测试</a><br>
      <a href="#623-测试授权认证设置微服务网关（gateway）">6.2.3. 测试授权认证设置微服务网关（GateWay）</a><br>
      <a href="#624-测试数据库加载客户端id和密码是否成功">6.2.4. 测试数据库加载客户端ID和密码是否成功</a><br>
      <a href="#625-测试数据库加载用户数据库是否成功">6.2.5. 测试数据库加载用户数据库是否成功</a><br>
      <a href="#626-测试订单购物车微服务">6.2.6. 测试订单购物车微服务</a><br>
      <a href="#627-测试购物车添加，并存储到redis">6.2.7. 测试购物车添加，并存储到redis</a><br>
      <a href="#628-测试购物车查询，在redis中">6.2.8. 测试购物车查询，在redis中</a><br>
      <a href="#629-到底allowbeandefinitionoverriding应该设置true还是false？">6.2.9. 到底allowBeanDefinitionOverriding应该设置true还是false？</a><br>
      <a href="#6210-解决令牌校验的问题">6.2.10. 解决令牌校验的问题</a><br>
      <a href="#6211-实现微服务之间认证（oauth和user模块的认证访问）">6.2.11. 实现微服务之间认证（OAuth和User模块的认证访问）</a><br>
      <a href="#6212-警用url灵活的验证，后面再实现">6.2.12. 警用URL灵活的验证，后面再实现</a><br>
      <a href="#6213-微服务之间的认证（order微服务访问goods微服务）">6.2.13. 微服务之间的认证（Order微服务访问Goods微服务）</a><br>
      <a href="#6214-熔断模式开启">6.2.14. 熔断模式开启</a><br>
      <a href="#6215-微服务中获取用户令牌信息并解析获取用户信息">6.2.15. 微服务中获取用户令牌信息并解析获取用户信息</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#7-微信支付">7. 微信支付</a></summary>
  <ul>
    <a href="#71-运行">7.1. 运行</a><br>
    <a href="#72-测试生成微信支付连接">7.2. 测试生成微信支付连接</a><br>
    <a href="#73-测试刚才创建的订单的支付状态">7.3. 测试刚才创建的订单的支付状态</a><br>
    <a href="#74-安装rabbitmq（docker）">7.4. 安装RabbitMQ（Docker）</a><br>
    <a href="#75-测试支付好后，订单状态通过rabbit自动更新订单状态">7.5. 测试支付好后，订单状态通过Rabbit自动更新订单状态</a><br>
    <a href="#76-测试超时订单处理">7.6. 测试超时订单处理</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#8-秒杀">8. 秒杀</a></summary>
  <ul>
  <details open>
    <summary><a href="#81-秒杀业务分析">8.1. 秒杀业务分析</a>  </summary>
    <ul>
      <a href="#811-需求分析">8.1.1. 需求分析</a><br>
      <a href="#812-秒杀需求分析">8.1.2. 秒杀需求分析</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#82-运行与测试">8.2. 运行与测试</a>  </summary>
    <ul>
      <a href="#821-创建秒杀微服务">8.2.1. 创建秒杀微服务</a><br>
      <a href="#822-测试秒杀商品查询并存入到redis中">8.2.2. 测试秒杀商品查询并存入到Redis中</a><br>
      <a href="#823-测试查询秒杀商品频道列表中的商品列表，从redis中查询">8.2.3. 测试查询秒杀商品频道列表中的商品列表，从Redis中查询</a><br>
      <a href="#824-测试秒杀页商品查询">8.2.4. 测试秒杀页商品查询</a><br>
    <details open>
      <summary><a href="#825-测试多线程排队抢单，队列削峰需要看第二遍">8.2.5. 测试多线程排队抢单，队列削峰(需要看第二遍)</a>    </summary>
      <ul>
        <a href="#8251-异步实现">8.2.5.1. 异步实现</a><br>
        <a href="#8252-测试">8.2.5.2. 测试</a><br>
      </ul>
    </details>
      <a href="#826-测试当前用户抢单状态查询">8.2.6. 测试当前用户抢单状态查询</a><br>
      <a href="#827-防止秒杀重复排队需要看第二遍">8.2.7. 防止秒杀重复排队(需要看第二遍)</a><br>
    <details open>
      <summary><a href="#828-测试抢单超卖和数据不精确的问题需要看第二遍">8.2.8. 测试抢单超卖和数据不精确的问题(需要看第二遍)</a>    </summary>
      <ul>
        <a href="#8281-问题描述">8.2.8.1. 问题描述</a><br>
        <a href="#8282-思路分析">8.2.8.2. 思路分析</a><br>
        <a href="#8283-测试">8.2.8.3. 测试</a><br>
      </ul>
    </details>
      <a href="#829-解决数据不精确的问题">8.2.9. 解决数据不精确的问题</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#9-分布式事务介绍">9. 分布式事务介绍</a></summary>
  <ul>
    <a href="#91-什么是事务">9.1. 什么是事务</a><br>
    <a href="#92-本地事务">9.2. 本地事务</a><br>
    <a href="#93-什么是分布式事务">9.3. 什么是分布式事务</a><br>
  <details open>
    <summary><a href="#94-分布式事务应用架构">9.4. 分布式事务应用架构</a>  </summary>
    <ul>
      <a href="#941-单一服务分布式事务">9.4.1. 单一服务分布式事务</a><br>
      <a href="#942-多服务分布式事务">9.4.2. 多服务分布式事务</a><br>
      <a href="#943-多服务多数据源分布式事务">9.4.3. 多服务多数据源分布式事务</a><br>
      <a href="#944-15-cap定理">9.4.4. 1.5 CAP定理</a><br>
    </ul>
  </details>
    <a href="#95-分布式事务解决方案">9.5. 分布式事务解决方案</a><br>
    <a href="#96-基于xa协议的两阶段提交2pc">9.6. 基于XA协议的两阶段提交(2PC)</a><br>
    <a href="#97-补偿（代码补偿）事务（tcc）-3pc">9.7. 补偿（代码补偿）事务（TCC）-3PC</a><br>
    <a href="#98-本地消息表（异步确保）-事务的最终一致性">9.8. 本地消息表（异步确保）-事务的最终一致性</a><br>
    <a href="#99-mq-事务消息-最终一致性">9.9. MQ 事务消息-最终一致性</a><br>
  <details open>
    <summary><a href="#910-seata-2pc-改进">9.10. Seata-2PC->改进</a>  </summary>
    <ul>
      <a href="#9101-seata介绍">9.10.1. Seata介绍</a><br>
      <a href="#9102-at模式">9.10.2. AT模式</a><br>
      <a href="#9103-tcc模式">9.10.3. TCC模式</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#10-集群高可用">10. 集群高可用</a></summary>
  <ul>
    <a href="#101-学习目标">10.1. 学习目标</a><br>
  <details open>
    <summary><a href="#102-集群概述">10.2. 集群概述</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#1021-什么是集群">10.2.1. 什么是集群</a>    </summary>
      <ul>
        <a href="#10211-集群概念">10.2.1.1. 集群概念</a><br>
        <a href="#10212-集群的特点">10.2.1.2. 集群的特点</a><br>
        <a href="#10213-集群的两大能力">10.2.1.3. 集群的两大能力</a><br>
      </ul>
    </details>
      <a href="#1022-集群与分布式的区别">10.2.2. 集群与分布式的区别</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#103-eureka集群">10.3. Eureka集群</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#1031-eureka简介">10.3.1. Eureka简介</a>    </summary>
      <ul>
        <a href="#10311-什么是eureka">10.3.1.1. 什么是Eureka</a><br>
        <a href="#10312-eureka的架构">10.3.1.2. Eureka的架构</a><br>
      </ul>
    </details>
    </ul>
  </details>
  <details open>
    <summary><a href="#104-redis-cluster">10.4. Redis Cluster</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#1041-redis-cluster简介">10.4.1. Redis-Cluster简介</a>    </summary>
      <ul>
      <details open>
        <summary><a href="#10411-什么是redis-cluster">10.4.1.1. 什么是Redis-Cluster</a>      </summary>
        <ul>
          <a href="#104111-分布存储机制-槽">10.4.1.1.1. 分布存储机制-槽</a><br>
          <a href="#104112-容错机制-投票">10.4.1.1.2. 容错机制-投票</a><br>
        </ul>
      </details>
      <details open>
        <summary><a href="#10412-搭建redis-cluster">10.4.1.2. 搭建Redis-Cluster</a>      </summary>
        <ul>
          <a href="#104121-搭建要求">10.4.1.2.1. 搭建要求</a><br>
          <a href="#104122-准备工作">10.4.1.2.2. 准备工作</a><br>
          <a href="#104123-配置集群">10.4.1.2.3. 配置集群</a><br>
        </ul>
      </details>
      <details open>
        <summary><a href="#10413-连接redis-cluster">10.4.1.3. 连接Redis-Cluster</a>      </summary>
        <ul>
          <a href="#104131-客户端工具连接">10.4.1.3.1. 客户端工具连接</a><br>
          <a href="#104132-springboot连接redis集群">10.4.1.3.2. springboot连接redis集群</a><br>
        </ul>
      </details>
      <details open>
        <summary><a href="#10414-redis的持久化">10.4.1.4. Redis的持久化</a>      </summary>
        <ul>
          <a href="#104141-redis的持久化介绍">10.4.1.4.1. redis的持久化介绍</a><br>
          <a href="#104142-开启rdb">10.4.1.4.2. 开启RDB</a><br>
          <a href="#104143-开启aof">10.4.1.4.3. 开启AOF</a><br>
          <a href="#104144-模式的抉择应用场景介绍">10.4.1.4.4. 模式的抉择应用场景介绍</a><br>
        </ul>
      </details>
      <details open>
        <summary><a href="#10415-redis哨兵模式">10.4.1.5. Redis哨兵模式</a>      </summary>
        <ul>
          <a href="#104151-redis的主从复制实现高可用">10.4.1.5.1. Redis的主从复制实现高可用</a><br>
        </ul>
      </details>
      <details open>
        <summary><a href="#10416-redis缓存击穿问题解决">10.4.1.6. redis缓存击穿问题解决</a>      </summary>
        <ul>
          <a href="#104161-什么是缓存击穿">10.4.1.6.1. 什么是缓存击穿</a><br>
          <a href="#104162-缓存击穿的解决方案">10.4.1.6.2. 缓存击穿的解决方案</a><br>
        </ul>
      </details>
      <details open>
        <summary><a href="#10417-redis缓存雪崩问题解决（作业）">10.4.1.7. Redis缓存雪崩问题解决（作业）</a>      </summary>
        <ul>
          <a href="#104171-什么是缓存雪崩">10.4.1.7.1. 什么是缓存雪崩</a><br>
          <a href="#104172-如何解决">10.4.1.7.2. 如何解决</a><br>
          <a href="#104173-二级缓存解决雪崩的案例">10.4.1.7.3. 二级缓存解决雪崩的案例</a><br>
        </ul>
      </details>
      </ul>
    </details>
    </ul>
  </details>
  </ul>
</details>


<h1>通用准备工作</h1>

[VMWare Fusion安装CentOS 8，配置静态网关)](./../../Server/Linux/Linux.md)

[CentOS8 Docker 安装](./../../Server/Docker/Docker.md)

# 1. 框架搭建

## 1.1. 走进电商

### 1.1.1. 电商行业分析

近年来，世界经济正向数字化转型，大力发展数字经济成为全球共识。党的十九大报告明确提出要建设“数字中国”“网络强国”，我国数字经济发展进入新阶段，市场规模位居全球第二，数字经济与实体经济深度融合，有力促进了供给侧结构性改革。电子商务是数字经济的重要组成部分，是数字经济最活跃、最集中的表现形式之一。2017年，在政府和市场共同推动下，我国电子商务发展更加注重效率、质量和创新，取得了一系列新的进展，在壮大数字经济、共建“一带一路”、助力乡村振兴、带动创新创业、促进经济转型升级等诸多方面发挥了重要作用，成为我国经济增长的新动力。
2017年，我国电子商务交易规模继续扩大，并保持高速增长态势。国家统计局数据显示，2017年全国电子商务交易额达29.16万亿元，同比增长11.7%；网上零售额7.18万亿元，同比增长32.2%。我国电子商务优势进一步扩大，网络零售规模全球最大、产业创新活力世界领先。数据显示，截止2017年底，全国网络购物用户规模达5.33亿，同比增长14.3%；非银行支付机构发生网络支付金额达143.26万亿元，同比增长44.32%；全国快递服务企业业务量累计完成400.6亿件，同比增长28%；电子商务直接从业人员和间接带动就业达4250万人。

![1559552887142](./images/mall-1.png)



2018天猫全天成交额记录

![1559553569582](./images/mall-2.png)



### 1.1.2. 电商系统技术特点

-  技术新

- 技术范围广

- 分布式

- 高并发、集群、负载均衡、高可用

- 海量数据

- 业务复杂

- 系统安全



### 1.1.3. 主要电商模式

**B2B**

```
B2B （ Business to Business）是指进行电子商务交易的供需双方都是商家（或企业、公司），她（他）们使用了互联网的技术或各种商务网络平台，完成商务交易的过程。电子商务是现代 B2B marketing的一种具体主要的表现形式。

案例：阿里巴巴、慧聪网
```



**C2C**

```
C2C即 Customer（Consumer） to Customer（Consumer），意思就是消费者个人间的电子商务行为。比如一个消费者有一台电脑，通过网络进行交易，把它出售给另外一个消费者，此种交易类型就称为C2C电子商务。

案例：淘宝、易趣、瓜子二手车
```



**B2C**

```
B2C是Business-to-Customer的缩写，而其中文简称为“商对客”。“商对客”是电子商务的一种模式，也就是通常说的直接面向消费者销售产品和服务商业零售模式。这种形式的电子商务一般以网络零售业为主，主要借助于互联网开展在线销售活动。B2C即企业通过互联网为消费者提供一个新型的购物环境——网上商店，消费者通过网络在网上购物、网上支付等消费行为。

案例：唯品会、乐蜂网
```



**C2B**

```
C2B（Consumer to Business，即消费者到企业），是互联网经济时代新的商业模式。这一模式改变了原有生产者（企业和机构）和消费者的关系，是一种消费者贡献价值（Create Value）， 企业和机构消费价值（Consume Value）。

C2B模式和我们熟知的供需模式（DSM, Demand SupplyModel）恰恰相反，真正的C2B 应该先有消费者需求产生而后有企业生产，即先有消费者提出需求，后有生产企业按需求组织生产。通常情况为消费者根据自身需求定制产品和价格，或主动参与产品设计、生产和定价，产品、价格等彰显消费者的个性化需求，生产企业进行定制化生产。

案例：海尔商城、 尚品宅配
```



**O2O**

```
O2O即Online To Offline（在线离线/线上到线下），是指将线下的商务机会与互联网结合，让互联网成为线下交易的平台，这个概念最早来源于美国。O2O的概念非常广泛，既可涉及到线上，又可涉及到线下,可以通称为O2O。主流商业管理课程均对O2O这种新型的商业模式有所介绍及关注。

案例：美团、饿了吗
```



**F2C**

```
F2C指的是Factory to customer，即从厂商到消费者的电子商务模式。
```



**B2B2C**

```
B2B2C是一种电子商务类型的网络购物商业模式，B是BUSINESS的简称，C是CUSTOMER的简称，第一个B指的是商品或服务的供应商，第二个B指的是从事电子商务的企业，C则是表示消费者。

案例：京东商城、天猫商城
注：我们《畅购电商系统开发》课程采用B2C模式，之后的项目实战《品优购电商系统开发实战》采用B2B2C模式。
```






## 1.2. 畅购-需求分析与系统设计

### 1.2.1. 需求分析

网站前台静态原型演示，打开`资料\页面\前台\project-changgou-portal-fis3-master`，首页`index.html`

![1559111851979](./images/mall-3.png)



网站管理后台静态原型演示:http://czpm.itcast.cn/青橙后台/#g=1&p=后台首页

![1559112046165](./images/mall-4.png)

打开`资料\页面\后台\project-changgou-cmm-fis3-master\pages`,首页`all-medical-main.html`

![1559111970498](./images/mall-5.png)



### 1.2.2. 系统设计

畅购商城属于B2C电商模式，运营商将自己的产品发布到网站上，会员注册后，在网站上将商品添加到购物车，并且下单，完成线上支付，用户还可以参与秒杀抢购。



#### 1.2.2.1. 前后端分离

网站后台的部分采用前后端分离方式。

以前的JavaWeb项目大多数都是java程序员又当爹又当妈，又搞前端，又搞后端。随着时代的发展，渐渐的许多大中小公司开始把前后端的界限分的越来越明确，前端工程师只管前端的事情，后端工程师只管后端的事情。正所谓术业有专攻，一个人如果什么都会，那么他毕竟什么都不精。

**对于后端java工程师：**

把精力放在设计模式，spring+springmvc，linux，mysql事务隔离与锁机制，mongodb，http/tcp，多线程，分布式架构，弹性计算架构，微服务架构，java性能优化，以及相关的项目管理等等。

**对于前端工程师：**

把精力放在html5，css3，vuejs，webpack，nodejs，Google V8引擎，javascript多线程，模块化，面向切面编程，设计模式，浏览器兼容性，性能优化等等。

![1559553886871](./images/mall-6.png)

我们在本课程中提供与项目课程配套的管理后台的前端代码，但是不讲解前端的内容。这样我们会将更多的精力放在**后端代码**的开发上！





#### 1.2.2.2. 技术架构

![1560087134452](./images/mall-7.png)



#### 1.2.2.3. 系统架构图

![1560090475333](./images/mall-8.png)

## 1.3. 运行与测试

[通用准备工作](#通用准备工作)

[在Docker上安装MySQL8](#在Docker上安装MySQL8)

第一天主要是框架搭建，建立分布式项目，使用的是自己钻研的自动接口生成器实现商品的增删改查，下面操作可以操作成果：

**运行**

运行[EurekaServerApplication.java](./mall/mall-parent/mall-eureka/src/main/java/com/mall/EurekaServerApplication.java)，访问 [http://127.0.0.1:7001/](http://127.0.0.1:7001/)

运行[GoodsApplication.java](./mall/mall-parent/mall-service/mall-service-goods/src/main/java/com/mall/goods/GoodsApplication.java)

**测试**

访问Goods微服务的Swagger2[在线API文档](http://127.0.0.1:18081/swagger-ui.html)，测试Goods的增删改查。

## 1.4. 总结

1. Eureka微服务
2. 商品微服务，Eureka微服务，数据访问工程mall-common-db，公共模块mall-common的搭建
3. 商品的增删改查，还包括如下：
   通用批量添加（主要用于多对多的关系表）
   一（主表）对多（附表）关联查询
   多对一关联查询
   一对多关联查询
   一对多-级联新增
   高级精确查找
   高级模糊查找
4. 将Swagger配置类放在公共模块当中，实现Goods微服务的在线API文档
5. 在Docker上安装MySQL8并远程连接，请参考"1.框架搭建-在Docker上安装MySQL8并远程连接.txt"

## 1.5. 安装MySQL8 in Docker in CentOS8

<h6>在Docker上安装MySQL8</h6>

```
Install MySQL8 in Docker
1.下载Mysql的Docker镜像
    docker search mysql
    docker pull mysql:8.0.20(当时搜索的最新版本)
2.运行镜像，设置root账号初始密码（123456），映射本地宿主机端口3306到Docker端口3306。测试过程没有挂载本地数据盘：
    docker run -p 3307:3306 --name MYSQL8.0.20 -e MYSQL_ROOT_PASSWORD=root -d mysql:8.0.20
    解释下这条命令的参数：
        -p 3306:3306：将容器内的3306端口映射到实体机3306端口
        --name MYSQL8.0.20：给这个容器取一个容器记住的名字
        -e MYSQL_ROOT_PASSWORD=123456：docker的MySQL默认的root密码是随机的，这是改一下默认的root用户密码
        -d mysql:8.0.20：在后台运行mysql:8.0.20镜像产生的容器
3.新装了mysql8.0后再用navicat链接就会报2059的错误。上网查了发现是8.0之后mysql更改了密码的加密规则，只要在命令窗口把加密方法改回去即可。
    首先使用以下命令进入MySQL的docker容器
        docker exec -it MYSQL8.0.20 bash
    然后登录MySQL
        mysql -uroot -proot
    然后运行以下SQL即可
        GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION;
        alter user 'root'@'%' identified by 'root' password expire never;
        alter user 'root'@'%' identified with mysql_native_password by 'root';
        flush privileges;
4.退出MySQL，退出容器
    退出MySQL
    quit
    退出容器
    exit
5.重启MYSQL8.0.20
    docker restart MYSQL8.0.20
```

# 2. 分布式文件存储FastDFS

## 2.1. FastDFS

### 2.1.1. FastDFS简介

#### 2.1.1.1. FastDFS体系结构 

FastDFS是一个开源的轻量级[分布式文件系统](https://baike.baidu.com/item/%E5%88%86%E5%B8%83%E5%BC%8F%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F/1250388)，它对文件进行管理，功能包括：文件存储、文件同步、文件访问（文件上传、文件下载）等，解决了大容量存储和负载均衡的问题。特别适合以文件为载体的在线服务，如相册网站、视频网站等等。

FastDFS为互联网量身定制，充分考虑了冗余备份、负载均衡、线性扩容等机制，并注重高可用、高性能等指标，使用FastDFS很容易搭建一套高性能的文件服务器集群提供文件上传、下载等服务。

FastDFS 架构包括 Tracker server 和 Storage server。客户端请求 Tracker server 进行文件上传、下载，通过Tracker server 调度最终由 Storage server 完成文件上传和下载。

Tracker server 作用是负载均衡和调度，通过 Tracker server 在文件上传时可以根据一些策略找到Storage server 提供文件上传服务。可以将 tracker 称为追踪服务器或调度服务器。Storage server 作用是文件存储，客户端上传的文件最终存储在 Storage 服务器上，Storageserver 没有实现自己的文件系统而是利用操作系统的文件系统来管理文件。可以将storage称为存储服务器。

![1559117928459](./images/mall-9.png)



#### 2.1.1.2. 上传流程

![1559117994668](./images/mall-10.png)

客户端上传文件后存储服务器将文件 ID 返回给客户端，此文件 ID 用于以后访问该文件的索引信息。文件索引信息包括：组名，虚拟磁盘路径，数据两级目录，文件名。

![1559118013272](./images/mall-11.png)

**组名**：文件上传后所在的 storage 组名称，在文件上传成功后有storage 服务器返回，需要客户端自行保存。

**虚拟磁盘路径**：storage 配置的虚拟路径，与磁盘选项store_path*对应。如果配置了

store_path0 则是 M00，如果配置了 store_path1 则是 M01，以此类推。

**数据两级目录**：storage 服务器在每个虚拟磁盘路径下创建的两级目录，用于存储数据

文件。

**文件名**：与文件上传时不同。是由存储服务器根据特定信息生成，文件名包含：源存储

服务器 IP 地址、文件创建时间戳、文件大小、随机数和文件拓展名等信息。



### 2.1.2. FastDFS搭建

#### 2.1.2.1. 安装FastDFS镜像

我们使用Docker搭建FastDFS的开发环境,虚拟机中已经下载了fastdfs的镜像，可以通过`docker images`查看，如下图：

![1559180866611](./images/mall-12.png)

拉取镜像(已经下载了该镜像，大家无需下载了)

```properties
docker pull morunchang/fastdfs
```

运行tracker

```properties
docker run -d --name tracker --net=host morunchang/fastdfs sh tracker.sh
```

运行storage

```properties
docker run -d --name storage --net=host -e TRACKER_IP=192.168.211.132:22122 -e GROUP_NAME=group1 morunchang/fastdfs sh storage.sh
```

- 使用的网络模式是–net=host, 192.168.211.132是宿主机的IP
- group1是组名，即storage的组  
- 如果想要增加新的storage服务器，再次运行该命令，注意更换 新组名



#### 2.1.2.2. 配置Nginx

Nginx在这里主要提供对FastDFS图片访问的支持，Docker容器中已经集成了Nginx，我们需要修改nginx的配置,进入storage的容器内部，修改nginx.conf

```properties
docker exec -it storage  /bin/bash
```

进入后

```properties
vi /etc/nginx/conf/nginx.conf
```

添加以下内容

![1564792264719](./images/mall-13.png)

上图配置如下：

```properties
location ~ /M00 {
     root /data/fast_data/data;
     ngx_fastdfs_module;
}
```



禁止缓存：

```
add_header Cache-Control no-store;
```



退出容器

```
exit
```



重启storage容器

```properties
docker restart storage
```



查看启动容器`docker ps`

```properties
9f2391f73d97 morunchang/fastdfs "sh storage.sh" 12 minutes ago Up 12 seconds storage
e22a3c7f95ea morunchang/fastdfs "sh tracker.sh" 13 minutes ago Up 13 minutes tracker
```



开启启动设置

```properties
docker update --restart=always tracker
docker update --restart=always storage
```



### 2.1.3. 文件存储微服务

创建文件管理微服务changgou-service-file，该工程主要用于实现文件上传以及文件删除等功能。



#### 2.1.3.1. pom.xml依赖

修改pom.xml，引入依赖

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>changgou-service</artifactId>
        <groupId>com.changgou</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>
    <artifactId>changgou-service-file</artifactId>
    <description>文件上传工程</description>

    <!--依赖包-->
    <dependencies>
        <dependency>
            <groupId>net.oschina.zcx7878</groupId>
            <artifactId>fastdfs-client-java</artifactId>
            <version>1.27.0.0</version>
        </dependency>
        <dependency>
            <groupId>com.changgou</groupId>
            <artifactId>changgou-common</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>
    </dependencies>
</project>
```



#### 2.1.3.2. FastDFS配置

在resources文件夹下创建fasfDFS的配置文件fdfs_client.conf

```properties
connect_timeout=60
network_timeout=60
charset=UTF-8
http.tracker_http_port=8080
tracker_server=192.168.211.132:22122
```

connect_timeout：连接超时时间，单位为秒。

network_timeout：通信超时时间，单位为秒。发送或接收数据时。假设在超时时间后还不能发送或接收数据，则本次网络通信失败

charset： 字符集

http.tracker_http_port  ：.tracker的http端口

tracker_server： tracker服务器IP和端口设置



#### 2.1.3.3. application.yml配置

在resources文件夹下创建application.yml

```yaml
spring:
  servlet:
    multipart:
      max-file-size: 10MB
      max-request-size: 10MB
  application:
    name: file
server:
  port: 18082
eureka:
  client:
    service-url:
      defaultZone: http://127.0.0.1:7001/eureka
  instance:
    prefer-ip-address: true
feign:
  hystrix:
    enabled: true
```

max-file-size是单个文件大小，max-request-size是设置总上传的数据大小



#### 2.1.3.4. 启动类

创建com.changgou包，创建启动类FileApplication

```java
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@EnableEurekaClient
public class FileApplication {

    public static void main(String[] args) {
        SpringApplication.run(FileApplication.class);
    }
}
```

这里禁止了DataSource的加载创建。





### 2.1.4. 文件上传



#### 2.1.4.1. 文件信息封装

文件上传一般都有文件的名字、文件的内容、文件的扩展名、文件的md5值、文件的作者等相关属性，我们可以创建一个对象封装这些属性，代码如下：

创建`com.changgou.file.FastDFSFile`代码如下：

```java
public class FastDFSFile implements Serializable {

    //文件名字
    private String name;
    //文件内容
    private byte[] content;
    //文件扩展名
    private String ext;
    //文件MD5摘要值
    private String md5;
    //文件创建作者
    private String author;

    public FastDFSFile(String name, byte[] content, String ext, String md5, String author) {
        this.name = name;
        this.content = content;
        this.ext = ext;
        this.md5 = md5;
        this.author = author;
    }

    public FastDFSFile(String name, byte[] content, String ext) {
        this.name = name;
        this.content = content;
        this.ext = ext;
    }

    public FastDFSFile() {
    }

    //..get..set..toString
}
```



(可选)测试文件相关操作:

```java
package com.changgou.file.test;

import org.csource.fastdfs.*;
import org.junit.Test;

import java.io.*;
import java.net.InetSocketAddress;

/**
 * 描述
 *
 * @author 三国的包子
 * @version 1.0
 * @package PACKAGE_NAME *
 * @since 1.0
 */

public class FastdfsClientTest {

    /**
     * 文件上传
     *
     * @throws Exception
     */
    @Test
    public void upload() throws Exception {

        //加载全局的配置文件
        ClientGlobal.init("C:\\Users\\Administrator\\IdeaProjects\\beike\\changgou\\changgou-service\\changgou-service-file\\src\\main\\resources\\fdfs_client.conf");

        //创建TrackerClient客户端对象
        TrackerClient trackerClient = new TrackerClient();
        //通过TrackerClient对象获取TrackerServer信息
        TrackerServer trackerServer = trackerClient.getConnection();
        //获取StorageClient对象
        StorageClient storageClient = new StorageClient(trackerServer, null);

        //执行文件上传
        String[] jpgs = storageClient.upload_file("C:\\Users\\Administrator\\Pictures\\5b13cd6cN8e12d4aa.jpg", "jpg", null);

        for (String jpg : jpgs) {

            System.out.println(jpg);
        }

    }

    @Test
    public void delete() throws Exception {

        //加载全局的配置文件
        ClientGlobal.init("C:\\Users\\Administrator\\IdeaProjects\\beike\\changgou\\changgou-service\\changgou-service-file\\src\\main\\resources\\fdfs_client.conf");

        //创建TrackerClient客户端对象
        TrackerClient trackerClient = new TrackerClient();
        //通过TrackerClient对象获取TrackerServer信息
        TrackerServer trackerServer = trackerClient.getConnection();
        //获取StorageClient对象
        StorageClient storageClient = new StorageClient(trackerServer, null);
        //执行文件上传

        int group1 = storageClient.delete_file("group1", "M00/00/00/wKjThF1VEiyAJ0xzAANdC6JX9KA522.jpg");
        System.out.println(group1);
    }

    @Test
    public void download() throws Exception {

        //加载全局的配置文件
        ClientGlobal.init("C:\\Users\\Administrator\\IdeaProjects\\beike\\changgou\\changgou-service\\changgou-service-file\\src\\main\\resources\\fdfs_client.conf");

        //创建TrackerClient客户端对象
        TrackerClient trackerClient = new TrackerClient();
        //通过TrackerClient对象获取TrackerServer信息
        TrackerServer trackerServer = trackerClient.getConnection();
        //获取StorageClient对象
        StorageClient storageClient = new StorageClient(trackerServer, null);
        //执行文件上传
        byte[] bytes = storageClient.download_file("group1", "M00/00/00/wKjThF1VFfKAJRJDAANdC6JX9KA980.jpg");

        File file = new File("D:\\ceshi\\1234.jpg");

        FileOutputStream fileOutputStream = new FileOutputStream(file);

        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);

        bufferedOutputStream.write(bytes);

        bufferedOutputStream.close();

        fileOutputStream.close();
    }

    //获取文件的信息数据
    @Test
    public void getFileInfo() throws Exception {
        //加载全局的配置文件
        ClientGlobal.init("C:\\Users\\Administrator\\IdeaProjects\\beike\\changgou\\changgou-service\\changgou-service-file\\src\\main\\resources\\fdfs_client.conf");

        //创建TrackerClient客户端对象
        TrackerClient trackerClient = new TrackerClient();
        //通过TrackerClient对象获取TrackerServer信息
        TrackerServer trackerServer = trackerClient.getConnection();
        //获取StorageClient对象
        StorageClient storageClient = new StorageClient(trackerServer, null);
        //执行文件上传

        FileInfo group1 = storageClient.get_file_info("group1", "M00/00/00/wKjThF1VFfKAJRJDAANdC6JX9KA980.jpg");

        System.out.println(group1);

    }

    //获取组相关的信息
    @Test
    public void getGroupInfo() throws Exception {
        //加载全局的配置文件
        ClientGlobal.init("C:\\Users\\Administrator\\IdeaProjects\\beike\\changgou\\changgou-service\\changgou-service-file\\src\\main\\resources\\fdfs_client.conf");

        //创建TrackerClient客户端对象
        TrackerClient trackerClient = new TrackerClient();
        //通过TrackerClient对象获取TrackerServer信息
        TrackerServer trackerServer = trackerClient.getConnection();

        StorageServer group1 = trackerClient.getStoreStorage(trackerServer, "group1");
        System.out.println(group1.getStorePathIndex());

        //组对应的服务器的地址  因为有可能有多个服务器.
        ServerInfo[] group1s = trackerClient.getFetchStorages(trackerServer, "group1", "M00/00/00/wKjThF1VFfKAJRJDAANdC6JX9KA980.jpg");
        for (ServerInfo serverInfo : group1s) {
            System.out.println(serverInfo.getIpAddr());
            System.out.println(serverInfo.getPort());
        }
    }

    @Test
    public void getTrackerInfo() throws Exception {
        //加载全局的配置文件
        ClientGlobal.init("C:\\Users\\Administrator\\IdeaProjects\\beike\\changgou\\changgou-service\\changgou-service-file\\src\\main\\resources\\fdfs_client.conf");

        //创建TrackerClient客户端对象
        TrackerClient trackerClient = new TrackerClient();
        //通过TrackerClient对象获取TrackerServer信息
        TrackerServer trackerServer = trackerClient.getConnection();

        InetSocketAddress inetSocketAddress = trackerServer.getInetSocketAddress();
        System.out.println(inetSocketAddress);

    }


}
```





#### 2.1.4.2. 文件操作

创建com.changgou.util.FastDFSClient类,在该类中实现FastDFS信息获取以及文件的相关操作，代码如下：

(1)初始化Tracker信息

在`com.changgou.util.FastDFSClient`类中初始化Tracker信息,在类中添加如下静态块：

```java
/***
 * 初始化tracker信息
 */
static {
    try {
        //获取tracker的配置文件fdfs_client.conf的位置
        String filePath = new ClassPathResource("fdfs_client.conf").getPath();
        //加载tracker配置信息
        ClientGlobal.init(filePath);
    } catch (Exception e) {
        e.printStackTrace();
    }
}
```



(2)文件上传

在类中添加如下方法实现文件上传：

```java
/****
 * 文件上传
 * @param file : 要上传的文件信息封装->FastDFSFile
 * @return String[]
 *          1:文件上传所存储的组名
 *          2:文件存储路径
 */
public static String[] upload(FastDFSFile file){
    //获取文件作者
    NameValuePair[] meta_list = new NameValuePair[1];
    meta_list[0] =new NameValuePair(file.getAuthor());

    /***
     * 文件上传后的返回值
     * uploadResults[0]:文件上传所存储的组名，例如:group1
     * uploadResults[1]:文件存储路径,例如：M00/00/00/wKjThF0DBzaAP23MAAXz2mMp9oM26.jpeg
     */
    String[] uploadResults = null;
    try {
        //创建TrackerClient客户端对象
        TrackerClient trackerClient = new TrackerClient();
        //通过TrackerClient对象获取TrackerServer信息
        TrackerServer trackerServer = trackerClient.getConnection();
        //获取StorageClient对象
        StorageClient storageClient = new StorageClient(trackerServer, null);
        //执行文件上传
        uploadResults = storageClient.upload_file(file.getContent(), file.getExt(), meta_list);
    } catch (Exception e) {
        e.printStackTrace();
    }
    return uploadResults;
}
```

(3)获取文件信息

在类中添加如下方法实现获取文件信息：

```java
/***
 * 获取文件信息
 * @param groupName:组名
 * @param remoteFileName：文件存储完整名
 */
public static FileInfo getFile(String groupName,String remoteFileName){
    try {
        //创建TrackerClient对象
        TrackerClient trackerClient = new TrackerClient();
        //通过TrackerClient获得TrackerServer信息
        TrackerServer trackerServer =trackerClient.getConnection();
        //通过TrackerServer获取StorageClient对象
        StorageClient storageClient = new StorageClient(trackerServer,null);
        //获取文件信息
        return storageClient.get_file_info(groupName,remoteFileName);
    } catch (Exception e) {
        e.printStackTrace();
    }
    return null;
}
```

(4)文件下载

在类中添加如下方法实现文件下载：

```java
/***
 * 文件下载
 * @param groupName:组名
 * @param remoteFileName：文件存储完整名
 * @return
 */
public static InputStream downFile(String groupName,String remoteFileName){
    try {
        //创建TrackerClient对象
        TrackerClient trackerClient = new TrackerClient();
        //通过TrackerClient对象创建TrackerServer
        TrackerServer trackerServer = trackerClient.getConnection();
        //通过TrackerServer创建StorageClient
        StorageClient storageClient = new StorageClient(trackerServer,null);
        //通过StorageClient下载文件
        byte[] fileByte = storageClient.download_file(groupName, remoteFileName);
        //将字节数组转换成字节输入流
        return new ByteArrayInputStream(fileByte);
    } catch (Exception e) {
        e.printStackTrace();
    }
    return null;
}
```

(5)文件删除实现

```java
/***
 * 文件删除实现
 * @param groupName:组名
 * @param remoteFileName：文件存储完整名
 */
public static void deleteFile(String groupName,String remoteFileName){
    try {
        //创建TrackerClient对象
        TrackerClient trackerClient = new TrackerClient();
        //通过TrackerClient获取TrackerServer对象
        TrackerServer trackerServer = trackerClient.getConnection();
        //通过TrackerServer创建StorageClient
        StorageClient storageClient = new StorageClient(trackerServer,null);
        //通过StorageClient删除文件
        storageClient.delete_file(groupName,remoteFileName);
    } catch (Exception e) {
        e.printStackTrace();
    }
}
```

(6)获取组信息

```java
/***
 * 获取组信息
 * @param groupName :组名
 */
public static StorageServer getStorages(String groupName){
    try {
        //创建TrackerClient对象
        TrackerClient trackerClient = new TrackerClient();
        //通过TrackerClient获取TrackerServer对象
        TrackerServer trackerServer = trackerClient.getConnection();
        //通过trackerClient获取Storage组信息
        return trackerClient.getStoreStorage(trackerServer,groupName);
    } catch (Exception e) {
        e.printStackTrace();
    }
    return null;
}
```

(7)根据文件组名和文件存储路径获取Storage服务的IP、端口信息

```java
/***
 * 根据文件组名和文件存储路径获取Storage服务的IP、端口信息
 * @param groupName :组名
 * @param remoteFileName ：文件存储完整名
 */
public static ServerInfo[] getServerInfo(String groupName, String remoteFileName){
    try {
        //创建TrackerClient对象
        TrackerClient trackerClient = new TrackerClient();
        //通过TrackerClient获取TrackerServer对象
        TrackerServer trackerServer = trackerClient.getConnection();
        //获取服务信息
        return trackerClient.getFetchStorages(trackerServer,groupName,remoteFileName);
    } catch (Exception e) {
        e.printStackTrace();
    }
    return null;
}
```

(8)获取Tracker服务地址

```java
/***
 * 获取Tracker服务地址
 */
public static String getTrackerUrl(){
    try {
        //创建TrackerClient对象
        TrackerClient trackerClient = new TrackerClient();
        //通过TrackerClient获取TrackerServer对象
        TrackerServer trackerServer = trackerClient.getConnection();
        //获取Tracker地址
        return "http://"+trackerServer.getInetSocketAddress().getHostString()+":"+ClientGlobal.getG_tracker_http_port();
    } catch (IOException e) {
        e.printStackTrace();
    }
    return null;
}
```

(9)优化

我们可以发现，上面所有方法中都会涉及到获取TrackerServer或者StorageClient，我们可以把它们单独抽取出去，分别在类中添加如下2个方法：

```java
/***
 * 获取TrackerServer
 */
public static TrackerServer getTrackerServer() throws Exception{
    //创建TrackerClient对象
    TrackerClient trackerClient = new TrackerClient();
    //通过TrackerClient获取TrackerServer对象
    TrackerServer trackerServer = trackerClient.getConnection();
    return trackerServer;
}

/***
 * 获取StorageClient
 * @return
 * @throws Exception
 */
public static StorageClient getStorageClient() throws Exception{
    //获取TrackerServer
    TrackerServer trackerServer = getTrackerServer();
    //通过TrackerServer创建StorageClient
    StorageClient storageClient = new StorageClient(trackerServer,null);
    return storageClient;
}
```

修改其他方法，在需要使用TrackerServer和StorageClient的时候，直接调用上面的方法,完整代码如下：

```java
public class FastDFSClient {

    /***
     * 初始化tracker信息
     */
    static {
        try {
            //获取tracker的配置文件fdfs_client.conf的位置
            String filePath = new ClassPathResource("fdfs_client.conf").getPath();
            //加载tracker配置信息
            ClientGlobal.init(filePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /****
     * 文件上传
     * @param file : 要上传的文件信息封装->FastDFSFile
     * @return String[]
     *          1:文件上传所存储的组名
     *          2:文件存储路径
     */
    public static String[] upload(FastDFSFile file){
        //获取文件作者
        NameValuePair[] meta_list = new NameValuePair[1];
        meta_list[0] =new NameValuePair(file.getAuthor());

        /***
         * 文件上传后的返回值
         * uploadResults[0]:文件上传所存储的组名，例如:group1
         * uploadResults[1]:文件存储路径,例如：M00/00/00/wKjThF0DBzaAP23MAAXz2mMp9oM26.jpeg
         */
        String[] uploadResults = null;
        try {
            //获取StorageClient对象
            StorageClient storageClient = getStorageClient();
            //执行文件上传
            uploadResults = storageClient.upload_file(file.getContent(), file.getExt(), meta_list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return uploadResults;
    }


    /***
     * 获取文件信息
     * @param groupName:组名
     * @param remoteFileName：文件存储完整名
     */
    public static FileInfo getFile(String groupName,String remoteFileName){
        try {
            //获取StorageClient对象
            StorageClient storageClient = getStorageClient();
            //获取文件信息
            return storageClient.get_file_info(groupName,remoteFileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * 文件下载
     * @param groupName:组名
     * @param remoteFileName：文件存储完整名
     * @return
     */
    public static InputStream downFile(String groupName,String remoteFileName){
        try {
            //获取StorageClient
            StorageClient storageClient = getStorageClient();
            //通过StorageClient下载文件
            byte[] fileByte = storageClient.download_file(groupName, remoteFileName);
            //将字节数组转换成字节输入流
            return new ByteArrayInputStream(fileByte);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * 文件删除实现
     * @param groupName:组名
     * @param remoteFileName：文件存储完整名
     */
    public static void deleteFile(String groupName,String remoteFileName){
        try {
            //获取StorageClient
            StorageClient storageClient = getStorageClient();
            //通过StorageClient删除文件
            storageClient.delete_file(groupName,remoteFileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /***
     * 获取组信息
     * @param groupName :组名
     */
    public static StorageServer getStorages(String groupName){
        try {
            //创建TrackerClient对象
            TrackerClient trackerClient = new TrackerClient();
            //通过TrackerClient获取TrackerServer对象
            TrackerServer trackerServer = trackerClient.getConnection();
            //通过trackerClient获取Storage组信息
            return trackerClient.getStoreStorage(trackerServer,groupName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * 根据文件组名和文件存储路径获取Storage服务的IP、端口信息
     * @param groupName :组名
     * @param remoteFileName ：文件存储完整名
     */
    public static ServerInfo[] getServerInfo(String groupName, String remoteFileName){
        try {
            //创建TrackerClient对象
            TrackerClient trackerClient = new TrackerClient();
            //通过TrackerClient获取TrackerServer对象
            TrackerServer trackerServer = trackerClient.getConnection();
            //获取服务信息
            return trackerClient.getFetchStorages(trackerServer,groupName,remoteFileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * 获取Tracker服务地址
     */
    public static String getTrackerUrl(){
        try {
            //创建TrackerClient对象
            TrackerClient trackerClient = new TrackerClient();
            //通过TrackerClient获取TrackerServer对象
            TrackerServer trackerServer = trackerClient.getConnection();
            //获取Tracker地址
            return "http://"+trackerServer.getInetSocketAddress().getHostString()+":"+ClientGlobal.getG_tracker_http_port();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     * 获取TrackerServer
     */
    public static TrackerServer getTrackerServer() throws Exception{
        //创建TrackerClient对象
        TrackerClient trackerClient = new TrackerClient();
        //通过TrackerClient获取TrackerServer对象
        TrackerServer trackerServer = trackerClient.getConnection();
        return trackerServer;
    }

    /***
     * 获取StorageClient
     * @return
     * @throws Exception
     */
    public static StorageClient getStorageClient() throws Exception{
        //获取TrackerServer
        TrackerServer trackerServer = getTrackerServer();
        //通过TrackerServer创建StorageClient
        StorageClient storageClient = new StorageClient(trackerServer,null);
        return storageClient;
    }
}
```

#### 2.1.4.3. 文件上传

创建一个FileController，在该控制器中实现文件上传操作，代码如下：

```java
@RestController
@CrossOrigin
public class FileController {

    /***
     * 文件上传
     * @return
     */
    @PostMapping(value = "/upload")
    public String upload(@RequestParam("file")MultipartFile file) throws Exception {
        //封装一个FastDFSFile
        FastDFSFile fastDFSFile = new FastDFSFile(
                file.getOriginalFilename(), //文件名字
                file.getBytes(),            //文件字节数组
                StringUtils.getFilenameExtension(file.getOriginalFilename()));//文件扩展名

        //文件上传
        String[] uploads = FastDFSClient.upload(fastDFSFile);
        //组装文件上传地址
        return FastDFSClient.getTrackerUrl()+"/"+uploads[0]+"/"+uploads[1];
    }
}
```



## 2.2. 运行与测试

[通用准备工作](#通用准备工作)

[分布式文件存储FastDFS在Docker上的安装步骤和细节说明](#分布式文件存储FastDFS-FastDFS在Docker上的安装步骤和细节说明)

**运行**

运行FastDFS，根据上面的说明

运行[EurekaServerApplication.java](./mall/mall-parent/mall-eureka/src/main/java/com/mall/EurekaServerApplication.java)，访问 [http://127.0.0.1:7001/](http://127.0.0.1:7001/)

运行 [FileApplication.java](./mall/mall-parent/mall-service/mall-service-file/src/main/java/com/mall/file/FileApplication.java)

**测试**

访问Swagger2[在线文档测试](http://127.0.0.1:18082/swagger-ui.html)，测试网址http://127.0.0.1:18082/upload，返回结果是http://172.16.26.2:8080/group1/M00/00/00/rBAaAl-37iSEfdM2AAAAAE3d3Q480.jpeg



## 2.3. 分布式文件存储FastDFS-FastDFS在Docker上的安装步骤和细节说明

<h6>分布式文件存储FastDFS-FastDFS在Docker上的安装步骤和细节说明</h6>

```
The installation steps of FastDFS：
1.Pull image
    docker pull morunchang/fastdfs(if not installed)
2.Run tracker
    docker run -d --name tracker --net=host morunchang/fastdfs sh tracker.sh
3.Run storage
    docker run -d --name storage --net=host -e TRACKER_IP=172.16.26.2:22122 -e GROUP_NAME=group1 morunchang/fastdfs sh storage.sh
4.Open port number
    firewall-cmd --zone=public --add-port 21000/tcp --permanent;
    firewall-cmd --zone=public --add-port 22000/tcp --permanent;
    firewall-cmd --zone=public --add-port 22122/tcp --permanent;
    firewall-cmd --zone=public --add-port 23000/tcp --permanent;
    firewall-cmd --zone=public --add-port 8080/tcp --permanent;
    firewall-cmd --reload;

使用的网络模式是–net=host, TRACKER_IP=192.168.0.104是宿主机的IP,
group1是组名，即storage的组
如果想要增加新的storage服务器，再次运行该命令，注意更换 新组名

Configure Nginx
1.Nginx在这里主要提供对FastDFS图片访问的支持，Docker容器中已经集成了Nginx，我们需要修改nginx的配置,进入storage的容器内部，修改nginx.conf
    docker exec -it storage  /bin/bash
2.进入后
    vi /etc/nginx/conf/nginx.conf
    添加以下内容
    location ~ /M00 {
         root /data/fast_data/data;
         ngx_fastdfs_module;
    }
3.禁止缓存
    add_header Cache-Control no-store;
4.退出容器
    exit
5.重启storage容器
    docker restart storage
6.查看启动容器docker ps
    9f2391f73d97 morunchang/fastdfs "sh storage.sh" 12 minutes ago Up 12 seconds storage
    e22a3c7f95ea morunchang/fastdfs "sh tracker.sh" 13 minutes ago Up 13 minutes tracker
7.开启启动设置
    docker update --restart=always tracker
    docker update --restart=always storage

```

## 2.4. Install Lua in CentOS8（在CentOS8上面安装Lua语言环境）

```
    Preparation:
        yum install gcc
        yum install make
    Installation:
        curl -R -O http://www.lua.org/ftp/lua-5.4.0.tar.gz
        tar zxf lua-5.4.0.tar.gz
        cd lua-5.4.0
        make linux test
        make install
    Test:
        lua
        lua -i
```

## 2.5. Install OpenResty in CentOS8（在CentOS8上面安装OpenResty）

```
    Preparation:
        yum install yum-utils
        yum-config-manager --add-repo https://openresty.org/package/centos/openresty.repo
    Installation:
        yum install -y openresty
    Open 80 port:
        firewall-cmd --zone=public --remove-port=80/tcp --permanent
        firewall-cmd --reload
    Start:
        systemctl start openresty
    Restart:
        systemctl restart openresty
    Stop:
        systemctl stop openresty
    Reboot CentOS8:
        reboot
    Visit local site:
        http://172.16.26.128/
    See current OpenResty version:
        /usr/local/openresty/bin/openresty -V
    Install cmd tool for openresty named resty:
        yum install -y openresty-resty
```

## 2.6. Install Redis in Docker（在Docker上面安装Redis）

```
    Pull image:
        docker pull redis
    Run:
        docker run -itd --name redis-mall --restart=always -p 6379:6379 redis
    Test:
        docker exec -it redis-mall /bin/bash
        redis-cli
```

## 2.7. Test Openresty connect MySQL8 via Lua（测试通过Lua语言Openresty连接MySQL8）

    Config in /etc/mysql/my.cnf about MySQL8 in docker!!!!!!
    ===================================================================
    mkdir -p /home/data/mysql/conf && mkdir -p /home/data/mysql/data
    
    docker cp MySQL8.0.20:/etc/mysql/my.cnf /home/data/mysql/conf/my.cnf
    vi /home/data/mysql/conf/my.cnf
        default_authentication_plugin=mysql_native_password
    
    docker run --name mysql8-Lua \
    -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root \
    --mount type=bind,src=/home/data/mysql/conf/my.cnf,dst=/etc/mysql/my.cnf \
    --mount type=bind,src=/home/data/mysql/data,dst=/var/lib/mysql \
    --restart=on-failure:3 \
    -d mysql:8.0.20
    ===================================================================
**测试Lua语言**

```
    mkdir /root/lua/
    touch /root/lua/update_content.lua
    vi /root/lua/update_content.lua
        ngx.header.content_type="application/json;charset=utf8"
        local cjson = require("cjson")
        local mysql = require("resty.mysql")
        local uri_args = ngx.req.get_uri_args()
        local id = uri_args["id"]
        local db = mysql:new()
        db:set_timeout(1000)
        local props = {
            host = "172.16.26.2",
            port = 3306,
            database = "mall_content",
            user = "root",
            password = "root"
        }
        local res = db:connect(props)
        local select_sql = "select url,pic from content where status ='1' and category_id="..id.." order by sort_order"
        res = db:query(select_sql)
        db:close()
        local redis = require("resty.redis")
        local red = redis:new()
        red:set_timeout(2000)
        local ip ="172.16.26.2"
        local port = 6379
        red:connect(ip,port)
        red:set("content_"..id,cjson.encode(res))
        red:close()
        ngx.say("{flag:true}")


    touch /root/lua/read_content.lua
    vi /root/lua/read_content.lua
        ngx.header.content_type="application/json;charset=utf8"
        local uri_args = ngx.req.get_uri_args();
        local id = uri_args["id"];
        --获取本地缓存
        local cache_ngx = ngx.shared.dis_cache;
        --根据ID 获取本地缓存数据
        local contentCache = cache_ngx:get('content_cache_'..id);
        if contentCache == "" or contentCache == nil then
            local redis = require("resty.redis");
            local red = redis:new()
            red:set_timeout(2000)
            red:connect("172.16.26.128", 6379)
            local rescontent=red:get("content_"..id);
            if ngx.null == rescontent then
                local cjson = require("cjson");
                local mysql = require("resty.mysql");
                local db = mysql:new();
                db:set_timeout(2000)
                local props = {
                    host = "172.16.26.2",
                    port = 3306,
                    database = "mall_content",
                    user = "root",
                    password = "root"
                }
                local res = db:connect(props);
                local select_sql = "select url,pic from content where status ='1' and category_id="..id.." order by sort_order";
                res = db:query(select_sql);
                local responsejson = cjson.encode(res);
                red:set("content_"..id,responsejson);
                ngx.say(responsejson);
                db:close()
            else
                cache_ngx:set('content_cache_'..id, rescontent, 10*60);
                ngx.say(rescontent)
            end
            red:close()
        else
            ngx.say(contentCache)
        end

    vi /usr/local/openresty/nginx/conf/nginx.conf 添加头信息，和 location信息
        lua_shared_dict dis_cache 128m;
        server {
            listen       80;
            server_name  localhost;
            location /update_content {
                content_by_lua_file /root/lua/update_content.lua;
            }
            location /read_content {
                 content_by_lua_file /root/lua/read_content.lua;
            }
        }

    Visit local site(Verify the data exits the MySQL8 database):
        172.16.26.2/update_content?id=1
        172.16.26.2/read_content?id=1

5. Nginx Current Limit
    控制速率的方式之一就是采用漏桶算法。
        (1)漏桶算法实现控制速率限流
            漏桶(Leaky Bucket)算法思路很简单,水(请求)先进入到漏桶里,漏桶以一定的速度出水(接口有响应速率),当水流入速度过大会直接溢出(访问频率超过接口响应速率),然后就拒绝请求,可以看出漏桶算法能强行限制数据的传输速率.示意图如下:
        (2)nginx的配置
            vi /usr/local/openresty/nginx/conf/nginx.conf
                user  root root;
                worker_processes  1;
                events {
                    worker_connections  1024;
                }
                http {
                    include       mime.types;
                    default_type  application/octet-stream;
                    //cache
                    lua_shared_dict dis_cache 128m;
                    //限流设置
                    limit_req_zone $binary_remote_addr zone=contentRateLimit:10m rate=2r/s;
                    sendfile        on;
                    //tcp_nopush     on;
                    //keepalive_timeout  0;
                    keepalive_timeout  65;
                    //gzip  on;
                    server {
                        listen       80;
                        server_name  localhost;
                        location /update_content {
                            content_by_lua_file /root/lua/update_content.lua;
                        }
                        location /read_content {
                            //使用限流配置
                            limit_req zone=contentRateLimit;
                            content_by_lua_file /root/lua/read_content.lua;
                        }
                    }
                }
                配置说明：
                    binary_remote_addr 是一种key，表示基于 remote_addr(客户端IP) 来做限流，binary_ 的目的是压缩内存占用量。
                    zone：定义共享内存区来存储访问信息， contentRateLimit:10m 表示一个大小为10M，名字为contentRateLimit的内存区域。1M能存储16000 IP地址的访问信息，10M可以存储16W IP地址访问信息。
                    rate 用于设置最大访问速率，rate=10r/s 表示每秒最多处理10个请求。Nginx 实际上以毫秒为粒度来跟踪请求信息，因此 10r/s 实际上是限制：每100毫秒处理一个请求。这意味着，自上一个请求处理完后，若后续100毫秒内又有请求到达，将拒绝处理该请求.我们这里设置成2 方便测试。
            systemctl restart openresty
            Visit local site(Verify the data exits the MySQL8 database):
                172.16.26.128/read_content?id=1
        (3) Dealing with burst flow
            上面例子限制 2r/s，如果有时正常流量突然增大，超出的请求将被拒绝，无法处理突发流量，可以结合 **burst** 参数使用来解决该问题。
                server {
                    listen       80;
                    server_name  localhost;
                    location /update_content {
                        content_by_lua_file /root/lua/update_content.lua;
                    }
                    location /read_content {
                        limit_req zone=contentRateLimit burst=4;
                        content_by_lua_file /root/lua/read_content.lua;
                    }
                }
            burst 译为突发、爆发，表示在超过设定的处理速率后能额外处理的请求数,当 rate=10r/s 时，将1s拆成10份，即每100ms可处理1个请求。
            此处，**burst=4 **，若同时有4个请求到达，Nginx 会处理第一个请求，剩余3个请求将放入队列，然后每隔500ms从队列中获取一个请求进行处理。若请求数大于4，将拒绝处理多余的请求，直接返回503.
            不过，单独使用 burst 参数并不实用。假设 burst=50 ，rate依然为10r/s，排队中的50个请求虽然每100ms会处理一个，但第50个请求却需要等待 50 * 100ms即 5s，这么长的处理时间自然难以接受。
            因此，burst 往往结合 nodelay 一起使用。
                server {
                    listen       80;
                    server_name  localhost;
                    location /update_content {
                        content_by_lua_file /root/lua/update_content.lua;
                    }
                    location /read_content {
                        limit_req zone=contentRateLimit burst=4 nodelay;
                        content_by_lua_file /root/lua/read_content.lua;
                    }
                }
            如上表示：
            平均每秒允许不超过2个请求，突发不超过4个请求，并且处理突发4个请求的时候，没有延迟，等到完成之后，按照正常的速率处理。
            如上两种配置结合就达到了速率稳定，但突然流量也能正常处理的效果。完整配置代码如下：
                user  root root;
                worker_processes  1;
                events {
                    worker_connections  1024;
                }
                http {
                    include       mime.types;
                    default_type  application/octet-stream;
                    //cache
                    lua_shared_dict dis_cache 128m;
                    //限流设置
                    limit_req_zone $binary_remote_addr zone=contentRateLimit:10m rate=2r/s;
                    sendfile        on;
                    //tcp_nopush     on;
                    //keepalive_timeout  0;
                    keepalive_timeout  65;
                    //gzip  on;
                    server {
                        listen       80;
                        server_name  localhost;
                        location /update_content {
                            content_by_lua_file /root/lua/update_content.lua;
                        }
                        location /read_content {
                            limit_req zone=contentRateLimit burst=4 nodelay;
                            content_by_lua_file /root/lua/read_content.lua;
                        }
                    }
                }
            测试：
                在1秒钟之内可以刷新4次，正常处理。
                但是超过之后，连续刷新5次，抛出异常。
    控制并发量（连接数）
        ngx_http_limit_conn_module  提供了限制连接数的能力。主要是利用limit_conn_zone和limit_conn两个指令。
        利用连接数限制 某一个用户的ip连接的数量来控制流量。
        注意：并非所有连接都被计算在内 只有当服务器正在处理请求并且已经读取了整个请求头时，才会计算有效连接。此处忽略测试。
        配置语法：
            Syntax:	limit_conn zone number;
            Default: —;
            Context: http, server, location;
        (1)配置限制固定连接数
            http {
                include       mime.types;
                default_type  application/octet-stream;
                //cache
                lua_shared_dict dis_cache 128m;
                //限流设置
                limit_req_zone $binary_remote_addr zone=contentRateLimit:10m rate=2r/s;
                //根据IP地址来限制，存储内存大小10M
                limit_conn_zone $binary_remote_addr zone=addr:1m;
                sendfile        on;
                //tcp_nopush     on;
                //keepalive_timeout  0;
                keepalive_timeout  65;
                //gzip  on;
                server {
                    listen       80;
                    server_name  localhost;
                    //所有以brand开始的请求，访问本地changgou-service-goods微服务
                    location /brand {
                        limit_conn addr 2;
                        proxy_pass http://192.168.0.103:18081;
                    }
                    location /update_content {
                        content_by_lua_file /root/lua/update_content.lua;
                    }
                    location /read_content {
                        limit_req zone=contentRateLimit burst=4 nodelay;
                        content_by_lua_file /root/lua/read_content.lua;
                    }
                }
            }
            配置说明：
                limit_conn_zone $binary_remote_addr zone=addr:10m;  表示限制根据用户的IP地址来显示，设置存储地址为的内存大小10M
                limit_conn addr 2;   表示 同一个地址只允许连接2次。
            测试：
                此时开3个线程，测试的时候会发生异常，开2个就不会有异常
        (2)限制每个客户端IP与服务器的连接数，同时限制与虚拟服务器的连接总数。脚本实际输入不能出现中文(了解)
            limit_conn_zone $binary_remote_addr zone=perip:10m;
            limit_conn_zone $server_name zone=perserver:10m;
            server {
                listen       80;
                server_name  localhost;
                charset utf-8;
                location / {
                    limit_conn perip 10;//单个客户端ip与服务器的连接数．
                    limit_conn perserver 100; ＃限制与服务器的总连接数
                    root   html;
                    index  index.html index.htm;
                }
            }
```

## 2.8. Nginx Current Limit（Nginx限流）

```
    控制速率的方式之一就是采用漏桶算法。
        (1)漏桶算法实现控制速率限流
            漏桶(Leaky Bucket)算法思路很简单,水(请求)先进入到漏桶里,漏桶以一定的速度出水(接口有响应速率),当水流入速度过大会直接溢出(访问频率超过接口响应速率),然后就拒绝请求,可以看出漏桶算法能强行限制数据的传输速率.示意图如下:
        (2)nginx的配置
            vi /usr/local/openresty/nginx/conf/nginx.conf
                user  root root;
                worker_processes  1;
                events {
                    worker_connections  1024;
                }
                http {
                    include       mime.types;
                    default_type  application/octet-stream;
                    //cache
                    lua_shared_dict dis_cache 128m;
                    //限流设置
                    limit_req_zone $binary_remote_addr zone=contentRateLimit:10m rate=2r/s;
                    sendfile        on;
                    //tcp_nopush     on;
                    //keepalive_timeout  0;
                    keepalive_timeout  65;
                    //gzip  on;
                    server {
                        listen       80;
                        server_name  localhost;
                        location /update_content {
                            content_by_lua_file /root/lua/update_content.lua;
                        }
                        location /read_content {
                            //使用限流配置
                            limit_req zone=contentRateLimit;
                            content_by_lua_file /root/lua/read_content.lua;
                        }
                    }
                }
                配置说明：
                    binary_remote_addr 是一种key，表示基于 remote_addr(客户端IP) 来做限流，binary_ 的目的是压缩内存占用量。
                    zone：定义共享内存区来存储访问信息， contentRateLimit:10m 表示一个大小为10M，名字为contentRateLimit的内存区域。1M能存储16000 IP地址的访问信息，10M可以存储16W IP地址访问信息。
                    rate 用于设置最大访问速率，rate=10r/s 表示每秒最多处理10个请求。Nginx 实际上以毫秒为粒度来跟踪请求信息，因此 10r/s 实际上是限制：每100毫秒处理一个请求。这意味着，自上一个请求处理完后，若后续100毫秒内又有请求到达，将拒绝处理该请求.我们这里设置成2 方便测试。
            systemctl restart openresty
            Visit local site(Verify the data exits the MySQL8 database):
                172.16.26.128/read_content?id=1
        (3) Dealing with burst flow
            上面例子限制 2r/s，如果有时正常流量突然增大，超出的请求将被拒绝，无法处理突发流量，可以结合 **burst** 参数使用来解决该问题。
                server {
                    listen       80;
                    server_name  localhost;
                    location /update_content {
                        content_by_lua_file /root/lua/update_content.lua;
                    }
                    location /read_content {
                        limit_req zone=contentRateLimit burst=4;
                        content_by_lua_file /root/lua/read_content.lua;
                    }
                }
            burst 译为突发、爆发，表示在超过设定的处理速率后能额外处理的请求数,当 rate=10r/s 时，将1s拆成10份，即每100ms可处理1个请求。
            此处，**burst=4 **，若同时有4个请求到达，Nginx 会处理第一个请求，剩余3个请求将放入队列，然后每隔500ms从队列中获取一个请求进行处理。若请求数大于4，将拒绝处理多余的请求，直接返回503.
            不过，单独使用 burst 参数并不实用。假设 burst=50 ，rate依然为10r/s，排队中的50个请求虽然每100ms会处理一个，但第50个请求却需要等待 50 * 100ms即 5s，这么长的处理时间自然难以接受。
            因此，burst 往往结合 nodelay 一起使用。
                server {
                    listen       80;
                    server_name  localhost;
                    location /update_content {
                        content_by_lua_file /root/lua/update_content.lua;
                    }
                    location /read_content {
                        limit_req zone=contentRateLimit burst=4 nodelay;
                        content_by_lua_file /root/lua/read_content.lua;
                    }
                }
            如上表示：
            平均每秒允许不超过2个请求，突发不超过4个请求，并且处理突发4个请求的时候，没有延迟，等到完成之后，按照正常的速率处理。
            如上两种配置结合就达到了速率稳定，但突然流量也能正常处理的效果。完整配置代码如下：
                user  root root;
                worker_processes  1;
                events {
                    worker_connections  1024;
                }
                http {
                    include       mime.types;
                    default_type  application/octet-stream;
                    //cache
                    lua_shared_dict dis_cache 128m;
                    //限流设置
                    limit_req_zone $binary_remote_addr zone=contentRateLimit:10m rate=2r/s;
                    sendfile        on;
                    //tcp_nopush     on;
                    //keepalive_timeout  0;
                    keepalive_timeout  65;
                    //gzip  on;
                    server {
                        listen       80;
                        server_name  localhost;
                        location /update_content {
                            content_by_lua_file /root/lua/update_content.lua;
                        }
                        location /read_content {
                            limit_req zone=contentRateLimit burst=4 nodelay;
                            content_by_lua_file /root/lua/read_content.lua;
                        }
                    }
                }
            测试：
                在1秒钟之内可以刷新4次，正常处理。
                但是超过之后，连续刷新5次，抛出异常。
    控制并发量（连接数）
        ngx_http_limit_conn_module  提供了限制连接数的能力。主要是利用limit_conn_zone和limit_conn两个指令。
        利用连接数限制 某一个用户的ip连接的数量来控制流量。
        注意：并非所有连接都被计算在内 只有当服务器正在处理请求并且已经读取了整个请求头时，才会计算有效连接。此处忽略测试。
        配置语法：
            Syntax:	limit_conn zone number;
            Default: —;
            Context: http, server, location;
        (1)配置限制固定连接数
            http {
                include       mime.types;
                default_type  application/octet-stream;
                //cache
                lua_shared_dict dis_cache 128m;
                //限流设置
                limit_req_zone $binary_remote_addr zone=contentRateLimit:10m rate=2r/s;
                //根据IP地址来限制，存储内存大小10M
                limit_conn_zone $binary_remote_addr zone=addr:1m;
                sendfile        on;
                //tcp_nopush     on;
                //keepalive_timeout  0;
                keepalive_timeout  65;
                //gzip  on;
                server {
                    listen       80;
                    server_name  localhost;
                    //所有以brand开始的请求，访问本地changgou-service-goods微服务
                    location /brand {
                        limit_conn addr 2;
                        proxy_pass http://192.168.0.103:18081;
                    }
                    location /update_content {
                        content_by_lua_file /root/lua/update_content.lua;
                    }
                    location /read_content {
                        limit_req zone=contentRateLimit burst=4 nodelay;
                        content_by_lua_file /root/lua/read_content.lua;
                    }
                }
            }
            配置说明：
                limit_conn_zone $binary_remote_addr zone=addr:10m;  表示限制根据用户的IP地址来显示，设置存储地址为的内存大小10M
                limit_conn addr 2;   表示 同一个地址只允许连接2次。
            测试：
                此时开3个线程，测试的时候会发生异常，开2个就不会有异常
        (2)限制每个客户端IP与服务器的连接数，同时限制与虚拟服务器的连接总数。脚本实际输入不能出现中文(了解)
            limit_conn_zone $binary_remote_addr zone=perip:10m;
            limit_conn_zone $server_name zone=perserver:10m;
            server {
                listen       80;
                server_name  localhost;
                charset utf-8;
                location / {
                    limit_conn perip 10;#单个客户端ip与服务器的连接数．
                    limit_conn perserver 100; ＃限制与服务器的总连接数
                    root   html;
                    index  index.html index.htm;
                }
            }
```

# 3. Elasticsearch

## 3.1. Elasticsearch 安装 

<h1>Elasticsearch 安装</h1>

我们之前已经使用过elasticsearch了，这里不再对它进行介绍了，直接下载安装，本章节将采用Docker安装，不过在市面上还有很多采用linxu安装，关于linux安装，已经提供了安装手册，这里就不讲了。



(1)docker镜像下载

```properties
docker pull elasticsearch:5.6.8
docker pull elasticsearch:7.9.3(当时最新)
docker pull elasticsearch:7.9.2(当时最新，配合IK分词器版本)
```

(2)安装es容器

在Linux安装之前要设置命令参数：

**sudo vi /etc/sysctl.conf**（命令）

​	**vm.max_map_count=655360**（内容）

**sysctl -w vm.max_map_count=655360**（命令：不重启， 直接生效当前的命令）

```properties
这里需要手动设置一下虚拟机内存，就是因为云服务器是1G运行内存的导致内存不够无法启动。-Xms512m -Xmx512m

docker run -d --name mall_elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node"  -e ES_JAVA_OPTS="-Xms512m -Xmx512m" elasticsearch:7.9.2(可行)

firewall-cmd --zone=public --add-port 9200/tcp --permanent;（开放端口9200）
firewall-cmd --zone=public --add-port 9300/tcp --permanent;（开放端口9300）

firewall-cmd --reload;（开放端口生效）
```

 9200端口(Web管理平台端口)  9300(服务默认端口)

浏览器输入地址访问：[ES Web管理平台](http://172.16.26.2:9200/)

(3)开启远程连接

上面完成安装后，es并不能正常使用，elasticsearch从5版本以后默认不开启远程连接，程序直接连接会报如下错误：

```java
failed to load elasticsearch nodes : org.elasticsearch.client.transport.NoNodeAvailableException: None of the configured nodes are available: [{#transport#-1}{5ttLpMhkRjKLkvoY7ltUWg}{192.168.211.132}{192.168.211.132:9300}]
```

我们需要修改es配置开启远程连接，代码如下：

登录容器

```properties
docker exec -it mall_elasticsearch /bin/bash
```

修改elasticsearch.yml文件

```properties
root@07f22eb41bb5:/usr/share/elasticsearch/config# vi /usr/share/elasticsearch/config/elasticsearch.yml
bash: vi: command not found
```

vi命令无法识别，因为docker容器里面没有该命令，我们可以安装该编辑器。

安装vim编辑器

```properties
apt-get update
apt-get install vim
```

安装好了后，修改elasticsearch.yml配置，如下图：

```properties
vi elasticsearch.yml
```

修改如下：

```properties
cluster.name: "my-application"
network.host: 0.0.0.0
```

使用exit命令退出容器后，重启docker

```properties
docker restart mall_elasticsearch
```

(4)系统参数配置

重启后发现重启启动失败了，这时什么原因呢？这与我们刚才修改的配置有关，因为elasticsearch在启动的时候会进行一些检查，比如最多打开的文件的个数以及虚拟内存区域数量等等，如果你放开了此配置，意味着需要打开更多的文件以及虚拟内存，所以我们还需要系统调优 

修改vi /etc/security/limits.conf ，追加内容 (nofile是单个进程允许打开的最大文件个数 soft nofile 是软限制 hard nofile是硬限制 )

```properties
* soft nofile 65536
* hard nofile 65536
```

修改vi /etc/sysctl.conf，追加内容 (限制一个进程可以拥有的VMA(虚拟内存区域)的数量 )

```properties
vm.max_map_count=655360
```

执行下面命令 修改内核参数马上生效

```properties
sysctl -p
```

重新启动虚拟机，再次启动容器，发现已经可以启动并远程访问 

```properties
reboot
```

(5)跨域配置

修改elasticsearch/config下的配置文件：elasticsearch.yml，增加以下三个属性内容，并重启:

登录容器

```properties
docker exec -it mall_elasticsearch /bin/bash
```

修改elasticsearch.yml文件

```properties
vi /usr/share/elasticsearch/config/elasticsearch.yml
```

内容：

```properties
http.cors.enabled: true
http.cors.allow-origin: "*"
```

其中：
http.cors.enabled: true：此步为允许elasticsearch跨域访问，默认是false。
http.cors.allow-origin: "*"：表示跨域访问允许的域名地址（*表示任意）。

exit命令退出容器后，重启容器

```properties
docker restart mall_elasticsearch
```

小提示：如果想让容器开启重启，可以执行下面命令

```properties
docker update --restart=always 容器名称或者容器id
```

## 3.2. IK分词器安装 

(1)安装ik分词器

IK分词器下载地址https://github.com/medcl/elasticsearch-analysis-ik/releases

将ik分词器上传到服务器上，然后解压，并改名字为ik，将ik目录拷贝到docker容器的plugins目录下，**需要注意的是ElasticSearch的版本要和IK版本一致**

```properties
wget https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v7.9.2/elasticsearch-analysis-ik-7.9.2.zip(有些慢，可以提前下载好，传入虚拟机中)
unzip elasticsearch-analysis-ik-7.9.2.zip -d ./ik(解压并重命名)
mv elasticsearch-analysis-ik-7.9.2 ik(重命名作用)
docker cp ./ik mall_elasticsearch:/usr/share/elasticsearch/plugins（复制）

docker restart mall_elasticsearch（重启并访问生效）

-----------------------其他命令----------------------------
	
./bin/elasticsearch-plugin  list(查看已安装插件列表)

docker exec -it mall_elasticsearch /bin/bash
cd /usr/share/elasticsearch/plugins

./bin/elasticsearch-plugin install https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v7.9.2/elasticsearch-analysis-ik-7.9.2.zip
```

## 3.3. Kibana

### 3.3.1. Kibana下载安装

我们项目中不再使用linux，直接使用Docker，所有这里就不演示在windows的下载安装了。

(1)镜像下载

```properties
docker pull kibana:7.9.2（配合运行的版本）
```

为了节省时间，虚拟机中已经存在该版本的镜像了.

(2)安装kibana容器

执行如下命令，开始安装kibana容器

```properties
docker run -it -d --name kibana --restart=always -p 5601:5601 kibana:7.9.2
docker exec -it kibana /bin/bash
vi /usr/share/kibana/config/kibana.yml(修改elasticsearch.hosts为当前可以访问的IP)
exit
docker restart kibana

firewall-cmd --zone=public --add-port 5601/tcp --permanent;（5601）

firewall-cmd --reload;（开放端口生效）
```

**修改kibana.yml配置后，可能会过一会儿生效**

restart=always:每次服务都会重启，也就是开启启动

5601:5601:端口号

[Kibana访问地址](http://172.16.26.2:5601/)

## 3.4. 运行与测试

### 3.4.1. 准备工作

[通用准备工作](#通用准备工作)

[Elasticsearch 安装](#Elasticsearch 安装)

**导入数据**

因为数据是从mysql导入Elasticsearch中的，所以先在Navicat导入mall_goods数据库中下面的数据，导入流程，右击‘Tables’->Import Wizard-Text file，先导入[spu.txt](./sql/spu.txt)，在导入[sku.txt](./sql/sku.txt)（一定按照顺序，因为外键关联）。

### 3.4.2. 运行

运行[EurekaServerApplication.java](./mall/mall-parent/mall-eureka/src/main/java/com/mall/EurekaServerApplication.java)，访问 [http://127.0.0.1:7001/](http://127.0.0.1:7001/)

运行[SearchApplication.java](./mall/mall-parent/mall-service/mall-service-search/src/main/java/com/mall/search/SearchApplication.java)

### 3.4.3. 测试

访问Swagger2[在线文档测试](http://127.0.0.1:18085/swagger-ui.html)，运行 GET "http://127.0.0.1:18085/search/import"导入数据

访问[Kibana界面开发工具](http://172.16.26.2:5601/app/dev_tools#/console)查看数据是否导入成功

通过输入下面的命令查询全部数据

```json
GET /skuinfo/_search
{
  "query": {
    "match_all": {}
  }
}
```

运行 POST "http://127.0.0.1:18085/search"查询数据，下面是查询条件

```json
{
    "keyInput":{
        "keywords":"移动",
        "category":"手机",
        "brand":"vivo",
        "price":"0-500000"
    },
    "pageInfo":{
        "pageNum":0,
        "pageSize":20
    },
    "sortInfo":[
        {
            "field":"price",
            "rule":"DESC"
        }
    ]
}
```



# 4. 微服务网关和Jwt令牌

**学习目标**

+ 掌握微服务网关的系统搭建

+ 了解什么是微服务网关以及它的作用

+ 掌握系统中心微服务的搭建

+ 掌握用户密码加密存储bcrypt

+ 了解JWT鉴权的介绍

+ ==掌握JWT的鉴权的使用==

  使用Jwt令牌来存储用户登录信息，在微服务网关中识别登录信息(用户的身份)

+ ==掌握网关使用JWT进行校验==

+ 掌握==网关==限流



## 4.1. 微服务网关

### 4.1.1. 微服务网关的概述

不同的微服务一般会有不同的网络地址，而外部客户端可能需要调用多个服务的接口才能完成一个业务需求，如果让客户端直接与各个微服务通信，会有以下的问题：

+ 客户端会多次请求不同的微服务，增加了客户端的复杂性
+ 存在跨域请求，在一定场景下处理相对复杂
+ 认证复杂，每个服务都需要独立认证
+ 难以重构，随着项目的迭代，可能需要重新划分微服务。例如，可能将多个服务合并成一个或者将一个服务拆分成多个。如果客户端直接与微服务通信，那么重构将会很难实施
+ 某些微服务可能使用了防火墙 / 浏览器不友好的协议，直接访问会有一定的困难

以上这些问题可以借助网关解决。

网关是介于客户端和服务器端之间的中间层，所有的外部请求都会先经过 网关这一层。也就是说，API 的实现方面更多的考虑业务逻辑，而安全、性能、监控可以交由 网关来做，这样既提高业务灵活性又不缺安全性，典型的架构图如图所示：



优点如下：

+ 安全 ，只有网关系统对外进行暴露，微服务可以隐藏在内网，通过防火墙保护。
+ 易于监控。可以在网关收集监控数据并将其推送到外部系统进行分析。
+ 易于认证。可以在网关上进行认证，然后再将请求转发到后端的微服务，而无须在每个微服务中进行认证。
+ 减少了客户端与各个微服务之间的交互次数
+ 易于统一授权。

总结：微服务网关就是一个系统，通过暴露该微服务网关系统，方便我们进行相关的鉴权，安全控制，日志统一处理，易于监控的相关功能。



### 4.1.2. 微服务网关技术

实现微服务网关的技术有很多，

+ nginx  Nginx (tengine x) 是一个高性能的[HTTP](https://baike.baidu.com/item/HTTP)和[反向代理](https://baike.baidu.com/item/%E5%8F%8D%E5%90%91%E4%BB%A3%E7%90%86/7793488)web服务器，同时也提供了IMAP/POP3/SMTP服务
+ zuul ,Zuul 是 Netflix 出品的一个基于 JVM 路由和服务端的负载均衡器。
+ spring-cloud-gateway, 是spring 出品的 基于spring 的网关项目，集成断路器，路径重写，性能比Zuul好。

我们使用gateway这个网关技术，无缝衔接到基于spring cloud的微服务开发中来。

gateway官网：

https://spring.io/projects/spring-cloud-gateway



## 4.2. 网关系统使用

### 4.2.1. 需求分析

​	由于我们开发的系统 有包括前台系统和后台系统，后台的系统 给管理员使用。那么也需要调用各种微服务，所以我们针对 系统管理搭建一个网关系统。分析如下：

### 4.2.2. 跨域配置

有时候，我们需要对所有微服务跨域请求进行处理，则可以在gateway中进行跨域支持。修改application.yml,添加如下代码：

```yaml
spring:
  cloud:
    gateway:
      globalcors:
        cors-configurations:
          '[/**]': # 匹配所有请求
              allowedOrigins: "*" #跨域处理 允许所有的域
              allowedMethods: # 支持的方法
                - GET
                - POST
                - PUT
                - DELETE
```



最终文件如下：

```yaml
spring:
  cloud:
    gateway:
      globalcors:
        cors-configurations:
          '[/**]': # 匹配所有请求
              allowedOrigins: "*" #跨域处理 允许所有的域
              allowedMethods: # 支持的方法
                - GET
                - POST
                - PUT
                - DELETE
  application:
    name: gateway-web
server:
  port: 8001
eureka:
  client:
    service-url:
      defaultZone: http://127.0.0.1:7001/eureka
  instance:
    prefer-ip-address: true
management:
  endpoint:
    gateway:
      enabled: true
    web:
      exposure:
        include: true
```





### 4.2.3. 网关过滤配置

路由过滤器允许以某种方式修改传入的HTTP请求或传出的HTTP响应。 路径过滤器的范围限定为特定路径。 Spring Cloud Gateway包含许多内置的GatewayFilter工厂。如上图，根据请求路径路由到不同微服务去，这块可以使用Gateway的路由过滤功能实现。

过滤器 有 20 多个 实现 类， 包括 头部 过滤器、 路径 类 过滤器、 Hystrix 过滤器 和 变更 请求 URL 的 过滤器， 还有 参数 和 状态 码 等 其他 类型 的 过滤器。

内置的过滤器工厂有22个实现类，包括 头部过滤器、路径过滤器、Hystrix 过滤器 、请求URL 变更过滤器，还有参数和状态码等其他类型的过滤器。根据过滤器工厂的用途来划分，可以分为以下几种：Header、Parameter、Path、Body、Status、Session、Redirect、Retry、RateLimiter和Hystrix。



### 4.2.4. 网关限流

网关可以做很多的事情，比如，限流，当我们的系统 被频繁的请求的时候，就有可能 将系统压垮，所以 为了解决这个问题，需要在每一个微服务中做限流操作，但是如果有了网关，那么就可以在网关系统做限流，因为所有的请求都需要先通过网关系统才能路由到微服务中。



#### 4.2.4.1. 思路分析



#### 4.2.4.2. 令牌桶算法

令牌桶算法是比较常见的限流算法之一，大概描述如下：
1）所有的请求在处理之前都需要拿到一个可用的令牌才会被处理；
2）根据限流大小，设置按照一定的速率往桶里添加令牌；
3）桶设置最大的放置令牌限制，当桶满时、新添加的令牌就被丢弃或者拒绝；
4）请求达到后首先要获取令牌桶中的令牌，拿着令牌才可以进行其他的业务逻辑，处理完业务逻辑之后，将令牌直接删除；
5）令牌桶有最低限额，当桶中的令牌达到最低限额的时候，请求处理完之后将不会删除令牌，以此保证足够的限流



这个算法的实现，有很多技术，Guaua是其中之一，redis客户端也有其实现。



#### 4.2.4.3. 使用令牌桶进行请求次数限流

spring cloud gateway 默认使用redis的RateLimter限流算法来实现。所以我们要使用首先需要引入redis的依赖

(1)引入redis依赖

在changgou-gateway的pom.xml中引入redis的依赖

```xml
<!--redis-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis-reactive</artifactId>
    <version>2.1.3.RELEASE</version>
</dependency>
```



(2)定义KeyResolver

在Applicatioin引导类中添加如下代码，KeyResolver用于计算某一个类型的限流的KEY也就是说，可以通过KeyResolver来指定限流的Key。

我们可以根据IP来限流，比如每个IP每秒钟只能请求一次，在GatewayWebApplication定义key的获取，获取客户端IP，将IP作为key，如下代码：

```java
/***
 * IP限流
 * @return
 */
@Bean(name="ipKeyResolver")
public KeyResolver userKeyResolver() {
    return new KeyResolver() {
        @Override
        public Mono<String> resolve(ServerWebExchange exchange) {
            //获取远程客户端IP
            String hostName = exchange.getRequest().getRemoteAddress().getAddress().getHostAddress();
            System.out.println("hostName:"+hostName);
            return Mono.just(hostName);
        }
    };
}
```



(3)修改application.yml中配置项，指定限制流量的配置以及REDIS的配置，如图

修改如下图：

配置代码如下：

```yaml
spring:
  cloud:
    gateway:
      globalcors:
        corsConfigurations:
          '[/**]': # 匹配所有请求
              allowedOrigins: "*" #跨域处理 允许所有的域
              allowedMethods: # 支持的方法
                - GET
                - POST
                - PUT
                - DELETE
      routes:
            - id: changgou_goods_route
              uri: lb://goods
              predicates:
              - Path=/api/brand**
              filters:
              - StripPrefix=1
              - name: RequestRateLimiter #请求数限流 名字不能随便写 ，使用默认的facatory
                args:
                  key-resolver: "#{@ipKeyResolver}"
                  redis-rate-limiter.replenishRate: 1
                  redis-rate-limiter.burstCapacity: 1

  application:
    name: gateway-web
  redis:
    host: 192.168.211.132
    port: 6379

server:
  port: 8001
eureka:
  client:
    service-url:
      defaultZone: http://127.0.0.1:7001/eureka
  instance:
    prefer-ip-address: true
management:
  endpoint:
    gateway:
      enabled: true
    web:
      exposure:
        include: true
```

解释：

`redis-rate-limiter.replenishRate`是您希望允许用户每秒执行多少请求，而不会丢弃任何请求。这是令牌桶填充的速率

`redis-rate-limiter.burstCapacity`是指令牌桶的容量，允许在一秒钟内完成的最大请求数,将此值设置为零将阻止所有请求。

 key-resolver: "#{@ipKeyResolver}" 用于通过SPEL表达式来指定使用哪一个KeyResolver.

如上配置：

表示 一秒内，允许 一个请求通过，令牌桶的填充速率也是一秒钟添加一个令牌。

最大突发状况 也只允许 一秒内有一次请求，可以根据业务来调整 。



多次请求会发生如下情况







## 4.3. 用户登录

项目中有2个重要角色，分别为管理员和用户，下面几章我们将实现购物下单和支付，用户如果没登录是没法下单和支付的，所以我们这里需要实现一个登录功能。



## 4.4. JWT讲解

### 4.4.1. 需求分析



### 4.4.2. 什么是JWT

JSON Web Token（JWT）是一个非常轻巧的规范。这个规范允许我们使用JWT在用户和服务器之间传递安全可靠的信息。



### 4.4.3. JWT的构成

一个JWT实际上就是一个字符串，它由三部分组成，头部、载荷与签名。(需要看第二遍)

**头部（Header）**

头部用于描述关于该JWT的最基本的信息，例如其类型以及签名所用的算法等。这也可以被表示成一个JSON对象。

```json
{"typ":"JWT","alg":"HS256"}
```

在头部指明了签名算法是HS256算法。 我们进行BASE64编码http://base64.xpcha.com/，编码后的字符串如下：

```
eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9
```

> 小知识：Base64是一种基于64个可打印字符来表示二进制数据的表示方法。由于2的6次方等于64，所以每6个比特为一个单元，对应某个可打印字符。三个字节有24个比特，对应于4个Base64单元，即3个字节需要用4个可打印字符来表示。JDK 中提供了非常方便的 **BASE64Encoder** 和 **BASE64Decoder**，用它们可以非常方便的完成基于 BASE64 的编码和解码

**载荷（playload）**

载荷就是存放有效信息的地方。这个名字像是特指飞机上承载的货品，这些有效信息包含三个部分

（1）标准中注册的声明（建议但不强制使用）

```
iss: jwt签发者
sub: jwt所面向的用户
aud: 接收jwt的一方
exp: jwt的过期时间，这个过期时间必须要大于签发时间
nbf: 定义在什么时间之前，该jwt都是不可用的.
iat: jwt的签发时间
jti: jwt的唯一身份标识，主要用来作为一次性token,从而回避重放攻击。
```

（2）公共的声明

公共的声明可以添加任何的信息，一般添加用户的相关信息或其他业务需要的必要信息.但不建议添加敏感信息，因为该部分在客户端可解密.   

（3）私有的声明

私有声明是提供者和消费者所共同定义的声明，一般不建议存放敏感信息，因为base64是对称解密的，意味着该部分信息可以归类为明文信息。

这个指的就是自定义的claim。比如下面面结构举例中的admin和name都属于自定的claim。这些claim跟JWT标准规定的claim区别在于：JWT规定的claim，JWT的接收方在拿到JWT之后，都知道怎么对这些标准的claim进行验证(还不知道是否能够验证)；而private claims不会验证，除非明确告诉接收方要对这些claim进行验证以及规则才行。

定义一个payload:

```
{"sub":"1234567890","name":"John Doe","admin":true}
```

然后将其进行base64加密，得到Jwt的第二部分。

```
eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9
```

**签证（signature）**

jwt的第三部分是一个签证信息，这个签证信息由三部分组成：

> header (base64后的)
>
> payload (base64后的)
>
> secret（秘钥->盐）

这个部分需要base64加密后的header和base64加密后的payload使用.连接组成的字符串，然后通过header中声明的加密方式进行加盐secret组合加密，然后就构成了jwt的第三部分。

```
TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ
```

将这三部分用.连接成一个完整的字符串,构成了最终的jwt:

```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ
```

**注意**：secret是保存在服务器端的，jwt的签发生成也是在服务器端的，secret就是用来进行jwt的签发和jwt的验证，所以，它就是你服务端的私钥，在任何场景都不应该流露出去。一旦客户端得知这个secret, 那就意味着客户端是可以自我签发jwt了。



### 4.4.4. JJWT的介绍和使用

JJWT是一个提供端到端的JWT创建和验证的Java库。永远免费和开源(Apache License，版本2.0)，JJWT很容易使用和理解。它被设计成一个以建筑为中心的流畅界面，隐藏了它的大部分复杂性。

官方文档：

https://github.com/jwtk/jjwt



#### 4.4.4.1. 创建TOKEN

(1)依赖引入

在changgou-parent项目中的pom.xml中添加依赖：

```xml
<!--鉴权-->
<dependency>
    <groupId>io.jsonwebtoken</groupId>
    <artifactId>jjwt</artifactId>
    <version>0.9.0</version>
</dependency>
```



(2)创建测试

在changgou-common的/test/java下创建测试类，并设置测试方法

```java
public class JwtTest {

    /****
     * 创建Jwt令牌
     */
    @Test
    public void testCreateJwt(){
        JwtBuilder builder= Jwts.builder()
                .setId("888")             //设置唯一编号
                .setSubject("小白")       //设置主题  可以是JSON数据
                .setIssuedAt(new Date())  //设置签发日期
                .signWith(SignatureAlgorithm.HS256,"itcast");//设置签名 使用HS256算法，并设置SecretKey(字符串)
        //构建 并返回一个字符串
        System.out.println( builder.compact() );
    }
}
```

运行打印结果：

```properties
eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI4ODgiLCJzdWIiOiLlsI_nmb0iLCJpYXQiOjE1NjIwNjIyODd9.RBLpZ79USMplQyfJCZFD2muHV_KLks7M1ZsjTu6Aez4
```

再次运行，会发现每次运行的结果是不一样的，因为我们的载荷中包含了时间。



#### 4.4.4.2. TOKEN解析

我们刚才已经创建了token ，在web应用中这个操作是由服务端进行然后发给客户端，客户端在下次向服务端发送请求时需要携带这个token（这就好像是拿着一张门票一样），那服务端接到这个token 应该解析出token中的信息（例如用户id）,根据这些信息查询数据库返回相应的结果。

```java
/***
 * 解析Jwt令牌数据
 */
@Test
public void testParseJwt(){
    String compactJwt="eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI4ODgiLCJzdWIiOiLlsI_nmb0iLCJpYXQiOjE1NjIwNjIyODd9.RBLpZ79USMplQyfJCZFD2muHV_KLks7M1ZsjTu6Aez4";
    Claims claims = Jwts.parser().
            setSigningKey("itcast").
            parseClaimsJws(compactJwt).
            getBody();
    System.out.println(claims);
}
```

运行打印效果：

```
{jti=888, sub=小白, iat=1562062287}
```

试着将token或签名秘钥篡改一下，会发现运行时就会报错，所以解析token也就是验证token.



#### 4.4.4.3. 设置过期时间

有很多时候，我们并不希望签发的token是永久生效的，所以我们可以为token添加一个过期时间。

```properties
.setExpiration(date)//用于设置过期时间 ，参数为Date类型数据
```

运行，打印效果如下：

```properties
eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI4ODgiLCJzdWIiOiLlsI_nmb0iLCJpYXQiOjE1NjIwNjI5MjUsImV4cCI6MTU2MjA2MjkyNX0._vs4METaPkCza52LuN0-2NGGWIIO7v51xt40DHY1U1Q
```



##### 4.4.4.3.1. 解析TOKEN

```java
/***
 * 解析Jwt令牌数据
 */
@Test
public void testParseJwt(){
    String compactJwt="eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI4ODgiLCJzdWIiOiLlsI_nmb0iLCJpYXQiOjE1NjIwNjI5MjUsImV4cCI6MTU2MjA2MjkyNX0._vs4METaPkCza52LuN0-2NGGWIIO7v51xt40DHY1U1Q";
    Claims claims = Jwts.parser().
            setSigningKey("itcast").
            parseClaimsJws(compactJwt).
            getBody();
    System.out.println(claims);
}
```

#### 4.4.4.4. 自定义claims

我们刚才的例子只是存储了id和subject两个信息，如果你想存储更多的信息（例如角色）可以定义自定义claims。

创建测试类，并设置测试方法：



```properties
eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI4ODgiLCJzdWIiOiLlsI_nmb0iLCJpYXQiOjE1NjIwNjMyOTIsImFkZHJlc3MiOiLmt7HlnLPpu5Hpqazorq3nu4PokKXnqIvluo_lkZjkuK3lv4MiLCJuYW1lIjoi546L5LqUIiwiYWdlIjoyN30.ZSbHt5qrxz0F1Ma9rVHHAIy4jMCBGIHoNaaPQXxV_dk
```



解析TOKEN:

```JAVA
/***
 * 解析Jwt令牌数据
 */
@Test
public void testParseJwt(){
    String compactJwt="eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI4ODgiLCJzdWIiOiLlsI_nmb0iLCJpYXQiOjE1NjIwNjMyOTIsImFkZHJlc3MiOiLmt7HlnLPpu5Hpqazorq3nu4PokKXnqIvluo_lkZjkuK3lv4MiLCJuYW1lIjoi546L5LqUIiwiYWdlIjoyN30.ZSbHt5qrxz0F1Ma9rVHHAIy4jMCBGIHoNaaPQXxV_dk";
    Claims claims = Jwts.parser().
            setSigningKey("itcast").
            parseClaimsJws(compactJwt).
            getBody();
    System.out.println(claims);
}
```





### 4.4.5. 鉴权处理

#### 4.4.5.1. 思路分析

```properties
1.用户通过访问微服务网关调用微服务，同时携带头文件信息
2.在微服务网关这里进行拦截，拦截后获取用户要访问的路径
3.识别用户访问的路径是否需要登录，如果需要，识别用户的身份是否能访问该路径[这里可以基于数据库设计一套权限]
4.如果需要权限访问，用户已经登录，则放行
5.如果需要权限访问，且用户未登录，则提示用户需要登录
6.用户通过网关访问用户微服务，进行登录验证
7.验证通过后，用户微服务会颁发一个令牌给网关，网关会将用户信息封装到头文件中，并响应用户
8.用户下次访问，携带头文件中的令牌信息即可识别是否登录
```



#### 4.4.5.2. 用户登录签发TOKEN

(1)生成令牌工具类

在changgou-common中创建类entity.JwtUtil，主要辅助生成Jwt令牌信息，代码如下：

```java
public class JwtUtil {

    //有效期为
    public static final Long JWT_TTL = 3600000L;// 60 * 60 *1000  一个小时

    //Jwt令牌信息
    public static final String JWT_KEY = "itcast";

    public static String createJWT(String id, String subject, Long ttlMillis) {
        //指定算法
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        //当前系统时间
        long nowMillis = System.currentTimeMillis();
        //令牌签发时间
        Date now = new Date(nowMillis);

        //如果令牌有效期为null，则默认设置有效期1小时
        if(ttlMillis==null){
            ttlMillis=JwtUtil.JWT_TTL;
        }

        //令牌过期时间设置
        long expMillis = nowMillis + ttlMillis;
        Date expDate = new Date(expMillis);

        //生成秘钥
        SecretKey secretKey = generalKey();

        //封装Jwt令牌信息
        JwtBuilder builder = Jwts.builder()
                .setId(id)                    //唯一的ID
                .setSubject(subject)          // 主题  可以是JSON数据
                .setIssuer("admin")          // 签发者
                .setIssuedAt(now)             // 签发时间
                .signWith(signatureAlgorithm, secretKey) // 签名算法以及密匙
                .setExpiration(expDate);      // 设置过期时间
        return builder.compact();
    }

    /**
     * 生成加密 secretKey
     * @return
     */
    public static SecretKey generalKey() {
        byte[] encodedKey = Base64.getEncoder().encode(JwtUtil.JWT_KEY.getBytes());
        SecretKey key = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
        return key;
    }


    /**
     * 解析令牌数据
     * @param jwt
     * @return
     * @throws Exception
     */
    public static Claims parseJWT(String jwt) throws Exception {
        SecretKey secretKey = generalKey();
        return Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(jwt)
                .getBody();
    }
}
```



(2) 用户登录成功 则 签发TOKEN，修改登录的方法：

![1562070521132](./images/mall-14.png)

代码如下：

```java
/***
 * 用户登录
 */
@RequestMapping(value = "/login")
public Result login(String username,String password){
    //查询用户信息
    User user = userService.findById(username);

    if(user!=null && BCrypt.checkpw(password,user.getPassword())){
        //设置令牌信息
        Map<String,Object> info = new HashMap<String,Object>();
        info.put("role","USER");
        info.put("success","SUCCESS");
        info.put("username",username);
        //生成令牌
        String jwt = JwtUtil.createJWT(UUID.randomUUID().toString(), JSON.toJSONString(info),null);
        return new Result(true,StatusCode.OK,"登录成功！",jwt);
    }
    return  new Result(false,StatusCode.LOGINERROR,"账号或者密码错误！");
}
```



#### 4.4.5.3. 网关过滤器拦截请求处理

拷贝JwtUtil到changgou-gateway-web中

![1562116408008](./images/mall-14.png)





#### 4.4.5.4. 自定义全局过滤器

创建 过滤器类，如图所示：

![1562116467343](./images/mall-14.png)

AuthorizeFilter代码如下：

```java
@Component
public class AuthorizeFilter implements GlobalFilter, Ordered {

    //令牌头名字
    private static final String AUTHORIZE_TOKEN = "Authorization";

    /***
     * 全局过滤器
     * @param exchange
     * @param chain
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //获取Request、Response对象
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();

        //获取请求的URI
        String path = request.getURI().getPath();

        //如果是登录、goods等开放的微服务[这里的goods部分开放],则直接放行,这里不做完整演示，完整演示需要设计一套权限系统
        if (path.startsWith("/api/user/login") || path.startsWith("/api/brand/search/")) {
            //放行
            Mono<Void> filter = chain.filter(exchange);
            return filter;
        }

        //获取头文件中的令牌信息
        String tokent = request.getHeaders().getFirst(AUTHORIZE_TOKEN);

        //如果头文件中没有，则从请求参数中获取
        if (StringUtils.isEmpty(tokent)) {
            tokent = request.getQueryParams().getFirst(AUTHORIZE_TOKEN);
        }

        //如果为空，则输出错误代码
        if (StringUtils.isEmpty(tokent)) {
            //设置方法不允许被访问，405错误代码
           response.setStatusCode(HttpStatus.METHOD_NOT_ALLOWED);
           return response.setComplete();
        }

        //解析令牌数据
        try {
            Claims claims = JwtUtil.parseJWT(tokent);
        } catch (Exception e) {
            e.printStackTrace();
            //解析失败，响应401错误
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }

        //放行
        return chain.filter(exchange);
    }


    /***
     * 过滤器执行顺序
     * @return
     */
    @Override
    public int getOrder() {
        return 0;
    }
}
```





#### 4.4.5.5. 配置过滤规则



```yaml
spring:
  cloud:
    gateway:
      globalcors:
        corsConfigurations:
          '[/**]': # 匹配所有请求
              allowedOrigins: "*" #跨域处理 允许所有的域
              allowedMethods: # 支持的方法
                - GET
                - POST
                - PUT
                - DELETE
      routes:
            - id: changgou_goods_route
              uri: lb://goods
              predicates:
              - Path=/api/album/**,/api/brand/**,/api/cache/**,/api/categoryBrand/**,/api/category/**,/api/para/**,/api/pref/**,/api/sku/**,/api/spec/**,/api/spu/**,/api/stockBack/**,/api/template/**
              filters:
              - StripPrefix=1
              - name: RequestRateLimiter #请求数限流 名字不能随便写 ，使用默认的facatory
                args:
                  key-resolver: "#{@ipKeyResolver}"
                  redis-rate-limiter.replenishRate: 1
                  redis-rate-limiter.burstCapacity: 1
            //用户微服务
            - id: changgou_user_route
              uri: lb://user
              predicates:
              - Path=/api/user/**,/api/address/**,/api/areas/**,/api/cities/**,/api/provinces/**
              filters:
              - StripPrefix=1

  application:
    name: gateway-web
  //Redis配置
  redis:
    host: 192.168.211.132
    port: 6379

server:
  port: 8001
eureka:
  client:
    service-url:
      defaultZone: http://127.0.0.1:7001/eureka
  instance:
    prefer-ip-address: true
management:
  endpoint:
    gateway:
      enabled: true
    web:
      exposure:
        include: true
```







### 4.4.6. 会话保持

用户每次请求的时候，我们都需要获取令牌数据，方法有多重，可以在每次提交的时候，将数据提交到头文件中，也可以将数据存储到Cookie中，每次从Cookie中校验数据，还可以每次将令牌数据以参数的方式提交到网关，这里面采用Cookie的方式比较容易实现。



#### 4.4.6.1. 登录封装Cookie

修改user微服务，每次登录的时候，添加令牌信息到Cookie中，修改changgou-service-user的`com.changgou.user.controller.UserController`的`login`方法





#### 4.4.6.2. 过滤器获取令牌数据

每次在网关中通过过滤器获取Cookie中的令牌，然后对令牌数据进行解析，修改微服务网关changgou-gateway-web中的AuthorizeFilter，

登录后测试，可以识别用户身份，不登录无法识别。如下访问`http://localhost:8001/api/user`会携带令牌数据：

## 4.5. 运行与测试

### 4.5.1. 准备工作

[通用准备工作](#通用准备工作)

### 4.5.2. 运行

运行[EurekaServerApplication.java](./mall/mall-parent/mall-eureka/src/main/java/com/mall/EurekaServerApplication.java)，访问 [http://127.0.0.1:7001/](http://127.0.0.1:7001/)

运行[GoodsApplication.java](./mall/mall-parent/mall-service/mall-service-goods/src/main/java/com/mall/goods/GoodsApplication.java)，[Goods在线API文档](http://127.0.0.1:18081/swagger-ui.html)

运行[UserApplication.java](./mall/mall-parent/mall-service/mall-service-user/src/main/java/com/mall/user/UserApplication.java)，[User在线API文档](http://127.0.0.1:18087/swagger-ui.html)

运行[GatewayWebApplication.java](./mall/mall-parent/mall-gateway/mall-gateway-web/src/main/java/com/mall/gateway/GatewayWebApplication.java)，网关微服务要最后启动，注意清缓存

**测试**

先利用user的API新增一个用户数据方便测试，

```
username:yang
password:$2a$10$vkYrswEN6fUeZ2C80qV1Fuk6ysmBiN0OVAjGWxZgQgVnMyZQpo0RO（Bcrypt加密存储，解密后是yang）
```

测试下面的URL，获取临时JWT token，http://127.0.0.1:18087/user/login?password=yang&username=yang，在Response headers中获取临时的JWT token，以方便访问其他url资源。

```
authorization: eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJlYTI2YjRkNi1hOTNjLTRlOTUtYjZkZi05M2M4ZjhlYTFhYmUiLCJzdWIiOiJ7XCJ1c2VySW5mb1wiOntcImlkXCI6XCIxXCIsXCJpc0RlbGV0ZVwiOjAsXCJuYW1lXCI6XCJ5YW5nXCIsXCJwYXNzd29yZFwiOlwiJDJhJDEwJHZrWXJzd0VONmZVZVoyQzgwcVYxRnVrNnlzbUJpTjBPVkFqR1d4WmdRZ1ZuTXlaUXBvMFJPXCIsXCJ1c2VybmFtZVwiOlwieWFuZ1wifSxcInJvbGVcIjpcIlVTRVJcIixcInN1Y2Nlc3NcIjpcIlNVQ0NFU1NcIn0iLCJpc3MiOiJhZG1pbiIsImlhdCI6MTYwOTk1MDQ2NiwiZXhwIjoxNjA5OTU0MDY2fQ.qSIfTy4wqHVQW1biZNr7y-g9HcmksKu5tQTN6l2sLcQ 
 connection: keep-alive 
 content-type: application/json 
 date: Wed, 06 Jan 2021 16:27:46 GMT 
 keep-alive: timeout=60 
 transfer-encoding: chunked 
 vary: Origin, Access-Control-Request-Method, Access-Control-Request-Headers
```

然后尝试通过spring-cloud-gateway网关技术，访问其他微服务的资源，其中的Authorization字段可以放在path，header，或者session中，具体可以通过代码修改。利用PostMan测试的时候注意

```
http://127.0.0.1:8001/api/sku/100000006163?Authorization=eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIzZmY3OGYwZS0yMmExLTQ4MzgtYWY5Mi04MTNkY2JlYzQ1MzMiLCJzdWIiOiJ7XCJ1c2VySW5mb1wiOntcImlkXCI6XCIxXCIsXCJpc0RlbGV0ZVwiOjAsXCJuYW1lXCI6XCJ5YW5nXCIsXCJwYXNzd29yZFwiOlwiJDJhJDEwJHZrWXJzd0VONmZVZVoyQzgwcVYxRnVrNnlzbUJpTjBPVkFqR1d4WmdRZ1ZuTXlaUXBvMFJPXCIsXCJ1c2VybmFtZVwiOlwieWFuZ1wifSxcInJvbGVcIjpcIlVTRVJcIixcInN1Y2Nlc3NcIjpcIlNVQ0NFU1NcIn0iLCJpc3MiOiJhZG1pbiIsImlhdCI6MTYwOTk1MjQ4NCwiZXhwIjoxNjA5OTU2MDg0fQ.ls8jaT3ypYl-v8Dkf8pnHZp9hhgQ1YqAEMrc0a-wn4I
```

最后访问成功JWT生效，通过Gateway访问其他微服务的资源也生效

# 5. Spring Security Oauth2 JWT

## 5.1. 学习目标

- 用户认证分析

- 认证技术方案了解

- ==SpringSecurity Oauth2.0入门==

  ```
  oauth2.0认证模式
  	授权码授权模式
  	密码授权模式
  授权流程
  ```

- ==用户授权认证开发==

## 5.2. 认证技术方案

### 5.2.1. 单点登录技术方案

分布式系统要实现单点登录，通常将认证系统独立抽取出来，并且将用户身份信息存储在单独的存储介质，比如： MySQL、Redis，考虑性能要求，通常存储在Redis中，如下图：

单点登录的特点是： 

```
1、认证系统为独立的系统。 
2、各子系统通过Http或其它协议与认证系统通信，完成用户认证。 
3、用户身份信息存储在Redis集群。
```

 Java中有很多用户认证的框架都可以实现单点登录：

```
 1、Apache Shiro. 
 2、CAS 
 3、Spring security CAS    
```



### 5.2.2. Oauth2认证

OAuth（开放授权）是一个开放标准，允许用户授权第三方移动应用访问他们存储在另外的服务提供者上的信息，而不需要将用户名和密码提供给第三方移动应用或分享他们数据的所有内容，OAuth2.0是OAuth协议的延续版本。

#### 5.2.2.1. Oauth2认证流程

第三方认证技术方案最主要是解决认证协议的通用标准 问题，因为要实现 跨系统认证，各系统之间要遵循一定的接口协议。
OAUTH协议为用户资源的授权提供了一个安全的、开放而又简易的标准。同时，任何第三方都可以使用OAUTH认证服务，任何服务提供商都可以实现自身的OAUTH认证服务，因而OAUTH是开放的。业界提供了OAUTH的多种实现如PHP、JavaScript，Java，Ruby等各种语言开发包，大大节约了程序员的时间，因而OAUTH是简易的。互联网很多服务如Open API，很多大公司如Google，Yahoo，Microsoft等都提供了OAUTH认证服务，这些都足以说明OAUTH标准逐渐成为开放资源授权的标准。
Oauth协议目前发展到2.0版本，1.0版本过于复杂，2.0版本已得到广泛应用。
参考：https://baike.baidu.com/item/oAuth/7153134?fr=aladdin
Oauth协议：https://tools.ietf.org/html/rfc6749
下边分析一个Oauth2认证的例子，黑马程序员网站使用微信认证的过程：

1.客户端请求第三方授权

用户进入黑马程序的登录页面，点击微信的图标以微信账号登录系统，用户是自己在微信里信息的资源拥有者。

点击“用QQ账号登录”出现一个二维码，此时用户扫描二维码，开始给黑马程序员授权。    

2.资源拥有者同意给客户端授权

资源拥有者扫描二维码表示资源拥有者同意给客户端授权，微信会对资源拥有者的身份进行验证， 验证通过后，QQ会询问用户是否给授权黑马程序员访问自己的QQ数据，用户点击“确认登录”表示同意授权，QQ认证服务器会 颁发一个授权码，并重定向到黑马程序员的网站。    

3.客户端获取到授权码，请求认证服务器申请令牌 此过程用户看不到，客户端应用程序请求认证服务器，请求携带授权码。 

4.认证服务器向客户端响应令牌 认证服务器验证了客户端请求的授权码，如果合法则给客户端颁发令牌，令牌是客户端访问资源的通行证。 此交互过程用户看不到，当客户端拿到令牌后，用户在黑马程序员看到已经登录成功。 

5.客户端请求资源服务器的资源 客户端携带令牌访问资源服务器的资源。 黑马程序员网站携带令牌请求访问微信服务器获取用户的基本信息。 

6.资源服务器返回受保护资源 资源服务器校验令牌的合法性，如果合法则向用户响应资源信息内容。 注意：资源服务器和认证服务器可以是一个服务也可以分开的服务，如果是分开的服务资源服务器通常要请求认证 服务器来校验令牌的合法性。    

Oauth2.0认证流程如下： 引自Oauth2.0协议rfc6749 https://tools.ietf.org/html/rfc6749    

Oauth2包括以下角色： 

1、客户端 本身不存储资源，需要通过资源拥有者的授权去请求资源服务器的资源，比如：畅购在线Android客户端、畅购在 线Web客户端（浏览器端）、微信客户端等。 

2、资源拥有者 通常为用户，也可以是应用程序，即该资源的拥有者。 

3、授权服务器（也称认证服务器） 用来对资源拥有的身份进行认证、对访问资源进行授权。客户端要想访问资源需要通过认证服务器由资源拥有者授 权后方可访问。 

4、资源服务器 存储资源的服务器，比如，畅购网用户管理服务器存储了畅购网的用户信息等。客户端最终访问资源服务器获取资源信息。    

#### 5.2.2.2. Oauth2在项目的应用

Oauth2是一个标准的开放的授权协议，应用程序可以根据自己的要求去使用Oauth2，本项目使用Oauth2实现如 下目标：

1、畅购访问第三方系统的资源 

2、外部系统访问畅购的资源 

3、畅购前端（客户端） 访问畅购微服务的资源。 

4、畅购微服务之间访问资源，例如：微服务A访问微服务B的资源，B访问A的资源。

### 5.2.3. Spring security Oauth2认证解决方案 

本项目采用 Spring security + Oauth2完成用户认证及用户授权，Spring security 是一个强大的和高度可定制的身份验证和访问控制框架，Spring security 框架集成了Oauth2协议。    

1、用户请求认证服务完成认证。 

2、认证服务下发用户身份令牌，拥有身份令牌表示身份合法。 

3、用户携带令牌请求资源服务，请求资源服务必先经过网关。 

4、网关校验用户身份令牌的合法，不合法表示用户没有登录，如果合法则放行继续访问。 

5、资源服务获取令牌，根据令牌完成授权。 

6、资源服务完成授权则响应资源信息。

## 5.3. Security Oauth2.0入门

### 5.3.1. Oauth2授权模式

#### 5.3.1.1. Oauth2授权模式

Oauth2有以下授权模式： 

```properties
1.授权码模式（Authorization Code）
2.隐式授权模式（Implicit） 
3.密码模式（Resource Owner Password Credentials） 
4.客户端模式（Client Credentials） 
```

其中授权码模式和密码模式应用较多，本小节介绍授权码模式。

## 5.4. 运行与测试

### 5.4.1. 运行

[通用准备工作](#通用准备工作)

运行[EurekaServerApplication.java](./mall/mall-parent/mall-eureka/src/main/java/com/mall/EurekaServerApplication.java)，访问 [http://127.0.0.1:7001/](http://127.0.0.1:7001/)

运行[OAuthApplication.java](./mall/mall-parent/mall-user-oauth/src/main/java/com/mall/OAuthApplication.java)，[OAuth在线API文档（需要验证账号密码：yanglin/yanglin）](http://127.0.0.1:9001/swagger-ui.html)

### 5.4.2. 测试授权码模式

#### 5.4.2.1. 申请授权码

访问下面的url，输入账号密码验证：**yanglin/yanglin**，获取授权码，例如：http://127.0.0.1/?code=EMimJc

```
client_id：客户端id，和授权配置类中设置的客户端id一致。 
response_type：授权码模式固定为code 
scop：客户端范围，和授权配置类中设置的scop一致。 
redirect_uri：跳转uri，当授权码申请成功后会跳转到此地址，并在后边带上code参数（授权码）

http://127.0.0.1:9001/oauth/authorize?client_id=yanglin&response_type=code&scop=app&redirect_uri=http://127.0.0.1
```

#### 5.4.2.2. 申请令牌

通过POSTMAN访问下面的URL

```
URL：http://127.0.0.1:9001/oauth/token
method：POST

Authorization（http Basic）->Basic Auth
	Username:changgou
	Password:changgou

Body->form-data
  grant_type：授权类型，填写authorization_code，表示授权码模式 
  code：授权码，就是刚刚获取的授权码，注意：授权码只使用一次就无效了，需要重新申请。 
  redirect_uri：申请授权码时的跳转url，一定和申请授权码时用的redirect_uri一致。 (http://127.0.0.1)
```

点击发送请求后获得下面的结果：

```json
{
    "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJhcHAiXSwibmFtZSI6bnVsbCwiaWQiOm51bGwsImV4cCI6MTYxMDU0ODMxNCwianRpIjoiOWRkYTgyZjctOTJmZi00NzRiLTllY2MtODQyZDAyM2M0MWIwIiwiY2xpZW50X2lkIjoiY2hhbmdnb3UiLCJ1c2VybmFtZSI6ImNoYW5nZ291In0.iCwHGRX3TcD0XF1DKjSy9GQrEmeiXvqekIYfosyor38c_kXB8ByqGWHubWXuMsYCQL0MNuyBNOxfpoatjQIKrFbEpKO7jRusMgE7mnCdQ4H35qgmDx68ILI0BMrLVvjbGXpYx4k6jMhOffN9ugddrP-DJZzQDUmeSxldUe1mvt4IVUMqzn08exhoBb2rztEcT24fI_M6NPItm48rSDWpA7emflN1b58PE4JfhVU_c_T81_gcntJLyvTkS6e7tMeNWEqzIy30RF7gCKVBbtOb8YEifzliqp4qYX-QLHQqmBsotyYrur_exYs7A1VpbBiJ5uTsYj2J-FjgZpWv_qLWCA",
    "token_type": "bearer",
    "refresh_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJhcHAiXSwiYXRpIjoiOWRkYTgyZjctOTJmZi00NzRiLTllY2MtODQyZDAyM2M0MWIwIiwibmFtZSI6bnVsbCwiaWQiOm51bGwsImV4cCI6MTYxMDU0ODMxNCwianRpIjoiMjVlOWQyMzItMjFjOS00NTQ0LTgyYjAtZDBkMDhhMmJiMzU5IiwiY2xpZW50X2lkIjoiY2hhbmdnb3UiLCJ1c2VybmFtZSI6ImNoYW5nZ291In0.S25RvLO9rcDbUw4WrISn47inyoO4CazqmNgbIhTct1OwtWqEivYSQcLW5zUxO8WZDVml2FKXK9gELldOQi0Fh1l0wu1HECGJ1lVL6CwZqnJCENLp2dnYmzwGK-E5y8qRA7jw8iQm4PUjEuniAckGBVxx0ZP2Pa4SOgHaM7mae8JYomqeE5OVJJIs6VVwQj6Adj6E0cqmwiLaiIBkBuh9ouWFrRrhO81ax6U9EB_hV0MMjNibgVw9Z5DPoOdntnX8ZCBqqWja_5ZNkGCfJI7-JGuU8-OmAlUgpahHxpjivG60L72ou-FB2MtRYlTMZHE1bfB-z__dzzVzwNaSbWxh2A",
    "expires_in": 3599,
    "scope": "app",
    "jti": "9dda82f7-92ff-474b-9ecc-842d023c41b0"
}


access_token：访问令牌，携带此令牌访问资源，访问各大微服务的令牌
token_type：有MAC Token与Bearer Token两种类型，两种的校验算法不同，RFC 6750建议Oauth2采用 Bearer Token（http://www.rfcreader.com/#rfc6750）。 
refresh_token：刷新令牌，使用此令牌可以延长访问令牌的过期时间。 
expires_in：过期时间，单位为秒。 
scope：范围，与定义的客户端范围一致。    
jti：当前token的唯一标识
```

#### 5.4.2.3. 校验令牌

使用POSTMAN校验令牌。

```
Get: http://localhost:9001/oauth/check_token?token=[access_token]

Authorization（http Basic）->Basic Auth
	Username:yanglin
	Password:yanglin
```

成功的话返回下面的信息，说明token还未过期。

```json
{
    "scope": [
        "app"
    ],
    "name": null,
    "active": true,
    "id": null,
    "exp": 1610557527,
    "jti": "a2b70a98-d8fb-4dd8-b9a6-49be607b5d2b",
    "client_id": "yanglin",
    "username": "yanglin"
}

exp：过期时间，long类型，距离1970年的秒数（new Date().getTime()可得到当前时间距离1970年的毫秒数）。 
username： 用户名 
client_id：客户端Id，在oauth_client_details中配置 
scope：客户端范围，在oauth_client_details表中配置 
jti：与令牌对应的唯一标识 companyId、userpic、name、utype、
id：这些字段是本认证服务在Spring Security基础上扩展的用户身份信息    
```

失败的话返回下面的信息，说明令牌过期

```json
{
    "error": "invalid_token",
    "error": "Token has expired"
}
```

#### 5.4.2.4. 刷新令牌

刷新令牌是当令牌快过期时重新生成一个令牌，它于授权码授权和密码授权生成令牌不同，刷新令牌不需要授权码 也不需要账号和密码，只需要一个刷新令牌、客户端id和客户端密码。

测试如下：  

```
Post：http://localhost:9001/oauth/token

参数：    
Body->form-data
  grant_type： 固定为 refresh_token 
  refresh_token：刷新令牌（注意不是access_token，而是refresh_token）   
```

成功的话，返回下面的信息

```json
{
    "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJhcHAiXSwiZXhwIjoxNjEwNTU3NjM0LCJqdGkiOiIxNmU1YzQzNi1kNmI3LTQ2YjMtYTBiMC0yZDliNzhiNGVmZGEiLCJjbGllbnRfaWQiOiJ5YW5nbGluIn0.AbR2IUw25laGr117Kg-rWSD4TlWkEz8rkiGdobbun-a1u3WLu2kGaCMzFPJgo2IKg_kSz_ZuLC-L74UqtIH_WS97iz65nsQZzkY3DDzhGkHsgPBloiMKcwgNf14YgxN2E9YvloRfp7KaaQLD8mHsx5wTA3-y2PeUs7XPUs9yknz9c3i8WFrRGeVGySGa8RsiVWOAV-28FBa9Cn4fM3CCHxCK_kTqeOES8QXt1BiONe3-Lg0dP2yG2jy2vi1KBHVk3CjcbhMN5Dh0mKnVdwcO8usXwRDzh-RFKd1gw5nZkR3jKjqhPuuMDY3t7PAZalk3AbDLuz59nZCLEwXGL_Ms3Q",
    "token_type": "bearer",
    "refresh_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJhcHAiXSwiYXRpIjoiMTZlNWM0MzYtZDZiNy00NmIzLWEwYjAtMmQ5Yjc4YjRlZmRhIiwiZXhwIjoxNjEwNTU3NTI3LCJqdGkiOiJlNzI2ZTMxOC03ZTJhLTQ0NjMtOTc5Zi0zN2NjYmNmZDI0MjEiLCJjbGllbnRfaWQiOiJ5YW5nbGluIn0.K_XJBWyuIcuK7k-QvUh5hrSyG8Tn1_gpPpy_kE7-9idW6oir_bUYkaOF6GcLIOo3JddwQaynvmDh7kS8wmnBttItCVUUDGUp2NKwQHaQqxj2K5NrPeX2zplejwmcxDTwIcydJXcht6jndWJsEe0c8mR4dHplme9icKizz71ASLeaMpLQCxbGpNa_2IH0iIFHK8bvg31xAdrgOUfEbngTYc_6YdmqCcnb9pbwc8bGbbTXwPQcKPh7iJrn5zcTXbnZEqerLAoeLCZ7wkopD6bMD315k7IBhMG0VWB8q3KtFI5_TFl3KeeYI_AaqW2uji-piX1oQjYMJQUIXZFYGmWC6g",
    "expires_in": 3599,
    "scope": "app",
    "jti": "16e5c436-d6b7-46b3-a0b0-2d9b78b4efda"
}
```

**注意**：以前的令牌还是会起作用，所以，令牌的周期不要设置太长，太长不安全。

### 5.4.3. 测试密码模式

密码模式（Resource Owner Password Credentials）与授权码模式的区别是申请令牌不再使用授权码，而是直接 通过用户名和密码即可申请令牌。 

测试如下： 

```
Url：http://localhost:9001/oauth/token
Method：Post

Authorization（http Basic）->Basic Auth
	Username:yanglin
	Password:yanglin

Body->form-data
  grant_type：密码模式授权填写 password 
  username：账号(随便写)
  password：密码(一定是 yanglin，在代码中固定的，后面可以改从数据库获取)


密码设置在下面这段代码中：
String pwd = new BCryptPasswordEncoder().encode("yanglin");
```

成功后返回下面信息，token信息是可以用的。

```json
{
    "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJhcHAiXSwibmFtZSI6bnVsbCwiaWQiOm51bGwsImV4cCI6MTYxMDU1MjMxNiwiYXV0aG9yaXRpZXMiOlsic2Vja2lsbF9saXN0IiwiZ29vZHNfbGlzdCJdLCJqdGkiOiI5NGJhYTNiMC05NTExLTQ3ZGYtODk2MS1mNWUxMDI5MTM4NjYiLCJjbGllbnRfaWQiOiJjaGFuZ2dvdSIsInVzZXJuYW1lIjoieWFuZ2xpbiJ9.ZWxIRMtSJCDhSD0Sb6HfRQ9UGHrpBzQJY2N_BeC4WrteqZEw_u-hQPYEPmB5Ac7q2_3bec8BROvB7rFKVJdm3dteIM3vbMcK4yJt8Ocsgktrfrik2ZAcG6AeoSCU2ZXiPjHFGMf6kl3xbfkJL2aVVybOtyDk03wcJPHEe23f8v3KqW4kHd-uOz5ZzdXSAlCsJOVk36-iGuBQyMxj-m7MXh1nCDg4fUpYHR3sjDHtj6dxLtH8isl0qhdVNXvXphVgC0_ke_DVoPSBgjIROXH-0nwnpKPjTVxqExEOzBm6j72gHkHUlt6lkT2TuuG7mGcMbdL9Dd2hdXL1w6kCjLh24A",
    "token_type": "bearer",
    "refresh_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJhcHAiXSwiYXRpIjoiOTRiYWEzYjAtOTUxMS00N2RmLTg5NjEtZjVlMTAyOTEzODY2IiwibmFtZSI6bnVsbCwiaWQiOm51bGwsImV4cCI6MTYxMDU1MjMxNiwiYXV0aG9yaXRpZXMiOlsic2Vja2lsbF9saXN0IiwiZ29vZHNfbGlzdCJdLCJqdGkiOiJkMjI3ZDc0Zi1mMjFlLTQ5OTctODM0Ny0wNjA5MWJkYWZiM2MiLCJjbGllbnRfaWQiOiJjaGFuZ2dvdSIsInVzZXJuYW1lIjoieWFuZ2xpbiJ9.Z-LWgvZq68BME0TQS4cel0vSFwLrDED_oIRjdHVUvLd8RsSDtwQdTD6f_VhrvsKpF5gw_rZMiWIC78i-EnzXPcwJ8sULmtS2YaUEAj1s6YEaKhLPuBSG9Bi43P4gszti6h82Ut0RbFGVIevEnwL9kRWlBiee_hEZwAcgiQGONcStVwMFc7cdVih2DWI8OQRApVqHIW3yMnlApMFHIYGdoCwANq66MORRmAZXp9FaWLuEz4N3jp5tEESd2AxZqtFUjXLZ72uUmqIuggRXnfzuEwYDjloJ9IJpMiYXTX4O5DkwskiMiob6i7jhey3nQhbxwsf4knBDl2IK7f9lbmH5VQ",
    "expires_in": 3599,
    "scope": "app",
    "jti": "94baa3b0-9511-47df-8961-f5e102913866"
}
```

### 5.4.4. 生成私钥公钥

#### 5.4.4.1. 生成私钥

Spring Security 提供对JWT的支持，本节我们使用Spring Security 提供的JwtHelper来创建JWT令牌，校验JWT令牌 等操作。 这里JWT令牌我们采用非对称算法进行加密，所以我们要先生成公钥和私钥。

(1)生成密钥证书，下边命令生成密钥证书，采用RSA 算法每个证书包含公钥和私钥 

创建一个文件夹，在该文件夹下执行如下命令行（生成证书：包含公钥、私钥，他们是一对）：

```properties
keytool -genkeypair -alias yanglin -keyalg RSA -keypass yanglin -keystore yanglin.keystore -storepass yanglin
======================================================================
yanglin@yanglindeMacBook-Pro resources % keytool -genkeypair -alias yanglin -keyalg RSA -keypass yanglin -keystore yanglin.keystore -storepass yanglin
What is your first and last name?
  [Unknown]:  yang
What is the name of your organizational unit?
  [Unknown]:  no
What is the name of your organization?
  [Unknown]:  no
What is the name of your City or Locality?
  [Unknown]:  ShangHai
What is the name of your State or Province?
  [Unknown]:  China
What is the two-letter country code for this unit?
  [Unknown]:  CN
Is CN=yang, OU=no, O=no, L=ShangHai, ST=China, C=CN correct?
  [no]:  y

```

Keytool 是一个java提供的证书管理工具 

```properties
-alias：密钥的别名 
-keyalg：使用的hash算法 
-keypass：密钥的访问密码(加载需要这个密码)
-keystore：密钥库文件名，xc.keystore保存了生成的证书 
-storepass：密钥库的访问密码(解析里面的数据信息需要这个密码)
```

```
keystore 是Eclipse 打包生成的签名。
jks是Android studio 生成的签名。
都是用来打包的，并保证应用的唯一性，这就是他们的最大的区别！
备注:很多第三方市场,我们上传apk的时候,他们只支持keystore,需要我们把.jks签名转化为.keystore!
```

(2)查询证书信息

```properties
keytool -list -keystore yanglin.keystore
======================================================================
yanglin@yanglindeMacBook-Pro resources % keytool -list -keystore yanglin.keystore
Enter keystore password:  
Keystore type: PKCS12
Keystore provider: SUN

Your keystore contains 1 entry

yanglin, Jan 13, 2021, PrivateKeyEntry, 
Certificate fingerprint (SHA-256): D1:3B:CE:EE:38:14:A2:25:45:43:BF:52:8E:A9:EA:B0:C7:78:41:C4:5D:57:00:14:94:81:4A:47:79:1F:49:4E
```

(3)删除别名 

```properties
keytool -delete -alias yanglin -keystore yanglin.keystore
```

#### 5.4.4.2. opensl 从私钥导出不带签名的公钥

用下面的命令导出私钥

```properties
keytool -list -rfc --keystore yanglin.keystore | openssl x509 -inform pem -pubkey
======================================================================
yanglin@yanglindeMacBook-Pro resources % keytool -list -rfc --keystore yanglin.keystore | openssl x509 -inform pem -pubkey
Enter keystore password:  yanglin
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqbNgffXW03mr64Cs3PHA
OIvXWmiXRW2aMuC+AwrDJCgLGBNj0BcjIGQY2UmWQHGQkWZgW2w3B2kf5viqSwHX
0iGE5qZJfTQrEulMA1DJ/oS60oxcKNEMzDulsOiPZJA/U9PszHNl9eigs6RwK0B4
GIfqR3ImrUxrASLNHsI5vA29NboZ30JWoylVbKliwiUv+ZAz9oTWt7vT4rZV1u/9
o2DDxG9K6Cx1ZXIZR3bUx3YEMQeUma5rquQ7BeyCXs2yb8TJ6RkISmq9PEzQEwFM
p8jtvBrnodAr/6mKFpTxUO5F1VXUuotDXooXNhpxwJY6oMJ//zZaw94j4k8aY7bW
/QIDAQAB
-----END PUBLIC KEY-----
-----BEGIN CERTIFICATE-----
MIIDUTCCAjmgAwIBAgIEc0prkTANBgkqhkiG9w0BAQsFADBZMQswCQYDVQQGEwJD
TjEOMAwGA1UECBMFQ2hpbmExETAPBgNVBAcTCFNoYW5nSGFpMQswCQYDVQQKEwJu
bzELMAkGA1UECxMCbm8xDTALBgNVBAMTBHlhbmcwHhcNMjEwMTEzMTUzMzUzWhcN
MjEwNDEzMTUzMzUzWjBZMQswCQYDVQQGEwJDTjEOMAwGA1UECBMFQ2hpbmExETAP
BgNVBAcTCFNoYW5nSGFpMQswCQYDVQQKEwJubzELMAkGA1UECxMCbm8xDTALBgNV
BAMTBHlhbmcwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCps2B99dbT
eavrgKzc8cA4i9daaJdFbZoy4L4DCsMkKAsYE2PQFyMgZBjZSZZAcZCRZmBbbDcH
aR/m+KpLAdfSIYTmpkl9NCsS6UwDUMn+hLrSjFwo0QzMO6Ww6I9kkD9T0+zMc2X1
6KCzpHArQHgYh+pHciatTGsBIs0ewjm8Db01uhnfQlajKVVsqWLCJS/5kDP2hNa3
u9PitlXW7/2jYMPEb0roLHVlchlHdtTHdgQxB5SZrmuq5DsF7IJezbJvxMnpGQhK
ar08TNATAUynyO28Gueh0Cv/qYoWlPFQ7kXVVdS6i0Neihc2GnHAljqgwn//NlrD
3iPiTxpjttb9AgMBAAGjITAfMB0GA1UdDgQWBBRMC4/BMXNyOhB9RlgQtHzyU986
ijANBgkqhkiG9w0BAQsFAAOCAQEABqmHxgGZRw7bnQbKzP80abTKSTKvrZET/wNw
ENi68GkfPMDRCVG2pfoUkrGJXlZA5gHfwK8uEhGDBpnSOuOUddtXv/Z8wixUSuvF
gzRqrSmElS7ZQazcnX8U9/t6FD/BlI/mq05UQIXwsr0n8wHukynmFNyYTDoj+Ve0
SdgAkNVnBDvTzNUlBE2VMqnDAZ4Hb3GymHR7SjeQbGS7s9/O55x98UH17iWP9bsI
458HhQgd6VkG0nVMGFGmVPmJbKe+aWIVyB3X/EKrbA3ss1cXtwoM/sHDZE+shV08
ZF9UywBnFDk3AoZOSRrmjct8TIYZg56Q6RmPOG/eYpQ9nLjLRw==
-----END CERTIFICATE-----

```

复制PUBLIC KEY信息，整成一行，保存在public.key文件中，这时public.key是不带签名的公钥。

```properties
-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqbNgffXW03mr64Cs3PHAOIvXWmiXRW2aMuC+AwrDJCgLGBNj0BcjIGQY2UmWQHGQkWZgW2w3B2kf5viqSwHX0iGE5qZJfTQrEulMA1DJ/oS60oxcKNEMzDulsOiPZJA/U9PszHNl9eigs6RwK0B4GIfqR3ImrUxrASLNHsI5vA29NboZ30JWoylVbKliwiUv+ZAz9oTWt7vT4rZV1u/9o2DDxG9K6Cx1ZXIZR3bUx3YEMQeUma5rquQ7BeyCXs2yb8TJ6RkISmq9PEzQEwFMp8jtvBrnodAr/6mKFpTxUO5F1VXUuotDXooXNhpxwJY6oMJ//zZaw94j4k8aY7bW/QIDAQAB-----END PUBLIC KEY-----
```

**修改配置文件适应新证书，测试后标明授权码模式和密码模式，私钥证书是生效的，说明生成私钥成功并且有效**

#### 5.4.4.3. 同时测试生成和解析令牌

[生成JWT令牌CreateJwtTest.java](./mall/mall-parent/mall-user-oauth/src/test/java/com/mall/token/CreateJwtTest.java)

[解析JWT令牌ParseJwtTest.java](./mall/mall-parent/mall-user-oauth/src/test/java/com/mall/token/ParseJwtTest.java)

### 5.4.5. 用户密码登录测试

[UserJwt-丰富载荷信息](./mall/mall-parent/mall-user-oauth/src/main/java/com/mall/oauth/util/UserJwt.java)

运行[GatewayWebApplication.java](./mall/mall-parent/mall-gateway/mall-gateway-web/src/main/java/com/mall/gateway/GatewayWebApplication.java)。

运行[UserApplication.java](./mall/mall-parent/mall-service/mall-service-user/src/main/java/com/mall/user/UserApplication.java)。

运行[OAuthApplication.java](./mall/mall-parent/mall-user-oauth/src/main/java/com/mall/OAuthApplication.java)，[OAuth在线API文档（需要验证账号密码：yanglin/yanglin）](http://127.0.0.1:9001/swagger-ui.html)

测试下面的URL，用于用户直接用账号和密码（**从mall_user查询和修改账号和密码**）登录获取令牌

```json
curl -X GET "http://127.0.0.1:9001/user/login?password=yang&username=yang" -H "accept: */*"
```

返回下面的信息

```json
{
  "massage": "令牌生成成功",
  "authToken": {
    "accessToken": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJhcHAiXSwibmFtZSI6bnVsbCwiaWQiOm51bGwsImV4cCI6MTYxMDU2NDE2NiwiYXV0aG9yaXRpZXMiOlsic2Vja2lsbF9saXN0IiwiZ29vZHNfbGlzdCJdLCJqdGkiOiJlMDhhMDcwZS1jYjI3LTRkYzMtOWRmZi1iNWI1NjZkMjQwOGQiLCJjbGllbnRfaWQiOiJ5YW5nbGluIiwidXNlcm5hbWUiOiJ5YW5nbGluIn0.S_tUsImaMgvwuKgdV7KGkyrPXoGWxucElfNSk3mJ1F5_J5EcUm4KH_Fsprcjf25R6kCiIZlf80BkwrvSrX-3JPNHwZbp3B9wvXbSRG4liwD832-ACEwC6CwmrubY55QDLJ96CbbERpL0QFWZxP3-HuiHw8cqLH63nhen-oiGzw-V9oWNwK5Tl3dFGV4Fj5RZst-frHQY0C9fTZGmapDGq3S1_tvx_BTtgK7nkPvNVb24r6wZMLNa10Ckx4tylp3f_Xmyo0I6fXnv_lMgc8KGTAoVwRQD5yOXWOG4ughlcJOdQOP2nR_wG2MG9U7l2XcTU9KD4KzoHjVQ7r33vBH5WQ",
    "tokenType": "bearer",
    "refreshToken": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJhcHAiXSwiYXRpIjoiZTA4YTA3MGUtY2IyNy00ZGMzLTlkZmYtYjViNTY2ZDI0MDhkIiwibmFtZSI6bnVsbCwiaWQiOm51bGwsImV4cCI6MTYxMDU2NDE2NiwiYXV0aG9yaXRpZXMiOlsic2Vja2lsbF9saXN0IiwiZ29vZHNfbGlzdCJdLCJqdGkiOiI0ZmM0YWZmNi0zZjU3LTQ0NWQtOGI5NC01YjNlZDIzOTQ0OGUiLCJjbGllbnRfaWQiOiJ5YW5nbGluIiwidXNlcm5hbWUiOiJ5YW5nbGluIn0.p87o64rAs07wPZ4lboZF-3KDUkAgU9rN5NiFbg9GQARp-ozvirebBXXQAYV5_OAbNDQf4cIQ9pCM-Er-GV60yutIsObJ4suzUInwZeKV5yMIOCoCrScquZ-7NfQunJRCZ8ztgC_n5xmwZpzy9VbBZUHzOrZzqAKiH-LUd80JIJv1eJfAYvRyDTA6BhLXC0WS1dsmXG9yspgkMhPcDScODCAsqJRt3zcv_HIFtMFXjBhzhd7_rOfIOQ6kyeCdlR1NKzBhX97iOYKKPBR0fpVjB7OKe6Cl-uoS1jwwPYsz8Yvimz0HV2CgJO8TrC02UVkLbx35ks1jbeJgJ38xeaDgjg",
    "expiresIn": 3599,
    "scope": "app",
    "jti": "e08a070e-cb27-4dc3-9dff-b5b566d2408d"
  }
}
```

# 6. 购物车

## 6.1. 学习目标

- 资源服务器授权配置
- 掌握OAuth认证微服务动态加载数据
- 掌握购物车流程
- 掌握购物车渲染流程
- OAuth2.0认证并获取用户令牌数据
- 微服务与微服务之间的认证



## 6.2. 运行与测试

### 6.2.1. 运行

[通用准备工作](#通用准备工作)

运行[EurekaServerApplication.java](./mall/mall-parent/mall-eureka/src/main/java/com/mall/EurekaServerApplication.java)，访问 [http://127.0.0.1:7001/](http://127.0.0.1:7001/)

运行[OAuthApplication.java](./mall/mall-parent/mall-user-oauth/src/main/java/com/mall/OAuthApplication.java)，[OAuth在线API文档（需要验证账号密码：yanglin/yanglin）](http://127.0.0.1:9001/swagger-ui.html)

将上面生成的公钥public.key拷贝到需要设置资源权限微服务工程的resources目录下，同时复制[ResourceServerConfig.java](./mall/mall-parent/mall-service/mall-service-user/src/main/java/com/mall/user/config/ResourceServerConfig.java)到微服务当中，例如用户微服务

运行[UserApplication.java](./mall/mall-parent/mall-service/mall-service-user/src/main/java/com/mall/user/UserApplication.java)，[User在线API文档](http://127.0.0.1:18087/swagger-ui.html)

### 6.2.2. 测试

需要在user-auth，user两个数据库中的oauth_client_details添加信息。

```
client_id:yanglin
client_secret:$2a$10$ThJt7f/UCUnRv6.WzuDpReJ4y5yGUvZp5u14Ph.6n/dax/bRkoica
authorized_grant_types:password
scope:app
web_server_redirect_uri:http://127.0.0.1

使用的加密代码：
new BCryptPasswordEncoder().encode(clientSecret)
```

访问[OAuth在线API文档（需要验证账号密码：yang/yang）](http://127.0.0.1:9001/swagger-ui.html)，输入账号密码获取令牌

```
curl -X GET "http://127.0.0.1:9001/user/login?password=yang&username=yang" -H "accept: */*"
```

```json
{
  "massage": "令牌生成成功",
  "authToken": {
    "accessToken": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJhcHAiXSwibmFtZSI6bnVsbCwiaWQiOm51bGwsImV4cCI6MTYxMDYzODQ5OCwiYXV0aG9yaXRpZXMiOlsic2Vja2lsbF9saXN0IiwiZ29vZHNfbGlzdCJdLCJqdGkiOiIwYTY0MzFhYS03ODAzLTRiMmMtYTBiMC1kZTJiNTM3MGFlOTEiLCJjbGllbnRfaWQiOiJ5YW5nbGluIiwidXNlcm5hbWUiOiJ5YW5nbGluIn0.D25w3QYdfJzbMkBsvcoBmDVPSPpe_RCO6tnPJaR8CalDYbr0GSXuFaHMst1-_laFw1SfeDJPvXOTv0-cRKfGVeK0F4mAbfsqf5_KoxzZeIpIV7YbBIrevTamdPPs3CgN2W4zdSRPOXBkLoexb-CMejFvJw6Hkskp45qTY9WthIsjsd72xKt4V5s7vdNnTCJWsFIbhtAc69K0vr_sUb-kyMM8Zr9_-N_XecJ8SkkXD3GQM5hx8ABaPc_n2TCsU6AIxBzC1moCy1wBlX24TqnCpMkMkjbrg46JnsRsc4gEGCqiCEstGUMX2xRAaeeHIaOXEENi4-c-ZcckfoChRyqsWw",
    "tokenType": "bearer",
    "refreshToken": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJhcHAiXSwiYXRpIjoiMGE2NDMxYWEtNzgwMy00YjJjLWEwYjAtZGUyYjUzNzBhZTkxIiwibmFtZSI6bnVsbCwiaWQiOm51bGwsImV4cCI6MTYxMDYzODQ5OCwiYXV0aG9yaXRpZXMiOlsic2Vja2lsbF9saXN0IiwiZ29vZHNfbGlzdCJdLCJqdGkiOiIwOTg5Y2I5OC1jNGI5LTRlYjEtYWIwMS0yZjJiNmQ0YmFkM2QiLCJjbGllbnRfaWQiOiJ5YW5nbGluIiwidXNlcm5hbWUiOiJ5YW5nbGluIn0.J3ugyf2Mer_coxvz9JVzHfSFxT-kTTW4pdUePRTsornbMdX-8pMPxiy7O6sJdqWigqt1NR-xpDcpoQV1v-GPrFGcgT3QaC_Aq11-TW6i9soGDausaRe5t7ylieIzC6gtHosiAedEdvrq-VVqP4dqIpD9j8FMfb3gDNH0fh0X5fhzSA9nCejXIpl6X_QLSrVqbIxsQ_ZINJoFrCumW376nXTs6-K6Rm3rBQ5Ex31lUXvxnwHGf1bUPIVlQxdsQSyNRBe3Lyd9HOB2uSoJkuUzgre3hc1oB9zYTggdjdBeJ9-2ySFDQP8xOdc6zMk9LqVg44KvsQpMbi6b1v3qO380SQ",
    "expiresIn": 3599,
    "scope": "app",
    "jti": "0a6431aa-7803-4b2c-a0b0-de2b5370ae91"
  }
}
```

获取令牌后，访问[User在线API文档](http://127.0.0.1:18087/swagger-ui.html)，在Swagger右上角位置输入“bear ”+token，如下：

```
bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJhcHAiXSwibmFtZSI6bnVsbCwiaWQiOm51bGwsImV4cCI6MTYxMDYzNDQwOSwiYXV0aG9yaXRpZXMiOlsic2Vja2lsbF9saXN0IiwiZ29vZHNfbGlzdCJdLCJqdGkiOiI5ZTgzNDM4Yi1lNTA2LTRmMzEtODUyNC1hZDA0YzJmMDkwODciLCJjbGllbnRfaWQiOiJ5YW5nbGluIiwidXNlcm5hbWUiOiJ5YW5nbGluIn0.o7GMHf09LnbSyCAvWiVwNUpzyAFw9nPGqA6AlksbT4mdpazbK0nW3a0mPSruLdiTc11V1lZOIvK_kNqYkZWoAki9xxWBOGRfA_ZEqrgg86VeL5pi0hkKR892DFce-VgHbwkBFQCzRoRmZG8lXR5zUc1ipYuHl1Ui6g0jkjQSKMwOm8GJu5S8_cgiO_5o1hYJRSnv4WcDAZPaiMOie-Cj_MtryGytX_me_j0evCsjIeHV1Ca9Fgucs_SB8d_aEWwX2AoN75TyjzcslLgY-JUaCwccyQpB7H2BUejifCHDqlvzXus1EKhi73xfxwUemmDONiOEI20ALXhrEl1ApCYoJg
```

接着访问任一一个资源，比如，用户添加，即可正常返回数据，同时注意用户权限的动态配置。

```
curl -X POST "http://127.0.0.1:18087/user" -H "accept: */*"
```

### 6.2.3. 测试授权认证设置微服务网关（GateWay）

访问[OAuth在线API文档（弹出客户端认证窗口，需要验证账号密码：yanglin/yanglin）](http://127.0.0.1:9001/swagger-ui.html)，输入账号密码获取令牌

```
curl -X GET "http://127.0.0.1:9001/user/login?password=yang&username=yang" -H "accept: */*"
//这个账号密码是mall_user数据库中的user表中的数据
```

```json
{
  "massage": "令牌生成成功",
  "authToken": {
    "accessToken": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJhcHAiXSwibmFtZSI6bnVsbCwiaWQiOm51bGwsImV4cCI6MTYxMDYzODQ5OCwiYXV0aG9yaXRpZXMiOlsic2Vja2lsbF9saXN0IiwiZ29vZHNfbGlzdCJdLCJqdGkiOiIwYTY0MzFhYS03ODAzLTRiMmMtYTBiMC1kZTJiNTM3MGFlOTEiLCJjbGllbnRfaWQiOiJ5YW5nbGluIiwidXNlcm5hbWUiOiJ5YW5nbGluIn0.D25w3QYdfJzbMkBsvcoBmDVPSPpe_RCO6tnPJaR8CalDYbr0GSXuFaHMst1-_laFw1SfeDJPvXOTv0-cRKfGVeK0F4mAbfsqf5_KoxzZeIpIV7YbBIrevTamdPPs3CgN2W4zdSRPOXBkLoexb-CMejFvJw6Hkskp45qTY9WthIsjsd72xKt4V5s7vdNnTCJWsFIbhtAc69K0vr_sUb-kyMM8Zr9_-N_XecJ8SkkXD3GQM5hx8ABaPc_n2TCsU6AIxBzC1moCy1wBlX24TqnCpMkMkjbrg46JnsRsc4gEGCqiCEstGUMX2xRAaeeHIaOXEENi4-c-ZcckfoChRyqsWw",
    "tokenType": "bearer",
    "refreshToken": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJhcHAiXSwiYXRpIjoiMGE2NDMxYWEtNzgwMy00YjJjLWEwYjAtZGUyYjUzNzBhZTkxIiwibmFtZSI6bnVsbCwiaWQiOm51bGwsImV4cCI6MTYxMDYzODQ5OCwiYXV0aG9yaXRpZXMiOlsic2Vja2lsbF9saXN0IiwiZ29vZHNfbGlzdCJdLCJqdGkiOiIwOTg5Y2I5OC1jNGI5LTRlYjEtYWIwMS0yZjJiNmQ0YmFkM2QiLCJjbGllbnRfaWQiOiJ5YW5nbGluIiwidXNlcm5hbWUiOiJ5YW5nbGluIn0.J3ugyf2Mer_coxvz9JVzHfSFxT-kTTW4pdUePRTsornbMdX-8pMPxiy7O6sJdqWigqt1NR-xpDcpoQV1v-GPrFGcgT3QaC_Aq11-TW6i9soGDausaRe5t7ylieIzC6gtHosiAedEdvrq-VVqP4dqIpD9j8FMfb3gDNH0fh0X5fhzSA9nCejXIpl6X_QLSrVqbIxsQ_ZINJoFrCumW376nXTs6-K6Rm3rBQ5Ex31lUXvxnwHGf1bUPIVlQxdsQSyNRBe3Lyd9HOB2uSoJkuUzgre3hc1oB9zYTggdjdBeJ9-2ySFDQP8xOdc6zMk9LqVg44KvsQpMbi6b1v3qO380SQ",
    "expiresIn": 3599,
    "scope": "app",
    "jti": "0a6431aa-7803-4b2c-a0b0-de2b5370ae91"
  }
}
```

利用POSTMan测试GateWay网关对接是否成功，测试下面的URL（其中加上上面的accessToken），其中Authorization可以放在header

```
http://127.0.0.1:8001/api/sku/100000006163?Authorization=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJhcHAiXSwibmFtZSI6bnVsbCwiaWQiOm51bGwsImV4cCI6MTYxMDY1MjUwNywiYXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiIsIlJPTEVfVVNFUiJdLCJqdGkiOiJiNGY3YzIyMy02MTU0LTRmNmUtYWU5Ny01YTM2ODdiYjRmNjUiLCJjbGllbnRfaWQiOiJ5YW5nbGluIiwidXNlcm5hbWUiOiJ5YW5nbGluIn0.iDxnf9tlW63gRnaRErrly2KtXs0TeGecT0kN-_Vyyl7osmB6Y6X1UIJ2OOzypBXQ4zCVaYGq5VJZ5Xftd3txVpFS8ddkUweVF8IDL5UnlbZw_fSZx_i1q3aENUGjV8WdhVj0ZYOynKSrMG8oJCP0eRG4wuQKqmzQ_1Gu0qXrGQWH6it6gdmHBvekv0BixFIYI0tvVKNt0ZikmVKn0vQ76k5U6B9nqtL2C5M8mv7alImtroQaSgOs7ek-5eNYNYRSAcSU90DqottFIqh9BXJfRCo6Kha40s6H5hDOLrXbqfEw6j-z_tQ_HrvDecNouE5kXACW1Grm1hRxmDDa1D4CWg
```

成功后返回数据！

### 6.2.4. 测试数据库加载客户端ID和密码是否成功

需要在user-auth，user两个数据库中的oauth_client_details添加信息。

```
client_id:yanglin
client_secret:$2a$10$ThJt7f/UCUnRv6.WzuDpReJ4y5yGUvZp5u14Ph.6n/dax/bRkoica
authorized_grant_types:password
scope:app
web_server_redirect_uri:http://127.0.0.1

使用的加密代码：
new BCryptPasswordEncoder().encode(clientSecret)
```

访问[OAuth在线API文档（需要验证账号密码：yanglin/yanglin）](http://127.0.0.1:9001/swagger-ui.html)成功，则表示数据库加载客户端ID和密码成功

### 6.2.5. 测试数据库加载用户数据库是否成功

需要在user-auth，user两个数据库中的oauth_client_details添加信息。

### 6.2.6. 测试订单购物车微服务

运行[OrderApplication.java](./mall/mall-parent/mall-service/mall-service-order/src/main/java/com/mall/OrderApplication.java)，[Order在线API文档](http://127.0.0.1:18088/swagger-ui.html)

认证信息中添加上面登录成功后返回的accessToken，并测试其中的功能

### 6.2.7. 测试购物车添加，并存储到redis

运行[OrderApplication.java](./mall/mall-parent/mall-service/mall-service-order/src/main/java/com/mall/OrderApplication.java)，[Order在线API文档](http://127.0.0.1:18088/swagger-ui.html)

认证信息中添加上面登录成功后返回的accessToken，然后运行下面的功能

```
curl -X POST "http://127.0.0.1:18088/cart/add?id=100000015158&num=2" -H "accept: */*"
```

### 6.2.8. 测试购物车查询，在redis中

运行[OrderApplication.java](./mall/mall-parent/mall-service/mall-service-order/src/main/java/com/mall/OrderApplication.java)，[Order在线API文档](http://127.0.0.1:18088/swagger-ui.html)

认证信息中添加上面登录成功后返回的accessToken，然后运行下面的功能

```
curl -X GET "http://127.0.0.1:18088/cart/list" -H "accept: */*"
```

### 6.2.9. 到底allowBeanDefinitionOverriding应该设置true还是false？

```properties
spring.main.allow-bean-definition-overriding=true
```

上面代码中可以看到，spring中默认是true，也就是默认支持名称相同的bean的覆盖。而springboot中的默认值是false，也就是不支持名称相同的bean被覆盖。
那么我们自己应该如何选择呢？
这里笔者认为默认不覆盖比较好。
因为还是推荐一个系统中不要存在名称相同的bean，否则后者覆盖前者，多人分工合作的时候，难以避免某些bean被覆盖，会出现很多诡异的问题 ，甚至会带来线上真实的业务损失。
bean的名称不相同，依据具体的业务给bean起名字。这样不但可以解决bean名称重复的问题，还可以大大提高程序的可读性与可维护性。
只有当集成了第三方的库，不同库直接由于是多个团队开发的，甚至这些团队属于不同的国家，有可能会出现bean名称相同的情况。这种情况就需要根据实际需求来设置allowBeanDefinitionOverriding的值了。

### 6.2.10. 解决令牌校验的问题

```xml
    <build>
        <resources>
<!--            <resource>-->
<!--                <directory>src/main/resources</directory>-->
<!--                <filtering>true</filtering>-->
<!--                <excludes>-->
<!--                    <exclude>**/*.key</exclude>-->
<!--                </excludes>-->
<!--            </resource>-->
            <resource>
                <directory>src/main/resources</directory>
                <includes>
                    <include>**.key</include>
                    <include>**.properties</include>
                </includes>
            </resource>
        </resources>
    </build>
```

### 6.2.11. 实现微服务之间认证（OAuth和User模块的认证访问）

[FeignInterceptor.javaFeign-调用拦截器为了在访问时候添加Token](./mall/mall-parent/mall-user-oauth/src/main/java/com/mall/oauth/config/FeignInterceptor.java)

[AdminToken.java-设置管理员Token以调用其他微服务](./mall/mall-parent/mall-user-oauth/src/main/java/com/mall/oauth/util/AdminToken.java)

### 6.2.12. 警用URL灵活的验证，后面再实现

```java
//                .withObjectPostProcessor(new ObjectPostProcessor<FilterSecurityInterceptor>() {
//                    @Override
//                    public <O extends FilterSecurityInterceptor> O postProcess(O o) {
//                        o.setSecurityMetadataSource(appFilterInvocationSecurityMetadataSource);
//                        o.setAccessDecisionManager(urlAccessDecisionManager);
//                        return o;
//                    }
//                })
```

### 6.2.13. 微服务之间的认证（Order微服务访问Goods微服务）

微服务之间的认证就是各大微服务传递有效令牌的过程

```java
public class FeignInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (requestAttributes != null) {
            //1.获取请求对象
            HttpServletRequest request = requestAttributes.getRequest();
            Enumeration<String> headerNames = request.getHeaderNames();
            if (headerNames != null) {
                //2.获取请求对象中的所有的头信息(网关传递过来的)
                while (headerNames.hasMoreElements()) {
                    String name = headerNames.nextElement();//头的名称
                    String value = request.getHeader(name);//头名称对应的值
                    System.out.println("name:" + name + "::::::::value:" + value);
                    //3.将头信息传递给fegin (restTemplate)
                    requestTemplate.header(name, value);
                }
            }
        }
//        requestTemplate.header("Authorization", "bearer" + AdminToken.adminToken());
    }
}

/***
 * 创建拦截器Bean对象
 * @return
 */
@Bean
public FeignInterceptor feignInterceptor(){
    return new FeignInterceptor();
}
```

### 6.2.14. 熔断模式开启

```
// 开启时Feign的熔断：默认是线程隔离：100个连接
feign.hystrix.enabled=true
```

用户当前请求的时候对应线程的数据,如果开启了熔断,默认是线程池隔离,会开启新的线程,需要将熔断策略换成信号量隔离,此时不会开启新的

这时我们发现这块的ServletRequestAttributes始终为空，RequestContextHolder.getRequestAttributes()该方法是从ThreadLocal变量里面取得相应信息的，当hystrix断路器的隔离策略为THREAD（线程）时，是无法取得ThreadLocal中的值。

解决方案：hystrix隔离策略换为SEMAPHORE

修改changgou-web-order的application.yml配置文件，在application.yml中添加如下代码，代码如下：

```properties
hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds=30000
hystrix.command.default.execution.isolation.strategy=SEMAPHORE #信号量
```

| **隔离方式** | **是否支持超时**                                             | **是否支持熔断**                                             | **隔离原理**         | **是否是异步调用**                     | **资源消耗**                                 |
| ------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | -------------------- | -------------------------------------- | -------------------------------------------- |
| 线程池隔离   | 支持，可直接返回                                             | 支持，当线程池到达maxSize后，再请求会触发fallback接口进行熔断 | 每个服务单独用线程池 | 可以是异步，也可以是同步。看调用的方法 | 大，大量线程的上下文切换，容易造成机器负载高 |
| 信号量隔离   | 不支持，如果阻塞，只能通过调用协议（如：socket超时才能返回） | 支持，当信号量达到maxConcurrentRequests后。再请求会触发fallback | 通过信号量的计数器   | 同步调用，不支持异步                   | 小，只是个计数器                             |

### 6.2.15. 微服务中获取用户令牌信息并解析获取用户信息

[TokenDecode.java](./mall/mall-parent/mall-service/mall-service-order/src/main/java/com/mall/order/config/TokenDecode.java)

注意公钥文件的加载：

pom.xml

```xml
    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <includes>
                    <include>**.key</include>
                    <include>**.properties</include>
                </includes>
            </resource>
        </resources>
    </build>
```

# 7. 微信支付

## 7.1. 运行

运行[EurekaServerApplication.java](./mall/mall-parent/mall-eureka/src/main/java/com/mall/EurekaServerApplication.java)，访问 [http://127.0.0.1:7001/](http://127.0.0.1:7001/)

运行[OrderApplication.java](./mall/mall-parent/mall-service/mall-service-order/src/main/java/com/mall/OrderApplication.java)，[Order在线API文档](http://127.0.0.1:18088/swagger-ui.html)

运行[WeixinPayApplication.java](./mall/mall-parent/mall-service/mall-service-pay/src/main/java/com/mall/pay/WeixinPayApplication.java)，[WeixinPay在线API文档](http://127.0.0.1:18089/swagger-ui.html)

## 7.2. 测试生成微信支付连接

访问[WeixinPay在线API文档](http://127.0.0.1:18089/swagger-ui.html)，测试下面的链接：

```
curl -X GET "http://127.0.0.1:18092/weixin/pay/create/native?out_trade_no=801977840830566400&total_fee=2" -H "accept: */*"
```

返回信息，返回支付链接，可以从微信打开：

```json
{
  "out_trade_no": "801977840830566400",
  "code_url": "weixin://wxpay/bizpayurl?pr=c1zN1XBzz",//支付链接，可以从微信打开
  "total_fee": "2"//单位：分
}
```

## 7.3. 测试刚才创建的订单的支付状态

访问[WeixinPay在线API文档](http://127.0.0.1:18089/swagger-ui.html)，测试下面的链接：

```
curl -X GET "http://127.0.0.1:18092/weixin/pay/status/query?out_trade_no=801977840830566400" -H "accept: */*"
```

```json
{
  "nonce_str": "wmTh3hmC4Fy8EqPA",
  "device_info": "",
  "out_trade_no": "801977840830566400",
  "trade_state": "NOTPAY",
  "appid": "wx8397f8696b538317",
  "total_fee": "2",
  "sign": "21042FB45FBCB557C39AC9180E5F864B",
  "trade_state_desc": "订单未支付",
  "return_msg": "OK",
  "result_code": "SUCCESS",
  "mch_id": "1473426802",
  "return_code": "SUCCESS"
}
```

## 7.4. 安装RabbitMQ（Docker）

使用下面命令安装并运行RabbitMQ：

```bash
//下载镜像
docker pull rabbitmq:3.8.10-management

// 启动容器（15672是管理界面的端口，5672是服务的端口。这里顺便将管理系统的用户名和密码设置为guest guest）
docker run -dit --name Myrabbitmq -e RABBITMQ_DEFAULT_USER=guest -e RABBITMQ_DEFAULT_PASS=guest -p 15672:15672 -p 5672:5672 rabbitmq:3.8.10-management

// 设置自定重启
docker update --restart=always Myrabbitmq
```

访问下面[RabbitMQ管理页面](http://172.16.26.2:15672/)，用户名和密码为guest/guest。

## 7.5. 测试支付好后，订单状态通过Rabbit自动更新订单状态

访问[WeixinPay在线API文档](http://127.0.0.1:18089/swagger-ui.html)，测试下面的地址。

```
801977840830566400 // 获取订单号

// 测试下面的地址，表示支付状态更新，任务会推送到到RabbitMQ，然后RabbitMQ会向Order微服务发送请求（准确来说应该是Order微服务向RabbitMQ获取任务信息）
curl -X GET "http://127.0.0.1:18089/weixin/pay/notify/url" -H "accept: */*"
```

然后Order微服务就会接收到消息，执行相应的任务，如果Order微服务是停掉的，启动后也会向RabbitMQ获取任务，并且获取响应消息，执行响应的任务，下面是源代码：

[OrderUpdateListener.java](./mall/mall-parent/mall-service/mall-service-order/src/main/java/com/mall/order/listener/OrderUpdateListener.java)

## 7.6. 测试超时订单处理

订单超时，需要设置队列，RabbitMQ队列在实际工作中是手动创建的，当前项目只是为了开发测试方便

[延时监听器](./mall/mall-parent/mall-service/mall-service-order/src/main/java/com/mall/order/mq/listener/DelayMessageListener.java)
[QueueConfig.java-队列设置](./mall/mall-parent/mall-service/mall-service-order/src/main/java/com/mall/order/mq/queue/QueueConfig.java)

下面是测试新增订单时候延时向RabbitMQ发送消息，延续的时间就是订单的有效时间，时间已过就会删除订单或者使订单失效，具体代码

[OrderController.java](./mall/mall-parent/mall-service/mall-service-order/src/main/java/com/mall/order/controller/OrderController.java)

```
curl -X POST "http://127.0.0.1:18088/order/mq-add-order" -H "accept: */*"

//后台打印的消息
创建订单时间Fri Jan 22 02:32:09 CST 2021
监听消息的时间：Fri Jan 22 02:32:22 CST 2021
监听消息的内容：802002636585766912
```

# 8. 秒杀

**学习目标**

- ==秒杀业务分析==
- 秒杀商品压入Redis缓存
- Spring定时任务了解-定时将秒杀商品存入到Redis中
- 秒杀商品频道页实现-秒杀商品列表页
- 秒杀商品详情页实现
- 下单实现(普通下单)
- ==多线程异步抢单实现-队列削峰==

## 8.1. 秒杀业务分析

### 8.1.1. 需求分析

所谓“秒杀”，就是网络卖家发布一些超低价格的商品，所有买家在同一时间网上抢购的一种销售方式。通俗一点讲就是网络商家为促销等目的组织的网上限时抢购活动。由于商品价格低廉，往往一上架就被抢购一空，有时只用一秒钟。

秒杀商品通常有两种限制：库存限制、时间限制。

需求：

```properties
（1）录入秒杀商品数据，主要包括：商品标题、原价、秒杀价、商品图片、介绍、秒杀时段等信息
（2）秒杀频道首页列出秒杀商品（进行中的）点击秒杀商品图片跳转到秒杀商品详细页。
（3）商品详细页显示秒杀商品信息，点击立即抢购实现秒杀下单，下单时扣减库存。当库存为0或不在活动期范围内时无法秒杀。
（4）秒杀下单成功，直接跳转到支付页面（微信扫码），支付成功，跳转到成功页，填写收货地址、电话、收件人等信息，完成订单。
（5）当用户秒杀下单5分钟内未支付，取消预订单，调用微信支付的关闭订单接口，恢复库存。
```

### 8.1.2. 秒杀需求分析

秒杀技术实现核心思想是运用缓存减少数据库瞬间的访问压力！读取商品详细信息时运用缓存，当用户点击抢购时减少缓存中的库存数量，当库存数为0时或活动期结束时，同步到数据库。 产生的秒杀预订单也不会立刻写到数据库中，而是先写到缓存，当用户付款成功后再写入数据库。

当然，上面实现的思路只是一种最简单的方式，并未考虑其中一些问题，例如并发状况容易产生的问题。我们看看下面这张思路更严谨的图：

![image-20210122235839952](./images/mall-14.png)

## 8.2. 运行与测试

### 8.2.1. 创建秒杀微服务

启动[SeckillApplication.java](./mall/mall-parent/mall-service/mall-service-seckill/src/main/java/com/mall/seckill/SeckillApplication.java)微服务，[秒杀在线API文档](http://127.0.0.1:18090/swagger-ui.html)

### 8.2.2. 测试秒杀商品查询并存入到Redis中



每两小时（开发时候设置的是5秒），查询到各个时间段的商品，并存入Redis中，启动启动[SeckillApplication.java](./mall/mall-parent/mall-service/mall-service-seckill/src/main/java/com/mall/seckill/SeckillApplication.java)微服务即可测试，会打印存储的数据。

运用下面的接口和数据结构，实时插入，方便测试

具体代码在 [SeckillGoodsPushTask.java](./mall/mall-parent/mall-service/mall-service-seckill/src/main/java/com/mall/seckill/timer/SeckillGoodsPushTask.java)

```json
curl -X POST "http://127.0.0.1:18090/seckill-goods" -H "accept: */*"

{
  "checkTime": "2021-03-13T08:17:45.857Z",
  "costPrice": 0,
  "endTime": "2021-03-13T08:17:45.857Z",
  "introduction": "string",
  "name": "Name4",// 名字可任意写
  "num": 0,
  "price": 0,
  "remark": "Remark Description",
  "skuId": "string",
  "smallPic": "string",
  "startTime": "2021-03-13T08:17:45.857Z",
  "status": "1",//商品状态
  "stockCount": 4,//库存数量
  "supId": "string"
}
```

### 8.2.3. 测试查询秒杀商品频道列表中的商品列表，从Redis中查询

访问[秒杀在线API文档](http://127.0.0.1:18090/swagger-ui.html)，测试下面的url，注意小时是偶数。

具体代码在[SeckillGoodsController.java](./mall/mall-parent/mall-service/mall-service-seckill/src/main/java/com/mall/seckill/controller/SeckillGoodsController.java)

```
curl -X POST "http://127.0.0.1:18090/seckill-goods/list" -H "accept: */*" -H "Content-Type: application/json" -d "\"2021-01-22T18:52:21.171Z\""
```

### 8.2.4. 测试秒杀页商品查询

访问[秒杀在线API文档](http://127.0.0.1:18090/swagger-ui.html)，测试下面的url，注意其中的数据是写死的，为了学习测试。

具体代码在[SeckillGoodsController.java](./mall/mall-parent/mall-service/mall-service-seckill/src/main/java/com/mall/seckill/controller/SeckillGoodsController.java)

```
id看上面的返回的秒杀商品id，time看startTime和endTime，目的是查看time时间段内的秒杀商品

curl -X GET "http://127.0.0.1:18090/seckill-goods/one?id=802299553253806080&time=2021012218" -H "accept: */*"
```

### 8.2.5. 测试多线程排队抢单，队列削峰(需要看第二遍)

在审视秒杀中，操作一般都是比较复杂的，而且并发量特别高，比如，检查当前账号操作是否已经秒杀过该商品，检查该账号是否存在存在刷单行为，记录用户操作日志等。

下订单这里，我们一般采用多线程下单，但多线程中我们又需要保证用户抢单的公平性，也就是先抢先下单。我们可以这样实现，用户进入秒杀抢单，如果用户复合抢单资格，只需要记录用户抢单数据，存入队列，多线程从队列中进行消费即可，存入队列采用左压，多线程下单采用右取的方式。

#### 8.2.5.1. 异步实现

要想使用Spring的异步操作，需要先开启异步操作，用`@EnableAsync`注解开启，然后在对应的异步方法上添加注解`@Async`即可。

创建com.mall.seckill.task.MultiThreadingCreateOrder类，在类中创建一个createOrder方法，并在方法上添加`@Async`,

具体代码在[SeckillApplication.java](./mall/mall-parent/mall-service/mall-service-seckill/src/main/java/com/mall/seckill/SeckillApplication.java)

具体代码在[MultiThreadingCreateOrder.java](./mall/mall-parent/mall-service/mall-service-seckill/src/main/java/com/mall/seckill/task/MultiThreadingCreateOrder.java)

#### 8.2.5.2. 测试

访问[秒杀在线API文档](http://127.0.0.1:18090/swagger-ui.html)，测试下面的url。点击几次，抢多少次。id是商品ID，time是抢单时间段，库存为0是就抛出卖完了的异常信息：throw new RuntimeException("卖完了");

重新测试的话，可以吧相对应的商品ID的数据库库存stock_count改为其他数字

具体代码在[SeckillOrderController.java](./mall/mall-parent/mall-service/mall-service-seckill/src/main/java/com/mall/seckill/controller/SeckillOrderController.java)

```
curl -X GET "http://127.0.0.1:18090/seckill-order/add?id=802524623839657984&time=2021012304" -H "accept: */*"
```

### 8.2.6. 测试当前用户抢单状态查询

代码写死了用户名username=yanglin

首先测试前面的抢单，然后访问[秒杀在线API文档](http://127.0.0.1:18090/swagger-ui.html)，测试下面的URL

具体代码在[SeckillOrderController.java](./mall/mall-parent/mall-service/mall-service-seckill/src/main/java/com/mall/seckill/controller/SeckillOrderController.java)

```
curl -X GET "http://127.0.0.1:18090/seckill-order/query" -H "accept: */*"
```

返回下面的结果，说明正在抢单中，还未抢成功。

```json
{
  "username": "yanglin",
  "createTime": 1611382177909,
  "status": 1,// 抢单排队中
  "goodsId": "802524623839657984",
  "money": null,
  "orderId": null,
  "time": "2021012306"
}
```

大概10秒的样子，再查询，就会返回下面的结果，说明抢单成功。

```json
{
  "username": "yanglin",
  "createTime": 1611381896273,
  "status": 2,// 秒杀等待支付
  "goodsId": "802524623839657984",
  "money": 10,
  "orderId": "802539412013858816",
  "time": "2021012306"
}
```

### 8.2.7. 防止秒杀重复排队(需要看第二遍)

用户每次抢单的时候，一旦排队，我们设置一个自增值，让该值的初始值为1，每次进入抢单的时候，对它进行递增，如果值>1，则表明已经排队,不允许重复排队,如果重复排队，则对外抛出异常，并抛出异常信息100表示已经正在排队。

具体代码在[SeckillOrderServiceImpl.java](./mall/mall-parent/mall-service/mall-service-seckill/src/main/java/com/mall/seckill/service/impl/SeckillOrderServiceImpl.java)

```java
// 该当前的用户加一个数字+1   incr         使用hash    key field  2，同时Redis是单线程，redisTemplate.boundHashOps不可能同时执行，不用担心并发的问题
Long userQueueCount = redisTemplate.boundHashOps(SystemConstants.SEC_KILL_QUEUE_REPEAT_KEY).increment(username, 1);

//判断 是否大于1 如果是,返回 ,否则 就放行 重复了.
if(userQueueCount>1){
    throw  new RuntimeException("20006");
}
```

然后测试下面的抢单，然后访问[秒杀在线API文档](http://127.0.0.1:18090/swagger-ui.html)，测试下面的URL，也就是抢单接口，测试两次

```
curl -X GET "http://127.0.0.1:18090/seckill-order/add?id=802524623839657984&time=2021012304" -H "accept: */*"
```

第二次就会出现下面的信息（返回结构需要修饰）：

```json
{
  "errorMessage": "20006，重复下单了"
}
```

### 8.2.8. 测试抢单超卖和数据不精确的问题(需要看第二遍)

#### 8.2.8.1. 问题描述

超卖问题，这里是指多人抢购同一商品的时候，多人同时判断是否有库存，如果只剩一个，则都会判断有库存，此时会导致超卖现象产生，也就是一个商品下了多个订单的现象。

#### 8.2.8.2. 思路分析

解决超卖问题，可以利用Redis队列实现，**给每件商品创建一个独立的商品个数队列**，例如：A商品有2个，A商品的ID为1001，则可以创建一个队列,key=SeckillGoodsCountList_1001,往该队列中塞2次该商品ID。

每次给用户下单的时候，先从队列中取数据，如果能取到数据，则表明有库存，如果取不到，则表明没有库存，这样就可以防止超卖问题产生了。

在我们队Redis进行操作的时候，很多时候，都是先将数据查询出来，在内存中修改，然后存入到Redis，在并发场景，会出现数据错乱问题，**为了控制数量准确，我们单独将商品数量整一个自增键，自增键是线程安全的，所以不担心并发场景的问题**。

#### 8.2.8.3. 测试

首先注意，在启动[SeckillApplication.java](./mall/mall-parent/mall-service/mall-service-seckill/src/main/java/com/mall/seckill/SeckillApplication.java)微服务时候，加载的秒杀商品数据是复合要求的（其中的时区问题，以及秒杀商品数据筛选都很关键，可以在数据库中手动修改）。

```java
 String username = System.currentTimeMillis() + "yanglin";//测试生成不同的用户名测试超卖问题，暂时的
```

接下来访问[秒杀在线API文档](http://127.0.0.1:18090/swagger-ui.html)，测试下面的URL，也就是抢单接口，测试多次（商品有几个，就超过几次，同时在用户名的控制上，也是随机生成，为了测试超卖问题）

```
curl -X GET "http://127.0.0.1:18090/seckill-order/add?id=802524623839657984&time=2021012310" -H "accept: */*"
```

同时在后台会打印出下面的数据，多次抢单的用户会取不到商品，并且打印“卖完了”。

```
取出的商品：802524623839657984
开始模拟下单操作=====start====Sat Jan 23 09:35:03 UTC 2021task-3
取出的商品：802524623839657984
开始模拟下单操作=====start====Sat Jan 23 09:35:04 UTC 2021task-4
取出的商品：802524623839657984
开始模拟下单操作=====start====Sat Jan 23 09:35:05 UTC 2021task-5
取出的商品：802524623839657984
开始模拟下单操作=====start====Sat Jan 23 09:35:05 UTC 2021task-6
取出的商品：null
卖完了
取出的商品：null
卖完了
取出的商品：null
卖完了
取出的商品：null
卖完了
取出的商品：null
卖完了
取出的商品：null
卖完了
开始模拟下单操作=====end====Sat Jan 23 09:35:13 UTC 2021task-3
开始模拟下单操作=====end====Sat Jan 23 09:35:14 UTC 2021task-4
开始模拟下单操作=====end====Sat Jan 23 09:35:15 UTC 2021task-5
开始模拟下单操作=====end====Sat Jan 23 09:35:15 UTC 2021task-6
```

### 8.2.9. 解决数据不精确的问题

出现在这个文件中[MultiThreadingCreateOrder.java](./mall/mall-parent/mall-service/mall-service-seckill/src/main/java/com/mall/seckill/task/MultiThreadingCreateOrder.java)

```java
//5.减库存
//seckillGoods.setStockCount(seckillGoods.getStockCount() - 1);
//通过Redis自增，自减单线程的特性解决
Long increment = redisTemplate.boundHashOps(SystemConstants.SECK_KILL_GOODS_COUNT_KEY).increment(id, -1L);
seckillGoods.setStockCount(increment.intValue());
```

# 9. 分布式事务介绍

## 9.1. 什么是事务

数据库事务(简称：事务，Transaction)是指数据库执行过程中的一个逻辑单位，由一个有限的数据库操作序列构成[由当前业务逻辑多个不同操作构成]。

事务拥有以下四个特性，习惯上被称为ACID特性：

**原子性(Atomicity)**：事务作为一个整体被执行，包含在其中的对数据库的操作要么全部被执行，要么都不执行。

**一致性(Consistency)**：事务应确保数据库的状态从一个一致状态转变为另一个一致状态。一致状态是指数据库中的数据应满足完整性约束。除此之外，一致性还有另外一层语义，就是事务的中间状态不能被观察到(这层语义也有说应该属于原子性)。

**隔离性(Isolation)**：多个事务并发执行时，一个事务的执行不应影响其他事务的执行，如同只有这一个操作在被数据库所执行一样。

**持久性(Durability)**：已被提交的事务对数据库的修改应该永久保存在数据库中。在事务结束时，此操作将不可逆转。

## 9.2. 本地事务

起初，事务仅限于对单一数据库资源的访问控制,架构服务化以后，事务的概念延伸到了服务中。倘若将一个单一的服务操作作为一个事务，那么整个服务操作只能涉及一个单一的数据库资源,这类基于单个服务单一数据库资源访问的事务，被称为本地事务(Local Transaction)。 

## 9.3. 什么是分布式事务

分布式事务指事务的参与者、支持事务的服务器、资源服务器以及事务管理器分别位于不同的分布式系统的不同节点之上,且属于不同的应用，分布式事务需要保证这些操作要么全部成功，要么全部失败。本质上来说，分布式事务就是为了保证不同数据库的数据一致性。

## 9.4. 分布式事务应用架构

本地事务主要限制在单个会话内，不涉及多个数据库资源。但是在基于SOA(Service-Oriented Architecture，面向服务架构)的分布式应用环境下，越来越多的应用要求对多个数据库资源，多个服务的访问都能纳入到同一个事务当中，分布式事务应运而生。 

### 9.4.1. 单一服务分布式事务

最早的分布式事务应用架构很简单，不涉及服务间的访问调用，仅仅是服务内操作涉及到对**多个数据库资源**的访问。 

### 9.4.2. 多服务分布式事务

当一个服务操作访问不同的数据库资源，又希望对它们的访问具有事务特性时，就需要采用分布式事务来协调所有的事务参与者。

对于上面介绍的分布式事务应用架构，尽管一个服务操作会访问多个数据库资源，但是毕竟整个事务还是控制在单一服务的内部。如果一个服务操作需要调用另外一个服务，这时的事务就需要跨越多个服务了。在这种情况下，起始于某个服务的事务在调用另外一个服务的时候，需要以某种机制流转到另外一个服务，从而使被调用的服务访问的资源也自动加入到该事务当中来。

### 9.4.3. 多服务多数据源分布式事务

如果将上面这两种场景(一个服务可以调用多个数据库资源，也可以调用其他服务)结合在一起，对此进行延伸，整个分布式事务的参与者将会组成如下图所示的树形拓扑结构。在一个跨服务的分布式事务中，事务的发起者和提交均系同一个，它可以是整个调用的客户端，也可以是客户端最先调用的那个服务。 

较之基于单一数据库资源访问的本地事务，分布式事务的应用架构更为复杂。在不同的分布式应用架构下，实现一个分布式事务要考虑的问题并不完全一样，比如对多资源的协调、事务的跨服务传播等，实现机制也是复杂多变。 

事务的作用：

```
保证每个事务的数据一致性。
```

### 9.4.4. 1.5 CAP定理

CAP 定理，又被叫作布鲁尔定理。对于设计分布式系统(不仅仅是分布式事务)的架构师来说，CAP 就是你的入门理论。

一致性（Consistency）、可用性（Availability）、分区容错性（Partition tolerance）

**C (一致性)：**对某个指定的客户端来说，读操作能返回最新的写操作。

对于数据分布在不同节点上的数据来说，如果在某个节点更新了数据，那么在其他节点如果都能读取到这个最新的数据，那么就称为强一致，如果有某个节点没有读取到，那就是分布式不一致。

**A (可用性)：**非故障的节点在合理的时间内返回合理的响应(不是错误和超时的响应)。可用性的两个关键一个是合理的时间，一个是合理的响应。

合理的时间指的是请求不能无限被阻塞，应该在合理的时间给出返回。合理的响应指的是系统应该明确返回结果并且结果是正确的，这里的正确指的是比如应该返回 50，而不是返回 40。

**P (分区容错性)：**当出现网络分区后，系统能够继续工作。打个比方，这里集群有多台机器，有台机器网络出现了问题，但是这个集群仍然可以正常工作。

熟悉 CAP 的人都知道，三者不能共有，如果感兴趣可以搜索 CAP 的证明，在分布式系统中，网络无法 100% 可靠，分区其实是一个必然现象。

如果我们选择了 CA 而放弃了 P，那么当发生分区现象时，为了保证一致性，这个时候必须拒绝请求，但是 A 又不允许，所以分布式系统理论上不可能选择 CA 架构，只能选择 CP 或者 AP 架构。

对于 CP 来说，放弃可用性，追求一致性和分区容错性，我们的 ZooKeeper 其实就是追求的强一致。

对于 AP 来说，放弃一致性(这里说的一致性是强一致性)，追求分区容错性和可用性，这是很多分布式系统设计时的选择，后面的 BASE 也是根据 AP 来扩展。

顺便一提，CAP 理论中是忽略网络延迟，也就是当事务提交时，从节点 A 复制到节点 B 没有延迟，但是在现实中这个是明显不可能的，所以总会有一定的时间是不一致。

同时 CAP 中选择两个，比如你选择了 CP，并不是叫你放弃 A。因为 P 出现的概率实在是太小了，大部分的时间你仍然需要保证 CA。

就算分区出现了你也要为后来的 A 做准备，比如通过一些日志的手段，是其他机器回复至可用。

## 9.5. 分布式事务解决方案

1.XA两段提交(低效率)-21 XA JTA分布式事务解决方案

2.TCC三段提交(2段,高效率[不推荐(补偿代码)])

3.本地消息(MQ+Table)

4.事务消息(RocketMQ[alibaba])

5.Seata(alibaba)

## 9.6. 基于XA协议的两阶段提交(2PC)

两阶段提交协议(Two Phase Commitment Protocol)中，涉及到两种角色

==一个事务协调者==（coordinator）：负责协调多个参与者进行事务投票及提交(回滚)
多个==事务参与者==（participants）：即本地事务执行者

总共处理步骤有两个
（1）投票阶段（voting phase）：协调者将通知事务参与者准备提交或取消事务，然后进入表决过程。参与者将告知协调者自己的决策：同意（事务参与者本地事务执行成功，但未提交）或取消（本地事务执行故障）；
（2）提交阶段（commit phase）：收到参与者的通知后，协调者再向参与者发出通知，根据反馈情况决定各参与者是否要提交还是回滚；

如果任一资源管理器在第一阶段返回准备失败，那么事务管理器会要求所有资源管理器在第二阶段执行回滚操作。通过事务管理器的两阶段协调，最终所有资源管理器要么全部提交，要么全部回滚，最终状态都是一致的 

**优点：** 尽量保证了数据的强一致，适合对数据强一致要求很高的关键领域。

**缺点：** 牺牲了可用性，对性能影响较大，不适合高并发高性能场景，如果分布式系统跨接口调用，目前 .NET 界还没有实现方案。

## 9.7. 补偿（代码补偿）事务（TCC）-3PC

TCC 将事务提交分为 Try(method1) - Confirm(method2) - Cancel(method3) 3个操作。其和两阶段提交有点类似，Try为第一阶段，Confirm - Cancel为第二阶段，是一种应用层面侵入业务的两阶段提交。

| 操作方法 | 含义                                                         |
| -------- | ------------------------------------------------------------ |
| Try      | 预留业务资源/数据效验                                        |
| Confirm  | 确认执行业务操作，实际提交数据，不做任何业务检查，try成功，confirm必定成功，需保证幂等 |
| Cancel   | 取消执行业务操作，实际回滚数据，需保证幂等                   |

其核心在于将业务分为两个操作步骤完成。不依赖 RM 对分布式事务的支持，而是通过对业务逻辑的分解来实现分布式事务。

例如： A要向 B 转账，思路大概是： 

```
我们有一个本地方法，里面依次调用 
1、首先在 Try 阶段，要先调用远程接口把 B和 A的钱给冻结起来。 
2、在 Confirm 阶段，执行远程调用的转账的操作，转账成功进行解冻。 
3、如果第2步执行成功，那么转账成功，如果第二步执行失败，则调用远程冻结接口对应的解冻方法 (Cancel)。 
```

**优点：** 跟2PC比起来，实现以及流程相对简单了一些，但数据的一致性比2PC也要差一些

**缺点：** 缺点还是比较明显的，在2,3步中都有可能失败。TCC属于应用层的一种补偿方式，所以需要程序员在实现的时候多写很多补偿的代码，在一些场景中，一些业务流程可能用TCC不太好定义及处理。很容易造成非幂等问题。

## 9.8. 本地消息表（异步确保）-事务的最终一致性

本地消息表这种实现方式应该是业界使用最多的，其核心思想是将分布式事务拆分成本地事务进行处理，这种思路是来源于ebay。我们可以从下面的流程图中看出其中的一些细节： 

![1553321000110](./images/mall-15.png)

基本思路就是：

消息生产方，需要额外建一个消息表，并记录消息发送状态。消息表和业务数据要在一个事务里提交，也就是说他们要在一个数据库里面。然后消息会经过MQ发送到消息的消费方。如果消息发送失败，会进行重试发送。

消息消费方，需要处理这个消息，并完成自己的业务逻辑。此时如果本地事务处理成功，表明已经处理成功了，如果处理失败，那么就会重试执行。如果是业务上面的失败，可以给生产方发送一个业务补偿消息，通知生产方进行回滚等操作。

生产方和消费方定时扫描本地消息表，把还没处理完成的消息或者失败的消息再发送一遍。如果有靠谱的自动对账补账逻辑，这种方案还是非常实用的。

这种方案遵循BASE理论，采用的是最终一致性，笔者认为是这几种方案里面比较适合实际业务场景的，即不会出现像2PC那样复杂的实现(当调用链很长的时候，2PC的可用性是非常低的)，也不会像TCC那样可能出现确认或者回滚不了的情况。

**优点：** 一种非常经典的实现，避免了分布式事务，实现了最终一致性。在 .NET中 有现成的解决方案。

**缺点：** 消息表会耦合到业务系统中，如果没有封装好的解决方案，会有很多杂活需要处理。

## 9.9. MQ 事务消息-最终一致性

有一些第三方的MQ是支持事务消息的，比如RocketMQ，他们支持事务消息的方式也是类似于采用的二阶段提交，但是市面上一些主流的MQ都是不支持事务消息的，比如 RabbitMQ 和 Kafka 都不支持。

以阿里的 RocketMQ 中间件为例，其思路大致为：

第一阶段Prepared消息，会拿到消息的地址。
第二阶段执行本地事务，第三阶段通过第一阶段拿到的地址去访问消息，并修改状态。

也就是说在业务方法内要想消息队列提交两次请求，一次发送消息和一次确认消息。如果确认消息发送失败了RocketMQ会定期扫描消息集群中的事务消息，这时候发现了Prepared消息，它会向消息发送者确认，所以生产方需要实现一个check接口，RocketMQ会根据发送端设置的策略来决定是回滚还是继续发送确认消息。这样就保证了消息发送与本地事务同时成功或同时失败。

![](./images/mall-15.jpg)



**优点：** 实现了最终一致性，不需要依赖本地数据库事务。

**缺点：** 目前主流MQ中只有RocketMQ支持事务消息。

## 9.10. Seata-2PC->改进

2019 年 1 月，阿里巴巴中间件团队发起了开源项目 [*Fescar*](https://www.oschina.net/p/fescar)*（Fast & EaSy Commit And Rollback）*，和社区一起共建开源分布式事务解决方案。Fescar 的愿景是让分布式事务的使用像本地事务的使用一样，简单和高效，并逐步解决开发者们遇到的分布式事务方面的所有难题。

**Fescar 开源后，蚂蚁金服加入 Fescar 社区参与共建，并在 Fescar 0.4.0 版本中贡献了 TCC 模式。**

为了打造更中立、更开放、生态更加丰富的分布式事务开源社区，经过社区核心成员的投票，大家决定对 Fescar 进行品牌升级，并更名为 **Seata**，意为：**Simple Extensible Autonomous Transaction Architecture**，是一套一站式分布式事务解决方案。

Seata 融合了阿里巴巴和蚂蚁金服在分布式事务技术上的积累，并沉淀了新零售、云计算和新金融等场景下丰富的实践经验。

### 9.10.1. Seata介绍

解决分布式事务问题，有两个设计初衷

**对业务无侵入**：即减少技术架构上的微服务化所带来的分布式事务问题对业务的侵入
**高性能**：减少分布式事务解决方案所带来的性能消耗

seata中有两种分布式事务实现方案，AT及TCC

- AT模式（2PC->改进）主要关注多 DB 访问的数据一致性，当然也包括多服务下的多 DB 数据访问一致性问题

- TCC 模式主要关注业务拆分，在按照业务横向扩展资源时，解决微服务间调用的一致性问题

### 9.10.2. AT模式

Seata AT模式是基于XA事务演进而来的一个分布式事务中间件，XA是一个基于数据库实现的分布式事务协议，本质上和两阶段提交一样，需要数据库支持，Mysql5.6以上版本支持XA协议，其他数据库如Oracle，DB2也实现了XA接口

![1565820574338](./images/mall-16.png)

解释：

**Transaction Coordinator (TC)**： 事务协调器，维护全局事务的运行状态，负责协调并驱动全局事务的提交或回滚。
**Transaction Manager（TM）**： 控制全局事务的边界，负责开启一个全局事务，并最终发起全局提交或全局回滚的决议。
**Resource Manager (RM)**： 控制分支事务，负责分支注册、状态汇报，并接收事务协调器的指令，驱动分支（本地）事务的提交和回滚。

协调执行流程如下：

![1565820735168](./images/mall-17.png)

Branch就是指的分布式事务中每个独立的本地局部事务。



**第一阶段**

Seata 的 JDBC 数据源代理通过对业务 SQL 的解析，把业务数据在更新前后的数据镜像组织成回滚日志，利用 本地事务 的 ACID 特性，将业务数据的更新和回滚日志的写入在同一个 本地事务 中提交。

这样，可以保证：**任何提交的业务数据的更新一定有相应的回滚日志存在**

![1565820909345](./images/mall-18.png)

基于这样的机制，分支的本地事务便可以在全局事务的第一阶段提交，并马上释放本地事务锁定的资源

这也是Seata和XA事务的不同之处，两阶段提交往往对资源的锁定需要持续到第二阶段实际的提交或者回滚操作，而有了回滚日志之后，可以在第一阶段释放对资源的锁定，降低了锁范围，提高效率，即使第二阶段发生异常需要回滚，只需找对undolog中对应数据并反解析成sql来达到回滚目的

同时Seata通过代理数据源将业务sql的执行解析成undolog来与业务数据的更新同时入库，达到了对业务无侵入的效果。



**第二阶段**

如果决议是全局提交，此时分支事务此时已经完成提交，不需要同步协调处理（只需要异步清理回滚日志），Phase2 可以非常快速地完成.

![1565821037492](./images/mall-19.png)

如果决议是全局回滚，RM 收到协调器发来的回滚请求，通过 XID 和 Branch ID 找到相应的回滚日志记录，**通过回滚记录生成反向的更新 SQL 并执行**，以完成分支的回滚

![1565821069728](./images/mall-20.png)

### 9.10.3. TCC模式

seata也针对TCC做了适配兼容，支持TCC事务方案，原理前面已经介绍过，基本思路就是使用侵入业务上的补偿及事务管理器的协调来达到全局事务的一起提交及回滚。

![1565821173446](./images/mall-21.png)

# 10. 集群高可用

## 10.1. 学习目标

- 理解集群流程

- 理解分布式概念

- 能实现==Eureka集群==[集群配置]

- 能实现==Redis集群[Redis集群配置、哨兵策略(案例)、Redis击穿问题]==

  ```
  1.Redis集群的原理
  2.Redis集群会用->在java代码中能链接集群服务
  3.哨兵策略->监控集群的健康状态[作用]
  4.Redis击穿->如何解决击穿问题
  5.如何解决Redis雪崩问题->多级缓存
  ```

- RabbitMQ集群

## 10.2. 集群概述

### 10.2.1. 什么是集群

#### 10.2.1.1. 集群概念

集群是一种计算机系统， 它通过一组松散集成的计算机软件和/或硬件连接起来高度紧密地协作完成计算工作。在某种意义上，他们可以被看作是一台计算机。集群系统中的单个计算机通常称为节点，通常通过局域网连接，但也有其它的可能连接方式。集群计算机通常用来改进单个计算机的计算速度和/或可靠性。一般情况下集群计算机比单个计算机，比如工作站或超级计算机性能价格比要高得多。

#### 10.2.1.2. 集群的特点

集群拥有以下两个特点：

1. 可扩展性：集群的性能不限制于单一的服务实体，新的服务实体可以动态的添加到集群，从而增强集群的性能。
2. 高可用性：集群当其中一个节点发生故障时，这台节点上面所运行的应用程序将在另一台节点被自动接管，消除单点故障对于增强数据可用性、可达性和可靠性是非常重要的。

#### 10.2.1.3. 集群的两大能力

集群必须拥有以下两大能力：

1. 负载均衡：负载均衡把任务比较均匀的分布到集群环境下的计算和网络资源，以提高数据吞吐量。
2. 错误恢复：如果集群中的某一台服务器由于故障或者维护需要无法使用，资源和应用程序将转移到可用的集群节点上。这种由于某个节点的资源不能工作，另一个可用节点中的资源能够透明的接管并继续完成任务的过程，叫做错误恢复。

负载均衡和错误恢复要求各服务实体中有执行同一任务的资源存在，而且对于同一任务的各个资源来说，执行任务所需的信息视图必须是相同的。

### 10.2.2. 集群与分布式的区别

说到集群，可能大家会立刻联想到另一个和它很相近的一个词----“分布式”。那么集群和分布式是一回事吗？有什么联系和区别呢?

相同点：

​	分布式和集群都是需要有很多节点服务器通过网络协同工作完成整体的任务目标。

不同点：

​	分布式是指将业务系统进行拆分，即分布式的每一个节点都是实现不同的功能。而集群每个节点做的是同一件事情。

## 10.3. Eureka集群

### 10.3.1. Eureka简介

#### 10.3.1.1. 什么是Eureka

​	Eureka是一种基于REST（Representational State Transfer）的服务，主要用于AWS，用于定位服务，以实现中间层服务器的负载平衡和故障转移。我们将此服务称为Eureka Server。Eureka还附带了一个基于Java的客户端组件Eureka Client，它使与服务的交互变得更加容易。客户端还有一个内置的负载均衡器，可以进行基本的循环负载均衡。在Netflix，一个更复杂的负载均衡器包装Eureka，根据流量，资源使用，错误条件等多种因素提供加权负载平衡，以提供卓越的弹性。

理解：

​	Eureka是一个服务注册与发现的注册中心。类似于dubbo中的zookeeper.

官网地址：

​         https://github.com/Netflix/eureka/wiki/Eureka-at-a-glance

#### 10.3.1.2. Eureka的架构

![1558513888400](./images/mall-22.png)

application Service :相当于服务提供者

application Client :相当于服务消费者

make remote call :服务调用过程

us-east-1c d e 都是region:us-east-1 的可用区域。 

简单可以理解为：

​	 每一个erurak都是一个节点，默认启动时就是以集群的方式。

区别：

```
eureka:集群，各个节点的数据一致，各个节点都属于同等级别的注册中心，不存在leader的概念。
zookeeper：Zookeeper集群存在Leader节点，并且会进行Leader选举，Leader具有最高权限。
```

```
docker run --name redis-6379 -p 6379:6379 -d redis
docker run --name redis-6380 -p 6380:6379 -d redis
docker run --name redis-6381 -p 6381:6379 -d redis
```

## 10.4. Redis Cluster

### 10.4.1. Redis-Cluster简介

#### 10.4.1.1. 什么是Redis-Cluster

为何要搭建Redis集群。Redis是在内存中保存数据的，而我们的电脑一般内存都不大，这也就意味着Redis不适合存储大数据，适合存储大数据的是Hadoop生态系统的Hbase或者是MogoDB。Redis更适合处理高并发，一台设备的存储能力是很有限的，但是多台设备协同合作，就可以让内存增大很多倍，这就需要用到集群。

Redis集群搭建的方式有多种，例如使用客户端分片、Twemproxy、Codis等，但从redis 3.0之后版本支持redis-cluster集群，它是Redis官方提出的解决方案，Redis-Cluster采用无中心结构，每个节点保存数据和整个集群状态,每个节点都和其他所有节点连接。其redis-cluster架构图如下：

![1554000037613](./images/mall-23.png)

客户端与 redis 节点直连,不需要中间 proxy 层.客户端不需要连接集群所有节点连接集群中任何一个可用节点即可。

所有的 redis 节点彼此互联(PING-PONG 机制),内部使用二进制协议优化传输速度和带宽.

1. 所有集群节点的数据合起来才是完整数据
2. 集群节点的数量必须是奇数个
3. 判断集群是否可用:集群节点中主节点个数过半数以上宕机,则集群失败
4. 程序每次链接集群中的任意一台节点
5. Redis集群,会将16384个Hash槽分配给各个集群节点。
6. 所有存入到 Redis中的key的Hash值经过CRC16算法之后得到的值在0-16383之间。
7. 每次往 Redis中存/取数据的时候,会先计算出key经过CRC16算法后的值,然后根据值到指定的 Redis中获取数据。

##### 10.4.1.1.1. 分布存储机制-槽

（1）redis-cluster 把所有的物理节点映射到[0-16383]slot 上,cluster 负责维护

node<->slot<->value

（2）Redis 集群中内置了 16384 个哈希槽，当需要在 Redis 集群中放置一个 key-value 时，redis 先对 key 使用 crc16 算法算出一个结果，然后把结果对 16384 求余数，这样每个key 都会对应一个编号在 0-16383 之间的哈希槽，redis 会根据节点数量大致均等的将哈希槽映射到不同的节点。

​    例如三个节点：槽分布的值如下：

SERVER1:  0-5460

SERVER2:  5461-10922

SERVER3:  10923-16383

##### 10.4.1.1.2. 容错机制-投票

（1）选举过程是集群中所有master参与,如果半数以上master节点与故障节点通信超过(cluster-node-timeout),认为该节点故障，自动触发故障转移操作.  故障节点对应的从节点自动升级为主节点

（2）什么时候整个集群不可用(cluster_state:fail)? 

如果集群任意master挂掉,且当前master没有slave.集群进入fail状态,也可以理解成集群的slot映射[0-16383]不完成时进入fail状态. 

![1554000104675](./images/mall-24.png)

#### 10.4.1.2. 搭建Redis-Cluster

##### 10.4.1.2.1. 搭建要求

需要 6 台 redis 服务器。搭建伪集群。

需要 6 个 redis 实例。

需要运行在不同的端口 7001-7006

##### 10.4.1.2.2. 准备工作

（1）安装gcc 【此步省略】

Redis 是 c 语言开发的。安装 redis 需要 c 语言的编译环境。如果没有 gcc 需要在线安装。

```
yum install gcc-c++
```

（2）使用yum命令安装 ruby  （我们需要使用ruby脚本来实现集群搭建）【此步省略】

```
yum install ruby
yum install rubygems
```

```
----- 知识点小贴士 -----
Ruby，一种简单快捷的面向对象（面向对象程序设计）脚本语言，在20世纪90年代由日本人松本行弘(Yukihiro Matsumoto)开发，遵守GPL协议和Ruby License。它的灵感与特性来自于 Perl、Smalltalk、Eiffel、Ada以及 Lisp 语言。由 Ruby 语言本身还发展出了JRuby（Java平台）、IronRuby（.NET平台）等其他平台的 Ruby 语言替代品。Ruby的作者于1993年2月24日开始编写Ruby，直至1995年12月才正式公开发布于fj（新闻组）。因为Perl发音与6月诞生石pearl（珍珠）相同，因此Ruby以7月诞生石ruby（红宝石）命名
RubyGems简称gems，是一个用于对 Ruby组件进行打包的 Ruby 打包系统
```

（3）将redis源码包上传到 linux 系统  ，解压redis源码包

（4）编译redis源码  ，进入redis源码文件夹

```
make
```

看到以下输出结果，表示编译成功

![1554000161720](./images/mall-25.png)

（5）创建目录/usr/local/redis-cluster目录，  安装6个redis实例，分别安装在以下目录

```properties
/usr/local/redis-cluster/redis-1
/usr/local/redis-cluster/redis-2
/usr/local/redis-cluster/redis-3
/usr/local/redis-cluster/redis-4
/usr/local/redis-cluster/redis-5
/usr/local/redis-cluster/redis-6
```

以第一个redis实例为例，命令如下

```
make install PREFIX=/usr/local/redis-cluster/redis-1
```

![1554000236118](./images/mall-26.png)

出现此提示表示成功，按此方法安装其余5个redis实例

（6）复制配置文件  将 /redis-3.0.0/redis.conf 复制到redis下的bin目录下

```properties
[root@localhost redis-3.0.0]# cp redis.conf /usr/local/redis-cluster/redis-1/bin
[root@localhost redis-3.0.0]# cp redis.conf /usr/local/redis-cluster/redis-2/bin
[root@localhost redis-3.0.0]# cp redis.conf /usr/local/redis-cluster/redis-3/bin
[root@localhost redis-3.0.0]# cp redis.conf /usr/local/redis-cluster/redis-4/bin
[root@localhost redis-3.0.0]# cp redis.conf /usr/local/redis-cluster/redis-5/bin
[root@localhost redis-3.0.0]# cp redis.conf /usr/local/redis-cluster/redis-6/bin
```

##### 10.4.1.2.3. 配置集群

（1）修改每个redis节点的配置文件redis.conf

​	  修改运行端口为7001 （7002 7003 .....）

​	  将cluster-enabled yes 前的注释去掉(632行)

![1554000285785](./images/mall-27.png)



集群：

```
6个节点
3主
3从

1)创建6个节点  7001-7006
2)开启集群
3)串联集群[将集群链接到一起]
```







（2）启动每个redis实例

​	以第一个实例为例，命令如下

```properties
cd /usr/local/redis-cluster/redis-1/bin/
./redis-server redis.conf
```

![1554000368400](./images/mall-28.png)



把其余的5个也启动起来，然后查看一下是不是都启动起来了

```properties
[root@localhost ~]# ps -ef | grep redis
root     15776 15775  0 08:19 pts/1    00:00:00 ./redis-server *:7001 [cluster]
root     15810 15784  0 08:22 pts/2    00:00:00 ./redis-server *:7002 [cluster]
root     15831 15813  0 08:23 pts/3    00:00:00 ./redis-server *:7003 [cluster]
root     15852 15834  0 08:23 pts/4    00:00:00 ./redis-server *:7004 [cluster]
root     15872 15856  0 08:24 pts/5    00:00:00 ./redis-server *:7005 [cluster]
root     15891 15875  0 08:24 pts/6    00:00:00 ./redis-server *:7006 [cluster]
root     15926 15895  0 08:24 pts/7    00:00:00 grep redis
```



（3）上传redis-3.0.0.gem ，安装 ruby用于搭建redis集群的脚本。

```properties
[root@localhost ~]# gem install redis-3.0.0.gem
Successfully installed redis-3.0.0
1 gem installed
Installing ri documentation for redis-3.0.0...
Installing RDoc documentation for redis-3.0.0...
```



（4）使用 ruby 脚本搭建集群。

进入redis源码目录中的src目录  执行下面的命令  `redis-trib.rb` ruby工具,可以实现Redis集群,`create`创建集群，`--replicas`创建主从关系 1：是否随机创建（是）。

```properties
./redis-trib.rb create --replicas 1 192.168.25.140:7001 192.168.25.140:7002 192.168.25.140:7003
192.168.25.140:7004 192.168.25.140:7005 192.168.25.140:7006
```



出现下列提示信息

```properties
>>> Creating cluster
Connecting to node 192.168.25.140:7001: OK
Connecting to node 192.168.25.140:7002: OK
Connecting to node 192.168.25.140:7003: OK
Connecting to node 192.168.25.140:7004: OK
Connecting to node 192.168.25.140:7005: OK
Connecting to node 192.168.25.140:7006: OK
>>> Performing hash slots allocation on 6 nodes...
Using 3 masters:
192.168.25.140:7001
192.168.25.140:7002
192.168.25.140:7003
Adding replica 192.168.25.140:7004 to 192.168.25.140:7001
Adding replica 192.168.25.140:7005 to 192.168.25.140:7002
Adding replica 192.168.25.140:7006 to 192.168.25.140:7003
M: 1800237a743c2aa918ade045a28128448c6ce689 192.168.25.140:7001
   slots:0-5460 (5461 slots) master
M: 7cb3f7d5c60bfbd3ab28800f8fd3bf6de005bf0d 192.168.25.140:7002
   slots:5461-10922 (5462 slots) master
M: 436e88ec323a2f8bb08bf09f7df07cc7909fcf81 192.168.25.140:7003
   slots:10923-16383 (5461 slots) master
S: c2a39a94b5f41532cd83bf6643e98fc277c2f441 192.168.25.140:7004
   replicates 1800237a743c2aa918ade045a28128448c6ce689
S: b0e38d80273515c84b1a01820d8ecee04547d776 192.168.25.140:7005
   replicates 7cb3f7d5c60bfbd3ab28800f8fd3bf6de005bf0d
S: 03bf6bd7e3e6eece5a02043224497c2c8e185132 192.168.25.140:7006
   replicates 436e88ec323a2f8bb08bf09f7df07cc7909fcf81
Can I set the above configuration? (type 'yes' to accept): yes
>>> Nodes configuration updated
>>> Assign a different config epoch to each node
>>> Sending CLUSTER MEET messages to join the cluster
Waiting for the cluster to join....
>>> Performing Cluster Check (using node 192.168.25.140:7001)
M: 1800237a743c2aa918ade045a28128448c6ce689 192.168.25.140:7001
   slots:0-5460 (5461 slots) master
M: 7cb3f7d5c60bfbd3ab28800f8fd3bf6de005bf0d 192.168.25.140:7002
   slots:5461-10922 (5462 slots) master
M: 436e88ec323a2f8bb08bf09f7df07cc7909fcf81 192.168.25.140:7003
   slots:10923-16383 (5461 slots) master
M: c2a39a94b5f41532cd83bf6643e98fc277c2f441 192.168.25.140:7004
   slots: (0 slots) master
   replicates 1800237a743c2aa918ade045a28128448c6ce689
M: b0e38d80273515c84b1a01820d8ecee04547d776 192.168.25.140:7005
   slots: (0 slots) master
   replicates 7cb3f7d5c60bfbd3ab28800f8fd3bf6de005bf0d
M: 03bf6bd7e3e6eece5a02043224497c2c8e185132 192.168.25.140:7006
   slots: (0 slots) master
   replicates 436e88ec323a2f8bb08bf09f7df07cc7909fcf81
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.
```



#### 10.4.1.3. 连接Redis-Cluster

##### 10.4.1.3.1. 客户端工具连接

Redis-cli 连接集群：

```
redis-cli -p ip地址 -p 端口 -c
```



-c：代表连接的是 redis 集群

测试值的存取:

（1）从本地连接到集群redis  使用7001端口 加 -c 参数

（2）存入name值为abc ，系统提示此值被存入到了7002端口所在的redis （槽是5798）

（3）提取name的值，可以提取。

（4）退出（quit）

（5）再次以7001端口进入 ，不带-c

（6）查询name值，无法获取，因为值在7002端口的redis上

（7）我们以7002端口进入，获取name值发现是可以获取的,而以其它端口进入均不能获取

 

##### 10.4.1.3.2. springboot连接redis集群

（1）创建工程 ，打包方式jar包，命名为：changgou-redis-demo

（2）添加redis起步依赖

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
   <modelVersion>4.0.0</modelVersion>
   <parent>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-parent</artifactId>
      <version>2.1.4.RELEASE</version>
      <relativePath/> <!-- lookup parent from repository -->
   </parent>
   <groupId>com.itheima</groupId>
   <artifactId>changgou-redis-demo</artifactId>
   <version>0.0.1-SNAPSHOT</version>
   <name>changgou-redis-demo</name>
   <description>redis</description>

   <properties>
      <java.version>1.8</java.version>
   </properties>

   <dependencies>
      <dependency>
         <groupId>org.springframework.boot</groupId>
         <artifactId>spring-boot-starter-data-redis</artifactId>
      </dependency>
      <dependency>
         <groupId>org.springframework.boot</groupId>
         <artifactId>spring-boot-starter-web</artifactId>
      </dependency>
      <dependency>
         <groupId>org.mybatis.spring.boot</groupId>
         <artifactId>mybatis-spring-boot-starter</artifactId>
         <version>2.0.1</version>
      </dependency>

      <dependency>
         <groupId>mysql</groupId>
         <artifactId>mysql-connector-java</artifactId>
         <scope>runtime</scope>
      </dependency>
      <dependency>
         <groupId>org.springframework.boot</groupId>
         <artifactId>spring-boot-starter-test</artifactId>
         <scope>test</scope>
      </dependency>


   </dependencies>

   <build>
      <plugins>
         <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
         </plugin>
      </plugins>
   </build>

</project>
```

（3）配置application.yml

```yaml
spring:
  datasource:
    driver-class-name: com.mysql.jdbc.Driver
    url: jdbc:mysql://127.0.0.1:3306/changgou_user?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC
    username: root
    password: itcast
  application:
    name: redis-demo
    #redis配置

//#  rabbitmq:
//#    addresses: 192.168.25.130:5672,192.168.25.134:5672
//#    username: guest
//#    password: guest
  redis:
    cluster:
      nodes:
      - 192.168.25.153:7001
      - 192.168.25.153:7002
      - 192.168.25.153:7003
      - 192.168.25.153:7004
      - 192.168.25.153:7005
      - 192.168.25.153:7006
server:
  ssl:
    enabled: false
  port: 9008
mybatis:
  configuration:
    map-underscore-to-camel-case: true
```



（4）创建测试类进行测试：

```java
package com.itheima.changgouredisdemo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ChanggouRedisDemoApplicationTests {

	@Autowired
	private RedisTemplate redisTemplate;
	@Test
	public void contextLoads() {
		redisTemplate.boundValueOps("key111").set("123");
		System.out.println(redisTemplate.boundValueOps("key111").get());

	}

}

```



#### 10.4.1.4. Redis的持久化

##### 10.4.1.4.1. redis的持久化介绍

​	Redis的数据都放在内存中。如果机器挂掉，内存的数据就不存在，数据不能恢复，严重影响使用。那么redis本身给我们提供了持久化机制。即时出现这样的问题，也能恢复数据。接下来我们来看下redis的两种持久化方

##### 10.4.1.4.2. 开启RDB 

RDB:  快照形式  （定期数据保存磁盘中）会产生一个dump.rdb文件,redis默认开启了RDB的持久化方式。

特点：会存在数据丢失，性能较好，用于数据备份。

如图：有一个文件产生

![1558590766000](./images/mall-29.png)

如图：redis.conf中 的默认的RDB的配置：

![1558590788839](./images/mall-30.png)

解释：

```properties
在 900 秒内最少有 1 个 key 被改动，或者 300 秒内最少有 10 个 key 被改动，又或者 60 秒内最少有 1000 个 key 被改动，以上三个条件随便满足一个，就触发一次保存操作。

if(在60秒之内有10000个keys发生变化时){
  进行镜像备份
}else if(在300秒之内有10个keys发生了变化){
  进行镜像备份
}else if(在900秒之内有1个keys发生了变化){
  进行镜像备份
}
```

##### 10.4.1.4.3. 开启AOF

AOF : append only file . 所有对redis的操作命令记录在.aof文件中,如果想恢复数据，重新加载文件，执行文件中的命令即可。默认的情况下 redis没有开启，要想开启，必须配置。

特点：每秒保存，数据完整性比较好，耗费性能。

开启AOF: 如图 去掉注释

![1558590810611](./images/mall-31.png)

配置 AOF的执行策略:

![1558590827986](./images/mall-32.png)

always:总是执行

everysec:每秒钟执行(默认)

no:不执行。

如果随着时间的推移，AOF文件中的数据越来越大，所以需要进行重写也就是压缩。

![1558590856034](./images/mall-33.png)

如图所示：自动压缩的比例为：

100：上一次AOF文件达到100%的时候进行压缩

64mb ：压缩时最小的文件大小。

Redis集中持久化

##### 10.4.1.4.4. 模式的抉择应用场景介绍

AOF 和RDB对比：

| 命令       | RDB    | AOF          |
| ---------- | ------ | ------------ |
| 启动优先级 | 低     | 高           |
| 体积       | 小     | 大           |
| 恢复速度   | 快     | 慢           |
| 数据安全性 | 丢数据 | 根据策略决定 |

RDB的最佳策略：

+ 关闭 
+ 集中管理（用于备份数据）
+ 主从模式，从开。

AOF的最佳策略：

+ 建议 开  每秒刷盘->aof日志文件中
+ AOF重写集中管理

最佳的策略：

+ 小分片（max_memery 4G左右）
+ 监控机器的负载

#### 10.4.1.5. Redis哨兵模式

Redis在使用过程中服务器毫无征兆的宕机，是一个麻烦的事情，如何保证备份的机器是原始服务器的完整备份呢？这时候就需要哨兵和复制。

Sentinel（哨兵）可以管理多个Redis服务器，它提供了监控，提醒以及自动的故障转移的功能，

Replication（复制）则是负责让一个Redis服务器可以配备多个备份的服务器。

Redis也是利用这两个功能来保证Redis的高可用的

##### 10.4.1.5.1. Redis的主从复制实现高可用

![1558592619048](./images/mall-34.png)

如图，通过主从架构，一个主节点，两个从节点。

通过手动监控的方式，监控master的宕机，以及出现故障将故障转移的方式可以做到高可用。

比如：如果主节点宕机，我们手动监控到主节点的宕机，并将某一个Slave变成主节点。 但是这样话，如何手动监控也是很麻烦的事情。所以使用sentinel机制就可以解决了这个问题，Sentinel（哨兵）是Redis 的高可用性解决方案。

+ 它能自动进行故障转移。
+ 客户端连接sentinel，不需要关系具体的master。
+ 当master地址改变时由sentinel更新到客户端。

![1558593335136](./images/mall-35.png)

架构原理如图：

1.多个sentinel 发现并确认master有问题。

2.sentinel内部选举领导

3.选举出slave作为新的master

4.通知其余的slave成为新master的slave

5.通知客户端 主从变化

6.如果老的master重新复活，那么成为新的master的slave



要实现上边的功能的主要细节主要有以下三个定时任务：

1. 每10秒，哨兵会向master和slave发送INFO命令(目的就是监控每一个节点信息)
2. 每2秒，哨兵会向master库和slave的频道(__sentinel__:hello)发送自己的信息 （sentinel节点通过__sentinel__:hello频道进行信息交换，比如加入哨兵集群，分享自己节点数据）
3. 每1秒，哨兵会向master和slave以及其他哨兵节点发送PING命令（目的就是 redis节点的状态监控，还有领导选举，主备切换选择等）



策略总结：

​	1.尽量为 每一个节点部署一个哨兵

​	2.哨兵也要搭建集群（防止哨兵单点故障）

​	3.每一个节点都同时设置quorum的值超过半数（N/2）+1



面试常问的问题：

​	主从复制，以及哨兵 和集群之间区别。

主从复制 是redis实现高可用的一个策略。将会有主节点 和从节点，从节点的数据完整的从主节点中复制一份。

哨兵：当系统节点异常宕机的时候，开发者可以手动进行故障转移恢复，但是手动比较麻烦，所以通过哨兵机制自动进行监控和恢复。为了解决哨兵也会单点故障的问题，可以建立哨兵集群。

集群：即使使用哨兵，redis每个实例也是全量存储，每个redis存储的内容都是完整的数据，浪费内存且有木桶效应。为了最大化利用内存，可以采用集群，就是分布式存储。这个时候可以使用redis集群。将不同的数据分配到不同的节点中，这样就可以横向扩展，扩容。



#### 10.4.1.6. redis缓存击穿问题解决

##### 10.4.1.6.1. 什么是缓存击穿

![1558597517593](./images/mall-36.png)

如图：

 	1. 当用户根据key 查询数据时，先查询缓存，如果缓存有命中，返回，
 	2. 但是如果缓存没有命中直接穿过缓存层，访问数据层 如果有，则存储指缓存，
 	3. 但是同样如果没有命中，（也就是数据库中也没有数据）直接返回用户，但是不缓存

这就是缓存的穿透。如果某一个key 请求量很大，但是存储层也没有数据，大量的请求都会达到存储层就会造成数据库压力巨大，有可能宕机的情况。

##### 10.4.1.6.2. 缓存击穿的解决方案

如图：

1.当缓存中没有命中的时候，从数据库中获取

2.当数据库中也没有数据的时候，我们直接将null 作为值设置redis中的key上边。

3.此时如果没有数据，一般情况下都需要设置一个过期时间，例如：5分钟失效。（为了避免过多的KEY 存储在redis中）

4.返回给用户，

5.用户再次访问时，已经有KEY了。此时KEY的值是null而已，这样就可以在缓存中命中，解决了缓存穿透的问题。

![1558681487990](./images/mall-37.png)

（2）例如：代码如下：

![1558684609508](./images/mall-38.png)



注意：缓存空对象会有两个问题：

第一，空值做了缓存，意味着缓存层中存了更多的键，需要更多的内存空间 ( 如果是攻击，问题更严重 )，比较有效的方法是针对这类数据设置一个较短的过期时间，让其自动剔除。

 第二，缓存层和存储层的数据会有一段时间窗口的不一致，可能会对业务有一定影响。例如过期时间设置为 5分钟，如果此时存储层添加了这个数据，那此段时间就会出现缓存层和存储层数据的不一致，此时可以利用消息系统或者其他方式清除掉缓存层中的空对象。



#### 10.4.1.7. Redis缓存雪崩问题解决（作业）

##### 10.4.1.7.1. 什么是缓存雪崩

  如果缓存集中在一段时间内失效，发生大量的缓存穿透，所有的查询都落在数据库上，造成了缓存雪崩。

##### 10.4.1.7.2. 如何解决

这个没有完美解决办法，但可以分析用户行为，尽量让失效时间点均匀分布。

+ 限流 加锁排队

在缓存失效后，通过对某一个key加锁或者是队列 来控制key的线程访问的数量。例如：某一个key 只允许一个线程进行 操作。

+ 限流

在缓存失效后，某一个key 做count统计限流，达到一定的阈值，直接丢弃，不再查询数据库。例如：令牌桶算法。等等。

+ 数据预热

在缓存失效应当尽量避免某一段时间，可以先进行数据预热，比如某些热门的商品。提前在上线之前，或者开放给用户使用之前，先进行loading 缓存中，这样用户使用的时候，直接从缓存中获取。要注意的是，要更加业务来进行过期时间的设置 ，尽量均匀。

+ 做缓存降级（二级缓存策略）

当分布式缓存失效的时候，可以采用本地缓存，本地缓存没有再查询数据库。这种方式，可以避免很多数据分布式缓存没有，就直接打到数据库的情况。



##### 10.4.1.7.3. 二级缓存解决雪崩的案例

分析：

​	基本的思路：通过redis缓存+mybatis的二级缓存整合ehcache来实现。

​	EhCache 是一个纯Java的进程内缓存框架，具有快速、精干等特点，是Hibernate中默认的CacheProvider。



（1）在原来的工程中加入依赖

```xml
<dependency>
    <groupId>org.mybatis.caches</groupId>
    <artifactId>mybatis-ehcache</artifactId>
    <version>1.1.0</version>
</dependency>
```

（2）创建dao的接口 使用XML的方式

```java
@Mapper
public interface TbUserMapper {


    /**
     * 根据用户名查询用户的信息
     * @param username
     * @return
     */
    public TbUser findOne(String username);
}
```

（3）创建TbUserMapper.xml，如图加入echache的配置 开启二级缓存

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.itheima.changgouredisdemo.dao.TbUserMapper">
    <!--加入使用缓存-->
    <cache type="org.mybatis.caches.ehcache.EhcacheCache">
        <!--缓存自创建日期起至失效时的间隔时间一个小时-->
        <property name="timeToIdleSeconds" value="3600"/>
        <!--缓存创建以后，最后一次访问缓存的日期至失效之时的时间间隔一个小时-->
        <property name="timeToLiveSeconds" value="3600"/>
        <!--设置在缓存中保存的对象的最大的个数，这个按照业务进行配置-->
        <property name="maxEntriesLocalHeap" value="1000"/>

        <!--设置在磁盘中最大实体对象的个数-->
        <property name="maxEntriesLocalDisk" value="10000000"/>
        <!--缓存淘汰算法-->
        <property name="memoryStoreEvictionPolicy" value="LRU"/>
    </cache>

    <select id="findOne" resultType="com.itheima.changgouredisdemo.pojo.TbUser" parameterType="string">
          SELECT * from tb_user where username = #{username}
    </select>

</mapper>


```



（4）配置application.yml

![1558754573194](./images/mall-39.png)



```yaml
mybatis:
  configuration:
    map-underscore-to-camel-case: true
    //# 指定mapper映射文件目录
  mapper-locations: classpath:mapper/*Mapper.xml
```



（5）创建controller service 来进行测试：

```java
package com.itheima.changgouredisdemo.controller;

import com.itheima.changgouredisdemo.pojo.TbUser;
import com.itheima.changgouredisdemo.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述
 *
 * @author 三国的包子
 * @version 1.0
 * @package com.itheima.changgouredisdemo.controller *
 * @since 1.0
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/findOne/{username}")
    public TbUser findOne(@PathVariable String username) {
        return userService.findOne(username);
    }

}
```



```
package com.itheima.changgouredisdemo.service.impl;

import com.itheima.changgouredisdemo.dao.TbUserMapper;
import com.itheima.changgouredisdemo.pojo.TbUser;
import com.itheima.changgouredisdemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * 描述
 *
 * @author 三国的包子
 * @version 1.0
 * @package com.itheima.changgouredisdemo.service.impl *
 * @since 1.0
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private TbUserMapper userMapper;
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public TbUser findOne(String username) {
        TbUser user = (TbUser) redisTemplate.boundValueOps(username).get();
        if (redisTemplate.hasKey(username)) {
            return user;
        } else {
            //没有key 数据库中查询
            TbUser one = userMapper.findOne(username);
            System.out.println("第一次查询数据库");
            redisTemplate.boundValueOps(username).set(one);
            if (one == null) {
                redisTemplate.expire(username, 30, TimeUnit.SECONDS);
            }
            return one;
        }
    }
}
```

(6)测试：

已知： 数据库中有zhangsan

![1558754733955](./images/mall-40.png)

准备好redis



浏览器输入

```
http://localhost:9008/user/findOne/zhangsan
```

效果：

![1558754805646](./images/mall-41.png)

redis中:	也有数据

![1558754843688](./images/mall-42.png)

此时：修改数据库的数据zhangsan为zhangsan5,并清空redis缓存。

![1558754887756](./images/mall-43.png)

![1558754905648](./images/mall-44.png)

再次输入浏览器地址：

```
http://localhost:9008/user/findOne/zhangsan
```

此时数据库总已经没有zhangsan,但是效果却是

![1558754956981](./images/mall-45.png)

说明数据从二级缓存中也就是本地缓存中获取到了，测试成功。



