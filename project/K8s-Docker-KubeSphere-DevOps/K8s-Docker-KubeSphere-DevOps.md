<h2>目录</h2>

<details open>
  <summary><a href="#1-云平台核心">1. 云平台核心</a></summary>
  <ul>
  <details open>
    <summary><a href="#11-为什么用云平台">1.1. 为什么用云平台</a>  </summary>
    <ul>
      <a href="#111-公有云">1.1.1. 公有云</a><br>
      <a href="#112-私有云">1.1.2. 私有云</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#2-kubernetes实战入门">2. Kubernetes实战入门</a></summary>
  <ul>
    <a href="#21-是什么">2.1. 是什么</a><br>
  <details open>
    <summary><a href="#22-架构">2.2. 架构</a>  </summary>
    <ul>
      <a href="#221-工作方式">2.2.1. 工作方式</a><br>
    <details open>
      <summary><a href="#222-组件架构">2.2.2. 组件架构</a>    </summary>
      <ul>
      <details open>
        <summary><a href="#2221-控制平面组件（control-plane-components）">2.2.2.1. 控制平面组件（Control Plane Components）</a>      </summary>
        <ul>
          <a href="#22211-kube-apiserver">2.2.2.1.1. kube-apiserver</a><br>
          <a href="#22212-etcd">2.2.2.1.2. etcd</a><br>
          <a href="#22213-kube-scheduler">2.2.2.1.3. kube-scheduler</a><br>
          <a href="#22214-kube-controller-manager">2.2.2.1.4. kube-controller-manager</a><br>
          <a href="#22215-cloud-controller-manager">2.2.2.1.5. cloud-controller-manager</a><br>
        </ul>
      </details>
      <details open>
        <summary><a href="#2222-node-组件">2.2.2.2. Node 组件</a>      </summary>
        <ul>
          <a href="#22221-kubelet">2.2.2.2.1. kubelet</a><br>
          <a href="#22222-kube-proxy">2.2.2.2.2. kube-proxy</a><br>
        </ul>
      </details>
      </ul>
    </details>
    </ul>
  </details>
  <details open>
    <summary><a href="#23-centos下安装docker">2.3. centos下安装docker</a>  </summary>
    <ul>
      <a href="#231-移除以前docker相关包">2.3.1. 移除以前docker相关包</a><br>
      <a href="#232-配置yum源">2.3.2. 配置yum源</a><br>
      <a href="#233-安装docker">2.3.3. 安装docker</a><br>
      <a href="#234-启动并验证docker版本">2.3.4. 启动并验证docker版本</a><br>
      <a href="#235-配置自己的阿里云镜像加速">2.3.5. 配置自己的阿里云镜像加速</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#24-kubeadm创建集群">2.4. kubeadm创建集群</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#241-安装kubeadm">2.4.1. 安装kubeadm</a>    </summary>
      <ul>
        <a href="#2411-基础环境">2.4.1.1. 基础环境</a><br>
        <a href="#2412-安装kubelet、kubeadm、kubectl">2.4.1.2. 安装kubelet、kubeadm、kubectl</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#242-使用kubeadm引导集群">2.4.2. 使用kubeadm引导集群</a>    </summary>
      <ul>
        <a href="#2421-下载各个机器需要的镜像">2.4.2.1. 下载各个机器需要的镜像</a><br>
        <a href="#2422-初始化主节点">2.4.2.2. 初始化主节点</a><br>
      <details open>
        <summary><a href="#2423-根据提示继续">2.4.2.3. 根据提示继续</a>      </summary>
        <ul>
          <a href="#24231-设置kubeconfig">2.4.2.3.1. 设置.kube/config</a><br>
          <a href="#24232-安装网络组件">2.4.2.3.2. 安装网络组件</a><br>
        </ul>
      </details>
        <a href="#2424-加入node节点">2.4.2.4. 加入node节点</a><br>
        <a href="#2425-验证集群">2.4.2.5. 验证集群</a><br>
      <details open>
        <summary><a href="#2426-部署dashboard">2.4.2.6. 部署dashboard</a>      </summary>
        <ul>
          <a href="#24261-部署">2.4.2.6.1. 部署</a><br>
          <a href="#24262-设置访问端口">2.4.2.6.2. 设置访问端口</a><br>
          <a href="#24263-创建访问账号">2.4.2.6.3. 创建访问账号</a><br>
          <a href="#24264-令牌访问">2.4.2.6.4. 令牌访问</a><br>
          <a href="#24265-界面">2.4.2.6.5. 界面</a><br>
        </ul>
      </details>
      </ul>
    </details>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#3-kubernetes核心实战">3. Kubernetes核心实战</a></summary>
  <ul>
    <a href="#31-资源创建方式">3.1. 资源创建方式</a><br>
    <a href="#32-namespace">3.2. Namespace</a><br>
    <a href="#33-pod">3.3. Pod</a><br>
  <details open>
    <summary><a href="#34-deployment">3.4. Deployment</a>  </summary>
    <ul>
      <a href="#341-多副本">3.4.1. 多副本</a><br>
      <a href="#342-扩缩容">3.4.2. 扩缩容</a><br>
      <a href="#343-自愈故障转移">3.4.3. 自愈&故障转移</a><br>
      <a href="#344-滚动更新（不停机更新）">3.4.4. 滚动更新（不停机更新）</a><br>
      <a href="#345-版本回退">3.4.5. 版本回退</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#35-service">3.5. Service</a>  </summary>
    <ul>
      <a href="#351-clusterip集群ip">3.5.1. ClusterIP(集群IP)</a><br>
      <a href="#352-nodeport">3.5.2. NodePort</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#36-ingressservice的统一网关入口，总网关">3.6. Ingress(Service的统一网关入口，总网关)</a>  </summary>
    <ul>
      <a href="#361-安装">3.6.1. 安装</a><br>
    <details open>
      <summary><a href="#362-使用">3.6.2. 使用</a>    </summary>
      <ul>
        <a href="#3621-测试环境">3.6.2.1. 测试环境</a><br>
        <a href="#3622-域名访问">3.6.2.2. 域名访问</a><br>
        <a href="#3623-路径重写会截取路径">3.6.2.3. 路径重写(会截取路径)</a><br>
        <a href="#3624-流量限制">3.6.2.4. 流量限制</a><br>
      </ul>
    </details>
    </ul>
  </details>
  <details open>
    <summary><a href="#37-存储抽象">3.7. 存储抽象</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#371-环境准备">3.7.1. 环境准备</a>    </summary>
      <ul>
        <a href="#3711-所有节点">3.7.1.1. 所有节点</a><br>
        <a href="#3712-主节点">3.7.1.2. 主节点</a><br>
        <a href="#3713-从节点">3.7.1.3. 从节点</a><br>
        <a href="#3714-原生方式数据挂载">3.7.1.4. 原生方式数据挂载</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#372-pvpvc">3.7.2. PV&PVC</a>    </summary>
      <ul>
        <a href="#3721-创建pv池">3.7.2.1. 创建pv池</a><br>
        <a href="#3722-pvc创建与绑定">3.7.2.2. PVC创建与绑定</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#373-configmap">3.7.3. ConfigMap</a>    </summary>
      <ul>
      <details open>
        <summary><a href="#3731-redis示例">3.7.3.1. redis示例</a>      </summary>
        <ul>
          <a href="#37311-把之前的配置文件创建为配置集">3.7.3.1.1. 把之前的配置文件创建为配置集</a><br>
          <a href="#37312-创建pod">3.7.3.1.2. 创建Pod</a><br>
          <a href="#37313-检查默认配置">3.7.3.1.3. 检查默认配置</a><br>
          <a href="#37314-修改configmap">3.7.3.1.4. 修改ConfigMap</a><br>
          <a href="#37315-检查配置是否更新">3.7.3.1.5. 检查配置是否更新</a><br>
        </ul>
      </details>
      </ul>
    </details>
      <a href="#374-secret">3.7.4. Secret</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#4-kubernetes上安装kubesphere">4. Kubernetes上安装KubeSphere</a></summary>
  <ul>
    <a href="#41-安装docker（安装过就不用安装了）">4.1. 安装Docker（安装过就不用安装了）</a><br>
  <details open>
    <summary><a href="#42-安装kubernetes（安装过就不用安装了）">4.2. 安装Kubernetes（安装过就不用安装了）</a>  </summary>
    <ul>
      <a href="#421-基本环境">4.2.1. 基本环境</a><br>
      <a href="#422-安装kubelet、kubeadm、kubectl">4.2.2. 安装kubelet、kubeadm、kubectl</a><br>
    <details open>
      <summary><a href="#423-初始化master节点">4.2.3. 初始化master节点</a>    </summary>
      <ul>
        <a href="#4231-初始化">4.2.3.1. 初始化</a><br>
        <a href="#4232-记录关键信息">4.2.3.2. 记录关键信息</a><br>
        <a href="#4233-安装calico网络插件">4.2.3.3. 安装Calico网络插件</a><br>
      </ul>
    </details>
    </ul>
  </details>
  <details open>
    <summary><a href="#43-安装kubesphere前置环境">4.3. 安装KubeSphere前置环境</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#431-nfs文件系统">4.3.1. nfs文件系统</a>    </summary>
      <ul>
        <a href="#4311-安装nfs-server">4.3.1.1. 安装nfs-server</a><br>
        <a href="#4312-配置nfs-client（选做，因为kubesphere是动态分配，所以不用手动绑定）">4.3.1.2. 配置nfs-client（选做，因为Kubesphere是动态分配，所以不用手动绑定）</a><br>
        <a href="#4313-配置默认存储">4.3.1.3. 配置默认存储</a><br>
        <a href="#4314-验证默认存储类是否生效">4.3.1.4. 验证默认存储类是否生效</a><br>
      </ul>
    </details>
      <a href="#432-metrics-server">4.3.2. metrics-server</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#44-安装kubesphere">4.4. 安装KubeSphere</a>  </summary>
    <ul>
      <a href="#441-下载核心文件">4.4.1. 下载核心文件</a><br>
      <a href="#442-修改cluster-configuration">4.4.2. 修改cluster-configuration</a><br>
      <a href="#443-执行安装">4.4.3. 执行安装</a><br>
      <a href="#444-查看安装进度">4.4.4. 查看安装进度</a><br>
      <a href="#445-卸载kubesphere">4.4.5. 卸载Kubesphere</a><br>
    <details open>
      <summary><a href="#446-附录">4.4.6. 附录</a>    </summary>
      <ul>
        <a href="#4461-kubesphere-installeryaml">4.4.6.1. kubesphere-installer.yaml</a><br>
        <a href="#4462-cluster-configurationyaml">4.4.6.2. cluster-configuration.yaml</a><br>
      </ul>
    </details>
    </ul>
  </details>
  </ul>
</details>


<h1>K8s-Docker-KubeSphere-DevOps</h1>

# 1. 云平台核心

## 1.1. 为什么用云平台

- 环境统一
- 按需付费 

- 即开即用 
- 稳定性强

- ......

### 1.1.1. 公有云

**购买云服务商提供的公共服务器**

公有云是最常见的云计算部署类型。公有云资源（例如服务器和存储空间）由第三方云服务提供商拥有和运营，这些资源通过 Internet 提供。在公有云中，所有硬件、软件和其他支持性基础结构均为云提供商所拥有和管理。Microsoft Azure 是公有云的一个示例。

在公有云中，你与其他组织或云“租户”共享相同的硬件、存储和网络设备，并且你可以使用 Web 浏览器访问服务和管理帐户。公有云部署通常用于提供基于 Web 的电子邮件、网上办公应用、存储以及测试和开发环境。

公有云优势：

- **成本更低**：无需购买硬件或软件，仅对使用的服务付费。
- **无需维护**：维护由服务提供商提供。

- **近乎无限制的缩放性**：提供按需资源，可满足业务需求。
- **高可靠性**：具备众多服务器，确保免受故障影响。

- - 可用性： N个9   9   全年的故障时间： 365*24*3600*(1-99.9999%)

### 1.1.2. 私有云

**自己搭建云平台，或者购买**

私有云由专供一个企业或组织使用的云计算资源构成。私有云可在物理上位于组织的现场数据中心，也可由第三方服务提供商托管。但是，在私有云中，服务和基础结构始终在私有网络上进行维护，硬件和软件专供组织使用。

这样，私有云可使组织更加方便地自定义资源，从而满足特定的 IT 需求。私有云的使用对象通常为政府机构、金融机构以及其他具备业务关键性运营且希望对环境拥有更大控制权的中型到大型组织。

私有云优势：

- **灵活性更强**：组织可自定义云环境以满足特定业务需求。
- **控制力更强**：资源不与其他组织共享，因此能获得更高的控制力以及更高的隐私级别。

- **可伸缩性更强**：与本地基础结构相比，私有云通常具有更强的可伸缩性。

# 2. Kubernetes实战入门

## 2.1. 是什么

![img](./images/K8s-Docker-KubeSphere-DevOps-1.png)

我们急需一个大规模容器编排系统

kubernetes具有以下特性：

- **服务发现和负载均衡**
  Kubernetes 可以使用 DNS 名称或自己的 IP 地址公开容器，如果进入容器的流量很大， Kubernetes 可以负载均衡并分配网络流量，从而使部署稳定。
- **存储编排**
  Kubernetes 允许你自动挂载你选择的存储系统，例如本地存储、公共云提供商等。

- **自动部署和回滚**
  你可以使用 Kubernetes 描述已部署容器的所需状态，它可以以受控的速率将实际状态 更改为期望状态。例如，你可以自动化 Kubernetes 来为你的部署创建新容器， 删除现有容器并将它们的所有资源用于新容器。
- **自动完成装箱计算**
  Kubernetes 允许你指定每个容器所需 CPU 和内存（RAM）。 当容器指定了资源请求时，Kubernetes 可以做出更好的决策来管理容器的资源。

- **自我修复**
  Kubernetes 重新启动失败的容器、替换容器、杀死不响应用户定义的 运行状况检查的容器，并且在准备好服务之前不将其通告给客户端。
- **密钥与配置管理**
  Kubernetes 允许你存储和管理敏感信息，例如密码、OAuth 令牌和 ssh 密钥。 你可以在不重建容器镜像的情况下部署和更新密钥和应用程序配置，也无需在堆栈配置中暴露密钥。

**Kubernetes 为你提供了一个可弹性运行分布式系统的框架。 Kubernetes 会满足你的扩展要求、故障转移、部署模式等。 例如，Kubernetes 可以轻松管理系统的 Canary 部署。**

## 2.2. 架构

### 2.2.1. 工作方式

Kubernetes **Cluster** **=** N **Master** Node **+** N **Worker** Node：N主节点+N工作节点； N>=1

### 2.2.2. 组件架构

![img](./images/K8s-Docker-KubeSphere-DevOps-2.png)

#### 2.2.2.1. 控制平面组件（Control Plane Components） 

控制平面的组件对集群做出全局决策(比如调度)，以及检测和响应集群事件（例如，当不满足部署的 `replicas` 字段时，启动新的 [pod](https://kubernetes.io/docs/concepts/workloads/pods/pod-overview/)）。

控制平面组件可以在集群中的任何节点上运行。 然而，为了简单起见，设置脚本通常会在同一个计算机上启动所有控制平面组件， 并且不会在此计算机上运行用户容器。 请参阅[使用 kubeadm 构建高可用性集群](https://kubernetes.io/zh/docs/setup/production-environment/tools/kubeadm/high-availability/) 中关于多 VM 控制平面设置的示例。

##### 2.2.2.1.1. kube-apiserver

API 服务器是 Kubernetes [控制面](https://kubernetes.io/zh/docs/reference/glossary/?all=true#term-control-plane)的组件， 该组件公开了 Kubernetes API。 API 服务器是 Kubernetes 控制面的前端。

Kubernetes API 服务器的主要实现是 [kube-apiserver](https://kubernetes.io/zh/docs/reference/command-line-tools-reference/kube-apiserver/)。 kube-apiserver 设计上考虑了水平伸缩，也就是说，它可通过部署多个实例进行伸缩。 你可以运行 kube-apiserver 的多个实例，并在这些实例之间平衡流量。

##### 2.2.2.1.2. etcd

etcd 是**兼具一致性和高可用性的键值数据库**，可以作为保存 Kubernetes 所有集群数据的后台数据库。

您的 Kubernetes 集群的 etcd 数据库通常需要有个备份计划。

##### 2.2.2.1.3. kube-scheduler

控制平面组件，负责监视新创建的、未指定运行[节点（node）](https://kubernetes.io/zh/docs/concepts/architecture/nodes/)的 [Pods](https://kubernetes.io/docs/concepts/workloads/pods/pod-overview/)，选择节点让 Pod 在上面运行。

调度决策考虑的因素包括单个 Pod 和 Pod 集合的资源需求、硬件/软件/策略约束、亲和性和反亲和性规范、数据位置、工作负载间的干扰和最后时限。

##### 2.2.2.1.4. kube-controller-manager

在主节点上运行 [控制器](https://kubernetes.io/zh/docs/concepts/architecture/controller/) 的组件。

从逻辑上讲，每个[控制器](https://kubernetes.io/zh/docs/concepts/architecture/controller/)都是一个单独的进程， 但是为了降低复杂性，它们都被编译到同一个可执行文件，并在一个进程中运行。

这些控制器包括:

- 节点控制器（Node Controller）: 负责在节点出现故障时进行通知和响应
- 任务控制器（Job controller）: 监测代表一次性任务的 Job 对象，然后创建 Pods 来运行这些任务直至完成

- 端点控制器（Endpoints Controller）: 填充端点(Endpoints)对象(即加入 Service 与 Pod)
- 服务帐户和令牌控制器（Service Account & Token Controllers）: 为新的命名空间创建默认帐户和 API 访问令牌

##### 2.2.2.1.5. cloud-controller-manager

云控制器管理器是指嵌入特定云的控制逻辑的 [控制平面](https://kubernetes.io/zh/docs/reference/glossary/?all=true#term-control-plane)组件。 云控制器管理器允许您链接集群到云提供商的应用编程接口中， 并把和该云平台交互的组件与只和您的集群交互的组件分离开。

`cloud-controller-manager` 仅运行特定于云平台的控制回路。 如果你在自己的环境中运行 Kubernetes，或者在本地计算机中运行学习环境， 所部署的环境中不需要云控制器管理器。

与 `kube-controller-manager` 类似，`cloud-controller-manager` 将若干逻辑上独立的 控制回路组合到同一个可执行文件中，供你以同一进程的方式运行。 你可以对其执行水平扩容（运行不止一个副本）以提升性能或者增强容错能力。

下面的控制器都包含对云平台驱动的依赖：

- 节点控制器（Node Controller）: 用于在节点终止响应后检查云提供商以确定节点是否已被删除
- 路由控制器（Route Controller）: 用于在底层云基础架构中设置路由

- 服务控制器（Service Controller）: 用于创建、更新和删除云提供商负载均衡器

#### 2.2.2.2. Node 组件 

节点组件在每个节点上运行，维护运行的 Pod 并提供 Kubernetes 运行环境。

##### 2.2.2.2.1. kubelet

一个在集群中每个[节点（node）](https://kubernetes.io/zh/docs/concepts/architecture/nodes/)上运行的代理。 它保证[容器（containers）](https://kubernetes.io/zh/docs/concepts/overview/what-is-kubernetes/#why-containers)都 运行在 [Pod](https://kubernetes.io/docs/concepts/workloads/pods/pod-overview/) 中。

kubelet 接收一组通过各类机制提供给它的 PodSpecs，确保这些 PodSpecs 中描述的容器处于运行状态且健康。 kubelet 不会管理不是由 Kubernetes 创建的容器。

##### 2.2.2.2.2. kube-proxy

[kube-proxy](https://kubernetes.io/zh/docs/reference/command-line-tools-reference/kube-proxy/) 是集群中每个节点上运行的网络代理， 实现 Kubernetes [服务（Service）](https://kubernetes.io/zh/docs/concepts/services-networking/service/) 概念的一部分。

kube-proxy 维护节点上的网络规则。这些网络规则允许从集群内部或外部的网络会话与 Pod 进行网络通信。

如果操作系统提供了数据包过滤层并可用的话，kube-proxy 会通过它来实现网络规则。否则， kube-proxy 仅转发流量本身。

![img](./images/K8s-Docker-KubeSphere-DevOps-3.png)

## 2.3. centos下安装docker

### 2.3.1. 移除以前docker相关包

```bash
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
```

### 2.3.2. 配置yum源

```bash
sudo yum install -y yum-utils
sudo yum-config-manager \
--add-repo \
http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```

### 2.3.3. 安装docker

```bash
sudo yum install -y docker-ce docker-ce-cli containerd.io


// 以下是在安装k8s的时候使用, 也是比较新的Docker版本，为了兼容k8s
// 有可能最新的包不兼容该版本的Docker，尝试在命令行中添加 '--allowerasing' 来替换冲突的软件包 或 '--skip-broken' 来跳过无法安装的软件包 或 '--nobest' 来不只使用软件包的最佳候选
yum install -y docker-ce-20.10.7 docker-ce-cli-20.10.7  containerd.io-1.4.6 --allowerasing
```

### 2.3.4. 启动并验证docker版本

```bash
systemctl enable docker --now
docker -v
```

### 2.3.5. 配置自己的阿里云镜像加速

这里额外添加了docker的生产环境核心配置cgroup

```bash
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://w4plc3h7.mirror.aliyuncs.com"],
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
sudo docker info //可以检查是否修改好了Docker镜像地址
```



## 2.4. kubeadm创建集群

请参照以前Docker安装。先提前为所有机器安装Docker

### 2.4.1. 安装kubeadm

**一般开发或者测试都需要3三台可连接的公有云、私有云或者虚拟机！！！**

我在本地开发测试新创建了一台2GB RAM(内存) 2CPU 核的CentOS服务器（CentOS-Stream-8-x86_64-20211117-boot.iso），用的是**NAT模式**。

并复制上面的虚拟机，**复制的新虚拟机只需要修改里面的静态IP地址就好**，并且主机名，UUID不能相同

会自动生成下面的网卡ifcfg-ens*，但是需要用下面的命令把这个默认网卡修改成静态IP，并保存。

vim /etc/sysconfig/network-scripts/ifcfg-ens*

修改和新增配置：

```
// 修改
BOOTPROTO=static  //静态ip
ONBOOT=yes #将no改为yes
UUID=06cc84d6-a5f9-4055-88a2-d3b854d0ce5f // uuidgen ens37, 随机生成UUID, ens37是网卡名字
// 新增
// 设置网关，因为选择的是NAT模A，所以选择的是vmnet8虚拟网卡，Mac上执行下面的命令即可查看到对应vmnet8虚拟网卡的网关IP和子网掩码
// cat /Library/Preferences/VMware\ Fusion/vmnet8/nat.conf | grep gateway -A 2
NETMASK=255.255.255.0  #设置子网掩码
GATEWAY=172.16.26.2   #设置网关，需要和Mac、Windows电脑的网关一致！！！就是上面vmnet8虚拟网卡的网关IP
IPADDR=172.16.26.3 #设置centos IP静态地址地址，这时候静态IP自定义范围是172.16.26.3~172.16.26.255
//DNS1=114.114.114.114 #设置dns，也可以不设置
```



备份镜像在分配的静态IP地址分别如下：

```
172.16.26.3 // master
172.16.26.4 // node1
172.16.26.5 // node2
```

**注意：由于当时使用3台虚拟机模拟3台服务器的，所以在在复制虚拟机的时候可能忽略了解析DNS的配置文件/etc/resolv.conf，需要配置网关以正确解析DNS，如果不配置可能导致k8s部署的pod不能够解析DNS以至于不能访问外网**

```bash
[root@k8s-node1 ~]# cat /etc/resolv.conf
# Generated by NetworkManager
nameserver 172.16.26.2
```



- 一台兼容的 Linux 主机。Kubernetes 项目为基于 Debian 和 Red Hat 的 Linux 发行版以及一些不提供包管理器的发行版提供通用的指令
- 每台机器 2 GB 或更多的 RAM （如果少于这个数字将会影响你应用的运行内存)

- 2 CPU 核或更多
- 集群中的所有机器的网络彼此均能相互连接(公网和内网都可以)
  - **设置防火墙放行规则**

- 节点之中不可以有重复的主机名、MAC 地址或 product_uuid。请参见[这里](https://kubernetes.io/zh/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#verify-mac-address)了解更多详细信息。
  - **设置不同hostname**

- 开启机器上的某些端口。请参见[这里](https://kubernetes.io/zh/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#check-required-ports) 了解更多详细信息。

- - **内网互信**

- 禁用交换分区。为了保证 kubelet 正常工作，你 **必须** 禁用交换分区。
  - **永久关闭**


#### 2.4.1.1. 基础环境

所有机器执行以下操作

```bash
#各个机器分别设置自己对应的域名，为了使其有意义方便识别
hostnamectl set-hostname k8s-master
hostnamectl set-hostname k8s-node1
hostnamectl set-hostname k8s-node2

# 将 SELinux(Linux最新的安全组件)设置为 permissive 模式（相当于将其禁用），这是Linux的一些安全设置，需要将其禁用，方便后面的开发
# 临时禁用
sudo setenforce 0
# 永久禁用
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
# 禁用防火墙，非常重要
systemctl disable firewalld --now

# 关闭swap交换区
# 临时关闭
swapoff -a
# 永久关闭
sed -ri 's/.*swap.*/#&/' /etc/fstab

#允许 iptables 检查桥接流量，IPv6的流量桥接到IPv4上（k8s官方要求这样做的）
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
br_netfilter
EOF

# 使上面的配置生效
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system
```



#### 2.4.1.2. 安装kubelet、kubeadm、kubectl

```bash
# 配置k8s去哪里(阿里云镜像)下载
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=http://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=http://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg
   http://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF

# 安装最新的k8s，但是有可能和最新的Docker不兼容，可以用下面一条命令，是最新兼容Docker的版本
sudo yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
#sudo yum install -y kubelet kubeadm-1.20.9 kubectl-1.20.9 --disableexcludes=kubernetes

# 启动k8s
sudo systemctl enable --now kubelet

# 验证k8s组件版本
kubeadm version
kubectl version --client
kubelet --version

```

kubelet 现在每隔几秒就会重启，因为它陷入了一个等待 kubeadm 指令的死循环

### 2.4.2. 使用kubeadm引导集群

#### 2.4.2.1. 下载各个机器需要的镜像

```bash
# 在节点上新增镜像下载脚本文件。
sudo tee ./images.sh <<-'EOF'
#!/bin/bash
images=(
kube-apiserver:v1.20.9
kube-proxy:v1.20.9
kube-controller-manager:v1.20.9
kube-scheduler:v1.20.9
coredns:1.7.0
etcd:3.4.13-0
pause:3.2
)
for imageName in ${images[@]} ; do
docker pull registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/$imageName
done
EOF

# 赋予文件执行权限并执行脚本文件安装镜像
chmod +x ./images.sh && ./images.sh

# 保险起见，可以用下面的命令查看镜像是否安装好
docker images

// 安装成功的话，大概有下面7个镜像
[root@localhost ~]# docker images
REPOSITORY                                                                 TAG        IMAGE ID       CREATED         SIZE
registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/kube-proxy                v1.20.9    8dbf9a6aa186   4 months ago    99.7MB
registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/kube-apiserver            v1.20.9    0d0d57e4f64c   4 months ago    122MB
registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/kube-controller-manager   v1.20.9    eb07fd4ad3b4   4 months ago    116MB
registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/kube-scheduler            v1.20.9    295014c114b3   4 months ago    47.3MB
registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/etcd                      3.4.13-0   0369cf4303ff   15 months ago   253MB
registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/coredns                   1.7.0      bfe3a36ebd25   17 months ago   45.2MB
registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/pause                     3.2        80d28bedfe5d   21 months ago   683kB
```

#### 2.4.2.2. 初始化主节点

```bash
// 所有机器添加所有节点域名映射，尤其是master节点，是为了让所有机器知道master节点在哪里，以下需要修改为自己的，在master台机器上通过ip a命令查看内网ip
echo "172.16.26.3  cluster-endpoint" >> /etc/hosts
echo "172.16.26.4  k8s-node1" >> /etc/hosts
echo "172.16.26.5  k8s-node1" >> /etc/hosts

// 能执行下面命令ping通cluster-endpoint的话，说明上面的hosts配置成功
ping cluster-endpoint

// 主节点初始化，只需要在Master节点运行！！！！
// apiserver-advertise-address是master节点所在服务器的IP
// control-plane-endpoint是上面的配置的IP地址域名
// image-repository，这个是镜像地址，由于国外地址无法访问，故使用的阿里云仓库地址：registry.aliyuncs.com/google_containers
// kubernetes-version，这个参数是下载的k8s软件版本号
// service-cidr=10.96.0.0/16是做k8s内部做负载均衡的，以后安装时也套用即可，最好不要改
// pod-network-cidr=192.168.0.0/16，k8s内部的pod节点之间网络可以使用的IP段，需要和上面的service-cidr网络范围不重叠，最好也不要改，因为这是后面要安装组件calico相关的默认配置，不然安装calico组件时候需要修改CALICO_IPV4POOL_CIDR变量

kubeadm init \
--apiserver-advertise-address=172.16.26.3 \
--control-plane-endpoint=cluster-endpoint \
--image-repository registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images \
--kubernetes-version v1.20.9 \
--service-cidr=10.96.0.0/16 \
--pod-network-cidr=192.168.0.0/16

// 所有网络范围不重叠！！！！
```



```bash
// 上面的命令在Master节点运行成功后，需要你复制并粘贴下面的显示成功初始化的日志，并按照上面说的继续操作！！！
Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

You can now join any number of control-plane nodes by copying certificate authorities
and service account keys on each node and then running the following as root:

  kubeadm join cluster-endpoint:6443 --token aoheax.80arwz7jmez29tlc \
    --discovery-token-ca-cert-hash sha256:70239d9bc2705f2165a085e04ccb7b2f1124a2823477aff01bcd2c8b39448f17 \
    --control-plane 

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join cluster-endpoint:6443 --token aoheax.80arwz7jmez29tlc \
    --discovery-token-ca-cert-hash sha256:70239d9bc2705f2165a085e04ccb7b2f1124a2823477aff01bcd2c8b39448f17 
```



```
// 正如上面所说的，需要在master节点上运行下面的命令
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
// 如果你是root用户也可以直接运行下面的命令达到和上面命令一样的效果
export KUBECONFIG=/etc/kubernetes/admin.conf
```





```bash
// 在Master节点执行下面的命令，下载calico组件配置文件
curl https://docs.projectcalico.org/manifests/calico.yaml -O
// 在Master节点执行下面命令应用calico
kubectl apply -f calico.yaml
```



```bash
// 至今为止学习了下面的命令

// 执行前面的命令后，查看集群所有节点，
kubectl get nodes

// 根据配置文件，给集群创建资源，xxxx可以是calico
kubectl apply -f xxxx.yaml

// 查看集群部署了哪些应用？
docker ps   ===   kubectl get pods -A
// 运行中的应用在docker里面叫容器，在k8s里面叫Pod
kubectl get pods -A
```

#### 2.4.2.3. 根据提示继续

##### 2.4.2.3.1. 设置.kube/config

```
// 在前面已经在master节点中运行过下面命令，忽略！！
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

##### 2.4.2.3.2. 安装网络组件

[calico官网](https://docs.projectcalico.org/getting-started/kubernetes/self-managed-onprem/onpremises#install-calico-with-kubernetes-api-datastore-more-than-50-nodes)

```bash
// 在前面已经在master节点中运行过下面命令，忽略！！
curl https://docs.projectcalico.org/manifests/calico.yaml -O

kubectl apply -f calico.yaml
```

#### 2.4.2.4. 加入node节点

```bash
// 上面控制台信息最后的命令，24小时内有效，在两个node节点中运行，使其加入集群
kubeadm join cluster-endpoint:6443 --token aoheax.80arwz7jmez29tlc \
    --discovery-token-ca-cert-hash sha256:70239d9bc2705f2165a085e04ccb7b2f1124a2823477aff01bcd2c8b39448f17

// 如果Token过期，可以在Master节点执行下面的命令重新生成加入集群的token新令牌，把生成的命令在node节点中运行。高可用部署方式，也是在这一步的时候，使用添加主节点的命令即可
kubeadm token create --print-join-command
```

#### 2.4.2.5. 验证集群

- 验证集群节点状态
  - kubectl get nodes

#### 2.4.2.6. 部署dashboard

##### 2.4.2.6.1. 部署

kubernetes官方提供的可视化界面

https://github.com/kubernetes/dashboard

```bash
// 在Master节点上运行下面的命令创建部署dashboard，我在Gitlab上fork了kubernetes/dashboard在Github上的repository，下面可以替换成下面的url，这样就可正常下载了
kubectl apply -f https://gitlab.com/yanglin5260/dashboard/-/raw/v2.4.0/aio/deploy/recommended.yaml

# kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.4.0/aio/deploy/recommended.yaml

// 上面的命令可能下载yaml文件都很慢，所以需要执行下面的命令先下载yaml文件，再应用yaml文件
wget https://raw.githubusercontent.com/kubernetes/dashboard/v2.3.1/aio/deploy/recommended.yaml
kubectl apply -f recommended.yaml
kubectl apply -f kubernetes-dashboard.yaml
```



上面的命令实在不能下载yaml文件，可以下面的文件内容，文件名可以是kubernetes-dashboard-recommended.yaml。下面的文件修改了kubernetes-dashboard对外的固定端口号30001（nodePort: 30001），端口类型为NodePort（type: NodePort），并覆盖文件kubernetes-dashboard-recommended.yaml

```yml
echo "# Copyright 2017 The Kubernetes Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

apiVersion: v1
kind: Namespace
metadata:
  name: kubernetes-dashboard

---

apiVersion: v1
kind: ServiceAccount
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard

---

kind: Service
apiVersion: v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
spec:
  ports:
    - port: 443
      targetPort: 8443
      nodePort: 30001
  selector:
    k8s-app: kubernetes-dashboard
  type: NodePort
---

apiVersion: v1
kind: Secret
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard-certs
  namespace: kubernetes-dashboard
type: Opaque

---

apiVersion: v1
kind: Secret
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard-csrf
  namespace: kubernetes-dashboard
type: Opaque
data:
  csrf: ""

---

apiVersion: v1
kind: Secret
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard-key-holder
  namespace: kubernetes-dashboard
type: Opaque

---

kind: ConfigMap
apiVersion: v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard-settings
  namespace: kubernetes-dashboard

---

kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
rules:
  # Allow Dashboard to get, update and delete Dashboard exclusive secrets.
  - apiGroups: [""]
    resources: ["secrets"]
    resourceNames: ["kubernetes-dashboard-key-holder", "kubernetes-dashboard-certs", "kubernetes-dashboard-csrf"]
    verbs: ["get", "update", "delete"]
    # Allow Dashboard to get and update 'kubernetes-dashboard-settings' config map.
  - apiGroups: [""]
    resources: ["configmaps"]
    resourceNames: ["kubernetes-dashboard-settings"]
    verbs: ["get", "update"]
    # Allow Dashboard to get metrics.
  - apiGroups: [""]
    resources: ["services"]
    resourceNames: ["heapster", "dashboard-metrics-scraper"]
    verbs: ["proxy"]
  - apiGroups: [""]
    resources: ["services/proxy"]
    resourceNames: ["heapster", "http:heapster:", "https:heapster:", "dashboard-metrics-scraper", "http:dashboard-metrics-scraper"]
    verbs: ["get"]

---

kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
rules:
  # Allow Metrics Scraper to get metrics from the Metrics server
  - apiGroups: ["metrics.k8s.io"]
    resources: ["pods", "nodes"]
    verbs: ["get", "list", "watch"]

---

apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: kubernetes-dashboard
subjects:
  - kind: ServiceAccount
    name: kubernetes-dashboard
    namespace: kubernetes-dashboard

---

apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: kubernetes-dashboard
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: kubernetes-dashboard
subjects:
  - kind: ServiceAccount
    name: kubernetes-dashboard
    namespace: kubernetes-dashboard

---

kind: Deployment
apiVersion: apps/v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
spec:
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      k8s-app: kubernetes-dashboard
  template:
    metadata:
      labels:
        k8s-app: kubernetes-dashboard
    spec:
      containers:
        - name: kubernetes-dashboard
          image: kubernetesui/dashboard:v2.4.0
          imagePullPolicy: Always
          ports:
            - containerPort: 8443
              protocol: TCP
          args:
            - --auto-generate-certificates
            - --namespace=kubernetes-dashboard
            # Uncomment the following line to manually specify Kubernetes API server Host
            # If not specified, Dashboard will attempt to auto discover the API server and connect
            # to it. Uncomment only if the default does not work.
            # - --apiserver-host=http://my-address:port
          volumeMounts:
            - name: kubernetes-dashboard-certs
              mountPath: /certs
              # Create on-disk volume to store exec logs
            - mountPath: /tmp
              name: tmp-volume
          livenessProbe:
            httpGet:
              scheme: HTTPS
              path: /
              port: 8443
            initialDelaySeconds: 30
            timeoutSeconds: 30
          securityContext:
            allowPrivilegeEscalation: false
            readOnlyRootFilesystem: true
            runAsUser: 1001
            runAsGroup: 2001
      volumes:
        - name: kubernetes-dashboard-certs
          secret:
            secretName: kubernetes-dashboard-certs
        - name: tmp-volume
          emptyDir: {}
      serviceAccountName: kubernetes-dashboard
      nodeSelector:
        "kubernetes.io/os": linux
      # Comment the following tolerations if Dashboard must not be deployed on master
      tolerations:
        - key: node-role.kubernetes.io/master
          effect: NoSchedule

---

kind: Service
apiVersion: v1
metadata:
  labels:
    k8s-app: dashboard-metrics-scraper
  name: dashboard-metrics-scraper
  namespace: kubernetes-dashboard
spec:
  ports:
    - port: 8000
      targetPort: 8000
  selector:
    k8s-app: dashboard-metrics-scraper

---

kind: Deployment
apiVersion: apps/v1
metadata:
  labels:
    k8s-app: dashboard-metrics-scraper
  name: dashboard-metrics-scraper
  namespace: kubernetes-dashboard
spec:
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      k8s-app: dashboard-metrics-scraper
  template:
    metadata:
      labels:
        k8s-app: dashboard-metrics-scraper
    spec:
      securityContext:
        seccompProfile:
          type: RuntimeDefault
      containers:
        - name: dashboard-metrics-scraper
          image: kubernetesui/metrics-scraper:v1.0.7
          ports:
            - containerPort: 8000
              protocol: TCP
          livenessProbe:
            httpGet:
              scheme: HTTP
              path: /
              port: 8000
            initialDelaySeconds: 30
            timeoutSeconds: 30
          volumeMounts:
          - mountPath: /tmp
            name: tmp-volume
          securityContext:
            allowPrivilegeEscalation: false
            readOnlyRootFilesystem: true
            runAsUser: 1001
            runAsGroup: 2001
      serviceAccountName: kubernetes-dashboard
      nodeSelector:
        "kubernetes.io/os": linux
      # Comment the following tolerations if Dashboard must not be deployed on master
      tolerations:
        - key: node-role.kubernetes.io/master
          effect: NoSchedule
      volumes:
        - name: tmp-volume
          emptyDir: {}
" > kubernetes-dashboard-recommended.yaml
```

##### 2.4.2.6.2. 设置访问端口

```bash
// 在master节点上运行下面命令设置访问端口，将type: ClusterIP改为type: NodePort，并保存退出。这一步相当于把kubernetes-dashboard Web访问界面的端口暴露出来
kubectl edit svc kubernetes-dashboard -n kubernetes-dashboard

// 接着在master节点运行下面命令
kubectl get svc -A |grep kubernetes-dashboard

// 运行后可以找到kubernetes-dashboard暴露出来的端口是32766
[root@localhost ~]# kubectl get svc -A |grep kubernetes-dashboard
kubernetes-dashboard   dashboard-metrics-scraper   ClusterIP   10.96.60.168   <none>        8000/TCP                 46s
kubernetes-dashboard   kubernetes-dashboard        NodePort    10.96.223.26   <none>        443:32766/TCP            46s

// 之后，可以通过https://172.16.26.3:32766访问kubernetes dashboard Web界面，这时候出现需要你输入Token来激活Kubernetes dashboard，你需要通过后续的操作生成Token
// 其中IP地址可以是集群中的任意节点的IP地址，但是在本地利用虚拟机测试时候，Chrome、IE、Safari浏览器访问时候是不能访问的，因为访问需要的是HTTPS协议，唯有 Firefox 才能解忧，才能访问。不过后面租用了云服务器应该就不会出现这样的问题了
```

##### 2.4.2.6.3. 创建访问账号

在Master节点准备一个yaml文件，为了创建一个访问者身份账号

```bash
vi dash.yaml
```

文件内容如下：

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kubernetes-dashboard
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kubernetes-dashboard

```

接着在Master上运行下面的命令

```bash
kubectl apply -f dash.yaml
kubectl apply -f https://kuboard.cn/install-script/k8s-dashboard/auth.yaml
```

##### 2.4.2.6.4. 令牌访问

```bash
// 上面的命令运行成功后，在master运行下面的命令获取访问令牌，为了解锁Kubernetes Dashboard
kubectl -n kubernetes-dashboard get secret $(kubectl -n kubernetes-dashboard get sa/admin-user -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"
```

获取的Token如下：

```json
eyJhbGciOiJSUzI1NiIsImtpZCI6InUyenlFdFJ6V3BSbHlHSlFyMENocVlSem5KcERGSWYwWEFCX2pNUXVyRlUifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlcm5ldGVzLWRhc2hib2FyZCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJhZG1pbi11c2VyLXRva2VuLTg4czl6Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImFkbWluLXVzZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJlYWI3Y2I5MS04N2FmLTQ3ZmUtYjc4NS00NGUyOWJiMDQ1ZGYiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZXJuZXRlcy1kYXNoYm9hcmQ6YWRtaW4tdXNlciJ9.PeABA-dBmwtby5iG6qBPU-SJ3hi7JD2pWow1zNOg9ddeaHzqbwNFMOGCzyhdL9lan7XoTK4S_OzTFHtNZVhbF52HK1Q-kagHjdw9is4czHhr1EuEC2dEgzSlbOI0iRfXunrsizJWMumF-wuKjmG7Yo7TJ5wL0p0RsY_XhjHgzBE-A1QEG7qTIKmxIbhe_YaASdOApnFF_7BLuQmYt1FhDTDxg-Ww9jKwSoV2u9wPmfrQlWIoD3SfLcIIXhmLb62dv4vNVNPE-JCWcBFOvs3l7qmLg6aleE-RTSh2Ai28lE-_VSdWdDxPC4ht0rEBSBpfJwWlw1ov3VR2BYuwCmPdTA
```

##### 2.4.2.6.5. 界面

![image.png](./images/K8s-Docker-KubeSphere-DevOps-5.png)

# 3. Kubernetes核心实战

## 3.1. 资源创建方式

- 命令行
- YAML 

## 3.2. Namespace

名称空间用来隔离资源，默认只隔离资源，不隔离网络

```bash
// 通过下面的命令获取所有Namespace名称空间
kubectl get ns


// 运行中的应用在docker里面叫容器，在k8s里面叫Pod
kubectl get pods -A
// 但是下面命令只获取默认Namespace default下面的资源pod
kubectl get pods
// 而下面命令可以获取指定Namespace的Pod资源，比如获取Namespace为kubernetes-dashboard的pod
kubectl get pods -n kubernetes-dashboard

// 下面的命令可以创建和删除命名空间，删除会删除命名空间下的所有资源
kubectl create ns hello
kubectl delete ns hello
```

也可以通过yaml配置文件的方式创建命名空间，通过apply命令应用并创建，用delete命令基于yaml文件删除空间

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: hello
```

## 3.3. Pod

**运行中的一组容器**，Pod是kubernetes中应用的最小单位。

![img](./images/K8s-Docker-KubeSphere-DevOps-6.png)

```bash
// 创建第一个Pod
kubectl run mynginx --image=nginx

// 查看default名称空间的Pod，查看你上面创建的pod
kubectl get pod 

// 获取描述，可以查看pod中所有镜像的运行情况（比如nginx镜像在哪个节点下载运行），mynginx是你自己的Pod名字
kubectl describe pod mynginx

// 删除
kubectl delete pod Pod名字
// 查看Pod的运行日志
kubectl logs Pod名字

// 每个Pod - k8s都会分配一个ip，使用下面命令可以查看到分配给pod的IP以及更pod更多的信息
kubectl get pod -owide
// 使用Pod的ip+pod里面运行容器的端口
curl 192.168.169.136

// 集群中的任意一个机器以及任意的应用都能通过Pod分配的ip来访问这个Pod，但在集群外不能被访问

```

也可以使用yaml文件创建上面的pod

```yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    run: mynginx
  name: mynginx
//  namespace: default
spec:
  containers:
  - image: nginx
    name: mynginx

```

一个pod里面多容器

```yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    run: myapp
  name: myapp
spec:
  containers:
  - image: nginx
    name: nginx
  - image: tomcat:8.5.68
    name: tomcat

```

![image.png](./images/K8s-Docker-KubeSphere-DevOps-7.png)

**此时的应用还不能外部访问**

同一pod的容器也可以通过localhost访问

## 3.4. Deployment

控制Pod，使Pod拥有多副本，自愈，扩缩容等能力

```bash
// 比较下面两个创建pod的命令有何不同效果？
kubectl run mynginx --image=nginx
kubectl create deployment mytomcat --image=tomcat:8.5.68

// 自愈能力不同，删除第一个命令创建的pod，就会直接删除，不会有其他操作；而删除第二个命令创建的pod，会删除但是又会重新起一个新的pod
// 所以，使用Deployment部署时候，pod有更强大的自愈能力
kubectl delete <podName>

// 而deployment只能通过下面的命令删除，获取Deploy名字通过kubectl get deploy命令
kubectl delete deploy <deployName>
```

### 3.4.1. 多副本

```bash
// 可以部署多个副本，部署的pod分布在不同的节点当中
kubectl create deployment my-dep --image=nginx --replicas=3
```

也可以使用配置文件部署

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: my-dep
  name: my-dep
spec:
  replicas: 3
  selector:
    matchLabels:
      app: my-dep
  template:
    metadata:
      labels:
        app: my-dep
    spec:
      containers:
      - image: nginx
        name: nginx

```

### 3.4.2. 扩缩容

```bash
// 扩容和缩容的命令，修改replicas到你想扩容的pod数量
kubectl scale --replicas=5 deployment/my-dep
kubectl scale --replicas=2 deployment/my-dep

// 也可以通过下面的命令，修改my-dep 的deployment文件中的replicas，达到扩容和缩容的目的
kubectl edit deployment my-dep
```

### 3.4.3. 自愈&故障转移

- 停机
- 删除Pod
- 容器崩溃
- ....

最后可以达到保证副本数量的目的

### 3.4.4. 滚动更新（不停机更新）

```bash
// 应用版本更新时候，新版本启动时候，和旧版本的应用是并存的，只有流量全部进入到新版本的应用之后，旧版本的应用才会停止。达到不停机更新的目的，里面的算法比较复杂，后面可以研究一下
// 比如下面的命令，是更新nginx镜像到指定的版本, --record可以记录下本次版本的更新
kubectl set image deployment/my-dep nginx=nginx:1.16.1 --record
// 查看deployment 滚动更新的状态
kubectl rollout status deployment/my-dep

// 也可以通过修改deployment文件中的nginx版本号达到不停机滚动更新的目的
kubectl edit deployment/my-dep
```

### 3.4.5. 版本回退

```bash
// 历史记录
kubectl rollout history deployment/my-dep
[root@k8s-master ~]# kubectl rollout history deployment/my-dep
deployment.apps/my-dep 
REVISION  CHANGE-CAUSE
1         <none>
2         kubectl set image deployment/my-dep nginx=nginx:1.16.1 --record=true

// 查看某个历史详情
kubectl rollout history deployment/my-dep --revision=2

// 回滚(回到上次)
kubectl rollout undo deployment/my-dep

// 回滚(回到指定版本)
kubectl rollout undo deployment/my-dep --to-revision=2
```

**更多**：

**除了Deployment，k8s还有 `StatefulSet` 、`DaemonSet` 、`Job`  等 类型资源。我们都称为 `工作负载`。**

**有状态应用使用  `StatefulSet`  部署，无状态应用使用 `Deployment` 部署**

https://kubernetes.io/zh/docs/concepts/workloads/controllers/

## 3.5. Service

将一组 [Pods](https://kubernetes.io/docs/concepts/workloads/pods/pod-overview/) 公开为网络服务的抽象方法。例如mysql部署了三个pod，分配了三个内网地址，但是表现为一个Service服务地址，访问这个服务名字就可以了，后台会帮你做负载均衡

```bash
// 暴露my-dep Deploy的服务，--port=8000对外的服务端口号，--target-port=80对应的是内部所有pod，即内部nginx的端口号，其中my-dep是deployment的名字。
kubectl expose deployment my-dep --port=8000 --target-port=80

// 使用标签检索Pod的详细信息，在集群内任意节点（容器内部也可以）可以访问服务的IP:端口（也可以是服务名.命名空间.svc:端口号，my-dep.default.svc:8080），也是负载均衡的。但是这种服务名访问地址只在集群内部有效，外部访问就无效
kubectl get pod -l app=my-dep
```



```yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    app: my-dep
  name: my-dep
spec:
  selector:
    app: my-dep
  ports:
  - port: 8000
    protocol: TCP
    targetPort: 80

```

### 3.5.1. ClusterIP(集群IP)

```bash
// 等同于没有--type的，其中 --type=ClusterIP，是默认的参数。这种类型只有在集群内部可以访问
kubectl expose deployment my-dep --port=8000 --target-port=80 --type=ClusterIP
```



```yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    app: my-dep
  name: my-dep
spec:
  ports:
  - port: 8000
    protocol: TCP
    targetPort: 80
  selector:
    app: my-dep
  type: ClusterIP

```

### 3.5.2. NodePort

```bash
// 和ClusterIP不同的效果是NodePort，在集群外部也可以访问，即NodePort集群内部外部都可以访问，NodePort范围在 30000-32767 之间
kubectl expose deployment my-dep --port=8000 --target-port=80 --type=NodePort

// 删除Service的命令如下，my-dep是deployment的名字
kubectl delete svc my-dep
```



```yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    app: my-dep
  name: my-dep
spec:
  ports:
  - port: 8000
    protocol: TCP
    targetPort: 80
  selector:
    app: my-dep
  type: NodePort

```

## 3.6. Ingress(Service的统一网关入口，总网关)

### 3.6.1. 安装

```bash
wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.47.0/deploy/static/provider/baremetal/deploy.yaml

// 修改镜像
vi deploy.yaml
// 将image的值改为如下值：
registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/ingress-nginx-controller:v0.46.0

// 通过apply命令安装ingress
kubectl apply -f ingress.yaml

// 检查安装的结果
kubectl get pod,svc -n ingress-nginx

// 最后别忘记把svc暴露的端口要放行，80:31164/TCP,443:31126/TCP，其中访问HTTP 31164的话对应的是容器内的80端口，HTTPS的话对应的是容器内的443端口
[root@k8s-master ~]# kubectl get svc -A
NAMESPACE              NAME                                 TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)                      AGE
default                kubernetes                           ClusterIP   10.96.0.1      <none>        443/TCP                      24h
ingress-nginx          ingress-nginx-controller             NodePort    10.96.27.209   <none>        80:31164/TCP,443:31126/TCP   2m41s
ingress-nginx          ingress-nginx-controller-admission   ClusterIP   10.96.12.112   <none>        443/TCP                      2m41s
kube-system            kube-dns                             ClusterIP   10.96.0.10     <none>        53/UDP,53/TCP,9153/TCP       24h
kubernetes-dashboard   dashboard-metrics-scraper            ClusterIP   10.96.60.168   <none>        8000/TCP                     24h
kubernetes-dashboard   kubernetes-dashboard                 NodePort    10.96.223.26   <none>        443:32766/TCP                24h
```



如果下载不到上面的deploy.yaml文件，用以下文件

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: ingress-nginx
  labels:
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx

---

apiVersion: v1   # Source: ingress-nginx/templates/controller-serviceaccount.yaml
kind: ServiceAccount
metadata:
  labels:
    helm.sh/chart: ingress-nginx-3.33.0
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/version: 0.47.0
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: controller
  name: ingress-nginx
  namespace: ingress-nginx
automountServiceAccountToken: true
---

apiVersion: v1   # Source: ingress-nginx/templates/controller-configmap.yaml
kind: ConfigMap
metadata:
  labels:
    helm.sh/chart: ingress-nginx-3.33.0
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/version: 0.47.0
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: controller
  name: ingress-nginx-controller
  namespace: ingress-nginx
data:
---

apiVersion: rbac.authorization.k8s.io/v1  # Source: ingress-nginx/templates/clusterrole.yaml
kind: ClusterRole
metadata:
  labels:
    helm.sh/chart: ingress-nginx-3.33.0
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/version: 0.47.0
    app.kubernetes.io/managed-by: Helm
  name: ingress-nginx
rules:
  - apiGroups:
      - ''
    resources:
      - configmaps
      - endpoints
      - nodes
      - pods
      - secrets
    verbs:
      - list
      - watch
  - apiGroups:
      - ''
    resources:
      - nodes
    verbs:
      - get
  - apiGroups:
      - ''
    resources:
      - services
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - extensions
      - networking.k8s.io   # k8s 1.14+
    resources:
      - ingresses
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - ''
    resources:
      - events
    verbs:
      - create
      - patch
  - apiGroups:
      - extensions
      - networking.k8s.io   # k8s 1.14+
    resources:
      - ingresses/status
    verbs:
      - update
  - apiGroups:
      - networking.k8s.io   # k8s 1.14+
    resources:
      - ingressclasses
    verbs:
      - get
      - list
      - watch
---

apiVersion: rbac.authorization.k8s.io/v1  # Source: ingress-nginx/templates/clusterrolebinding.yaml
kind: ClusterRoleBinding
metadata:
  labels:
    helm.sh/chart: ingress-nginx-3.33.0
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/version: 0.47.0
    app.kubernetes.io/managed-by: Helm
  name: ingress-nginx
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: ingress-nginx
subjects:
  - kind: ServiceAccount
    name: ingress-nginx
    namespace: ingress-nginx
---

apiVersion: rbac.authorization.k8s.io/v1  # Source: ingress-nginx/templates/controller-role.yaml
kind: Role
metadata:
  labels:
    helm.sh/chart: ingress-nginx-3.33.0
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/version: 0.47.0
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: controller
  name: ingress-nginx
  namespace: ingress-nginx
rules:
  - apiGroups:
      - ''
    resources:
      - namespaces
    verbs:
      - get
  - apiGroups:
      - ''
    resources:
      - configmaps
      - pods
      - secrets
      - endpoints
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - ''
    resources:
      - services
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - extensions
      - networking.k8s.io   # k8s 1.14+
    resources:
      - ingresses
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - extensions
      - networking.k8s.io   # k8s 1.14+
    resources:
      - ingresses/status
    verbs:
      - update
  - apiGroups:
      - networking.k8s.io   # k8s 1.14+
    resources:
      - ingressclasses
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - ''
    resources:
      - configmaps
    resourceNames:
      - ingress-controller-leader-nginx
    verbs:
      - get
      - update
  - apiGroups:
      - ''
    resources:
      - configmaps
    verbs:
      - create
  - apiGroups:
      - ''
    resources:
      - events
    verbs:
      - create
      - patch
---

apiVersion: rbac.authorization.k8s.io/v1  # Source: ingress-nginx/templates/controller-rolebinding.yaml
kind: RoleBinding
metadata:
  labels:
    helm.sh/chart: ingress-nginx-3.33.0
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/version: 0.47.0
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: controller
  name: ingress-nginx
  namespace: ingress-nginx
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: ingress-nginx
subjects:
  - kind: ServiceAccount
    name: ingress-nginx
    namespace: ingress-nginx
---

apiVersion: v1   # Source: ingress-nginx/templates/controller-service-webhook.yaml
kind: Service
metadata:
  labels:
    helm.sh/chart: ingress-nginx-3.33.0
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/version: 0.47.0
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: controller
  name: ingress-nginx-controller-admission
  namespace: ingress-nginx
spec:
  type: ClusterIP
  ports:
    - name: https-webhook
      port: 443
      targetPort: webhook
  selector:
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/component: controller
---

apiVersion: v1  # Source: ingress-nginx/templates/controller-service.yaml
kind: Service
metadata:
  annotations:
  labels:
    helm.sh/chart: ingress-nginx-3.33.0
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/version: 0.47.0
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: controller
  name: ingress-nginx-controller
  namespace: ingress-nginx
spec:
  type: NodePort
  ports:
    - name: http
      port: 80
      protocol: TCP
      targetPort: http
    - name: https
      port: 443
      protocol: TCP
      targetPort: https
  selector:
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/component: controller
---

apiVersion: apps/v1  # Source: ingress-nginx/templates/controller-deployment.yaml
kind: Deployment
metadata:
  labels:
    helm.sh/chart: ingress-nginx-3.33.0
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/version: 0.47.0
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: controller
  name: ingress-nginx-controller
  namespace: ingress-nginx
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: ingress-nginx
      app.kubernetes.io/instance: ingress-nginx
      app.kubernetes.io/component: controller
  revisionHistoryLimit: 10
  minReadySeconds: 0
  template:
    metadata:
      labels:
        app.kubernetes.io/name: ingress-nginx
        app.kubernetes.io/instance: ingress-nginx
        app.kubernetes.io/component: controller
    spec:
      dnsPolicy: ClusterFirst
      containers:
        - name: controller
          image: registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/ingress-nginx-controller:v0.46.0
          imagePullPolicy: IfNotPresent
          lifecycle:
            preStop:
              exec:
                command:
                  - /wait-shutdown
          args:
            - /nginx-ingress-controller
            - --election-id=ingress-controller-leader
            - --ingress-class=nginx
            - --configmap=$(POD_NAMESPACE)/ingress-nginx-controller
            - --validating-webhook=:8443
            - --validating-webhook-certificate=/usr/local/certificates/cert
            - --validating-webhook-key=/usr/local/certificates/key
          securityContext:
            capabilities:
              drop:
                - ALL
              add:
                - NET_BIND_SERVICE
            runAsUser: 101
            allowPrivilegeEscalation: true
          env:
            - name: POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: POD_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
            - name: LD_PRELOAD
              value: /usr/local/lib/libmimalloc.so
          livenessProbe:
            failureThreshold: 5
            httpGet:
              path: /healthz
              port: 10254
              scheme: HTTP
            initialDelaySeconds: 10
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 1
          readinessProbe:
            failureThreshold: 3
            httpGet:
              path: /healthz
              port: 10254
              scheme: HTTP
            initialDelaySeconds: 10
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 1
          ports:
            - name: http
              containerPort: 80
              protocol: TCP
            - name: https
              containerPort: 443
              protocol: TCP
            - name: webhook
              containerPort: 8443
              protocol: TCP
          volumeMounts:
            - name: webhook-cert
              mountPath: /usr/local/certificates/
              readOnly: true
          resources:
            requests:
              cpu: 100m
              memory: 90Mi
      nodeSelector:
        kubernetes.io/os: linux
      serviceAccountName: ingress-nginx
      terminationGracePeriodSeconds: 300
      volumes:
        - name: webhook-cert
          secret:
            secretName: ingress-nginx-admission
---



apiVersion: admissionregistration.k8s.io/v1  # Source: ingress-nginx/templates/admission-webhooks/validating-webhook.yaml
kind: ValidatingWebhookConfiguration  # before changing this value, check the required kubernetes version
metadata:   # https://kubernetes.io/docs/reference/access-authn-authz/extensible-admission-controllers/#prerequisites
  labels:
    helm.sh/chart: ingress-nginx-3.33.0
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/version: 0.47.0
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: admission-webhook
  name: ingress-nginx-admission
webhooks:
  - name: validate.nginx.ingress.kubernetes.io
    matchPolicy: Equivalent
    rules:
      - apiGroups:
          - networking.k8s.io
        apiVersions:
          - v1beta1
        operations:
          - CREATE
          - UPDATE
        resources:
          - ingresses
    failurePolicy: Fail
    sideEffects: None
    admissionReviewVersions:
      - v1
      - v1beta1
    clientConfig:
      service:
        namespace: ingress-nginx
        name: ingress-nginx-controller-admission
        path: /networking/v1beta1/ingresses
---

apiVersion: v1  # Source: ingress-nginx/templates/admission-webhooks/job-patch/serviceaccount.yaml
kind: ServiceAccount
metadata:
  name: ingress-nginx-admission
  annotations:
    helm.sh/hook: pre-install,pre-upgrade,post-install,post-upgrade
    helm.sh/hook-delete-policy: before-hook-creation,hook-succeeded
  labels:
    helm.sh/chart: ingress-nginx-3.33.0
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/version: 0.47.0
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: admission-webhook
  namespace: ingress-nginx
---

apiVersion: rbac.authorization.k8s.io/v1  # Source: ingress-nginx/templates/admission-webhooks/job-patch/clusterrole.yaml
kind: ClusterRole
metadata:
  name: ingress-nginx-admission
  annotations:
    helm.sh/hook: pre-install,pre-upgrade,post-install,post-upgrade
    helm.sh/hook-delete-policy: before-hook-creation,hook-succeeded
  labels:
    helm.sh/chart: ingress-nginx-3.33.0
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/version: 0.47.0
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: admission-webhook
rules:
  - apiGroups:
      - admissionregistration.k8s.io
    resources:
      - validatingwebhookconfigurations
    verbs:
      - get
      - update
---

apiVersion: rbac.authorization.k8s.io/v1  # Source: ingress-nginx/templates/admission-webhooks/job-patch/clusterrolebinding.yaml
kind: ClusterRoleBinding
metadata:
  name: ingress-nginx-admission
  annotations:
    helm.sh/hook: pre-install,pre-upgrade,post-install,post-upgrade
    helm.sh/hook-delete-policy: before-hook-creation,hook-succeeded
  labels:
    helm.sh/chart: ingress-nginx-3.33.0
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/version: 0.47.0
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: admission-webhook
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: ingress-nginx-admission
subjects:
  - kind: ServiceAccount
    name: ingress-nginx-admission
    namespace: ingress-nginx
---

apiVersion: rbac.authorization.k8s.io/v1  # Source: ingress-nginx/templates/admission-webhooks/job-patch/role.yaml
kind: Role
metadata:
  name: ingress-nginx-admission
  annotations:
    helm.sh/hook: pre-install,pre-upgrade,post-install,post-upgrade
    helm.sh/hook-delete-policy: before-hook-creation,hook-succeeded
  labels:
    helm.sh/chart: ingress-nginx-3.33.0
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/version: 0.47.0
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: admission-webhook
  namespace: ingress-nginx
rules:
  - apiGroups:
      - ''
    resources:
      - secrets
    verbs:
      - get
      - create
---

apiVersion: rbac.authorization.k8s.io/v1  # Source: ingress-nginx/templates/admission-webhooks/job-patch/rolebinding.yaml
kind: RoleBinding
metadata:
  name: ingress-nginx-admission
  annotations:
    helm.sh/hook: pre-install,pre-upgrade,post-install,post-upgrade
    helm.sh/hook-delete-policy: before-hook-creation,hook-succeeded
  labels:
    helm.sh/chart: ingress-nginx-3.33.0
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/version: 0.47.0
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: admission-webhook
  namespace: ingress-nginx
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: ingress-nginx-admission
subjects:
  - kind: ServiceAccount
    name: ingress-nginx-admission
    namespace: ingress-nginx
---

apiVersion: batch/v1  # Source: ingress-nginx/templates/admission-webhooks/job-patch/job-createSecret.yaml
kind: Job
metadata:
  name: ingress-nginx-admission-create
  annotations:
    helm.sh/hook: pre-install,pre-upgrade
    helm.sh/hook-delete-policy: before-hook-creation,hook-succeeded
  labels:
    helm.sh/chart: ingress-nginx-3.33.0
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/version: 0.47.0
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: admission-webhook
  namespace: ingress-nginx
spec:
  template:
    metadata:
      name: ingress-nginx-admission-create
      labels:
        helm.sh/chart: ingress-nginx-3.33.0
        app.kubernetes.io/name: ingress-nginx
        app.kubernetes.io/instance: ingress-nginx
        app.kubernetes.io/version: 0.47.0
        app.kubernetes.io/managed-by: Helm
        app.kubernetes.io/component: admission-webhook
    spec:
      containers:
        - name: create
          image: docker.io/jettech/kube-webhook-certgen:v1.5.1
          imagePullPolicy: IfNotPresent
          args:
            - create
            - --host=ingress-nginx-controller-admission,ingress-nginx-controller-admission.$(POD_NAMESPACE).svc
            - --namespace=$(POD_NAMESPACE)
            - --secret-name=ingress-nginx-admission
          env:
            - name: POD_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
      restartPolicy: OnFailure
      serviceAccountName: ingress-nginx-admission
      securityContext:
        runAsNonRoot: true
        runAsUser: 2000
---

apiVersion: batch/v1  # Source: ingress-nginx/templates/admission-webhooks/job-patch/job-patchWebhook.yaml
kind: Job
metadata:
  name: ingress-nginx-admission-patch
  annotations:
    helm.sh/hook: post-install,post-upgrade
    helm.sh/hook-delete-policy: before-hook-creation,hook-succeeded
  labels:
    helm.sh/chart: ingress-nginx-3.33.0
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/instance: ingress-nginx
    app.kubernetes.io/version: 0.47.0
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/component: admission-webhook
  namespace: ingress-nginx
spec:
  template:
    metadata:
      name: ingress-nginx-admission-patch
      labels:
        helm.sh/chart: ingress-nginx-3.33.0
        app.kubernetes.io/name: ingress-nginx
        app.kubernetes.io/instance: ingress-nginx
        app.kubernetes.io/version: 0.47.0
        app.kubernetes.io/managed-by: Helm
        app.kubernetes.io/component: admission-webhook
    spec:
      containers:
        - name: patch
          image: docker.io/jettech/kube-webhook-certgen:v1.5.1
          imagePullPolicy: IfNotPresent
          args:
            - patch
            - --webhook-name=ingress-nginx-admission
            - --namespace=$(POD_NAMESPACE)
            - --patch-mutating=false
            - --secret-name=ingress-nginx-admission
            - --patch-failure-policy=Fail
          env:
            - name: POD_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
      restartPolicy: OnFailure
      serviceAccountName: ingress-nginx-admission
      securityContext:
        runAsNonRoot: true
        runAsUser: 2000

```

### 3.6.2. 使用

官网地址：https://kubernetes.github.io/ingress-nginx/

就是nginx做的



[https://139.198.163.211:31164/](https://139.198.163.211:31164/)

[http://139.198.163.211:31126/](https://139.198.163.211:31126/)

#### 3.6.2.1. 测试环境

应用如下yaml，准备好测试环境

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hello-server
spec:
  replicas: 2
  selector:
    matchLabels:
      app: hello-server
  template:
    metadata:
      labels:
        app: hello-server
    spec:
      containers:
      - name: hello-server
        image: registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/hello-server
        ports:
        - containerPort: 9000
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: nginx-demo
  name: nginx-demo
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx-demo
  template:
    metadata:
      labels:
        app: nginx-demo
    spec:
      containers:
      - image: nginx
        name: nginx
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: nginx-demo
  name: nginx-demo
spec:
  selector:
    app: nginx-demo
  ports:
  - port: 8000
    protocol: TCP
    targetPort: 80
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: hello-server
  name: hello-server
spec:
  selector:
    app: hello-server
  ports:
  - port: 8000
    protocol: TCP
    targetPort: 9000

```



#### 3.6.2.2. 域名访问



```
// 创建下面内容ingress-rule.yaml文件
vi ingress-rule.yaml

// 并应用yaml
kubectl apply -f ingress-rule.yaml

// 获取ingress rule
kubectl get ingress

[root@k8s-master ~]# kubectl get ingress
NAME               CLASS   HOSTS                                ADDRESS       PORTS   AGE
ingress-host-bar   nginx   hello.atguigu.com,demo.atguigu.com   172.16.26.4   80      6m44s

// 如上面结果显示，已经生成响应的rule，其中172.16.26.4是node1的IP地址
// 访问 http://172.16.26.4:31164/ 是访问HTTP协议，访问 https://172.16.26.4:31126/ 是访问HTTPS协议
// 测试时候需要修改域名，因为ingress的rule说明了遇到hello.atguigu.com或者demo.atguigu.com或者域名下的某个路径时候才会跳转到对应的pod应用中！！！！
// 80:31164/TCP,443:31126/TCP

```



```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress  
metadata:
  name: ingress-host-bar
spec:
  ingressClassName: nginx
  rules:
  - host: "hello.atguigu.com"
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
            name: hello-server
            port:
              number: 8000
  - host: "demo.atguigu.com"
    http:
      paths:
      - pathType: Prefix
        path: "/nginx"  # 把请求会转给下面的服务，下面的服务一定要能处理这个路径，不能处理就是404
        backend:
          service:
            name: nginx-demo  ## java，比如使用路径重写，去掉前缀nginx
            port:
              number: 8000

```



问题： path: "/nginx" 与  path: "/" 为什么会有不同的效果？

上述yaml文件中，**path: "/"**会由ingress层返回**ingress自带的404页面**，而**path: "/nginx"**通过了ingress层，进入了nginx-demo pod，而nginx-demo pod处理/nginx路径时候没有找到响应的资源，所以就返回**nginx-demo pod的404页面**

#### 3.6.2.3. 路径重写(会截取路径)

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress  
metadata:
  annotations:  # 注解信息
    nginx.ingress.kubernetes.io/rewrite-target: /$2   # 表示重写第二个
  name: ingress-host-bar
spec:
  ingressClassName: nginx
  rules:
  - host: "hello.atguigu.com"
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
            name: hello-server
            port:
              number: 8000
  - host: "demo.atguigu.com"
    http:
      paths:
      - pathType: Prefix
        path: "/nginx(/|$)(.*)"  # 把请求会转给下面的服务，下面的服务一定要能处理这个路径，不能处理就是404
        backend:
          service:
            name: nginx-demo  ## java，比如使用路径重写，去掉前缀nginx
            port:
              number: 8000

```

#### 3.6.2.4. 流量限制

比如每次限制多少次请求，**规则生成后可能不能立马生效！**

限流返回的默认HTTP状态值是503

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress-limit-rate
  annotations:
    nginx.ingress.kubernetes.io/limit-rps: "1"
spec:
  ingressClassName: nginx
  rules:
  - host: "haha.atguigu.com"
    http:
      paths:
      - pathType: Exact
        path: "/"
        backend:
          service:
            name: nginx-demo
            port:
              number: 8000

```

## 3.7. 存储抽象

### 3.7.1. 环境准备

#### 3.7.1.1. 所有节点

```bash
// 所有机器节点安装NFS文件工具包
yum install -y nfs-utils
```

#### 3.7.1.2. 主节点

```bash
// 在nfs主节点运行下面命令，这是后把Master节点当做nfs主节点运行下面的命令。命令的意思是
echo "/nfs/data/ *(insecure,rw,sync,no_root_squash)" > /etc/exports
// 在Master nfs主节点上创建上面说的文件夹
mkdir -p /nfs/data
// 在Master nfs主节点启动RPC远程绑定，为了达到同步目录的作用，现在永久生效，开机启动
systemctl enable rpcbind --now
// 现在启动，开机启动nfs服务器
systemctl enable nfs-server --now
// 使配置生效
exportfs -r
```

#### 3.7.1.3. 从节点

```bash
// 在从节点node1和node2上面运行下面的检查命令，检查是否可以挂载到Master主节点以及主节点暴露的路径，下面的IP地址是主节点的IP
showmount -e 172.16.26.3

// node1和node2节点上执行以下命令，创建目录，并挂载 nfs 服务器上的共享目录到本机路径 /nfs/data（注意路径的对应）
mkdir -p /nfs/data
mount -t nfs 172.16.26.3:/nfs/data /nfs/data

// 写入一个测试文件
echo "hello nfs server" > /nfs/data/test.txt
```



#### 3.7.1.4. 原生方式数据挂载

// 下面是原生方式挂载数据的一种部署方式

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: nginx-pv-demo
  name: nginx-pv-demo
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx-pv-demo
  template:
    metadata:
      labels:
        app: nginx-pv-demo
    spec:
      containers:
      - image: nginx
        name: nginx
        volumeMounts:
        - name: html
          mountPath: /usr/share/nginx/html  # 这是对应容器中的路径
      volumes:
        - name: html
          nfs:
            server: 172.16.26.3
            path: /nfs/data/nginx-pv  # 这是对应nfs服务器中路径，文件夹需要存在

```

### 3.7.2. PV&PVC

**PV：持久卷（Persistent Volume**），将应用需要持久化的数据保存到指定位置

**PVC：持久卷申明（Persistent Volume Claim）**，申明需要使用的持久卷规格（申请需要多少空间）

#### 3.7.2.1. 创建pv池

静态供应

```bash
// nfs主节点，master节点上运行
mkdir -p /nfs/data/01
mkdir -p /nfs/data/02
mkdir -p /nfs/data/03
```

创建PV

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv01-10m  # 名字随意取，但是最好有意义，且只能是小写
spec:
  capacity:
    storage: 10M  # 最多占用10M
  accessModes:
    - ReadWriteMany  # 表示可读可写多节点
  storageClassName: nfs  # 可以随意取，但是一类存取卷最好一致，因为它相当于命名空间
  nfs:
    path: /nfs/data/01
    server: 172.16.26.3
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv02-1gi
spec:
  capacity:
    storage: 1Gi  # 最多占用1G
  accessModes:
    - ReadWriteMany
  storageClassName: nfs
  nfs:
    path: /nfs/data/02
    server: 172.16.26.3
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv03-3gi
spec:
  capacity:
    storage: 3Gi  # 最多占用3G
  accessModes:
    - ReadWriteMany
  storageClassName: nfs
  nfs:
    path: /nfs/data/03
    server: 172.16.26.3

```

#### 3.7.2.2. PVC创建与绑定

上面的PV创建好后，接下来就可以创建PVC了

```yaml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: nginx-pvc
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 200Mi
  storageClassName: nfs

```

创建Pod绑定PVC

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: nginx-deploy-pvc
  name: nginx-deploy-pvc
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx-deploy-pvc
  template:
    metadata:
      labels:
        app: nginx-deploy-pvc
    spec:
      containers:
      - image: nginx
        name: nginx
        volumeMounts:
        - name: html
          mountPath: /usr/share/nginx/html
      volumes:
        - name: html
          persistentVolumeClaim:
            claimName: nginx-pvc  # 对应的PVC 存储卷申请书，就是上面的pvc

```

### 3.7.3. ConfigMap

**抽取应用配置，并且可以自动更新**

#### 3.7.3.1. redis示例

##### 3.7.3.1.1. 把之前的配置文件创建为配置集

```bash
// 创建配置，redis保存到k8s的etcd，不过首先要有redis.conf文件，并且里面有数据-键值对
kubectl create cm redis-conf --from-file=redis.conf
```



```yaml
apiVersion: v1
data:    #data是所有真正的数据（键值对形式），key：默认是文件名   value：配置文件的内容
  redis.conf: |
    appendonly yes
kind: ConfigMap
metadata:
  name: redis-conf
  namespace: default

```

##### 3.7.3.1.2. 创建Pod

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: redis
spec:
  containers:
  - name: redis
    image: redis
    command:
      - redis-server
      - "/redis-master/redis.conf"  #指的是redis容器内部的位置
    ports:
    - containerPort: 6379
    volumeMounts:
    - mountPath: /data
      name: data
    - mountPath: /redis-master
      name: config
  volumes:
    - name: data
      emptyDir: {}
    - name: config
      configMap:
        name: redis-conf # 引用名为redis-conf的配置集合，就是上面创建的配置集
        items:
        - key: redis.conf  # 引用配置集中的哪个位置
          path: redis.conf # 会在redis容器内部挂载目录 /redis-master 下创建名为redis.conf的配置文件

```

##### 3.7.3.1.3. 检查默认配置

```bash
kubectl exec -it redis -- redis-cli

127.0.0.1:6379> CONFIG GET appendonly
127.0.0.1:6379> CONFIG GET requirepass
```

##### 3.7.3.1.4. 修改ConfigMap

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: example-redis-config
data:
  redis-config: |
    maxmemory 2mb
    maxmemory-policy allkeys-lru 
```

##### 3.7.3.1.5. 检查配置是否更新

```bash
kubectl exec -it redis -- redis-cli

127.0.0.1:6379> CONFIG GET maxmemory
127.0.0.1:6379> CONFIG GET maxmemory-policy
```

检查指定文件内容是否已经更新

修改了CM。Pod里面的配置文件会跟着变



**配置值未更改，因为需要重新启动 Pod 才能从关联的 ConfigMap 中获取更新的值。**

**原因：我们的Pod部署的中间件自己本身没有热更新能力**

### 3.7.4. Secret

**Secret 对象类型用来保存敏感信息，例如密码、OAuth 令牌和 SSH 密钥。 将这些信息放在 secret 中比放在 [Pod](https://kubernetes.io/docs/concepts/workloads/pods/pod-overview/) 的定义或者 [容器镜像](https://kubernetes.io/zh/docs/reference/glossary/?all=true#term-image) 中来说更加安全和灵活。**

```bash
kubectl create secret docker-registry leifengyang-docker \
--docker-username=leifengyang \
--docker-password=Lfy123456 \
--docker-email=534096094@qq.com

// 创建私有镜像的登录信息，为了下面下载私有镜像时候不用手动输入命令，命令格式如下：
kubectl create secret docker-registry regcred \
  --docker-server=<你的镜像仓库服务器> \
  --docker-username=<你的用户名> \
  --docker-password=<你的密码> \
  --docker-email=<你的邮箱地址>
```



```yaml
apiVersion: v1
kind: Pod
metadata:
  name: private-nginx
spec:
  containers:
  - name: private-nginx
    image: leifengyang/guignginx:v1.0
  imagePullSecrets:
  - name: leifengyang-docker  # 说明使用哪个秘钥信息

```

# 4. Kubernetes上安装KubeSphere

**安装步骤**

- 选择4核8G（master）、8核16G（node1）、8核16G（node2） 三台机器，按量付费进行实验，CentOS7.9

- 安装Docker
- 安装Kubernetes

- 安装KubeSphere前置环境
- 安装KubeSphere

## 4.1. 安装Docker（安装过就不用安装了）

```bash
sudo yum remove docker*
sudo yum install -y yum-utils

#配置docker的yum地址
sudo yum-config-manager \
--add-repo \
http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo


#安装指定版本
sudo yum install -y docker-ce-20.10.7 docker-ce-cli-20.10.7 containerd.io-1.4.6

#	启动&开机启动docker
systemctl enable docker --now

# docker加速配置
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://82m9ar63.mirror.aliyuncs.com"],
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```



## 4.2. 安装Kubernetes（安装过就不用安装了）

### 4.2.1. 基本环境

每个机器使用内网ip互通

每个机器配置自己的hostname，不能用localhost

```bash
#设置每个机器自己的hostname
hostnamectl set-hostname xxx

# 将 SELinux 设置为 permissive 模式（相当于将其禁用）
sudo setenforce 0
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

#关闭swap
swapoff -a  
sed -ri 's/.*swap.*/#&/' /etc/fstab

#允许 iptables 检查桥接流量
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
br_netfilter
EOF

cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system
```

### 4.2.2. 安装kubelet、kubeadm、kubectl

```bash
#配置k8s的yum源地址
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=http://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=http://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg
   http://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF


#安装 kubelet，kubeadm，kubectl
sudo yum install -y kubelet-1.20.9 kubeadm-1.20.9 kubectl-1.20.9

#启动kubelet
sudo systemctl enable --now kubelet

#所有机器配置master域名
echo "172.31.0.4  k8s-master" >> /etc/hosts
```

### 4.2.3. 初始化master节点

#### 4.2.3.1. 初始化

```bash
kubeadm init \
--apiserver-advertise-address=172.31.0.4 \
--control-plane-endpoint=k8s-master \
--image-repository registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images \
--kubernetes-version v1.20.9 \
--service-cidr=10.96.0.0/16 \
--pod-network-cidr=192.168.0.0/16
```



#### 4.2.3.2. 记录关键信息

记录master执行完成后的日志

```bash
Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

You can now join any number of control-plane nodes by copying certificate authorities
and service account keys on each node and then running the following as root:

  kubeadm join k8s-master:6443 --token 3vckmv.lvrl05xpyftbs177 \
    --discovery-token-ca-cert-hash sha256:1dc274fed24778f5c284229d9fcba44a5df11efba018f9664cf5e8ff77907240 \
    --control-plane 

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join k8s-master:6443 --token 3vckmv.lvrl05xpyftbs177 \
    --discovery-token-ca-cert-hash sha256:1dc274fed24778f5c284229d9fcba44a5df11efba018f9664cf5e8ff77907240
```

#### 4.2.3.3. 安装Calico网络插件

```bash
curl https://docs.projectcalico.org/manifests/calico.yaml -O

kubectl apply -f calico.yaml
```

## 4.3. 安装KubeSphere前置环境

### 4.3.1. nfs文件系统

#### 4.3.1.1. 安装nfs-server

```bash
# 在每个机器。
yum install -y nfs-utils


# 在master 执行以下命令，追加内容，为了不覆盖其他设置
echo "/nfs/data/ *(insecure,rw,sync,no_root_squash)" >> /etc/exports


# 执行以下命令，启动 nfs 服务;创建共享目录
mkdir -p /nfs/data


# 在master执行
systemctl enable rpcbind
systemctl enable nfs-server
systemctl start rpcbind
systemctl start nfs-server

# 使配置生效
exportfs -r


#检查配置是否生效
exportfs
```

#### 4.3.1.2. 配置nfs-client（选做，因为Kubesphere是动态分配，所以不用手动绑定）

```bash
showmount -e 172.31.0.4

mkdir -p /nfs/data

mount -t nfs 172.31.0.4:/nfs/data /nfs/data
```

#### 4.3.1.3. 配置默认存储

配置动态供应的默认存储类

```bash
// 创建并应用存储类
vim sc.yaml
kubectl apply -f sc.yaml

// 检查存储类是否新增成功,确认配置是否生效
kubectl get sc
kubectl get pod
```



```yaml
## 创建了一个存储类
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: nfs-storage
  annotations:
    storageclass.kubernetes.io/is-default-class: "true"
provisioner: k8s-sigs.io/nfs-subdir-external-provisioner
parameters:
  archiveOnDelete: "true"  ## 删除pv的时候，pv的内容是否要备份

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nfs-client-provisioner
  labels:
    app: nfs-client-provisioner
  # replace with namespace where provisioner is deployed
  namespace: default
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: nfs-client-provisioner
  template:
    metadata:
      labels:
        app: nfs-client-provisioner
    spec:
      serviceAccountName: nfs-client-provisioner
      containers:
        - name: nfs-client-provisioner
          image: registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/nfs-subdir-external-provisioner:v4.0.2
          # resources:
          #    limits:
          #      cpu: 10m
          #    requests:
          #      cpu: 10m
          volumeMounts:
            - name: nfs-client-root
              mountPath: /persistentvolumes
          env:
            - name: PROVISIONER_NAME
              value: k8s-sigs.io/nfs-subdir-external-provisioner
            - name: NFS_SERVER
              value: 172.16.26.3 ## 指定自己nfs服务器地址
            - name: NFS_PATH  
              value: /nfs/data  ## nfs服务器共享的目录
      volumes:
        - name: nfs-client-root
          nfs:
            server: 172.16.26.3
            path: /nfs/data
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: nfs-client-provisioner
  # replace with namespace where provisioner is deployed
  namespace: default
---
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: nfs-client-provisioner-runner
rules:
  - apiGroups: [""]
    resources: ["nodes"]
    verbs: ["get", "list", "watch"]
  - apiGroups: [""]
    resources: ["persistentvolumes"]
    verbs: ["get", "list", "watch", "create", "delete"]
  - apiGroups: [""]
    resources: ["persistentvolumeclaims"]
    verbs: ["get", "list", "watch", "update"]
  - apiGroups: ["storage.k8s.io"]
    resources: ["storageclasses"]
    verbs: ["get", "list", "watch"]
  - apiGroups: [""]
    resources: ["events"]
    verbs: ["create", "update", "patch"]
---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: run-nfs-client-provisioner
subjects:
  - kind: ServiceAccount
    name: nfs-client-provisioner
    # replace with namespace where provisioner is deployed
    namespace: default
roleRef:
  kind: ClusterRole
  name: nfs-client-provisioner-runner
  apiGroup: rbac.authorization.k8s.io
---
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: leader-locking-nfs-client-provisioner
  # replace with namespace where provisioner is deployed
  namespace: default
rules:
  - apiGroups: [""]
    resources: ["endpoints"]
    verbs: ["get", "list", "watch", "create", "update", "patch"]
---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: leader-locking-nfs-client-provisioner
  # replace with namespace where provisioner is deployed
  namespace: default
subjects:
  - kind: ServiceAccount
    name: nfs-client-provisioner
    # replace with namespace where provisioner is deployed
    namespace: default
roleRef:
  kind: Role
  name: leader-locking-nfs-client-provisioner
  apiGroup: rbac.authorization.k8s.io

```

#### 4.3.1.4. 验证默认存储类是否生效

```bash
vim pvc.yaml
kubectl apply -f pvc.yaml
```



```yaml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: nginx-pvc
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 200Mi
  # storageClassName: nfs  # 这个不要写，因为它与上面配置的默认存储类的名字不同


```



### 4.3.2. metrics-server

```
vim metrics.yaml
kubectl apply -f metrics.yaml

// 查看节点占用内存，核心数
kubectl top nodes
kubectl top pods
```

集群指标监控组件

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  labels:
    k8s-app: metrics-server
  name: metrics-server
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  labels:
    k8s-app: metrics-server
    rbac.authorization.k8s.io/aggregate-to-admin: "true"
    rbac.authorization.k8s.io/aggregate-to-edit: "true"
    rbac.authorization.k8s.io/aggregate-to-view: "true"
  name: system:aggregated-metrics-reader
rules:
- apiGroups:
  - metrics.k8s.io
  resources:
  - pods
  - nodes
  verbs:
  - get
  - list
  - watch
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  labels:
    k8s-app: metrics-server
  name: system:metrics-server
rules:
- apiGroups:
  - ""
  resources:
  - pods
  - nodes
  - nodes/stats
  - namespaces
  - configmaps
  verbs:
  - get
  - list
  - watch
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  labels:
    k8s-app: metrics-server
  name: metrics-server-auth-reader
  namespace: kube-system
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: extension-apiserver-authentication-reader
subjects:
- kind: ServiceAccount
  name: metrics-server
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  labels:
    k8s-app: metrics-server
  name: metrics-server:system:auth-delegator
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: system:auth-delegator
subjects:
- kind: ServiceAccount
  name: metrics-server
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  labels:
    k8s-app: metrics-server
  name: system:metrics-server
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: system:metrics-server
subjects:
- kind: ServiceAccount
  name: metrics-server
  namespace: kube-system
---
apiVersion: v1
kind: Service
metadata:
  labels:
    k8s-app: metrics-server
  name: metrics-server
  namespace: kube-system
spec:
  ports:
  - name: https
    port: 443
    protocol: TCP
    targetPort: https
  selector:
    k8s-app: metrics-server
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    k8s-app: metrics-server
  name: metrics-server
  namespace: kube-system
spec:
  selector:
    matchLabels:
      k8s-app: metrics-server
  strategy:
    rollingUpdate:
      maxUnavailable: 0
  template:
    metadata:
      labels:
        k8s-app: metrics-server
    spec:
      containers:
      - args:
        - --cert-dir=/tmp
        - --kubelet-insecure-tls
        - --secure-port=4443
        - --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname
        - --kubelet-use-node-status-port
        image: registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/metrics-server:v0.4.3
        imagePullPolicy: IfNotPresent
        livenessProbe:
          failureThreshold: 3
          httpGet:
            path: /livez
            port: https
            scheme: HTTPS
          periodSeconds: 10
        name: metrics-server
        ports:
        - containerPort: 4443
          name: https
          protocol: TCP
        readinessProbe:
          failureThreshold: 3
          httpGet:
            path: /readyz
            port: https
            scheme: HTTPS
          periodSeconds: 10
        securityContext:
          readOnlyRootFilesystem: true
          runAsNonRoot: true
          runAsUser: 1000
        volumeMounts:
        - mountPath: /tmp
          name: tmp-dir
      nodeSelector:
        kubernetes.io/os: linux
      priorityClassName: system-cluster-critical
      serviceAccountName: metrics-server
      volumes:
      - emptyDir: {}
        name: tmp-dir
---
apiVersion: apiregistration.k8s.io/v1
kind: APIService
metadata:
  labels:
    k8s-app: metrics-server
  name: v1beta1.metrics.k8s.io
spec:
  group: metrics.k8s.io
  groupPriorityMinimum: 100
  insecureSkipTLSVerify: true
  service:
    name: metrics-server
    namespace: kube-system
  version: v1beta1
  versionPriority: 100

```

## 4.4. 安装KubeSphere

https://kubesphere.com.cn/

### 4.4.1. 下载核心文件

如果下载不到，请复制附录的内容。需要先下载文件：

```bash
wget https://github.com/kubesphere/ks-installer/releases/download/v3.2.0/kubesphere-installer.yaml
wget https://github.com/kubesphere/ks-installer/releases/download/v3.2.0/cluster-configuration.yaml
```

### 4.4.2. 修改cluster-configuration

在 cluster-configuration.yaml中指定我们需要开启的功能

参照官网“启用可插拔组件” 

https://kubesphere.com.cn/docs/pluggable-components/overview/



```
// 需要修改cluster-configuration.yaml，修改Localhost修改为内网IP，以及打开很多常用功能（基本见到false都改为true）
vim cluster-configuration.yaml
```

### 4.4.3. 执行安装

```bash
kubectl apply -f kubesphere-installer.yaml
kubectl apply -f cluster-configuration.yaml
```

### 4.4.4. 查看安装进度

```bash
kubectl logs -n kubesphere-system $(kubectl get pod -n kubesphere-system -l app=ks-install -o jsonpath='{.items[0].metadata.name}') -f
```

访问**任意机器**的 30880端口

账号 ： admin

密码（从控制台获取） ： P@88w0rd



**解决etcd监控证书找不到问题**

```bash
kubectl -n kubesphere-monitoring-system create secret generic kube-etcd-client-certs  --from-file=etcd-client-ca.crt=/etc/kubernetes/pki/etcd/ca.crt  --from-file=etcd-client.crt=/etc/kubernetes/pki/apiserver-etcd-client.crt  --from-file=etcd-client.key=/etc/kubernetes/pki/apiserver-etcd-client.key
```

### 4.4.5. 卸载Kubesphere

```bash
kubectl delete -f kubesphere-installer.yaml
```

### 4.4.6. 附录

#### 4.4.6.1. kubesphere-installer.yaml

```yaml
---
apiVersion: apiextensions.k8s.io/v1beta1
kind: CustomResourceDefinition
metadata:
  name: clusterconfigurations.installer.kubesphere.io
spec:
  group: installer.kubesphere.io
  versions:
  - name: v1alpha1
    served: true
    storage: true
  scope: Namespaced
  names:
    plural: clusterconfigurations
    singular: clusterconfiguration
    kind: ClusterConfiguration
    shortNames:
    - cc

---
apiVersion: v1
kind: Namespace
metadata:
  name: kubesphere-system

---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: ks-installer
  namespace: kubesphere-system

---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: ks-installer
rules:
- apiGroups:
  - ""
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - apps
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - extensions
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - batch
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - rbac.authorization.k8s.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - apiregistration.k8s.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - apiextensions.k8s.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - tenant.kubesphere.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - certificates.k8s.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - devops.kubesphere.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - monitoring.coreos.com
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - logging.kubesphere.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - jaegertracing.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - storage.k8s.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - admissionregistration.k8s.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - policy
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - autoscaling
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - networking.istio.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - config.istio.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - iam.kubesphere.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - notification.kubesphere.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - auditing.kubesphere.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - events.kubesphere.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - core.kubefed.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - installer.kubesphere.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - storage.kubesphere.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - security.istio.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - monitoring.kiali.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - kiali.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - networking.k8s.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - kubeedge.kubesphere.io
  resources:
  - '*'
  verbs:
  - '*'
- apiGroups:
  - types.kubefed.io
  resources:
  - '*'
  verbs:
  - '*'

---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: ks-installer
subjects:
- kind: ServiceAccount
  name: ks-installer
  namespace: kubesphere-system
roleRef:
  kind: ClusterRole
  name: ks-installer
  apiGroup: rbac.authorization.k8s.io

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ks-installer
  namespace: kubesphere-system
  labels:
    app: ks-install
spec:
  replicas: 1
  selector:
    matchLabels:
      app: ks-install
  template:
    metadata:
      labels:
        app: ks-install
    spec:
      serviceAccountName: ks-installer
      containers:
      - name: installer
        image: kubesphere/ks-installer:v3.1.1
        imagePullPolicy: "Always"
        resources:
          limits:
            cpu: "1"
            memory: 1Gi
          requests:
            cpu: 20m
            memory: 100Mi
        volumeMounts:
        - mountPath: /etc/localtime
          name: host-time
      volumes:
      - hostPath:
          path: /etc/localtime
          type: ""
        name: host-time
```

#### 4.4.6.2. cluster-configuration.yaml

```yaml
---
apiVersion: installer.kubesphere.io/v1alpha1
kind: ClusterConfiguration
metadata:
  name: ks-installer
  namespace: kubesphere-system
  labels:
    version: v3.1.1
spec:
  persistence:
    storageClass: ""        # If there is no default StorageClass in your cluster, you need to specify an existing StorageClass here.
  authentication:
    jwtSecret: ""           # Keep the jwtSecret consistent with the Host Cluster. Retrieve the jwtSecret by executing "kubectl -n kubesphere-system get cm kubesphere-config -o yaml | grep -v "apiVersion" | grep jwtSecret" on the Host Cluster.
  local_registry: ""        # Add your private registry address if it is needed.
  etcd:
    monitoring: true       # Enable or disable etcd monitoring dashboard installation. You have to create a Secret for etcd before you enable it.
    endpointIps: 172.31.0.4  # etcd cluster EndpointIps. It can be a bunch of IPs here.
    port: 2379              # etcd port.
    tlsEnable: true
  common:
    redis:
      enabled: true
    openldap:
      enabled: true
    minioVolumeSize: 20Gi # Minio PVC size.
    openldapVolumeSize: 2Gi   # openldap PVC size.
    redisVolumSize: 2Gi # Redis PVC size.
    monitoring:
      # type: external   # Whether to specify the external prometheus stack, and need to modify the endpoint at the next line.
      endpoint: http://prometheus-operated.kubesphere-monitoring-system.svc:9090 # Prometheus endpoint to get metrics data.
    es:   # Storage backend for logging, events and auditing.
      # elasticsearchMasterReplicas: 1   # The total number of master nodes. Even numbers are not allowed.
      # elasticsearchDataReplicas: 1     # The total number of data nodes.
      elasticsearchMasterVolumeSize: 4Gi   # The volume size of Elasticsearch master nodes.
      elasticsearchDataVolumeSize: 20Gi    # The volume size of Elasticsearch data nodes.
      logMaxAge: 7                     # Log retention time in built-in Elasticsearch. It is 7 days by default.
      elkPrefix: logstash              # The string making up index names. The index name will be formatted as ks-<elk_prefix>-log.
      basicAuth:
        enabled: false
        username: ""
        password: ""
      externalElasticsearchUrl: ""
      externalElasticsearchPort: ""
  console:
    enableMultiLogin: true  # Enable or disable simultaneous logins. It allows different users to log in with the same account at the same time.
    port: 30880
  alerting:                # (CPU: 0.1 Core, Memory: 100 MiB) It enables users to customize alerting policies to send messages to receivers in time with different time intervals and alerting levels to choose from.
    enabled: true         # Enable or disable the KubeSphere Alerting System.
    # thanosruler:
    #   replicas: 1
    #   resources: {}
  auditing:                # Provide a security-relevant chronological set of records，recording the sequence of activities happening on the platform, initiated by different tenants.
    enabled: true         # Enable or disable the KubeSphere Auditing Log System. 
  devops:                  # (CPU: 0.47 Core, Memory: 8.6 G) Provide an out-of-the-box CI/CD system based on Jenkins, and automated workflow tools including Source-to-Image & Binary-to-Image.
    enabled: true             # Enable or disable the KubeSphere DevOps System.
    jenkinsMemoryLim: 2Gi      # Jenkins memory limit.
    jenkinsMemoryReq: 1500Mi   # Jenkins memory request.
    jenkinsVolumeSize: 8Gi     # Jenkins volume size.
    jenkinsJavaOpts_Xms: 512m  # The following three fields are JVM parameters.
    jenkinsJavaOpts_Xmx: 512m
    jenkinsJavaOpts_MaxRAM: 2g
  events:                  # Provide a graphical web console for Kubernetes Events exporting, filtering and alerting in multi-tenant Kubernetes clusters.
    enabled: true         # Enable or disable the KubeSphere Events System.
    ruler:
      enabled: true
      replicas: 2
  logging:                 # (CPU: 57 m, Memory: 2.76 G) Flexible logging functions are provided for log query, collection and management in a unified console. Additional log collectors can be added, such as Elasticsearch, Kafka and Fluentd.
    enabled: true         # Enable or disable the KubeSphere Logging System.
    logsidecar:
      enabled: true
      replicas: 2
  metrics_server:                    # (CPU: 56 m, Memory: 44.35 MiB) It enables HPA (Horizontal Pod Autoscaler).
    enabled: false                   # Enable or disable metrics-server.
  monitoring:
    storageClass: ""                 # If there is an independent StorageClass you need for Prometheus, you can specify it here. The default StorageClass is used by default.
    # prometheusReplicas: 1          # Prometheus replicas are responsible for monitoring different segments of data source and providing high availability.
    prometheusMemoryRequest: 400Mi   # Prometheus request memory.
    prometheusVolumeSize: 20Gi       # Prometheus PVC size.
    # alertmanagerReplicas: 1          # AlertManager Replicas.
  multicluster:
    clusterRole: none  # host | member | none  # You can install a solo cluster, or specify it as the Host or Member Cluster.
  network:
    networkpolicy: # Network policies allow network isolation within the same cluster, which means firewalls can be set up between certain instances (Pods).
      # Make sure that the CNI network plugin used by the cluster supports NetworkPolicy. There are a number of CNI network plugins that support NetworkPolicy, including Calico, Cilium, Kube-router, Romana and Weave Net.
      enabled: true # Enable or disable network policies.
    ippool: # Use Pod IP Pools to manage the Pod network address space. Pods to be created can be assigned IP addresses from a Pod IP Pool.
      type: calico # Specify "calico" for this field if Calico is used as your CNI plugin. "none" means that Pod IP Pools are disabled.
    topology: # Use Service Topology to view Service-to-Service communication based on Weave Scope.
      type: none # Specify "weave-scope" for this field to enable Service Topology. "none" means that Service Topology is disabled.
  openpitrix: # An App Store that is accessible to all platform tenants. You can use it to manage apps across their entire lifecycle.
    store:
      enabled: true # Enable or disable the KubeSphere App Store.
  servicemesh:         # (0.3 Core, 300 MiB) Provide fine-grained traffic management, observability and tracing, and visualized traffic topology.
    enabled: true     # Base component (pilot). Enable or disable KubeSphere Service Mesh (Istio-based).
  kubeedge:          # Add edge nodes to your cluster and deploy workloads on edge nodes.
    enabled: true   # Enable or disable KubeEdge.
    cloudCore:
      nodeSelector: {"node-role.kubernetes.io/worker": ""}
      tolerations: []
      cloudhubPort: "10000"
      cloudhubQuicPort: "10001"
      cloudhubHttpsPort: "10002"
      cloudstreamPort: "10003"
      tunnelPort: "10004"
      cloudHub:
        advertiseAddress: # At least a public IP address or an IP address which can be accessed by edge nodes must be provided.
          - ""            # Note that once KubeEdge is enabled, CloudCore will malfunction if the address is not provided.
        nodeLimit: "100"
      service:
        cloudhubNodePort: "30000"
        cloudhubQuicNodePort: "30001"
        cloudhubHttpsNodePort: "30002"
        cloudstreamNodePort: "30003"
        tunnelNodePort: "30004"
    edgeWatcher:
      nodeSelector: {"node-role.kubernetes.io/worker": ""}
      tolerations: []
      edgeWatcherAgent:
        nodeSelector: {"node-role.kubernetes.io/worker": ""}
        tolerations: []
```



