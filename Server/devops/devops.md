<h2>目录</h2>

<details open>
  <summary><a href="#1-动态jenkins-slave">1. 动态Jenkins Slave</a></summary>
  <ul>
  <details open>
    <summary><a href="#11-longhorn分布式存储安装">1.1. Longhorn分布式存储安装</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#111-longhorn简介">1.1.1. Longhorn简介</a>    </summary>
      <ul>
        <a href="#1111-功能特性">1.1.1.1. 功能特性</a><br>
        <a href="#1112-longhorn-是什么">1.1.1.2. Longhorn 是什么?</a><br>
        <a href="#1113-使用微服务简化分布式块存储">1.1.1.3. 使用微服务简化分布式块存储</a><br>
        <a href="#1114-在不依赖云提供商的情况下在-kubernetes-中使用持久化存储">1.1.1.4. 在不依赖云提供商的情况下在 Kubernetes 中使用持久化存储</a><br>
        <a href="#1115-跨多个计算或存储主机调度多个副本replicas">1.1.1.5. 跨多个计算或存储主机调度多个副本(Replicas)</a><br>
        <a href="#1116-为每个卷分配多个存储前端">1.1.1.6. 为每个卷分配多个存储前端</a><br>
        <a href="#1117-指定定期快照和备份操作的计划">1.1.1.7. 指定定期快照和备份操作的计划</a><br>
      </ul>
    </details>
      <a href="#112-k8s安装并使用longhorn（master节点）">1.1.2. k8s安装并使用Longhorn（Master节点）</a><br>
    </ul>
  </details>
    <a href="#12-安装">1.2. 安装</a><br>
    <a href="#13-优点">1.3. 优点</a><br>
    <a href="#14-配置">1.4. 配置</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#2-jenkins-pipeline部署kubernetes应用">2. Jenkins Pipeline部署Kubernetes应用</a></summary>
  <ul>
    <a href="#21-jenkins-pipeline-介绍">2.1. Jenkins Pipeline 介绍</a><br>
  <details open>
    <summary><a href="#22-部署-kubernetes-应用">2.2. 部署 Kubernetes 应用</a>  </summary>
    <ul>
      <a href="#221-部署mysql8数据库">2.2.1. 部署MySQL8数据库</a><br>
    </ul>
  </details>
  </ul>
</details>


<h1>Devops</h1>

# 1. 动态Jenkins Slave

参考文档：

https://www.qikqiak.com/k8s-book/docs/36.Jenkins%20Slave.html

https://docs.ucloud.cn/uk8s/bestpractice/cicd

https://devopscube.com/setup-jenkins-on-kubernetes-cluster/

https://blog.csdn.net/gaobingjin/article/details/120966332

https://blog.csdn.net/weixin_34248258/article/details/92926584

## 1.1. Longhorn分布式存储安装

### 1.1.1. Longhorn简介

**Longhorn** 是用于 Kubernetes 的轻量级、可靠且功能强大的分布式**块存储**系统。

**块存储**会将数据拆分成块，并单独存储各个块。每个数据块都有一个唯一标识符，所以存储系统能将较小的数据存放在最方便的位置。这意味着有些数据可以存储在 [Linux®](https://www.redhat.com/zh/topics/linux) 环境中，有些则可以存储在 Windows 单元中。

`Longhorn` 使用容器(`containers`)和微服务(`microservices`)实现分布式块存储。

`Longhorn` 为每个块设备卷(`device volume`)创建一个专用的存储控制器(`storage controller`)，

并跨存储在多个节点上的多个副本同步复制该卷。存储控制器(`storage controller`)和副本(`replicas`)本身是使用 Kubernetes 编排的。

#### 1.1.1.1. 功能特性

- 无单点故障的企业级分布式块存储
- 块存储增量快照
- 备份到辅助存储（[NFS](https://www.extrahop.com/resources/protocols/nfs/)或[S3](https://aws.amazon.com/s3/)兼容的对象存储）建立在高效的更改块检测之上
- 定期快照和备份
- 自动化(`Automated`)、无中断升级(`non-disruptive upgrades`)。您可以升级整个 `Longhorn` 软件堆栈，而不会中断正在运行的存储卷。
- 直观的 `GUI` 仪表板

#### 1.1.1.2. Longhorn 是什么?

`Longhorn` 是 `Kubernetes` 的轻量级、可靠且易于使用的分布式块存储系统。

`Longhorn` 支持以下架构：

1. `AMD64`
2. `ARM64` (实验性的)

`Longhorn` 是免费的开源软件。最初由 `Rancher Labs` 开发，现在作为 `Cloud Native Computing Foundation` 的沙箱项目进行开发。

使用 `Longhorn`，您可以：

- 使用 `Longhorn` 卷作为 `Kubernetes` 集群中分布式有状态应用程序的持久存储
- 将块存储划分为 `Longhorn` 卷，这样无论是否有云提供商，都可以使用 `Kubernetes` 卷
- 跨多个节点和数据中心复制块存储以提高可用性
- 将备份数据存储在 `NFS` 或 `AWS S3` 等外部存储上
- 创建跨集群灾难恢复卷，以便可以从第二个 `Kubernetes` 集群的备份中快速恢复来自主 `Kubernetes` 集群的数据
- 安排卷的定期快照，并安排定期备份到 `NFS` 或 `S3` 兼容的辅助存储
- 从备份恢复卷
- 在不中断持久卷的情况下升级 `Longhorn`

`Longhorn` 带有独立的 `UI`，可以使用 `Helm`、`kubectl` 或 `Rancher app catalog` 进行安装。

#### 1.1.1.3. 使用微服务简化分布式块存储

由于现代云环境需要数万到数百万的分布式块存储卷，一些存储控制器已经成为高度复杂的分布式系统。
相比之下，`Longhorn` 可以通过将一个大块存储控制器划分为多个较小的存储控制器来简化存储系统，只要这些卷仍然可以从一个公共磁盘池构建。

通过每个卷使用一个存储控制器，Longhorn 将每个卷变成了一个微服务。控制器称为 Longhorn 引擎。

`Longhorn Manager` 组件编排 `Longhorn` 引擎，使它们协同工作。

#### 1.1.1.4. 在不依赖云提供商的情况下在 Kubernetes 中使用持久化存储

`Pod` 可以直接引用存储，但不推荐这样做，因为它不允许 `Pod` 或容器是可移植的。

相反，应在 `Kubernetes` 持久卷 (`PV`) 和持久卷声明 (`PVC`) 中定义工作负载的存储要求。

使用 `Longhorn`，您可以指定卷的大小、`IOPS` 要求以及在为卷提供存储资源的主机上所需的同步副本数量。

然后，您的 `Kubernetes` 资源可以为每个 `Longhorn` 卷使用 `PVC` 和相应的 `PV`，

或者使用 `Longhorn` 存储类(`storage class`)为工作负载自动创建 `PV`。

`Replicas` 在底层磁盘或网络存储上进行精简配置。

#### 1.1.1.5. 跨多个计算或存储主机调度多个副本(Replicas)

为了提高可用性(`availability`)，`Longhorn` 创建了每个卷的副本。副本包含卷的一系列快照，每个快照都存储来自前一个快照的更改。

卷的每个副本也在一个容器中运行，因此具有三个副本的卷会产生四个容器。

每个卷的副本数量可在 `Longhorn` 中配置，以及将安排副本的节点。`Longhorn` 监控每个副本的健康状况并执行修复，并在必要时重建副本。

#### 1.1.1.6. 为每个卷分配多个存储前端

常见的前端包括 `Linux` 内核设备（映射在 `/dev/longhorn` 下）和一个 `iSCSI` 目标。

#### 1.1.1.7. 指定定期快照和备份操作的计划

指定这些操作的频率（每小时、每天、每周、每月和每年）、执行这些操作的确切时间（例如，每个星期日凌晨 `3:00`），以及保留多少定期快照和备份集。

### 1.1.2. k8s安装并使用Longhorn（Master节点）

参考文章：

https://www.cnblogs.com/98record/p/k8s-gao-ke-yong-bu-shulonghorn.html

https://www.cnblogs.com/itzgr/p/13207220.html#_label0_0

https://www.codeleading.com/article/66636080154/

```bash
# 部署的时候，由于国内网不好，我先导入到GitLab（后面可以更新），之后用的是Longhorn最新版本（不能成功安装），因为某些镜像名字改变，在Docker Hub不存在，下载不了

# 检测环境是否准备好
curl -sSfL https://gitlab.com/yanglin5260/longhorn/-/raw/master/scripts/environment_check.sh | bash

# 部署longhorn之前先在节点上安装 open-iscsi，安装时候可能有些慢
kubectl apply -f https://gitlab.com/yanglin5260/longhorn/-/raw/master/deploy/prerequisite/longhorn-iscsi-installation.yaml
# 部署完成后，运行以下命令来检查安装程序的 pod 状态，运行完后应该是ErrImagePull状态
kubectl get pod -w | grep longhorn-iscsi-installation
# 通过下面的命令查看日志，出现 iscsi install successfully 字样表示安装成功
kubectl logs -c iscsi-installation `kubectl get pods -A|grep longhorn-iscsi-installation | awk '{print $2}' | sed -n '1p'`
kubectl logs -c iscsi-installation `kubectl get pods -A|grep longhorn-iscsi-installation | awk '{print $2}' | sed -n '2p'`
# 安装完之后卸载
kubectl delete -f https://gitlab.com/yanglin5260/longhorn/-/raw/master/deploy/prerequisite/longhorn-iscsi-installation.yaml
kubectl delete -f https://gitlab.com/yanglin5260/longhorn/-/raw/v1.1.3/deploy/prerequisite/longhorn-iscsi-installation.yaml

# CentOS8还可以通过下面的命令安装或者验证
yum install iscsi-initiator-utils

# 下载官方的安装文件，但是国内网不好，我先导入GitLab（后面可以更新），再下载。其中的-O选项是指定文件名的意思，用这种方法可以对单个文件进行覆盖
kubectl apply -f https://gitlab.com/yanglin5260/longhorn/-/raw/master/deploy/longhorn.yaml
wget https://gitlab.com/yanglin5260/longhorn/-/raw/master/deploy/longhorn.yaml -O longhorn.yaml
# 搜索app: longhorn-ui关键词，修改为 type: ClusterIP 为 type: NodePort，并修改 nodePort: null为nodePort: 30003，目的是为了使Longhornweb界面可以被外网访问
# 也修正了某些不能被下载的镜像，后面需要手动修改！！！！共5处，有一处单独的地方csi-node-driver-registrar镜像，需要注意还未下载成功，还需要修改相关的4对键值参数，共9处修改，还是未能成功！
vim longhorn.yaml
# 安装Longhorn
kubectl apply -f longhorn.yaml
# 观察安装是否成功的情况
watch -n 1 kubectl get pods --namespace longhorn-system -owide
kubectl describe pods --namespace longhorn-system longhorn-driver-deployer-68b65fc75-hpjst
kubectl describe pods --namespace longhorn-system longhorn-manager-ktqtp
kubectl describe pods --namespace longhorn-system longhorn-manager-zwhdp
kubectl describe pods --namespace longhorn-system longhorn-ui-7477474748-mz46h
kubectl describe pods --namespace longhorn-system csi-attacher-75588bff58-fw978
kubectl describe pods --namespace longhorn-system csi-provisioner-669c8cc698-cl4xp
kubectl describe pods --namespace longhorn-system csi-resizer-5c88bfd4cf-g65gh
kubectl describe pods --namespace longhorn-system longhorn-csi-plugin-dt8pr
kubectl describe pods --namespace longhorn-system longhorn-csi-plugin-ngtkb



# 如果安装不成功，通过下面的命令卸载，重复上述步骤重新安装
# 查看longhorn-system namespace是否被删除
kubectl get ns -A
# 如果过了几分钟没有被删除，可通过如下操作进行删除，可以先执行第一条强制删除namespace命令
kubectl delete ns longhorn-system
kubectl apply -f https://gitlab.com/yanglin5260/longhorn/-/raw/master/uninstall/uninstall.yaml
kubectl delete -f https://gitlab.com/yanglin5260/longhorn/-/raw/master/deploy/longhorn.yaml
# 查看卸载应用是否正常运行，是否已经完成
watch -n 1 kubectl get job/longhorn-uninstall -n default
# 查看longhorn-system namespace是否被删除
watch -n 1 kubectl get ns -A
# 实在删除不了，需要具体查看，可以reboot重启master节点!!!!
# 如果删除成功，最后卸载删除脚本
kubectl delete -f https://gitlab.com/yanglin5260/longhorn/-/raw/master/uninstall/uninstall.yaml
```

**open-iscsi**是一个实现 [RFC3720](https://tools.ietf.org/html/rfc7143) iSCSI协议的高性能initiator程序。iSCSI使得访问SAN上的存储不再只能依赖Fibre Channel，也可以通过TCP协议和以太网络。在很多Linux平台都可以方便的下载到open-iscsi包。

有了它就可以在Linux上直接连接远端的block device了，就像使用本地block device一样方便。



**提示：若部署异常可删除重建，有时候会出现无法删除longhorn-system namespace，若出现无法删除namespace，可通过如下操作进行删除：**

```bash
wget https://gitlab.com/yanglin5260/longhorn/-/raw/master/uninstall/uninstall.yaml
kubectl apply -f uninstall.yaml
kubectl delete -f longhorn.yaml
# 查看longhorn-system namespace是否被删除
kubectl get ns -A
```

## 1.2. 安装

1. 创建实际数据的存储位置172.16.26.3:/data/devops，Jenkins的数据都存储在这里

```bash
# 创建实际数据的存储位置172.16.26.3:/data/devops，Jenkins的数据都存储在这里
yum -y install rpcbind nfs-utils
mkdir -p /data/devops/jenkins2
chmod 777 /data/devops/jenkins2
sudo chown -R 1000:1000 /data/devops/jenkins2
echo "/data/devops *(insecure,rw,sync,no_root_squash)" > /etc/exports
systemctl enable rpcbind --now
systemctl enable nfs-server --now
exportfs -r
```

2. 利用下面命令部署或者卸载应用，[devops.yaml](./devops.yaml)

```bash
kubectl apply -f devops.yaml
kubectl delete -f devops.yaml
```

3. 访问Jenkins主页：http://172.16.26.3:30002，其中IP是当前集群任意宿主机的IP（比如当前三个节点的IP地址都可以使用），需要输入解锁密码！

下面的命令可以或者初始化页面解锁密码

```bash
# 获取解锁密码
kubectl -n devops exec `kubectl get pods -A|grep jenkins|awk '{print $2}'` -- cat /var/jenkins_home/secrets/initialAdminPassword

# 进入容器
kubectl -n devops exec -it `kubectl get pods -A|grep jenkins|awk '{print $2}'` -- /bin/bash 
```

**注意：由于当时使用3台虚拟机模拟3台服务器的，所以在在复制虚拟机的时候可能忽略了解析DNS的配置文件/etc/resolv.conf，需要配置网关以正确解析DNS，如果不配置可能导致k8s部署的pod不能够解析DNS以至于不能访问外网**

```bash
[root@k8s-node1 ~]# cat /etc/resolv.conf
# Generated by NetworkManager
nameserver 172.16.26.2
```

4. 安装推荐的插件，需要等一会，然后输入管理员账号、密码、邮箱等信息，初始化时候既可以使用
5. 配置时区，访问http://172.16.26.3:30002/user/admin/configure最下面可以配置

## 1.3. 优点

Jenkins 安装完成了，接下来我们不用急着就去使用，我们要了解下在 Kubernetes 环境下面使用 Jenkins 有什么好处。

我们知道持续构建与发布是我们日常工作中必不可少的一个步骤，目前大多公司都采用 Jenkins 集群来搭建符合需求的 CI/CD 流程，然而传统的 Jenkins Slave 一主多从方式会存在一些痛点，比如：

- 主 Master 发生单点故障时，整个流程都不可用了
- 每个 Slave 的配置环境不一样，来完成不同语言的编译打包等操作，但是这些差异化的配置导致管理起来非常不方便，维护起来也是比较费劲
- 资源分配不均衡，有的 Slave 要运行的 job 出现排队等待，而有的 Slave 处于空闲状态
- 资源有浪费，每台 Slave 可能是物理机或者虚拟机，当 Slave 处于空闲状态时，也不会完全释放掉资源。

![k8s-jenkins](./images/devops-1.png)

从图上可以看到 Jenkins Master 和 Jenkins Slave 以 Pod 形式运行在 Kubernetes 集群的  Node 上，Master 运行在其中一个节点，并且将其配置数据存储到一个 Volume 上去，Slave运行在各个节点上，并且它不是一直处于运行状态，它会**按照需求动态的创建并自动删除**。

这种方式的工作流程大致为：**当 Jenkins Master 接受到 Build 请求时，会根据配置的 Label 动态创建一个运行在Pod中的Jenkins Slave 并注册到 Master 上，当运行完 Job 后，这个 Slave 会被注销并且这个 Pod 也会自动删除，恢复到最初状态**。

使用这种方式的好处：

- **服务高可用**，当 Jenkins Master 出现故障时，Kubernetes 会自动创建一个新的 Jenkins Master 容器，并且将 Volume 分配给新创建的容器，保证数据不丢失，从而达到集群服务高可用。
- **动态伸缩**，合理使用资源，每次运行 Job 时，会自动创建一个 Jenkins Slave，Job  完成后，Slave 自动注销并删除容器，资源自动释放，而且 Kubernetes 会根据每个资源的使用情况，动态分配 Slave到空闲的节点上创建，降低出现因某节点资源利用率高，还排队等待在该节点的情况。
- **扩展性好**，当 Kubernetes 集群的资源严重不足而导致 Job 排队等待时，可以很容易的添加一个 Kubernetes Node 到集群中，从而实现扩展。

## 1.4. 配置

接下来我们就需要来配置 Jenkins，让他能够动态的生成 Slave 的 Pod，实现高可用。

1. 需要国际化设置，设置语言为英文，访问[可选插件](http://172.16.26.3:30002/pluginManager/available)或者点击Manage Jenkins -> Manage Plugins -> Available，搜索**Localization: Chinese（Simplified）**，卸载**Localization: Chinese（Simplified）**插件并重启。

2. 我们需要安装**kubernetes**，访问[可选插件](http://172.16.26.3:30002/pluginManager/available)或者点击 Manage Jenkins -> Manage Plugins -> Available，搜索**Kubernetes**直接点击**install without restart**即可。有时候会出现需要安装其他插件

3. 安装完毕后，访问[Configure Clouds](http://172.16.26.3:30002/configureClouds/) 点击 Dashboard --> Configure Clouds，点击Add a new cloud -->选择 Kubernetes-->点击右边的Kubernetes Cloud details，然后填写 Kubernetes 和 Jenkins 配置信息。

   ```
   Kubernetes地址---https://kubernetes.default.svc.cluster.local
   Kubernetes命名空间---devops
   Jenkins地址---http://jenkins2.devops.svc.cluster.local:8080
   # 其中k8s内部访问域名的格式：服务名.namespace.svc.cluster.local:<port>
   ```

4. 配置Pod Template。首先点击右下角的**Pod Templates...-->添加Pod模板-->Pod Templates details...**，填写信息如下，并保存。

   ```
   名称---jnlp6
   命名空间---devops
   标签列表---haimaxy-jnlp6
   容器列表->添加容器（Container Template）：
     名称->jnlp6
     Docker镜像：cnych/jenkins:jnlp6
     分配伪终端->勾选
   卷->添加卷->Host Path Volume
   	主机路径：/var/run/docker.sock，挂载路径：/var/run/docker.sock
   	主机路径：/root/.kube，挂载路径：/root/.kube
   代理的空闲存活时间（分）(Time in minutes to retain slave when idle)：默认就行
   Service Account（Pod权限配置）：jenkins2
   ```

   在配置完成后发现启动 Jenkins Slave Pod 的时候，出现 Slave Pod 连接不上，然后尝试100次连接之后销毁 Pod，然后会再创建一个 Slave Pod 继续尝试连接，无限循环。如果出现这种情况的话就需要将 Slave Pod 中的运行命令和参数两个值（**运行的命令，命令参数**）给清空掉。

![image-20211218210313788](./images/devops-2.png)

   5. 测试。接下来我们就来添加一个 Job 任务，点击[Create a job](http://172.16.26.3:30002/newJob)，填写完信息就Save保存信息。点击**Build now**触发构建。通过`kubectl get pods -n devops`命令观察 Kubernetes 集群中 Pod 的变化，一个新的 Pod：jnlp-hfmvd 被创建了。看是否能够在 Slave Pod 中执行，任务执行完成后看 Pod 是否会被销毁。最后应该会构建成功！！

      ```
      Enter an item name---haimaxy-jnlp6-slave-demo
      Pipeline->OK
      	Pipeline-->script:
      		podTemplate (inheritFrom: "jnlp6"){
              node(POD_LABEL) {
                  container('jnlp6'){
                      stage("Run docker"){
                          sh 'docker info'
                          sh 'kubectl get pods'
                      }
                  }
              }
          }
      
      
      ########################################
      podTemplate：用Pod模版示例化一个Pod配置并在kubernetes内自动创建
      inheritFrom：意思是创建的Pod配置继承自jenkins-slave-temp模版
      POD_LABEL：自动创建Pod的label
      container：选择哪个容器执行脚本
      ```

# 2. Jenkins Pipeline部署Kubernetes应用

上节课我们实现了在 Kubernetes 环境中动态生成 Jenkins Slave 的方法，这节课我们来给大家讲解下如何在 Jenkins 中来部署一个 Kubernetes 应用。

## 2.1. Jenkins Pipeline 介绍

要实现在 Jenkins 中的构建工作，可以有多种方式，我们这里采用比较常用的 Pipeline  这种方式。Pipeline，简单来说，就是一套运行在 Jenkins  上的工作流框架，将原来独立运行于单个或者多个节点的任务连接起来，实现单个任务难以完成的复杂流程**编排**和**可视化**的工作。

Jenkins Pipeline 有几个核心概念：

- Node：节点，一个 Node 就是一个 Jenkins 节点，Master 或者 Agent，是执行 Step 的具体运行环境，比如我们之前动态运行的 Jenkins Slave 就是一个 Node 节点
- Stage：阶段，一个 Pipeline 可以划分为若干个 Stage，每个 Stage 代表一组操作，比如：Build、Test、Deploy，Stage 是一个逻辑分组的概念，可以跨多个 Node
- Step：步骤，Step 是最基本的操作单元，可以是打印一句话，也可以是构建一个 Docker 镜像，由各类 Jenkins 插件提供，比如命令：sh 'make'，就相当于我们平时 shell 终端中执行 make 命令一样。

那么我们如何创建 Jenkins Pipline 呢？

- Pipeline 脚本是由 Groovy 语言实现的，但是我们没必要单独去学习 Groovy，当然你会的话最好
- Pipeline 支持两种语法：Declarative(声明式)和 Scripted Pipeline(脚本式)语法
- Pipeline 也有两种创建方法：可以直接在 Jenkins 的 Web UI 界面中输入脚本；也可以通过创建一个 Jenkinsfile 脚本文件放入项目源码库中
- 一般我们都推荐在 Jenkins 中直接从源代码控制(SCMD)中直接载入 Jenkinsfile Pipeline 这种方法

## 2.2. 部署 Kubernetes 应用

### 2.2.1. 部署MySQL8数据库

https://gitee.com/yanglin5260/yanglin/raw/master/Server/Kubernetes-k8s/mysql8-cluster/mysql8-app.yaml



