<h2>目录</h2>

<details open>
  <summary><a href="#1-linux-简介">1. Linux 简介</a></summary>
  <ul>
    <a href="#11-linux-简介">1.1. Linux 简介</a><br>
    <a href="#12-linux的发行版">1.2. Linux的发行版</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#2-linux-安装">2. Linux 安装</a></summary>
  <ul>
  <details open>
    <summary><a href="#21-centos">2.1. CentOS</a>  </summary>
    <ul>
      <a href="#211-vmware-fusion安装centos-8，配置静态网关（mac）">2.1.1. VMWare Fusion安装CentOS 8，配置静态网关（Mac）</a><br>
      <a href="#212-网卡配置centos8">2.1.2. 网卡配置(CentOS8)</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#3-linux-系统启动过程">3. Linux 系统启动过程</a></summary>
  <ul>
    <a href="#31-linux-系统启动过程">3.1. Linux 系统启动过程</a><br>
    <a href="#32-内核引导">3.2. 内核引导</a><br>
  <details open>
    <summary><a href="#33-运行init">3.3. 运行init</a>  </summary>
    <ul>
      <a href="#331-运行级别">3.3.1. 运行级别</a><br>
    </ul>
  </details>
    <a href="#34-系统初始化">3.4. 系统初始化</a><br>
    <a href="#35-用户登录系统">3.5. 用户登录系统</a><br>
    <a href="#36-图形模式与文字模式的切换方式">3.6. 图形模式与文字模式的切换方式</a><br>
    <a href="#37-linux-关机">3.7. Linux 关机</a><br>
  </ul>
</details>
  <a href="#4-linux-系统目录结构">4. Linux 系统目录结构</a><br>
<details open>
  <summary><a href="#5-linux-忘记密码解决方法（重置密码方法）">5. Linux 忘记密码解决方法（重置密码方法）</a></summary>
  <ul>
    <a href="#51-启动centos8系统（关机后启动），在开机界面选择第一行，按e">5.1. 启动centos8系统（关机后启动），在开机界面选择第一行，按e</a><br>
    <a href="#52-进入以下界面，找到ro并将其修改为rw-initsysrootbinbash">5.2. 进入以下界面，找到ro并将其修改为rw init=/sysroot/bin/bash</a><br>
    <a href="#53-同时按住ctrl和x键，系统进入以下界面">5.3. 同时按住ctrl和x键，系统进入以下界面</a><br>
    <a href="#54-输入以下命令修改密码">5.4. 输入以下命令修改密码</a><br>
    <a href="#55-同时按住ctrl和d键，进入以下界面，输入reboot，重启系统">5.5. 同时按住Ctrl和d键，进入以下界面，输入reboot，重启系统</a><br>
    <a href="#56-系统重启成功后，输入新设置的密码即可正常登录">5.6. 系统重启成功后，输入新设置的密码即可正常登录</a><br>
  </ul>
</details>
  <a href="#6-linux-远程登录">6. Linux 远程登录</a><br>
<details open>
  <summary><a href="#7-linux-文件基本属性">7. Linux 文件基本属性</a></summary>
  <ul>
    <a href="#71-linux文件属主和属组">7.1. Linux文件属主和属组</a><br>
  <details open>
    <summary><a href="#72-更改文件属性">7.2. 更改文件属性</a>  </summary>
    <ul>
      <a href="#721-chgrp：更改文件属组">7.2.1. chgrp：更改文件属组</a><br>
      <a href="#722-chown：更改文件属主，也可以同时更改文件属组">7.2.2. chown：更改文件属主，也可以同时更改文件属组</a><br>
      <a href="#723-chmod：更改文件9个属性">7.2.3. chmod：更改文件9个属性</a><br>
      <a href="#724-符号类型改变文件权限">7.2.4. 符号类型改变文件权限</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#8-linux-文件与目录管理">8. Linux 文件与目录管理</a></summary>
  <ul>
  <details open>
    <summary><a href="#81-处理目录的常用命令需要看第二遍">8.1. 处理目录的常用命令(需要看第二遍)</a>  </summary>
    <ul>
      <a href="#811-ls-list-files">8.1.1. ls (list files)</a><br>
      <a href="#812-cd-change-directory">8.1.2. cd (change directory)</a><br>
      <a href="#813-pwd-print-working-directory">8.1.3. pwd (Print Working Directory)</a><br>
      <a href="#814-mkdir-make-directory">8.1.4. mkdir (make directory)</a><br>
      <a href="#815-rmdir-remove-directory">8.1.5. rmdir (remove directory)</a><br>
      <a href="#816-cp-copy-file">8.1.6. cp (copy file)</a><br>
      <a href="#817-rm-remove">8.1.7. rm (remove)</a><br>
      <a href="#818-mv-move-file">8.1.8. mv (move file)</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#82-linux-文件内容查看">8.2. Linux 文件内容查看</a>  </summary>
    <ul>
      <a href="#821-catconcatenate-files-and-print-on-the-standard-output">8.2.1. cat(concatenate files and print on the standard output)</a><br>
      <a href="#822-tacconcatenate-and-print-files-in-reverse">8.2.2. tac(concatenate and print files in reverse)</a><br>
      <a href="#823-nlnumber-lines-of-files">8.2.3. nl(number lines of files)</a><br>
      <a href="#824-more">8.2.4. more</a><br>
      <a href="#825-less">8.2.5. less</a><br>
      <a href="#826-head">8.2.6. head</a><br>
      <a href="#827-tail">8.2.7. tail</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#9-linux-用户和用户组管理">9. Linux 用户和用户组管理</a></summary>
  <ul>
  <details open>
    <summary><a href="#91-linux系统用户账号的管理">9.1. Linux系统用户账号的管理</a>  </summary>
    <ul>
      <a href="#911-添加新的用户账号使用useradd命令，其语法如下：">9.1.1. 添加新的用户账号使用useradd命令，其语法如下：</a><br>
      <a href="#912-删除帐号">9.1.2. 删除帐号</a><br>
      <a href="#913-修改帐号">9.1.3. 修改帐号</a><br>
      <a href="#914-用户口令的管理">9.1.4. 用户口令的管理</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#92-linux系统用户组的管理">9.2. Linux系统用户组的管理</a>  </summary>
    <ul>
      <a href="#921-增加一个新的用户组使用groupadd命令">9.2.1. 增加一个新的用户组使用groupadd命令</a><br>
      <a href="#922-如果要删除一个已有的用户组，使用groupdel命令">9.2.2. 如果要删除一个已有的用户组，使用groupdel命令</a><br>
      <a href="#923-修改用户组的属性使用groupmod命令">9.2.3. 修改用户组的属性使用groupmod命令</a><br>
      <a href="#924-如果一个用户同时属于多个用户组，那么用户可以在用户组之间切换，以便具有其他用户组的权限。">9.2.4. 如果一个用户同时属于多个用户组，那么用户可以在用户组之间切换，以便具有其他用户组的权限。</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#93-与用户账号有关的系统文件">9.3. 与用户账号有关的系统文件</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#931-etcpasswd文件是用户管理工作涉及的最重要的一个文件。">9.3.1. /etc/passwd文件是用户管理工作涉及的最重要的一个文件。</a>    </summary>
      <ul>
        <a href="#9311-用户名是代表用户账号的字符串。">9.3.1.1. "用户名"是代表用户账号的字符串。</a><br>
        <a href="#9312-口令一些系统中，存放着加密后的用户口令字。">9.3.1.2. “口令”一些系统中，存放着加密后的用户口令字。</a><br>
        <a href="#9313-用户标识号是一个整数，系统内部用它来标识用户。">9.3.1.3. “用户标识号”是一个整数，系统内部用它来标识用户。</a><br>
        <a href="#9314-组标识号字段记录的是用户所属的用户组。">9.3.1.4. “组标识号”字段记录的是用户所属的用户组。</a><br>
        <a href="#9315-注释性描述字段记录着用户的一些个人情况。">9.3.1.5. “注释性描述”字段记录着用户的一些个人情况。</a><br>
        <a href="#9316-主目录，也就是用户的起始工作目录。">9.3.1.6. “主目录”，也就是用户的起始工作目录。</a><br>
        <a href="#9317-用户登录后，要启动一个进程，负责将用户的操作传给内核，这个进程是用户登录到系统后运行的命令解释器或某个特定的程序，即shell。">9.3.1.7. 用户登录后，要启动一个进程，负责将用户的操作传给内核，这个进程是用户登录到系统后运行的命令解释器或某个特定的程序，即Shell。</a><br>
      </ul>
    </details>
      <a href="#932-系统中有一类用户称为伪用户（pseudo-users）。">9.3.2. 系统中有一类用户称为伪用户（pseudo users）。</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#94-拥有帐户文件">9.4. 拥有帐户文件</a>  </summary>
    <ul>
      <a href="#941-除了上面列出的伪用户外，还有许多标准的伪用户，例如：audit-cron-mail-usenet等，它们也都各自为相关的进程和文件所需要。">9.4.1. 除了上面列出的伪用户外，还有许多标准的伪用户，例如：audit, cron, mail, usenet等，它们也都各自为相关的进程和文件所需要。</a><br>
      <a href="#942-etcshadow中的记录行与etcpasswd中的一一对应，它由pwconv命令根据etcpasswd中的数据自动产生">9.4.2. /etc/shadow中的记录行与/etc/passwd中的一一对应，它由pwconv命令根据/etc/passwd中的数据自动产生</a><br>
      <a href="#943-用户组的所有信息都存放在etcgroup文件中。">9.4.3. 用户组的所有信息都存放在/etc/group文件中。</a><br>
    <details open>
      <summary><a href="#944-添加批量用户">9.4.4. 添加批量用户</a>    </summary>
      <ul>
        <a href="#9441-先编辑一个文本用户文件。">9.4.4.1. 先编辑一个文本用户文件。</a><br>
        <a href="#9442-以root身份执行命令-`usrsbinnewusers`，从刚创建的用户文件`usertxt`中导入数据，创建用户：">9.4.4.2. 以root身份执行命令 `/usr/sbin/newusers`，从刚创建的用户文件`user.txt`中导入数据，创建用户：</a><br>
        <a href="#9443-执行命令usrsbinpwunconv。">9.4.4.3. 执行命令/usr/sbin/pwunconv。</a><br>
        <a href="#9444-编辑每个用户的密码对照文件。">9.4.4.4. 编辑每个用户的密码对照文件。</a><br>
        <a href="#9445-以-root-身份执行命令-`usrsbinchpasswd`。">9.4.4.5. 以 root 身份执行命令 `/usr/sbin/chpasswd`。</a><br>
        <a href="#9446-确定密码经编码写入etcpasswd的密码栏后。">9.4.4.6. 确定密码经编码写入/etc/passwd的密码栏后。</a><br>
      </ul>
    </details>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#10-linux-磁盘管理">10. Linux 磁盘管理</a></summary>
  <ul>
    <a href="#101-df">10.1. df</a><br>
    <a href="#102-du">10.2. du</a><br>
  <details open>
    <summary><a href="#103-fdisk">10.3. fdisk</a>  </summary>
    <ul>
      <a href="#1031-磁盘格式化">10.3.1. 磁盘格式化</a><br>
    </ul>
  </details>
    <a href="#104-磁盘检验">10.4. 磁盘检验</a><br>
    <a href="#105-磁盘挂载与卸除">10.5. 磁盘挂载与卸除</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#11-vivim">11. vi/vim</a></summary>
  <ul>
    <a href="#111-什么是-vim？">11.1. 什么是 vim？</a><br>
  <details open>
    <summary><a href="#112-vivim-的使用">11.2. vi/vim 的使用</a>  </summary>
    <ul>
      <a href="#1121-命令模式">11.2.1. 命令模式</a><br>
      <a href="#1122-输入模式">11.2.2. 输入模式</a><br>
      <a href="#1123-底线命令模式">11.2.3. 底线命令模式</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#113-vivim-按键说明">11.3. vi/vim 按键说明</a>  </summary>
    <ul>
      <a href="#1131-第一部分：一般模式可用的光标移动、复制粘贴、搜索替换等">11.3.1. 第一部分：一般模式可用的光标移动、复制粘贴、搜索替换等</a><br>
      <a href="#1132-第二部分：一般模式切换到编辑模式的可用的按钮说明">11.3.2. 第二部分：一般模式切换到编辑模式的可用的按钮说明</a><br>
      <a href="#1133-第三部分：一般模式切换到指令行模式的可用的按钮说明">11.3.3. 第三部分：一般模式切换到指令行模式的可用的按钮说明</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#12-yum-命令">12. yum 命令</a></summary>
  <ul>
    <a href="#121-yum-语法">12.1. yum 语法</a><br>
    <a href="#122-yum常用命令">12.2. yum常用命令</a><br>
  <details open>
    <summary><a href="#123-国内-yum-源">12.3. 国内 yum 源</a>  </summary>
    <ul>
      <a href="#1231-安装步骤">12.3.1. 安装步骤</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#13-apt-命令">13. apt 命令</a></summary>
  <ul>
    <a href="#131-apt-语法">13.1. apt 语法</a><br>
    <a href="#132-apt-常用命令">13.2. apt 常用命令</a><br>
  </ul>
</details>


<h1>Linux</h1>

# 1. Linux 简介

## 1.1. Linux 简介

------

Linux内核最初只是由芬兰人**李纳斯·托瓦兹（Linus Torvalds）**在赫尔辛基大学上学时出于个人爱好而编写的。

Linux是一套免费使用和自由传播的类Unix操作系统，是一个基于POSIX和UNIX的多用户、多任务、支持多线程和多CPU的操作系统。

Linux能运行主要的UNIX工具软件、应用程序和网络协议。它支持32位和64位硬件。Linux继承了Unix以网络为核心的设计思想，是一个性能稳定的多用户网络操作系统。

------

## 1.2. Linux的发行版

Linux的发行版说简单点就是将Linux内核与应用软件做一个打包。

目前市面上较知名的发行版有：Ubuntu、RedHat、CentOS、Debian、Fedora、SuSE、OpenSUSE、TurboLinux、BluePoint、RedFlag、Xterm、SlackWare等。

# 2. Linux 安装

## 2.1. CentOS

### 2.1.1. VMWare Fusion安装CentOS 8，配置静态网关（Mac）

<h6>VMWare Fusion安装CentOS 8，配置静态网关</h6>

准备工作：VMWare Fusion

- 安装centos8

安装centos(看图操作)

![img](./images/Linux-8.png)

centos8内核是4.x

![img](./images/Linux-9.png)

 

![img](./images/Linux-10.png)

 

![img](./images/Linux-11.png)

 

![img](./images/Linux-12.png)

 

![img](./images/Linux-13.png)

 

配置虚拟机参数

 

![img](./images/Linux-14.png)

 

配置一下三个模块的参数

![img](./images/Linux-15.png)

按云服务器最低配标准配置：

1核 （后期可调整）

2G 内存（后期可调整）

40G储存（后期可调整）

设置虚拟机-网络适配器-**选择NAT模式**（NAT可以不受Mac主机或者Windows主机是否连接WiFi的影响）。

![img](./images/Linux-16.png)

![img](./images/Linux-17.png)

选择刚刚下载的centos iso包

![img](./images/Linux-18.png)

![img](./images/Linux-19.png)

 

![img](./images/Linux-20.png)

修改语言支持

![img](./images/Linux-21.png)

修改以下4项参数

 

![img](./images/Linux-22.png)

 

 

![img](./images/Linux-23.png)

 

![img](./images/Linux-24.png)

分区，我选择了自动创建。也可以手动创建。

![img](./images/Linux-25.png)

 

![img](./images/Linux-26.png)

 

![img](./images/Linux-27.png)

 这一步会基于以太网自动生成网卡信息，以及**其中关键的IP地址信息**

![img](./images/Linux-28.png)

 

![img](./images/Linux-29.png)

 

![img](./images/Linux-30.png)

安装过程比较久，所以可以设置根密码。也就是账号为root那个密码。

![img](./images/Linux-31.png)

 

![img](./images/Linux-32.png)

 

![img](./images/Linux-33.png)

 

![img](./images/Linux-34.png)

### 2.1.2. 网卡配置(CentOS8)

备份镜像位置:[备份镜像文件夹](/Volumes/Work HD/utils/VM and OS/)下的**CentOS-Stream 8 64-bit-backup-2021-11-21-NAT-root/yanglin-duplicate-and-change-stastic-ipaddr-based-on-vmnet8-network-info.vmwarevm**，可以复制该镜像，让后进行网关配置

**注意：由于当时使用3台虚拟机模拟3台服务器的，所以在在复制虚拟机的时候可能忽略了解析DNS的配置文件/etc/resolv.conf，需要配置网关以正确解析DNS**

```bash
[root@k8s-node1 ~]# cat /etc/resolv.conf
# Generated by NetworkManager
nameserver 172.16.26.2
```



进入网关配置文件夹，进行双网卡配置

cd /etc/sysconfig/network-scripts

见到 ifcfg-*，**这个就是网关配置文件**就是这个网关配置的名称

![img](./images/Linux-35.png)

- 设置**内网静态IP**（为了内部网络能够互相联通）

安装系统后，就会自动生成下面的网卡ifcfg-ens*，但是需要用下面的命令把这个默认网卡修改成静态IP，并保存。

vim /etc/sysconfig/network-scripts/ifcfg-ens*

修改和新增配置：

```
// 修改
BOOTPROTO=static  //静态ip
ONBOOT=yes #将no改为yes
UUID=06cc84d6-a5f9-4055-88a2-d3b854d0ce5f // uuidgen ens37, 随机生成UUID, ens37是网卡名字
// 新增
// 设置网关，因为选择的是NAT模A，所以选择的是vmnet8虚拟网卡，Mac上执行下面的命令即可查看到对应vmnet8虚拟网卡的网关IP和子网掩码
// cat /Library/Preferences/VMware\ Fusion/vmnet8/nat.conf | grep gateway -A 2
NETMASK=255.255.255.0  #设置子网掩码
GATEWAY=172.16.26.2   #设置网关，需要和Mac、Windows电脑的网关一致！！！就是上面vmnet8虚拟网卡的网关IP
IPADDR=172.16.26.3 #设置centos IP静态地址地址，这时候静态IP自定义范围是172.16.26.3~172.16.26.255
//DNS1=114.114.114.114 #设置dns，也可以不设置
```

- 重启虚拟机所有网卡生效（CentOS8）

```
reboot
nmcli device reapply ens37   #重连网卡
nmcli connection reload ——重载网卡
nmcli connection up ens33 ——激活网卡ens33
nmcli connection down ens33 ——停用网卡ens33
nmcli connection down ens33 && nmcli connection up ens33 ——重启网卡ens33
nmcli device status
```

设置虚拟机 网络适配器

选择NAT模式。(如果已经选择NAT模式，则忽略下一步重启 centos)

![img](./images/Linux-42.png)

# 3. Linux 系统启动过程

## 3.1. Linux 系统启动过程

linux启动时我们会看到许多启动信息。

Linux系统的启动过程并不是大家想象中的那么复杂，其过程可以分为5个阶段：

- 内核的引导。
- 运行init。
- 系统初始化。
- 建立终端 。
- 用户登录系统。

------

## 3.2. 内核引导

当计算机打开电源后，首先是BIOS开机自检，按照BIOS中设置的启动设备（通常是硬盘）来启动。

操作系统接管硬件以后，首先读入 /boot 目录下的内核文件。

![bg2013081702](./images/Linux-1.png)

------

## 3.3. 运行init

init 进程是系统所有进程的起点，你可以把它比拟成系统所有进程的老祖宗，没有这个进程，系统中任何进程都不会启动。

init 程序首先是需要读取配置文件 /etc/inittab。

init程序的类型：

- **SysV**: init, CentOS 5之前, 配置文件： /etc/inittab。
- **Upstart**: init,CentOS 6, 配置文件： /etc/inittab(几乎被废弃), /etc/init/*.conf。
- **Systemd**： systemd, CentOS 7和8，配置文件： /usr/lib/systemd/system、 /etc/systemd/system。

![bg2013081703](./images/Linux-2.png)

### 3.3.1. 运行级别

许多程序需要开机启动。它们在Windows叫做"服务"（service），在Linux就叫做"守护进程"（daemon）。

init进程的一大任务，就是去运行这些开机启动的程序。

但是，不同的场合需要启动不同的程序，比如用作服务器时，需要启动Apache（Apache服务器），用作桌面就不需要。

Linux允许为不同的场合，分配不同的开机启动程序，这就叫做"运行级别"（runlevel）。也就是说，启动时根据"运行级别"，确定要运行哪些程序。

![bg2013081704](./images/Linux-3.png)

Linux系统有7个运行级别(runlevel)：

- 运行级别0：系统停机状态，系统默认运行级别不能设为0，否则不能正常启动
- 运行级别1：单用户工作状态，root权限，用于系统维护，禁止远程登陆
- 运行级别2：多用户状态(没有NFS，Network File System)
- 运行级别3：完全的多用户状态(有NFS，Network File System)，登陆后进入控制台命令行模式
- 运行级别4：系统未使用，保留
- 运行级别5：X11控制台（图形化桌面），登陆后进入图形GUI模式
- 运行级别6：系统正常关闭并重启，默认运行级别不能设为6，否则不能正常启动

------

## 3.4. 系统初始化

在init的配置文件中有这么一行： si::sysinit:/etc/rc.d/rc.sysinit　它调用执行了/etc/rc.d/rc.sysinit，而rc.sysinit是一个bash shell的脚本，它主要是完成一些系统初始化的工作，rc.sysinit是每一个运行级别都要首先运行的重要脚本。

它主要完成的工作有：激活交换分区，检查磁盘，加载硬件模块以及其它一些需要优先执行任务。

```
l5:5:wait:/etc/rc.d/rc 5
```

这一行表示以5为参数运行/etc/rc.d/rc，/etc/rc.d/rc是一个Shell脚本，它接受5作为参数，去执行/etc/rc.d/rc5.d/目录下的所有的rc启动脚本，/etc/rc.d/rc5.d/目录中的这些启动脚本实际上都是一些连接文件，而不是真正的rc启动脚本，真正的rc启动脚本实际上都是放在/etc/rc.d/init.d/目录下。

而这些rc启动脚本有着类似的用法，它们一般能接受start、stop、restart、status等参数。

/etc/rc.d/rc5.d/中的rc启动脚本通常是K或S开头的连接文件，对于以S开头的启动脚本，将以start参数来运行。

而如果发现存在相应的脚本也存在K打头的连接，而且已经处于运行态了(以/var/lock/subsys/下的文件作为标志)，则将首先以stop为参数停止这些已经启动了的守护进程，然后再重新运行。

这样做是为了保证是当init改变运行级别时，所有相关的守护进程都将重启。

至于在每个运行级中将运行哪些守护进程，用户可以通过chkconfig或setup中的"System Services"来自行设定。

![bg2013081705](./images/Linux-4.png)

## 3.5. 用户登录系统

一般来说，用户的登录方式有三种：

- （1）命令行登录
- （2）ssh登录
- （3）图形界面登录

![bg2013081706](./images/Linux-5.png)

对于运行级别为5的图形方式用户来说，他们的登录是通过一个图形化的登录界面。登录成功后可以直接进入KDE、Gnome等窗口管理器。

KDE，K桌面环境(K Desktop Environment)的缩写。KDE是Linux 操作系统上流行的桌面环境之一。

GNOME是一套纯粹自由的计算机软件，运行在操作系统上，提供图形桌面环境。



而本文主要讲的还是文本方式登录的情况：当我们看到mingetty的登录界面时，我们就可以输入用户名和密码来登录系统了。

Linux的账号验证程序是login，login会接收mingetty传来的用户名作为用户名参数。

然后login会对用户名进行分析：如果用户名不是root，且存在/etc/nologin文件，login将输出nologin文件的内容，然后退出。

这通常用来系统维护时防止非root用户登录。只有/etc/securetty中登记了的终端才允许root用户登录，如果不存在这个文件，则root可以在任何终端上登录。

/etc/usertty（CentOS8没有）文件用于对用户作出附加访问限制，如果不存在这个文件，则没有其他限制。

------

## 3.6. 图形模式与文字模式的切换方式

Linux预设提供了六个命令窗口终端机让我们来登录。

默认我们登录的就是第一个窗口，也就是tty1，这个六个窗口分别为tty1,tty2 … tty6，你可以按下Ctrl + Alt + F1 ~ F6 来切换它们。

如果你安装了图形界面，默认情况下是进入图形界面的，此时你就可以按Ctrl + Alt + F1 ~ F6来进入其中一个命令窗口界面。

当你进入命令窗口界面后再返回图形界面只要按下Ctrl + Alt + F7 就回来了。

如果你用的vmware 虚拟机，命令窗口切换的快捷键为 Alt + Space + F1~F6. 如果你在图形界面下请按Alt + Shift + Ctrl + F1~F6 切换至命令窗口。

![bg2013081707](./images/Linux-6.png)

------

## 3.7. Linux 关机

在linux领域内大多用在服务器上，很少遇到关机的操作。毕竟服务器上跑一个服务是永无止境的，除非特殊情况下，不得已才会关机。

正确的关机流程为：sync > shutdown > reboot > halt

关机指令为：shutdown ，你可以man shutdown 来看一下帮助文档。

例如你可以运行如下命令关机：

```bash
sync 将数据由内存同步到硬盘中。

shutdown 关机指令，你可以man shutdown 来看一下帮助文档。例如你可以运行如下命令关机：

shutdown –h 10 ‘This server will shutdown after 10 mins’ 这个命令告诉大家，计算机将在10分钟后关机，并且会显示在登陆用户的当前屏幕中。

Shutdown –h now 立马关机

Shutdown –h 20:25 系统会在今天20:25关机

Shutdown –h +10 十分钟后关机

Shutdown –r now 系统立马重启

Shutdown –r +10 系统十分钟后重启

reboot 就是重启，等同于 shutdown –r now

halt 关闭系统，等同于shutdown –h now 和 poweroff
```

最后总结一下，不管是重启系统还是关闭系统，首先要运行sync命令，把内存中的数据写到磁盘中。

关机的命令有 shutdown –h now halt poweroff 和 init 0 , 重启系统的命令有 shutdown –r now ， reboot 和 init 6.

# 4. Linux 系统目录结构

登录系统后，在当前命令窗口下输入命令：

```
 ls / 
```

你会看到如下图所示:

![img](./images/Linux-45.png)

树状目录结构：

![img](./images/Linux-45.jpg)

以下是对这些目录的解释：

- **/bin**：
  bin 是 Binaries (二进制文件) 的缩写, 这个目录存放着最经常使用的命令。

- **/boot：**
  这里存放的是启动 Linux 时使用的一些核心文件，包括一些连接文件以及镜像文件。

- **/dev ：**
  dev 是 Device(设备) 的缩写, 该目录下存放的是 Linux 的外部设备，在 Linux 中访问设备的方式和访问文件的方式是相同的。

- **/etc：**
  etc 是 Etcetera(等等) 的缩写,这个目录用来存放所有的系统管理所需要的配置文件和子目录。

- **/home**：
  用户的主目录，在 Linux 中，每个用户都有一个自己的目录，一般该目录名是以用户的账号命名的，如上图中的 alice、bob 和 eve。

- **/lib**：
  lib 是 Library(库) 的缩写这个目录里存放着系统最基本的动态连接共享库，其作用类似于 Windows 里的 DLL 文件。几乎所有的应用程序都需要用到这些共享库。

- **/lost+found**：
  这个目录一般情况下是空的，当系统非法关机后，这里就存放了一些文件。

- **/media**：
  linux 系统会自动识别一些设备，例如U盘、光驱等等，当识别后，Linux 会把识别的设备挂载到这个目录下。

- **/mnt**：
  系统提供该目录是为了让用户临时挂载别的文件系统的（mount），我们可以将光驱挂载在 /mnt/ 上，然后进入该目录就可以查看光驱里的内容了。

- **/opt**：
  opt 是 optional(可选) 的缩写，这是给主机额外安装软件所摆放的目录。比如你安装一个ORACLE数据库则就可以放到这个目录下。默认是空的。

- **/proc**：
  proc 是 Processes(进程) 的缩写，/proc 是一种伪文件系统（也即虚拟文件系统），存储的是当前内核运行状态的一系列特殊文件，这个目录是一个虚拟的目录，它是系统内存的映射，我们可以通过直接访问这个目录来获取系统信息。
  这个目录的内容不在硬盘上而是在内存里，我们也可以直接修改里面的某些文件，比如可以通过下面的命令来屏蔽主机的ping命令，使别人无法ping你的机器：

  ```bash
  echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_all
  ```

- **/root**：
  该目录为系统管理员，也称作超级权限者的用户主目录。

- **/sbin**：
  s 就是 Super User 的意思，是 Superuser Binaries (超级用户的二进制文件) 的缩写，这里存放的是系统管理员使用的系统管理程序。

- **/selinux**：
   这个目录是 Redhat/CentOS 所特有的目录(/etc/selinux/)，Selinux 是一个安全机制，类似于 windows 的防火墙，但是这套机制比较复杂，这个目录就是存放selinux相关的文件的。

- **/srv**：
   该目录存放一些服务启动之后需要提取的数据。

- **/sys**：

  这是 Linux2.6 内核的一个很大的变化。该目录下安装了 2.6 内核中新出现的一个文件系统 sysfs 。

  sysfs 文件系统集成了下面3种文件系统的信息：

  - 针对进程信息的 proc 文件系统

  - 针对设备的 devfs 文件系统

  - 针对伪终端的 devpts 文件系统。

  该文件系统是内核设备树的一个直观反映。

  当一个内核对象被创建的时候，对应的文件和目录也在内核对象子系统中被创建。

- **/tmp**：
  tmp 是 temporary(临时) 的缩写这个目录是用来存放一些临时文件的。

- **/usr**：
   usr 是 unix shared resources(共享资源) 的缩写，这是一个非常重要的目录，用户的很多应用程序和文件都放在这个目录下，类似于 windows 下的 program files 目录。

- **/usr/bin：**
  系统用户使用的应用程序。

- **/usr/sbin：**
  超级用户使用的比较高级的管理程序和系统守护程序。

- **/usr/src：**
  内核源代码默认的放置目录。

- **/var**：
  var 是 variable(变量) 的缩写，这个目录中存放着在不断扩充着的东西，我们习惯将那些经常被修改的目录放在这个目录下。包括各种日志文件。

- **/run**：
  是一个临时文件系统，存储系统启动以来的信息。当系统重启时，这个目录下的文件应该被删掉或清除。如果你的系统上有 /var/run 目录，应该让它指向 /run。

在 Linux 系统中，有几个目录是比较重要的，平时需要注意不要误删除或者随意更改内部文件。

**/etc**： 上边也提到了，这个是系统中的配置文件，如果你更改了该目录下的某个文件可能会导致系统不能启动。

**/bin, /sbin, /usr/bin, /usr/sbin**: 这是系统预设的执行文件的放置目录，比如 ls 就是在 /bin/ls 目录下的。

值得提出的是，/bin, /usr/bin 是给系统用户使用的指令（除root外的通用户），而/sbin, /usr/sbin 则是给 root 使用的指令。

**/var**： 这是一个非常重要的目录，系统上跑了很多程序，那么每个程序都会有相应的日志产生，而这些日志就被记录到这个目录下，具体在 /var/log 目录下，另外 mail 的预设放置也是在这里。

# 5. Linux 忘记密码解决方法（重置密码方法）

## 5.1. 启动centos8系统（关机后启动），在开机界面选择第一行，按e

![img](./images/Linux-46.png)

## 5.2. 进入以下界面，找到ro并将其修改为rw init=/sysroot/bin/bash

![img](./images/Linux-47.png)

## 5.3. 同时按住ctrl和x键，系统进入以下界面

![img](./images/Linux-48.png)

## 5.4. 输入以下命令修改密码

```bash
# chroot /sysroot/      /切换回原始系统/

# LANG=en           /把语言改为英文/

# passwd            /设置新密码/

# touch /.autorelabel    /使密码生效/
```

![img](./images/Linux-49.png)

## 5.5. 同时按住Ctrl和d键，进入以下界面，输入reboot，重启系统

![img](./images/Linux-50.png)

## 5.6. 系统重启成功后，输入新设置的密码即可正常登录

# 6. Linux 远程登录

- SSH服务端口号为22，可以使用ssh命令行登录（例如：`ssh ldz@192.168.0.1`），

- 文件传输FTP端口号为21，默认情况下FTP协议使用TCP端口中的20和21这两个端口，其中20用于传输数据，21用于传输控制信息。但是，是否使用20作为传输数据的端口与FTP使用的传输模式有关，如果采用主动模式，那么数据传输端口就是20；如果采用被动模式，则具体最终使用哪个端口要服务器端和客户端协商决定。

# 7. Linux 文件基本属性

Linux 系统是一种典型的多用户系统，不同的用户处于不同的地位，拥有不同的权限。

为了保护系统的安全性，Linux 系统对不同的用户访问同一文件（包括目录文件）的权限做了不同的规定。

在 Linux 中我们通常使用以下两个命令来修改文件或目录的所属用户与权限：

- chown (change ownership) ： 修改所属用户与组。
- chmod (change mode) ： 修改用户的权限。



在 Linux 中我们可以使用 **ls –l** 命令来显示一个文件的属性以及文件所属的用户和组。

在 Linux 中第一个字符代表这个文件是目录、文件或链接文件等等。

- 当为 **d** 则是目录
- 当为 **-** 则是文件；
- 若是 **l** 则表示为链接文档(link file)；
- 若是 **b** 则表示为装置文件里面的可供储存的接口设备(可随机存取装置)；
- 若是 **c** 则表示为装置文件里面的串行端口设备，例如键盘、鼠标(一次性读取装置)。

接下来的字符中，以三个为一组，且均为 **rwx** 的三个参数的组合。其中， **r** 代表可读(read)、 **w** 代表可写(write)、 **x** 代表可执行(execute)。 

每个文件的属性由左边第一部分的 10 个字符来确定。

![363003_1227493859FdXT](./images/Linux-52.png)

从左至右用 **0-9** 这些数字来表示。

第 **0** 位确定文件类型。

第 **1-3** 位确定属主（该文件的所有者）拥有该文件的权限。

第 **4-6** 位确定属组（所有者的同组用户）拥有该文件的权限。

第 **7-9** 位确定其他用户拥有该文件的权限。

第 **1、4、7** 位表示读权限，如果用 **r** 字符表示，则有读权限，如果用 **-** 字符表示，则没有读权限；

第 **2、5、8** 位表示写权限，如果用 **w** 字符表示，则有写权限，如果用 **-** 字符表示没有写权限；

第 **3、6、9** 位表示可执行权限，如果用 **x** 字符表示，则有执行权限，如果用 **-** 字符表示，则没有执行权限。

------

## 7.1. Linux文件属主和属组

文件所有者以外的用户又可以分为**文件所有者的同组用户**和**其他用户**。

因此，Linux系统按文件所有者、文件所有者同组用户和其他用户来规定了不同的文件访问权限。

**对于 root 用户来说，一般情况下，文件的权限对其不起作用**。

------

## 7.2. 更改文件属性

### 7.2.1. chgrp：更改文件属组

语法：

```bash
chgrp [-R] 属组名 文件名
```

参数选项

- -R：递归更改文件属组，就是在更改某个目录文件的属组时，如果加上-R的参数，那么该目录下的所有文件的属组都会更改。

### 7.2.2. chown：更改文件属主，也可以同时更改文件属组

语法：

```bash
chown [–R] 属主名 文件名
chown [-R] 属主名：属组名 文件名
```

进入 /root 目录（~）将install.log的拥有者改为bin这个账号：

```bash
[root@www ~] cd ~
[root@www ~]# chown bin install.log
[root@www ~]# ls -l
-rw-r--r--  1 bin  users 68495 Jun 25 08:53 install.log
```

将install.log的拥有者与群组改回为root：

```bash
[root@www ~]# chown root:root install.log
[root@www ~]# ls -l
-rw-r--r--  1 root root 68495 Jun 25 08:53 install.log
```

### 7.2.3. chmod：更改文件9个属性

Linux文件属性有两种设置方法，一种是数字，一种是符号。

Linux 文件的基本权限就有九个，分别是 **owner/group/others(拥有者/组/其他)** 三种身份各有自己的 **read/write/execute** 权限。

先复习一下刚刚上面提到的数据：文件的权限字符为： **-rwxrwxrwx** ， 这九个权限是三个三个一组的！其中，我们可以使用数字来代表各个权限，各权限的分数对照表如下：

- r:4
- w:2
- x:1

变更权限的指令 chmod 的语法：

```bash
 chmod [-R] xyz 文件或目录
```

选项与参数：

- xyz : 就是刚刚提到的数字类型的权限属性，为 rwx 属性数值的相加。
- -R : 进行递归(recursive)的持续变更，亦即连同次目录下的所有文件都会变更

### 7.2.4. 符号类型改变文件权限

还有一个改变权限的方法，从之前的介绍中我们可以发现，基本上就九个权限分别是：

- user：用户
- group：组
- others：其他

那么我们就可以使用 **u, g, o** 来代表三种身份的权限。

此外， **a** 则代表 **all**，即全部的身份。读写的权限可以写成 **r, w, x**，也就是可以使用下表的方式来看：

![image-20211206163249494](./images/Linux-7.png)

# 8. Linux 文件与目录管理

我们知道Linux的目录结构为树状结构，最顶级的目录为根目录 /。

- **绝对路径**：
  路径的写法，由根目录 **/** 写起。

- **相对路径**：

  路径的写法，不是由 **/** 写起，例如由 /usr/share/doc 要到 /usr/share/man 底下时，可以写成： **cd ../man** 这就是相对路径的写法。

------

你可以使用 **man [命令]** 来查看各个命令的使用文档，如 ：man cp。

## 8.1. 处理目录的常用命令(需要看第二遍)

### 8.1.1. ls (list files)

作用：ls 命令可能是最常被运行的，是用来列出目录及文件名

选项与参数：

- -a ：全部的文件，连同隐藏文件( 开头为 . 的文件) 一起列出来(常用)
- -d ：仅列出目录本身，而不是列出目录内的文件数据(常用)
- -l ：长数据串列出，包含文件的属性与权限等等数据；(常用)
- -h：human-readable, with -l and -s, print sizes like 1K 234M 2G etc.

### 8.1.2. cd (change directory)

作用：用来变换工作目录的命令。

语法：

```bash
 cd [相对路径或绝对路径]
#使用 mkdir 命令创建 runoob 目录
[root@www ~]# mkdir runoob

#使用绝对路径切换到 runoob 目录
[root@www ~]# cd /root/runoob/

#使用相对路径切换到 runoob 目录
[root@www ~]# cd ./runoob/

# 表示回到自己的家目录，亦即是 /root 这个目录
[root@www runoob]# cd ~

# 表示去到目前的上一级目录，亦即是 /root 的上一级目录的意思；
[root@www ~]# cd ..
```

### 8.1.3. pwd (Print Working Directory)

作用：显示目前所在目录的命令。

```bash
[root@www ~]# pwd [-P]
```

选项与参数：

- **-P, --physical** ：显示出**实际的物理路径**，而非使用**软连接或者硬链接 (link) 路径**。

### 8.1.4. mkdir (make directory)

作用：创建一个新的目录。

语法：

```bash
mkdir [-mp] 目录名称
```

选项与参数：

- **-m, --mode=MODE** ：配置文件的权限喔！直接配置，不需要看默认权限 (umask) 的脸色～，例如：mkdir -m 711 test2
- **-p, --parents** ：帮助你直接将所需要的目录(包含上一级目录)递归创建起来！

### 8.1.5. rmdir (remove directory)

作用：删除空的目录

语法：

```bash
 rmdir [-p] 目录名称
```

选项与参数：

- **-p, --parents**：连同上一级『空的』目录也一起删除

### 8.1.6. cp (copy file)

作用：拷贝文件和目录。

语法:

```bash
[root@www ~]# cp [-adfilprsu] 来源档(source) 目标档(destination)
[root@www ~]# cp [options] source1 source2 source3 .... directory
```

选项与参数：

- **-a, --archive**：相当于 -pdr 的意思，至於 pdr 请参考下列说明；(常用)
- **-d**：若来源档为连结档的属性(link file)，则复制连结档属性而非文件本身；
- **-f,--force**：为强制(force)的意思，若目标文件已经存在且无法开启，则移除后再尝试一次；
- **-i,--interactive**：若目标档(destination)已经存在时，在覆盖时会先询问动作的进行(常用)
- **-l, --link**：进行硬式连结(hard link)的连结档创建，而非复制文件本身；
- **-p, --preserve**：连同文件的属性一起复制过去，而非使用默认属性(备份常用)；
- **-R, -r, --recursive**：递归持续复制，用於目录的复制行为；(常用)
- **-s, --symbolic-link**：复制成为符号连结档 (symbolic link)，亦即『捷径』文件；
- **-u, --update**：若 destination 比 source 旧才升级 destination ！

### 8.1.7. rm (remove)

作用：移除文件或目录

语法：

```bash
 rm [-fir] 文件或目录
```

选项与参数：

- -f, --force ：就是 force 的意思，忽略不存在的文件，不会出现警告信息；
- -i：互动模式，在删除前会询问使用者是否动作
- -I：prompt once before removing more than three files, or when removing recursively; less intrusive than -i, while still giving protection against most mistakes
- -r, -R, --recursive：递归删除啊！最常用在目录的删除了！这是非常危险的选项！！！

### 8.1.8. mv (move file)

作用：移动文件与目录，或修改名称

语法：

```bash
[root@www ~]# mv [-fiu] source destination
[root@www ~]# mv [options] source1 source2 source3 .... directory
```

选项与参数：

- -f ：force 强制的意思，如果目标文件已经存在，不会询问而直接覆盖；
- -i, --interactive ：若目标文件 (destination) 已经存在时，就会询问是否覆盖！
- -u, --update ：若目标文件已经存在，且 source 比较新，才会升级 (update)

## 8.2. Linux 文件内容查看

### 8.2.1. cat(concatenate files and print on the standard output)

作用：由第一行开始显示文件内容

语法：

```bash
cat [-AbEnTv]
```

选项与参数：

- -A, --show-all ：相当於 -vET 的整合选项，可列出一些特殊字符而不是空白而已；
- -b, --number-nonblank ：列出行号，仅针对非空白行做行号显示，空白行不标行号！
- -e：equivalent to -vE
- -E, --show-ends ：将结尾的断行字节 $ 显示出来；
- -n, --number ：列印出行号，连同空白行也会有行号，与 -b 的选项不同；
- -T, --show-tabs ：将 [tab] 按键以 ^I 显示出来；
- -v, --show-nonprinting ：列出一些看不出来的特殊字符

### 8.2.2. tac(concatenate and print files in reverse)

tac与cat命令刚好相反，文件内容从最后一行开始显示，可以看出 tac 是 cat 的倒着写！

### 8.2.3. nl(number lines of files)

显示行号

语法：

```
nl [-bnw] 文件
```

选项与参数：

- -b, --body-numbering=STYLE ：指定行号指定的方式，主要有两种：
  -b a ：a->number all lines，表示不论是否为空行，也同样列出行号(类似 cat -n)；
  -b t ：t->number only nonempty lines，如果有空行，空的那一行不要列出行号(默认值)；
- -n ：列出行号表示的方法，主要有三种：
  -n ln ：left justified, no leading zeros，行号在荧幕的最左方显示，且不加 0；
  -n rn ：right justified, no leading zeros，行号在自己栏位的最右方显示，且不加 0 ；
  -n rz ：right justified, leading zeros，行号在自己栏位的最右方显示，且加 0 ；
- -w, --number-width=NUMBER ：use NUMBER columns for line numbers，行号栏位的占用的列数。

用 nl 列出 /etc/issue 的内容

```bash
[root@www ~]# nl /etc/issue
     1  CentOS release 6.4 (Final)
     2  Kernel \r on an \m
```

### 8.2.4. more

一页一页翻动

在 more 这个程序的运行过程中，你有几个按键可以按的：

- 空白键 (space)：代表向下翻一页；
- Enter     ：代表向下翻『一行』；
- /字串     ：代表在这个显示的内容当中，向下搜寻『字串』这个关键字；
- :f      ：立刻显示出当前文件名以及目前显示的行数；
- q or Q or INTERRUPT ：代表立刻离开 more ，不再显示该文件内容。
- b, ^B ：代表往回翻页，不过这动作只对文件有用，对管线无用。

### 8.2.5. less

一页一页翻动，以下实例输出/etc/man.config文件的内容：

less运行时可以输入的命令有：

- 空白键  ：向下翻动一页；
- [pagedown]：向下翻动一页；
- [pageup] ：向上翻动一页；
- /字串   ：向下搜寻『字串』的功能；
- ?字串   ：向上搜寻『字串』的功能；
- n     ：重复前一个搜寻 (与 / 或 ? 有关！)
- N     ：反向的重复前一个搜寻 (与 / 或 ? 有关！)
- q     ：离开 less 这个程序；

### 8.2.6. head

取出文件前面几行

语法：

```bash
head [-n number] 文件 
```

选项与参数：

- -n, --lines=[-]NUM ：后面接数字，代表显示几行的意思

默认的情况中，显示前面 10 行！若要显示前 20 行，就需要自定义参数

### 8.2.7. tail

取出文件后面几行

语法：

```bash
tail [-n number] 文件 
```

选项与参数：

- -n ：后面接数字，代表显示几行的意思
- -f, --follow[={name|descriptor}] ：表示持续侦测后面所接的档名，要等到按下[ctrl]-c才会结束tail的侦测

# 9. Linux 用户和用户组管理

Linux系统是一个多用户多任务的分时操作系统，任何一个要使用系统资源的用户，都必须首先向系统管理员申请一个账号，然后以这个账号的身份进入系统。

每个用户账号都拥有一个唯一的用户名和各自的口令。

用户在登录时键入正确的用户名和口令后，就能够进入系统和自己的主目录。

实现用户账号的管理，要完成的工作主要有如下几个方面：

- 用户账号的添加、删除与修改。
- 用户口令的管理。
- 用户组的管理。

------

## 9.1. Linux系统用户账号的管理

用户账号的管理工作主要涉及到用户账号的添加、修改和删除。

添加用户账号就是在系统中创建一个新账号，然后为新账号分配用户号、用户组、主目录和登录Shell等资源。刚添加的账号是被锁定的，无法使用。

### 9.1.1. 添加新的用户账号使用useradd命令，其语法如下：

```bash
useradd 选项 用户名
```

参数说明：

- 选项:

  - -c comment 指定一段注释性描述。
  - -d 目录 指定用户主目录，如果此目录不存在，则同时使用-m选项，可以创建主目录。
  - -g 用户组 指定用户所属的用户组。
  - -G 用户组，用户组 指定用户所属的附加组。
  - -s Shell文件 指定用户的登录Shell。
  - -u 用户号 指定用户的用户号，如果同时有-o选项，则可以重复使用其他用户的标识号。
  - -m, --create-home：Create the user's home directory if it does not exist. 
  - -o, --non-unique ：allow to create users with duplicate  (non-unique) UID

- 用户名:

  指定新账号的登录名

### 9.1.2. 删除帐号

如果一个用户的账号不再使用，可以从系统中删除。删除用户账号就是要将/etc/passwd等系统文件中的该用户记录删除，必要时还删除用户的主目录。

删除一个已有的用户账号使用`userdel`命令，其格式如下：

```bash
userdel 选项 用户名
```

常用的选项是 **-r, --remove**，它的作用是把用户的主目录一起删除。

### 9.1.3. 修改帐号

修改用户账号就是根据实际情况更改用户的有关属性，如用户号、主目录、用户组、登录Shell等。

修改已有用户的信息使用`usermod`命令，其格式如下：

```
usermod 选项 用户名
```

常用的选项包括`-c, -d, -m, -g, -G, -s, -u以及-o等`，这些选项的意义与`useradd`命令中的选项一样，可以为用户指定新的资源值。

另外，有些系统可以使用选项：-l 新用户名

这个选项指定一个新的账号，即将原来的用户名改为新的用户名。

### 9.1.4. 用户口令的管理

用户管理的一项重要内容是用户口令的管理。用户账号刚创建时没有口令，但是被系统锁定，无法使用，必须为其指定口令后才可以使用，即使是指定空口令。

指定和修改用户口令的Shell命令是`passwd`。超级用户可以为自己和其他用户指定口令，普通用户只能用它修改自己的口令。命令的格式为：

```bash
passwd 选项 用户名
```

可使用的选项：

- -l, --lock: 锁定口令，即禁用账号。
- -u, --unlock：口令解锁。
- -d, --delete：使账号无口令。
- -f, --force：强迫用户下次登录时修改口令。

如果默认用户名，则修改当前用户的口令。

例如，假设当前用户是sam，则下面的命令修改该用户自己的口令：

```bash
$ passwd 
Old password:****** 
New password:******* 
Re-enter new password:*******
```

普通用户修改自己的口令时，passwd命令会先询问原口令，验证后再要求用户输入两遍新口令，如果两次输入的口令一致，则将这个口令指定给用户；而超级用户为用户指定口令时，就不需要知道原口令。

为了系统安全起见，用户应该选择比较复杂的口令，例如最好使用8位长的口令，口令中包含有大写、小写字母和数字，并且应该与姓名、生日等不相同。

## 9.2. Linux系统用户组的管理

用户组的管理涉及用户组的添加、删除和修改。组的增加、删除和修改实际上就是对/etc/group文件的更新。

### 9.2.1. 增加一个新的用户组使用groupadd命令

```bash
groupadd 选项 用户组
```

可以使用的选项有：

- -g： GID 指定新用户组的组标识号（GID,The numerical value of the group's ID）。
- -o, --non-unique： 一般与-g选项同时使用，表示新用户组的GID可以与系统已有用户组的GID相同。

### 9.2.2. 如果要删除一个已有的用户组，使用groupdel命令

```bash
groupdel 用户组
```

### 9.2.3. 修改用户组的属性使用groupmod命令

```bash
groupmod 选项 用户组
```

常用的选项有：

- -g： GID 为用户组指定新的组标识号。
- -o： 与-g选项同时使用，用户组的新GID可以与系统已有用户组的GID相同。
- -n, --new-name NEW_GROUP：新用户组 将用户组的名字改为新名字

### 9.2.4. 如果一个用户同时属于多个用户组，那么用户可以在用户组之间切换，以便具有其他用户组的权限。

用户可以在登录后，使用命令newgrp(log in to a new group)切换到其他用户组，这个命令的参数就是目的用户组。例如：

```bash
$ newgrp root
```

这条命令将当前用户切换到root用户组，前提条件是root用户组确实是该用户的主组或附加组。类似于用户账号的管理，用户组的管理也可以通过集成的系统管理工具来完成。

## 9.3. 与用户账号有关的系统文件

完成用户管理的工作有许多种方法，但是每一种方法实际上都是对有关的系统文件进行修改。

与用户和用户组相关的信息都存放在一些系统文件中，这些文件包括/etc/passwd, /etc/shadow, /etc/group等。

### 9.3.1. /etc/passwd文件是用户管理工作涉及的最重要的一个文件。

Linux系统中的每个用户都在/etc/passwd文件中有一个对应的记录行，它记录了这个用户的一些基本属性。

这个文件对所有用户都是可读的。它的内容类似下面的例子：

```bash
＃ cat /etc/passwd

root:x:0:0:Superuser:/:
daemon:x:1:1:System daemons:/etc:
bin:x:2:2:Owner of system commands:/bin:
sys:x:3:3:Owner of system files:/usr/sys:
adm:x:4:4:System accounting:/usr/adm:
uucp:x:5:5:UUCP administrator:/usr/lib/uucp:
auth:x:7:21:Authentication administrator:/tcb/files/auth:
cron:x:9:16:Cron daemon:/usr/spool/cron:
listen:x:37:4:Network daemon:/usr/net/nls:
lp:x:71:18:Printer administrator:/usr/spool/lp:
sam:x:200:50:Sam san:/home/sam:/bin/sh
```

从上面的例子我们可以看到，/etc/passwd中一行记录对应着一个用户，每行记录又被冒号(:)分隔为7个字段，其格式和具体含义如下：

```
用户名:口令:用户标识号:组标识号:注释性描述:主目录:登录Shell
```

#### 9.3.1.1. "用户名"是代表用户账号的字符串。

通常长度不超过8个字符，并且由大小写字母和/或数字组成。登录名中不能有冒号(:)，因为冒号在这里是分隔符。

为了兼容起见，登录名中最好不要包含点字符(.)，并且不使用连字符(-)和加号(+)打头。

#### 9.3.1.2. “口令”一些系统中，存放着加密后的用户口令字。

虽然这个字段存放的只是用户口令的加密串，不是明文，但是由于/etc/passwd文件对所有用户都可读，所以这仍是一个安全隐患。因此，现在许多Linux 系统（如SVR4）都使用了shadow技术，把真正的加密后的用户口令字存放到/etc/shadow文件中，而在/etc/passwd文件的口令字段中只存放一个特殊的字符，例如“x”或者“*”。

#### 9.3.1.3. “用户标识号”是一个整数，系统内部用它来标识用户。

一般情况下它与用户名是一一对应的。如果几个用户名对应的用户标识号是一样的，系统内部将把它们视为同一个用户，但是它们可以有不同的口令、不同的主目录以及不同的登录Shell等。

通常用户标识号的取值范围是0～65 535。0是超级用户root的标识号，1～99由系统保留，作为管理账号，普通用户的标识号从100开始。在Linux系统中，这个界限是500。

#### 9.3.1.4. “组标识号”字段记录的是用户所属的用户组。

它对应着/etc/group文件中的一条记录。

#### 9.3.1.5. “注释性描述”字段记录着用户的一些个人情况。

例如用户的真实姓名、电话、地址等，这个字段并没有什么实际的用途。在不同的Linux 系统中，这个字段的格式并没有统一。在许多Linux系统中，这个字段存放的是一段任意的注释性描述文字。

#### 9.3.1.6. “主目录”，也就是用户的起始工作目录。

它是用户在登录到系统之后所处的目录。在大多数系统中，各用户的主目录都被组织在同一个特定的目录下，而用户主目录的名称就是该用户的登录名。各用户对自己的主目录有读、写、执行（搜索）权限，其他用户对此目录的访问权限则根据具体情况设置。

#### 9.3.1.7. 用户登录后，要启动一个进程，负责将用户的操作传给内核，这个进程是用户登录到系统后运行的命令解释器或某个特定的程序，即Shell。

Shell是用户与Linux系统之间的接口。Linux的Shell有许多种，每种都有不同的特点。常用的有sh(Bourne Shell), csh(C Shell), ksh(Korn Shell), tcsh(TENEX/TOPS-20 type C Shell), bash(Bourne Again Shell)等。

Bash是Bourne-Again SHell的缩写，为了纪念shell鼻祖Stephen Bourne(他是当代Unix Shell程序/bin/sh的创造者，该程序出现在贝尔实验室第七版Unix上)。

系统管理员可以根据系统情况和用户习惯为用户指定某个Shell。如果不指定Shell，那么系统使用sh为默认的登录Shell，即这个字段的值为/bin/sh。

用户的登录Shell也可以指定为某个特定的程序（此程序不是一个命令解释器）。

利用这一特点，我们可以限制用户只能运行指定的应用程序，在该应用程序运行结束后，用户就自动退出了系统。有些Linux 系统要求只有那些在系统中登记了的程序才能出现在这个字段中。

### 9.3.2. 系统中有一类用户称为伪用户（pseudo users）。

这些用户在/etc/passwd文件中也占有一条记录，但是不能登录，因为它们的登录Shell为空。它们的存在主要是方便系统管理，满足相应的系统进程对文件属主的要求。

常见的伪用户如下所示：

```
伪 用 户 含 义 
bin 拥有可执行的用户命令文件 
sys 拥有系统文件 
adm 拥有帐户文件 
uucp UUCP使用 
lp lp或lpd子系统使用 
nobody NFS使用
```

## 9.4. 拥有帐户文件

### 9.4.1. 除了上面列出的伪用户外，还有许多标准的伪用户，例如：audit, cron, mail, usenet等，它们也都各自为相关的进程和文件所需要。

由于/etc/passwd文件是所有用户都可读的，如果用户的密码太简单或规律比较明显的话，一台普通的计算机就能够很容易地将它破解，因此对安全性要求较高的Linux系统都把加密后的口令字分离出来，单独存放在一个文件中，这个文件是/etc/shadow文件。 有超级用户才拥有该文件读权限，这就保证了用户密码的安全性。

### 9.4.2. /etc/shadow中的记录行与/etc/passwd中的一一对应，它由pwconv命令根据/etc/passwd中的数据自动产生

它的文件格式与/etc/passwd类似，由若干个字段组成，字段之间用":"隔开。这些字段是：

```
登录名:加密口令:最后一次修改时间:最小时间间隔:最大时间间隔:警告时间:不活动时间:失效时间:标志
```

1. "登录名"是与/etc/passwd文件中的登录名相一致的用户账号
2. "口令"字段存放的是加密后的用户口令字，长度为13个字符。如果为空，则对应用户没有口令，登录时不需要口令；如果含有不属于集合 { ./0-9A-Za-z }中的字符，则对应的用户不能登录。
3. "最后一次修改时间"表示的是从某个时刻起，到用户最后一次修改口令时的天数。时间起点对不同的系统可能不一样。例如在SCO Linux 中，这个时间起点是1970年1月1日。
4. "最小时间间隔"指的是两次修改口令之间所需的最小天数。
5. "最大时间间隔"指的是口令保持有效的最大天数。
6. "警告时间"字段表示的是从系统开始警告用户到用户密码正式失效之间的天数。
7. "不活动时间"表示的是用户没有登录活动但账号仍能保持有效的最大天数。
8. "失效时间"字段给出的是一个绝对的天数，如果使用了这个字段，那么就给出相应账号的生存期。期满后，该账号就不再是一个合法的账号，也就不能再用来登录了。

下面是/etc/shadow的一个例子：

```
/＃ cat /etc/shadow

root:Dnakfw28zf38w:8764:0:168:7:::
daemon:*::0:0::::
bin:*::0:0::::
sys:*::0:0::::
adm:*::0:0::::
uucp:*::0:0::::
nuucp:*::0:0::::
auth:*::0:0::::
cron:*::0:0::::
listen:*::0:0::::
lp:*::0:0::::
sam:EkdiSECLWPdSa:9740:0:0::::
```

### 9.4.3. 用户组的所有信息都存放在/etc/group文件中。

将用户分组是Linux 系统中对用户进行管理及控制访问权限的一种手段。

每个用户都属于某个用户组；一个组中可以有多个用户，一个用户也可以属于不同的组。

**当一个用户同时是多个组中的成员时，在/etc/passwd文件中记录的是用户所属的主组，也就是登录时所属的默认组，而其他组称为附加组**。

用户要访问属于附加组的文件时，必须首先使用newgrp命令使自己成为所要访问的组中的成员。

用户组的所有信息都存放在/etc/group文件中。此文件的格式也类似于/etc/passwd文件，由冒号(:)隔开若干个字段，这些字段有：

```
组名:口令:组标识号:组内用户列表
```

1. "组名"是用户组的名称，由字母或数字构成。与/etc/passwd中的登录名一样，组名不应重复。
2. "口令"字段存放的是用户组加密后的口令字。一般Linux 系统的用户组都没有口令，即这个字段一般为空，或者是*。
3. "组标识号"与用户标识号类似，也是一个整数，被系统内部用来标识组。
4. "组内用户列表"是属于这个组的所有用户的列表，不同用户之间用逗号(,)分隔。这个用户组可能是用户的主组，也可能是附加组。

/etc/group文件的一个例子如下：

```
root::0:root
bin::2:root,bin
sys::3:root,uucp
adm::4:root,adm
daemon::5:root,daemon
lp::7:root,lp
users::20:root,sam
```

### 9.4.4. 添加批量用户

添加和删除用户对每位Linux系统管理员都是轻而易举的事，比较棘手的是如果要添加几十个、上百个甚至上千个用户时，我们不太可能还使用useradd一个一个地添加，必然要找一种简便的创建大量用户的方法，方法如下：

#### 9.4.4.1. 先编辑一个文本用户文件。

每一列按照`/etc/passwd`密码文件的格式书写，要注意每个用户的用户名、UID、宿主目录都不可以相同，其中密码栏可以留做空白或输入x号。

#### 9.4.4.2. 以root身份执行命令 `/usr/sbin/newusers`，从刚创建的用户文件`user.txt`中导入数据，创建用户：

```bash
# newusers < user.txt
```

然后可以执行命令 `vipw` 或 `vi /etc/passwd` 检查 `/etc/passwd` 文件是否已经出现这些用户的数据，并且用户的宿主目录是否已经创建。

#### 9.4.4.3. 执行命令/usr/sbin/pwunconv。

将 `/etc/shadow` 产生的 `shadow` 密码解码，然后回写到 `/etc/passwd` 中，并将`/etc/shadow`的`shadow`密码栏删掉。这是为了方便下一步的密码转换工作，即先取消 `shadow password` 功能。

```bahs
# pwunconv
```

#### 9.4.4.4. 编辑每个用户的密码对照文件。

格式为：

```
用户名:密码
```

实例文件 `passwd.txt` 内容如下：

```
user001:123456
user002:123456
user003:123456
user004:123456
user005:123456
user006:123456
```

#### 9.4.4.5. 以 root 身份执行命令 `/usr/sbin/chpasswd`。

创建用户密码，`chpasswd` 会将经过 `/usr/bin/passwd` 命令编码过的密码写入 `/etc/passwd` 的密码栏。

```bash
# chpasswd < passwd.txt
```

#### 9.4.4.6. 确定密码经编码写入/etc/passwd的密码栏后。

执行命令 `/usr/sbin/pwconv` 将密码编码为 `shadow password`，并将结果写入 `/etc/shadow`。

```bash
# pwconv
```

这样就完成了大量用户的创建了，之后您可以到/home下检查这些用户宿主目录的权限设置是否都正确，并登录验证用户密码是否正确。

# 10. Linux 磁盘管理

Linux磁盘管理好坏直接关系到整个系统的性能问题。

Linux磁盘管理常用三个命令为df、du和fdisk。

- df：display free disk space，列出可用的磁盘空间
- du：display disk usage statistics，列出使用了的磁盘空间
- fdisk：DOS partition maintenance program，用于磁盘分区

------

## 10.1. df

df命令参数功能：检查文件系统的磁盘空间占用情况。可以利用该命令来获取硬盘被占用了多少空间，目前还剩下多少空间等信息。

语法：

```bash
df [-ahikHTm] [目录或文件名]
```

选项与参数：

- -a,--all：include pseudo, duplicate, inaccessible file systems，列出所有的文件系统，包括系统特有的 /proc 等文件系统；
- -k ：like --block-size=1K，以 KBytes 的容量显示各文件系统；
- -m ：df --block-size=1M（CentOS8是这样的），以 MBytes 的容量显示各文件系统；
- -h,--human-readable：print sizes in powers of 1024 (e.g., 1023M)，以人们较易阅读的 GBytes, MBytes, KBytes 等格式自行显示；

- -H,--si：print sizes in powers of 1000 (e.g., 1.1G)，以 M=1000K 取代 M=1024K 的进位方式；
- -T, --print-type ：显示文件系统类型, 连同该 partition 的 filesystem 名称 (例如 ext3) 也列出；
- -i, --inodes： list inode information instead of block usage，不用硬盘容量，而以 inode 的数量来显示

## 10.2. du

Linux du命令也是查看使用空间的，但是与df命令不同的是Linux du命令是对文件和目录磁盘使用的空间的查看，还是和df命令有一些区别的，这里介绍Linux du命令。

语法：

```
du [-ahskm] 文件或目录名称
```

选项与参数：

- -a, --all ：write counts for all files, not just directories，列出所有的文件与目录容量，因为默认仅统计目录底下的文件量而已。
- -h,--human-readable：以人们较易读的容量格式 (G/M) 显示；
- -s,--summarize：display only a total for each argument，列出总量而已，而不列出每个各别的目录占用容量；
- -S,--separate-dirs：for directories do not include size of subdirectories不包括子目录下的总计，与 -s 有点差别。
- -k,--block-size=1K：以 KBytes 列出容量显示；
- -m,--block-size=1M ：以 MBytes 列出容量显示；
- -B, --block-size=SIZE

与 df 不一样的是，du 这个命令其实会直接到文件系统内去搜寻所有的文件数据。

------

## 10.3. fdisk

fdisk 是 Linux 的磁盘分区表操作工具。manipulate disk partition table

语法：

```bash
fdisk [-l] 装置名称
```

选项与参数：

- -l ：List the partition tables for the specified devices and then exit.  If no devices are given, those mentioned in /proc/partitions (if that file exists) are used.输出后面接的装置所有的分区内容。若仅有 fdisk -l 时， 则系统将会把整个系统内能够搜寻到的装置的分区均列出来。

### 10.3.1. 磁盘格式化

磁盘分割完毕后自然就是要进行文件系统的格式化，格式化的命令非常的简单，使用 `mkfs`（make filesystem） 命令。

语法：

```bash
mkfs [-t 文件系统格式] 装置文件名
```

选项与参数：

- -t, --type：可以接文件系统格式，例如 ext3, ext2, vfat 等(系统有支持才会生效)



查看 mkfs 支持的文件格式

```bash
[root@www ~]# mkfs[tab][tab]
mkfs         mkfs.cramfs  mkfs.ext2    mkfs.ext3    mkfs.msdos   mkfs.vfat
```

按下两个[tab]，会发现 mkfs 支持的文件格式如上所示。

## 10.4. 磁盘检验

fsck（check and repair a Linux filesystem）用来检查和维护不一致的文件系统。

若系统掉电或磁盘发生问题，可利用fsck命令对文件系统进行检查。

语法：

```bash
fsck [-t 文件系统] [-ACay] 装置名称
```

选项与参数：

- -t : 给定档案系统的型式，若在 /etc/fstab 中已有定义或 kernel 本身已支援的则不需加上此参数
- -s : 依序一个一个地执行 fsck 的指令来检查
- -A : 对/etc/fstab 中所有列出来的 分区（partition）做检查
- -C : 显示完整的检查进度
- -d : 打印出 e2fsck 的 debug 结果
- -p : 同时有 -A 条件时，同时有多个 fsck 的检查一起执行
- -R : 同时有 -A 条件时，省略 / 不检查
- -V : 详细显示模式
- -a : 如果检查有错则自动修复
- -r : 如果检查有错则由使用者回答是否修复
- -y : 选项指定检测每个文件是自动输入yes，在不确定那些是不正常的时候，可以执行 # fsck -y 全部检查修复。



查看系统有多少文件系统支持的 fsck 命令：

```bash
[root@www ~]# fsck[tab][tab]
fsck         fsck.cramfs  fsck.ext2    fsck.ext3    fsck.msdos   fsck.vfat
```

## 10.5. 磁盘挂载与卸除

Linux 的磁盘挂载使用 `mount` 命令，卸载使用 `umount` 命令。

磁盘挂载语法：

```bash
mount [-t 文件系统] [-L Label名] [-o 额外选项] [-n]  装置文件名  挂载点
```

用默认的方式，将刚刚创建的 /dev/mapper/cl-root 挂载到 /mnt/hdc6 上面！

```bash
[root@www ~]# mkdir /mnt/hdc6
[root@www ~]# mount /dev/mapper/cl-root /mnt/hdc6
[root@www ~]# df
Filesystem           1K-blocks      Used Available Use% Mounted on
.....中间省略.....
/dev/mapper/cl-root  1976312     42072   1833836   3% /mnt/hdc6
```



磁盘卸载命令 `umount` 语法：

```bash
umount [-fn] 装置文件名或挂载点
```

选项与参数：

- -f ：强制卸除！可用在类似网络文件系统 (NFS) 无法读取到的情况下；
- -n ：不升级 /etc/mtab 情况下卸除。

卸载/dev/mapper/cl-root

```bash
[root@www ~]# umount /dev/mapper/cl-root
```

# 11. vi/vim

## 11.1. 什么是 vim？

Vim是从 vi 发展出来的一个文本编辑器。代码补完、编译及错误跳转等方便编程的功能特别丰富，在程序员中被广泛使用。

vim 键盘图：

![img](./images/Linux-53.gif)

------

## 11.2. vi/vim 的使用

基本上 vi/vim 共分为三种模式，分别是**命令模式（Command mode）**，**输入模式（Insert mode）**和**底线命令模式（Last line mode）**。 这三种模式的作用分别是：

### 11.2.1. 命令模式

用户刚刚启动 vi/vim，便进入了命令模式。

此状态下敲击键盘动作会被Vim识别为命令，而非输入字符。比如我们此时按下i，并不会输入一个字符，i被当作了一个命令。

以下是常用的几个命令：

- **i** 切换到输入模式，以输入字符。
- **x** 删除当前光标所在处的字符。
- **:** 切换到底线命令模式，以在最底一行输入命令。

若想要编辑文本：启动Vim，进入了命令模式，按下i，切换到输入模式。

命令模式只有一些最基本的命令，因此仍要依靠底线命令模式输入更多命令。

### 11.2.2. 输入模式

在命令模式下按下i就进入了输入模式。

在输入模式中，可以使用以下按键：

- **字符按键以及Shift组合**，输入字符
- **ENTER**，回车键，换行
- **BACK SPACE**，退格键，删除光标前一个字符
- **DEL**，删除键，删除光标后一个字符
- **方向键**，在文本中移动光标
- **HOME**/**END**，移动光标到行首/行尾
- **Page Up**/**Page Down**，上/下翻页
- **Insert**，切换光标为输入/替换模式，光标将变成竖线/下划线
- **ESC**，退出输入模式，切换到命令模式

### 11.2.3. 底线命令模式

在命令模式下按下:（英文冒号）就进入了底线命令模式。

底线命令模式可以输入单个或多个字符的命令，可用的命令非常多。

在底线命令模式中，基本的命令有（已经省略了冒号）：

- q 退出程序
- w 保存文件

按ESC键可随时退出底线命令模式。

简单的说，我们可以将这三个模式想成底下的图标来表示：

![img](./images/Linux-53.png)

## 11.3. vi/vim 按键说明

除了上面简易范例的 i, Esc, :wq 之外，其实 vim 还有非常多的按键可以使用。

### 11.3.1. 第一部分：一般模式可用的光标移动、复制粘贴、搜索替换等

| 移动光标的方法                                               |                                                              |
| :----------------------------------------------------------- | ------------------------------------------------------------ |
| h 或 向左箭头键(←)                                           | 光标向左移动一个字符                                         |
| j 或 向下箭头键(↓)                                           | 光标向下移动一个字符                                         |
| k 或 向上箭头键(↑)                                           | 光标向上移动一个字符                                         |
| l 或 向右箭头键(→)                                           | 光标向右移动一个字符                                         |
| 如果你将右手放在键盘上的话，你会发现 hjkl 是排列在一起的，因此可以使用这四个按钮来移动光标。 如果想要进行多次移动的话，例如向下移动 30 行，可以使用 "30j" 或 "30↓" 的组合按键， 亦即加上想要进行的次数(数字)后，按下动作即可！ |                                                              |
| [Ctrl] + [f]                                                 | 屏幕『向下』移动一页，相当于 [Page Down]按键 (常用)          |
| [Ctrl] + [b]                                                 | 屏幕『向上』移动一页，相当于 [Page Up] 按键 (常用)           |
| [Ctrl] + [d]                                                 | 屏幕『向下』移动半页                                         |
| [Ctrl] + [u]                                                 | 屏幕『向上』移动半页                                         |
| +                                                            | 光标移动到非空格符的下一行                                   |
| -                                                            | 光标移动到非空格符的上一行                                   |
| n<space>                                                     | 那个 n 表示『数字』，例如 20 。按下数字后再按空格键，光标会向右移动这一行的 n 个字符。例如 20<space> 则光标会向后面移动 20 个字符距离。 |
| 0 或功能键[Home]                                             | 这是数字『 0 』：移动到这一行的最前面字符处 (常用)           |
| $ 或功能键[End]                                              | 移动到这一行的最后面字符处(常用)                             |
| H                                                            | 光标移动到这个屏幕的最上方那一行的第一个字符                 |
| M                                                            | 光标移动到这个屏幕的中央那一行的第一个字符                   |
| L                                                            | 光标移动到这个屏幕的最下方那一行的第一个字符                 |
| G                                                            | 移动到这个档案的最后一行(常用)                               |
| nG                                                           | n 为数字。移动到这个档案的第 n 行。例如 20G 则会移动到这个档案的第 20 行(可配合 :set nu) |
| gg                                                           | 移动到这个档案的第一行，相当于 1G 啊！ (常用)                |
| n<Enter>                                                     | n 为数字。光标向下移动 n 行(常用)                            |
| 搜索替换                                                     |                                                              |
| /word                                                        | 向光标之下寻找一个名称为 word 的字符串。例如要在档案内搜寻 vbird 这个字符串，就输入 /vbird 即可！ (常用) |
| ?word                                                        | 向光标之上寻找一个字符串名称为 word 的字符串。               |
| n                                                            | 这个 n 是英文按键。代表重复前一个搜寻的动作。举例来说， 如果刚刚我们执行 /vbird 去向下搜寻 vbird 这个字符串，则按下 n 后，会向下继续搜寻下一个名称为 vbird 的字符串。如果是执行 ?vbird 的话，那么按下 n 则会向上继续搜寻名称为 vbird 的字符串！ |
| N                                                            | 这个 N 是英文按键。与 n 刚好相反，为『反向』进行前一个搜寻动作。 例如 /vbird 后，按下 N 则表示『向上』搜寻 vbird 。 |
| 使用 /word 配合 n 及 N 是非常有帮助的！可以让你重复的找到一些你搜寻的关键词！ |                                                              |
| :n1,n2s/word1/word2/g                                        | n1 与 n2 为数字。在第 n1 与 n2 行之间寻找 word1 这个字符串，并将该字符串取代为 word2 ！举例来说，在 100 到 200 行之间搜寻 vbird 并取代为 VBIRD 则： 『:100,200s/vbird/VBIRD/g』。(常用) |
| **:1,$s/word1/word2/g** 或 **:%s/word1/word2/g**             | 从第一行到最后一行寻找 word1 字符串，并将该字符串取代为 word2 ！(常用) |
| **:1,$s/word1/word2/gc** 或 **:%s/word1/word2/gc**           | 从第一行到最后一行寻找 word1 字符串，并将该字符串取代为 word2 ！且在取代前显示提示字符给用户确认 (confirm) 是否需要取代！(常用) |
| 删除、复制与贴上                                             |                                                              |
| x, X                                                         | 在一行字当中，x 为向后删除一个字符 (相当于 [del] 按键)， X 为向前删除一个字符(相当于 [backspace] 亦即是退格键) (常用) |
| nx                                                           | n 为数字，连续向后删除 n 个字符。举例来说，我要连续删除 10 个字符， 『10x』。 |
| dd                                                           | 删除游标所在的那一整行(常用)                                 |
| ndd                                                          | n 为数字。删除光标所在的向下 n 行，例如 20dd 则是删除 20 行 (常用) |
| d1G                                                          | 删除光标所在到第一行的所有数据                               |
| dG                                                           | 删除光标所在到最后一行的所有数据                             |
| d$                                                           | 删除游标所在处，到该行的最后一个字符                         |
| d0                                                           | 那个是数字的 0 ，删除游标所在处，到该行的最前面一个字符      |
| yy                                                           | 复制游标所在的那一行(常用)                                   |
| nyy                                                          | n 为数字。复制光标所在的向下 n 行，例如 20yy 则是复制 20 行(常用) |
| y1G                                                          | 复制游标所在行到第一行的所有数据                             |
| yG                                                           | 复制游标所在行到最后一行的所有数据                           |
| y0                                                           | 复制光标所在的那个字符到该行行首的所有数据                   |
| y$                                                           | 复制光标所在的那个字符到该行行尾的所有数据                   |
| p, P                                                         | p 为将已复制的数据在光标下一行贴上，P 则为贴在游标上一行！ 举例来说，我目前光标在第 20 行，且已经复制了 10 行数据。则按下 p 后， 那 10 行数据会贴在原本的 20 行之后，亦即由 21 行开始贴。但如果是按下 P 呢？ 那么原本的第 20 行会被推到变成 30 行。 (常用) |
| J                                                            | 将光标所在行与下一行的数据结合成同一行                       |
| c                                                            | 重复删除多个数据，例如向下删除 10 行，[ 10cj ]               |
| u                                                            | 复原前一个动作。(常用)                                       |
| [Ctrl]+r                                                     | 重做上一个动作。(常用)                                       |
| 这个 u 与 [Ctrl]+r 是很常用的指令！一个是复原，另一个则是重做一次～ 利用这两个功能按键，你的编辑，嘿嘿！很快乐的啦！ |                                                              |
| .                                                            | 不要怀疑！这就是小数点！意思是重复前一个动作的意思。 如果你想要重复删除、重复贴上等等动作，按下小数点『.』就好了！ (常用) |

### 11.3.2. 第二部分：一般模式切换到编辑模式的可用的按钮说明

| 进入输入或取代的编辑模式                                     |                                                              |
| :----------------------------------------------------------- | ------------------------------------------------------------ |
| i, I                                                         | 进入输入模式(Insert mode)： i 为『从目前光标所在处输入』， I 为『在目前所在行的第一个非空格符处开始输入』。 (常用) |
| a, A                                                         | 进入输入模式(Insert mode)： a 为『从目前光标所在的下一个字符处开始输入』， A 为『从光标所在行的最后一个字符处开始输入』。(常用) |
| o, O                                                         | 进入输入模式(Insert mode)： 这是英文字母 o 的大小写。o 为在目前光标所在的下一行处输入新的一行； O 为在目前光标所在的上一行处输入新的一行！(常用) |
| r, R                                                         | 进入取代模式(Replace mode)： r 只会取代光标所在的那一个字符一次；R会一直取代光标所在的文字，直到按下 ESC 为止；(常用) |
| 上面这些按键中，在 vi 画面的左下角处会出现『--INSERT--』或『--REPLACE--』的字样。 由名称就知道该动作了吧！！特别注意的是，我们上面也提过了，你想要在档案里面输入字符时， 一定要在左下角处看到 INSERT 或 REPLACE 才能输入喔！ |                                                              |
| [Esc]                                                        | 退出编辑模式，回到一般模式中(常用)                           |

### 11.3.3. 第三部分：一般模式切换到指令行模式的可用的按钮说明

| 指令行的储存、离开等指令                                     |                                                              |
| :----------------------------------------------------------- | ------------------------------------------------------------ |
| :w                                                           | 将编辑的数据写入硬盘档案中(常用)                             |
| :w!                                                          | 若文件属性为『只读』时，强制写入该档案。不过，到底能不能写入， 还是跟你对该档案的档案权限有关啊！ |
| :q                                                           | 离开 vi (常用)                                               |
| :q!                                                          | 若曾修改过档案，又不想储存，使用 ! 为强制离开不储存档案。    |
| 注意一下啊，那个惊叹号 (!) 在 vi 当中，常常具有『强制』的意思～ |                                                              |
| :wq                                                          | 储存后离开，若为 :wq! 则为强制储存后离开 (常用)              |
| ZZ                                                           | 这是大写的 Z 喔！如果修改过，保存当前文件，然后退出！效果等同于(保存并退出) |
| ZQ                                                           | 不保存，强制退出。效果等同于 **:q!**。                       |
| :w [filename]                                                | 将编辑的数据储存成另一个档案（类似另存新档）                 |
| :r [filename]                                                | 在编辑的数据中，读入另一个档案的数据。亦即将 『filename』 这个档案内容加到游标所在行后面 |
| :n1,n2 w [filename]                                          | 将 n1 到 n2 的内容储存成 filename 这个档案。                 |
| :! command                                                   | 暂时离开 vi 到指令行模式下执行 command 的显示结果！例如 『:! ls /home』即可在 vi 当中察看 /home 底下以 ls 输出的档案信息！ |
| vim 环境的变更                                               |                                                              |
| :set nu                                                      | 显示行号，设定之后，会在每一行的前缀显示该行的行号           |
| :set nonu                                                    | 与 set nu 相反，为取消行号！                                 |

# 12. yum 命令

yum（ Yellow dog Updater, Modified）是一个在 Fedora 和 RedHat 以及 SUSE 中的 Shell 前端软件包管理器。

## 12.1. yum 语法

```bash
yum [options] [command] [package ...]
```

- **options：**可选，选项包括-h（帮助），-y（当安装过程提示选择全部为 "yes"），-q（不显示安装的过程）等等。
- **command：**要进行的操作。
- **package：**安装的包名。

------

## 12.2. yum常用命令

1. 列出所有可更新的软件清单命令：**yum check-update**
2. 更新所有软件命令：**yum update**
3. 仅安装指定的软件命令：**yum install <package_name>**
4. 仅更新指定的软件命令：**yum update <package_name>**
5. 列出所有可安裝的软件清单命令：**yum list**
6. 删除软件包命令：**yum remove <package_name>**
7. 查找软件包命令：**yum search <keyword>**
8. 清除缓存命令:

- **yum clean packages**: 清除缓存目录下的软件包
- **yum clean headers**: 清除缓存目录下的 headers
- **yum clean oldheaders**: 清除缓存目录下旧的 headers
- **yum clean, yum clean all (= yum clean packages; yum clean oldheaders)** :清除缓存目录下的软件包及旧的 headers

## 12.3. 国内 yum 源

### 12.3.1. 安装步骤

首先备份/etc/yum.repos.d/CentOS-Base.repo

```bash
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
```

下载对应版本 repo 文件, 放入 /etc/yum.repos.d/ (操作前请做好相应备份)

- [CentOS5](http://mirrors.163.com/.help/CentOS5-Base-163.repo) ：http://mirrors.163.com/.help/CentOS5-Base-163.repo
- [CentOS6](http://mirrors.163.com/.help/CentOS6-Base-163.repo) ：http://mirrors.163.com/.help/CentOS6-Base-163.repo
- [CentOS7](http://mirrors.163.com/.help/CentOS7-Base-163.repo) ：http://mirrors.163.com/.help/CentOS7-Base-163.repo
- [CentOS8](http://mirrors.163.com/.help/CentOS8-Base-163.repo) ：http://mirrors.163.com/.help/CentOS8-Base-163.repo

```bash
wget http://mirrors.163.com/.help/CentOS8-Base-163.repo
mv CentOS8-Base-163.repo CentOS-Base.repo
```

运行以下命令生成缓存

```bash
yum clean all
yum makecache
```

# 13. apt 命令

apt（Advanced Packaging Tool）是一个在 Debian 和 Ubuntu 中的 Shell 前端软件包管理器。

apt 命令执行需要超级管理员权限(root)。

## 13.1. apt 语法

```
  apt [options] [command] [package ...]
```

- **options：**可选，选项包括 -h（帮助），-y（当安装过程提示选择全部为"yes"），-q（不显示安装的过程）等等。
- **command：**要进行的操作。
- **package**：安装的包名。

------

## 13.2. apt 常用命令

- 列出所有可更新的软件清单命令：**sudo apt update**

- 升级软件包：**sudo apt upgrade**

  列出可更新的软件包及版本信息：**apt list --upgradeable**

  升级软件包，升级前先删除需要更新软件包：**sudo apt full-upgrade**

- 安装指定的软件命令：**sudo apt install <package_name>**

  安装多个软件包：**sudo apt install <package_1> <package_2> <package_3>**

- 更新指定的软件命令：**sudo apt update <package_name>**

- 显示软件包具体信息,例如：版本号，安装大小，依赖关系等等：**sudo apt show <package_name>**

- 删除软件包命令：**sudo apt remove <package_name>**

- 清理不再使用的依赖和库文件: **sudo apt autoremove**

- 移除软件包及配置文件: **sudo apt purge <package_name>**

- 查找软件包命令： **sudo apt search <keyword>**

- 列出所有已安装的包：**apt list --installe**d

- 列出所有已安装的包的版本信息：**apt list --all-versions**



