<h2>目录</h2>

<details open>
  <summary><a href="#1-kubernetes安装">1. Kubernetes安装</a></summary>
  <ul>
  <details open>
    <summary><a href="#11-k8s集群安装脚本">1.1. k8s集群安装脚本</a>  </summary>
    <ul>
      <a href="#111-主节点和从节点运行的脚本（docker和k8s的安装）">1.1.1. 主节点和从节点运行的脚本（Docker和k8s的安装）</a><br>
      <a href="#112-只在主节点运行的脚本（初始化k8s，安装）">1.1.2. 只在主节点运行的脚本（初始化k8s，安装）</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#12-自定义部署应用">1.2. 自定义部署应用</a>  </summary>
    <ul>
      <a href="#121-前置条件">1.2.1. 前置条件</a><br>
      <a href="#122-自定义部署">1.2.2. 自定义部署</a><br>
      <a href="#123-mysql8集群（需要一点点地修改，并有待验证）">1.2.3. MySQL8集群（需要一点点地修改，并有待验证）</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#13-centos下安装docker">1.3. centos下安装docker</a>  </summary>
    <ul>
      <a href="#131-移除以前docker相关包">1.3.1. 移除以前docker相关包</a><br>
      <a href="#132-配置yum源">1.3.2. 配置yum源</a><br>
      <a href="#133-安装docker">1.3.3. 安装docker</a><br>
      <a href="#134-启动并验证docker版本">1.3.4. 启动并验证docker版本</a><br>
      <a href="#135-配置自己的阿里云镜像加速">1.3.5. 配置自己的阿里云镜像加速</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#14-kubeadm引导安装kubernetes">1.4. kubeadm引导安装Kubernetes</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#141-安装kubeadm">1.4.1. 安装kubeadm</a>    </summary>
      <ul>
        <a href="#1411-基础环境">1.4.1.1. 基础环境</a><br>
        <a href="#1412-安装kubelet、kubeadm、kubectl">1.4.1.2. 安装kubelet、kubeadm、kubectl</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#142-使用kubeadm引导集群">1.4.2. 使用kubeadm引导集群</a>    </summary>
      <ul>
        <a href="#1421-下载各个机器需要的镜像">1.4.2.1. 下载各个机器需要的镜像</a><br>
        <a href="#1422-初始化主节点">1.4.2.2. 初始化主节点</a><br>
        <a href="#1423-安装calico网络组件">1.4.2.3. 安装calico网络组件</a><br>
      <details open>
        <summary><a href="#1424-部署dashboard">1.4.2.4. 部署dashboard</a>      </summary>
        <ul>
          <a href="#14241-部署">1.4.2.4.1. 部署</a><br>
          <a href="#14242-设置访问端口">1.4.2.4.2. 设置访问端口</a><br>
          <a href="#14243-创建访问账号">1.4.2.4.3. 创建访问账号</a><br>
          <a href="#14244-令牌访问">1.4.2.4.4. 令牌访问</a><br>
          <a href="#14245-界面">1.4.2.4.5. 界面</a><br>
        </ul>
      </details>
      </ul>
    </details>
    </ul>
  </details>
  <details open>
    <summary><a href="#15-mac单击上安装kubernetes（k8s）">1.5. Mac单击上安装Kubernetes（k8s）</a>  </summary>
    <ul>
      <a href="#151-docker-for-mac">1.5.1. Docker for Mac</a><br>
      <a href="#152-拉取k8s镜像">1.5.2. 拉取k8s镜像</a><br>
    <details open>
      <summary><a href="#153-安装-kubernetes-dashboard">1.5.3. 安装 Kubernetes Dashboard</a>    </summary>
      <ul>
        <a href="#1531-设置访问端口">1.5.3.1. 设置访问端口</a><br>
        <a href="#1532-创建访问账号">1.5.3.2. 创建访问账号</a><br>
        <a href="#1533-令牌访问">1.5.3.3. 令牌访问</a><br>
        <a href="#1534-界面">1.5.3.4. 界面</a><br>
      </ul>
    </details>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#2-mysql主从集群（mac单机版）">2. MySQL主从集群（Mac单机版）</a></summary>
  <ul>
  <details open>
    <summary><a href="#21-准备mysql8主从集群的镜像">2.1. 准备MySQL8主从集群的镜像</a>  </summary>
    <ul>
      <a href="#211-准备master镜像">2.1.1. 准备Master镜像</a><br>
      <a href="#212-准备slave镜像">2.1.2. 准备Slave镜像</a><br>
    </ul>
  </details>
    <a href="#22-开始使用修改好的dockerfile创建mysqlde-master和slave镜像">2.2. 开始使用修改好的dockerfile创建mysqlde master和slave镜像</a><br>
  <details open>
    <summary><a href="#23-创建pv和pvc，用于mysql主从存储持久化数据">2.3. 创建pv和pvc，用于mysql主从存储持久化数据</a>  </summary>
    <ul>
      <a href="#231-master节点的pv、pvc">2.3.1. Master节点的PV、PVC</a><br>
      <a href="#232-slave节点的pv、pvc">2.3.2. Slave节点的PV、PVC</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#24-master节点的replicationcontroller和service">2.4. Master节点的ReplicationController和Service</a>  </summary>
    <ul>
      <a href="#241-先要开启mac上的nfs服务blocked">2.4.1. 先要开启Mac上的NFS服务(Blocked)</a><br>
      <a href="#242-replicationcontroller">2.4.2. ReplicationController</a><br>
      <a href="#243-service">2.4.3. Service</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#3-mysql主从集群（centos8，一主双从三节点）">3. MySQL主从集群（CentOS8，一主双从三节点）</a></summary>
  <ul>
  <details open>
    <summary><a href="#31-准备mysql8主从集群的镜像">3.1. 准备MySQL8主从集群的镜像</a>  </summary>
    <ul>
      <a href="#311-准备master镜像">3.1.1. 准备Master镜像</a><br>
      <a href="#312-准备slave镜像">3.1.2. 准备Slave镜像</a><br>
    </ul>
  </details>
    <a href="#32-开始使用修改好的dockerfile创建mysqlde-master和slave镜像">3.2. 开始使用修改好的dockerfile创建mysqlde master和slave镜像</a><br>
  <details open>
    <summary><a href="#33-创建pv和pvc，用于mysql主从存储持久化数据">3.3. 创建pv和pvc，用于mysql主从存储持久化数据</a>  </summary>
    <ul>
      <a href="#331-master-mysql的pv、pvc">3.3.1. Master MySQL的PV、PVC</a><br>
      <a href="#332-slave-mysql的pv、pvc">3.3.2. Slave MySQL的PV、PVC</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#34-master节点的replicationcontroller和service">3.4. Master节点的ReplicationController和Service</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#341-先要开启centos上的nfs服务">3.4.1. 先要开启CentOS上的NFS服务</a>    </summary>
      <ul>
        <a href="#3411-安装nfs文件工具包">3.4.1.1. 安装NFS文件工具包</a><br>
        <a href="#3412-启动nfs服务">3.4.1.2. 启动NFS服务</a><br>
        <a href="#3413-检测nfs服务是否可用">3.4.1.3. 检测NFS服务是否可用</a><br>
      </ul>
    </details>
      <a href="#342-replicationcontroller">3.4.2. ReplicationController</a><br>
      <a href="#343-service">3.4.3. Service</a><br>
      <a href="#344-验证master连接">3.4.4. 验证Master连接</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#35-slave节点的replicationcontroller和service">3.5. Slave节点的ReplicationController和Service</a>  </summary>
    <ul>
      <a href="#351-先要开启centos上的nfs服务">3.5.1. 先要开启CentOS上的NFS服务</a><br>
      <a href="#352-replicationcontroller">3.5.2. ReplicationController</a><br>
      <a href="#353-service">3.5.3. Service</a><br>
      <a href="#354-验证slave连接">3.5.4. 验证Slave连接</a><br>
      <a href="#355-需要修正mysql构建镜像的配置">3.5.5. 需要修正MySQL构建镜像的配置</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#4-kubernetes简介">4. Kubernetes简介</a></summary>
  <ul>
  <details open>
    <summary><a href="#41-k8s概念和特性">4.1. K8S概念和特性</a>  </summary>
    <ul>
      <a href="#411-部署发展历程">4.1.1. 部署发展历程</a><br>
      <a href="#412-k8s概述">4.1.2. K8S概述</a><br>
      <a href="#413-k8s概述">4.1.3. K8S概述</a><br>
    <details open>
      <summary><a href="#414-k8s功能">4.1.4. K8S功能</a>    </summary>
      <ul>
        <a href="#4141-自动装箱">4.1.4.1. 自动装箱</a><br>
        <a href="#4142-自我修复自愈能力">4.1.4.2. 自我修复(自愈能力)</a><br>
        <a href="#4143-水平扩展">4.1.4.3. 水平扩展</a><br>
        <a href="#4144-服务发现">4.1.4.4. 服务发现</a><br>
        <a href="#4145-滚动更新">4.1.4.5. 滚动更新</a><br>
        <a href="#4146-版本回退">4.1.4.6. 版本回退</a><br>
        <a href="#4147-密钥和配置管理">4.1.4.7. 密钥和配置管理</a><br>
        <a href="#4148-存储编排">4.1.4.8. 存储编排</a><br>
        <a href="#4149-批处理">4.1.4.9. 批处理</a><br>
      </ul>
    </details>
    </ul>
  </details>
  <details open>
    <summary><a href="#42-k8s架构组件">4.2. K8S架构组件</a>  </summary>
    <ul>
      <a href="#421-完整架构图">4.2.1. 完整架构图</a><br>
      <a href="#422-架构细节">4.2.2. 架构细节</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#43-k8s核心概念">4.3. K8S核心概念</a>  </summary>
    <ul>
      <a href="#431-pod">4.3.1. Pod</a><br>
      <a href="#432-volume">4.3.2. Volume</a><br>
      <a href="#433-controller">4.3.3. Controller</a><br>
      <a href="#434-deployment">4.3.4. Deployment</a><br>
      <a href="#435-service">4.3.5. Service</a><br>
      <a href="#436-label">4.3.6. Label</a><br>
      <a href="#437-namespace">4.3.7. Namespace</a><br>
      <a href="#438-api">4.3.8. API</a><br>
    </ul>
  </details>
    <a href="#44-完整流程">4.4. 完整流程</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#5-kubernetes集群管理工具kubectl">5. Kubernetes集群管理工具kubectl</a></summary>
  <ul>
    <a href="#51-概述">5.1. 概述</a><br>
    <a href="#52-命令格式">5.2. 命令格式</a><br>
  <details open>
    <summary><a href="#53-常见命令">5.3. 常见命令</a>  </summary>
    <ul>
      <a href="#531-kubectl-help-获取更多信息">5.3.1. kubectl help 获取更多信息</a><br>
      <a href="#532-基础命令">5.3.2. 基础命令</a><br>
      <a href="#533-部署命令">5.3.3. 部署命令</a><br>
      <a href="#534-集群管理命令">5.3.4. 集群管理命令</a><br>
      <a href="#535-故障和调试命令">5.3.5. 故障和调试命令</a><br>
      <a href="#536-其它命令">5.3.6. 其它命令</a><br>
      <a href="#537-目前使用的命令">5.3.7. 目前使用的命令</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#6-kubernetes集群yaml文件详解">6. Kubernetes集群YAML文件详解</a></summary>
  <ul>
    <a href="#61-概述">6.1. 概述</a><br>
  <details open>
    <summary><a href="#62-yaml文件组成部分">6.2. YAML文件组成部分</a>  </summary>
    <ul>
      <a href="#621-控制器的定义">6.2.1. 控制器的定义</a><br>
      <a href="#622-被控制的对象">6.2.2. 被控制的对象</a><br>
      <a href="#623-属性说明">6.2.3. 属性说明</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#63-如何快速编写yaml文件">6.3. 如何快速编写YAML文件</a>  </summary>
    <ul>
      <a href="#631-使用kubectl-create命令">6.3.1. 使用kubectl create命令</a><br>
      <a href="#632-使用kubectl-get命令导出yaml文件">6.3.2. 使用kubectl get命令导出yaml文件</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#7-kubernetes核心技术pod">7. Kubernetes核心技术Pod</a></summary>
  <ul>
  <details open>
    <summary><a href="#71-pod概述">7.1. Pod概述</a>  </summary>
    <ul>
      <a href="#711-pod基本概念">7.1.1. Pod基本概念</a><br>
      <a href="#712-pod存在的意义">7.1.2. Pod存在的意义</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#72-pod实现机制">7.2. Pod实现机制</a>  </summary>
    <ul>
      <a href="#721-共享网络">7.2.1. 共享网络</a><br>
      <a href="#722-共享存储">7.2.2. 共享存储</a><br>
    </ul>
  </details>
    <a href="#73-pod镜像拉取策略">7.3. Pod镜像拉取策略</a><br>
  <details open>
    <summary><a href="#74-pod资源限制">7.4. Pod资源限制</a>  </summary>
    <ul>
      <a href="#741-示例">7.4.1. 示例</a><br>
    </ul>
  </details>
    <a href="#75-pod重启机制">7.5. Pod重启机制</a><br>
    <a href="#76-pod健康检查">7.6. Pod健康检查</a><br>
  <details open>
    <summary><a href="#77-pod调度策略">7.7. Pod调度策略</a>  </summary>
    <ul>
      <a href="#771-创建pod流程">7.7.1. 创建Pod流程</a><br>
    <details open>
      <summary><a href="#772-影响pod调度的属性">7.7.2. 影响Pod调度的属性</a>    </summary>
      <ul>
        <a href="#7721-根据request找到足够node节点进行调度">7.7.2.1. 根据request找到足够node节点进行调度</a><br>
        <a href="#7722-节点选择器标签影响pod调度">7.7.2.2. 节点选择器标签影响Pod调度</a><br>
        <a href="#7723-节点亲和性">7.7.2.3. 节点亲和性</a><br>
      </ul>
    </details>
    </ul>
  </details>
  <details open>
    <summary><a href="#78-污点和污点容忍">7.8. 污点和污点容忍</a>  </summary>
    <ul>
      <a href="#781-概述">7.8.1. 概述</a><br>
      <a href="#782-场景">7.8.2. 场景</a><br>
      <a href="#783-查看污点情况">7.8.3. 查看污点情况</a><br>
      <a href="#784-未节点添加污点">7.8.4. 未节点添加污点</a><br>
      <a href="#785-删除污点">7.8.5. 删除污点</a><br>
      <a href="#786-演示">7.8.6. 演示</a><br>
      <a href="#787-污点容忍">7.8.7. 污点容忍</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#8-kubernetes核心技术-controller">8. Kubernetes核心技术-Controller</a></summary>
  <ul>
    <a href="#81-什么是controller">8.1. 什么是Controller</a><br>
    <a href="#82-pod和controller的关系">8.2. Pod和Controller的关系</a><br>
    <a href="#83-deployment控制器应用">8.3. Deployment控制器应用</a><br>
  <details open>
    <summary><a href="#84-deployment部署应用">8.4. Deployment部署应用</a>  </summary>
    <ul>
      <a href="#841-使用yaml创建pod">8.4.1. 使用YAML创建Pod</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#85-升级回滚和弹性伸缩">8.5. 升级回滚和弹性伸缩</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#851-应用升级和回滚">8.5.1. 应用升级和回滚</a>    </summary>
      <ul>
        <a href="#8511-查看升级状态">8.5.1.1. 查看升级状态</a><br>
        <a href="#8512-查看历史版本">8.5.1.2. 查看历史版本</a><br>
        <a href="#8513-应用回滚">8.5.1.3. 应用回滚</a><br>
      </ul>
    </details>
      <a href="#852-弹性伸缩">8.5.2. 弹性伸缩</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#9-kubernetes核心技术service">9. Kubernetes核心技术Service</a></summary>
  <ul>
    <a href="#91-前言">9.1. 前言</a><br>
  <details open>
    <summary><a href="#92-service存在的意义">9.2. Service存在的意义</a>  </summary>
    <ul>
      <a href="#921-防止pod失联【服务发现】">9.2.1. 防止Pod失联【服务发现】</a><br>
      <a href="#922-定义pod访问策略【负载均衡】">9.2.2. 定义Pod访问策略【负载均衡】</a><br>
    </ul>
  </details>
    <a href="#93-pod和service的关系">9.3. Pod和Service的关系</a><br>
  <details open>
    <summary><a href="#94-service常用类型">9.4. Service常用类型</a>  </summary>
    <ul>
      <a href="#941-举例">9.4.1. 举例</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#10-kubernetes控制器controller详解">10. Kubernetes控制器Controller详解</a></summary>
  <ul>
  <details open>
    <summary><a href="#101-statefulset">10.1. Statefulset</a>  </summary>
    <ul>
      <a href="#1011-无状态应用">10.1.1. 无状态应用</a><br>
      <a href="#1012-有状态应用">10.1.2. 有状态应用</a><br>
      <a href="#1013-部署有状态应用">10.1.3. 部署有状态应用</a><br>
    </ul>
  </details>
    <a href="#102-daemonset">10.2. DaemonSet</a><br>
  <details open>
    <summary><a href="#103-job和cronjob">10.3. Job和CronJob</a>  </summary>
    <ul>
      <a href="#1031-job">10.3.1. Job</a><br>
      <a href="#1032-cronjob">10.3.2. CronJob</a><br>
    </ul>
  </details>
    <a href="#104-删除svc-和-statefulset">10.4. 删除svc 和 statefulset</a><br>
  <details open>
    <summary><a href="#105-replication-controller">10.5. Replication Controller</a>  </summary>
    <ul>
      <a href="#1051-replica-set">10.5.1. Replica Set</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#11-kubernetes配置管理">11. Kubernetes配置管理</a></summary>
  <ul>
  <details open>
    <summary><a href="#111-secret">11.1. Secret</a>  </summary>
    <ul>
      <a href="#1111-变量形式挂载到pod">11.1.1. 变量形式挂载到Pod</a><br>
      <a href="#1112-数据卷形式挂载">11.1.2. 数据卷形式挂载</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#112-configmap">11.2. ConfigMap</a>  </summary>
    <ul>
      <a href="#1121-创建配置文件">11.2.1. 创建配置文件</a><br>
      <a href="#1122-创建configmap">11.2.2. 创建ConfigMap</a><br>
      <a href="#1123-volume数据卷形式挂载">11.2.3. Volume数据卷形式挂载</a><br>
      <a href="#1124-以变量的形式挂载pod">11.2.4. 以变量的形式挂载Pod</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#12-kubernetes集群安全机制">12. Kubernetes集群安全机制</a></summary>
  <ul>
  <details open>
    <summary><a href="#121-概述">12.1. 概述</a>  </summary>
    <ul>
      <a href="#1211-认证">12.1.1. 认证</a><br>
      <a href="#1212-鉴权">12.1.2. 鉴权</a><br>
      <a href="#1213-准入控制">12.1.3. 准入控制</a><br>
    </ul>
  </details>
    <a href="#122-rbac介绍">12.2. RBAC介绍</a><br>
  <details open>
    <summary><a href="#123-rbac实现鉴权">12.3. RBAC实现鉴权</a>  </summary>
    <ul>
      <a href="#1231-创建命名空间">12.3.1. 创建命名空间</a><br>
      <a href="#1232-命名空间创建pod">12.3.2. 命名空间创建Pod</a><br>
      <a href="#1233-创建角色">12.3.3. 创建角色</a><br>
      <a href="#1234-创建角色绑定">12.3.4. 创建角色绑定</a><br>
      <a href="#1235-使用证书识别身份">12.3.5. 使用证书识别身份???</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#13-kubernetes核心技术ingress">13. Kubernetes核心技术Ingress</a></summary>
  <ul>
    <a href="#131-前言">13.1. 前言</a><br>
    <a href="#132-ingress和pod关系">13.2. Ingress和Pod关系</a><br>
    <a href="#133-ingress工作流程">13.3. Ingress工作流程</a><br>
  <details open>
    <summary><a href="#134-使用ingress">13.4. 使用Ingress</a>  </summary>
    <ul>
      <a href="#1341-创建nginx-pod">13.4.1. 创建Nginx Pod</a><br>
      <a href="#1342-部署-ingress-controller">13.4.2. 部署 ingress controller</a><br>
      <a href="#1343-创建ingress规则文件">13.4.3. 创建ingress规则文件</a><br>
      <a href="#1344-添加域名访问规则">13.4.4. 添加域名访问规则</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#14-kubernetes核心技术helm">14. Kubernetes核心技术Helm</a></summary>
  <ul>
    <a href="#141-为什么引入helm">14.1. 为什么引入Helm</a><br>
    <a href="#142-helm介绍">14.2. Helm介绍</a><br>
    <a href="#143-helm组件及架构">14.3. Helm组件及架构</a><br>
    <a href="#144-helm-v3变化">14.4. Helm v3变化</a><br>
    <a href="#145-helm安装配置">14.5. helm安装配置</a><br>
    <a href="#146-helm仓库">14.6. helm仓库</a><br>
    <a href="#147-helm基本命令">14.7. helm基本命令</a><br>
  <details open>
    <summary><a href="#148-使用helm快速部署应用">14.8. 使用helm快速部署应用</a>  </summary>
    <ul>
      <a href="#1481-使用命令搜索应用">14.8.1. 使用命令搜索应用</a><br>
      <a href="#1482-根据搜索内容选择安装">14.8.2. 根据搜索内容选择安装</a><br>
    <details open>
      <summary><a href="#1483-如果自己创建chart">14.8.3. 如果自己创建Chart</a>    </summary>
      <ul>
        <a href="#14831-目录格式">14.8.3.1. 目录格式</a><br>
      </ul>
    </details>
      <a href="#1484-在templates文件夹创建两个文件">14.8.4. 在templates文件夹创建两个文件</a><br>
      <a href="#1485-安装mychart">14.8.5. 安装mychart</a><br>
      <a href="#1486-应用升级">14.8.6. 应用升级</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#149-chart模板使用">14.9. chart模板使用</a>  </summary>
    <ul>
      <a href="#1491-定义变量和值">14.9.1. 定义变量和值</a><br>
      <a href="#1492-获取变量和值">14.9.2. 获取变量和值</a><br>
      <a href="#1493-安装应用">14.9.3. 安装应用</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#15-kubernetes持久化存储">15. Kubernetes持久化存储</a></summary>
  <ul>
    <a href="#151-前言">15.1. 前言</a><br>
  <details open>
    <summary><a href="#152-nfs网络存储">15.2. nfs网络存储</a>  </summary>
    <ul>
      <a href="#1521-持久化服务器上操作">15.2.1. 持久化服务器上操作</a><br>
      <a href="#1522-node节点上操作">15.2.2. Node节点上操作</a><br>
      <a href="#1523-启动nfs服务端">15.2.3. 启动nfs服务端</a><br>
      <a href="#1524-k8s集群部署应用">15.2.4. K8s集群部署应用</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#153-pv和pvc">15.3. PV和PVC</a>  </summary>
    <ul>
      <a href="#1531-实现流程">15.3.1. 实现流程</a><br>
      <a href="#1532-举例">15.3.2. 举例</a><br>
    </ul>
  </details>
  </ul>
</details>




<h1>Kubernetes-k8s</h1>

# 1. Kubernetes安装

<h2>前置条件！！！</h2>

**一般开发或者测试都需要3三台可连接的公有云、私有云或者虚拟机！！！**

备份镜像位置:[备份镜像文件夹](/Volumes/Work HD/utils/VM and OS/)下的**CentOS-Stream 8 64-bit-backup-2021-11-21-NAT-root/yanglin-duplicate-and-change-stastic-ipaddr-based-on-vmnet8-network-info.vmwarevm**，可以复制该镜像，让后进行网关配置

并复制上面的虚拟机，**复制的新虚拟机只需要修改里面的静态IP地址就好**，并且主机名，UUID不能相同



会自动生成下面的网卡ifcfg-ens*，但是需要用下面的命令把这个默认网卡修改成静态IP，并保存。

vim /etc/sysconfig/network-scripts/ifcfg-ens*

修改和新增配置：

```
// 修改
BOOTPROTO=static  //静态ip
ONBOOT=yes #将no改为yes
UUID=06cc84d6-a5f9-4055-88a2-d3b854d0ce5f // uuidgen ens37, 随机生成UUID, ens37是网卡名字
// 新增
// 设置网关，因为选择的是NAT模A，所以选择的是vmnet8虚拟网卡，Mac上执行下面的命令即可查看到对应vmnet8虚拟网卡的网关IP和子网掩码
// cat /Library/Preferences/VMware\ Fusion/vmnet8/nat.conf | grep gateway -A 2
NETMASK=255.255.255.0  #设置子网掩码
GATEWAY=172.16.26.2   #设置网关，需要和Mac、Windows电脑的网关一致！！！就是上面vmnet8虚拟网卡的网关IP
IPADDR=172.16.26.3 #设置centos IP静态地址地址，这时候静态IP自定义范围是172.16.26.3~172.16.26.255
//DNS1=114.114.114.114 #设置dns，也可以不设置
```



备份镜像在分配的静态IP地址分别如下：

```
172.16.26.3 // master
172.16.26.4 // node1
172.16.26.5 // node2
```

**注意：由于当时使用3台虚拟机模拟3台服务器的，所以在在复制虚拟机的时候可能忽略了解析DNS的配置文件/etc/resolv.conf，需要配置网关以正确解析DNS，如果不配置可能导致k8s部署的pod不能够解析DNS以至于不能访问外网**

```bash
[root@k8s-node1 ~]# cat /etc/resolv.conf
# Generated by NetworkManager
nameserver 172.16.26.2
```

## 1.1. k8s集群安装脚本

### 1.1.1. 主节点和从节点运行的脚本（Docker和k8s的安装）

```bash
# 主节点和从节点都要运行
# 安装Docker和k8s
# 新建文件并粘贴下面的内容
touch install-docker-k8s.sh & chmod 777 install-docker-k8s.sh & vim install-docker-k8s.sh
# 执行安装，传入节点的hostname，不传默认值为k8s-master，注意除了master节点，其他节点一定要传！！！！
./install-docker-k8s.sh <hostname>

```



```bash
#!/bin/bash


hn=${1-k8s-master}
hostnamectl set-hostname $hn
# 定义IP
master_ip=172.16.26.3
node1_ip=172.16.26.4
node2_ip=172.16.26.5

if test -z "$(grep cluster-endpoint /etc/hosts)";
then
	echo ${master_ip}"  cluster-endpoint" >> /etc/hosts
fi
if test -z "$(grep k8s-node1 /etc/hosts)";
then
	echo ${node1_ip}"  k8s-node1" >> /etc/hosts
fi
if test -z "$(grep k8s-node2 /etc/hosts)";
then
	echo ${node2_ip}"  k8s-node2" >> /etc/hosts
fi

echo "==========================Start Install Docker=========================="


sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine

sudo yum install -y yum-utils
sudo yum-config-manager \
--add-repo \
http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

sudo yum install -y docker-ce docker-ce-cli containerd.io --allowerasing

systemctl enable docker --now
docker -v

sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://w4plc3h7.mirror.aliyuncs.com"],
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

sudo systemctl daemon-reload
sudo systemctl restart docker
sudo docker info


echo "==========================Finish Install Docker=========================="


echo "==========================Start Install Kubernetes(k8s)=========================="


# 将 SELinux 设置为 permissive 模式（相当于将其禁用），这是Linux的一些安全设置，需要将其禁用，方便后面的开发
# 临时禁用
sudo setenforce 0
# 永久禁用
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
# 禁用防火墙，非常重要
systemctl disable firewalld --now

# 关闭swap交换区
# 临时关闭
swapoff -a
# 永久关闭
sed -ri 's/.*swap.*/#&/' /etc/fstab


#允许 iptables 检查桥接流量，IPv6的流量桥接到IPv4上（k8s官方要求这样做的）
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
br_netfilter
EOF

# 使上面的配置生效
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system

# 配置k8s去哪里(阿里云镜像)下载
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=http://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=http://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg
   http://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF

# 安装最新的k8s，kube-*组件的版本要和后面安装的kube-*镜像版本要一致。如果想要知道版本号，运行一下没有版本号的命令再验证版本即可
sudo yum install -y kubelet-1.22.4 kubeadm-1.22.4 kubectl-1.22.4 --disableexcludes=kubernetes
#sudo yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes

# 启动k8s
sudo systemctl enable --now kubelet


# 验证k8s组件版本
kubeadm version
kubectl version --client
kubelet --version

# 在节点上新增镜像下载脚本文件，注意etcd的版本号，可在https://etcd.io/docs/看到最新的版本号，下载容器的版本号如下
sudo tee ./images.sh <<-'EOF'
#!/bin/bash
images=(
kube-apiserver:v1.22.4
kube-proxy:v1.22.4
kube-controller-manager:v1.22.4
kube-scheduler:v1.22.4
coredns:latest
etcd:3.5.0-0
pause:latest
)
for imageName in ${images[@]} ; do
docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/$imageName
done
EOF

# 赋予文件执行权限并执行脚本文件安装镜像
chmod +x ./images.sh && ./images.sh

# 保险起见，可以用下面的命令查看镜像是否安装好
docker images

echo "==========================Finish Install Kubernetes(k8s)=========================="

```

### 1.1.2. 只在主节点运行的脚本（初始化k8s，安装）

```yaml
# 只在主节点运行
# 初始化k8s，以及安装Kubernetes Dashboard
# 新建文件并粘贴下面的内容
touch init-k8s-install-kuboard.sh & chmod 777 init-k8s-install-kuboard.sh & vim init-k8s-install-kuboard.sh
# 执行安装
./init-k8s-install-kuboard.sh
# 可以利用下面命令查看初始化情况
kubectl get pods -A -owide
```



```bash
#!/bin/bash

#apiserver_advertise_address=`ifconfig -a|grep inet|grep -v 127.0.0.1|grep -v inet6|awk '{print $2}'|tr -d "addr:"|sed -n '2p'`
apiserver_advertise_address=172.16.26.3

echo "==========================Start Init Master node for Kubernetes(k8s) -- about 4 mins=========================="

# if test -z "$(grep cluster-endpoint /etc/hosts)";
# then
# 	echo ${apiserver_advertise_address}"  cluster-endpoint" >> /etc/hosts
# fi
# 初始化前先强制重置之前可能存在的初始化
kubeadm reset --force

# 正式初始化
kubeadm init \
--apiserver-advertise-address=${apiserver_advertise_address} \
--control-plane-endpoint=cluster-endpoint \
--image-repository registry.cn-hangzhou.aliyuncs.com/google_containers \
--kubernetes-version v1.22.4 \
--service-cidr=10.96.0.0/16 \
--pod-network-cidr=192.168.0.0/16

# 初始化主节点后，需要运行下面的命令。其中/bin/cp是原生的cp命令，是为了去掉默认的交互-i，而-force的作用是复制时候会强制覆盖文件
mkdir -p $HOME/.kube
sudo rm -rf $HOME/.kube/config
sudo cp -rf -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
# 如果你是root用户也可以直接运行下面的命令达到和上面命令一样的效果
export KUBECONFIG=/etc/kubernetes/admin.conf



# 在Master节点执行下面的命令，下载calico组件配置文件，-s是静音模式，不输出任何信息
curl https://docs.projectcalico.org/manifests/calico.yaml -O -s
# 在Master节点执行下面命令应用calico
kubectl apply -f calico.yaml

echo "==========================Finish Init Master node for Kubernetes(k8s)=========================="


echo "==========================Start Install Kubernetes Dashboard=========================="

# 安装之前先要卸载存在的Kubernetes Dashboard应用
sudo kubectl -n kubernetes-dashboard delete $(sudo kubectl -n kubernetes-dashboard get pod -oname | grep dashboard)

# 在Master节点上运行下面的命令创建部署dashboard，我在Gitlab上fork了kubernetes/dashboard在Github上的repository，并修改了kubernetes dashboard对外的固定端口号30001（nodePort: 30001），端口类型为NodePort（type: NodePort），并覆盖文件kubernetes-dashboard-recommended.yaml。kubernetes dashboard的版本后面可以手动重新拉取并更新
# ========================================================================================================
echo '# Copyright 2017 The Kubernetes Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

apiVersion: v1
kind: Namespace
metadata:
  name: kubernetes-dashboard

---

apiVersion: v1
kind: ServiceAccount
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard

---

kind: Service
apiVersion: v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
spec:
  ports:
    - port: 443
      targetPort: 8443
      nodePort: 30001
  selector:
    k8s-app: kubernetes-dashboard
  type: NodePort
---

apiVersion: v1
kind: Secret
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard-certs
  namespace: kubernetes-dashboard
type: Opaque

---

apiVersion: v1
kind: Secret
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard-csrf
  namespace: kubernetes-dashboard
type: Opaque
data:
  csrf: ""

---

apiVersion: v1
kind: Secret
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard-key-holder
  namespace: kubernetes-dashboard
type: Opaque

---

kind: ConfigMap
apiVersion: v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard-settings
  namespace: kubernetes-dashboard

---

kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
rules:
  # Allow Dashboard to get, update and delete Dashboard exclusive secrets.
  - apiGroups: [""]
    resources: ["secrets"]
    resourceNames: ["kubernetes-dashboard-key-holder", "kubernetes-dashboard-certs", "kubernetes-dashboard-csrf"]
    verbs: ["get", "update", "delete"]
    # Allow Dashboard to get and update 'kubernetes-dashboard-settings' config map.
  - apiGroups: [""]
    resources: ["configmaps"]
    resourceNames: ["kubernetes-dashboard-settings"]
    verbs: ["get", "update"]
    # Allow Dashboard to get metrics.
  - apiGroups: [""]
    resources: ["services"]
    resourceNames: ["heapster", "dashboard-metrics-scraper"]
    verbs: ["proxy"]
  - apiGroups: [""]
    resources: ["services/proxy"]
    resourceNames: ["heapster", "http:heapster:", "https:heapster:", "dashboard-metrics-scraper", "http:dashboard-metrics-scraper"]
    verbs: ["get"]

---

kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
rules:
  # Allow Metrics Scraper to get metrics from the Metrics server
  - apiGroups: ["metrics.k8s.io"]
    resources: ["pods", "nodes"]
    verbs: ["get", "list", "watch"]

---

apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: kubernetes-dashboard
subjects:
  - kind: ServiceAccount
    name: kubernetes-dashboard
    namespace: kubernetes-dashboard

---

apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: kubernetes-dashboard
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: kubernetes-dashboard
subjects:
  - kind: ServiceAccount
    name: kubernetes-dashboard
    namespace: kubernetes-dashboard

---

kind: Deployment
apiVersion: apps/v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
spec:
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      k8s-app: kubernetes-dashboard
  template:
    metadata:
      labels:
        k8s-app: kubernetes-dashboard
    spec:
      containers:
        - name: kubernetes-dashboard
          image: kubernetesui/dashboard:v2.4.0
          imagePullPolicy: Always
          ports:
            - containerPort: 8443
              protocol: TCP
          args:
            - --auto-generate-certificates
            - --namespace=kubernetes-dashboard
            # Uncomment the following line to manually specify Kubernetes API server Host
            # If not specified, Dashboard will attempt to auto discover the API server and connect
            # to it. Uncomment only if the default does not work.
            # - --apiserver-host=http://my-address:port
          volumeMounts:
            - name: kubernetes-dashboard-certs
              mountPath: /certs
              # Create on-disk volume to store exec logs
            - mountPath: /tmp
              name: tmp-volume
          livenessProbe:
            httpGet:
              scheme: HTTPS
              path: /
              port: 8443
            initialDelaySeconds: 30
            timeoutSeconds: 30
          securityContext:
            allowPrivilegeEscalation: false
            readOnlyRootFilesystem: true
            runAsUser: 1001
            runAsGroup: 2001
      volumes:
        - name: kubernetes-dashboard-certs
          secret:
            secretName: kubernetes-dashboard-certs
        - name: tmp-volume
          emptyDir: {}
      serviceAccountName: kubernetes-dashboard
      nodeSelector:
        "kubernetes.io/os": linux
      # Comment the following tolerations if Dashboard must not be deployed on master
      tolerations:
        - key: node-role.kubernetes.io/master
          effect: NoSchedule

---

kind: Service
apiVersion: v1
metadata:
  labels:
    k8s-app: dashboard-metrics-scraper
  name: dashboard-metrics-scraper
  namespace: kubernetes-dashboard
spec:
  ports:
    - port: 8000
      targetPort: 8000
  selector:
    k8s-app: dashboard-metrics-scraper

---

kind: Deployment
apiVersion: apps/v1
metadata:
  labels:
    k8s-app: dashboard-metrics-scraper
  name: dashboard-metrics-scraper
  namespace: kubernetes-dashboard
spec:
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      k8s-app: dashboard-metrics-scraper
  template:
    metadata:
      labels:
        k8s-app: dashboard-metrics-scraper
    spec:
      securityContext:
        seccompProfile:
          type: RuntimeDefault
      containers:
        - name: dashboard-metrics-scraper
          image: kubernetesui/metrics-scraper:v1.0.7
          ports:
            - containerPort: 8000
              protocol: TCP
          livenessProbe:
            httpGet:
              scheme: HTTP
              path: /
              port: 8000
            initialDelaySeconds: 30
            timeoutSeconds: 30
          volumeMounts:
          - mountPath: /tmp
            name: tmp-volume
          securityContext:
            allowPrivilegeEscalation: false
            readOnlyRootFilesystem: true
            runAsUser: 1001
            runAsGroup: 2001
      serviceAccountName: kubernetes-dashboard
      nodeSelector:
        "kubernetes.io/os": linux
      # Comment the following tolerations if Dashboard must not be deployed on master
      tolerations:
        - key: node-role.kubernetes.io/master
          effect: NoSchedule
      volumes:
        - name: tmp-volume
          emptyDir: {}
' > kubernetes-dashboard-recommended.yaml
# ========================================================================================================

kubectl apply -f kubernetes-dashboard-recommended.yaml

# 在Master节点准备一个yaml文件，为了创建一个访问者身份账号
kubectl apply -f https://kuboard.cn/install-script/k8s-dashboard/auth.yaml

# kubernetes dashboard对外的端口号了，前面文件中已经修改好了
# 在master节点上运行下面命令设置访问端口，将type: ClusterIP改为type: NodePort，并保存退出。这一步相当于把kubernetes-dashboard Web访问界面的端口暴露出来
# kubectl edit svc kubernetes-dashboard -n kubernetes-dashboard

# 接着在master节点运行下面命令可以查看kubernetes dashboard对外的端口号
kubectl get svc -A |grep kubernetes-dashboard

# 上面的命令运行成功后，在master运行下面的命令获取访问令牌，为了解锁Kubernetes Dashboard
kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')


echo -e "\n===================================================================="
echo -e "\n===============================Wait a minute! You can check kuboard status with below command!====================================="
echo -e "\nkubectl -n kubernetes-dashboard get pod"
echo -e "\n"

echo -e "\nWhen kuboard pod is ok, you can visit https://"${apiserver_advertise_address}":30001 to Kubernetes Dashboard by Firefox with token: \n"
kubectl -n kubernetes-dashboard get secret $(kubectl -n kubernetes-dashboard get sa/admin-user -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"

echo -e "\n===================================================================="


echo -e "\n==========================Finish Install Kubernetes Dashboard=========================="


```

## 1.2. 自定义部署应用

### 1.2.1. 前置条件

首先需要一主双从的服务器，通过上面的脚本安装所需要的环境

```bash
# 主节点加运行下面命令可重新打印work node加入集群的命令
# 如果Token过期，可以在Master节点执行下面的命令重新生成加入集群的token新令牌，把生成的命令在node节点中运行。高可用部署方式，也是在这一步的时候，使用添加主节点的命令即可
kubeadm token create --print-join-command

# 在需要加入集群的节点中运行上面打印的命令
# 需要注意的是，从节点被删除后重新加入时候需要在被删除的从节点运行下面的命令重置配置，再重新运行上面加入集群的命令
kubeadm reset --force

```



### 1.2.2. 自定义部署

```bash
# 初始化文件
vim custom-app.yaml
```



```yaml
# NameSpace
apiVersion: v1
kind: Namespace
metadata:
  name: k8s

---

# 节点不可以设置，通过命令新加入集群时候会报错
# # k8s-master
# apiVersion: v1
# kind: Node
# metadata:
#   name: "172.16.26.3"
#   labels:
#     name: k8s-master
#   namespace: k8s

# ---
# # k8s-node1
# apiVersion: v1
# kind: Node
# metadata:
#   name: "172.16.26.4"
#   labels:
#     name: k8s-node1
#   namespace: k8s

# ---
# # k8s-node2
# apiVersion: v1
# kind: Node
# metadata:
#   name: "172.16.26.5"
#   labels:
#     name: k8s-node2
#   namespace: k8s



```

### 1.2.3. MySQL8集群（需要一点点地修改，并有待验证）

```bash
# 初始化文件，由于下载的镜像较多，本机的虚拟机运行时候时间有点长
vim mysql8-app.yaml
kubectl apply -f mysql8-app.yaml
```



```yaml


```



## 1.3. centos下安装docker

### 1.3.1. 移除以前docker相关包

```bash
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
```

### 1.3.2. 配置yum源

```bash
sudo yum install -y yum-utils
sudo yum-config-manager \
--add-repo \
http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```

### 1.3.3. 安装docker

```bash
sudo yum install -y docker-ce docker-ce-cli containerd.io


// 以下是在安装k8s的时候使用, 也是比较新的Docker版本，为了兼容k8s
// 有可能最新的包不兼容该版本的Docker，尝试在命令行中添加 '--allowerasing' 来替换冲突的软件包 或 '--skip-broken' 来跳过无法安装的软件包 或 '--nobest' 来不只使用软件包的最佳候选
yum install -y docker-ce-20.10.7 docker-ce-cli-20.10.7  containerd.io-1.4.6 --allowerasing
```

### 1.3.4. 启动并验证docker版本

```bash
systemctl enable docker --now
docker -v
```

### 1.3.5. 配置自己的阿里云镜像加速

这里额外添加了docker的生产环境核心配置cgroup

```bash
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://w4plc3h7.mirror.aliyuncs.com"],
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
sudo docker info //可以检查是否修改好了Docker镜像地址
```

## 1.4. kubeadm引导安装Kubernetes

请参照以前Docker安装。先提前为所有机器安装Docker

### 1.4.1. 安装kubeadm

备份镜像在分配的静态IP地址分别如下：

```
172.16.26.6 // single node
```



- 一台兼容的 Linux 主机。Kubernetes 项目为基于 Debian 和 Red Hat 的 Linux 发行版以及一些不提供包管理器的发行版提供通用的指令
- 每台机器 2 GB 或更多的 RAM （如果少于这个数字将会影响你应用的运行内存)

- 2 CPU 核或更多
- 集群中的所有机器的网络彼此均能相互连接(公网和内网都可以)
  - **设置防火墙放行规则**

- 节点之中不可以有重复的主机名、MAC 地址或 product_uuid。请参见[这里](https://kubernetes.io/zh/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#verify-mac-address)了解更多详细信息。
  - **设置不同hostname**

- 开启机器上的某些端口。请参见[这里](https://kubernetes.io/zh/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#check-required-ports) 了解更多详细信息。

- - **内网互信**

- 禁用交换分区。为了保证 kubelet 正常工作，你 **必须** 禁用交换分区。
  - **永久关闭**


#### 1.4.1.1. 基础环境

所有机器执行以下操作

```bash
# 各个机器分别设置自己对应的域名，为了使其有意义方便识别
hostnamectl set-hostname k8s-single-node


# 将 SELinux 设置为 permissive 模式（相当于将其禁用），这是Linux的一些安全设置，需要将其禁用，方便后面的开发
// 临时禁用
sudo setenforce 0
// 永久禁用
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
// 禁用防火墙，非常重要
systemctl disable firewalld --now

/#关闭swap
swapoff -a  
sed -ri 's/.*swap.*/#&/' /etc/fstab

/#允许 iptables 检查桥接流量，IPv6的流量桥接到IPv4上（k8s官方要求这样做的）
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
br_netfilter
EOF

// 使上面的配置生效
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system
```

#### 1.4.1.2. 安装kubelet、kubeadm、kubectl

```bash
// 配置k8s去哪里(阿里云镜像)下载
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=http://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=http://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg
   http://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF

// 安装k8s
sudo yum install -y kubelet-1.20.9 kubeadm-1.20.9 kubectl-1.20.9 --disableexcludes=kubernetes

// 启动k8s
sudo systemctl enable --now kubelet
```

kubelet 现在每隔几秒就会重启，因为它陷入了一个等待 kubeadm 指令的死循环

### 1.4.2. 使用kubeadm引导集群

#### 1.4.2.1. 下载各个机器需要的镜像

```bash
// 在节点上新增镜像下载脚本文件。注意下面的/是为了目录格式化，复制时候需要去掉
sudo tee ./images.sh <<-'EOF'
#!/bin/bash
images=(
kube-apiserver:v1.20.9
kube-proxy:v1.20.9
kube-controller-manager:v1.20.9
kube-scheduler:v1.20.9
coredns:1.7.0
etcd:3.4.13-0
pause:3.2
)
for imageName in ${images[@]} ; do
docker pull registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/$imageName
done
EOF

// 赋予文件执行权限并执行脚本文件安装镜像
chmod +x ./images.sh && ./images.sh

// 保险起见，可以用下面的命令查看镜像是否安装好
docker images

// 安装成功的话，大概有下面7个镜像
[root@localhost ~]# docker images
REPOSITORY                                                                 TAG        IMAGE ID       CREATED         SIZE
registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/kube-proxy                v1.20.9    8dbf9a6aa186   4 months ago    99.7MB
registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/kube-apiserver            v1.20.9    0d0d57e4f64c   4 months ago    122MB
registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/kube-controller-manager   v1.20.9    eb07fd4ad3b4   4 months ago    116MB
registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/kube-scheduler            v1.20.9    295014c114b3   4 months ago    47.3MB
registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/etcd                      3.4.13-0   0369cf4303ff   15 months ago   253MB
registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/coredns                   1.7.0      bfe3a36ebd25   17 months ago   45.2MB
registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/pause                     3.2        80d28bedfe5d   21 months ago   683kB
```

#### 1.4.2.2. 初始化主节点

```bash
// 所有机器添加所有节点域名映射，尤其是master节点，是为了让所有机器知道master节点在哪里，以下需要修改为自己的，在master台机器上通过ip a命令查看内网ip
echo "172.16.26.6  single-node" >> /etc/hosts

// 能执行下面命令ping通cluster-endpoint的话，说明上面的hosts配置成功
ping cluster-endpoint

// 主节点初始化，只需要在Master节点运行！！！！
// apiserver-advertise-address是master节点所在服务器的IP
// control-plane-endpoint是上面的配置的IP地址域名
// image-repository，这个是镜像地址，由于国外地址无法访问，故使用的阿里云仓库地址：registry.aliyuncs.com/google_containers
// kubernetes-version，这个参数是下载的k8s软件版本号
// service-cidr=10.96.0.0/16是做k8s内部做负载均衡的，以后安装时也套用即可，最好不要改
// pod-network-cidr=192.168.0.0/16，k8s内部的pod节点之间网络可以使用的IP段，需要和上面的service-cidr网络范围不重叠，最好也不要改，因为这是后面要安装组件calico相关的默认配置，不然安装calico组件时候需要修改CALICO_IPV4POOL_CIDR变量

kubeadm init \
--apiserver-advertise-address=172.16.26.6 \
--control-plane-endpoint=single-node \
--image-repository registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images \
--kubernetes-version v1.20.9 \
--service-cidr=10.96.0.0/16 \
--pod-network-cidr=192.168.0.0/16

// 上面的初始化不成功的话，可以通过下面的命令重置一下，再重新初始化
kubeadm reset

// 所有网络范围不重叠！！！！
```



```bash
// 上面的命令在Master节点运行成功后，需要你复制并粘贴下面的显示成功初始化的日志，并按照上面说的继续操作！！！
Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

You can now join any number of control-plane nodes by copying certificate authorities
and service account keys on each node and then running the following as root:

  kubeadm join single-node:6443 --token v3bcru.mvr6z1nnve9owf8k \
    --discovery-token-ca-cert-hash sha256:1b5459c275565329ed1cd19102f55fbbc79e1dd2ab850d85e1197c4049b8a4df \
    --control-plane 

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join single-node:6443 --token v3bcru.mvr6z1nnve9owf8k \
    --discovery-token-ca-cert-hash sha256:1b5459c275565329ed1cd19102f55fbbc79e1dd2ab850d85e1197c4049b8a4df 
```



```
// 正如上面所说的，需要在master节点上运行下面的命令
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
// 如果你是root用户也可以直接运行下面的命令达到和上面命令一样的效果
export KUBECONFIG=/etc/kubernetes/admin.conf
```

#### 1.4.2.3. 安装calico网络组件

[calico官网](https://docs.projectcalico.org/getting-started/kubernetes/self-managed-onprem/onpremises#install-calico-with-kubernetes-api-datastore-more-than-50-nodes)

```bash
// 在master节点中运行过下面命令安装calico网络组件
curl https://docs.projectcalico.org/manifests/calico.yaml -O
kubectl apply -f calico.yaml
```

#### 1.4.2.4. 部署dashboard

##### 1.4.2.4.1. 部署

kubernetes官方提供的可视化界面

https://github.com/kubernetes/dashboard

```
// 在Master节点上运行下面的命令创建部署dashboard
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.3.1/aio/deploy/recommended.yaml

kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.4.0/aio/deploy/recommended.yaml

// 上面的命令可能下载yaml文件都很慢，所以需要执行下面的命令先下载yaml文件，再应用yaml文件
wget https://raw.githubusercontent.com/kubernetes/dashboard/v2.3.1/aio/deploy/recommended.yaml
kubectl apply -f recommended.yaml
kubectl apply -f kubernetes-dashboard.yaml
```



上面的命令实在不能下载yaml文件，可以下面的文件内容，文件名可以是kubernetes-dashboard.yaml：

```bash
vim kubernetes-dashboard.yaml
kubectl apply -f kubernetes-dashboard.yaml
```



```yml
apiVersion: v1
kind: Namespace
metadata:
  name: kubernetes-dashboard

---

apiVersion: v1
kind: ServiceAccount
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard

---

kind: Service
apiVersion: v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
spec:
  ports:
    - port: 443
      targetPort: 8443
  selector:
    k8s-app: kubernetes-dashboard

---

apiVersion: v1
kind: Secret
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard-certs
  namespace: kubernetes-dashboard
type: Opaque

---

apiVersion: v1
kind: Secret
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard-csrf
  namespace: kubernetes-dashboard
type: Opaque
data:
  csrf: ""

---

apiVersion: v1
kind: Secret
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard-key-holder
  namespace: kubernetes-dashboard
type: Opaque

---

kind: ConfigMap
apiVersion: v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard-settings
  namespace: kubernetes-dashboard

---

kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
rules:
  - apiGroups: [""]
    resources: ["secrets"]
    resourceNames: ["kubernetes-dashboard-key-holder", "kubernetes-dashboard-certs", "kubernetes-dashboard-csrf"]
    verbs: ["get", "update", "delete"]
  - apiGroups: [""]
    resources: ["configmaps"]
    resourceNames: ["kubernetes-dashboard-settings"]
    verbs: ["get", "update"]
  - apiGroups: [""]
    resources: ["services"]
    resourceNames: ["heapster", "dashboard-metrics-scraper"]
    verbs: ["proxy"]
  - apiGroups: [""]
    resources: ["services/proxy"]
    resourceNames: ["heapster", "http:heapster:", "https:heapster:", "dashboard-metrics-scraper", "http:dashboard-metrics-scraper"]
    verbs: ["get"]

---

kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
rules:
  - apiGroups: ["metrics.k8s.io"]
    resources: ["pods", "nodes"]
    verbs: ["get", "list", "watch"]

---

apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: kubernetes-dashboard
subjects:
  - kind: ServiceAccount
    name: kubernetes-dashboard
    namespace: kubernetes-dashboard

---

apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: kubernetes-dashboard
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: kubernetes-dashboard
subjects:
  - kind: ServiceAccount
    name: kubernetes-dashboard
    namespace: kubernetes-dashboard

---

kind: Deployment
apiVersion: apps/v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
spec:
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      k8s-app: kubernetes-dashboard
  template:
    metadata:
      labels:
        k8s-app: kubernetes-dashboard
    spec:
      containers:
        - name: kubernetes-dashboard
          image: kubernetesui/dashboard:v2.3.1
          imagePullPolicy: Always
          ports:
            - containerPort: 8443
              protocol: TCP
          args:
            - --auto-generate-certificates
            - --namespace=kubernetes-dashboard
          volumeMounts:
            - name: kubernetes-dashboard-certs
              mountPath: /certs
            - mountPath: /tmp
              name: tmp-volume
          livenessProbe:
            httpGet:
              scheme: HTTPS
              path: /
              port: 8443
            initialDelaySeconds: 30
            timeoutSeconds: 30
          securityContext:
            allowPrivilegeEscalation: false
            readOnlyRootFilesystem: true
            runAsUser: 1001
            runAsGroup: 2001
      volumes:
        - name: kubernetes-dashboard-certs
          secret:
            secretName: kubernetes-dashboard-certs
        - name: tmp-volume
          emptyDir: {}
      serviceAccountName: kubernetes-dashboard
      nodeSelector:
        "kubernetes.io/os": linux
      tolerations:
        - key: node-role.kubernetes.io/master
          effect: NoSchedule

---

kind: Service
apiVersion: v1
metadata:
  labels:
    k8s-app: dashboard-metrics-scraper
  name: dashboard-metrics-scraper
  namespace: kubernetes-dashboard
spec:
  ports:
    - port: 8000
      targetPort: 8000
  selector:
    k8s-app: dashboard-metrics-scraper

---

kind: Deployment
apiVersion: apps/v1
metadata:
  labels:
    k8s-app: dashboard-metrics-scraper
  name: dashboard-metrics-scraper
  namespace: kubernetes-dashboard
spec:
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      k8s-app: dashboard-metrics-scraper
  template:
    metadata:
      labels:
        k8s-app: dashboard-metrics-scraper
      annotations:
        seccomp.security.alpha.kubernetes.io/pod: 'runtime/default'
    spec:
      containers:
        - name: dashboard-metrics-scraper
          image: kubernetesui/metrics-scraper:v1.0.6
          ports:
            - containerPort: 8000
              protocol: TCP
          livenessProbe:
            httpGet:
              scheme: HTTP
              path: /
              port: 8000
            initialDelaySeconds: 30
            timeoutSeconds: 30
          volumeMounts:
          - mountPath: /tmp
            name: tmp-volume
          securityContext:
            allowPrivilegeEscalation: false
            readOnlyRootFilesystem: true
            runAsUser: 1001
            runAsGroup: 2001
      serviceAccountName: kubernetes-dashboard
      nodeSelector:
        "kubernetes.io/os": linux
      tolerations:
        - key: node-role.kubernetes.io/master
          effect: NoSchedule
      volumes:
        - name: tmp-volume
          emptyDir: {}

```

##### 1.4.2.4.2. 设置访问端口

```bash
// 在master节点上运行下面命令设置访问端口，将type: ClusterIP改为type: NodePort，并保存退出。这一步相当于把kubernetes-dashboard Web访问界面的端口暴露出来
kubectl edit svc kubernetes-dashboard -n kubernetes-dashboard

// 接着在master节点运行下面命令
kubectl get svc -A |grep kubernetes-dashboard

// 运行后可以找到kubernetes-dashboard暴露出来的端口是32766
[root@localhost ~]# kubectl get svc -A |grep kubernetes-dashboard
kubernetes-dashboard   dashboard-metrics-scraper   ClusterIP   10.96.60.168   <none>        8000/TCP                 46s
kubernetes-dashboard   kubernetes-dashboard        NodePort    10.96.223.26   <none>        443:32766/TCP            46s

// 之后，可以通过https://172.16.26.6:30512访问kubernetes dashboard Web界面，这时候出现需要你输入Token来激活Kubernetes dashboard，你需要通过后续的操作生成Token
// 其中IP地址可以是集群中的任意节点的IP地址，但是在本地利用虚拟机测试时候，Chrome、IE、Safari浏览器访问时候是不能访问的，因为访问需要的是HTTPS协议，唯有 Firefox 才能解忧，才能访问。不过后面租用了云服务器应该就不会出现这样的问题了
```

##### 1.4.2.4.3. 创建访问账号

在Master节点准备一个yaml文件，为了创建一个访问者身份账号

```bash
vi dash.yaml
```

文件内容如下：

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kubernetes-dashboard
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kubernetes-dashboard

```

接着在Master上运行下面的命令

```bash
kubectl apply -f dash.yaml
```

##### 1.4.2.4.4. 令牌访问

```bash
// 上面的命令运行成功后，在master运行下面的命令获取访问令牌，为了解锁Kubernetes Dashboard
kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep kubernetes-dashboard-admin | awk '{print $1}')
```

获取的Token如下：

```json
eyJhbGciOiJSUzI1NiIsImtpZCI6Ii1kWER1MElibWlNVTNiazQ2R2Q1ZUVnM0tYdUFnMzhIc19EV0hFLUtLVTQifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlcm5ldGVzLWRhc2hib2FyZCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJrdWJlcm5ldGVzLWRhc2hib2FyZC10b2tlbi1jYzUyOSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJrdWJlcm5ldGVzLWRhc2hib2FyZCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6ImU2NzZkNjlhLWFkNzUtNDAxZS1iZDEzLTk3ODg4NzU3NWYzMSIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDprdWJlcm5ldGVzLWRhc2hib2FyZDprdWJlcm5ldGVzLWRhc2hib2FyZCJ9.QBHj9TiZj5UMzQqecdgU6iv-a0L_EmPeSkUoEN03wU-TFmklIKvL8bEIdAcXTIuzbYLUAg3E6qpI8GcmmO18MbrxmJTZUMpaDmsVI3NZY8a16_d5FwLrwIg8DRteBBulzTeaje7NPd-G68JzlvF3kvVV6Dmr4-g2AQoxTtYpeGSePYU07RXarqrXWqKiQIiNFo4dZhLMvkORrdPrhzY6OAKYyZ9rl-82uslH6KKTSrLB0EsTDsC5L-ayhpLpWQLGs4P-UrEFPsgVKhWZ9cHplPAsICAXxS428O2ISxbEVqa06_2KQJjeu398S8nmBKnyed1xVQw6CawT_pAlzFAXUQ
```

##### 1.4.2.4.5. 界面

![image.png](./images/Kubernetes-k8s-8.png)

## 1.5. Mac单击上安装Kubernetes（k8s）

### 1.5.1. Docker for Mac

安装k8s之前，需要安装docker，这个比较简单

**同时需要注意的是**，Docker和k8s的版本匹配，需要参考https://github.com/maguowei/k8s-docker-desktop-for-mac 的README.md的简介页面，其中简介中说明了在Mac上最新的Kubernetes的版本号是多少，对应的Docker版本是多少（Docker的版本可以有差别，但是别差别太大，因为可能会有兼容性问题），经过测试可用。示例如下：

说明:

- 当前在 `Docker Desktop (Mac) Version 4.1.1 (Kubernetes: v1.21.5)`上经过测试可用
- 使用 `Kubeadm` 在`Ubuntu`上安装 `Kubernetes` 请查看 [gotok8s](https://github.com/maguowei/gotok8s)



![image-20211122105612840](./images/Kubernetes-k8s-5.png)

### 1.5.2. 拉取k8s镜像

但是由于众所周知的原因, 国内的网络下不能很方便的下载 Kubernetes 集群所需要的镜像, 导致集群启用失败。这里提供了一个简单的方法, 利用 [GitHub Actions](https://links.jianshu.com/go?to=https%3A%2F%2Fdeveloper.github.com%2Factions%2Fcreating-github-actions%2F) 实现 k8s.gcr.io 上 kubernetes 依赖镜像自动同步到 [Docker Hub](https://links.jianshu.com/go?to=https%3A%2F%2Fhub.docker.com%2F) 上指定的仓库中。 通过 [**load_images.sh**](https://github.com/maguowei/k8s-docker-desktop-for-mac/blob/master/load_images.sh) 将所需镜像从 Docker Hub 的同步仓库中取回，并重新打上原始的tag. 镜像对应关系文件可以查看: [**images**](https://github.com/maguowei/k8s-docker-desktop-for-mac/blob/master/images)。



```bash
// 需要跳转到合适的文件路径下，在该文件夹下打开命令行可能需要下载临时文件
// 1. 下载k8s镜像加载文件
git clone https://github.com/gotok8s/k8s-docker-desktop-for-mac.git

// 2. 进入 k8s-docker-desktop-for-mac项目
cd k8s-docker-desktop-for-mac/

// 3. 运行镜像加载文件，拉取镜像（需要大概2分钟）
./load_images.sh
```



接下来打开docker Kubernetes配置页面，勾选`Enable Kubernetes`，再点击`Apply & Restart`，需要等k8s和docker重新启动，需要等一会。

![image-20211122112432961](./images/Kubernetes-k8s-6.png)



```bash
// 如果安装成功，通过下面的命令验证
kubectl cluster-info
kubectl get nodes
kubectl describe node
```

### 1.5.3. 安装 Kubernetes Dashboard

```bash
// 部署 Kubernetes Dashboard
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended.yaml
kubectl apply -f https://gitlab.com/yanglin5260/dashboard/-/raw/v2.4.0/aio/deploy/recommended.yaml
// 开启本机访问代理
kubectl proxy
```



```bash
// 部署 Kubernetes Dashboard，用下面官方的下载命令在国内不能正常下载
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended.yaml
```

所以需要手动创建yaml文件，可以用`vim recommended.yaml`，再通过`kubectl apply -f recommended.yaml`命令安装Kubernetes Dashboard，同时通过命令开启本级访问代理：`kubectl proxy`

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: kubernetes-dashboard

---

apiVersion: v1
kind: ServiceAccount
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard

---

kind: Service
apiVersion: v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
spec:
  ports:
    - port: 443
      targetPort: 8443
  selector:
    k8s-app: kubernetes-dashboard

---

apiVersion: v1
kind: Secret
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard-certs
  namespace: kubernetes-dashboard
type: Opaque

---

apiVersion: v1
kind: Secret
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard-csrf
  namespace: kubernetes-dashboard
type: Opaque
data:
  csrf: ""

---

apiVersion: v1
kind: Secret
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard-key-holder
  namespace: kubernetes-dashboard
type: Opaque

---

kind: ConfigMap
apiVersion: v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard-settings
  namespace: kubernetes-dashboard

---

kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
rules:
  - apiGroups: [""]
    resources: ["secrets"]
    resourceNames: ["kubernetes-dashboard-key-holder", "kubernetes-dashboard-certs", "kubernetes-dashboard-csrf"]
    verbs: ["get", "update", "delete"]
  - apiGroups: [""]
    resources: ["configmaps"]
    resourceNames: ["kubernetes-dashboard-settings"]
    verbs: ["get", "update"]
  - apiGroups: [""]
    resources: ["services"]
    resourceNames: ["heapster", "dashboard-metrics-scraper"]
    verbs: ["proxy"]
  - apiGroups: [""]
    resources: ["services/proxy"]
    resourceNames: ["heapster", "http:heapster:", "https:heapster:", "dashboard-metrics-scraper", "http:dashboard-metrics-scraper"]
    verbs: ["get"]

---

kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
rules:
  - apiGroups: ["metrics.k8s.io"]
    resources: ["pods", "nodes"]
    verbs: ["get", "list", "watch"]

---

apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: kubernetes-dashboard
subjects:
  - kind: ServiceAccount
    name: kubernetes-dashboard
    namespace: kubernetes-dashboard

---

apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: kubernetes-dashboard
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: kubernetes-dashboard
subjects:
  - kind: ServiceAccount
    name: kubernetes-dashboard
    namespace: kubernetes-dashboard

---

kind: Deployment
apiVersion: apps/v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
spec:
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      k8s-app: kubernetes-dashboard
  template:
    metadata:
      labels:
        k8s-app: kubernetes-dashboard
    spec:
      containers:
        - name: kubernetes-dashboard
          image: kubernetesui/dashboard:v2.3.1
          imagePullPolicy: Always
          ports:
            - containerPort: 8443
              protocol: TCP
          args:
            - --auto-generate-certificates
            - --namespace=kubernetes-dashboard
          volumeMounts:
            - name: kubernetes-dashboard-certs
              mountPath: /certs
            - mountPath: /tmp
              name: tmp-volume
          livenessProbe:
            httpGet:
              scheme: HTTPS
              path: /
              port: 8443
            initialDelaySeconds: 30
            timeoutSeconds: 30
          securityContext:
            allowPrivilegeEscalation: false
            readOnlyRootFilesystem: true
            runAsUser: 1001
            runAsGroup: 2001
      volumes:
        - name: kubernetes-dashboard-certs
          secret:
            secretName: kubernetes-dashboard-certs
        - name: tmp-volume
          emptyDir: {}
      serviceAccountName: kubernetes-dashboard
      nodeSelector:
        "kubernetes.io/os": linux
      tolerations:
        - key: node-role.kubernetes.io/master
          effect: NoSchedule

---

kind: Service
apiVersion: v1
metadata:
  labels:
    k8s-app: dashboard-metrics-scraper
  name: dashboard-metrics-scraper
  namespace: kubernetes-dashboard
spec:
  ports:
    - port: 8000
      targetPort: 8000
  selector:
    k8s-app: dashboard-metrics-scraper

---

kind: Deployment
apiVersion: apps/v1
metadata:
  labels:
    k8s-app: dashboard-metrics-scraper
  name: dashboard-metrics-scraper
  namespace: kubernetes-dashboard
spec:
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      k8s-app: dashboard-metrics-scraper
  template:
    metadata:
      labels:
        k8s-app: dashboard-metrics-scraper
      annotations:
        seccomp.security.alpha.kubernetes.io/pod: 'runtime/default'
    spec:
      containers:
        - name: dashboard-metrics-scraper
          image: kubernetesui/metrics-scraper:v1.0.6
          ports:
            - containerPort: 8000
              protocol: TCP
          livenessProbe:
            httpGet:
              scheme: HTTP
              path: /
              port: 8000
            initialDelaySeconds: 30
            timeoutSeconds: 30
          volumeMounts:
          - mountPath: /tmp
            name: tmp-volume
          securityContext:
            allowPrivilegeEscalation: false
            readOnlyRootFilesystem: true
            runAsUser: 1001
            runAsGroup: 2001
      serviceAccountName: kubernetes-dashboard
      nodeSelector:
        "kubernetes.io/os": linux
      tolerations:
        - key: node-role.kubernetes.io/master
          effect: NoSchedule
      volumes:
        - name: tmp-volume
          emptyDir: {}

```



#### 1.5.3.1. 设置访问端口

```bash
// 运行下面命令设置访问端口，将type: ClusterIP改为type: NodePort，并保存退出。这一步相当于把kubernetes-dashboard Web访问界面的端口暴露出来
kubectl edit svc kubernetes-dashboard -n kubernetes-dashboard

// 运行下面命令查看暴露出来的端口
kubectl get svc -A |grep kubernetes-dashboard

// 运行后可以找到kubernetes-dashboard暴露出来的端口是30001
C02CP3PKMD6R:Temp C5323535$ kubectl get svc -A |grep kubernetes-dashboard
kubernetes-dashboard   dashboard-metrics-scraper   ClusterIP   10.96.201.108    <none>        8000/TCP                 179m
kubernetes-dashboard   kubernetes-dashboard        NodePort    10.104.172.186   <none>        443:30001/TCP            179m

// 之后，可以通过 https://127.0.0.1:30001 或者 https://10.59.168.145:30001 访问kubernetes dashboard Web界面，这时候会出现需要你输入Token来激活Kubernetes dashboard，你需要通过后续的操作生成Token
// 其中IP地址可以是集群中的任意节点的IP地址，但是在本地利用虚拟机测试时候，Chrome、IE、Safari浏览器访问时候是不能访问的，因为访问需要的是HTTPS协议，唯有 Firefox 才能解忧，才能访问。不过后面租用了云服务器应该就不会出现这样的问题了
```



#### 1.5.3.2. 创建访问账号

在Master节点准备一个yaml文件，为了创建一个访问者身份账号

```
# 在Master节点准备一个yaml文件，为了创建一个访问者身份账号
kubectl apply -f https://kuboard.cn/install-script/k8s-dashboard/auth.yaml

# kubernetes dashboard对外的端口号了，前面文件中已经修改好了
# 在master节点上运行下面命令设置访问端口，将type: ClusterIP改为type: NodePort，并保存退出。这一步相当于把kubernetes-dashboard Web访问界面的端口暴露出来
# kubectl edit svc kubernetes-dashboard -n kubernetes-dashboard

# 接着在master节点运行下面命令可以查看kubernetes dashboard对外的端口号
kubectl get svc -A |grep kubernetes-dashboard

# 上面的命令运行成功后，在master运行下面的命令获取访问令牌，为了解锁Kubernetes Dashboard
kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')
```



```bash
vi dash.yaml
```

文件内容如下：

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kubernetes-dashboard
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kubernetes-dashboard

```

运行下面的命令

```bash
kubectl apply -f dash.yaml
```

#### 1.5.3.3. 令牌访问

```bash
// 上面的命令运行成功后，运行下面的命令获取访问令牌，为了解锁Kubernetes Dashboard
kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep kubernetes-dashboard-admin | awk '{print $1}')
```

获取的Token如下，Mac点：

```json
eyJhbGciOiJSUzI1NiIsImtpZCI6ImJKbUV0UTl4M19FOUo5YjRLblI5M202MENGU2dzcTNwMmRLQW9YWERyMkEifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlcm5ldGVzLWRhc2hib2FyZCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJrdWJlcm5ldGVzLWRhc2hib2FyZC10b2tlbi1ka2Y2eiIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJrdWJlcm5ldGVzLWRhc2hib2FyZCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6ImY5OWZlZWI4LWIyYmItNDE4MC04OGJiLTViN2VmYTViMDliZCIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDprdWJlcm5ldGVzLWRhc2hib2FyZDprdWJlcm5ldGVzLWRhc2hib2FyZCJ9.oiecqVb3vpsswGuvYUlkXnID6bMJxF4HhYGsY_7yqKnOeuaXFMuYA12wxbFxy3A79yIo4N-C4TB-Qqpr3P-unPg7Wsppnt0tN-n2EmahhmSP7EH0FRjXysWYfGoYRk7FyC14ua460A-lCR-2sKPvWNlZDOz6oCXd5QGbhNjbVUVzL17nuPw8MHytwLUX6cxoT-__fkFXJLPzXQdlrhlV7gl6evOe8EKiFAttQl-Wi9OeIWJxkCyGRDk3lHd9xLV7Ly0n76m3C5D_2fj3mL_ilrnwGFb565uMfz0B8FMVzSWOrRKlqNbz59G5qFzKV9zfpb0YU9Bm99NOgjCz4uxZmQ
```

#### 1.5.3.4. 界面

![image-20211122153838388](./images/Kubernetes-k8s-7.png)

# 2. MySQL主从集群（Mac单机版）

所有的配置文件都在[mysql8集群部署](./mysql8-cluster)文件夹中



参考文章：

https://blog.csdn.net/weixin_33089993/article/details/113271223

https://www.jianshu.com/p/9d9242fb8f8e

## 2.1. 准备MySQL8主从集群的镜像

需要从远程路径https://github.com/docker-library/mysql/tree/master/8.0下载准备镜像的配置文件（其中MySQL8的版本也是不断的在更新，目前的版本是8.0.27-1debian10，可在下面的Dockerfile.debian文件中看到。后续可根据情况适时更新版本），命令如下：

```bash
// 下载mysql的Docker build repository，复制其中 /8.0 文件夹下面的两个文件Dockerfile.debian，docker-entrypoint.sh, config文件夹到指定文件夹用于备用
git clone https://github.com/docker-library/mysql.git
```



### 2.1.1. 准备Master镜像

```bash
// 先复制一份Master的镜像配置文件以作相应的修改
mkdir master
cp Dockerfile.debian ./master/Dockerfile
cp docker-entrypoint.sh ./master/docker-entrypoint.sh
mkdir master/config/
cp -R config/ master/config/


// 在Dockerfile中添加如下内容，将mysql master的server-id设置为1（集群中server-id的设置，以及server-id不重复对于集群非常重要）
// 添加的位置大概在 VOLUME /var/lib/mysql 内容的上面
RUN sed -i '/\[mysqld\]/a server-id=1\nlog-bin' /etc/mysql/my.cnf
```

![image-20211123191939715](./images/Kubernetes-k8s-1.png)



```bash
// 在./master/docker-entrypoint.sh中添加如下内容，创建一个复制用户并赋权限，刷新系统权限表，
// 添加位置在 docker_setup_db() 内容下，具体位置如下图
// 注意变量的配置：MYSQL_REPLICATION_USER、MYSQL_REPLICATION_PASSWORD
// 还要注意，修改了root和repl的密码加密方式
	if [ -n "$MYSQL_REPLICATION_USER" ] && [ -n "$MYSQL_REPLICATION_PASSWORD" ]; then
		mysql_note "Creating replication user ${MYSQL_REPLICATION_USER}"
		docker_process_sql --database=mysql <<<"CREATE USER '$MYSQL_REPLICATION_USER'@'%' IDENTIFIED BY '$MYSQL_REPLICATION_PASSWORD' ;"
		docker_process_sql --database=mysql <<<"ALTER USER '$MYSQL_REPLICATION_USER'@'%' IDENTIFIED WITH mysql_native_password BY '$MYSQL_REPLICATION_PASSWORD' PASSWORD EXPIRE NEVER ;"
		docker_process_sql --database=mysql <<<"ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY '$MYSQL_ROOT_PASSWORD' PASSWORD EXPIRE NEVER ;"

		mysql_note "Giving replication user ${MYSQL_REPLICATION_USER} access to schema all database"
		docker_process_sql --database=mysql <<<"GRANT REPLICATION SLAVE ON *.* TO '$MYSQL_REPLICATION_USER'@'%' IDENTIFIED BY '$MYSQL_REPLICATION_PASSWORD' ;"
		docker_process_sql --database=mysql <<<"FLUSH PRIVILEGES ;"
	fi
```

![image-20211123173542526](./images/Kubernetes-k8s-2.png)

### 2.1.2. 准备Slave镜像

```bash
// 先复制一份Slave的镜像配置文件以作相应的修改
mkdir slave
cp Dockerfile.debian ./slave/Dockerfile
cp docker-entrypoint.sh ./slave/docker-entrypoint.sh
mkdir slave/config/
cp -R config/ slave/config/

// 在Dockerfile-slave中添加如下内容，将mysql slave的server-id设置为一个（集群中server-id的设置，以及server-id不重复对于集群非常重要）
// 添加的位置大概在 VOLUME /var/lib/mysql 内容的上面
RUN RAND="$(date +%s | rev | cut -c 1-2)$(echo ${RANDOM})" && sed -i '/\[mysqld\]/a server-id='$RAND'\nlog-bin' /etc/mysql/my.cnf
```

![image-20211123192110255](./images/Kubernetes-k8s-3.png)

```bash
// 在docker-entrypoint-slave.sh中添加如下内容，配置连接master主机的host、user、password等参数，并启动复制进程。
// 添加位置在 docker_setup_db() 内容下，具体位置如下图
// 注意变量的配置：MYSQL_REPLICATION_USER、MYSQL_REPLICATION_PASSWORD、MYSQL_MASTER_SERVICE_HOST
	if [ -n "$MYSQL_REPLICATION_USER" ] && [ -n "$MYSQL_REPLICATION_PASSWORD" ] && [ -n "$MYSQL_MASTER_SERVICE_HOST" ]; then
		mysql_note "Stop slave server"
		docker_process_sql --database=mysql <<<"STOP SLAVE ;"

		mysql_note "Change master info, master_host=${MYSQL_MASTER_SERVICE_HOST}, master_user=${MYSQL_REPLICATION_USER}, master_password=${MYSQL_REPLICATION_PASSWORD}"
		docker_process_sql --database=mysql <<<"CHANGE MASTER TO master_host='$MYSQL_MASTER_SERVICE_HOST', master_user='$MYSQL_REPLICATION_USER', master_password='$MYSQL_REPLICATION_PASSWORD' ;"

		mysql_note "Start slave server"
		docker_process_sql --database=mysql <<<"START SLAVE ;"
	fi
```

![image-20211123180612282](./images/Kubernetes-k8s-4.png)

## 2.2. 开始使用修改好的dockerfile创建mysqlde master和slave镜像

```bash
// 推送自己的镜像时候，需要先检查自己是否登录了Docker hub，登录和登出命令如下（账号是yanglin520，密码是常用的）：
docker login
docker logout


// 进入master文件夹执行下面的命令，构建镜像（注意需要修改镜像的版本）
docker build -t yanglin520/mysql8-master:8.0.27 .
// 进入slave文件夹执行下面的命令
docker build -t yanglin520/mysql8-slave:8.0.27 .
// 使用命令查看master和slave镜像是否在本地镜像仓库
docker images

// 分别执行下面的命令推送镜像（注意需要修改镜像的版本）
docker push yanglin520/mysql8-master:8.0.27
docker push yanglin520/mysql8-slave:8.0.27
```

## 2.3. 创建pv和pvc，用于mysql主从存储持久化数据

### 2.3.1. Master节点的PV、PVC

```bash
// 在master目录下新增master的pv和pvc文件
vim master/nfs-pv-master.yml
vim master/nfs-pvc-master.yaml

// 之后创建PV，PVC资源
kubectl create -f master/nfs-pv-master.yml
kubectl create -f master/nfs-pvc-master.yaml

// 利用命令查看资源是否创建成功
kubectl get pv,pvc
```

**Master PV**

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-nfs-mysql-master # 名字随意取，但是最好有意义，且只能是小写
spec:
  capacity:
    storage: 5Gi # 最多占用磁盘空间
  accessModes:
    - ReadWriteOnce
  storageClassName: nfs # 可以随意取，但是一类存取卷最好一致，因为它相当于命名空间
  nfs:
    path: /Users/C5323535/Work/tempdata/mysql_master # NFS存储Server的文件夹地址，必须要存在，可以手动创建
    server: 10.59.168.145 # 作为NFS存储Server的IP地址
  persistentVolumeReclaimPolicy: Recycle # 回收策略：回收

```

**Master PVC**

```yaml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: pv-nfs-mysql-master
spec:
  accessModes:
    - ReadWriteOnce
  storageClassName: nfs # 可以随意取，但是一类存取卷最好一致，因为它相当于命名空间
  resources:
    requests:
      storage: 5Gi # 申请申明存储大小

```

### 2.3.2. Slave节点的PV、PVC

```bash
// 在slave目录下新增slave的pv和pvc文件
vim slave/nfs-pv-slave.yml
vim slave/nfs-pvc-slave.yaml

// 之后创建PV，PVC资源
kubectl create -f slave/nfs-pv-slave.yml
kubectl create -f slave/nfs-pvc-slave.yaml

// 利用命令查看资源是否创建成功
kubectl get pv,pvc
```

**Slave PV**

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-nfs-mysql-slave # 名字随意取，但是最好有意义，且只能是小写
spec:
  capacity:
    storage: 6Gi # 最多占用磁盘空间
  accessModes:
    - ReadWriteOnce
  storageClassName: nfs # 可以随意取，但是一类存取卷最好一致，因为它相当于命名空间
  nfs:
    path: /Users/C5323535/Work/tempdata/mysql_slave # NFS存储Server的文件夹地址，必须要存在，可以手动创建
    server: 10.59.168.145 # 作为NFS存储Server的IP地址
  persistentVolumeReclaimPolicy: Recycle # 回收策略：回收

```

**Slave PVC**

```yaml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: pv-nfs-mysql-slave
spec:
  accessModes:
    - ReadWriteOnce
  storageClassName: nfs # 可以随意取，但是一类存取卷最好一致，因为它相当于命名空间
  resources:
    requests:
      storage: 6Gi # 申请申明存储大小

```

## 2.4. Master节点的ReplicationController和Service

### 2.4.1. 先要开启Mac上的NFS服务(Blocked)

```bash
// 修改配置，为开启NFS服务做准备
sudo vim /etc/exports
// 添加如下内容，注意确认Mac的IP地址网段和子网掩码！！！改变挂载路径的话需要等一会儿，或者用sudo nfsd restart重启NFS服务，才能使新共享文件夹生效
/Users/C5323535/Work/tempmacshare -alldirs -rw -network=10.0.0.0 -mask=255.255.224.0
/Users/C5323535/Work/tempmacshare -alldirs -rw -network=192.0.0.0 -mask=255.255.225.0
/Users/C5323535/Work/tempmacshare *(insecure,rw,sync,no_root_squash) -alldirs -rw -maproot=C5323535:staff -network=10.0.0.0 -mask=255.255.224.0
/Users/C5323535/Work/tempmacshare *(insecure,rw,sync,no_root_squash) -alldirs -rw -maproot=root:staff -network=10.0.0.0 -mask=255.255.224.0


// 上面命令的知识扩展：
  // rw 读写方式，根据权限需要有时也设置为 ro(read only)
  // bg 如果安装失败(服务器没有响应), 在后台一直尝试，继续发其他的安装请求
  // hard 如果服务器当机，让试图访问它的操作被阻塞，直到服务器恢复为止
  // intr 允许用户中断被阻塞的操作(并且让它们返回一条错误消息)
  // tcp 选择通过 TCP 来传输，默认的 UDP 不好。
  // /Users/C5323535 *(insecure,rw,sync,no_root_squash)
  // /Users/C5323535 -alldirs -rw -maproot=yanglin:staff -network 10.59.168.0 -mask 255.255.224.0
  // /Users/C5323535/Work/tempmacshare *(insecure,rw,async,no_root_squash,anonuid=65534,anongid=65534)


// 控制服务
// 先查看nfsd是否开启
nfsd status
// 如果没开启，执行开启命令
nfsd start
// 下面有nfsd的常用命令
sudo nfsd enable
sudo nfsd disable
sudo nfsd start
sudo nfsd stop
sudo nfsd restart
sudo nfsd status

// 检查当前机器是否可以挂载到IP为10.59.168.145的Mac机器
showmount -e 10.59.168.145

// 挂载生效的话，大概会显示下面的的信息
C02CP3PKMD6R:Work C5323535$ showmount -e 10.59.168.145
Exports list on 10.59.168.145:
/Users/C5323535/Work/tempmacshare   10.0.0.0


// 在当前机器上创建目录，并挂载到作为NFS服务器的Mac上的共享目录 /Users/C5323535/Work/tempmacshare/ 到本机路径 /Users/C5323535/Work/tempdata/（注意路径的对应）
sudo mkdir -p /Users/C5323535/Work/tempdata/ & chmod -Rr 777 /Users/C5323535/Work/tempdata/
sudo mkdir -p /Users/C5323535/Work/tempmacshare/ & chmod -Rr 777 /Users/C5323535/Work/tempmacshare/
sudo mount -t nfs 10.59.168.145:/Users/C5323535/Work/tempmacshare /Users/C5323535/Work/tempdata
sudo mount -t nfs -o nolock,nfsvers=3,vers=3 10.59.168.145:/Users/C5323535/Work/tempmacshare /Users/C5323535/Work/tempdata

// 写入一个测试文件
echo "hello nfs server" > /nfs/data/test.txt

// 在Master nfs主节点上创建上面说的文件夹
mkdir -p /Users/C5323535/Work/tempdata/
// 在Master nfs主节点启动RPC远程绑定，为了达到同步目录的作用，现在永久生效，开机启动
systemctl enable rpcbind --now
// 现在启动，开机启动nfs服务器
systemctl enable nfs-server --now
// 使配置生效
exportfs -r
```



### 2.4.2. ReplicationController

```bash
// 在master目录下新增Master节点的ReplicationController文件
vim master/mysql-master-rc.yml

// 之后创建ReplicationController资源
kubectl create -f master/mysql-master-rc.yml

// 利用命令查看资源是否创建成功，rc是ReplicationController的缩写
kubectl get rc,pod
```

**Master ReplicationController内容**

```yaml
apiVersion: v1
kind: ReplicationController
metadata:
  name: mysql-master
spec:
  replicas: 1
  selector:
    name: mysql-master
  template:
    metadata:
      labels:
        name: mysql-master
    spec:
      containers:
        - name: mysql-master
          image: yanglin520/mysql8-slave:8.0.27 # 这是上面构建并上传到Docker的镜像
          volumeMounts:
            - mountPath: /var/lib/mysql
              name: mysql-master-data
          ports:
            - containerPort: 3306
          env:
            - name: MYSQL_REPLICATION_USER
              value: "repl"
            - name: MYSQL_REPLICAITON_PASSWORD
              value: "12345678"
      volumes:
        - name: mysql-master-data
          persistentVolumeClaim:
            claimName: pv-nfs-mysql-master

```

### 2.4.3. Service

```bash
// 在master目录下新增Master节点的Service资源文件
vim master/mysql-master-svc.yml

// 之后创建Service资源
kubectl create -f master/mysql-master-svc.yml

// 利用命令查看资源是否创建成功，svc是Service的缩写
kubectl get svc,pod
```

**Master Service内容**

```yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    name: mysql-master
  name: mysql-master
spec:
  type: NodePort
  selector:
    name: mysql-master
  ports:
    - port: 3306
      targetPort: 3306
      protocol: TCP
      name: http
      nodePort: 30066

```

# 3. MySQL主从集群（CentOS8，一主双从三节点）

所有的配置文件都在[mysql8集群部署](./mysql8-cluster)文件夹中



参考文章：

https://blog.csdn.net/weixin_33089993/article/details/113271223

https://blog.csdn.net/qq_38900565/article/details/114832445

https://www.jianshu.com/p/9d9242fb8f8e

## 3.1. 准备MySQL8主从集群的镜像

需要从远程路径https://github.com/docker-library/mysql/tree/master/8.0下载准备镜像的配置文件（其中MySQL8的版本也是不断的在更新，目前的版本是8.0.27-1debian10，可在下面的Dockerfile.debian文件中看到。后续可根据情况适时更新版本），命令如下：

```bash
// 没安装Git的话，先下载Git
yum install git

// 下载mysql的Docker build repository，复制其中 /8.0 文件夹下面的两个文件Dockerfile.debian，docker-entrypoint.sh, config文件夹到指定文件夹用于备用
git clone https://github.com/docker-library/mysql.git
```



### 3.1.1. 准备Master镜像

```bash
// 先复制一份Master的镜像配置文件以作相应的修改
mkdir master
cp ./mysql/8.0/Dockerfile.debian ./master/Dockerfile
cp ./mysql/8.0/docker-entrypoint.sh ./master/docker-entrypoint.sh
mkdir master/config/
cp -R ./mysql/8.0/config/ master/config/


// 在Dockerfile中添加如下内容，将mysql master的server-id设置为1（集群中server-id的设置，以及server-id不重复对于集群非常重要）
vim ./master/Dockerfile

// 添加的位置大概在 VOLUME /var/lib/mysql 内容的上面
RUN sed -i '/\[mysqld\]/a server-id=1\nlog-bin' /etc/mysql/my.cnf
```

![image-20211123191939715](./images/Kubernetes-k8s-1.png)



```bash
// 在./master/docker-entrypoint.sh中添加如下内容，创建一个复制用户并赋权限，刷新系统权限表，
vim ./master/docker-entrypoint.sh

// 添加位置在 docker_setup_db() 内容下，具体位置如下图
// 注意变量的配置：MYSQL_REPLICATION_USER、MYSQL_REPLICATION_PASSWORD
	if [ -n "$MYSQL_REPLICATION_USER" ] && [ -n "$MYSQL_REPLICATION_PASSWORD" ]; then
		mysql_note "Creating replication user ${MYSQL_REPLICATION_USER}"
		docker_process_sql --database=mysql <<<"CREATE USER '$MYSQL_REPLICATION_USER'@'%' IDENTIFIED BY '$MYSQL_REPLICATION_PASSWORD' ;"
		docker_process_sql --database=mysql <<<"ALTER USER '$MYSQL_REPLICATION_USER'@'%' IDENTIFIED WITH mysql_native_password BY '$MYSQL_REPLICATION_PASSWORD' PASSWORD EXPIRE NEVER ;"
		docker_process_sql --database=mysql <<<"ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY '$MYSQL_ROOT_PASSWORD' PASSWORD EXPIRE NEVER ;"

		mysql_note "Giving replication user ${MYSQL_REPLICATION_USER} access to schema all database"
		docker_process_sql --database=mysql <<<"GRANT REPLICATION SLAVE ON *.* TO '$MYSQL_REPLICATION_USER'@'%' IDENTIFIED BY '$MYSQL_REPLICATION_PASSWORD' ;"
		docker_process_sql --database=mysql <<<"FLUSH PRIVILEGES ;"
	fi
```

![image-20211123173542526](./images/Kubernetes-k8s-2.png)

### 3.1.2. 准备Slave镜像

```bash
// 先复制一份Slave的镜像配置文件以作相应的修改
mkdir slave
cp ./mysql/8.0/Dockerfile.debian ./slave/Dockerfile
cp ./mysql/8.0/docker-entrypoint.sh ./slave/docker-entrypoint.sh
mkdir slave/config/
cp -R ./mysql/8.0/config/ slave/config/

// 在Dockerfile中添加如下内容，将mysql slave的server-id设置为一个（集群中server-id的设置，以及server-id不重复对于集群非常重要）
vim ./slave/Dockerfile

// 添加的位置大概在 VOLUME /var/lib/mysql 内容的上面
RUN RAND="$(date +%s | rev | cut -c 1-2)$(echo ${RANDOM})" && sed -i '/\[mysqld\]/a server-id='$RAND'\nlog-bin' /etc/mysql/my.cnf
```

![image-20211123192110255](./images/Kubernetes-k8s-3.png)

```bash
// 在docker-entrypoint.sh中添加如下内容，配置连接master主机的host、user、password等参数，并启动复制进程。
vim ./slave/docker-entrypoint.sh

// 添加位置在 docker_setup_db() 内容下，具体位置如下图
// 注意变量的配置：MYSQL_REPLICATION_USER、MYSQL_REPLICATION_PASSWORD、MYSQL_MASTER_SERVICE_HOST
	if [ -n "$MYSQL_REPLICATION_USER" ] && [ -n "$MYSQL_REPLICATION_PASSWORD" ] && [ -n "$MYSQL_MASTER_SERVICE_HOST" ]; then
		mysql_note "Stop slave server"
		docker_process_sql --database=mysql <<<"STOP SLAVE ;"

		mysql_note "Change master info, master_host=${MYSQL_MASTER_SERVICE_HOST}, master_user=${MYSQL_REPLICATION_USER}, master_password=${MYSQL_REPLICATION_PASSWORD}"
		docker_process_sql --database=mysql <<<"CHANGE MASTER TO master_host='$MYSQL_MASTER_SERVICE_HOST', master_user='$MYSQL_REPLICATION_USER', master_password='$MYSQL_REPLICATION_PASSWORD' ;"

		mysql_note "Start slave server"
		docker_process_sql --database=mysql <<<"START SLAVE ;"
	fi
```

![image-20211123180612282](./images/Kubernetes-k8s-4.png)

## 3.2. 开始使用修改好的dockerfile创建mysqlde master和slave镜像

```bash
// 推送自己的镜像时候，需要先检查自己是否登录了Docker hub，登录和登出命令如下（账号是yanglin520，密码是常用的）：
docker login
docker logout


// 进入master文件夹执行下面的命令，构建镜像（注意需要修改镜像的版本）
docker build -t yanglin520/mysql8-master:8.0.27 .
// 进入slave文件夹执行下面的命令
docker build -t yanglin520/mysql8-slave:8.0.27 .
// 使用命令查看master和slave镜像是否在本地镜像仓库
docker images

// 分别执行下面的命令推送镜像（注意需要修改镜像的版本）
docker push yanglin520/mysql8-master:8.0.27
docker push yanglin520/mysql8-slave:8.0.27

// 有时候网络不好，可以直接下载之前构建并上传好的镜像
docker pull yanglin520/mysql8-master:8.0.27
docker pull yanglin520/mysql8-slave:8.0.27
```

## 3.3. 创建pv和pvc，用于mysql主从存储持久化数据

### 3.3.1. Master MySQL的PV、PVC

```bash
// 在master目录下分别新增master的pv和pvc文件
vim master/nfs-pv-master.yml
vim master/nfs-pvc-master.yaml

// 之后创建并应用PV，PVC资源
kubectl create -f master/nfs-pv-master.yml
kubectl create -f master/nfs-pvc-master.yaml

// 如果资源没创建对，可以通过下面的命令删除
kubectl delete pv pv-nfs-mysql-master
kubectl delete pvc pvc-nfs-mysql-master

// 利用命令查看资源是否创建成功
kubectl get pv,pvc
```

**Master PV**

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-nfs-mysql-master # 名字随意取，但是最好有意义，且只能是小写
spec:
  capacity:
    storage: 5Gi # 最多占用磁盘空间
  accessModes:
    - ReadWriteOnce
  storageClassName: nfs # 可以随意取，但是一类存取卷最好一致，因为它相当于命名空间
  nfs:
    path: /root/data/mysql_master # NFS存储Server的文件夹地址，必须要存在，可以手动创建
    server: 172.16.26.3 # 作为NFS存储Server的IP地址
  persistentVolumeReclaimPolicy: Recycle # 回收策略：回收

```

**Master PVC**

```yaml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: pvc-nfs-mysql-master
spec:
  accessModes:
    - ReadWriteOnce
  storageClassName: nfs # 可以随意取，但是一类存取卷最好一致，因为它相当于命名空间
  resources:
    requests:
      storage: 5Gi # 申请申明存储大小

```

### 3.3.2. Slave MySQL的PV、PVC

```bash
// 在slave目录下分别新增slave的pv和pvc文件
vim slave/nfs-pv-slave.yaml
vim slave/nfs-pvc-slave.yaml

// 之后创建PV，PVC资源
kubectl create -f slave/nfs-pv-slave.yaml
kubectl create -f slave/nfs-pvc-slave.yaml

// 如果资源没创建对，可以通过下面的命令删除
kubectl delete pv pv-nfs-mysql-slave
kubectl delete pvc pvc-nfs-mysql-slave

// 利用命令查看资源是否创建成功
kubectl get pv,pvc
```

**Slave PV**

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-nfs-mysql-slave # 名字随意取，但是最好有意义，且只能是小写
spec:
  capacity:
    storage: 6Gi # 最多占用磁盘空间
  accessModes:
    - ReadWriteOnce
  storageClassName: nfs # 可以随意取，但是一类存取卷最好一致，因为它相当于命名空间
  nfs:
    path: /root/data/mysql_slave # NFS存储Server的文件夹地址，必须要存在，可以手动创建
    server: 172.16.26.3 # 作为NFS存储Server的IP地址
  persistentVolumeReclaimPolicy: Recycle # 回收策略：回收

```

**Slave PVC**

```yaml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: pvc-nfs-mysql-slave
spec:
  accessModes:
    - ReadWriteOnce
  storageClassName: nfs # 可以随意取，但是一类存取卷最好一致，因为它相当于命名空间
  resources:
    requests:
      storage: 6Gi # 申请申明存储大小

```

## 3.4. Master节点的ReplicationController和Service

### 3.4.1. 先要开启CentOS上的NFS服务

#### 3.4.1.1. 安装NFS文件工具包

```bash
// 安装NFS文件工具包
yum install -y nfs-utils
```

#### 3.4.1.2. 启动NFS服务

```bash
// 创建测试文件夹
mkdir -p /root/data/mysql_master
mkdir -p /root/data/mysql_slave


// 在nfs主节点运行下面命令，这是后把Master节点当做nfs主节点运行下面的命令。命令的意思是设置共享文件夹
echo "/root/data/ *(insecure,rw,sync,no_root_squash)" > /etc/exports

// 在Master nfs主节点启动RPC远程绑定，为了达到同步目录的作用，现在永久生效，开机启动
systemctl enable rpcbind --now
// 现在启动，开机启动nfs服务器
systemctl enable nfs-server --now
// 使配置生效
exportfs -r
```

#### 3.4.1.3. 检测NFS服务是否可用

```bash
// 可以在任意几点上运行检测节点暴露的文件夹位置
showmount -e 172.16.26.3

// 信息正常的话会输入下面的信息，这样可以证明172.16.26.3是可以被挂在数据，被作为NFS文件系统的
[root@k8s-single-node ~]# showmount -e 172.16.26.3
Export list for 172.16.26.3:
/root/data *
```

### 3.4.2. ReplicationController

```bash
// 在master目录下新增Master节点的ReplicationController文件
vim master/mysql-master-rc.yml

// 之后创建ReplicationController资源
kubectl create -f master/mysql-master-rc.yml

// 如果需要，可以删除创建ReplicationController资源
kubectl delete rc mysql-master
kubectl delete pod recycler-for-pv-nfs-mysql-master
kubectl delete pod recycler-for-pv-nfs-mysql-slave

// 利用命令查看资源是否创建成功，rc是ReplicationController的缩写
kubectl get rc,pod
```

**Master ReplicationController内容**

```yaml
apiVersion: v1
kind: ReplicationController
metadata:
  name: mysql-master
spec:
  replicas: 1
  selector:
    name: mysql-master
  template:
    metadata:
      labels:
        name: mysql-master
    spec:
      containers:
        - name: mysql-master
          args:
            - --default-time-zone=+08:00
            - --character-set-server=utf8mb4
            - --collation-server=utf8mb4_general_ci
            - --innodb_flush_log_at_trx_commit=1
          image: yanglin520/mysql8-master:8.0.27 # 这是上面构建并上传到Docker的镜像
          volumeMounts:
            - mountPath: /var/lib/mysql
              name: mysql-master-data
          ports:
            - containerPort: 3306
          env:
            - name: MYSQL_USER
              value: "mysql"
            - name: MYSQL_PASSWORD
              value: "mysql"
            - name: MYSQL_ROOT_PASSWORD
              value: "root"
            - name: MYSQL_ALLOW_EMPTY_PASSWORD
              value: "TRUE"
            - name: MYSQL_REPLICATION_USER
              value: "repl"
            - name: MYSQL_REPLICAITON_PASSWORD
              value: "repl"
      volumes:
        - name: mysql-master-data
          persistentVolumeClaim:
            claimName: pvc-nfs-mysql-master

```

### 3.4.3. Service

```bash
// 在master目录下新增Master节点的Service资源文件
vim master/mysql-master-svc.yml

// 之后创建Service资源
kubectl create -f master/mysql-master-svc.yml

// 删除Service资源命令
kubectl delete svc mysql-master

// 利用命令查看资源是否创建成功，svc是Service的缩写
kubectl get svc,pod
```

**Master Service内容**

```yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    name: mysql-master
  name: mysql-master
spec:
  type: NodePort
  selector:
    name: mysql-master
  ports:
    - port: 3306
      targetPort: 3306
      protocol: TCP
      name: http
      nodePort: 30066

```

### 3.4.4. 验证Master连接

```bash
// 利用下面命令查看，master-mysql的pod是否启动好，启动好后通过客户端连接测试
kubectl get svc,pod

// 通过root的密码三个节点都可以连接，root的账号密码
172.16.26.3:30066
172.16.26.4:30066
172.16.26.5:30066
```

用本地的客户端连接成功，通过三个节点的IP地址:30066都可以访问。

之前一直没有连接上，是因为上面的Service配置中对应的端口不对，应该是**targetPort: 3306**

## 3.5. Slave节点的ReplicationController和Service

### 3.5.1. 先要开启CentOS上的NFS服务

上面构建Master的时候已经开启了，可以通过下面的命令看是否数据文件夹同步成功

```bash
showmount -e 172.16.26.3
```

### 3.5.2. ReplicationController

```bash
// 在slave目录下新增Slave节点的ReplicationController文件
vim slave/mysql-slave-rc.yml

// 之后创建ReplicationController资源
kubectl create -f slave/mysql-slave-rc.yml

// 如果需要，可以删除创建ReplicationController资源
kubectl delete rc mysql-slave
kubectl delete pod recycler-for-pv-nfs-mysql-master
kubectl delete pod recycler-for-pv-nfs-mysql-slave

// 利用命令查看资源是否创建成功，rc是ReplicationController的缩写
kubectl get rc,pod
```

**Slave ReplicationController内容**

```yaml
apiVersion: v1
kind: ReplicationController
metadata:
  name: mysql-slave
spec:
  replicas: 1
  selector:
    name: mysql-slave
  template:
    metadata:
      labels:
        name: mysql-slave
    spec:
      containers:
        - name: mysql-slave
          args:
            - --default-time-zone=+08:00
            - --character-set-server=utf8mb4
            - --collation-server=utf8mb4_general_ci
            - --innodb_flush_log_at_trx_commit=1
          image: yanglin520/mysql8-slave:8.0.27 # 这是上面构建并上传到Docker的镜像
          volumeMounts:
            - mountPath: /var/lib/mysql
              name: mysql-slave-data
          ports:
            - containerPort: 3306
          env:
            - name: MYSQL_USER
              value: "mysql"
            - name: MYSQL_PASSWORD
              value: "mysql"
            - name: MYSQL_ROOT_PASSWORD
              value: "root"
            - name: MYSQL_ALLOW_EMPTY_PASSWORD
              value: "TRUE"
            - name: MYSQL_REPLICATION_USER
              value: "repl"
            - name: MYSQL_REPLICAITON_PASSWORD
              value: "repl"
            - name: MYSQL_MASTER_SERVICE_HOST
              value: "mysql-master"
      volumes:
        - name: mysql-slave-data
          persistentVolumeClaim:
            claimName: pvc-nfs-mysql-slave

```

### 3.5.3. Service

```bash
// 在slave目录下新增Slave节点的Service资源文件
vim slave/mysql-slave-svc.yml

// 之后创建Service资源
kubectl create -f slave/mysql-slave-svc.yml

// 删除Service资源命令
kubectl delete svc mysql-slave

// 利用命令查看资源是否创建成功，svc是Service的缩写
kubectl get svc,pod
```

**Master Service内容**

```yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    name: mysql-slave
  name: mysql-slave
spec:
  type: NodePort
  selector:
    name: mysql-slave
  ports:
    - port: 3306
      targetPort: 3306
      protocol: TCP
      name: http
      nodePort: 30067

```

### 3.5.4. 验证Slave连接

```bash
// 利用下面命令查看，master-mysql的pod是否启动好，启动好后通过客户端连接测试
kubectl get svc,pod

// 通过root的密码三个节点都可以连接，root的账号密码，用本地的客户端连接成功！
172.16.26.3:30067
172.16.26.4:30067
172.16.26.5:30067
```

### 3.5.5. 需要修正MySQL构建镜像的配置

修改root和repl用户的加密方式，重新构建master mysql镜像

```
// docker-entrypoint.sh，Master
		docker_process_sql --database=mysql <<<"ALTER USER '$MYSQL_REPLICATION_USER'@'%' IDENTIFIED WITH mysql_native_password BY '$MYSQL_REPLICATION_PASSWORD' PASSWORD EXPIRE NEVER ;"
		docker_process_sql --database=mysql <<<"ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY '$MYSQL_ROOT_PASSWORD' PASSWORD EXPIRE NEVER ;"
```

# 4. Kubernetes简介

## 4.1. K8S概念和特性

### 4.1.1. 部署发展历程

我们的项目部署也在经历下面的这样一个历程

> 传统部署 -> 虚拟化部署时代 -> 容器部署时代

![image-20201122104102715](./images/Kubernetes-k8s-9.png)

- **传统部署时代**：早期，组织在物理服务器上运行应用程序。无法为物理服务器中的应用程序定义资源边界，这会导致资源分配问题。例如，如果在物理服务器上运行多个应用程序，则可能会出现-一个应用程序占用大部分资源的情况，结果可能导致其他应用程序的性能下降。一种解决方案是在不同的物理服务器上运行每个应用程序，但是由于资源利用不足而无法扩展，并且组织维护许多物理服务器的成本很高。
- **虚拟化部署时代**：作为解决方案，引入了虚拟化功能，它允许您在单个物理服务器的CPU.上运行多个虚拟机（VM）。虚拟化功能允许应用程序在VM之间隔离，并提供安全级别，因为一一个应用程序的信息不能被另一应用程序自由地访问。因为虚拟化可以轻松地添加或更新应用程序、降低硬件成本等等，所以虚拟化可以更好地利用物理服务器中的资源，并可以实现更好的可伸缩性。每个VM是一台完整的计算机，在虚拟化硬件之上运行所有组件，包括其自己的操作系统。
- **容器部署时代**：容器类似于VM，但是它们具有轻量级的隔离属性，可以在应用程序之间共享操作系统 （OS），因此，容器被认为是轻量级的。容器与VM类似，具有自己的文件系统、CPU、内存、进程空间等。由于它们与基础架构分离，因此可以跨云和OS分发进行移植。

容器因具有许多优势而变得流行起来。下面列出了容器的一些好处：

- 敏捷应用程序的创建和部署：与使用VM镜像相比，提高了容器镜像创建的简便性和效率。
- 持续开发、集成和部署：通过简单的回滚（由于镜像不可变性），提供可靠且频繁的容器镜像构建和部署。
- 关注开发与运维的分离：在构建/时而不是在部署时创建应用程序容器镜像，将应用程序与基础架构分离。
- 可观察性：不仅可以显示操作系统级别的信息和指标，还可以显示应用程序的运行状况和其他指标信号。
- 跨开发、测试和生产的环境一致性：在便携式计算机上与在云中相同地运行。
- 云和操作系统分发的可移植性：可在Ubuntu、RHEL、RHEL、CoreOS、本地、Google Kubernetes Engine和其它任何其它地方运行。
- 以应用程序为中心的管理：提高抽象级别，从在虚拟硬件上运行OS到使用逻辑资源在OS上运行应用程序。
- 松散耦合、分布式、弹性、解放的微服务：应用程序被分解成较小的独立部分，并且可以动态部署和管理-而不是在一台大型单机上器体运行。
- 资源隔离：可预测的应用程序性能。

### 4.1.2. K8S概述

kubernetes，简称K8s，是用8 代替8 个字符“ubernete”而成的缩写。是一个开源的，用于管理云平台中多个主机上的容器化的应用，Kubernetes 的目标是让部署容器化的应用简单并且高效（powerful），Kubernetes 提供了应用部署，规划，更新，维护的一种机制。

传统的应用部署方式是通过插件或脚本来安装应用。这样做的缺点是应用的运行、配置、管理、所有生存周期将与当前操作系统绑定，这样做并不利于应用的升级更新/回滚等操作，当然也可以通过创建虚拟机的方式来实现某些功能，但是虚拟机非常重，并不利于可移植性。

新的方式是通过部署容器方式实现，每个容器之间互相隔离，每个容器有自己的文件系统，容器之间进程不会相互影响，能区分计算资源。相对于虚拟机，容器能快速部署，由于容器与底层设施、机器文件系统解耦的。

> 总结：
>
> - K8s是谷歌在2014年发布的容器化集群管理系统
> - 使用k8s进行容器化应用部署
> - 使用k8s利于应用扩展
> - k8s目标实施让部署容器化应用更加简洁和高效

### 4.1.3. K8S概述

Kubernetes 是一个轻便的和可扩展的开源平台，用于管理容器化应用和服务。通过Kubernetes 能够进行应用的自动化部署和扩缩容。在Kubernetes 中，会将组成应用的容器组合成一个逻辑单元以更易管理和发现。

Kubernetes 积累了作为Google 生产环境运行工作负载15 年的经验，并吸收了来自于社区的最佳想法和实践。

### 4.1.4. K8S功能

#### 4.1.4.1. 自动装箱

基于容器对应用运行环境的资源配置要求自动部署应用容器

#### 4.1.4.2. 自我修复(自愈能力)

当容器失败时，会对容器进行重启

当所部署的Node节点有问题时，会对容器进行重新部署和重新调度

当容器未通过监控检查时，会关闭此容器直到容器正常运行时，才会对外提供服务

如果某个服务器上的应用不响应了，Kubernetes会自动在其它的地方创建一个

#### 4.1.4.3. 水平扩展

通过简单的命令、用户UI 界面或基于CPU 等资源使用情况，对应用容器进行规模扩大或规模剪裁

> 当我们有大量的请求来临时，我们可以增加副本数量，从而达到水平扩展的效果

当黄色应用过度忙碌，会来扩展一个应用

![image-20201122112301750](./images/Kubernetes-k8s-10.png)

#### 4.1.4.4. 服务发现

用户不需使用额外的服务发现机制，就能够基于Kubernetes 自身能力实现服务发现和负载均衡

> 对外提供统一的入口，让它来做节点的调度和负载均衡， 相当于微服务里面的网关

![image-20200928101711968](./images/Kubernetes-k8s-11.png)

#### 4.1.4.5. 滚动更新

可以根据应用的变化，对应用容器运行的应用，进行一次性或批量式更新

> 添加应用的时候，不是加进去就马上可以进行使用，而是需要判断这个添加进去的应用是否能够正常使用

#### 4.1.4.6. 版本回退

可以根据应用部署情况，对应用容器运行的应用，进行历史版本即时回退

> 类似于Git中的回滚

#### 4.1.4.7. 密钥和配置管理

在不需要重新构建镜像的情况下，可以部署和更新密钥和应用配置，类似热部署。

#### 4.1.4.8. 存储编排

自动实现存储系统挂载及应用，特别对有状态应用实现数据持久化非常重要

存储系统可以来自于本地目录、网络存储(NFS、Gluster、Ceph 等)、公共云存储服务

#### 4.1.4.9. 批处理

提供一次性任务，定时任务；满足批量数据处理和分析的场景

## 4.2. K8S架构组件

### 4.2.1. 完整架构图

![image-20200928103059652](./images/Kubernetes-k8s-12.png)

![image-20200928110124821](./images/Kubernetes-k8s-13.png)

### 4.2.2. 架构细节

K8S架构主要包含两部分：Master（主控节点）和 node（工作节点）

master节点架构图

![image-20201122113057343](./images/Kubernetes-k8s-14.png)

Node节点架构图

![image-20201122155629990](./images/Kubernetes-k8s-15.png)

k8s 集群控制节点，对集群进行调度管理，接受集群外用户去集群操作请求；

- **master**：主控节点
  - API Server：集群统一入口，以restful风格进行操作，同时交给etcd存储
    - 提供认证、授权、访问控制、API注册和发现等机制
  - scheduler：节点的调度，选择node节点应用部署
  - controller-manager：处理集群中常规后台任务，一个资源对应一个控制器
  - etcd：存储系统，用于保存集群中的相关数据
- **Work node**：工作节点
  - Kubelet：master派到node节点代表，管理本机容器
    - 一个集群中每个节点上运行的代理，它保证容器都运行在Pod中
    - 负责维护容器的生命周期，同时也负责Volume(CSI，Container Storage Interface) 和 网络(CNI)的管理
  - kube-proxy：提供网络代理，负载均衡等操作
- 容器运行环境【**Container Runtime**】
  - 容器运行环境是负责运行容器的软件
  - Kubernetes支持多个容器运行环境：Docker、containerd、cri-o、rktlet以及任何实现Kubernetes CRI (容器运行环境接口) 的软件。
- fluentd：是一个守护进程，它有助于提升集群层面日志

## 4.3. K8S核心概念

### 4.3.1. Pod

- Pod是K8s中最小的单元
- 一组容器的集合
- 共享网络【一个Pod中的所有容器共享同一网络】
- 生命周期是短暂的（服务器重启后，就找不到了）

### 4.3.2. Volume

- 声明在Pod容器中可访问的文件目录
- 可以被挂载到Pod中一个或多个容器指定路径下
- 支持多种后端存储抽象【本地存储、分布式存储、云存储】

### 4.3.3. Controller

- 确保预期的pod副本数量【ReplicaSet】
- 无状态应用部署【Deployment】
  - 无状态就是指，不需要依赖于网络或者ip
- 有状态应用部署【StatefulSet】
  - 有状态需要特定的条件
- 确保所有的node运行同一个pod 【DaemonSet】
- 一次性任务和定时任务【Job和CronJob】

### 4.3.4. Deployment

- 定义一组Pod副本数目，版本等
- 通过控制器【Controller】维持Pod数目【自动回复失败的Pod】
- 通过控制器以指定的策略控制版本【滚动升级、回滚等】

### 4.3.5. Service

- 定义一组pod的访问规则
- Pod的负载均衡，提供一个或多个Pod的稳定访问地址
- 支持多种方式【ClusterIP、NodePort、LoadBalancer】

可以用来组合pod，同时对外提供服务

### 4.3.6. Label

label：标签，用于对象资源查询，筛选

![image-20201122161713638](./images/Kubernetes-k8s-16.png)

### 4.3.7. Namespace

命名空间，逻辑隔离

- 一个集群内部的逻辑隔离机制【鉴权、资源】
- 每个资源都属于一个namespace
- 同一个namespace所有资源不能重复
- 不同namespace可以资源名重复

### 4.3.8. API

我们通过Kubernetes的API来操作整个集群

同时我们可以通过 kubectl 、ui、curl 最终发送 http + json/yaml 方式的请求给API Server，然后控制整个K8S集群，K8S中所有的资源对象都可以**采用 yaml 或 json 格式的文件定义或描述**

## 4.4. 完整流程

![image-20201122163512535](./images/Kubernetes-k8s-17.png)

- 通过Kubectl提交一个创建RC（Replication Controller）的请求，该请求通过APlserver写入etcd
- 此时Controller Manager通过API Server的监听资源变化的接口监听到此RC事件
- 分析之后，发现当前集群中还没有它所对应的Pod实例

接下来就如上图显示的流程：

- 于是根据RC里的Pod模板定义一个生成Pod对象，通过APIServer写入etcd
- 此事件被Scheduler发现，它立即执行执行一个复杂的调度流程，为这个新的Pod选定一个落户的Node，然后通过API Server将这一结果写入etcd中
- 目标Node上运行的Kubelet进程通过APiserver监测到这个"新生的Pod.并按照它的定义，启动该Pod并任劳任怨地负责它的下半生，直到Pod的生命结束
- 随后，我们通过Kubectl提交一个新的映射到该Pod的Service的创建请求
- ControllerManager通过Label标签查询到关联的Pod实例，然后生成Service的Endpoints信息，并通过APIServer写入到etcd中，
- 接下来，所有Node上运行的Proxy进程通过APIServer查询并监听Service对象与其对应的Endponts信息，建立一个软件方式的负载均衡器来实现Service访问到后端Pod的流量转发功能

# 5. Kubernetes集群管理工具kubectl

## 5.1. 概述

kubectl是Kubernetes集群的命令行工具，通过kubectl能够对集群本身进行管理，并能够在集群上进行容器化应用的安装和部署

## 5.2. 命令格式

命令格式如下

```
kubectl [command] [type] [name] [flags]
```

参数

- command：指定要对资源执行的操作，例如create、get、describe、delete
- type：指定资源类型，资源类型是大小写敏感的，开发者能够以单数 、复数和缩略的形式

例如：

```
kubectl get pod pod1
kubectl get pods pod1
kubectl get po pod1
```

- name：指定资源的名称，名称也是大小写敏感的，如果省略名称，则会显示所有的资源，例如

```
kubectl get pods
```

- flags：指定可选的参数，例如，可用 -s 或者 -server参数指定Kubernetes API server的地址和端口

## 5.3. 常见命令

### 5.3.1. kubectl help 获取更多信息

通过 help命令，能够获取帮助信息

```bash
# 获取kubectl的命令
kubectl --help

# 获取某个命令的介绍和使用
kubectl get --help
```

### 5.3.2. 基础命令

常见的基础命令

| 命令    | 介绍                                           |
| ------- | ---------------------------------------------- |
| create  | 通过文件名或标准输入创建资源                   |
| expose  | 将一个资源公开为一个新的Service                |
| run     | 在集群中运行一个特定的镜像                     |
| set     | 在对象上设置特定的功能                         |
| get     | 显示一个或多个资源                             |
| explain | 文档参考资料                                   |
| edit    | 使用默认的编辑器编辑一个资源                   |
| delete  | 通过文件名，标准输入，资源名称或标签来删除资源 |

### 5.3.3. 部署命令

| 命令           | 介绍                                               |
| -------------- | -------------------------------------------------- |
| rollout        | 管理资源的发布                                     |
| rolling-update | 对给定的复制控制器滚动更新                         |
| scale          | 扩容或缩容Pod数量，Deployment、ReplicaSet、RC或Job |
| autoscale      | 创建一个自动选择扩容或缩容并设置Pod数量            |

### 5.3.4. 集群管理命令

| 命令         | 介绍                           |
| ------------ | ------------------------------ |
| certificate  | 修改证书资源                   |
| cluster-info | 显示集群信息                   |
| top          | 显示资源(CPU/M)                |
| cordon       | 标记节点不可调度               |
| uncordon     | 标记节点可被调度               |
| drain        | 驱逐节点上的应用，准备下线维护 |
| taint        | 修改节点taint标记              |

### 5.3.5. 故障和调试命令

| 命令         | 介绍                                                         |
| ------------ | ------------------------------------------------------------ |
| describe     | 显示特定资源或资源组的详细信息                               |
| logs         | 在一个Pod中打印一个容器日志，如果Pod只有一个容器，容器名称是可选的 |
| attach       | 附加到一个运行的容器                                         |
| exec         | 执行命令到容器                                               |
| port-forward | 转发一个或多个                                               |
| proxy        | 运行一个proxy到Kubernetes API Server                         |
| cp           | 拷贝文件或目录到容器中                                       |
| auth         | 检查授权                                                     |

### 5.3.6. 其它命令

| 命令         | 介绍                                                |
| ------------ | --------------------------------------------------- |
| apply        | 通过文件名或标准输入对资源应用配置                  |
| patch        | 使用补丁修改、更新资源的字段                        |
| replace      | 通过文件名或标准输入替换一个资源                    |
| convert      | 不同的API版本之间转换配置文件                       |
| label        | 更新资源上的标签                                    |
| annotate     | 更新资源上的注释                                    |
| completion   | 用于实现kubectl工具自动补全                         |
| api-versions | 打印受支持的API版本                                 |
| config       | 修改kubeconfig文件（用于访问API，比如配置认证信息） |
| help         | 所有命令帮助                                        |
| plugin       | 运行一个命令行插件                                  |
| version      | 打印客户端和服务版本信息                            |

### 5.3.7. 目前使用的命令

```bash
# 创建一个nginx镜像
kubectl create deployment nginx --image=nginx

# 对外暴露端口
kubectl expose deployment nginx --port=80 --type=NodePort

# 查看资源
kubectl get pod, svc
```

# 6. Kubernetes集群YAML文件详解

## 6.1. 概述

k8s 集群中对资源管理和资源对象编排部署都可以通过声明样式（YAML）文件来解决，也就是可以把需要对资源对象操作编辑到YAML 格式文件中，我们把这种文件叫做资源清单文件，通过kubectl 命令直接使用资源清单文件就可以实现对大量的资源对象进行编排部署了。

YAML文件：就是资源清单文件，用于资源编排

## 6.2. YAML文件组成部分

主要分为了两部分，一个是**控制器的定义**和**被控制的对象**

### 6.2.1. 控制器的定义

![image-20201114110444032](./images/Kubernetes-k8s-18.png)

### 6.2.2. 被控制的对象

包含一些 镜像，版本、端口等

![image-20201114110600165](./images/Kubernetes-k8s-19.png)

### 6.2.3. 属性说明

在一个YAML文件的控制器定义中，有很多属性名称

| 属性名称   | 介绍                                                         |
| ---------- | ------------------------------------------------------------ |
| apiVersion | API版本（kubectl api-versions获取所有）                      |
| kind       | 资源类型（kubectl api-resources获取所有资源类型，以及对应的版本） |
| metadata   | 资源元数据                                                   |
| spec       | 资源规格                                                     |
| replicas   | 副本数量                                                     |
| selector   | 标签选择器                                                   |
| template   | Pod模板                                                      |
| metadata   | Pod元数据                                                    |
| spec       | Pod规格                                                      |
| containers | 容器配置                                                     |

## 6.3. 如何快速编写YAML文件

一般来说，我们很少自己手写YAML文件，因为这里面涉及到了很多内容，我们一般都会借助工具来创建

### 6.3.1. 使用kubectl create命令

这种方式一般用于资源没有部署的时候，我们可以直接创建一个YAML配置文件

```bash
# 尝试运行,并不会真正的创建镜像
kubectl create deployment web --image=nginx -o yaml --dry-run
```

或者我们可以输出到一个文件中

```bash
kubectl create deployment web --image=nginx -o yaml --dry-run > hello.yaml
```

然后我们就在文件中直接修改即可

### 6.3.2. 使用kubectl get命令导出yaml文件

可以首先查看一个目前已经部署的镜像

```bash
kubectl get deploy -A
```

然后我们导出 nginx的配置

```bash
kubectl get deploy nginx -o=yaml --export > nginx.yaml
```

然后会生成一个 `nginx.yaml` 的配置文件

# 7. Kubernetes核心技术Pod

## 7.1. Pod概述

Pod是K8S系统中可以创建和管理的最小单元，是资源对象模型中由用户创建或部署的最小资源对象模型，也是在K8S上运行容器化应用的资源对象，其它的资源对象都是用来支撑或者扩展Pod对象功能的，比如控制器对象是用来管控Pod对象的，Service或者Ingress资源对象是用来暴露Pod引用对象的，PersistentVolume资源对象是用来为Pod提供存储等等，K8S不会直接处理容器，而是Pod，Pod是由一个或多个container组成。

Pod是Kubernetes的最重要概念，每一个Pod都有**一个特殊的被称为 “根容器”的Pause容器**。Pause容器对应的镜像属于Kubernetes平台的一部分，除了Pause容器，每个Pod还包含**一个或多个紧密相关的用户业务容器**。

![image-20201114185528215](./images/Kubernetes-k8s-20.png)

### 7.1.1. Pod基本概念

- 最小部署的单元
- Pod里面是由一个或多个容器组成【一组容器的集合】
- 一个pod中的容器是共享网络命名空间
- Pod是短暂的
- 每个Pod包含一个或多个紧密相关的用户业务容器

### 7.1.2. Pod存在的意义

- 创建容器使用docker，一个docker对应一个容器，一个容器运行一个应用进程
- Pod是多进程设计，运用多个应用程序，也就是一个Pod里面有多个容器，而一个容器里面运行一个应用程序

- Pod的存在是为了亲密性应用
  - 两个应用之间进行交互
  - 网络之间的调用【通过127.0.0.1 或 socket】
  - 两个应用之间需要频繁调用

Pod是在K8S集群中运行部署应用或服务的最小单元，它是可以支持多容器的。Pod的设计理念是支持多个容器在一个Pod中共享网络地址和文件系统，可以通过进程间通信和文件共享这种简单高效的方式组合完成服务。同时Pod对多容器的支持是K8S中最基础的设计理念。在生产环境中，通常是由不同的团队各自开发构建自己的容器镜像，在部署的时候组合成一个微服务对外提供服务。

目前K8S的业务主要可以分为以下几种

- 长期伺服型：long-running
- 批处理型：batch
- 节点后台支撑型：node-daemon
- 有状态应用型：stateful application

上述的几种类型，分别对应的小机器人控制器为：Deployment、Job、DaemonSet 和 StatefulSet (后面将介绍控制器)

## 7.2. Pod实现机制

主要有以下两大机制

- 共享网络
- 共享存储

### 7.2.1. 共享网络

容器本身之间相互隔离的，一般是通过 **namespace** 和 **group** 进行隔离，那么Pod里面的容器如何实现通信？

- 首先需要满足前提条件，也就是容器都在同一个**namespace**之间



关于Pod实现原理，首先会在Pod会创建一个根容器： `pause容器`，然后我们在创建业务容器 【nginx，redis 等】，在我们创建业务容器的时候，会把它添加到 `info容器` 中

而在 `info容器` 中会独立出 ip地址，mac地址，port 等信息，然后实现网络的共享

![image-20201114190913859](./images/Kubernetes-k8s-21.png)

完整步骤如下

- 通过 Pause 容器，把其它业务容器加入到Pause容器里，让所有业务容器在同一个名称空间中，可以实现网络共享

### 7.2.2. 共享存储

Pod持久化数据，专门存储到某个地方中，使用 Volumn数据卷进行共享存储

![image-20201114193124160](./images/Kubernetes-k8s-22.png)

## 7.3. Pod镜像拉取策略

我们以具体实例来说，拉取策略就是 `imagePullPolicy`

![image-20201114193605230](./images/Kubernetes-k8s-23.png)

拉取策略主要分为了以下几种

- IfNotPresent：默认值，镜像在宿主机上不存在才拉取
- Always：每次创建Pod都会重新拉取一次镜像
- Never：Pod永远不会主动拉取这个镜像

## 7.4. Pod资源限制

也就是我们Pod在进行调度的时候，可以对调度的资源进行限制。

例如我们限制 Pod调度是使用的资源是2C-4G，那么在调度对应的node节点时，只会占用对应的资源，对于不满足资源的节点，将不会进行调度

![image-20201114194057920](./images/Kubernetes-k8s-24.png)

### 7.4.1. 示例

我们在下面的地方进行资源的限制

![image-20201114194245517](./images/Kubernetes-k8s-25.png)

这里分了两个部分

- request：表示调度所需的资源
- limits：表示最大所占用的资源

## 7.5. Pod重启机制

因为Pod中包含了很多个容器，假设某个容器出现问题了，那么就会触发Pod重启机制

![image-20201114194722125](./images/Kubernetes-k8s-26.png)

重启策略主要分为以下三种

- Always：当容器终止退出后，总是重启容器，默认策略【nginx等，需要不断提供服务】
- OnFailure：当容器异常退出（退出状态码非0）时，才重启容器。
- Never：当容器终止退出，从不重启容器 【批量任务】

## 7.6. Pod健康检查

通过容器检查，原来我们使用下面的命令来检查

```
kubectl get pod
```

但是有的时候，程序可能出现了 **Java** 堆内存溢出，程序还在运行，但是不能对外提供服务了，这个时候就不能通过 容器检查来判断服务是否可用了

这个时候就可以使用应用层面的检查

```bash
# 存活检查，如果检查失败，将杀死容器，根据Pod的设置的restartPolicy参数【重启策略】来操作
livenessProbe

# 就绪检查，如果检查失败，Kubernetes会把Pod从Service endpoints中【剔除】
readinessProbe
```

![image-20201114195807564](./images/Kubernetes-k8s-27.png)

Probe支持以下三种检查方式

- http Get：发送HTTP请求，返回200 - 400范围状态码为成功
- exec：执行Shell命令返回状态码是0为成功
- tcpSocket：发起TCP Socket建立成功

## 7.7. Pod调度策略

### 7.7.1. 创建Pod流程

- 首先创建一个pod，然后创建一个API Server 和 etcd【把创建出来的信息存储在etcd中】
- 然后创建 Scheduler，监控API Server是否有新的Pod，如果有的话，会通过调度算法，把pod调度某个node上
- 在node节点，会通过kubelet的apiserver读取etcd拿到分配在当前node节点上的pod，然后通过docker创建容器

![image-20201114201611308](./images/Kubernetes-k8s-28.png)

### 7.7.2. 影响Pod调度的属性

Pod资源限制对Pod的调度会有影响

#### 7.7.2.1. 根据request找到足够node节点进行调度

![image-20201114194245517](./images/Kubernetes-k8s-29.png)

#### 7.7.2.2. 节点选择器标签影响Pod调度

![image-20201114202456151](./images/Kubernetes-k8s-30.png)

关于节点选择器，其实就是有两个环境，然后环境之间所用的资源配置不同

![image-20201114202643905](./images/Kubernetes-k8s-31.png)

我们可以通过以下命令，给我们的节点新增标签，然后节点选择器就会进行调度了

```
kubectl label node node1 env_role=prod
kubectl label nodes <node-name> <label-key>=<label-value>
```

#### 7.7.2.3. 节点亲和性

节点亲和性 **nodeAffinity** 和 之前nodeSelector 基本一样的，根据节点上标签约束来决定Pod调度到哪些节点上

- 硬亲和性：**约束条件必须满足**
- 软亲和性：尝试满足，不保证

![image-20201114203433939](./images/Kubernetes-k8s-32.png)

支持常用操作符：in、NotIn、Exists、Gt、Lt、DoesNotExists

反亲和性：就是和亲和性刚刚相反，如 NotIn、DoesNotExists等

## 7.8. 污点和污点容忍

### 7.8.1. 概述

nodeSelector 和 NodeAffinity，都是Prod调度到某些节点上，属于Pod的属性，是在调度的时候实现的。

Taint 污点：节点不做普通分配调度，是节点属性

### 7.8.2. 场景

- 专用节点【限制ip】，配备特定的节点
- 配置特定硬件的节点【固态硬盘】
- 基于Taint驱逐【在node1不放，在node2放】

### 7.8.3. 查看污点情况

```
kubectl describe node k8smaster | grep Taint
```

![image-20201114204124819](./images/Kubernetes-k8s-33.png)

污点值有三个

- NoSchedule：一定不被调度
- PreferNoSchedule：尽量不被调度【也有被调度的几率】
- NoExecute：不会调度，并且还会驱逐Node已有Pod，将Pod驱逐到其他node中

### 7.8.4. 未节点添加污点

```
kubectl taint node [node] key=value:污点的三个值
```

举例：

```bash
# 其中的env_role=是任意的键值对，yes=NoSchedule是污点值
kubectl taint node k8snode1 env_role=yes:NoSchedule
```

### 7.8.5. 删除污点

```
kubectl taint node k8snode1 env_role:NoSchedule-
```

![image-20201114210022883](./images/Kubernetes-k8s-34.png)

### 7.8.6. 演示

我们现在创建多个Pod，查看最后分配到Node上的情况

首先我们创建一个 nginx 的pod

```
kubectl create deployment web --image=nginx
```

然后使用命令查看

```
kubectl get pods -o wide
```

![image-20201114204917548](./images/Kubernetes-k8s-35.png)

我们可以非常明显的看到，这个Pod已经被分配到 k8snode1 节点上了

下面我们把pod复制5份，在查看情况pod情况

```
kubectl scale deployment web --replicas=5
```

我们可以发现，因为master节点存在污点的情况，所以节点都被分配到了 node1 和 node2节点上

![image-20201114205135282](./images/Kubernetes-k8s-36.png)

我们可以使用下面命令，把刚刚我们创建的pod都删除

```
kubectl delete deployment web
```

现在给了更好的演示污点的用法，我们现在给 node1节点打上污点

```
kubectl taint node k8snode1 env_role=yes:NoSchedule
```

然后我们查看污点是否成功添加

```
kubectl describe node k8snode1 | grep Taint
```

![image-20201114205516154](./images/Kubernetes-k8s-37.png)

然后我们在创建一个 pod

```bash
# 创建nginx pod
kubectl create deployment web --image=nginx
# 复制五次
kubectl scale deployment web --replicas=5
```

然后我们在进行查看

```
kubectl get pods -o wide
```

我们能够看到现在所有的pod都被分配到了 k8snode2上，因为刚刚我们给node1节点设置了污点

![image-20201114205654867](./images/Kubernetes-k8s-38.png)

最后我们可以删除刚刚添加的污点

```
kubectl taint node k8snode1 env_role:NoSchedule-
```

### 7.8.7. 污点容忍

污点容忍就是某个节点可能被调度，也可能不被调度

```yaml
# 如果 operator 是 Exists （此时容忍度不能指定 value），或者
# 如果 operator 是 Equal ，则它们的 value 应该相等

tolerations:
  - key: "key1"
    operator: "Equal"
    value: "value1"
    effect: "NoSchedule"
  - key: "key1"
    operator: "Equal"
    value: "value1"
    effect: "NoExecute"
```

# 8. Kubernetes核心技术-Controller

## 8.1. 什么是Controller

Controller是在集群上管理和运行容器的对象，Controller是实际存在的，Pod是虚拟机的

## 8.2. Pod和Controller的关系

Pod是通过Controller实现应用的运维，比如弹性伸缩，滚动升级等

Pod 和 Controller之间是**通过label标签**来建立关系，同时Controller又被称为控制器工作负载

![image-20201116092431237](./images/Kubernetes-k8s-39.png)

## 8.3. Deployment控制器应用

- Deployment控制器可以部署无状态应用
- 管理Pod和ReplicaSet
- 部署，滚动升级等功能
- 应用场景：web服务，微服务

Deployment表示用户对K8S集群的一次更新操作。

Deployment是一个比RS( Replica Set，RS) 应用模型更广的 API 对象，可以是创建一个新的服务，更新一个新的服务，也可以是滚动升级一个服务。

滚动升级一个服务，实际是创建一个新的RS，然后逐渐将新 RS 中副本数增加到理想状态，将旧RS中的副本数减少到0的复合操作。

这样一个复合操作用一个RS是不好描述的，所以用一个更通用的Deployment来描述。以K8S的发展方向，未来对所有长期伺服型的业务的管理，都会通过Deployment来管理。

## 8.4. Deployment部署应用

之前我们也使用Deployment部署过应用，如下代码所示

```
kubectrl create deployment web --image=nginx
```

但是上述代码不是很好的进行复用，因为每次我们都需要重新输入代码，所以我们都是通过YAML进行配置

但是我们可以尝试使用上面的代码创建一个镜像【只是尝试，不会创建】

```
kubectl create deployment web --image=nginx --dry-run -o yaml > nginx.yaml
```

然后输出一个yaml配置文件 `nginx.yml` ，配置文件如下所示

```
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: web
  name: web
spec:
  replicas: 1
  selector:
    matchLabels:
      app: web
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: web
    spec:
      containers:
      - image: nginx
        name: nginx
        resources: {}
status: {}
```

我们看到的 selector 和 label 就是我们Pod 和 Controller之间建立关系的桥梁

![image-20201116093638951](./images/Kubernetes-k8s-40.png)

### 8.4.1. 使用YAML创建Pod

通过刚刚的代码，我们已经生成了YAML文件，下面我们就可以使用该配置文件快速创建Pod镜像了

```bash
kubectl apply -f nginx.yaml
```

但是因为这个方式创建的，我们只能在集群内部进行访问，所以我们还需要对外暴露端口

```
kubectl expose deployment web --port=80 --type=NodePort --target-port=80 --name=web1
```

关于上述命令，有几个参数

- --port：就是我们内部的端口号
- --target-port：就是暴露外面访问的端口号
- --name：名称
- --type：类型

同理，我们一样可以导出对应的配置文件

```
kubectl expose deployment web --port=80 --type=NodePort --target-port=80 --name=web1 -o yaml > web1.yaml
```

## 8.5. 升级回滚和弹性伸缩

- 升级： 假设从版本为1.14 升级到 1.15 ，这就叫应用的升级【**升级可以保证服务不中断**】
- 回滚：从版本1.15 变成 1.14，这就叫应用的回滚
- 弹性伸缩：我们根据不同的业务场景，来改变Pod的数量对外提供服务，这就是弹性伸缩

### 8.5.1. 应用升级和回滚

首先我们先创建一个 1.14版本的Pod

```
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: web
  name: web
spec:
  replicas: 1
  selector:
    matchLabels:
      app: web
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: web
    spec:
      containers:
      - image: nginx:1.14
        name: nginx
        resources: {}
status: {}
```

我们先指定版本为1.14，然后开始创建我们的Pod

```
kubectl apply -f nginx.yaml
```

同时，我们使用docker images命令，就能看到我们成功拉取到了一个 1.14版本的镜像

![image-20201116105710966](./images/Kubernetes-k8s-41.png)

我们使用下面的命令，可以将nginx从 1.14 升级到 1.15

```
kubectl set image deployment web nginx=nginx:1.15
```

在我们执行完命令后，能看到升级的过程

![image-20201116105847069](./images/Kubernetes-k8s-42.png)

- 首先是开始的nginx 1.14版本的Pod在运行，然后 1.15版本的在创建
- 然后在1.15版本创建完成后，就会暂停1.14版本
- 最后把1.14版本的Pod移除，完成我们的升级

我们在下载 1.15版本，容器就处于ContainerCreating状态，然后下载完成后，就用 1.15版本去替换1.14版本了，这么做的好处就是：**升级可以保证服务不中断**

我们到我们的node2节点上，查看我们的 docker images;

![image-20201116111315000](./images/Kubernetes-k8s-43.png)

能够看到，我们已经成功拉取到了 1.15版本的nginx了

#### 8.5.1.1. 查看升级状态

下面可以，查看升级状态

```bash
kubectl rollout status deployment web
```

#### 8.5.1.2. 查看历史版本

我们还可以查看历史版本

```bash
kubectl rollout history deployment web
```

#### 8.5.1.3. 应用回滚

我们可以使用下面命令，完成回滚操作，也就是回滚到上一个版本

```
kubectl rollout undo deployment web
```

然后我们就可以查看状态

```
kubectl rollout status deployment web
```

同时我们还可以回滚到指定版本

```
kubectl rollout undo deployment web --to-revision=2
```

### 8.5.2. 弹性伸缩

弹性伸缩，也就是我们通过命令一下创建10个副本

```
kubectl scale deployment web --replicas=10
```

# 9. Kubernetes核心技术Service

## 9.1. 前言

要稳定地提供服务需要服务发现和负载均衡能力。服务发现完成的工作，是针对客户端访问的服务，找到对应的后端服务实例。在K8S集群中，客户端需要访问的服务就是Service对象。每个Service会对应一个集群内部有效的虚拟IP，集群内部通过虚拟IP访问一个服务。

在K8S集群中，微服务的负载均衡是由kube-proxy实现的。kube-proxy是k8s集群内部的负载均衡器。它是一个分布式代理服务器，在K8S的每个节点上都有一个；这一设计体现了它的伸缩性优势，需要访问服务的节点越多，提供负载均衡能力的kube-proxy就越多，高可用节点也随之增多。与之相比，我们平时在服务器端使用反向代理作负载均衡，还要进一步解决反向代理的高可用问题。

## 9.2. Service存在的意义

### 9.2.1. 防止Pod失联【服务发现】

因为Pod每次创建都对应一个IP地址，而这个IP地址是短暂的，每次随着Pod的更新都会变化，假设当我们的前端页面有多个Pod时候，同时后端也多个Pod，这个时候，他们之间的相互访问，就需要通过注册中心，拿到Pod的IP地址，然后去访问对应的Pod

### 9.2.2. 定义Pod访问策略【负载均衡】

页面前端的Pod访问到后端的Pod，中间会通过Service一层，而Service在这里还能做负载均衡，负载均衡的策略有很多种实现策略，例如：

- 随机
- 轮询
- 响应比

## 9.3. Pod和Service的关系

这里Pod 和 Service 之间还是根据 label 和 selector 建立关联的 【和Controller一样】

![image-20201117094142491](./images/Kubernetes-k8s-44.png)

我们在访问service的时候，其实也是需要有一个ip地址，这个ip肯定不是pod的ip地址，而是 虚拟IP（VIP，Virtual IP）

## 9.4. Service常用类型

Service常用类型有三种

- ClusterIp：集群内部访问
- NodePort：对外访问应用使用
- LoadBalancer：对外访问应用使用，公有云

### 9.4.1. 举例

我们可以导出一个文件包含service的配置信息

```
kubectl expose deployment web --port=80 --target-port=80 --dry-run -o yaml > service.yaml
```

service.yaml 如下所示

```
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    app: web
  name: web
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: web
status:
  loadBalancer: {}
```

如果我们没有做设置的话，默认使用的是第一种方式 ClusterIp，也就是只能在集群内部使用，我们可以添加一个type字段，用来设置我们的service类型

```
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    app: web
  name: web
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: web
  type: NodePort
status:
  loadBalancer: {}
```

修改完命令后，我们使用创建一个pod

```
kubectl apply -f service.yaml
```

然后能够看到，已经成功修改为 NodePort类型了，最后剩下的一种方式就是LoadBalanced：对外访问应用使用公有云

node一般是在内网进行部署，而外网一般是不能访问到的，那么如何访问的呢？

- 找到一台可以通过外网访问机器，安装nginx，反向代理
- 手动把可以访问的节点添加到nginx中

如果我们使用LoadBalancer，就会有负载均衡的控制器，类似于nginx的功能，就不需要自己添加到nginx上

# 10. Kubernetes控制器Controller详解

## 10.1. Statefulset

Statefulset主要是用来部署有状态应用

对于StatefulSet中的Pod，每个Pod挂载自己独立的存储，如果一个Pod出现故障，从其他节点启动一个同样名字的Pod，要挂载上原来Pod的存储继续以它的状态提供服务。

### 10.1.1. 无状态应用

我们原来使用 deployment，部署的都是无状态的应用，那什么是无状态应用？

- 认为Pod都是一样的
- 没有顺序要求
- 不考虑应用在哪个node上运行
- 能够进行随意伸缩和扩展

### 10.1.2. 有状态应用

上述的因素都需要考虑到

- 让每个Pod独立的
- 让每个Pod独立的，保持Pod启动顺序和唯一性
- 唯一的网络标识符，持久存储
- 有序，比如mysql中的主从

适合StatefulSet的业务包括数据库服务MySQL和PostgreSQL，集群化管理服务Zookeeper、etcd等有状态服务

StatefulSet的另一种典型应用场景是作为一种比普通容器更稳定可靠的模拟虚拟机的机制。传统的虚拟机正是一种有状态的产物，运维人员需要不断地维护它，容器刚开始流行时，我们用容器来模拟虚拟机使用，所有状态都保存在容器里，而这已被证明是非常不安全、不可靠的。

使用StatefulSet，Pod仍然可以通过漂移到不同节点提供高可用，而存储也可以通过外挂的存储来提供高可靠性，StatefulSet做的只是**将确定的Pod与确定的存储关联起来保证状态的连续性**。

### 10.1.3. 部署有状态应用

无头service， **ClusterIp：none**

这里就需要使用 StatefulSet部署有状态应用

![image-20201117202950336](./images/Kubernetes-k8s-45.png)

![image-20201117203130867](./images/Kubernetes-k8s-46.png)

然后通过查看pod，能否发现3个pod都有唯一的名称

![image-20201117203217016](./images/Kubernetes-k8s-47.png)

然后我们在查看service，发现是无头的service

![image-20201117203245641](./images/Kubernetes-k8s-48.png)

这里有状态的约定，肯定不是简简单单通过名称来进行约定，而是更加复杂的操作

- deployment：是有身份的，有唯一标识
- statefulset：根据主机名 + 按照一定规则生成域名

每个pod有唯一的主机名，并且有唯一的域名

- 格式：主机名称.service名称.名称空间.svc.cluster.local
- 举例：nginx-statefulset-0.default.svc.cluster.local

## 10.2. DaemonSet

DaemonSet 即后台支撑型服务，主要是用来部署守护进程

长期伺服型和批处理型的核心在业务应用，可能有些节点运行多个同类业务的Pod，有些节点上又没有这类的Pod运行；而后台支撑型服务的核心关注点在K8S集群中的节点(物理机或虚拟机)，要保证每个节点上都有一个此类Pod运行。节点可能是所有集群节点，也可能是通过 nodeSelector选定的一些特定节点。典型的后台支撑型服务包括：存储、日志和监控等。在每个节点上支撑K8S集群运行的服务。

守护进程在我们每个节点上，运行的是同一个pod，新加入的节点也同样运行在同一个pod里面

- 例子：在每个node节点安装数据采集工具

![image-20201117204430836](./images/Kubernetes-k8s-49.png)

## 10.3. Job和CronJob

- 一次性任务：一次性执行完就结束
- 定时任务：周期性执行

Job是K8S中用来控制批处理型任务的API对象。

批处理业务与长期伺服业务的主要区别就是批处理业务的运行有头有尾，而长期伺服业务在用户不停止的情况下永远运行。Job管理的Pod根据用户的设置把任务成功完成就自动退出了。成功完成的标志根据不同的 spec.completions 策略而不同：

单Pod型任务有一个Pod成功就标志完成；

定数成功行任务保证有N个任务全部成功；

工作队列性任务根据应用确定的全局成功而标志成功。

### 10.3.1. Job

Job也即一次性任务

![image-20201117205635945](./images/Kubernetes-k8s-50.png)

使用下面命令，能够看到目前已经存在的Job

```
kubectl get jobs
```

![image-20201117205948374](./images/Kubernetes-k8s-51.png)

在计算完成后，通过命令查看，能够发现该任务已经完成

![image-20201117210031725](./images/Kubernetes-k8s-52.png)

我们可以通过查看日志，查看到一次性任务的结果

```
kubectl logs pi-qpqff
```

![image-20201117210110343](./images/Kubernetes-k8s-53.png)

### 10.3.2. CronJob

定时任务，cronjob.yaml如下所示

![image-20201117210309069](./images/Kubernetes-k8s-54.png)

这里面的命令就是每个一段时间，这里是通过 cron 表达式配置的，通过 schedule字段

然后下面命令就是每个一段时间输出

我们首先用上述的配置文件，创建一个定时任务

```
kubectl apply -f cronjob.yaml
```

创建完成后，我们就可以通过下面命令查看定时任务

```
kubectl get cronjobs
```

我们可以通过日志进行查看

```
kubectl logs hello-1599100140-wkn79
```

然后每次执行，就会多出一个 pod

![image-20201117210751068](./images/Kubernetes-k8s-55.png)

## 10.4. 删除svc 和 statefulset

使用下面命令，可以删除我们添加的svc 和 statefulset

```
kubectl delete svc web

kubectl delete statefulset --all
```

## 10.5. Replication Controller

Replication Controller 简称 **RC**，是K8S中的复制控制器。RC是K8S集群中最早的保证Pod高可用的API对象。通过监控运行中的Pod来保证集群中运行指定数目的Pod副本。指定的数目可以是多个也可以是1个；少于指定数目，RC就会启动新的Pod副本；多于指定数目，RC就会杀死多余的Pod副本。

即使在指定数目为1的情况下，通过RC运行Pod也比直接运行Pod更明智，因为RC也可以发挥它高可用的能力，保证永远有一个Pod在运行。RC是K8S中较早期的技术概念，只适用于长期伺服型的业务类型，比如控制Pod提供高可用的Web服务。

### 10.5.1. Replica Set

Replica Set 检查 RS，也就是副本集。RS是新一代的RC，提供同样高可用能力，区别主要在于RS后来居上，能够支持更多种类的匹配模式。副本集对象一般不单独使用，而是作为Deployment的理想状态参数来使用

# 11. Kubernetes配置管理

## 11.1. Secret

Secret的主要作用就是**加密数据**，然后存在etcd里面，让Pod容器以挂载Volume方式进行访问

场景：用户名和密码进行加密

一般场景的是对某个字符串进行base64编码 进行加密

```
echo -n 'admin' | base64
```

![image-20201117212037668](./images/Kubernetes-k8s-56.png)

### 11.1.1. 变量形式挂载到Pod

- 创建secret加密数据的yaml文件 secret.yaml

![image-20201117212124476](./images/Kubernetes-k8s-57.png)

然后使用下面命令创建一个pod

```
kubectl create -f secret.yaml
```

通过get命令查看

```
kubectl get pods
```

然后我们通过下面的命令，进入到我们的容器内部

```
kubectl exec -it mysecret bash
```

然后我们就可以输出我们的值，这就是以变量的形式挂载到我们的容器中

```bash
# 输出用户
echo $SECRET_USERNAME
# 输出密码
echo $SECRET_PASSWORD
```

最后如果我们要删除这个Pod，就可以使用这个命令

```
kubectl delete -f secret.yaml
```

### 11.1.2. 数据卷形式挂载

首先我们创建一个 secret-val.yaml 文件

![image-20201118084321590](./images/Kubernetes-k8s-58.png)

然后创建我们的 Pod

```bash
# 根据配置创建容器
kubectl apply -f secret-val.yaml
# 进入容器
kubectl exec -it mypod bash
# 查看
ls /etc/foo
```

## 11.2. ConfigMap

ConfigMap作用是**存储不加密的数据**到etcd中，让Pod以变量或数据卷Volume挂载到容器中

应用场景：配置文件

### 11.2.1. 创建配置文件

首先我们需要创建一个配置文件 `redis.properties`

```
redis.port=127.0.0.1
redis.port=6379
redis.password=123456
```

### 11.2.2. 创建ConfigMap

我们使用命令创建configmap

```
kubectl create configmap redis-config --from-file=redis.properties
```

然后查看详细信息

```
kubectl describe cm redis-config
```

### 11.2.3. Volume数据卷形式挂载

首先我们需要创建一个 `cm.yaml`

![image-20201118085847424](./images/Kubernetes-k8s-59.png)

然后使用该yaml创建我们的pod

```bash
# 创建
kubectl apply -f cm.yaml
# 查看
kubectl get pods
```

最后我们通过命令就可以查看结果输出了

```
kubectl logs mypod
```

![image-20201118090712780](./images/Kubernetes-k8s-60.png)

### 11.2.4. 以变量的形式挂载Pod

首先我们也有一个 myconfig.yaml文件，声明变量信息，然后以ConfigMap创建

![image-20201118090911260](./images/Kubernetes-k8s-61.png)

然后我们就可以创建我们的配置文件

```bash
# 创建pod
kubectl apply -f myconfig.yaml
# 获取
kubectl get cm
```

![image-20201118091042287](./images/Kubernetes-k8s-62.png)

然后我们创建完该pod后，我们就需要在创建一个 config-var.yaml 来使用我们的配置信息

![image-20201118091249520](./images/Kubernetes-k8s-63.png)

最后我们查看输出

```
kubectl logs mypod
```

# 12. Kubernetes集群安全机制

## 12.1. 概述

当我们访问K8S集群时，需要经过三个步骤完成具体操作

- 认证
- 鉴权【授权】
- 准入控制

进行访问的时候，都需要经过 apiserver， apiserver做统一协调，比如门卫

- 访问过程中，需要证书、token、或者用户名和密码
- 如果访问pod需要serviceAccount

![image-20201118092356107](./images/Kubernetes-k8s-64.png)

### 12.1.1. 认证

对外不暴露8080端口，只能内部访问，对外使用的端口6443

客户端身份认证常用方式

- https证书认证，基于ca证书
- http token认证，通过token来识别用户
- http基本认证，用户名 + 密码认证

### 12.1.2. 鉴权

基于RBAC（Role-Based Access Control，基于角色的访问控制）进行鉴权操作

基于角色访问控制

### 12.1.3. 准入控制

就是准入控制器的列表，如果列表有请求内容就通过，没有的话，就拒绝

## 12.2. RBAC介绍

基于角色的访问控制，为某个角色设置访问内容，然后用户分配该角色后，就拥有该角色的访问权限

![image-20201118093949893](./images/Kubernetes-k8s-65.png)

k8s中有默认的几个角色

- role：特定命名空间访问权限
- ClusterRole：所有命名空间的访问权限

角色绑定

- roleBinding：角色绑定到主体
- ClusterRoleBinding：集群角色绑定到主体

主体

- user：用户
- group：用户组
- serviceAccount：服务账号

## 12.3. RBAC实现鉴权

- 创建命名空间

### 12.3.1. 创建命名空间

我们可以首先查看已经存在的命名空间

```
kubectl get namespace
```

![image-20201118094516426](./images/Kubernetes-k8s-66.png)

然后我们创建一个自己的命名空间 roledemo

```
kubectl create ns roledemo
```

### 12.3.2. 命名空间创建Pod

为什么要创建命名空间？因为如果不创建命名空间的话，默认是在default下

```bash
kubectl run nginx --image=nginx -n roledemo
```

### 12.3.3. 创建角色

我们通过rbac-role.yaml进行创建

![image-20201118094851338](./images/Kubernetes-k8s-67.png)

tip：这个角色只对pod 有 get、list权限

然后通过 yaml创建我们的role

```bash
# 创建
kubectl apply -f rbac-role.yaml
# 查看
kubectl get role -n roledemo
```

![image-20201118095141786](./images/Kubernetes-k8s-68.png)

### 12.3.4. 创建角色绑定

我们还是通过 role-rolebinding.yaml 的方式，来创建我们的角色绑定

![image-20201118095248052](./images/Kubernetes-k8s-69.png)

然后创建我们的角色绑定

```bash
# 创建角色绑定
kubectl apply -f rbac-rolebinding.yaml
# 查看角色绑定
kubectl get role, rolebinding -n roledemo
```

![image-20201118095357067](./images/Kubernetes-k8s-70.png)

### 12.3.5. 使用证书识别身份???

我们首先得有一个 rbac-user.sh 证书脚本

![image-20201118095541427](./images/Kubernetes-k8s-71.png)

![image-20201118095627954](./images/Kubernetes-k8s-72.png)

这里包含了**很多证书文件**????，在TSL目录下，需要复制过来

通过下面命令执行我们的脚本

```
./rbac-user.sh
```

最后我们进行测试

```
# 用get命令查看 pod 【有权限】
kubectl get pods -n roledemo
# 用get命令查看svc 【没权限】
kubectl get svc -n roledmeo
```

![image-20201118100051043](./images/Kubernetes-k8s-73.png)

# 13. Kubernetes核心技术Ingress

## 13.1. 前言

原来我们需要将端口号对外暴露，通过 ip + 端口号就可以进行访问

原来是使用Service中的NodePort来实现

- 在每个节点上都会启动端口
- 在访问的时候通过任何节点，通过ip + 端口号就能实现访问

但是NodePort还存在一些缺陷

- 因为端口不能重复，所以每个端口只能使用一次，一个端口对应一个应用
- 实际访问中都是用域名，根据不同域名跳转到不同端口服务中

## 13.2. Ingress和Pod关系

pod 和 ingress 是通过service进行关联的，而ingress作为统一入口，由service关联一组pod中

![image-20201118102637839](./images/Kubernetes-k8s-74.png)

- 首先service就是关联我们的pod
- 然后ingress作为入口，首先需要到service，然后发现一组pod
- 发现pod后，就可以做负载均衡等操作

## 13.3. Ingress工作流程

在实际的访问中，我们都是需要维护很多域名， a.com 和 b.com

然后不同的域名对应的不同的Service，然后service管理不同的pod

![image-20201118102858617](./images/Kubernetes-k8s-75.png)

需要注意，ingress不是内置的组件，需要我们单独的安装

## 13.4. 使用Ingress

步骤如下所示

- 部署ingress Controller【需要下载官方的】
- 创建ingress规则【对哪个Pod、名称空间配置规则】

### 13.4.1. 创建Nginx Pod

创建一个nginx应用，然后对外暴露端口

```bash
# 创建pod
kubectl create deployment web --image=nginx
# 查看
kubectl get pods
```

对外暴露端口

```bash
kubectl expose deployment web --port=80 --target-port=80 --type:NodePort
```

### 13.4.2. 部署 ingress controller

下面我们来通过yaml的方式，部署我们的ingress，配置文件如下所示

![image-20201118105427248](./images/Kubernetes-k8s-76.png)

这个文件里面，需要注意的是 hostNetwork: true，改成ture是为了让后面访问到

```
kubectl apply -f ingress-con.yaml
```

通过这种方式，其实我们在外面就能访问，这里还需要在外面添加一层

```
kubectl apply -f ingress-con.yaml
```

![image-20201118111256631](./images/Kubernetes-k8s-77.png)

最后通过下面命令，查看是否成功部署 ingress

```
kubectl get pods -n ingress-nginx
```

![image-20201118111424735](./images/Kubernetes-k8s-78.png)

### 13.4.3. 创建ingress规则文件

创建ingress规则文件，ingress-h.yaml

![image-20201118111700534](./images/Kubernetes-k8s-79.png)

### 13.4.4. 添加域名访问规则

在windows 的 hosts文件，添加域名访问规则【因为我们没有域名解析，所以只能这样做】

![image-20201118112029820](./images/Kubernetes-k8s-80.png)

最后通过域名就能访问

![image-20201118112212519](./images/Kubernetes-k8s-81.png)

# 14. Kubernetes核心技术Helm

Helm就是一个包管理工具【类似于npm】

![img](./images/Kubernetes-k8s-82.png)

## 14.1. 为什么引入Helm

首先在原来项目中都是基于yaml文件来进行部署发布的，而目前项目大部分微服务化或者模块化，会分成很多个组件来部署，每个组件可能对应一个deployment.yaml,一个service.yaml,一个Ingress.yaml还可能存在各种依赖关系，这样一个项目如果有5个组件，很可能就有15个不同的yaml文件，这些yaml分散存放，如果某天进行项目恢复的话，很难知道部署顺序，依赖关系等，而所有这些包括

- 基于yaml配置的集中存放
- 基于项目的打包
- 组件间的依赖

但是这种方式部署，会有什么问题呢？

- 如果使用之前部署单一应用，少数服务的应用，比较合适
- 但如果部署微服务项目，可能有几十个服务，每个服务都有一套yaml文件，需要维护大量的yaml文件，版本管理特别不方便

Helm的引入，就是为了解决这个问题

- 使用Helm可以把这些YAML文件作为整体管理
- 实现YAML文件高效复用
- 使用helm应用级别的版本管理

## 14.2. Helm介绍

Helm是一个Kubernetes的包管理工具，就像Linux下的包管理器，如yum/apt等，可以很方便的将之前打包好的yaml文件部署到kubernetes上。

Helm有三个重要概念

- helm：一个命令行客户端工具，主要用于Kubernetes应用chart的创建、打包、发布和管理
- Chart：应用描述，一系列用于描述k8s资源相关文件的集合
- Release：基于Chart的部署实体，一个chart被Helm运行后将会生成对应的release，将在K8S中创建出真实的运行资源对象。也就是应用级别的版本管理
- Repository：用于发布和存储Chart的仓库

## 14.3. Helm组件及架构

Helm采用客户端/服务端架构，有如下组件组成

- Helm CLI是Helm客户端，可以在本地执行
- Tiller是服务器端组件，在Kubernetes集群上运行，并管理Kubernetes应用程序
- Repository是Chart仓库，Helm客户端通过HTTP协议来访问仓库中Chart索引文件和压缩包

![image-20201119095458328](./images/Kubernetes-k8s-83.png)

## 14.4. Helm v3变化

2019年11月13日，Helm团队发布了Helm v3的第一个稳定版本

该版本主要变化如下

- 架构变化
  - 最明显的变化是Tiller的删除
  - V3版本删除Tiller
  - relesase可以在不同命名空间重用

V3之前

![image-20201118171523403](./images/Kubernetes-k8s-84.png)

V3版本

![image-20201118171956054](./images/Kubernetes-k8s-85.png)

## 14.5. helm安装配置

```bash
# Linux安装snap命令
yum install epel-release
yum install snapd
systemctl enable --now snapd.socket
ln -s /var/lib/snapd/snap /snap
# 单独执行安装helm的命令，因为snap可能还没准备好
snap install helm --classic
# 验证，可能需要重启
helm version
```

## 14.6. helm仓库

添加仓库

```
helm repo add 仓库名  仓库地址 
```

例如

```bash
# 配置微软源
helm repo add stable http://mirror.azure.cn/kubernetes/charts
# 配置阿里源
helm repo add aliyun https://kubernetes.oss-cn-hangzhou.aliyuncs.com/charts
# 配置google源
helm repo add google https://kubernetes-charts.storage.googleapis.com/

# 更新
helm repo update
```

然后可以查看我们添加的仓库地址

```bash
# 查看全部
helm repo list
# 查看某个
helm search repo stable
```

![image-20201118195732281](./images/Kubernetes-k8s-86.png)

或者可以删除我们添加的源

```
helm repo remove stable
```

## 14.7. helm基本命令

- chart install
- chart upgrade
- chart rollback

## 14.8. 使用helm快速部署应用

### 14.8.1. 使用命令搜索应用

首先我们使用命令，搜索我们需要安装的应用

```
# 搜索 weave仓库
helm search repo weave
```

![image-20201118200603643](./images/Kubernetes-k8s-87.png)

### 14.8.2. 根据搜索内容选择安装

搜索完成后，使用命令进行安装

```
helm install ui aliyun/weave-scope
```

可以通过下面命令，来下载yaml文件

```
kubectl apply -f weave-scope.yaml
```

安装完成后，通过下面命令即可查看

```
helm list
```

![image-20201118203727585](./images/Kubernetes-k8s-88.png)

同时可以通过下面命令，查看更新具体的信息

```
helm status ui
```

但是我们通过查看 svc状态，发现没有对象暴露端口

![image-20201118205031343](./images/Kubernetes-k8s-89.png)

所以我们需要修改service的yaml文件，添加NodePort

```
kubectl edit svc ui-weave-scope
```

![image-20201118205129431](./images/Kubernetes-k8s-90.png)

这样就可以对外暴露端口了

![image-20201118205147631](./images/Kubernetes-k8s-91.png)

然后我们通过 ip + 32185 即可访问

### 14.8.3. 如果自己创建Chart

使用命令，自己创建Chart

```
helm create mychart
```

创建完成后，我们就能看到在当前文件夹下，创建了一个 mychart目录

![image-20201118210755621](./images/Kubernetes-k8s-92.png)

#### 14.8.3.1. 目录格式

- templates：编写yaml文件存放到这个目录
- values.yaml：存放的是全局的yaml文件
- chart.yaml：当前chart属性配置信息

### 14.8.4. 在templates文件夹创建两个文件

我们创建以下两个

- deployment.yaml
- service.yaml

我们可以通过下面命令创建出yaml文件

```bash
# 导出deployment.yaml
kubectl create deployment web1 --image=nginx --dry-run -o yaml > deployment.yaml
# 导出service.yaml 【可能需要创建 deployment，不然会报错】
kubectl expose deployment web1 --port=80 --target-port=80 --type=NodePort --dry-run -o yaml > service.yaml
```

### 14.8.5. 安装mychart

执行命令创建

```bash
helm install web1 mychart
```

![image-20201118213120916](./images/Kubernetes-k8s-93.png)

### 14.8.6. 应用升级

当我们修改了mychart中的东西后，就可以进行升级操作

```
helm upgrade web1 mychart
```

## 14.9. chart模板使用

通过传递参数，动态渲染模板，yaml内容动态从传入参数生成

![image-20201118213630083](./images/Kubernetes-k8s-94.png)

刚刚我们创建mychart的时候，看到有values.yaml文件，这个文件就是一些全局的变量，然后在templates中能取到变量的值，下面我们可以利用这个，来完成动态模板

- 在values.yaml定义变量和值
- 具体yaml文件，获取定义变量值
- yaml文件中大体有几个地方不同
  - image
  - tag
  - label
  - port
  - replicas

### 14.9.1. 定义变量和值

在values.yaml定义变量和值

![image-20201118214050899](./images/Kubernetes-k8s-95.png)

### 14.9.2. 获取变量和值

我们通过表达式形式 使用全局变量 `{{.Values.变量名称}}`

例如： `{{.Release.Name}}`

![image-20201118214413203](./images/Kubernetes-k8s-96.png)

### 14.9.3. 安装应用

在我们修改完上述的信息后，就可以尝试的创建应用了

```
helm install --dry-run web2 mychart
```

![image-20201118214727058](./images/Kubernetes-k8s-97.png)

# 15. Kubernetes持久化存储

## 15.1. 前言

之前我们有提到数据卷：`emptydir` ，是本地存储，pod重启，数据就不存在了，需要对数据持久化存储

对于数据持久化存储【pod重启，数据还存在】，有两种方式

- nfs：网络存储【通过一台服务器来存储】
- PV和PVC

## 15.2. nfs网络存储

### 15.2.1. 持久化服务器上操作

- 找一台新的服务器nfs服务端，安装nfs
- 设置挂载路径

使用命令安装nfs

```
yum install -y nfs-utils
```

首先创建存放数据的目录

```
mkdir -p /data/nfs
```

设置挂载路径

```bash
# 打开文件
vim /etc/exports
# 添加如下内容
/data/nfs *(rw,no_root_squash)
```

执行完成后，即部署完我们的持久化服务器

### 15.2.2. Node节点上操作

然后需要在k8s集群node节点上安装nfs，这里需要在 node1 和 node2节点上安装

```
yum install -y nfs-utils
```

执行完成后，会自动帮我们挂载上

### 15.2.3. 启动nfs服务端

下面我们回到nfs服务端，启动我们的nfs服务

```bash
# 启动服务
systemctl start nfs
# 或者使用以下命令进行启动
service nfs-server start
```

![image-20201119082047766](./images/Kubernetes-k8s-98.png)

### 15.2.4. K8s集群部署应用

最后我们在k8s集群上部署应用，使用nfs持久化存储

```bash
# 创建一个pv文件
mkdir pv
# 进入
cd pv
```

然后创建一个yaml文件 `nfs-nginx.yaml`

![image-20201119082317625](./images/Kubernetes-k8s-99.png)

通过这个方式，就挂载到了刚刚我们的nfs数据节点下的 /data/nfs 目录

最后就变成了： /usr/share/nginx/html -> 192.168.44.134/data/nfs 内容是对应的

我们通过这个 yaml文件，创建一个pod

```
kubectl apply -f nfs-nginx.yaml
```

创建完成后，我们也可以查看日志

```
kubectl describe pod nginx-dep1
```

![image-20201119083444454](./images/Kubernetes-k8s-100.png)

可以看到，我们的pod已经成功创建出来了，同时下图也是出于Running状态

![image-20201119083514247](./images/Kubernetes-k8s-101.png)

下面我们就可以进行测试了，比如现在nfs服务节点上添加数据，然后在看数据是否存在 pod中

```bash
# 进入pod中查看
kubectl exec -it nginx-dep1 bash
```

![image-20201119095847548](./images/Kubernetes-k8s-102.png)

## 15.3. PV和PVC

对于上述的方式，我们都知道，我们的ip 和端口是直接放在我们的容器上的，这样管理起来可能不方便

所以这里就需要用到 pv 和 pvc的概念了，方便我们配置和管理我们的 ip 地址等元信息

**PV**：持久化存储，对存储的资源进行抽象，对外提供可以调用的地方【生产者】

**PVC**：用于调用，不需要关心内部实现细节【消费者】

PV 和 PVC 使得 K8S 集群具备了存储的逻辑抽象能力。

使得在配置Pod的逻辑里可以忽略对实际后台存储 技术的配置，而把这项配置的工作交给PV的配置者，即集群的管理者。存储的PV和PVC的这种关系，跟计算的Node和Pod的关系是非常类似的；PV和Node是资源的提供者，根据集群的基础设施变化而变化，由K8s集群管理员配置；而PVC和Pod是资源的使用者，根据业务服务的需求变化而变化，由K8s集群的使用者即服务的管理员来配置。

### 15.3.1. 实现流程

- PVC绑定PV
- 定义PVC
- 定义PV【数据卷定义，指定数据存储服务器的ip、路径、容量和匹配模式】

### 15.3.2. 举例

创建一个 pvc.yaml

![image-20201119101753419](./images/Kubernetes-k8s-103.png)

第一部分是定义一个 deployment，做一个部署

- 副本数：3
- 挂载路径
- 调用：是通过pvc的模式

然后定义pvc

![image-20201119101843498](./images/Kubernetes-k8s-104.png)

然后在创建一个 `pv.yaml`

![image-20201119101957777](./images/Kubernetes-k8s-105.png)

然后就可以创建pod了

```
kubectl apply -f pv.yaml
```

然后我们就可以通过下面命令，查看我们的 pv 和 pvc之间的绑定关系

```
kubectl get pv, pvc
```

![image-20201119102332786](./images/Kubernetes-k8s-106.png)

到这里为止，我们就完成了我们 pv 和 pvc的绑定操作，通过之前的方式，进入pod中查看内容

```
kubect exec -it nginx-dep1 bash
```

然后查看 /usr/share/nginx.html

![image-20201119102448226](./images/Kubernetes-k8s-107.png)

也同样能看到刚刚的内容，其实这种操作和之前我们的nfs是一样的，只是多了一层pvc绑定pv的操作



