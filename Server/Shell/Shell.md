<h2>目录</h2>

<details open>
  <summary><a href="#1-简介">1. 简介</a></summary>
  <ul>
    <a href="#11-shell-脚本">1.1. Shell 脚本</a><br>
    <a href="#12-shell-环境">1.2. Shell 环境</a><br>
    <a href="#13-第一个shell脚本">1.3. 第一个shell脚本</a><br>
  <details open>
    <summary><a href="#14-实例">1.4. 实例</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#141-运行-shell-脚本有两种方法：">1.4.1. 运行 Shell 脚本有两种方法：</a>    </summary>
      <ul>
        <a href="#1411-作为可执行程序">1.4.1.1. 作为可执行程序</a><br>
        <a href="#1412-作为解释器参数">1.4.1.2. 作为解释器参数</a><br>
      </ul>
    </details>
    </ul>
  </details>
    <a href="#15-查看当前是什么shell">1.5. 查看当前是什么Shell</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#2-变量">2. 变量</a></summary>
  <ul>
  <details open>
    <summary><a href="#21-定义变量">2.1. 定义变量</a>  </summary>
    <ul>
      <a href="#211-使用变量">2.1.1. 使用变量</a><br>
      <a href="#212-只读变量">2.1.2. 只读变量</a><br>
      <a href="#213-删除变量">2.1.3. 删除变量</a><br>
      <a href="#214-变量类型">2.1.4. 变量类型</a><br>
      <a href="#215-自定义环境变量（暂时有效，重启电脑后失效）">2.1.5. 自定义环境变量（暂时有效，重启电脑后失效）</a><br>
      <a href="#216-用户环境变量配置（当前用户永久有效）">2.1.6. 用户环境变量配置（当前用户永久有效）</a><br>
      <a href="#217-全局环境变量配置（所有用户永久有效）">2.1.7. 全局环境变量配置（所有用户永久有效）</a><br>
      <a href="#218-环境变量初始化与对应文件的生效顺序">2.1.8. 环境变量初始化与对应文件的生效顺序</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#22-shell-字符串">2.2. Shell 字符串</a>  </summary>
    <ul>
      <a href="#221-单引号">2.2.1. 单引号</a><br>
      <a href="#222-双引号">2.2.2. 双引号</a><br>
      <a href="#223-拼接字符串">2.2.3. 拼接字符串</a><br>
      <a href="#224-获取字符串长度">2.2.4. 获取字符串长度</a><br>
      <a href="#225-提取子字符串">2.2.5. 提取子字符串</a><br>
      <a href="#226-查找子字符串">2.2.6. 查找子字符串</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#23-shell-数组">2.3. Shell 数组</a>  </summary>
    <ul>
      <a href="#231-定义数组">2.3.1. 定义数组</a><br>
      <a href="#232-读取数组">2.3.2. 读取数组</a><br>
      <a href="#233-获取数组的长度">2.3.3. 获取数组的长度</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#24-shell-注释">2.4. Shell 注释</a>  </summary>
    <ul>
      <a href="#241-单行注释">2.4.1. 单行注释</a><br>
      <a href="#242-多行注释">2.4.2. 多行注释</a><br>
    </ul>
  </details>
  </ul>
</details>
  <a href="#3-传递参数">3. 传递参数</a><br>
<details open>
  <summary><a href="#4-数组">4. 数组</a></summary>
  <ul>
  <details open>
    <summary><a href="#41-数组遍历">4.1. 数组遍历</a>  </summary>
    <ul>
      <a href="#411-标准的for循环">4.1.1. 标准的for循环</a><br>
      <a href="#412-for--in">4.1.2. for … in</a><br>
      <a href="#413-while循环法：">4.1.3. While循环法：</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#5-基本运算符">5. 基本运算符</a></summary>
  <ul>
    <a href="#51-算术运算符">5.1. 算术运算符</a><br>
    <a href="#52-关系运算符">5.2. 关系运算符</a><br>
    <a href="#53-布尔运算符">5.3. 布尔运算符</a><br>
    <a href="#54-逻辑运算符">5.4. 逻辑运算符</a><br>
    <a href="#55-字符串运算符">5.5. 字符串运算符</a><br>
    <a href="#56-文件测试运算符">5.6. 文件测试运算符</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#6-echo-命令">6. echo 命令</a></summary>
  <ul>
    <a href="#61-显示普通字符串">6.1. 显示普通字符串:</a><br>
    <a href="#62-显示转义字符">6.2. 显示转义字符</a><br>
    <a href="#63-显示变量">6.3. 显示变量</a><br>
    <a href="#64-显示换行">6.4. 显示换行</a><br>
    <a href="#65-显示不换行">6.5. 显示不换行</a><br>
    <a href="#66-原样输出字符串，不进行转义或取变量用单引号">6.6. 原样输出字符串，不进行转义或取变量(用单引号)</a><br>
    <a href="#67-显示命令执行结果">6.7. 显示命令执行结果</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#7-printf-命令">7. printf 命令</a></summary>
  <ul>
    <a href="#71-printf-的转义序列">7.1. printf 的转义序列</a><br>
  </ul>
</details>
  <a href="#8-test-命令">8. test 命令</a><br>
<details open>
  <summary><a href="#9-流程控制">9. 流程控制</a></summary>
  <ul>
  <details open>
    <summary><a href="#91-if-else">9.1. if else</a>  </summary>
    <ul>
      <a href="#911-if">9.1.1. if</a><br>
      <a href="#912-if-else">9.1.2. if else</a><br>
      <a href="#913-if-else-if-else">9.1.3. if else-if else</a><br>
    </ul>
  </details>
    <a href="#92-for-循环">9.2. for 循环</a><br>
  <details open>
    <summary><a href="#93-while-语句">9.3. while 语句</a>  </summary>
    <ul>
      <a href="#931-无限循环">9.3.1. 无限循环</a><br>
    </ul>
  </details>
    <a href="#94-until-循环">9.4. until 循环</a><br>
    <a href="#95-case">9.5. case</a><br>
  <details open>
    <summary><a href="#96-跳出循环">9.6. 跳出循环</a>  </summary>
    <ul>
      <a href="#961-break命令">9.6.1. break命令</a><br>
      <a href="#962-continue">9.6.2. continue</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#10-函数">10. 函数</a></summary>
  <ul>
    <a href="#101-函数参数">10.1. 函数参数</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#11-输入输出重定向">11. 输入/输出重定向</a></summary>
  <ul>
    <a href="#111-here-document">11.1. Here Document</a><br>
    <a href="#112-devnull-文件">11.2. /dev/null 文件</a><br>
  </ul>
</details>
  <a href="#12-文件包含">12. 文件包含</a><br>


<h1>Shell</h1>

# 1. 简介

Shell 是一个用 C 语言编写的程序，它是用户使用 Linux 的桥梁。Shell 既是一种命令语言，又是一种程序设计语言。

Shell 是指一种应用程序，这个应用程序提供了一个界面，用户通过这个界面访问操作系统内核的服务。

Ken Thompson 的 sh 是第一种 Unix Shell，Windows Explorer 是一个典型的图形界面 Shell。

[**Shell 在线工具**](https://www.runoob.com/try/showbash.php?filename=helloworld)

------

## 1.1. Shell 脚本

Shell 脚本（shell script），是一种为 shell 编写的脚本程序。

业界所说的 shell 通常都是指 shell 脚本，但读者朋友要知道，shell 和 shell script 是两个不同的概念。

由于习惯的原因，简洁起见，本文出现的 "shell编程" 都是指 shell 脚本编程，不是指开发 shell 自身。

------

## 1.2. Shell 环境

Shell 编程跟 JavaScript、php 编程一样，只要有一个能编写代码的文本编辑器和一个能解释执行的脚本解释器就可以了。

Linux 的 Shell 种类众多，常见的有：

- Bourne Shell（/usr/bin/sh或/bin/sh）
- Bourne Again Shell（/bin/bash）
- C Shell（/usr/bin/csh）
- K Shell（/usr/bin/ksh）
- Shell for Root（/sbin/sh）
- ……

本教程关注的是 Bash，也就是 Bourne Again Shell，由于易用和免费，Bash 在日常工作中被广泛使用。同时，Bash 也是大多数Linux 系统默认的 Shell。

在一般情况下，人们并不区分 Bourne Shell 和 Bourne Again Shell，所以，像 **#!/bin/sh**，它同样也可以改为 **#!/bin/bash**。

**#!** 告诉系统其后路径所指定的程序即是解释此脚本文件的 Shell 程序。

------

## 1.3. 第一个shell脚本

打开文本编辑器(可以使用 vi/vim 命令来创建文件)，新建一个文件 test.sh，扩展名为 sh（sh代表shell），扩展名并不影响脚本执行，见名知意就好，如果你用 php 写 shell 脚本，扩展名就用 php 好了。

输入一些代码，第一行一般是这样：

## 1.4. 实例

```bash
/#!/bin/bash
echo "Hello World !"
```

**#!** 是一个约定的标记，它告诉系统这个脚本需要什么解释器来执行，即使用哪一种 Shell。

echo 命令用于向窗口输出文本。

### 1.4.1. 运行 Shell 脚本有两种方法：

#### 1.4.1.1. 作为可执行程序

将上面的代码保存为 test.sh，并 cd 到相应目录：

```bash
chmod +x ./test.sh  #使脚本具有执行权限
./test.sh  #执行脚本
```

注意，一定要写成 **./test.sh**，而不是 **test.sh**，运行其它二进制的程序也一样，直接写 test.sh，linux 系统会去 PATH 里寻找有没有叫 test.sh 的，而只有 /bin, /sbin, /usr/bin，/usr/sbin 等在 PATH 里，你的当前目录通常不在 PATH 里，所以写成 test.sh 是会找不到命令的，要用 ./test.sh 告诉系统说，就在当前目录找。

#### 1.4.1.2. 作为解释器参数

这种运行方式是，直接运行解释器，其参数就是 shell 脚本的文件名，如：

```bash
/bin/sh test.sh
/bin/php test.php
```

这种方式运行的脚本，不需要在第一行指定解释器信息，写了也没用。

## 1.5. 查看当前是什么Shell

```bash
echo $SHELL
```

# 2. 变量

## 2.1. 定义变量

定义变量时，变量名不加美元符号（$，PHP语言中变量需要），如：

```bash
your_name="runoob.com"
```

注意，**变量名和等号之间不能有空格**，这可能和你熟悉的所有编程语言都不一样。同时，变量名的命名须遵循如下规则：

- 命名只能使用英文字母，数字和下划线，首个字符不能以数字开头。
- 中间不能有空格，可以使用下划线（_）。
- 不能使用标点符号。
- 不能使用bash里的关键字（可用help命令查看保留关键字）。

有效的 Shell 变量名示例如下：

```bash
RUNOOB
LD_LIBRARY_PATH
_var
var2
```

无效的变量命名：

```bash
?var=123
user*name=runoob
```

除了显式地直接赋值，还可以用语句给变量赋值，如：

```bash
for file in `ls /etc`
或
for file in $(ls /etc)
```

以上语句将 /etc 下目录的文件名循环出来。

------

### 2.1.1. 使用变量

使用一个定义过的变量，只要在变量名前面加美元符号即可，如：

```bash
your_name="qinjx"
echo $your_name
echo ${your_name}
```

**变量名外面的花括号是可选的，加不加都行，加花括号是为了帮助解释器识别变量的边界**，比如下面这种情况：

```bash
for skill in Ada Coffe Action Java; do
    echo "I am good at ${skill}Script"
done
```

已定义的变量，可以被重新定义，如：

```bash
your_name="tom"
echo $your_name
your_name="alibaba"
echo $your_name
```

这样写是合法的，但注意，第二次赋值的时候不能写$your_name="alibaba"，**使用变量的时候才加美元符（$）**。

### 2.1.2. 只读变量

使用 readonly 命令可以将变量定义为只读变量，只读变量的值不能被改变。

下面的例子尝试更改只读变量，结果报错：

```bash
#!/bin/bash
myUrl="https://www.google.com"
readonly myUrl
myUrl="https://www.runoob.com"
```

运行脚本，结果如下：

```bash
/bin/sh: NAME: This variable is read only.
```

### 2.1.3. 删除变量

使用 unset 命令可以删除变量。语法：

```bash
unset variable_name
```

变量被删除后不能再次使用。unset 命令不能删除只读变量。

**实例**

```bash
/#!/bin/sh
myUrl="https://www.runoob.com"
unset myUrl
echo $myUrl
```

以上实例执行将没有任何输出。

### 2.1.4. 变量类型

运行shell时，会同时存在三种变量：

1. **局部变量（普通变量）** 局部变量在脚本或命令中定义，仅在当前shell实例中有效，其他shell启动的程序不能访问局部变量。
2. **环境变量（全局变量）** **使用`env`命令可以查看环境变量**，环境变量又可以分为自定义环境变量和bash内置的环境变量，环境变量可以在命令行中设置和创建，用户退出命令行时这些变量值就会丢失，想要永久保存环境变量，可在用户家目录下的. bash_profile或. bashrc(非用户登录模式特有，如：SSH)文件中，或在/etc/profile文件中定义，这样每次用户登录时这些变量都将被初始化。。
3. **shell变量** **使用`set`命令可以查看环境变量**shell变量是由shell程序设置的特殊变量。shell变量中有一部分是环境变量，有一部分是局部变量，这些变量保证了shell的正常运行

### 2.1.5. 自定义环境变量（暂时有效，重启电脑后失效）

bash下

- 设置：export 变量名=变量值
- 删除：unset 变量名

csh下

- 设置：setenv 变量名 变量值
- 删除：unsetenv 变量名

```bash
# 设置环境变量
export NAME=qzlking1
env|grep qzl #＜==查看定义结果
cat /etc/profile|grep qzl

source /etc/profile

# 删除环境变量
unset NAME
echo  $NAME
```

### 2.1.6. 用户环境变量配置（当前用户永久有效）

对于用户的环境变量设置，常见的是用户家目录下的.bashrc和.bash_profile

```bash
# 进入home目录
cd ~
vi .bashrc
vi .bash_profile

# 可以设置例如如下的内容
export RESIN_HOME=/application/resin　　

# 使设置的用户环境变量生效，Linux执行下面的命令后会立即生效，但是Mac需要重启电脑后生效
source ~/.bashrc
source ~/.bash_profile
```

### 2.1.7. 全局环境变量配置（所有用户永久有效）

常见的全局环境变量配置文件，**/etc/profile; /etc/bashrc; /etc/profile.d**这三个配置文件，如果想要在登陆后初始化或者显示加载的内容，只需要把脚本文件放在/etc/profile.d文件下即可（不需要加执行权限）。

```bash
# 编辑配置
vi /etc/profile
vi /etc/bashrc
vi /etc/profile.d

# 可以设置例如如下的内容
export RESIN_HOME=/application/resin　　

# 使设置的用户环境变量生效，Linux执行下面的命令后会立即生效，但是Mac需要重启电脑后生效
source /etc/profile
source /etc/bashrc
source /etc/profile.d
```

### 2.1.8. 环境变量初始化与对应文件的生效顺序

![img](./images/Shell-1.png)

用户登录系统后首先会加载/etc/profile全局环境变量文件。加载完后，执行/etc/profile.d目录下的脚本文件（如：系统的字符集设置/etc/sysconfigil8n）

然后在运行$HOME/.bash_profile（用户环境变量文件），在这文件里会找$HOME/.bashrc（用户环境变量文件），有就执行，没有就不执行。

在$HOME/.bashrc找/etc/bashrc（全局环境变量文件）有就执行，没有就不执行。



如果希望用户的Shell不是登陆时启动的（如：手动敲下bash时启动或者远程ssh连接情况），非登陆Shell只会加载$HOME/.bashrc（用户环境变量文件），并会去找/etc/bashrc（全局环境变量文件）。

即非登陆Shell想读到设置的环境变量，需要将变量设定等写入$HOME/.bashrc（用户环境变量文件）或etc/bashrc（全局环境变量文件），而不是$HOME/.bash_profile或/etc/profile

## 2.2. Shell 字符串

字符串是shell编程中最常用最有用的数据类型（除了数字和字符串，也没啥其它类型好用了），字符串可以用单引号，也可以用双引号，也可以不用引号。

### 2.2.1. 单引号

```bash
str='this is a string'+'testtt'
echo ${str}


输出的内容是：
this is a string+testtt
```

单引号字符串的限制：

- **单引号里**的任何字符都会**原样输出**，**单引号字符串中的变量是无效的**；
- **单引号字串中不能出现单独一个的单引号**（对单引号使用转义符后也不行），但可成对出现，作为字符串拼接使用。

### 2.2.2. 双引号

```bash
your_name='runoob'
str="Hello, I know you are \"$your_name\"! \n"
echo -e $str
```

-e：enable interpretation of backslash escapes，启用反斜杠转义的解释

输出结果为：

```bash
Hello, I know you are "runoob"! 
```

双引号的优点：

- **双引号里可以有变量**
- **双引号里可以出现转义字符**

### 2.2.3. 拼接字符串

```bash
your_name="runoob"
# 使用双引号拼接
greeting="hello, "$your_name" !"
greeting_1="hello, ${your_name} !"
echo $greeting  $greeting_1
# 使用单引号拼接
greeting_2='hello, '$your_name' !'
greeting_3='hello, ${your_name} !'
echo $greeting_2  $greeting_3
```

输出结果为：

```bash
hello, runoob ! hello, runoob !
hello, runoob ! hello, ${your_name} !
```

**单引号中不能有变量**

### 2.2.4. 获取字符串长度

```bash
string="abcd"
echo ${#string} #输出 4
```

### 2.2.5. 提取子字符串

以下实例**从字符串第 2 个字符开始截取 4 个字符**：

```bash
string="runoob is a great site"
echo ${string:1:4} # 输出 unoo
```

**注意**：第一个字符的索引值为 **0**。

### 2.2.6. 查找子字符串

查找字符 **i** 或 **o** 的位置(哪个字母先出现就计算哪个)：

```bash
string="runoob is a great site"
echo `expr index "$string" io`  # 输出 4
```

**注意：** 以上脚本中 **`** 是反引号，而不是单引号 **'**，不要看错了哦。

------

## 2.3. Shell 数组

bash支持一维数组（不支持多维数组），并且没有限定数组的大小。

类似于 C 语言，数组元素的下标由 0 开始编号。获取数组中的元素要利用下标，下标可以是整数或算术表达式，其值应大于或等于 0。

### 2.3.1. 定义数组

在 Shell 中，用括号来表示数组，数组元素用"**空格**"符号分割开。定义数组的一般形式为：

```bash
数组名=(值1 值2 ... 值n)
```

例如：

```bash
array_name=(value0 value1 value2 value3)
```

或者

```bash
array_name=(
value0
value1
value2
value3
)
```

还可以单独定义数组的各个分量：

```bash
array_name[0]=value0
array_name[1]=value1
array_name[n]=valuen
```

可以不使用连续的下标，而且下标的范围没有限制。

### 2.3.2. 读取数组

读取数组元素值的一般格式是：

```bash
${数组名[下标]}
```

例如：

```bash
valuen=${array_name[n]}
```

使用 **@** 符号可以获取数组中的所有元素，例如：

```bash
echo ${array_name[@]}
```

### 2.3.3. 获取数组的长度

获取数组长度的方法与获取字符串长度的方法相同，例如：@和*区别

```bash
/# 取得数组元素的个数
length=${#array_name[@]}
/# 或者
length=${#array_name[*]}
/# 取得数组单个元素的长度
lengthn=${#array_name[n]}
```

------

## 2.4. Shell 注释

### 2.4.1. 单行注释

以 **#** 开头的行就是注释，会被解释器忽略。

通过每一行加一个 **#** 号设置多行注释，像这样：

```bash
#--------------------------------------------
# 这是一个注释
# author：菜鸟教程
# site：www.runoob.com
# slogan：学的不仅是技术，更是梦想！
#--------------------------------------------
##### 用户配置区 开始 #####
#
#
# 这里可以添加脚本描述信息
# 
#
##### 用户配置区 结束  #####
```

如果在开发过程中，遇到大段的代码需要临时注释起来，过一会儿又取消注释，怎么办呢？

每一行加个#符号太费力了，可以把这一段要注释的代码用一对花括号括起来，定义成一个函数，没有地方调用这个函数，这块代码就不会执行，达到了和注释一样的效果。

```bash
if false; then
# 注释内容块
fi
```

### 2.4.2. 多行注释

多行注释还可以使用以下格式：

```bash
:<<EOF
注释内容...
注释内容...
注释内容...
EOF
```

EOF 也可以使用其他符号:

```bash
:<<'
注释内容...
注释内容...
注释内容...
'

:<<!
注释内容...
注释内容...
注释内容...
!
```

# 3. 传递参数

我们可以在执行 Shell 脚本时，向脚本传递参数，脚本内获取参数的格式为：**$n**。**n** 代表一个数字，1 为执行脚本的第一个参数，2 为执行脚本的第二个参数，以此类推……

以下实例我们向脚本传递三个参数，并分别输出，其中 **$0** 为执行的文件名（包含文件路径）：

```bash
#!/bin/bash
# author:菜鸟教程
# url:www.runoob.com

echo "Shell 传递参数实例！";
echo "执行的文件名：$0";
echo "第一个参数为：$1";
echo "第二个参数为：$2";
echo "第三个参数为：$3";
```

为脚本设置可执行权限，并执行脚本，输出结果如下所示：

```bash
$ chmod +x test.sh 
$ ./test.sh 1 2 3
Shell 传递参数实例！
执行的文件名：./test.sh
第一个参数为：1
第二个参数为：2
第三个参数为：3
```

另外，还有几个特殊字符用来处理参数：

| 参数处理 | 说明                                                         |
| :------- | :----------------------------------------------------------- |
| $#       | 传递到脚本的参数个数                                         |
| $*       | 以一个单字符串显示所有向脚本传递的参数。 如"$*"用「"」括起来的情况、以"$1 $2 … $n"的形式输出所有参数。 |
| $$       | 脚本运行的当前进程ID号                                       |
| $!       | 后台运行的最后一个进程的ID号                                 |
| $@       | 与$*相同，但是使用时加引号，并在引号中返回每个参数。 如"$@"用「"」括起来的情况、以"$1" "$2" … "$n" 的形式输出所有参数。 |
| $-       | 显示Shell使用的当前选项，与[set命令](https://www.runoob.com/linux/linux-comm-set.html)功能相同。 |
| $?       | 显示最后命令的退出状态。0表示没有错误，其他任何值表明有错误。 |

```bash
#!/bin/bash
# author:菜鸟教程
# url:www.runoob.com

echo "Shell 传递参数实例！";
echo "第一个参数为：$1";

echo "参数个数为：$#";
echo "传递的参数作为一个字符串显示：$*";
```

执行脚本，输出结果如下所示：

```bash
$ chmod +x test.sh 
$ ./test.sh 1 2 3
Shell 传递参数实例！
第一个参数为：1
参数个数为：3
传递的参数作为一个字符串显示：1 2 3
```

**$* 与 $@ 区别**：

- 相同点：都是引用所有参数。
- 不同点：只有在双引号中体现出来。假设在脚本运行时写了三个参数 1、2、3，，则 " * " 等价于 "1 2 3"（传递了一个参数），而 "@" 等价于 "1" "2" "3"（传递了三个参数）。

```bash
#!/bin/bash
# author:菜鸟教程
# url:www.runoob.com

echo "-- \$* 演示 ---"
for i in "$*"; do
    echo $i
done

echo "-- \$@ 演示 ---"
for i in "$@"; do
    echo $i
done
```

执行脚本，输出结果如下所示：

```bash
$ chmod +x test.sh 
$ ./test.sh 1 2 3
-- $* 演示 ---
1 2 3
-- $@ 演示 ---
1
2
3
```

# 4. 数组

## 4.1. 数组遍历

### 4.1.1. 标准的for循环

```bash
for(( i=0;i<${#array[@]};i++)) do
#${#array[@]}获取数组长度用于循环
echo ${array[i]};
done;
```

### 4.1.2. for … in

遍历（不带数组下标）：

```bash
for element in ${array[@]}
#也可以写成for element in ${array[*]}
do
echo $element
done
```

遍历（带数组下标）：

```bash
for i in "${!arr[@]}";
do
  printf "%s\t%s\n" "$i" "${arr[$i]}"
done
```

### 4.1.3. While循环法：

```bash
i=0  
while [ $i -lt ${#array[@]} ]  
#当变量（下标）小于数组长度时进入循环体
do  
  echo ${ array[$i] }  
  #按下标打印数组元素
  let i++  
done 
```

# 5. 基本运算符

Shell 和其他编程语言一样，支持多种运算符，包括：

- 算数运算符
- 关系运算符
- 布尔运算符
- 字符串运算符
- 文件测试运算符

原生bash不支持简单的数学运算，但是可以通过其他命令来实现，例如 awk 和 expr，expr 最常用。

expr 是一款表达式计算工具，使用它能完成表达式的求值操作。

例如，两个数相加(**注意使用的是反引号 ` 而不是单引号 '**)：

```bash
#!/bin/bash

val=`expr 2 + 2`
echo "两数之和为 : $val"
```

执行脚本，输出结果如下所示：

```
两数之和为 : 4
```

两点注意：

- **表达式和运算符之间要有空格，例如 2+2 是不对的，必须写成 2 + 2**，这与我们熟悉的大多数编程语言不一样。
- **完整的表达式要被``包含，注意这个字符不是常用的单引号，在 Esc 键下边**。

------

## 5.1. 算术运算符

下表列出了常用的算术运算符，假定变量 a 为 10，变量 b 为 20：

| 运算符 | 说明                                          | 举例                          |
| :----- | :-------------------------------------------- | :---------------------------- |
| +      | 加法                                          | `expr $a + $b` 结果为 30。    |
| -      | 减法                                          | `expr $a - $b` 结果为 -10。   |
| *      | 乘法                                          | `expr $a \* $b` 结果为  200。 |
| /      | 除法                                          | `expr $b / $a` 结果为 2。     |
| %      | 取余                                          | `expr $b % $a` 结果为 0。     |
| =      | 赋值                                          | a=$b 将把变量 b 的值赋给 a。  |
| ==     | 相等。用于比较两个数字，相同则返回 true。     | [ $a == $b ] 返回 false。     |
| !=     | 不相等。用于比较两个数字，不相同则返回 true。 | [ $a != $b ] 返回 true。      |

**注意：**条件表达式要放在方括号之间，并且要有空格，例如: **[$a==$b]** 是错误的，必须写成 **[ $a == $b ]**。

> **注意：**
>
> - 乘号(*)前边必须加反斜杠(\\)才能实现乘法运算；
> - 在 MAC 中 shell 的 expr 语法是：**$((表达式))**，此处表达式中的 "*" 不需要转义符号 "\\" 。

------

## 5.2. 关系运算符

关系运算符**只支持数字，不支持字符串，除非字符串的值是数字**。

下表列出了常用的关系运算符，假定变量 a 为 10，变量 b 为 20：

| 运算符 | 说明                                                  | 举例                       |
| :----- | :---------------------------------------------------- | :------------------------- |
| -eq    | 检测两个数是否相等，相等返回 true。                   | [ $a -eq $b ] 返回 false。 |
| -ne    | 检测两个数是否不相等，不相等返回 true。               | [ $a -ne $b ] 返回 true。  |
| -gt    | 检测左边的数是否大于右边的，如果是，则返回 true。     | [ $a -gt $b ] 返回 false。 |
| -lt    | 检测左边的数是否小于右边的，如果是，则返回 true。     | [ $a -lt $b ] 返回 true。  |
| -ge    | 检测左边的数是否大于等于右边的，如果是，则返回 true。 | [ $a -ge $b ] 返回 false。 |
| -le    | 检测左边的数是否小于等于右边的，如果是，则返回 true。 | [ $a -le $b ] 返回 true。  |

## 5.3. 布尔运算符

下表列出了常用的布尔运算符，假定变量 a 为 10，变量 b 为 20：

| 运算符 | 说明                                                | 举例                                     |
| :----- | :-------------------------------------------------- | :--------------------------------------- |
| !      | 非运算，表达式为 true 则返回 false，否则返回 true。 | [ ! false ] 返回 true。                  |
| -o     | 或运算，有一个表达式为 true 则返回 true。           | [ $a -lt 20 -o $b -gt 100 ] 返回 true。  |
| -a     | 与运算，两个表达式都为 true 才返回 true。           | [ $a -lt 20 -a $b -gt 100 ] 返回 false。 |

## 5.4. 逻辑运算符

以下介绍 Shell 的逻辑运算符，假定变量 a 为 10，变量 b 为 20:

| 运算符 | 说明       | 举例                                       |
| :----- | :--------- | :----------------------------------------- |
| &&     | 逻辑的 AND | [[ $a -lt 100 && $b -gt 100 ]] 返回 false  |
| \|\|   | 逻辑的 OR  | [[ $a -lt 100 \|\| $b -gt 100 ]] 返回 true |

## 5.5. 字符串运算符

下表列出了常用的字符串运算符，假定变量 a 为 "abc"，变量 b 为 "efg"：

| 运算符 | 说明                                         | 举例                     |
| :----- | :------------------------------------------- | :----------------------- |
| =      | 检测两个字符串是否相等，相等返回 true。      | [ $a = $b ] 返回 false。 |
| !=     | 检测两个字符串是否相等，不相等返回 true。    | [ $a != $b ] 返回 true。 |
| -z     | 检测字符串长度是否为0，为0返回 true。        | [ -z $a ] 返回 false。   |
| -n     | 检测字符串长度是否不为 0，不为 0 返回 true。 | [ -n "$a" ] 返回 true。  |
| $      | 检测字符串是否为空，不为空返回 true。        | [ $a ] 返回 true。       |

## 5.6. 文件测试运算符

文件测试运算符用于检测 Unix 文件的各种属性。

属性检测描述如下：

| 操作符          | 说明                                                         | 举例                      |
| :-------------- | :----------------------------------------------------------- | :------------------------ |
| File1 –ef File2 | 检测两个文件是否具有同样的设备号和i结点号，如果是，则返回 true。 |                           |
| File1 –nt File2 | 文件1是否比文件2 新，如果是，则返回 true。                   |                           |
| File1 –ot File2 | 文件1是否比文件2 旧，如果是，则返回 true。                   |                           |
| -b file         | 检测文件是否是块设备文件，如果是，则返回 true。              | [ -b $file ] 返回 false。 |
| -c file         | 检测文件是否是字符设备文件，如果是，则返回 true。            | [ -c $file ] 返回 false。 |
| -d file         | 检测文件是否是目录，如果是，则返回 true。                    | [ -d $file ] 返回 false。 |
| -f file         | 检测文件是否是普通文件（既不是目录，也不是设备文件），如果是，则返回 true。 | [ -f $file ] 返回 true。  |
| -g file         | 检测文件是否设置了 SGID 位，如果是，则返回 true。            | [ -g $file ] 返回 false。 |
| -k file         | 检测文件是否设置了粘着位(Sticky Bit)，如果是，则返回 true。  | [ -k $file ] 返回 false。 |
| -p file         | 检测文件是否是有名管道，如果是，则返回 true。                | [ -p $file ] 返回 false。 |
| -u file         | 检测文件是否设置了 SUID 位，如果是，则返回 true。            | [ -u $file ] 返回 false。 |
| -r file         | 检测文件是否可读，如果是，则返回 true。                      | [ -r $file ] 返回 true。  |
| -w file         | 检测文件是否可写，如果是，则返回 true。                      | [ -w $file ] 返回 true。  |
| -x file         | 检测文件是否可执行，如果是，则返回 true。                    | [ -x $file ] 返回 true。  |
| -s file         | 检测文件是否为空（文件大小是否大于0），不为空返回 true。     | [ -s $file ] 返回 true。  |
| -e file         | 检测文件（包括目录）是否存在，如果是，则返回 true。          | [ -e $file ] 返回 true。  |

SUID 是 Set User ID, SGID 是 Set Group ID的意思

其他检查符：

- **-S**: 判断某文件是否 socket。
- **-L**: 检测文件是否存在并且是一个符号链接。

# 6. echo 命令

Shell 的 echo 指令与 PHP 的 echo 指令类似，都是用于字符串的输出。

## 6.1. 显示普通字符串:

```bash
echo "It is a test"
```

这里的双引号完全可以省略，以下命令与上面实例效果一致：

```bash
echo It is a test
```

## 6.2. 显示转义字符

```bash
echo "\"It is a test\""
```

结果将是:

```
"It is a test"
```

同样，双引号也可以省略

```bash
echo \"It is a test\"
```

## 6.3. 显示变量

read 命令从标准输入中读取一行,并把输入行的每个字段的值指定给 shell 变量，需要手动输入

```bash
#!/bin/sh
read name 
echo "$name It is a test"
```

以上代码保存为 test.sh，name 接收标准输入的变量，结果将是:

```bash
[root@www ~]# sh test.sh
OK                     #标准输入
OK It is a test        #输出
```

## 6.4. 显示换行

-e：enable interpretation of backslash escapes，启用反斜杠转义的解释

```bash
echo -e "OK! \n" # -e 开启转义
echo "It is a test"
```

输出结果：

```bash
OK!

It is a test
```

## 6.5. 显示不换行

```bash
/#!/bin/sh
echo -e "OK! \c" # -e 开启转义 \c 不换行
echo "It is a test"
```

输出结果：

```
OK! It is a test
```

## 6.6. 原样输出字符串，不进行转义或取变量(用单引号)

```bash
echo '$name\"'
```

输出结果：

```
$name\"
```

## 6.7. 显示命令执行结果

```bash
echo `date`
```

**注意：** 这里使用的是反引号 **`**, 而不是单引号 **'**。

结果将显示当前日期

```
Thu Jul 24 10:08:46 CST 2014
```

# 7. printf 命令

printf 命令模仿 C 程序库（library）里的 printf() 程序。

printf 由 POSIX 标准所定义，因此使用 printf 的脚本比使用 echo 移植性好。

printf 使用引用文本或空格分隔的参数，外面可以在 printf 中使用格式化字符串，还可以制定字符串的宽度、左右对齐方式等。默认 printf 不会像 echo 自动添加换行符，我们可以手动添加 \n。

**可移植操作系统接口**（Portable Operating System Interface，缩写为**POSIX**）是IEEE为要在各种UNIX操作系统上运行软件，而定义API的一系列互相关联的标准的总称，其正式称呼为IEEE Std 1003，而国际标准名称为ISO/IEC 9945。

printf 命令的语法：

```bash
printf  format-string  [arguments...]
```

**参数说明：**

- **format-string:** 为格式控制字符串
- **arguments:** 为参数列表。

**实例**

```bash
$ echo "Hello, Shell"
Hello, Shell
$ printf "Hello, Shell\n"
Hello, Shell
$
注意两个有无\n的区别
```

接下来,我来用一个脚本来体现 printf 的强大功能：

**实例**

```bash
#!/bin/bash
# author:菜鸟教程
# url:www.runoob.com
 
printf "%-10s %-8s %-4s\n" 姓名 性别 体重kg  
printf "%-10s %-8s %-4.2f\n" 郭靖 男 66.1234
printf "%-10s %-8s %-4.2f\n" 杨过 男 48.6543
printf "%-10s %-8s %-4.2f\n" 郭芙 女 47.9876
```

执行脚本，输出结果如下所示：

```
姓名     性别   体重kg
郭靖     男      66.12
杨过     男      48.65
郭芙     女      47.99
```

**%s %c %d %f** 都是格式替代符，**％s** 输出一个字符串，**％d** 整型输出，**％c** 输出一个字符，**％f** 输出实数，以小数形式输出。

**%-10s** 指一个宽度为 10 个字符（**-** 表示左对齐，没有则表示右对齐），任何字符都会被显示在 10 个字符宽的字符内，如果不足则自动以空格填充，超过也会将内容全部显示出来。

**%-4.2f** 指格式化为小数，其中 **.2** 指保留2位小数。



**实例**

```bash
#!/bin/bash
# author:菜鸟教程
# url:www.runoob.com
 
# format-string为双引号
/printf "%d %s\n" 1 "abc"

# 单引号与双引号效果一样
printf '%d %s\n' 1 "abc"

# 没有引号也可以输出
printf %s abcdef

# 格式只指定了一个参数，但多出的参数仍然会按照该格式输出，format-string 被重用!
printf %s abc def

printf "%s\n" abc def

printf "%s %s %s\n" a b c d e f g h i j

# 如果没有 arguments，那么 %s 用NULL代替，%d 用 0 代替
printf "%s and %d \n"
```

执行脚本，输出结果如下所示：

```bash
1 abc
1 abc
abcdefabcdef
abc
def
a b c
d e f
g h i
j  
 and 0
```

------

## 7.1. printf 的转义序列

| 序列  | 说明                                                         |
| :---- | :----------------------------------------------------------- |
| \a    | alert (BEL)，警告字符，通常为ASCII的BEL字符                  |
| \b    | backspace，后退                                              |
| \c    | produce no further output，抑制（不显示）输出结果中任何结尾的换行字符（只在%b格式指示符控制下的参数字符串中有效），而且，任何留在参数里的字符、任何接下来的参数以及任何留在格式字符串中的字符，都被忽略 |
| \f    | form feed换页（formfeed）                                    |
| \n    | new line，换行                                               |
| \r    | Carriage return，回车                                        |
| \t    | horizontal tab，水平制表符                                   |
| \v    | vertical tab，垂直制表符                                     |
| \\    | 一个字面上的反斜杠字符                                       |
| \ddd  | \NNN   byte with octal value NNN (1 to 3 digits)，表示1到3位数八进制值的字符。仅在格式字符串中有效 |
| \0ddd | \uHHHH Unicode (ISO/IEC 10646) character with hex value HHHH (4 digits)，表示1到3位的八进制值字符 |

# 8. test 命令

Shell中的 test 命令用于检查某个条件是否成立，它可以进行数值、字符和文件三个方面的测试。

------

# 9. 流程控制

和Java、PHP等语言不一样，sh的流程控制不可为空，在sh/bash里可不能这么写，如果else分支没有语句执行，就不要写这个else。

------

## 9.1. if else

### 9.1.1. if

if 语句语法格式：

```bash
if condition
then
    command1 
    command2
    ...
    commandN
fi
```

写成一行（适用于终端命令提示符）：

```bash
if [ $(ps -ef | grep -c "ssh") -gt 1 ]; then echo "true"; fi
```

末尾的fi就是if倒过来拼写，后面还会遇到类似的。

### 9.1.2. if else

if else 语法格式：

```bash
if condition
then
    command1 
    command2
    ...
    commandN
else
    command
fi
```

### 9.1.3. if else-if else

if else-if else 语法格式：

```bash
if condition1
then
    command1
elif condition2 
then 
    command2
else
    commandN
fi
```

## 9.2. for 循环

与其他编程语言类似，Shell支持for循环。

for循环一般格式为：

```bash
for var in item1 item2 ... itemN
do
    command1
    command2
    ...
    commandN
done
```

写成一行：

```bash
for var in item1 item2 ... itemN; do command1; command2… done;
```

当变量值在列表里，for循环即执行一次所有命令，使用变量名获取列表中的当前取值。命令可为任何有效的shell命令和语句。in列表可以包含替换、字符串和文件名。

in列表是可选的，如果不用它，for循环使用命令行的位置参数。

## 9.3. while 语句

while循环用于不断执行一系列命令，也用于从输入文件中读取数据；命令通常为测试条件。其格式为：

```bash
while condition
do
    command
done
```

以下是一个基本的while循环，测试条件是：如果int小于等于5，那么条件返回真。int从0开始，每次循环处理时，int加1。运行上述脚本，返回数字1到5，然后终止。

```bash
#!/bin/bash
int=1
while(( $int<=5 ))
do
    echo $int
    let "int++"
done
```

### 9.3.1. 无限循环

无限循环语法格式：

```bash
while :
do
    command
done
```

或者

```bash
while true
do
    command
done
```

或者

```bash
for (( ; ; ))
```

------

## 9.4. until 循环

until 循环执行一系列命令直至条件为 true 时停止。

until 循环与 while 循环在处理方式上刚好相反。

一般 while 循环优于 until 循环，但在某些时候—也只是极少数情况下，until 循环更加有用。

## 9.5. case

Shell case语句为多选择语句。可以用case语句匹配一个值与一个模式，如果匹配成功，执行相匹配的命令。case语句格式如下：

```bash
case 值 in
模式1)
    command1
    command2
    ...
    commandN
    ;;
模式2）
    command1
    command2
    ...
    commandN
    ;;
esac
```

case工作方式如上所示。取值后面必须为单词in，每一模式必须以右括号结束。取值可以为变量或常数。匹配发现取值符合某一模式后，其间所有命令开始执行直至 ;;。

取值将检测匹配的每一个模式。一旦模式匹配，则执行完匹配模式相应命令后不再继续其他模式。如果无一匹配模式，使用星号 * 捕获该值，再执行后面的命令。

## 9.6. 跳出循环

在循环过程中，有时候需要在未达到循环结束条件时强制跳出循环，Shell使用两个命令来实现该功能：break和continue。

### 9.6.1. break命令

break命令允许跳出所有循环（终止执行后面的所有循环）。

### 9.6.2. continue

continue命令与break命令类似，只有一点差别，它不会跳出所有循环，仅仅跳出当前循环。

# 10. 函数

linux shell 可以用户定义函数，然后在shell脚本中可以随便调用。

shell中函数的定义格式如下：

```bash
[ function ] funname [()]

{

    action;

    [return int;]

}
```

说明：

1. 可以带function fun() 定义，也可以直接fun() 定义，不带任何参数。
2. 参数返回，可以显示加：return 返回，如果不加，将以最后一条命令运行结果，作为返回值。 return后跟数值n(0-255

**函数返回值在调用该函数后通过 $? 来获得**。

**注意**：**所有函数在使用前必须定义**。这意味着必须将函数放在脚本开始部分，直至shell解释器首次发现它时，才可以使用。调用函数仅使用其函数名即可。

## 10.1. 函数参数

在Shell中，调用函数时可以向其传递参数。在函数体内部，通过 $n 的形式来获取参数的值，例如，$1表示第一个参数，$2表示第二个参数...

带参数的函数示例：

```bash
#!/bin/bash
# author:菜鸟教程
# url:www.runoob.com

funWithParam(){
    echo "第一个参数为 $1 !"
    echo "第二个参数为 $2 !"
    echo "第十个参数为 $10 !"
    echo "第十个参数为 ${10} !"
    echo "第十一个参数为 ${11} !"
    echo "参数总数有 $# 个!"
    echo "作为一个字符串输出所有参数 $* !"
}
funWithParam 1 2 3 4 5 6 7 8 9 34 73
```

输出结果：

```
第一个参数为 1 !
第二个参数为 2 !
第十个参数为 10 !
第十个参数为 34 !
第十一个参数为 73 !
参数总数有 11 个!
作为一个字符串输出所有参数 1 2 3 4 5 6 7 8 9 34 73 !
```

**注意，$10 不能获取第十个参数，获取第十个参数需要${10}。当n>=10时，需要使用${n}来获取参数**。

另外，还有几个特殊字符用来处理参数：

| 参数处理 | 说明                                                         |
| :------- | :----------------------------------------------------------- |
| $#       | 传递到脚本或函数的参数个数                                   |
| $*       | 以一个单字符串显示所有向脚本传递的参数                       |
| $$       | 脚本运行的当前进程ID号                                       |
| $!       | 后台运行的最后一个进程的ID号                                 |
| $@       | 与$*相同，但是使用时加引号，并在引号中返回每个参数。         |
| $-       | 显示Shell使用的当前选项，与set命令功能相同。                 |
| $?       | 显示最后命令的退出状态。0表示没有错误，其他任何值表明有错误。 |

# 11. 输入/输出重定向

大多数 UNIX 系统命令从你的终端接受输入并将所产生的输出发送回到您的终端。一个命令通常从一个叫标准输入的地方读取输入，默认情况下，这恰好是你的终端。同样，一个命令通常将其输出写入到标准输出，默认情况下，这也是你的终端。

重定向命令列表如下：

| 命令            | 说明                                               |
| :-------------- | :------------------------------------------------- |
| command > file  | 将输出重定向到 file。                              |
| command < file  | 将输入重定向到 file。                              |
| command >> file | 将输出以**追加**的方式重定向到 file。              |
| n > file        | 将文件描述符为 n 的文件重定向到 file。             |
| n >> file       | 将文件描述符为 n 的文件以追加的方式重定向到 file。 |
| n >& m          | 将输出文件 m 和 n 合并。                           |
| n <& m          | 将输入文件 m 和 n 合并。                           |
| << tag          | 将开始标记 tag 和结束标记 tag 之间的内容作为输入。 |

**需要注意的是文件描述符 0 通常是标准输入（STDIN），1 是标准输出（STDOUT），2 是标准错误输出（STDERR）。**



------

## 11.1. Here Document

Here Document 是 Shell 中的一种特殊的重定向方式，用来将输入重定向到一个交互式 Shell 脚本或程序。

它的基本的形式如下：

```bash
command << delimiter
    document
delimiter
```

它的作用是将两个 delimiter 之间的内容(document) 作为输入传递给 command。

注意：

- 结尾的delimiter 一定要顶格写，前面不能有任何字符，后面也不能有任何字符，包括空格和 tab 缩进。
- 开始的delimiter前后的空格会被忽略掉。

在命令行中通过 **wc -l** 命令计算 Here Document 的行数：

```bash
$ wc -l << EOF
    欢迎来到
    菜鸟教程
    www.runoob.com
EOF
3          # 输出结果为 3 行
$
```

我们也可以将 Here Document 用在脚本中，例如：

```bash
#!/bin/bash
# author:菜鸟教程
# url:www.runoob.com

cat << EOF
欢迎来到
菜鸟教程
www.runoob.com
EOF
```

执行以上脚本，输出结果：

```bash
欢迎来到
菜鸟教程
www.runoob.com
```

------

## 11.2. /dev/null 文件

如果希望执行某个命令，但又不希望在屏幕上显示输出结果，那么可以将输出重定向到 /dev/null：

```bash
$ command > /dev/null
```

/dev/null 是一个特殊的文件，写入到它的内容都会被丢弃；如果尝试从该文件读取内容，那么什么也读不到。但是 /dev/null 文件非常有用，将命令的输出重定向到它，会起到"禁止输出"的效果。

如果希望屏蔽 stdout 和 stderr，可以这样写：

```bash
$ command > /dev/null 2>&1
```

**注意：**0 是标准输入（STDIN），1 是标准输出（STDOUT），2 是标准错误输出（STDERR）。

这里的 **2** 和 **>** 之间不可以有空格，**2>** 是一体的时候才表示错误输出。

# 12. 文件包含

和其他语言一样，Shell 也可以包含外部脚本。这样可以很方便的封装一些公用的代码作为一个独立的文件。

Shell 文件包含的语法格式如下：

```bash
. filename   # 注意点号(.)和文件名中间有一空格
或
source filename
```

创建两个 shell 脚本文件。

test1.sh 代码如下：

```bash
#!/bin/bash
# author:菜鸟教程
# url:www.runoob.com

url="http://www.runoob.com"
```

test2.sh 代码如下：

```bash
#!/bin/bash
# author:菜鸟教程
# url:www.runoob.com

#使用 . 号来引用test1.sh 文件
. ./test1.sh

# 或者使用以下包含文件代码
# source ./test1.sh

echo "菜鸟教程官网地址：$url"
```

接下来，我们为 test2.sh 添加可执行权限并执行：

```bash
$ chmod +x test2.sh 
$ ./test2.sh 
菜鸟教程官网地址：http://www.runoob.com
```

**注：被包含的文件 test1.sh 不需要可执行权限**。



