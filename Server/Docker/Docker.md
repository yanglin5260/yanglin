<h2>目录</h2>

<details open>
  <summary><a href="#1-docker-架构">1. Docker 架构</a></summary>
  <ul>
    <a href="#11-初始必读">1.1. 初始必读</a><br>
  <details open>
    <summary><a href="#12-mysql主从同步中的server-id示例详解">1.2. MySQL主从同步中的server-id示例详解</a>  </summary>
    <ul>
      <a href="#121-前言">1.2.1. 前言</a><br>
      <a href="#122-server-id配置">1.2.2. server-id配置</a><br>
      <a href="#123-server-id用途">1.2.3. server-id用途</a><br>
      <a href="#124-server-uuid配置">1.2.4. server-uuid配置</a><br>
    </ul>
  </details>
    <a href="#13-简介">1.3. 简介</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#2-安装步骤">2. 安装步骤</a></summary>
  <ul>
  <details open>
    <summary><a href="#21-windows-docker-安装">2.1. Windows Docker 安装</a>  </summary>
    <ul>
      <a href="#211-win7、win8-系统">2.1.1. win7、win8 系统</a><br>
      <a href="#212-win10-系统">2.1.2. Win10 系统</a><br>
    <details open>
      <summary><a href="#213-开启-hyper-v">2.1.3. 开启 Hyper-V</a>    </summary>
      <ul>
        <a href="#2131-安装-toolbox">2.1.3.1. 安装 Toolbox</a><br>
        <a href="#2132-运行安装文件">2.1.3.2. 运行安装文件</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#214-镜像加速">2.1.4. 镜像加速</a>    </summary>
      <ul>
        <a href="#2141-windows-10">2.1.4.1. Windows 10</a><br>
      </ul>
    </details>
    </ul>
  </details>
  <details open>
    <summary><a href="#22-macos-docker-安装">2.2. MacOS Docker 安装</a>  </summary>
    <ul>
      <a href="#221-手动下载安装">2.2.1. 手动下载安装</a><br>
      <a href="#222-镜像加速">2.2.2. 镜像加速</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#23-centos8-docker-安装">2.3. CentOS8 Docker 安装</a>  </summary>
    <ul>
      <a href="#231-安装前准备">2.3.1. 安装前准备</a><br>
      <a href="#232-使用官方安装脚本自动安装">2.3.2. 使用官方安装脚本自动安装</a><br>
    <details open>
      <summary><a href="#233-手动安装">2.3.3. 手动安装</a>    </summary>
      <ul>
        <a href="#2331-卸载旧版本">2.3.3.1. 卸载旧版本</a><br>
      <details open>
        <summary><a href="#2332-手动安装前准备">2.3.3.2. 手动安装前准备</a>      </summary>
        <ul>
          <a href="#23321-使用-docker-仓库进行安装">2.3.3.2.1. 使用 Docker 仓库进行安装</a><br>
          <a href="#23322-使用官方源地址（比较慢）">2.3.3.2.2. 使用官方源地址（比较慢）</a><br>
          <a href="#23323-阿里云">2.3.3.2.3. 阿里云</a><br>
          <a href="#23324-清华大学源">2.3.3.2.4. 清华大学源</a><br>
        </ul>
      </details>
        <a href="#2333-安装-docker-engine-community">2.3.3.3. 安装 Docker Engine-Community</a><br>
      </ul>
    </details>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#3-docker-hello-world">3. Docker Hello World</a></summary>
  <ul>
    <a href="#31-docker-hello-world">3.1. Docker Hello World</a><br>
    <a href="#32-运行交互式的容器">3.2. 运行交互式的容器</a><br>
    <a href="#33-启动容器（后台模式）">3.3. 启动容器（后台模式）</a><br>
    <a href="#34-停止容器">3.4. 停止容器</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#4-docker-容器使用">4. Docker 容器使用</a></summary>
  <ul>
    <a href="#41-docker-客户端">4.1. Docker 客户端</a><br>
    <a href="#42-运行一个web应用">4.2. 运行一个web应用</a><br>
    <a href="#43-查看-web-应用容器">4.3. 查看 WEB 应用容器</a><br>
    <a href="#44-网络端口的快捷方式">4.4. 网络端口的快捷方式</a><br>
    <a href="#45-查看web应用程序日志">4.5. 查看WEB应用程序日志</a><br>
    <a href="#46-查看web应用程序容器的进程">4.6. 查看WEB应用程序容器的进程</a><br>
    <a href="#47-检查web应用程序">4.7. 检查WEB应用程序</a><br>
    <a href="#48-停止web应用容器">4.8. 停止WEB应用容器</a><br>
    <a href="#49-重启web应用容器">4.9. 重启WEB应用容器</a><br>
    <a href="#410-移除web应用容器">4.10. 移除WEB应用容器</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#5-docker-镜像使用">5. Docker 镜像使用</a></summary>
  <ul>
    <a href="#51-docker-镜像使用">5.1. Docker 镜像使用</a><br>
    <a href="#52-列出镜像列表">5.2. 列出镜像列表</a><br>
    <a href="#53-获取一个新的镜像">5.3. 获取一个新的镜像</a><br>
    <a href="#54-查找镜像">5.4. 查找镜像</a><br>
    <a href="#55-拖取镜像">5.5. 拖取镜像</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#6-docker-容器连接">6. Docker 容器连接</a></summary>
  <ul>
    <a href="#61-网络端口映射">6.1. 网络端口映射</a><br>
  <details open>
    <summary><a href="#62-docker-容器互联">6.2. Docker 容器互联</a>  </summary>
    <ul>
      <a href="#621-容器命名">6.2.1. 容器命名</a><br>
      <a href="#622-新建网络">6.2.2. 新建网络</a><br>
      <a href="#623-连接容器">6.2.3. 连接容器</a><br>
    </ul>
  </details>
    <a href="#63-配置-dns">6.3. 配置 DNS</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#7-docker-仓库管理">7. Docker 仓库管理</a></summary>
  <ul>
  <details open>
    <summary><a href="#71-docker-hub">7.1. Docker Hub</a>  </summary>
    <ul>
      <a href="#711-注册">7.1.1. 注册</a><br>
      <a href="#712-登录和退出">7.1.2. 登录和退出</a><br>
      <a href="#713-推送镜像">7.1.3. 推送镜像</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#8-docker-dockerfile">8. Docker Dockerfile</a></summary>
  <ul>
    <a href="#81-什么是-dockerfile？">8.1. 什么是 Dockerfile？</a><br>
    <a href="#82-使用-dockerfile-定制镜像">8.2. 使用 Dockerfile 定制镜像</a><br>
    <a href="#83-开始构建镜像">8.3. 开始构建镜像</a><br>
    <a href="#84-上下文路径">8.4. 上下文路径</a><br>
  <details open>
    <summary><a href="#85-指令详解">8.5. 指令详解</a>  </summary>
    <ul>
      <a href="#851-copy">8.5.1. COPY</a><br>
      <a href="#852-add">8.5.2. ADD</a><br>
      <a href="#853-cmd">8.5.3. CMD</a><br>
      <a href="#854-entrypoint">8.5.4. ENTRYPOINT</a><br>
      <a href="#855-env">8.5.5. ENV</a><br>
      <a href="#856-arg">8.5.6. ARG</a><br>
      <a href="#857-volume">8.5.7. VOLUME</a><br>
      <a href="#858-expose">8.5.8. EXPOSE</a><br>
      <a href="#859-workdir">8.5.9. WORKDIR</a><br>
      <a href="#8510-user">8.5.10. USER</a><br>
      <a href="#8511-healthcheck">8.5.11. HEALTHCHECK</a><br>
      <a href="#8512-onbuild">8.5.12. ONBUILD</a><br>
      <a href="#8513-label">8.5.13. LABEL</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#9-docker-compose">9. Docker Compose</a></summary>
  <ul>
    <a href="#91-compose-简介">9.1. Compose 简介</a><br>
    <a href="#92-yaml-配置实例">9.2. yaml 配置实例</a><br>
  <details open>
    <summary><a href="#93-compose-安装">9.3. Compose 安装</a>  </summary>
    <ul>
      <a href="#931-linux">9.3.1. Linux</a><br>
      <a href="#932-macos">9.3.2. macOS</a><br>
      <a href="#933-windows-pc">9.3.3. windows PC</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#94-使用">9.4. 使用</a>  </summary>
    <ul>
      <a href="#941-准备">9.4.1. 准备</a><br>
      <a href="#942-创建-dockerfile-文件">9.4.2. 创建 Dockerfile 文件</a><br>
      <a href="#943-创建-docker-composeyml">9.4.3. 创建 docker-compose.yml</a><br>
      <a href="#944-使用-compose-命令构建和运行您的应用">9.4.4. 使用 Compose 命令构建和运行您的应用</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#95-yml-配置指令参考">9.5. yml 配置指令参考</a>  </summary>
    <ul>
      <a href="#951-version">9.5.1. version</a><br>
      <a href="#952-build">9.5.2. build</a><br>
      <a href="#953-cap_add，cap_drop">9.5.3. cap_add，cap_drop</a><br>
      <a href="#954-cgroup_parent">9.5.4. cgroup_parent</a><br>
      <a href="#955-command">9.5.5. command</a><br>
      <a href="#956-container_name">9.5.6. container_name</a><br>
      <a href="#957-depends_on">9.5.7. depends_on</a><br>
      <a href="#958-deploy">9.5.8. deploy</a><br>
      <a href="#959-devices">9.5.9. devices</a><br>
      <a href="#9510-dns">9.5.10. dns</a><br>
      <a href="#9511-dns_search">9.5.11. dns_search</a><br>
      <a href="#9512-entrypoint">9.5.12. entrypoint</a><br>
      <a href="#9513-env_file">9.5.13. env_file</a><br>
      <a href="#9514-environment">9.5.14. environment</a><br>
      <a href="#9515-expose">9.5.15. expose</a><br>
      <a href="#9516-extra_hosts">9.5.16. extra_hosts</a><br>
      <a href="#9517-healthcheck">9.5.17. healthcheck</a><br>
      <a href="#9518-image">9.5.18. image</a><br>
      <a href="#9519-logging">9.5.19. logging</a><br>
      <a href="#9520-network_mode">9.5.20. network_mode</a><br>
      <a href="#9521-restart">9.5.21. restart</a><br>
      <a href="#9522-secrets">9.5.22. secrets</a><br>
      <a href="#9523-security_opt">9.5.23. security_opt</a><br>
      <a href="#9524-stop_grace_period">9.5.24. stop_grace_period</a><br>
      <a href="#9525-stop_signal">9.5.25. stop_signal</a><br>
      <a href="#9526-sysctls">9.5.26. sysctls</a><br>
      <a href="#9527-tmpfs">9.5.27. tmpfs</a><br>
      <a href="#9528-ulimits">9.5.28. ulimits</a><br>
      <a href="#9529-volumes">9.5.29. volumes</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#10-docker-machine">10. Docker Machine</a></summary>
  <ul>
    <a href="#101-简介">10.1. 简介</a><br>
  <details open>
    <summary><a href="#102-安装">10.2. 安装</a>  </summary>
    <ul>
      <a href="#1021-linux-安装命令">10.2.1. Linux 安装命令</a><br>
      <a href="#1022-macos-安装命令">10.2.2. macOS 安装命令</a><br>
      <a href="#1023-windows-安装命令">10.2.3. Windows 安装命令</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#103-使用">10.3. 使用</a>  </summary>
    <ul>
      <a href="#1031-列出可用的机器">10.3.1. 列出可用的机器</a><br>
      <a href="#1032-创建机器">10.3.2. 创建机器</a><br>
      <a href="#1033-查看机器的-ip">10.3.3. 查看机器的 ip</a><br>
      <a href="#1034-停止机器">10.3.4. 停止机器</a><br>
      <a href="#1035-启动机器">10.3.5. 启动机器</a><br>
      <a href="#1036-进入机器">10.3.6. 进入机器</a><br>
      <a href="#1037-docker-machine-命令参数说明">10.3.7. docker-machine 命令参数说明</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#11-swarm-集群管理">11. Swarm 集群管理</a></summary>
  <ul>
    <a href="#111-简介">11.1. 简介</a><br>
    <a href="#112-原理">11.2. 原理</a><br>
  <details open>
    <summary><a href="#113-使用">11.3. 使用</a>  </summary>
    <ul>
      <a href="#1131-1、创建-swarm-集群管理节点（manager）">11.3.1. 1、创建 swarm 集群管理节点（manager）</a><br>
      <a href="#1132-2、创建-swarm-集群工作节点（worker）">11.3.2. 2、创建 swarm 集群工作节点（worker）</a><br>
      <a href="#1133-3、查看集群信息">11.3.3. 3、查看集群信息</a><br>
      <a href="#1134-4、部署服务到集群中">11.3.4. 4、部署服务到集群中</a><br>
      <a href="#1135-5、查看服务部署情况">11.3.5. 5、查看服务部署情况</a><br>
      <a href="#1136-6、扩展集群服务">11.3.6. 6、扩展集群服务</a><br>
      <a href="#1137-7、删除服务">11.3.7. 7、删除服务</a><br>
      <a href="#1138-8、滚动升级服务">11.3.8. 8、滚动升级服务</a><br>
      <a href="#1139-9、停止某个节点接收新的任务">11.3.9. 9、停止某个节点接收新的任务</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#12-docker-命令大全">12. Docker 命令大全</a></summary>
  <ul>
  <details open>
    <summary><a href="#121-容器生命周期管理">12.1. 容器生命周期管理</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#1211-run">12.1.1. run</a>    </summary>
      <ul>
        <a href="#12111-语法">12.1.1.1. 语法</a><br>
        <a href="#12112-实例">12.1.1.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1212-startstoprestart">12.1.2. start/stop/restart</a>    </summary>
      <ul>
        <a href="#12121-语法">12.1.2.1. 语法</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1213-kill">12.1.3. kill</a>    </summary>
      <ul>
        <a href="#12131-语法">12.1.3.1. 语法</a><br>
        <a href="#12132-实例">12.1.3.2. 实例</a><br>
        <a href="#12133-stop和kill的却别">12.1.3.3. stop和kill的却别</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1214-rm">12.1.4. rm</a>    </summary>
      <ul>
        <a href="#12141-语法">12.1.4.1. 语法</a><br>
        <a href="#12142-实例">12.1.4.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1215-pauseunpause">12.1.5. pause/unpause</a>    </summary>
      <ul>
        <a href="#12151-语法">12.1.5.1. 语法</a><br>
        <a href="#12152-实例">12.1.5.2. 实例</a><br>
        <a href="#12153-stop与pause的区别">12.1.5.3. stop与pause的区别</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1216-create">12.1.6. create</a>    </summary>
      <ul>
        <a href="#12161-语法">12.1.6.1. 语法</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1217-exec">12.1.7. exec</a>    </summary>
      <ul>
        <a href="#12171-语法">12.1.7.1. 语法</a><br>
        <a href="#12172-实例">12.1.7.2. 实例</a><br>
      </ul>
    </details>
    </ul>
  </details>
  <details open>
    <summary><a href="#122-容器操作">12.2. 容器操作</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#1221-ps">12.2.1. ps</a>    </summary>
      <ul>
        <a href="#12211-语法">12.2.1.1. 语法</a><br>
        <a href="#12212-实例">12.2.1.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1222-inspect">12.2.2. inspect</a>    </summary>
      <ul>
        <a href="#12221-语法">12.2.2.1. 语法</a><br>
        <a href="#12222-实例">12.2.2.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1223-top">12.2.3. top</a>    </summary>
      <ul>
        <a href="#12231-语法">12.2.3.1. 语法</a><br>
        <a href="#12232-实例">12.2.3.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1224-attach">12.2.4. attach</a>    </summary>
      <ul>
        <a href="#12241-语法">12.2.4.1. 语法</a><br>
        <a href="#12242-实例">12.2.4.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1225-events">12.2.5. events</a>    </summary>
      <ul>
        <a href="#12251-语法">12.2.5.1. 语法</a><br>
        <a href="#12252-实例">12.2.5.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1226-logs">12.2.6. logs</a>    </summary>
      <ul>
        <a href="#12261-语法">12.2.6.1. 语法</a><br>
        <a href="#12262-实例">12.2.6.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1227-wait">12.2.7. wait</a>    </summary>
      <ul>
        <a href="#12271-语法">12.2.7.1. 语法</a><br>
        <a href="#12272-实例">12.2.7.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1228-export">12.2.8. export</a>    </summary>
      <ul>
        <a href="#12281-语法">12.2.8.1. 语法</a><br>
        <a href="#12282-实例">12.2.8.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1229-port">12.2.9. port</a>    </summary>
      <ul>
        <a href="#12291-语法">12.2.9.1. 语法</a><br>
        <a href="#12292-实例">12.2.9.2. 实例</a><br>
      </ul>
    </details>
    </ul>
  </details>
  <details open>
    <summary><a href="#123-容器rootfs命令">12.3. 容器rootfs命令</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#1231-commit">12.3.1. commit</a>    </summary>
      <ul>
        <a href="#12311-语法">12.3.1.1. 语法</a><br>
        <a href="#12312-实例">12.3.1.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1232-cp">12.3.2. cp</a>    </summary>
      <ul>
        <a href="#12321-语法">12.3.2.1. 语法</a><br>
        <a href="#12322-实例">12.3.2.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1233-diff">12.3.3. diff</a>    </summary>
      <ul>
        <a href="#12331-语法">12.3.3.1. 语法</a><br>
        <a href="#12332-实例">12.3.3.2. 实例</a><br>
      </ul>
    </details>
    </ul>
  </details>
  <details open>
    <summary><a href="#124-镜像仓库">12.4. 镜像仓库</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#1241-loginlogout">12.4.1. login/logout</a>    </summary>
      <ul>
        <a href="#12411-语法">12.4.1.1. 语法</a><br>
        <a href="#12412-实例">12.4.1.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1242-pull">12.4.2. pull</a>    </summary>
      <ul>
        <a href="#12421-语法">12.4.2.1. 语法</a><br>
        <a href="#12422-实例">12.4.2.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1243-search">12.4.3. search</a>    </summary>
      <ul>
        <a href="#12431-语法">12.4.3.1. 语法</a><br>
        <a href="#12432-实例">12.4.3.2. 实例</a><br>
      </ul>
    </details>
    </ul>
  </details>
  <details open>
    <summary><a href="#125-本地镜像管理">12.5. 本地镜像管理</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#1251-images">12.5.1. images</a>    </summary>
      <ul>
        <a href="#12511-语法">12.5.1.1. 语法</a><br>
        <a href="#12512-实例">12.5.1.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1252-rmi">12.5.2. rmi</a>    </summary>
      <ul>
        <a href="#12521-语法">12.5.2.1. 语法</a><br>
        <a href="#12522-实例">12.5.2.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1253-tag">12.5.3. tag</a>    </summary>
      <ul>
        <a href="#12531-语法">12.5.3.1. 语法</a><br>
        <a href="#12532-实例">12.5.3.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1254-build">12.5.4. build</a>    </summary>
      <ul>
        <a href="#12541-语法">12.5.4.1. 语法</a><br>
        <a href="#12542-实例">12.5.4.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1255-history">12.5.5. history</a>    </summary>
      <ul>
        <a href="#12551-语法">12.5.5.1. 语法</a><br>
        <a href="#12552-实例">12.5.5.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1256-save">12.5.6. save</a>    </summary>
      <ul>
        <a href="#12561-语法">12.5.6.1. 语法</a><br>
        <a href="#12562-实例">12.5.6.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1257-load">12.5.7. load</a>    </summary>
      <ul>
        <a href="#12571-语法">12.5.7.1. 语法</a><br>
        <a href="#12572-实例">12.5.7.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1258-import">12.5.8. import</a>    </summary>
      <ul>
        <a href="#12581-语法">12.5.8.1. 语法</a><br>
        <a href="#12582-实例">12.5.8.2. 实例</a><br>
      </ul>
    </details>
    </ul>
  </details>
  <details open>
    <summary><a href="#126-infoversion">12.6. info|version</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#1261-info">12.6.1. info</a>    </summary>
      <ul>
        <a href="#12611-语法">12.6.1.1. 语法</a><br>
        <a href="#12612-实例">12.6.1.2. 实例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1262-version">12.6.2. version</a>    </summary>
      <ul>
        <a href="#12621-语法">12.6.2.1. 语法</a><br>
        <a href="#12622-实例">12.6.2.2. 实例</a><br>
      </ul>
    </details>
    </ul>
  </details>
  </ul>
</details>


# 1. Docker 架构

## 1.1. 初始必读

在Mac上使用`brew install --cask docker`命令下载并安装docker（这是Mac上图形化的安装，可能有些慢，安装好后需要手动启动），下载慢的话可以试试手动下载并手动安装（http://mirrors.aliyun.com/docker-toolbox/mac/docker-for-mac/stable/amd64/Docker.dmg，阿里云镜像）



**查询Docker所有镜像的tag的命令脚本**

[docker-tag.sh](./docker-tag.sh)

笔记地址：https://blog.csdn.net/CodyGuo/article/details/86515354



MySQL8集群搭建的参考网址：

https://honglimin.cn/mysql/243.html

https://blog.csdn.net/a314687289/article/details/112270828

https://juejin.cn/post/6850418110856429581

https://github.com/techial1042/Blog/issues/68

https://www.i4k.xyz/article/qq_38900565/114832445

https://juejin.cn/post/6844903925154316296

https://zhuanlan.zhihu.com/p/101666729

## 1.2. MySQL主从同步中的server-id示例详解

### 1.2.1. 前言

当我们搭建MySQL集群时，自然需要完成数据库的主从同步来保证数据一致性。而主从同步的方式也分很多种，一主多从、链式主从、多主多从，根据你的需要来进行设置。但只要你需要主从同步，就一定要注意server-id的配置，否则会出现主从复制异常。

在控制数据库数据复制和日志管理中，有两个重要的配置：server-id和server-uuid，他们会影响二进制日志文件记录和全局事务标识。

### 1.2.2. server-id配置

当你使用主从拓扑时，一定要对所有MySQL实例都分别指定一个独特的互不相同的server-id。默认值为0，当server-id=0时，对于主机来说依然会记录二进制日志，但会拒绝所有的从机连接；对于从机来说则会拒绝连接其它实例。

MySQL实例的server-id是一个全局变量，可以直接查看：

```
mysql> show variables like '%server_id%';
+---------------+-----------+
| Variable_name | Value |
+---------------+-----------+
| server_id | 171562767 |
+---------------+-----------+
1 row in set (0.00 sec)
```

我们可以在线直接修改全局变量server-id，但不会立即生效，所以修改后记得重启服务。而重启后又会重新读取系统配置文件配置，导致刚才的修改失效，因此建议修改配置文件后重启服务而不是在线修改。

### 1.2.3. server-id用途

server-id用于标识数据库实例，防止在链式主从、多主多从拓扑中导致SQL语句的无限循环：

- 标记binlog event的源实例
- 过滤主库binlog，当发现server-id相同时，跳过该event执行，避免无限循环执行。
- 如果设置了replicate-same-server-id=1，则执行所有event，但有可能导致无限循环执行SQL语句。

我们用两个例子来说明server-id为什么不要重复：

1. 当主库和备库server-id重复时

   由于默认情况replicate-same-server-id=0，因此备库会跳过所有主库同步的数据，导致主从数据的不一致。

2. 当两个备库server-id重复时

   会导致从库跟主库的连接时断时连，产生大量异常。根据MySQL的设计，主库和从库通过事件机制进行连接和同步，当新的连接到来时，**如果发现server-id相同，主库会断开之前的连接并重新注册新连接。当A库连接上主库时，此时B库连接到来，会断开A库连接，A库再进行重连，周而复始导致大量异常信息**。



**生成server-id的规则**

既然server-id不能相同，而当我们有10个实例时，怎么保证每个都不同呢？有几种常用的方法：

- 随机数
- 时间戳
- IP地址+端口
- 在管理中心集中分配，生成自增ID

上面的这些方法都可以，但是注意不要超过了最大值2^32-1，同时值最好>2。我采用的方法是IP地址后两位+本机MySQL实例序号，但如果是通过docker来进行管理多实例时，这个怎么生成大家可以想下有没有什么优美的解决方案。

### 1.2.4. server-uuid配置

MySQL服务会自动创建并生成server-uuid配置：

- 读取${data_dir}/auto.cnf文件中的UUID
- 如果不存在，自动创建文件和生成新的UUID并读取

```
shell> cat ~/mysql/data/auto.cnf
[auto]
server-uuid=fd5d03bc-cfde-11e9-ae59-48d539355108
```

这个auto.cnf配置风格类似于my.cnf，但这个文件只包含一个auto配置块和一行server-uuid配置。它是自动创建的，因此不要修改它的内容。

在主从拓扑中，主从可以知道互相的UUID，在主机上使用show slave hosts，在从机上使用show slave status查看Master_UUID字段。

server-uuid参数并不能取代server-id，他们有不同的作用。当主从同步时如果主从实例的server-uuid相同会报错退出，不过我们可以通过设置replicate-same-server-id=1来避免报错（不推荐）。

## 1.3. 简介

![img](./images/Docker-1.png)

Docker 是一个开源的应用容器引擎，基于 [Go 语言](https://www.w3cschool.cn/go/go-tutorial.html) 并遵从Apache2.0协议开源。

Docker 可以让开发者打包他们的应用以及依赖包到一个轻量级、可移植的容器中，然后发布到任何流行的 Linux 机器上，也可以实现虚拟化。

容器是完全使用沙箱机制，相互之间不会有任何接口（类似 iPhone 的 app）,更重要的是容器性能开销极低。

Docker 包括三个基本概念:

- **镜像（Image）**：Docker 镜像（Image），就相当于是一个 root 文件系统。比如官方镜像 ubuntu:16.04 就包含了完整的一套 Ubuntu16.04 最小系统的 root 文件系统。
- **容器（Container）**：镜像（Image）和容器（Container）的关系，就像是面向对象程序设计中的类和实例一样，镜像是静态的定义，容器是镜像运行时的实体。容器可以被创建、启动、停止、删除、暂停等。
- **仓库（Repository）**：仓库可看着一个代码控制中心，用来保存镜像。

Docker 使用客户端-服务器 (C/S) 架构模式，使用远程API来管理和创建Docker容器。

Docker 容器通过 Docker 镜像来创建。

容器与镜像的关系类似于面向对象编程中的对象与类。

| Docker | 面向对象 |
| :----- | :------- |
| 容器   | 对象     |
| 镜像   | 类       |

![img](./images/Docker-2.png)

| 概念                   | 说明                                                         |
| :--------------------- | :----------------------------------------------------------- |
| Docker 镜像(Images)    | Docker 镜像是用于创建 Docker 容器的模板，比如 Ubuntu 系统。  |
| Docker 容器(Container) | 容器是独立运行的一个或一组应用，是镜像运行时的实体。         |
| Docker 客户端(Client)  | Docker 客户端通过命令行或者其他工具使用 Docker SDK (https://docs.docker.com/develop/sdk/) 与 Docker 的守护进程通信。 |
| Docker 主机(Host)      | 一个物理或者虚拟的机器用于执行 Docker 守护进程和容器。       |
| Docker Registry        | Docker 仓库用来保存镜像，可以理解为代码控制中的代码仓库。Docker Hub([https://hub.docker.com](https://hub.docker.com/)) 提供了庞大的镜像集合供使用。一个 Docker Registry 中可以包含多个仓库（Repository）；每个仓库可以包含多个标签（Tag）；每个标签对应一个镜像。通常，一个仓库会包含同一个软件不同版本的镜像，而标签就常用于对应该软件的各个版本。我们可以通过 **<仓库名>:<标签>** 的格式来指定具体是这个软件哪个版本的镜像。如果不给出标签，将以 **latest** 作为默认标签。 |
| Docker Machine         | Docker Machine是一个简化Docker安装的命令行工具，通过一个简单的命令行即可在相应的平台上安装Docker，比如VirtualBox、 Digital Ocean、Microsoft Azure。 |

# 2. 安装步骤

## 2.1. Windows Docker 安装

### 2.1.1. win7、win8 系统

win7、win8 等需要利用 docker toolbox 来安装，国内可以使用阿里云的镜像来下载，下载地址：http://mirrors.aliyun.com/docker-toolbox/windows/docker-toolbox/

安装比较简单，双击运行，点下一步即可，可以勾选自己需要的组件：

![img](./images/Docker-3.png)

docker toolbox 是一个工具集，它主要包含以下一些内容：

- Docker CLI - 客户端，用来运行 docker 引擎创建镜像和容器。
- Docker Machine - 可以让你在 Windows 的命令行中运行 docker 引擎命令。
- Docker Compose - 用来运行 docker-compose 命令。
- Kitematic - 这是 Docker 的 GUI 版本。
- Docker QuickStart shell - 这是一个已经配置好Docker的命令行环境。
- Oracle VM Virtualbox - 虚拟机。

下载完成之后直接点击安装，安装成功后，桌边会出现三个图标，如下图所示：

![img](./images/Docker-4.png)

点击 Docker QuickStart 图标来启动 Docker Toolbox 终端。



打开运行时候可能会报错：

到这个地址下载最新的[boot2docker.iso ](https://github.com/boot2docker/boot2docker/releases/download/v19.03.12/boot2docker.iso)https://github.com/boot2docker/boot2docker/releases

下载完后复制到C:\Users\Yanglin\.docker\machine\cache下后，重新点击 Docker QuickStart



如果系统显示 User Account Control 窗口来运行 VirtualBox 修改你的电脑，选择 Yes。

![img](./images/Docker-5.png)

**$** 符号那你可以输入以下命令来执行。

```
$ docker run hello-world
 Unable to find image 'hello-world:latest' locally
 Pulling repository hello-world
 91c95931e552: Download complete
 a8219747be10: Download complete
 Status: Downloaded newer image for hello-world:latest
 Hello from Docker.
 This message shows that your installation appears to be working correctly.

 To generate this message, Docker took the following steps:
  1. The Docker Engine CLI client contacted the Docker Engine daemon.
  2. The Docker Engine daemon pulled the "hello-world" image from the Docker Hub.
     (Assuming it was not already locally available.)
  3. The Docker Engine daemon created a new container from that image which runs the
     executable that produces the output you are currently reading.
  4. The Docker Engine daemon streamed that output to the Docker Engine CLI client, which sent it
     to your terminal.

 To try something more ambitious, you can run an Ubuntu container with:
  $ docker run -it ubuntu bash

 For more examples and ideas, visit:
  https://docs.docker.com/userguide/
```

------

### 2.1.2. Win10 系统

现在 Docker 有专门的 Win10 专业版系统的安装包，需要开启 Hyper-V。

### 2.1.3. 开启 Hyper-V

![img](./images/Docker-6.png)

程序和功能

![img](./images/Docker-7.png)

启用或关闭Windows功能

![img](./images/Docker-8.png)

选中Hyper-V

![img](./images/Docker-9.png)

#### 2.1.3.1. 安装 Toolbox

最新版 Toolbox 下载地址： 访问 https://www.docker.com/get-started，注册一个账号，然后登录。

点击 [Get started with Docker Desktop](https://hub.docker.com/?overlay=onboarding)，并下载 Windows 的版本，如果你还没有登录，会要求注册登录：

![img](./images/Docker-9.jpg)

![img](./images/Docker-10.png)

#### 2.1.3.2. 运行安装文件

双击下载的 Docker for Windows Installer 安装文件，一路 Next，点击 Finish 完成安装。

![img](./images/Docker-11.png)

![img](./images/Docker-12.png)

![img](./images/Docker-13.png)

桌边也会出现三个图标，入下图所示：

我们可以在命令行执行 docker version 来查看版本号，docker run hello-world 来载入测试镜像测试。

如果没启动，你可以在 Windows 搜索 Docker 来启动：

![img](./images/Docker-14.png)

启动后，也可以在通知栏上看到小鲸鱼图标：

![img](./images/Docker-15.png)

------

### 2.1.4. 镜像加速

#### 2.1.4.1. Windows 10

对于使用 Windows 10 的系统，在系统右下角托盘 Docker 图标内右键菜单选择 Settings，打开配置窗口后左侧导航菜单选择 Daemon。在 Registrymirrors 一栏中填写加速器地址 **https://registry.docker-cn.com** ，之后点击 Apply 保存后 Docker 就会重启并应用配置的镜像地址了。

![img](./images/Docker-16.png)

## 2.2. MacOS Docker 安装

### 2.2.1. 手动下载安装

如果需要手动下载，请点击以下链接下载 [Stable](https://download.docker.com/mac/stable/Docker.dmg) 或 [Edge](https://download.docker.com/mac/edge/Docker.dmg) 版本的 Docker for Mac。

如同 macOS 其它软件一样，安装也非常简单，双击下载的 .dmg 文件，然后将鲸鱼图标拖拽到 Application 文件夹即可。

![img](./images/Docker-17.png)

从应用中找到 Docker 图标并点击运行。可能会询问 macOS 的登陆密码，输入即可。

![img](./images/Docker-18.png)

点击顶部状态栏中的鲸鱼图标会弹出操作菜单。

![img](./images/Docker-19.png)

![img](./images/Docker-20.png)

第一次点击图标，可能会看到这个安装成功的界面，点击 "Got it!" 可以关闭这个窗口。

![img](./images/Docker-21.png)

启动终端后，通过命令可以检查安装后的 Docker 版本。

```bash
$ docker --version
Docker version 17.09.1-ce, build 19e2cf6
```

### 2.2.2. 镜像加速

鉴于国内网络问题，后续拉取 Docker 镜像十分缓慢，我们可以需要配置加速器来解决，我使用的是网易的镜像地址：**http://hub-mirror.c.163.com**。

在任务栏点击 Docker for mac 应用图标 -> Perferences... -> Daemon -> Registry mirrors。在列表中填写加速器地址即可。修改完成之后，点击 Apply & Restart 按钮，Docker 就会重启并应用配置的镜像地址了。

![img](./images/Docker-22.png)

之后我们可以通过 docker info 来查看是否配置成功。

```
$ docker info
...
Registry Mirrors:
 http://hub-mirror.c.163.com
Live Restore Enabled: false
```

## 2.3. CentOS8 Docker 安装

<h6>CentOS8 Docker 安装</h6>

Docker 支持以下的 64 位 CentOS 版本：

- CentOS 7
- CentOS 8
- 更高版本...

------

### 2.3.1. 安装前准备

可能会出现如下的错误：

```bash
package docker-ce-3:19.03.2-3.el7.x86_64 requires containerd.io >= 1.2.2-3, but none of the providers can be installed
- cannot install the best candidate for the job
- package containerd.io-1.2.2-3.3.el7.x86_64 is excluded
- package containerd.io-1.2.2-3.el7.x86_64 is excluded
- package containerd.io-1.2.4-3.1.el7.x86_64 is excluded
- package containerd.io-1.2.5-3.1.el7.x86_64 is excluded
- package containerd.io-1.2.6-3.3.el7.x86_64 is excluded
(try to add '--skip-broken' to skip uninstallable packages or '--nobest' to use not only best candidate packages)
```

先安装wget：

```
 yum -y install wget
```

再通过wget下载最新的containerd.io:

```bash
wget https://download.docker.com/linux/centos/7/x86_64/edge/Packages/containerd.io-1.2.10-3.2.el7.x86_64.rpm
```



### 2.3.2. 使用官方安装脚本自动安装

安装命令如下：

```
curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
```

也可以使用国内 daocloud 一键安装命令：

```
curl -sSL https://get.daocloud.io/docker | sh
```

设置开机启动 

```bash
systemctl enable docker
```



### 2.3.3. 手动安装

#### 2.3.3.1. 卸载旧版本

较旧的 Docker 版本称为 docker 或 docker-engine 。如果已安装这些程序，请卸载它们以及相关的依赖项。

$ **sudo** **yum remove** docker \
         docker-client \
         docker-client-latest \
         docker-common \
         docker-latest \
         docker-latest-logrotate \
         docker-logrotate \
         docker-engine

#### 2.3.3.2. 手动安装前准备

##### 2.3.3.2.1. 使用 Docker 仓库进行安装

在新主机上首次安装 Docker Engine-Community 之前，需要设置 Docker 仓库。之后，您可以从仓库安装和更新 Docker。

**设置仓库**

安装所需的软件包。yum-utils 提供了 yum-config-manager ，并且 device mapper 存储驱动程序需要 device-mapper-persistent-data 和 lvm2。

$ **sudo** **yum install** -y yum-utils \
 device-mapper-persistent-data \
 lvm2

使用以下命令来设置稳定的仓库。

##### 2.3.3.2.2. 使用官方源地址（比较慢）

$ **sudo** yum-config-manager \
  --add-repo \
  https:**//**download.docker.com**/**linux**/**centos**/**docker-ce.repo

可以选择国内的一些源地址：

##### 2.3.3.2.3. 阿里云

$ **sudo** yum-config-manager \
  --add-repo \
  http:**//**mirrors.aliyun.com**/**docker-ce**/**linux**/**centos**/**docker-ce.repo

##### 2.3.3.2.4. 清华大学源

$ **sudo** yum-config-manager \
  --add-repo \
  https:**//**mirrors.tuna.tsinghua.edu.cn**/**docker-ce**/**linux**/**centos**/**docker-ce.repo

#### 2.3.3.3. 安装 Docker Engine-Community

安装最新版本的 Docker Engine-Community 和 containerd，或者转到下一步安装特定版本：

```
$ sudo yum install docker-ce docker-ce-cli containerd.io
```

如果提示您接受 GPG 密钥，请选是。

> **有多个 Docker 仓库吗？**
>
> 如果启用了多个 Docker 仓库，则在未在 yum install 或 yum update 命令中指定版本的情况下，进行的安装或更新将始终安装最高版本，这可能不适合您的稳定性需求。

Docker 安装完默认未启动。并且已经创建好 docker 用户组，但该用户组下没有用户。

**要安装特定版本的 Docker Engine-Community，请在存储库中列出可用版本，然后选择并安装：**

1、列出并排序您存储库中可用的版本。此示例按版本号（从高到低）对结果进行排序。

$ **yum list** docker-ce --showduplicates **|** **sort** -r

docker-ce.x86_64  3:18.09.1-3.el7           docker-ce-stable
docker-ce.x86_64  3:18.09.0-3.el7           docker-ce-stable
docker-ce.x86_64  18.06.1.ce-3.el7           docker-ce-stable
docker-ce.x86_64  18.06.0.ce-3.el7           docker-ce-stable

2、通过其完整的软件包名称安装特定版本，该软件包名称是软件包名称（docker-ce）加上版本字符串（第二列），从第一个冒号（:）一直到第一个连字符，并用连字符（-）分隔。例如：docker-ce-18.09.1。

```
$ sudo yum install docker-ce-<VERSION_STRING> docker-ce-cli-<VERSION_STRING> containerd.io
```

启动 Docker。

```
$ sudo systemctl start docker
```

通过运行 hello-world 映像来验证是否正确安装了 Docker Engine-Community 。

```
$ sudo docker run hello-world
```

# 3. Docker Hello World

## 3.1. Docker Hello World

Docker 允许你在容器内运行应用程序， 使用 **docker run** 命令来在容器内运行一个应用程序。

输出Hello world

```
w3cschool@w3cschool:~$ docker run ubuntu:15.10 /bin/echo "Hello world"
Hello world
```

![img](./images/Docker-22.jpg)

各个参数解析：

- **docker:** Docker 的二进制执行文件。
- **run:**与前面的 docker 组合来运行一个容器。
- **ubuntu:15.10**指定要运行的镜像，Docker首先从本地主机上查找镜像是否存在，如果不存在，Docker 就会从镜像仓库 Docker Hub 下载公共镜像。
- **/bin/echo "Hello world":** 在启动的容器里执行的命令

以上命令完整的意思可以解释为：Docker 以 ubuntu15.10 镜像创建一个新容器，然后在容器里执行 bin/echo "Hello world"，然后输出结果。

------

## 3.2. 运行交互式的容器

我们通过docker的两个参数 -i -t，让docker运行的容器实现"对话"的能力

```
w3cschool@w3cschool:~$ docker run -i -t ubuntu:15.10 /bin/bash
root@dc0050c79503:/#
```

各个参数解析：

- **-t:**在新容器内指定一个伪终端或终端。（terminal）
- **-i:**允许你对容器内的标准输入 (STDIN) 进行交互。（interaction）

此时我们已进入一个 ubuntu15.10系统的容器

我们尝试在容器中运行命令 **cat /proc/version**和**ls**分别查看当前系统的版本信息和当前目录下的文件列表

![img](./images/Docker-23.jpg)

我们可以通过运行exit命令或者使用CTRL+D来退出容器。

------

## 3.3. 启动容器（后台模式）

使用以下命令创建一个以进程方式运行的容器

```
w3cschool@w3cschool:~$ docker run -d ubuntu:15.10 /bin/sh -c "while true; do echo hello world; sleep 1; done"
2b1b7a428627c51ab8810d541d759f072b4fc75487eed05812646b8534a2fe63
```

![img](./images/Docker-24.jpg)

在输出中，我们没有看到期望的"hello world"，而是一串长字符

**2b1b7a428627c51ab8810d541d759f072b4fc75487eed05812646b8534a2fe63**

这个长字符串叫做容器ID，对每个容器来说都是唯一的，我们可以通过容器ID来查看对应的容器发生了什么。

首先，我们需要确认容器有在运行，可以通过 **docker ps** 来查看

```
w3cschool@w3cschool:~$ docker ps
```

![img](./images/Docker-25.jpg)

**CONTAINER ID:**容器ID

**NAMES:**自动分配的容器名称

在容器内使用docker logs命令，查看容器内的标准输出

```
w3cschool@w3cschool:~$ docker logs 2b1b7a428627
```

![img](./images/Docker-26.jpg)

```
w3cschool@w3cschool:~$ docker logs amazing_cori
```

![img](./images/Docker-27.jpg)

------

## 3.4. 停止容器

我们使用 **docker stop** 命令来停止容器:

![img](./images/Docker-28.jpg)

通过docker ps查看，容器已经停止工作:

```
w3cschool@w3cschool:~$ docker ps
```

![img](./images/Docker-29.jpg)

也可以用下面的命令来停止:

```
w3cschool@w3cschool:~$ docker stop amazing_cori
```

# 4. Docker 容器使用

------

## 4.1. Docker 客户端

docker 客户端非常简单 ,我们可以直接输入 docker 命令来查看到 Docker 客户端的所有命令选项。

```
w3cschool@w3cschool:~# docker
```

![img](./images/Docker-30.jpg)

可以通过命令 **docker command --help** 更深入的了解指定的 Docker 命令使用方法。

例如我们要查看 **docker stats** 指令的具体使用方法：

```
w3cschool@w3cschool:~# docker stats --help
```

![img](./images/Docker-31.jpg)

------

## 4.2. 运行一个web应用

前面我们运行的容器并没有一些什么特别的用处。

接下来让我们尝试使用 docker 构建一个 web 应用程序。

我们将在docker容器中运行一个 Python Flask 应用来运行一个web应用。

```
w3cschool@w3cschool:~# docker run -d -P training/webapp python app.py
```

![img](./images/Docker-32.jpg)

参数说明:

- **-d:**让容器在后台运行。
- **-P:**将容器内部使用的网络端口映射到我们使用的主机上。

------

## 4.3. 查看 WEB 应用容器

使用 docker ps 来查看我们正在运行的容器

```
w3cschool@w3cschool:~$ docker ps
```

![img](./images/Docker-33.jpg)

这里多了端口信息。

```
PORTS
0.0.0.0:32769->5000/tcp
```

Docker 开放了 5000 端口（默认 Python Flask 端口）映射到主机端口 32769 上。

这时我们可以通过浏览器访问WEB应用

![img](./images/Docker-33.png)

我们也可以指定 -p 标识来绑定指定端口。

```
w3cschool@w3cschool:~$ docker run -d -p 5000:5000 training/webapp python app.py
```

**docker ps**查看正在运行的容器

![img](./images/Docker-34.jpg)

容器内部的 5000 端口映射到我们本地主机的 5000 端口上。

------

## 4.4. 网络端口的快捷方式

通过docker ps 命令可以查看到容器的端口映射，docker还提供了另一个快捷方式：docker port,使用 docker port 可以查看指定 （ID或者名字）容器的某个确定端口映射到宿主机的端口号。

上面我们创建的web应用容器ID为:7a38a1ad55c6 名字为：determined_swanson

我可以使用docker port 7a38a1ad55c6 或docker port determined_swanson来查看容器端口的映射情况

```
w3cschool@w3cschool:~$ docker port 7a38a1ad55c6
5000/tcp -> 0.0.0.0:5000
w3cschool@w3cschool:~$ docker port determined_swanson
5000/tcp -> 0.0.0.0:5000
```

------

## 4.5. 查看WEB应用程序日志

docker logs [ID或者名字] 可以查看容器内部的标准输出。

```
w3cschool@w3cschool:~$ docker logs -f 7a38a1ad55c6
 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
192.168.239.1 - - [09/May/2016 16:30:37] "GET / HTTP/1.1" 200 -
192.168.239.1 - - [09/May/2016 16:30:37] "GET /favicon.ico HTTP/1.1" 404 -
```

**-f:**让 **dokcer logs** 像使用 **tail -f** 一样来输出容器内部的标准输出。

从上面，我们可以看到应用程序使用的是 5000 端口并且能够查看到应用程序的访问日志。

------

## 4.6. 查看WEB应用程序容器的进程

我们还可以使用 docker top 来查看容器内部运行的进程

```
w3cschool@w3cschool:~$ docker top determined_swanson
```

![img](./images/Docker-35.jpg)

------

## 4.7. 检查WEB应用程序

使用 docker inspect 来查看Docker的底层信息。它会返回一个 JSON 文件记录着 Docker 容器的配置和状态信息。

```
w3cschool@w3cschool:~$ docker inspect determined_swanson
[
    {
        "Id": "7a38a1ad55c6914b360b565819604733db751d86afd2575236a70a2519527361",
        "Created": "2016-05-09T16:20:45.427996598Z",
        "Path": "python",
        "Args": [
            "app.py"
        ],
        "State": {
            "Status": "running",
......
```

------

## 4.8. 停止WEB应用容器

```
w3cschool@w3cschool:~$ docker stop determined_swanson   
determined_swanson
```

------

## 4.9. 重启WEB应用容器

已经停止的容器，我们可以使用命令 docker start 来启动。

```
w3cschool@w3cschool:~$ docker start determined_swanson
determined_swanson
```

docker ps -l 来查看正在运行的容器

![img](./images/Docker-36.jpg)

正在运行的容器，我们可以使用 docker restart 命令来重启

------

## 4.10. 移除WEB应用容器

我们可以使用 docker rm 命令来删除不需要的容器

```
w3cschool@w3cschool:~$ docker rm determined_swanson  
determined_swanson
```

删除容器时，容器必须是停止状态，否则会报如下错误

```
w3cschool@w3cschool:~$ docker rm determined_swanson
Error response from daemon: You cannot remove a running container 7a38a1ad55c6914b360b565819604733db751d86afd2575236a70a2519527361. Stop the container before attempting removal or use -f
```

# 5. Docker 镜像使用

## 5.1. Docker 镜像使用

当运行容器时，使用的镜像如果在本地中不存在，docker 就会自动从 docker 镜像仓库中下载，默认是从 Docker Hub 公共镜像源下载。

下面我们来学习：

- 1、管理和使用本地 Docker 主机镜像
- 2、创建镜像

------

## 5.2. 列出镜像列表

我们可以使用 **docker images** 来列出本地主机上的镜像。

```
w3cschool@w3cschool:~$ docker images           
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
ubuntu              14.04               90d5884b1ee0        5 days ago          188 MB
php                 5.6                 f40e9e0f10c8        9 days ago          444.8 MB
nginx               latest              6f8d099c3adc        12 days ago         182.7 MB
mysql               5.6                 f2e8d6c772c0        3 weeks ago         324.6 MB
httpd               latest              02ef73cf1bc0        3 weeks ago         194.4 MB
ubuntu              15.10               4e3b13c8a266        4 weeks ago         136.3 MB
hello-world         latest              690ed74de00f        6 months ago        960 B
training/webapp     latest              6fae60ef3446        11 months ago       348.8 MB
```

各个选项说明:

- **REPOSTITORY：**表示镜像的仓库源
- **TAG：**镜像的标签
- **IMAGE ID：**镜像ID
- **CREATED：**镜像创建时间
- **SIZE：**镜像大小

同一仓库源可以有多个 TAG，代表这个仓库源的不同个版本，如ubuntu仓库源里，有15.10、14.04等多个不同的版本，我们使用 REPOSTITORY:TAG 来定义不同的镜像。

所以，我们如果要使用版本为15.10的ubuntu系统镜像来运行容器时，命令如下：

```
w3cschool@w3cschool:~$ docker run -t -i ubuntu:15.10 /bin/bash 
root@d77ccb2e5cca:/#
```

如果要使用版本为14.04的ubuntu系统镜像来运行容器时，命令如下：

```
w3cschool@w3cschool:~$ docker run -t -i ubuntu:14.04 /bin/bash 
root@39e968165990:/# 
```

如果你不指定一个镜像的版本标签，例如你只使用 ubuntu，docker 将默认使用 ubuntu:latest 镜像。

------

## 5.3. 获取一个新的镜像

当我们在本地主机上使用一个不存在的镜像时 Docker 就会自动下载这个镜像。如果我们想预先下载这个镜像，我们可以使用 docker pull 命令来下载它。

```
Cw3cschool@w3cschool:~$ docker pull ubuntu:13.10
13.10: Pulling from library/ubuntu
6599cadaf950: Pull complete 
23eda618d451: Pull complete 
f0be3084efe9: Pull complete 
52de432f084b: Pull complete 
a3ed95caeb02: Pull complete 
Digest: sha256:15b79a6654811c8d992ebacdfbd5152fcf3d165e374e264076aa435214a947a3
Status: Downloaded newer image for ubuntu:13.10
```

下载完成后，我们可以直接使用这个镜像来运行容器。

------

## 5.4. 查找镜像

我们可以从 Docker Hub 网站来搜索镜像，Docker Hub 网址为：*https://hub.docker.com/*

我们也可以使用 docker search 命令来搜索镜像。比如我们需要一个httpd的镜像来作为我们的web服务。我们可以通过 docker search 命令搜索 httpd 来寻找适合我们的镜像。

```
w3cschool@w3cschool:~$  docker search httpd
```

![img](./images/Docker-37.jpg)

**NAME:**镜像仓库源的名称

**DESCRIPTION:**镜像的描述

**OFFICIAL:**是否docker官方发布

------

## 5.5. 拖取镜像

我们决定使用上图中的httpd 官方版本的镜像，使用命令 docker pull 来下载镜像。

```
w3cschool@w3cschool:~$ docker pull httpd
Using default tag: latest
latest: Pulling from library/ht
```

# 6. Docker 容器连接

容器中可以运行一些网络应用，要让外部也可以访问这些应用，可以通过 **-P** 或 **-p** 参数来指定端口映射。

下面我们来实现通过端口连接到一个 docker 容器。

------

## 6.1. 网络端口映射

我们使用 **-P** 绑定端口号，也可以使用 **-p** 标识来指定容器端口绑定到主机端口。

两种方式的区别是:

- **-P :**是容器内部端口**随机**映射到主机的高端口。
- **-p :** 是容器内部端口绑定到**指定**的主机端口。



另外，我们可以指定容器绑定的网络地址，比如绑定 127.0.0.1。

```
runoob@runoob:~$ docker run -d -p 127.0.0.1:5001:5000 training/webapp python app.py
95c6ceef88ca3e71eaf303c2833fd6701d8d1b2572b5613b5a932dfdfe8a857c
runoob@runoob:~$ docker ps
CONTAINER ID        IMAGE               COMMAND           ...     PORTS                                NAMES
95c6ceef88ca        training/webapp     "python app.py"   ...  5000/tcp, 127.0.0.1:5001->5000/tcp   adoring_stonebraker
```

这样我们就可以通过访问 127.0.0.1:5001 来访问容器的 5000 端口。

上面的例子中，默认都是绑定 tcp 端口，如果要绑定 UDP 端口，可以在端口后面加上 **/udp**。

```
runoob@runoob:~$ docker run -d -p 127.0.0.1:5000:5000/udp training/webapp python app.py
6779686f06f6204579c1d655dd8b2b31e8e809b245a97b2d3a8e35abe9dcd22a
runoob@runoob:~$ docker ps
CONTAINER ID        IMAGE               COMMAND           ...   PORTS                                NAMES
6779686f06f6        training/webapp     "python app.py"   ...   5000/tcp, 127.0.0.1:5000->5000/udp   drunk_visvesvaraya
```

**docker port** 命令可以让我们快捷地查看端口的绑定情况。

```
runoob@runoob:~$ docker port adoring_stonebraker 5000
127.0.0.1:5001
```

------

## 6.2. Docker 容器互联

端口映射并不是唯一把 docker 连接到另一个容器的方法。

docker 有一个连接系统允许将多个容器连接在一起，共享连接信息。

docker 连接会创建一个父子关系，其中父容器可以看到子容器的信息。

------

### 6.2.1. 容器命名

当我们创建一个容器的时候，docker 会自动对它进行命名。另外，我们也可以使用 **--name** 标识来命名容器。

### 6.2.2. 新建网络

下面先创建一个新的 Docker 网络。

```
$ docker network create -d bridge test-net
```

参数说明：

**-d**：参数指定 Docker 网络类型，有 bridge、overlay。

其中 overlay 网络类型用于 Swarm mode，在本小节中你可以忽略它。

### 6.2.3. 连接容器

运行一个容器并连接到新建的 test-net 网络:

```
$ docker run -itd --name test1 --network test-net ubuntu /bin/bash
```

打开新的终端，再运行一个容器并加入到 test-net 网络:

```
$ docker run -itd --name test2 --network test-net ubuntu /bin/bash
```

下面通过 ping 来证明 test1 容器和 test2 容器建立了互联关系。

如果 test1、test2 容器内中无 ping 命令，则在容器内执行以下命令安装 ping（即学即用：可以在一个容器里安装好，提交容器到镜像，在以新的镜像重新运行以上俩个容器）。

```
apt-get update
apt install iputils-ping
```

在 test1 容器输入以下命令：

![img](./images/Docker-38.png)

同理在 test2 容器也会成功连接到:

![img](./images/Docker-39.png)

这样，test1 容器和 test2 容器建立了互联关系。

如果你有多个容器之间需要互相连接，推荐使用 Docker Compose，后面会介绍。

------

## 6.3. 配置 DNS

**NDS, Domain name resolution**，**域名解析**是把域名指向网站空间IP，让人们通过注册的域名可以方便地访问到网站的一种服务。

我们可以在宿主机的 /etc/docker/daemon.json 文件中增加以下内容来设置全部容器的 DNS：

```
{
  "dns" : [
    "114.114.114.114",
    "8.8.8.8"
  ]
}
```

设置后，启动容器的 DNS 会自动配置为 114.114.114.114 和 8.8.8.8。

配置完，需要重启 docker 才能生效。

查看容器的 DNS 是否生效可以使用以下命令，它会输出容器的 DNS 信息：

```
$ docker run -it --rm  ubuntu  cat etc/resolv.conf
```

点击图片查看大图：

![img](./images/Docker-40.png)

**手动指定容器的配置**

如果只想在指定的容器设置 DNS，则可以使用以下命令：

```
$ docker run -it --rm -h host_ubuntu  --dns=114.114.114.114 --dns-search=test.com ubuntu
```

参数说明：

**--rm**：容器退出时自动清理容器内部的文件系统（也就是清理当前容器）。

**-h HOSTNAME 或者 --hostname=HOSTNAME**： 设定容器的主机名，它会被写到容器内的 /etc/hostname 和 /etc/hosts。

**--dns=IP_ADDRESS**： 添加 DNS 服务器到容器的 /etc/resolv.conf 中，让容器用这个服务器来解析所有不在 /etc/hosts 中的主机名。

**--dns-search=DOMAIN**： 设定容器的搜索域，当设定搜索域为 .example.com 时，在搜索一个名为 host 的主机时，DNS 不仅搜索 host，还会搜索 host.example.com。

点击图片查看大图：

![img](./images/Docker-41.png)

如果在容器启动时没有指定 **--dns** 和 **--dns-search**，Docker 会默认用宿主主机上的 /etc/resolv.conf 来配置容器的 DNS。

# 7. Docker 仓库管理

仓库（Repository）是集中存放镜像的地方。

## 7.1. Docker Hub

目前 Docker 官方维护了一个公共仓库 [Docker Hub](https://hub.docker.com/)。

大部分需求都可以通过在 Docker Hub 中直接下载镜像来实现。

### 7.1.1. 注册

在 [https://hub.docker.com](https://hub.docker.com/) 免费注册一个 Docker 账号。

### 7.1.2. 登录和退出

登录需要输入用户名和密码，登录成功后，我们就可以从 docker hub 上拉取自己账号下的全部镜像。

```
$ docker login
```

![img](./images/Docker-42.jpg)

**退出**

退出 docker hub 可以使用以下命令：

```
$ docker logout
```

拉取镜像

你可以通过 **docker search** 命令来查找官方仓库中的镜像，并利用 docker pull 命令来将它下载到本地。

### 7.1.3. 推送镜像

用户登录后，可以通过 docker push 命令将自己的镜像推送到 Docker Hub。

以下命令中的 username 请替换为你的 Docker 账号用户名。

```
$ docker tag ubuntu:18.04 username/ubuntu:18.04
$ docker image ls

REPOSITORY      TAG        IMAGE ID            CREATED           ...  
ubuntu          18.04      275d79972a86        6 days ago        ...  
username/ubuntu 18.04      275d79972a86        6 days ago        ...  
$ docker push username/ubuntu:18.04
$ docker search username/ubuntu

NAME             DESCRIPTION       STARS         OFFICIAL    AUTOMATED
username/ubuntu
```

# 8. Docker Dockerfile

## 8.1. 什么是 Dockerfile？

Dockerfile 是一个用来构建镜像的文本文件，文本内容包含了一条条构建镜像所需的指令和说明。

## 8.2. 使用 Dockerfile 定制镜像

这里仅讲解如何运行 Dockerfile 文件来定制一个镜像，具体 Dockerfile 文件内指令详解，将在下一节中介绍，这里你只要知道构建的流程即可。

**1、下面以定制一个 nginx 镜像（构建好的镜像内会有一个 /usr/share/nginx/html/index.html 文件）**

在一个空目录下，新建一个名为 Dockerfile 文件，并在文件内添加以下内容：

```
FROM nginx
RUN echo '这是一个本地构建的nginx镜像' > /usr/share/nginx/html/index.html
```

![img](./images/Docker-44.png)

**2、FROM 和 RUN 指令的作用**

**FROM**：定制的镜像都是基于 FROM 的镜像，这里的 nginx 就是定制需要的基础镜像。后续的操作都是基于 nginx。

**RUN**：用于执行后面跟着的命令行命令。有以下俩种格式：

shell 格式：

```
RUN <命令行命令>
/# <命令行命令> 等同于，在终端操作的 shell 命令。
```

exec 格式：

```
RUN ["可执行文件", "参数1", "参数2"]
/# 例如：
/# RUN ["./test.php", "dev", "offline"] 等价于 RUN ./test.php dev offline
```

**注意**：Dockerfile 的指令每执行一次都会在 docker 上新建一层。所以过多无意义的层，会造成镜像膨胀过大。例如：

FROM centos
RUN **yum** -y **install** **wget**
RUN **wget** -O redis.tar.gz "http://download.redis.io/releases/redis-5.0.3.tar.gz"
RUN **tar** -xvf redis.tar.gz



以上执行会创建 3 层镜像。可简化为以下格式：

FROM centos
RUN **yum** -y **install** **wget** \
  **&&** **wget** -O redis.tar.gz "http://download.redis.io/releases/redis-5.0.3.tar.gz" \
  **&&** **tar** -xvf redis.tar.gz

如上，以 **&&** 符号连接命令，**这样执行后，只会创建 1 层镜像**。

## 8.3. 开始构建镜像

在 Dockerfile 文件的存放目录下，执行构建动作。

以下示例，通过目录下的 Dockerfile 构建一个 nginx:v3（镜像名称:镜像标签）。

**注**：最后的 **.** 代表本次执行的上下文路径，下一节会介绍。

$ docker build -t nginx:v3 .

![img](./images/Docker-45.png)

以上显示，说明已经构建成功。

## 8.4. 上下文路径

上一节中，有提到指令最后一个 **.** 是上下文路径，那么什么是上下文路径呢？

$ docker build -t nginx:v3 .

上下文路径，是指 docker 在构建镜像，有时候想要使用到本机的文件（比如复制），docker build 命令得知这个路径后，会将路径下的所有内容打包。

**解析**：由于 docker 的运行模式是 C/S。我们本机是 C，docker 引擎是 S。实际的构建过程是在 docker 引擎下完成的，所以这个时候无法用到我们本机的文件。这就需要把我们本机的指定目录下的文件一起打包提供给 docker 引擎使用。

如果未说明最后一个参数，那么默认上下文路径就是 Dockerfile 所在的位置。

**注意**：上下文路径下不要放无用的文件，因为会一起打包发送给 docker 引擎，如果文件过多会造成过程缓慢。

------

## 8.5. 指令详解

### 8.5.1. COPY

复制指令，从上下文目录中复制文件或者目录到容器里指定路径。

格式：

```
COPY [--chown=<user>:<group>] <源路径1>...  <目标路径>
COPY [--chown=<user>:<group>] ["<源路径1>",...  "<目标路径>"]
```

**[--chown=<user>:<group>]**：可选参数，用户改变复制到容器内文件的拥有者和属组。

**<源路径>**：源文件或者源目录，这里可以是通配符表达式，其通配符规则要满足 Go 的 filepath.Match 规则。例如：

```
COPY hom* /mydir/
COPY hom?.txt /mydir/
```

**<目标路径>**：**容器内的指定路径，该路径不用事先建好，路径不存在的话，会自动创建**。

### 8.5.2. ADD

ADD 指令和 COPY 的使用格类似（同样需求下，官方推荐使用 COPY）。功能也类似，不同之处如下：

- ADD 的优点：在执行 <源文件> 为 tar 压缩文件的话，压缩格式为 gzip, bzip2 以及 xz 的情况下，会自动复制并解压到 <目标路径>。
- ADD 的缺点：**在不解压的需求前提下**，无法复制 tar 压缩文件（因为会帮你自动解压文件）。会令镜像构建缓存失效，从而可能会令镜像构建变得比较缓慢。具体是否使用，可以根据是否需要自动解压来决定。

### 8.5.3. CMD

类似于 RUN 指令，用于运行程序，但二者运行的时间点不同:

- CMD 在docker run 时运行。
- RUN 是在 docker build。

**作用**：为启动的容器指定默认要运行的程序，程序运行结束，容器也就结束。CMD 指令指定的程序可被 docker run 命令行参数中指定要运行的程序所覆盖，理解如下。

```
如你指定:
CMD ["/bin/echo", "this is a echo test"]
build后运行(假设镜像名为ec):
docker run ec
就会输出: this is a echo test
是不是感觉很像开机启动项，你可以暂时这样理解。

注意点：
docker run命令如果指定了参数会把CMD里的参数覆盖：
这里说明一下，如：docker run -it ubuntu /bin/bash 命令的参数是指/bin/bash 而非 -it ,-it只是docker的参数，而不是容器的参数。
同样是上面的ec镜像启动：
docker run ec /bin/echo hello
就不会输出：this is a echo test，因为CMD命令被”/bin/bash”覆盖了。
```



格式：

```
CMD <shell 命令> 
CMD ["<可执行文件或命令>","<param1>","<param2>",...] 
CMD ["<param1>","<param2>",...]  # 该写法是为 ENTRYPOINT 指令指定的程序提供默认参数
```

推荐使用第二种格式，执行过程比较明确。第一种格式实际上在运行的过程中也会自动转换成第二种格式运行，并且默认可执行文件是 sh。

### 8.5.4. ENTRYPOINT

类似于 CMD 指令，**但其不会被 docker run 的命令行参数指定的指令所覆盖**，而且这些命令行参数会被当作参数送给 ENTRYPOINT 指令指定的程序。

但是, 如果运行 docker run 时使用了 --entrypoint 选项，将覆盖 CMD 指令指定的程序。

**优点**：在执行 docker run 的时候可以指定 ENTRYPOINT 运行所需的参数。

**注意**：如果 Dockerfile 中如果存在多个 ENTRYPOINT 指令，仅最后一个生效。

格式：

```
ENTRYPOINT ["<executeable>","<param1>","<param2>",...]
```

可以搭配 CMD 命令使用：一般是变参才会使用 CMD ，这里的 CMD 等于是在给 ENTRYPOINT 传参，以下示例会提到。

示例：

假设已通过 Dockerfile 构建了 nginx:test 镜像：

```
FROM nginx

ENTRYPOINT ["nginx", "-c"] # 定参
CMD ["/etc/nginx/nginx.conf"] # 变参 
```

1、不传参运行

```
$ docker run  nginx:test
```

容器内会默认运行以下命令，启动主进程。

```
nginx -c /etc/nginx/nginx.conf
```

2、传参运行

```
$ docker run  nginx:test -c /etc/nginx/new.conf
```

容器内会默认运行以下命令，启动主进程(/etc/nginx/new.conf:假设容器内已有此文件)

```
nginx -c /etc/nginx/new.conf
```

### 8.5.5. ENV

设置环境变量，定义了环境变量，那么在后续的指令中，就可以使用这个环境变量。

格式：

```
ENV <key> <value>
ENV <key1>=<value1> <key2>=<value2>...
```

以下示例设置 NODE_VERSION = 7.2.0 ， 在后续的指令中可以通过 $NODE_VERSION 引用：

```
ENV NODE_VERSION 7.2.0

RUN curl -SLO "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.xz" \
  && curl -SLO "https://nodejs.org/dist/v$NODE_VERSION/SHASUMS256.txt.asc"
```

### 8.5.6. ARG

构建参数，与 ENV 作用一致。不过作用域不一样。ARG 设置的环境变量仅对 Dockerfile 内有效，也就是说只有 docker build 的过程中有效，构建好的镜像内不存在此环境变量。

构建命令 docker build 中可以用 --build-arg <参数名>=<值> 来覆盖。

格式：

```
ARG <参数名>[=<默认值>]
```

### 8.5.7. VOLUME

定义匿名数据卷。在启动容器时忘记挂载数据卷，会自动挂载到匿名卷。

作用：

- 避免重要的数据，因容器重启而丢失，这是非常致命的。
- 避免容器不断变大。

格式：

```
VOLUME ["<路径1>", "<路径2>"...]
VOLUME <路径>
```

**在启动容器 docker run 的时候，我们可以通过 -v 参数修改挂载点**。示例如下：

	docker run -p 3306:3306 --name mysql \
	-v /mydata/mysql/conf:/etc/mysql \
	-v /mydata/mysql/data:/var/lib/mysql \
	-v /mydata/mysql/log:/var/log/mysql \
	-e MYSQL_ROOT_PASSWORD=root \
	-d mysql:5.7

### 8.5.8. EXPOSE

仅仅只是声明端口。

作用：

- 帮助镜像使用者理解这个镜像服务的守护端口，以方便配置映射。
- 在运行时使用随机端口映射时，也就是 docker run -P 时，会自动随机映射 EXPOSE 的端口。

格式：

```
EXPOSE <端口1> [<端口2>...]
```

### 8.5.9. WORKDIR

指定工作目录。用 WORKDIR 指定的工作目录，会在构建镜像的每一层中都存在。（WORKDIR 指定的工作目录，必须是提前创建好的）。

docker build 构建镜像过程中的，每一个 RUN 命令都是新建的一层。只有通过 WORKDIR 创建的目录才会一直存在。

格式：

```
WORKDIR <工作目录路径>
```

### 8.5.10. USER

用于指定执行后续命令的用户和用户组，这边只是切换后续命令执行的用户（用户和用户组必须提前已经存在）（比如root用户，为了切换到用户指定的用户）。

格式：

```
USER <用户名>[:<用户组>]
```

```Dockerfile
RUN groupadd -r redis && useradd -r -g redis redis
USER redis
RUN [ "redis-server" ]
```

**如果以 `root` 执行的脚本，在执行期间希望改变身份，比如希望以某个已经建立好的用户来运行某个服务进程，不要使用 `su` 或者 `sudo`，这些都需要比较麻烦的配置，而且在 TTY 缺失的环境下经常出错。**建议使用 [`gosu`](https://github.com/tianon/gosu)。

```Dockerfile
/# 建立 redis 用户，并使用 gosu 换另一个用户执行命令
RUN groupadd -r redis && useradd -r -g redis redis
/# 下载 gosu
RUN wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/1.7/gosu-amd64" \
    && chmod +x /usr/local/bin/gosu \
    && gosu nobody true
/# 设置 CMD，并以另外的用户执行
CMD [ "exec", "gosu", "redis", "redis-server" ]
```

### 8.5.11. HEALTHCHECK

用于指定某个程序或者指令来监控 docker 容器服务的运行状态。

格式：

```
HEALTHCHECK [选项] CMD <命令>：设置检查容器健康状况的命令
HEALTHCHECK NONE：如果基础镜像有健康检查指令，使用这行可以屏蔽掉其健康检查指令

HEALTHCHECK [选项] CMD <命令> : 这边 CMD 后面跟随的命令使用，可以参考 CMD 的用法。
```



```Dockerfile
FROM nginx
RUN apt-get update && apt-get install -y curl && rm -rf /var/lib/apt/lists/*
HEALTHCHECK --interval=5s --timeout=3s \
  CMD curl -fs http://localhost/ || exit 1
```

这里我们设置了**每 5 秒检查一次**（这里为了试验所以间隔非常短，实际应该相对较长），**如果健康检查命令超过 3 秒没响应就视为失败**，**并且使用 `curl -fs http://localhost/ || exit 1` 作为健康检查命令**。

### 8.5.12. ONBUILD

用于延迟构建命令的执行。简单的说，就是 Dockerfile 里用 ONBUILD 指定的命令，在本次构建镜像的过程中不会执行（假设镜像为 test-build）。当有新的 Dockerfile 使用了之前构建的镜像 FROM test-build ，这时执行新镜像的 Dockerfile 构建时候，会执行 test-build 的 Dockerfile 里的 ONBUILD 指定的命令。

格式：

```
ONBUILD <其它指令>
```

### 8.5.13. LABEL

LABEL 指令用来给镜像添加一些元数据（metadata），以键值对的形式，语法格式如下：

```
LABEL <key>=<value> <key>=<value> <key>=<value> ...
```

比如我们可以添加镜像的作者：

```
LABEL org.opencontainers.image.authors="runoob"
```

# 9. Docker Compose

## 9.1. Compose 简介

Compose 是用于定义和运行多容器 Docker 应用程序的工具。

通过 Compose，您可以使用 YML 文件来配置应用程序需要的所有服务。然后，使用一个命令，就可以从 YML 文件配置中创建并启动所有服务。

Compose 使用的三个步骤：

- 使用 Dockerfile 定义应用程序的环境。
- 使用 docker-compose.yml 定义构成应用程序的服务，这样它们可以在隔离环境中一起运行。
- 最后，执行 docker-compose up 命令来启动并运行整个应用程序。

docker-compose.yml 的配置案例如下（配置参数参考下文）：

## 9.2. yaml 配置实例

```dockerfile
version: '3'
services:
  web:
    build: .
    ports:
      - "5000:5000"
    volumes:
      - .:/code
      - logvolume01:/var/log
    links:
      - redis
  redis:
    image: redis
volumes:
  logvolume01: {}
```



------

## 9.3. Compose 安装

### 9.3.1. Linux

Linux 上我们可以从 Github 上下载它的二进制包来使用，最新发行的版本地址：https://github.com/docker/compose/releases。

运行以下命令以下载 Docker Compose 的当前稳定版本：

```
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

要安装其他版本的 Compose，请替换 1.24.1。

将可执行权限应用于二进制文件：

```
$ sudo chmod +x /usr/local/bin/docker-compose
```

创建软链：

```
$ sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

测试是否安装成功：

```
$ docker-compose --version
cker-compose version 1.24.1, build 4667896b
```

**注意**： 对于 alpine，需要以下依赖包： py-pip，python-dev，libffi-dev，openssl-dev，gcc，libc-dev，和 make。

### 9.3.2. macOS

Mac 的 Docker 桌面版和 Docker Toolbox 已经包括 Compose 和其他 Docker 应用程序，因此 Mac 用户不需要单独安装 Compose。

### 9.3.3. windows PC

Windows 的 Docker 桌面版和 Docker Toolbox 已经包括 Compose 和其他 Docker 应用程序，因此 Windows 用户不需要单独安装 Compose。

------

## 9.4. 使用

### 9.4.1. 准备

创建一个测试目录：

```
$ mkdir composetest
$ cd composetest
```

在测试目录中创建一个名为 app.py 的文件，并复制粘贴以下内容：

**composetest/app.py 文件代码**

```python
import time

import redis
from flask import Flask

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)


def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)


@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello World! I have been seen {} times.\n'.format(count)
```

在此示例中，redis 是应用程序网络上的 redis 容器的主机名，该主机使用的端口为 6379。

在 composetest 目录中创建另一个名为 **requirements.txt** 的文件，内容如下：

```
flask
redis
```

### 9.4.2. 创建 Dockerfile 文件

在 composetest 目录中，创建一个名为 **Dockerfile** 的文件，内容如下：

```
FROM python:3.7-alpine
WORKDIR /code
ENV FLASK_APP app.py
ENV FLASK_RUN_HOST 0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
CMD ["flask", "run"]
```

**Dockerfile 内容解释：**

- **FROM python:3.7-alpine**: 从 Python 3.7 映像开始构建镜像。

- **WORKDIR /code**: 将工作目录设置为 /code。

- ```
  ENV FLASK_APP app.py
  ENV FLASK_RUN_HOST 0.0.0.0
  ```

  设置 flask 命令使用的环境变量。

- **RUN apk add --no-cache gcc musl-dev linux-headers**: 安装 gcc，以便诸如 MarkupSafe 和 SQLAlchemy 之类的 Python 包可以编译加速。

- ```
  COPY requirements.txt requirements.txt
  RUN pip install -r requirements.txt
  ```

  复制 requirements.txt 并安装 Python 依赖项。

- **COPY . .**: 将 . 项目中的当前目录复制到 . 镜像中的工作目录。

- **CMD ["flask", "run"]**: 容器提供默认的执行命令为：flask run。

### 9.4.3. 创建 docker-compose.yml

在测试目录中创建一个名为 docker-compose.yml 的文件，然后粘贴以下内容：

**docker-compose.yml 配置文件**

```dockerfile
version: '3'
services:
  web:
    build: .
    ports:
     - "5000:5000"
  redis:
    image: "redis:alpine"
```

该 Compose 文件定义了两个服务：web 和 redis。

- **web**：**该 web 服务使用从 Dockerfile 当前目录中构建的镜像**。然后，它将容器和主机绑定到暴露的端口 5000。此示例服务使用 Flask Web 服务器的默认端口 5000 。
- **redis**：该 redis 服务使用 Docker Hub 的公共 Redis 映像。

### 9.4.4. 使用 Compose 命令构建和运行您的应用

在测试目录中，执行以下命令来启动应用程序：

```
docker-compose up
```

如果你想在后台执行该服务可以加上 **-d** 参数：

```
docker-compose up -d
```

------

## 9.5. yml 配置指令参考

### 9.5.1. version

指定本 yml 依从的 compose 哪个版本制定的。

### 9.5.2. build

指定为构建镜像上下文路径：

例如 webapp 服务，指定为从上下文路径 ./dir/Dockerfile 所构建的镜像：

```
version: "3.7"
services:
  webapp:
    build: ./dir
```

或者，作为具有在上下文指定的路径的对象，以及可选的 Dockerfile 和 args：

```
version: "3.7"
services:
  webapp:
    build:
      context: ./dir
      dockerfile: Dockerfile-alternate
      args:
        buildno: 1
      labels:
        - "com.example.description=Accounting webapp"
        - "com.example.department=Finance"
        - "com.example.label-with-empty-value"
      target: prod
```

- context：上下文路径。
- dockerfile：指定构建镜像的 Dockerfile 文件名。
- args：添加构建参数，这是只能在构建过程中访问的环境变量。
- labels：设置构建镜像的标签。
- target：多层构建，可以指定构建哪一层。

### 9.5.3. cap_add，cap_drop

添加或删除容器拥有的宿主机的内核功能。

```
cap_add:
  - ALL # 开启全部权限

cap_drop:
  - SYS_PTRACE # 关闭 ptrace权限
```

### 9.5.4. cgroup_parent

为容器指定父 cgroup 组，意味着将继承该组的资源限制。

```
cgroup_parent: m-executor-abcd
```

### 9.5.5. command

覆盖容器启动的默认命令。

```
command: ["bundle", "exec", "thin", "-p", "3000"]
```

### 9.5.6. container_name

指定自定义容器名称，而不是生成的默认名称。

```
container_name: my-web-container
```

### 9.5.7. depends_on

设置依赖关系。下面的命令以及对应的相关解释。

- docker-compose up ：以依赖性顺序启动服务。在以下示例中，先启动 db 和 redis ，才会启动 web。
- docker-compose up SERVICE ：自动包含 SERVICE 的依赖项。在以下示例中，docker-compose up web 还将创建并启动 db 和 redis。
- docker-compose stop ：按依赖关系顺序停止服务。在以下示例中，web 在 db 和 redis 之前停止。

```
version: "3.7"
services:
  web:
    build: .
    depends_on:
      - db
      - redis
  redis:
    image: redis
  db:
    image: postgres
```

**注意：web 服务不会等待 redis db 完全启动 之后才启动**。

### 9.5.8. deploy

指定与服务的部署和运行有关的配置。只在 swarm 模式下才会有用。

```
version: "3.7"
services:
  redis:
    image: redis:alpine
    deploy:
      mode：replicated
      replicas: 6
      endpoint_mode: dnsrr
      labels: 
        description: "This redis service label"
      resources:
        limits:
          cpus: '0.50'
          memory: 50M
        reservations:
          cpus: '0.25'
          memory: 20M
      restart_policy:
        condition: on-failure
        delay: 5s
        max_attempts: 3
        window: 120s
```

可以选参数：

**endpoint_mode**：访问集群服务的方式。

```
endpoint_mode: vip 
/# Docker 集群服务一个对外的虚拟 ip。所有的请求都会通过这个虚拟 ip 到达集群服务内部的机器。
endpoint_mode: dnsrr
/# DNS 轮询（DNSRR）。所有的请求会自动轮询获取到集群 ip 列表中的一个 ip 地址。
```

**labels**：在服务上设置标签。可以用容器上的 labels（跟 deploy 同级的配置） 覆盖 deploy 下的 labels。

**mode**：指定服务提供的模式。

- **replicated**：复制服务，复制指定服务到集群的机器上。

- **global**：全局服务，服务将部署至集群的每个节点。

- 图解：下图中黄色的方块是 replicated 模式的运行情况，灰色方块是 global 模式的运行情况。

![img](./images/Docker-46.png)

**replicas：mode** 为 replicated 时，需要使用此参数配置具体运行的节点数量。

**resources**：配置服务器资源使用的限制，例如上例子，配置 redis 集群运行需要的 cpu 的百分比 和 内存的占用。避免占用资源过高出现异常。

**restart_policy**：配置如何在退出容器时重新启动容器。

- condition：可选 none，on-failure 或者 any（默认值：any）。
- delay：设置多久之后重启（默认值：0）。
- max_attempts：尝试重新启动容器的次数，超出次数，则不再尝试（默认值：一直重试）。
- window：设置容器重启超时时间（默认值：0）。

**rollback_config**：配置在更新失败的情况下应如何回滚服务。

- parallelism：一次要回滚的容器数。如果设置为0，则所有容器将同时回滚。
- delay：每个容器组回滚之间等待的时间（默认为0s）。
- failure_action：如果回滚失败，该怎么办。其中一个 continue 或者 pause（默认pause）。
- monitor：每个容器更新后，持续观察是否失败了的时间 (ns|us|ms|s|m|h)（默认为0s）。
- max_failure_ratio：在回滚期间可以容忍的故障率（默认为0）。
- order：回滚期间的操作顺序。其中一个 stop-first（串行回滚），或者 start-first（并行回滚）（默认 stop-first ）。

**update_config**：配置应如何更新服务，对于配置滚动更新很有用。

- parallelism：一次更新的容器数。
- delay：在更新一组容器之间等待的时间。
- failure_action：如果更新失败，该怎么办。其中一个 continue，rollback 或者pause （默认：pause）。
- monitor：每个容器更新后，持续观察是否失败了的时间 (ns|us|ms|s|m|h)（默认为0s）。
- max_failure_ratio：在更新过程中可以容忍的故障率。
- order：回滚期间的操作顺序。其中一个 stop-first（串行回滚），或者 start-first（并行回滚）（默认stop-first）。

**注**：仅支持 V3.4 及更高版本。

### 9.5.9. devices

指定设备映射列表。

```
devices:
  - "/dev/ttyUSB0:/dev/ttyUSB0"
```

### 9.5.10. dns

自定义 DNS 服务器，可以是单个值或列表的多个值。

```
dns: 8.8.8.8

dns:
  - 8.8.8.8
  - 9.9.9.9
```

### 9.5.11. dns_search

自定义 DNS 搜索域。可以是单个值或列表。

```
dns_search: example.com

dns_search:
  - dc1.example.com
  - dc2.example.com
```

### 9.5.12. entrypoint

覆盖容器默认的 entrypoint。

```
entrypoint: /code/entrypoint.sh
```

也可以是以下格式：

```
entrypoint:
    - php
    - -d
    - zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20100525/xdebug.so
    - -d
    - memory_limit=-1
    - vendor/bin/phpunit
```

### 9.5.13. env_file

从文件添加环境变量。可以是单个值或列表的多个值。

```
env_file: .env
```

也可以是列表格式：

```
env_file:
  - ./common.env
  - ./apps/web.env
  - /opt/secrets.env
```

**注意: env_file和environment中定义的环境变量是传给container用的而不是在docker-compose.yml中的环境变量用的docker-compose.yml中的环境变量${VARIABLE:-default}引用的是在.env中定义的或者同个shell export出来的**

**建议env_file 引入的文件名为.env，最好不要使用其他名称**

- 优点: .env文件变更会实时更新docker-compose中的引用，使用其他名称不会实时更新docker-compose中的引用，**使用其他名称在docker-compose中引用有时会WARNING: The DB_DIR variable is not set. Defaulting to a blank string**。
- 缺点: 自定义名称不方便



### 9.5.14. environment

添加环境变量。您可以使用数组或字典、任何布尔值，布尔值需要用引号引起来，以确保 YML 解析器不会将其转换为 True 或 False。

```
environment:
  RACK_ENV: development
  SHOW: 'true'
```

### 9.5.15. expose

暴露端口，但不映射到宿主机，只被连接的服务访问。

仅可以指定内部端口为参数：

```
expose:
 - "3000"
 - "8000"
```

### 9.5.16. extra_hosts

添加主机名映射。类似 docker client --add-host。

```
extra_hosts:
 - "somehost:162.242.195.82"
 - "otherhost:50.31.209.229"
```

以上会在此服务的内部容器中 /etc/hosts 创建一个具有 ip 地址和主机名的映射关系：

```
162.242.195.82  somehost
50.31.209.229   otherhost
```

### 9.5.17. healthcheck

用于检测 docker 服务是否健康运行。

```
healthcheck:
  test: ["CMD", "curl", "-f", "http://localhost"] # 设置检测程序
  interval: 1m30s # 设置检测间隔
  timeout: 10s # 设置检测超时时间
  retries: 3 # 设置重试次数
  start_period: 40s # 启动后，多少秒开始启动检测程序
```

### 9.5.18. image

指定容器运行的镜像。以下格式都可以：

```
image: redis
image: ubuntu:14.04
image: tutum/influxdb
image: example-registry.com:4000/postgresql
image: a4bc65fd # 镜像id
```

### 9.5.19. logging

服务的日志记录配置。

driver：指定服务容器的日志记录驱动程序，默认值为json-file。有以下三个选项

```
driver: "json-file"
driver: "syslog"
driver: "none"
```

仅在 json-file 驱动程序下，可以使用以下参数，限制日志得数量和大小。

```
logging:
  driver: json-file
  options:
    max-size: "200k" # 单个文件大小为200k
    max-file: "10" # 最多10个文件
```

当达到文件限制上限，会自动删除旧得文件。

syslog 驱动程序下，可以使用 syslog-address 指定日志接收地址。

```
logging:
  driver: syslog
  options:
    syslog-address: "tcp://192.168.0.42:123"
```

### 9.5.20. network_mode

设置网络模式。

```
network_mode: "bridge"
network_mode: "host"
network_mode: "none"
network_mode: "service:[service name]"
network_mode: "container:[container name/id]"
```

**networks**

配置容器连接的网络，引用顶级 networks 下的条目 。

```
services:
  some-service:
    networks:
      some-network:
        aliases:
         - alias1
      other-network:
        aliases:
         - alias2
networks:
  some-network:
    /# Use a custom driver
    driver: custom-driver-1
  other-network:
    /# Use a custom driver which takes special options
    driver: custom-driver-2
```

**aliases** ：同一网络上的其他容器可以使用服务名称或此别名来连接到对应容器的服务。

### 9.5.21. restart

- no：是默认的重启策略，在任何情况下都不会重启容器。
- always：容器总是重新启动。
- on-failure：在容器非正常退出时（退出状态非0），才会重启容器。
- unless-stopped：在容器退出时总是重启容器，但是不考虑在Docker守护进程启动时就已经停止了的容器

```
restart: "no"
restart: always
restart: on-failure
restart: unless-stopped
```

注：swarm 集群模式，请改用 restart_policy。

### 9.5.22. secrets

存储敏感数据，例如密码：

```
version: "3.1"
services:

mysql:
  image: mysql
  environment:
    MYSQL_ROOT_PASSWORD_FILE: /run/secrets/my_secret
  secrets:
    - my_secret

secrets:
  my_secret:
    file: ./my_secret.txt
```

### 9.5.23. security_opt

修改容器默认的 schema 标签。

```
security-opt：
  - label:user:USER   # 设置容器的用户标签
  - label:role:ROLE   # 设置容器的角色标签
  - label:type:TYPE   # 设置容器的安全策略标签
  - label:level:LEVEL  # 设置容器的安全等级标签
```

### 9.5.24. stop_grace_period

指定在容器无法处理 SIGTERM (或者任何 stop_signal 的信号)，等待多久后发送 SIGKILL 信号关闭容器。

```
stop_grace_period: 1s # 等待 1 秒
stop_grace_period: 1m30s # 等待 1 分 30 秒
```

默认的等待时间是 10 秒。

### 9.5.25. stop_signal

设置停止容器的替代信号。默认情况下使用 SIGTERM 。

以下示例，使用 SIGUSR1 替代信号 SIGTERM 来停止容器。

```
stop_signal: SIGUSR1
```

### 9.5.26. sysctls

设置容器中的内核参数，可以使用数组或字典格式。

```
sysctls:
  net.core.somaxconn: 1024
  net.ipv4.tcp_syncookies: 0

sysctls:
  - net.core.somaxconn=1024
  - net.ipv4.tcp_syncookies=0
```

### 9.5.27. tmpfs

在容器内安装一个临时文件系统。可以是单个值或列表的多个值。

```
tmpfs: /run

tmpfs:
  - /run
  - /tmp
```

### 9.5.28. ulimits

覆盖容器默认的 ulimit。

```
ulimits:
  nproc: 65535
  nofile:
    soft: 20000
    hard: 40000
```

### 9.5.29. volumes

将主机的数据卷或着文件挂载到容器里。

```
version: "3.7"
services:
  db:
    image: postgres:latest
    volumes:
      - "/localhost/postgres.sock:/var/run/postgres/postgres.sock"
      - "/localhost/data:/var/lib/postgresql/data"
```

# 10. Docker Machine

## 10.1. 简介

Docker Machine 是一种可以让您在虚拟主机上安装 Docker 的工具，并可以使用 docker-machine 命令来管理主机。

Docker Machine 也可以集中管理所有的 docker 主机，比如快速的给 100 台服务器安装上 docker。

Docker Machine 管理的虚拟主机可以是机上的，也可以是云供应商，如阿里云，腾讯云，AWS，或 DigitalOcean。

使用 docker-machine 命令，您可以启动，检查，停止和重新启动托管主机，也可以升级 Docker 客户端和守护程序，以及配置 Docker 客户端与您的主机进行通信。

------

## 10.2. 安装

安装 Docker Machine 之前你需要先安装 Docker。

Docker Machine 可以在多种平台上安装使用，包括 Linux 、MacOS 以及 windows。

### 10.2.1. Linux 安装命令

```
$ base=https://github.com/docker/machine/releases/download/v0.16.0 &&
  curl -L $base/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine &&
  sudo mv /tmp/docker-machine /usr/local/bin/docker-machine &&
  chmod +x /usr/local/bin/docker-machine
```

### 10.2.2. macOS 安装命令

```
$ base=https://github.com/docker/machine/releases/download/v0.16.0 &&
  curl -L $base/docker-machine-$(uname -s)-$(uname -m) >/usr/local/bin/docker-machine &&
  chmod +x /usr/local/bin/docker-machine
```

### 10.2.3. Windows 安装命令

如果你是 Windows 平台，可以使用 [Git BASH](https://git-for-windows.github.io/)，并输入以下命令：

```
$ base=https://github.com/docker/machine/releases/download/v0.16.0 &&
  mkdir -p "$HOME/bin" &&
  curl -L $base/docker-machine-Windows-x86_64.exe > "$HOME/bin/docker-machine.exe" &&
  chmod +x "$HOME/bin/docker-machine.exe"
```

查看是否安装成功：

```
$ docker-machine version
docker-machine version 0.16.0, build 9371605
```

------

## 10.3. 使用

本章通过 virtualbox 来介绍 docker-machine 的使用方法。其他云服务商操作与此基本一致。具体可以参考每家服务商的指导文档。

### 10.3.1. 列出可用的机器

可以看到目前只有这里默认的 default 虚拟机。

```
$ docker-machine ls
```

![img](./images/Docker-47.png)

### 10.3.2. 创建机器

创建一台名为 test 的机器。

```
$ docker-machine create --driver virtualbox test
```

- **--driver**：指定用来创建机器的驱动类型，这里是 virtualbox。

![img](./images/Docker-48.png)

### 10.3.3. 查看机器的 ip

```
$ docker-machine ip test
```

![img](./images/Docker-49.png)

### 10.3.4. 停止机器

```
$ docker-machine stop test
```

![img](./images/Docker-50.png)

### 10.3.5. 启动机器

```
$ docker-machine start test
```

![img](./images/Docker-51.png)

### 10.3.6. 进入机器

```
$ docker-machine ssh test
```

![img](./images/Docker-52.png)

### 10.3.7. docker-machine 命令参数说明

- **docker-machine active**：查看当前激活状态的 Docker 主机。

  ```
  $ docker-machine ls
  
  NAME      ACTIVE   DRIVER         STATE     URL
  dev       -        virtualbox     Running   tcp://192.168.99.103:2376
  staging   *        digitalocean   Running   tcp://203.0.113.81:2376
  
  $ echo $DOCKER_HOST
  tcp://203.0.113.81:2376
  
  $ docker-machine active
  staging
  ```

- **config**：查看当前激活状态 Docker 主机的连接信息。

- **create**：创建 Docker 主机

- **env**：显示连接到某个主机需要的环境变量

- **inspect**： 以 json 格式输出指定Docker的详细信息

- **ip**： 获取指定 Docker 主机的地址

- **kill**： 直接杀死指定的 Docker 主机

- **ls**： 列出所有的管理主机

- **provision**： 重新配置指定主机

- **regenerate-certs**： 为某个主机重新生成 TLS 信息

- **restart**： 重启指定的主机

- **rm**： 删除某台 Docker 主机，对应的虚拟机也会被删除

- **ssh**： 通过 SSH 连接到主机上，执行命令

- **scp**： 在 Docker 主机之间以及 Docker 主机和本地主机之间通过 scp 远程复制数据

- **mount**： 使用 SSHFS 从计算机装载或卸载目录

- **start**： 启动一个指定的 Docker 主机，如果对象是个虚拟机，该虚拟机将被启动

- **status**： 获取指定 Docker 主机的状态(包括：Running、Paused、Saved、Stopped、Stopping、Starting、Error)等

- **stop**： 停止一个指定的 Docker 主机

- **upgrade**： 将一个指定主机的 Docker 版本更新为最新

- **url**： 获取指定 Docker 主机的监听 URL

- **version**： 显示 Docker Machine 的版本或者主机 Docker 版本

- **help**： 显示帮助信息

# 11. Swarm 集群管理

## 11.1. 简介

Docker Swarm 是 Docker 的集群管理工具。它将 Docker 主机池转变为单个虚拟 Docker 主机。 Docker Swarm 提供了标准的 Docker API，所有任何已经与 Docker 守护程序通信的工具都可以使用 Swarm 轻松地扩展到多个主机。

支持的工具包括但不限于以下各项：

- Dokku
- Docker Compose
- Docker Machine
- Jenkins

## 11.2. 原理

如下图所示，swarm 集群由管理节点（manager）和工作节点（work node）构成。

- **swarm mananger**：负责整个集群的管理工作包括集群配置、服务管理等所有跟集群有关的工作。
- **work node**：即图中的 available node，主要负责运行相应的服务来执行任务 。

![img](./images/Docker-53.png)

------

## 11.3. 使用

以下示例，均以 Docker Machine 和 virtualbox 进行介绍，确保你的主机已安装 virtualbox。

### 11.3.1. 1、创建 swarm 集群管理节点（manager）

创建 docker 机器：

```
$ docker-machine create -d virtualbox swarm-manager
```

![img](./images/Docker-54.png)

初始化 swarm 集群，进行初始化的这台机器，就是集群的管理节点。

```
$ docker-machine ssh swarm-manager
$ docker swarm init --advertise-addr 192.168.99.107 #这里的 IP 为创建机器时分配的 ip。
```

![img](./images/Docker-55.png)

以上输出，证明已经初始化成功。需要把以下这行复制出来，在增加工作节点时会用到：

```
docker swarm join --token SWMTKN-1-4oogo9qziq768dma0uh3j0z0m5twlm10iynvz7ixza96k6jh9p-ajkb6w7qd06y1e33yrgko64sk 192.168.99.107:2377
```

### 11.3.2. 2、创建 swarm 集群工作节点（worker）

这里直接创建好俩台机器，swarm-worker1 和 swarm-worker2 。

![img](./images/Docker-56.png)

分别进入两个机器里，指定添加至上一步中创建的集群，这里会用到上一步复制的内容。

![img](./images/Docker-57.png)

以上数据输出说明已经添加成功。

上图中，由于上一步复制的内容比较长，会被自动截断，实际上在图运行的命令如下：

```
docker@swarm-worker1:~$ docker swarm join --token SWMTKN-1-4oogo9qziq768dma0uh3j0z0m5twlm10iynvz7ixza96k6jh9p-ajkb6w7qd06y1e33yrgko64sk 192.168.99.107:2377
```

### 11.3.3. 3、查看集群信息

进入管理节点，执行：docker info 可以查看当前集群的信息。

```
$ docker info
```

![img](./images/Docker-58.png)

通过画红圈的地方，可以知道当前运行的集群中，有三个节点，其中有一个是管理节点。

### 11.3.4. 4、部署服务到集群中

**注意**：跟集群管理有关的任何操作，都是在管理节点上操作的。

以下例子，在一个工作节点上创建一个名为 helloworld 的服务，这里是随机指派给一个工作节点：

```
docker@swarm-manager:~$ docker service create --replicas 1 --name helloworld alpine ping docker.com
```

![img](./images/Docker-59.png)

### 11.3.5. 5、查看服务部署情况

查看 helloworld 服务运行在哪个节点上，可以看到目前是在 swarm-worker1 节点：

```
docker@swarm-manager:~$ docker service ps helloworld
```

![img](./images/Docker-60.png)

查看 helloworld 部署的具体信息：

```
docker@swarm-manager:~$ docker service inspect --pretty helloworld
```

![img](./images/Docker-61.png)

### 11.3.6. 6、扩展集群服务

我们将上述的 helloworld 服务扩展到俩个节点。

```
docker@swarm-manager:~$ docker service scale helloworld=2
```

![img](./images/Docker-62.png)

可以看到已经从一个节点，扩展到两个节点。

![img](./images/Docker-63.png)

### 11.3.7. 7、删除服务

```
docker@swarm-manager:~$ docker service rm helloworld
```

![img](./images/Docker-64.png)

查看是否已删除：

![img](./images/Docker-65.png)

### 11.3.8. 8、滚动升级服务

以下实例，我们将介绍 redis 版本如何滚动升级至更高版本。

创建一个 3.0.6 版本的 redis。

```
docker@swarm-manager:~$ docker service create --replicas 1 --name redis --update-delay 10s redis:3.0.6
```

![img](./images/Docker-66.png)

滚动升级 redis 。

```
docker@swarm-manager:~$ docker service update --image redis:3.0.7 redis
```

![img](./images/Docker-67.png)

看图可以知道 redis 的版本已经从 3.0.6 升级到了 3.0.7，说明服务已经升级成功。

### 11.3.9. 9、停止某个节点接收新的任务

查看所有的节点：

```
docker@swarm-manager:~$ docker node ls
```

![img](./images/Docker-68.png)

可以看到目前所有的节点都是 Active, 可以接收新的任务分配。

停止节点 swarm-worker1：

![img](./images/Docker-69.png)

**注意**：swarm-worker1 状态变为 Drain。不会影响到集群的服务，只是 swarm-worker1 节点不再接收新的任务，集群的负载能力有所下降。

可以通过以下命令重新激活节点：

```
docker@swarm-manager:~$  docker node update --availability active swarm-worker1
```

![img](./images/Docker-70.png)

# 12. Docker 命令大全

------

## 12.1. 容器生命周期管理

### 12.1.1. run

**docker run ：**创建一个新的容器并运行一个命令

#### 12.1.1.1. 语法

```
docker run [OPTIONS] IMAGE [COMMAND] [ARG...]
```

OPTIONS说明：

- **-a stdin:** 指定标准输入输出内容类型，可选 STDIN/STDOUT/STDERR 三项；
- **-d:** 后台运行容器，并返回容器ID；
- **-i:** 以交互模式运行容器，通常与 -t 同时使用；
- **-P:** 随机端口映射，容器内部端口**随机**映射到主机的端口
- **-p:** 指定端口映射，格式为：**主机(宿主)端口:容器端口**
- **-t:** 为容器重新分配一个伪输入终端，通常与 -i 同时使用；
- **--name="nginx-lb":** 为容器指定一个名称；
- **--dns 8.8.8.8:** 指定容器使用的DNS服务器，默认和宿主一致；
- **--dns-search example.com:** 指定容器DNS搜索域名，默认和宿主一致；
- **-h "mars":** 指定容器的hostname；
- **-e username="ritchie":** 设置环境变量；
- **--env-file=[]:** 从指定文件读入环境变量；
- **--cpuset="0-2" or --cpuset="0,1,2":** 绑定容器到指定CPU运行；
- **-m :**设置容器使用内存最大值；
- **--net="bridge":** 指定容器的网络连接类型，支持 bridge/host/none/container: 四种类型；
- **--link=[]:** 添加链接到另一个容器；
- **--expose=[]:** 开放一个端口或一组端口；
- **--volume , -v:** 绑定一个卷

#### 12.1.1.2. 实例

使用docker镜像nginx:latest以后台模式启动一个容器,并将容器命名为mynginx。

```
docker run --name mynginx -d nginx:latest
```

使用镜像nginx:latest以后台模式启动一个容器,并将容器的80端口映射到主机随机端口。

```
docker run -P -d nginx:latest
```

使用镜像 nginx:latest，以后台模式启动一个容器,将容器的 80 端口映射到主机的 80 端口,主机的目录 /data 映射到容器的 /data。

```
docker run -p 80:80 -v /data:/data -d nginx:latest
```

绑定容器的 8080 端口，并将其映射到本地主机 127.0.0.1 的 80 端口上。

```
$ docker run -p 127.0.0.1:80:8080/tcp ubuntu bash
```

使用镜像nginx:latest以交互模式启动一个容器,在容器内执行/bin/bash命令。

```
runoob@runoob:~$ docker run -it nginx:latest /bin/bash
root@b8573233d675:/#
```

### 12.1.2. start/stop/restart

**docker start** :启动一个或多个已经被停止的容器（容器以空格分隔）

**docker stop** :停止一个运行中的容器

**docker restart** :重启容器

#### 12.1.2.1. 语法

```
docker start [OPTIONS] CONTAINER [CONTAINER...]
docker stop [OPTIONS] CONTAINER [CONTAINER...]
docker restart [OPTIONS] CONTAINER [CONTAINER...]
```

### 12.1.3. kill

**docker kill** :杀掉一个运行中的容器。

#### 12.1.3.1. 语法

```
docker kill [OPTIONS] CONTAINER [CONTAINER...]
```

OPTIONS说明：

- **-s :**向容器发送一个信号

#### 12.1.3.2. 实例

杀掉运行中的容器mynginx

```
runoob@runoob:~$ docker kill -s KILL mynginx
mynginx
```

#### 12.1.3.3. stop和kill的却别

- **docker stop**，支持“优雅退出”。先发送SIGTERM信号，在一段时间之后（10s）再发送SIGKILL信号。Docker内部的应用程序可以接收SIGTERM信号，然后做一些“退出前工作”，比如保存状态、处理当前请求等。
- **docker kill**，发送SIGKILL信号，应用程序直接退出。

### 12.1.4. rm

**docker rm ：**删除一个或多个容器。

#### 12.1.4.1. 语法

```
docker rm [OPTIONS] CONTAINER [CONTAINER...]
```

OPTIONS说明：

- **-f :**通过 SIGKILL 信号强制删除一个运行中的容器。
- **-l :**移除容器间的网络连接，而非容器本身。
- **-v :**删除与容器关联的卷。

#### 12.1.4.2. 实例

强制删除容器 db01、db02：

```
docker rm -f db01 db02
```

移除容器 nginx01 对容器 db01 的连接，连接名 db：

```
docker rm -l db 
```

删除容器 nginx01, 并删除容器挂载的数据卷：

```
docker rm -v nginx01
```

删除所有已经停止的容器：

```
docker rm $(docker ps -a -q)
```

### 12.1.5. pause/unpause

**docker pause** :暂停容器中所有的进程。

**docker unpause** :恢复容器中所有的进程。

#### 12.1.5.1. 语法

```
docker pause CONTAINER [CONTAINER...]
docker unpause CONTAINER [CONTAINER...]
```

#### 12.1.5.2. 实例

暂停数据库容器db01提供服务。

```
docker pause db01
```

恢复数据库容器 db01 提供服务。

```
docker unpause db01
```

#### 12.1.5.3. stop与pause的区别

docker stop container_id与docker pause container_id都可以起到停止容器运行的目的但其中有一些不同。

**docker pause**

docker pause命令暂停指定容器中的所有进程。在 Linux 上，这使用 cgroups freezer。传统上，当挂起一个进程时，会使用 SIGSTOP 信号，这个信号可以被被挂起的进程观察到

**docker stop**

docker stop命令。容器内的主进程会收到 SIGTERM，在一个宽限期后，会收到 SIGKILL。

**补充**

SIGTERM是终止信号。默认行为是终止进程，但它也可以被捕获或忽略。目的是要终止进程，无论是否优雅，但首先要让它有机会进行清理。

SIGKILL是终止信号。唯一的行为是立即终止进程。由于进程无法捕获信号，因此无法清除，因此这是最后的信号。

SIGSTOP是暂停信号。唯一的行为是暂停进程；信号不能被捕获或忽略。Shell 使用暂停（及其对应项，通过 SIGCONT 恢复）来实现作业控制。

### 12.1.6. create

**docker create ：**创建一个新的容器但不启动它

用法同 **docker run**

#### 12.1.6.1. 语法

```
docker create [OPTIONS] IMAGE [COMMAND] [ARG...]
```

用法同 **docker run**

### 12.1.7. exec

**docker exec ：**在运行的容器中执行命令

#### 12.1.7.1. 语法

```
docker exec [OPTIONS] CONTAINER COMMAND [ARG...]
```

OPTIONS说明：

- **-d :**分离模式: 在后台运行
- **-i :**即使没有附加也保持STDIN 打开
- **-t :**分配一个伪终端

#### 12.1.7.2. 实例

在容器 mynginx 中以交互模式执行容器内 /root/runoob.sh 脚本:

```
runoob@runoob:~$ docker exec -it mynginx /bin/sh /root/runoob.sh
http://www.runoob.com/
```

在容器 mynginx 中开启一个交互模式的终端:

```
runoob@runoob:~$ docker exec -i -t  mynginx /bin/bash
root@b1a0703e41e7:/#
```

## 12.2. 容器操作

### 12.2.1. ps

**docker ps :** 列出容器

#### 12.2.1.1. 语法

```
docker ps [OPTIONS]
```

OPTIONS说明：

- **-a :**显示所有的容器，包括未运行的。

- **-f :**根据条件过滤显示的内容。

  

- **--format :**指定返回值的模板文件。

- **-l :**显示最近创建的容器。

- **-n :**列出最近创建的n个容器。

- **--no-trunc :**不截断输出。

- **-q :**静默模式，只显示容器编号。

- **-s :**显示总的文件大小。

#### 12.2.1.2. 实例

列出所有在运行的容器信息。

```
runoob@runoob:~$ docker ps
CONTAINER ID   IMAGE          COMMAND                ...  PORTS                    NAMES
09b93464c2f7   nginx:latest   "nginx -g 'daemon off" ...  80/tcp, 443/tcp          myrunoob
96f7f14e99ab   mysql:5.6      "docker-entrypoint.sh" ...  0.0.0.0:3306->3306/tcp   mymysql
```

输出详情介绍：

**CONTAINER ID:** 容器 ID。

**IMAGE:** 使用的镜像。

**COMMAND:** 启动容器时运行的命令。

**CREATED:** 容器的创建时间。

**STATUS:** 容器状态。

状态有7种：

- created（已创建）
- restarting（重启中）
- running（运行中）
- removing（迁移中）
- paused（暂停）
- exited（停止）
- dead（死亡）

**PORTS:** 容器的端口信息和使用的连接类型（tcp\udp）。

**NAMES:** 自动分配的容器名称。

列出最近创建的5个容器信息。

```
runoob@runoob:~$ docker ps -n 5
CONTAINER ID        IMAGE               COMMAND                   CREATED           
09b93464c2f7        nginx:latest        "nginx -g 'daemon off"    2 days ago   ...     
b8573233d675        nginx:latest        "/bin/bash"               2 days ago   ...     
b1a0703e41e7        nginx:latest        "nginx -g 'daemon off"    2 days ago   ...    
f46fb1dec520        5c6e1090e771        "/bin/sh -c 'set -x \t"   2 days ago   ...   
a63b4a5597de        860c279d2fec        "bash"                    2 days ago   ...
```

列出所有创建的容器ID。

```
runoob@runoob:~$ docker ps -a -q
09b93464c2f7
b8573233d675
b1a0703e41e7
f46fb1dec520
a63b4a5597de
6a4aa42e947b
de7bb36e7968
43a432b73776
664a8ab1a585
ba52eb632bbd
...
```

### 12.2.2. inspect

**docker inspect :** 获取容器/镜像的元数据。

#### 12.2.2.1. 语法

```
docker inspect [OPTIONS] NAME|ID [NAME|ID...]
```

OPTIONS说明：

- **-f :**指定返回值的模板文件。
- **-s :**显示总的文件大小。
- **--type :**为指定类型返回JSON。

#### 12.2.2.2. 实例

获取镜像mysql:5.6的元信息。

```
runoob@runoob:~$ docker inspect mysql:5.6
```

### 12.2.3. top

**docker top :**查看容器中运行的进程信息，支持 ps 命令参数。

#### 12.2.3.1. 语法

```
docker top [OPTIONS] CONTAINER [ps OPTIONS]
```

容器运行时不一定有/bin/bash终端来交互执行top命令，而且容器还不一定有top命令，可以使用docker top来实现查看container中正在运行的进程。

#### 12.2.3.2. 实例

查看容器mymysql的进程信息。

```
runoob@runoob:~/mysql$ docker top mymysql
UID    PID    PPID    C      STIME   TTY  TIME       CMD
999    40347  40331   18     00:58   ?    00:00:02   mysqld
```

查看所有运行容器的进程信息。

```
for i in  `docker ps |grep Up|awk '{print $1}'`;do echo \ &&docker top $i; done
```

### 12.2.4. attach

**docker attach :**连接到正在运行中的容器（和exec类似，但是attach是直接连接并进入到容器，退出的话会导致容器停止，而exec只是模拟一个为终端执行命令，类似于SSH，所以attach不适合在生产环境，因为对容器的影像大）。

#### 12.2.4.1. 语法

```
docker attach [OPTIONS] CONTAINER
```

要attach上去的容器必须正在运行，可以同时连接上同一个container来共享屏幕（与screen命令的attach类似）。

官方文档中说attach后可以通过CTRL-C来detach，但实际上经过我的测试，如果container当前在运行bash，CTRL-C自然是当前行的输入，没有退出；如果container当前正在前台运行进程，如输出nginx的access.log日志，CTRL-C不仅会导致退出容器，而且还stop了。这不是我们想要的，detach的意思按理应该是脱离容器终端，但容器依然运行。好在attach是可以带上--sig-proxy=false来确保CTRL-D或CTRL-C不会关闭容器。

#### 12.2.4.2. 实例

容器mynginx将访问日志指到标准输出，连接到容器查看访问信息。

```
runoob@runoob:~$ docker attach --sig-proxy=false mynginx
192.168.239.1 - - [10/Jul/2016:16:54:26 +0000] "GET / HTTP/1.1" 304 0 "-" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36" "-"
```

### 12.2.5. events

**docker events :** 从服务器获取实时事件

#### 12.2.5.1. 语法

```
docker events [OPTIONS]
```

OPTIONS说明：

- **-f ：**根据条件过滤事件；
- **--since ：**从指定的时间戳后显示所有事件;
- **--until ：**流水时间显示到指定的时间为止；

#### 12.2.5.2. 实例

显示docker 2016年7月1日后的所有事件。

```
runoob@runoob:~/mysql$ docker events --since="1467302400"
2016-07-08T19:44:54.501277677+08:00 network connect 66f958fd13dc4314ad20034e576d5c5eba72e0849dcc38ad9e8436314a4149d4 (container=b8573233d675705df8c89796a2c2687cd8e36e03646457a15fb51022db440e64, name=bridge, type=bridge)
...
```

显示docker 镜像为mysql:5.6 2016年7月1日后的相关事件。

```
runoob@runoob:~/mysql$ docker events -f "image"="mysql:5.6" --since="1467302400" 
2016-07-11T00:38:53.975174837+08:00 container start 96f7f14e99ab9d2f60943a50be23035eda1623782cc5f930411bbea407a2bb10 (image=mysql:5.6, name=mymysql)
...
```

如果指定的时间是到秒级的，需要将时间转成时间戳。如果时间为日期的话，可以直接使用，如--since="2016-07-01"。

### 12.2.6. logs

**docker logs :** 获取容器的日志

#### 12.2.6.1. 语法

```
docker logs [OPTIONS] CONTAINER
```

OPTIONS说明：

- **-f :** 跟踪日志输出
- **--since :**显示某个开始时间的所有日志
- **-t :** 显示时间戳
- **--tail :**仅列出最新N条容器日志

#### 12.2.6.2. 实例

跟踪查看容器mynginx的日志输出。

```
runoob@runoob:~$ docker logs -f mynginx
192.168.239.1 - - [10/Jul/2016:16:53:33 +0000] "GET / HTTP/1.1" 200 612 "-" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36" "-"
2016/07/10 16:53:33 [error] 5#5: *1 open() "/usr/share/nginx/html/favicon.ico" failed (2: No such file or directory), client: 
...
```

查看容器mynginx从2016年7月1日后的最新10条日志。

```
docker logs --since="2016-07-01" --tail=10 mynginx
```

### 12.2.7. wait

**docker wait :** 阻塞运行直到容器停止，然后打印出它的退出代码。

#### 12.2.7.1. 语法

```
docker wait [OPTIONS] CONTAINER [CONTAINER...]
```

#### 12.2.7.2. 实例

```
docker wait CONTAINER
```

### 12.2.8. export

**docker export :**将容器文件系统作为一个tar归档文件导出到STDOUT。

#### 12.2.8.1. 语法

```
docker export [OPTIONS] CONTAINER
```

OPTIONS说明：

- **-o :**将输入内容写到文件。

#### 12.2.8.2. 实例

将id为a404c6c174a2的容器按日期保存为tar文件。

```
runoob@runoob:~$ docker export -o mysql-`date +%Y%m%d`.tar a404c6c174a2
runoob@runoob:~$ ls mysql-`date +%Y%m%d`.tar
mysql-20160711.tar
```

### 12.2.9. port

**docker port :**列出指定的容器的端口映射，或者查找将PRIVATE_PORT NAT到面向公众的端口。

#### 12.2.9.1. 语法

```
docker port [OPTIONS] CONTAINER [PRIVATE_PORT[/PROTO]]
```

#### 12.2.9.2. 实例

查看容器mynginx的端口映射情况。

```
runoob@runoob:~$ docker port mymysql
3306/tcp -> 0.0.0.0:3306
```

## 12.3. 容器rootfs命令

### 12.3.1. commit

**docker commit :**从容器创建一个新的镜像。有时候需要在基础镜像里面安装某些依赖，如果把命令写在Dockerfile里，部分依赖软件下载很慢，构建镜像的时候又会花很长的时间。所以最好是分装一个包含依赖库的新镜像。

#### 12.3.1.1. 语法

```
docker commit [OPTIONS] CONTAINER [REPOSITORY[:TAG]]
```

OPTIONS说明：

- **-a :**提交的镜像作者；
- **-c :**使用Dockerfile指令来创建镜像；
- **-m :**提交时的说明文字；
- **-p :**在commit时，将容器暂停。

#### 12.3.1.2. 实例

将容器a404c6c174a2 保存为新的镜像,并添加提交人信息和说明信息。

```
runoob@runoob:~$ docker commit -a "runoob.com" -m "my apache" a404c6c174a2  mymysql:v1 
sha256:37af1236adef1544e8886be23010b66577647a40bc02c0885a6600b33ee28057
runoob@runoob:~$ docker images mymysql:v1
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
mymysql             v1                  37af1236adef        15 seconds ago      329 MB
```

### 12.3.2. cp

**docker cp :**用于容器与主机之间的数据拷贝。

#### 12.3.2.1. 语法

```
docker cp [OPTIONS] CONTAINER:SRC_PATH DEST_PATH|-
docker cp [OPTIONS] SRC_PATH|- CONTAINER:DEST_PATH
```

OPTIONS说明：

- **-L :**保持源目标中的链接

#### 12.3.2.2. 实例

将主机/www/runoob目录拷贝到容器96f7f14e99ab的/www目录下。

```
docker cp /www/runoob 96f7f14e99ab:/www/
```

将主机/www/runoob目录拷贝到容器96f7f14e99ab中，目录重命名为www。

```
docker cp /www/runoob 96f7f14e99ab:/www
```

将容器96f7f14e99ab的/www目录拷贝到主机的/tmp目录中。

```
docker cp  96f7f14e99ab:/www /tmp/
```

### 12.3.3. diff

**docker diff :** 检查容器里文件结构的更改。

#### 12.3.3.1. 语法

```
docker diff [OPTIONS] CONTAINER
```

#### 12.3.3.2. 实例

查看容器mymysql的文件结构更改。

```
runoob@runoob:~$ docker diff mymysql
A /logs
A /mysql_data
C /run
C /run/mysqld
A /run/mysqld/mysqld.pid
A /run/mysqld/mysqld.sock
C /tmp
```

该命令可以追踪以下三种变化：

| 符号 | 描述             |
| :--- | :--------------- |
| A    | 创建了文件或目录 |
| D    | 删除了文件或目录 |
| C    | 修改了文件或目录 |

## 12.4. 镜像仓库

### 12.4.1. login/logout

**docker login :** 登陆到一个Docker镜像仓库，如果未指定镜像仓库地址，默认为官方仓库 Docker Hub

**docker logout :** 登出一个Docker镜像仓库，如果未指定镜像仓库地址，默认为官方仓库 Docker Hub

#### 12.4.1.1. 语法

```
docker login [OPTIONS] [SERVER]
docker logout [OPTIONS] [SERVER]
```

OPTIONS说明：

- **-u :**登陆的用户名
- **-p :**登陆的密码

#### 12.4.1.2. 实例

登陆到Docker Hub

```
docker login -u 用户名 -p 密码
```

登出Docker Hub

```
docker logout
```

### 12.4.2. pull

**docker pull :** 从镜像仓库中拉取或者更新指定镜像

#### 12.4.2.1. 语法

```
docker pull [OPTIONS] NAME[:TAG|@DIGEST]
```

OPTIONS说明：

- **-a :**拉取所有 tagged 镜像
- **--disable-content-trust :**忽略镜像的校验,默认开启

#### 12.4.2.2. 实例

从Docker Hub下载java最新版镜像。

```
docker pull java
```

从Docker Hub下载REPOSITORY为java的所有镜像。

```
docker pull -a java
```

### 12.4.3. search

**docker search :** 从Docker Hub查找镜像，**如果登录其他Hub，比如企业版的Hub，则不能使用该命令**。

#### 12.4.3.1. 语法

```
docker search [OPTIONS] TERM
```

OPTIONS说明：

- **--automated :**只列出 automated build类型的镜像；
- **--no-trunc :**显示完整的镜像描述；
- **-f <过滤条件>:**列出收藏数不小于指定值的镜像。

#### 12.4.3.2. 实例

从 Docker Hub 查找所有镜像名包含 java，并且收藏数大于 10 的镜像

```
runoob@runoob:~$ docker search -f stars=10 java
NAME                  DESCRIPTION                           STARS   OFFICIAL   AUTOMATED
java                  Java is a concurrent, class-based...   1037    [OK]       
anapsix/alpine-java   Oracle Java 8 (and 7) with GLIBC ...   115                [OK]
develar/java                                                 46                 [OK]
isuper/java-oracle    This repository contains all java...   38                 [OK]
lwieske/java-8        Oracle Java 8 Container - Full + ...   27                 [OK]
nimmis/java-centos    This is docker images of CentOS 7...   13                 [OK]
```

参数说明：

**NAME:** 镜像仓库源的名称

**DESCRIPTION:** 镜像的描述

**OFFICIAL:** 是否 docker 官方发布

**stars:** 类似 Github 里面的 star，表示点赞、喜欢的意思。

**AUTOMATED:** 自动构建，就是使用Docker Hub连接一个包含Dockerfile文件的GitHub仓库或者BitBucket仓库，Docker Hub则会自动构建镜像，通过这种方式构建出来的镜像会被标记为Automated Build，也称之为受信构建（Trusted Build），这种构建方式构建出来的镜像，其他人在使用时可以自由的查看Dockerfile内容，知道该镜像是怎么来的，同时，由于构建过程是自动的，所以能够确保仓库中的镜像都是最新的。

## 12.5. 本地镜像管理

### 12.5.1. images

**docker images :** 列出本地镜像。

#### 12.5.1.1. 语法

```
docker images [OPTIONS] [REPOSITORY[:TAG]]
```

OPTIONS说明：

- **-a :**列出本地所有的镜像（含中间映像层，默认情况下，过滤掉中间映像层）；
- **--digests :**显示镜像的摘要信息；
- **-f :**显示满足条件的镜像；
- **--format :**指定返回值的模板文件；
- **--no-trunc :**显示完整的镜像信息；
- **-q :**只显示镜像ID。

#### 12.5.1.2. 实例

查看本地镜像列表。

```
runoob@runoob:~$ docker images
REPOSITORY              TAG                 IMAGE ID            CREATED             SIZE
mymysql                 v1                  37af1236adef        5 minutes ago       329 MB
...
```

列出本地镜像中REPOSITORY为ubuntu的镜像列表。

```
root@runoob:~# docker images ubuntu
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
ubuntu              14.04               90d5884b1ee0        9 weeks ago         188 MB
ubuntu              15.10               4e3b13c8a266        3 months ago        136.3 MB
```

### 12.5.2. rmi

**docker rmi :** 删除本地一个或多个镜像。

#### 12.5.2.1. 语法

```
docker rmi [OPTIONS] IMAGE [IMAGE...]
```

OPTIONS说明：

- **-f :**强制删除；
- **--no-prune :**不移除该镜像的过程镜像，默认移除；

#### 12.5.2.2. 实例

强制删除本地镜像 runoob/ubuntu:v4。

```
root@runoob:~# docker rmi -f runoob/ubuntu:v4
Untagged: runoob/ubuntu:v4
Deleted: sha256:1c06aa18edee44230f93a90a7d88139235de12cd4c089d41eed8419b503072be
Deleted: sha256:85feb446e89a28d58ee7d80ea5ce367eebb7cec70f0ec18aa4faa874cbd97c73
```

### 12.5.3. tag

**docker tag :** 标记本地镜像，将其归入某一仓库。

#### 12.5.3.1. 语法

```
docker tag [OPTIONS] IMAGE[:TAG] [REGISTRYHOST/][USERNAME/]NAME[:TAG]
```

#### 12.5.3.2. 实例

将镜像ubuntu:15.10标记为 runoob/ubuntu:v3 镜像。

```
root@runoob:~# docker tag ubuntu:15.10 runoob/ubuntu:v3
root@runoob:~# docker images runoob/ubuntu:v3
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
runoob/ubuntu       v3                  4e3b13c8a266        3 months ago        136.3 MB
```

### 12.5.4. build

**docker build** 命令用于使用 Dockerfile 创建镜像。

#### 12.5.4.1. 语法

```
docker build [OPTIONS] PATH | URL | -
```

OPTIONS说明：

- **--build-arg=[] :**设置镜像创建时的变量；
- **--cpu-shares :**设置 cpu 使用权重；
- **--cpu-period :**限制 CPU CFS周期；
- **--cpu-quota :**限制 CPU CFS配额；
- **--cpuset-cpus :**指定使用的CPU id；
- **--cpuset-mems :**指定使用的内存 id；
- **--disable-content-trust :**忽略校验，默认开启；
- **-f :**指定要使用的Dockerfile路径；
- **--force-rm :**设置镜像过程中删除中间容器；
- **--isolation :**使用容器隔离技术；
- **--label=[] :**设置镜像使用的元数据；
- **-m :**设置内存最大值；
- **--memory-swap :**设置Swap的最大值为内存+swap，"-1"表示不限swap；
- **--no-cache :**创建镜像的过程不使用缓存；
- **--pull :**尝试去更新镜像的新版本；
- **--quiet, -q :**安静模式，成功后只输出镜像 ID；
- **--rm :**设置镜像成功后删除中间容器；
- **--shm-size :**设置/dev/shm的大小，默认值是64M；
- **--ulimit :**Ulimit配置。
- **--squash :**将 Dockerfile 中所有的操作压缩为一层。
- **--tag, -t:** 镜像的名字及标签，通常 name:tag 或者 name 格式；可以在一次构建中为一个镜像设置多个标签。
- **--network:** 默认 default。在构建期间设置RUN指令的网络模式

#### 12.5.4.2. 实例

使用当前目录的 Dockerfile 创建镜像，标签为 runoob/ubuntu:v1。

```
docker build -t runoob/ubuntu:v1 .
```

使用URL **github.com/creack/docker-firefox** 的 Dockerfile 创建镜像。

```
docker build github.com/creack/docker-firefox
```

也可以通过 -f Dockerfile 文件的位置：

```
$ docker build -f /path/to/a/Dockerfile .
```

在 Docker 守护进程执行 Dockerfile 中的指令前，首先会对 Dockerfile 进行语法检查，有语法错误时会返回：

```
$ docker build -t test/myapp .
Sending build context to Docker daemon 2.048 kB
Error response from daemon: Unknown instruction: RUNCMD
```

### 12.5.5. history

**docker history :** 查看指定镜像的创建历史。加上**--no-trunc**参数可以查看完整的构建过程，类似于看到了镜像的Dockerfile文件。

#### 12.5.5.1. 语法

```
docker history [OPTIONS] IMAGE
```

OPTIONS说明：

- **-H :**以可读的格式打印镜像大小和日期，默认为true；
- **--no-trunc :**显示完整的提交记录!!!!!!；
- **-q :**仅列出提交记录ID。

#### 12.5.5.2. 实例

查看本地镜像runoob/ubuntu:v3的创建历史。

```
root@runoob:~# docker history runoob/ubuntu:v3
IMAGE             CREATED           CREATED BY                                      SIZE      COMMENT
4e3b13c8a266      3 months ago      /bin/sh -c #(nop) CMD ["/bin/bash"]             0 B                 
<missing>         3 months ago      /bin/sh -c sed -i 's/^#\s*\(deb.*universe\)$/   1.863 kB            
<missing>         3 months ago      /bin/sh -c set -xe   && echo '#!/bin/sh' > /u   701 B               
<missing>         3 months ago      /bin/sh -c #(nop) ADD file:43cb048516c6b80f22   136.3 MB
```

### 12.5.6. save

**docker save :** 将指定镜像保存成 tar 归档文件。

#### 12.5.6.1. 语法

```
docker save [OPTIONS] IMAGE [IMAGE...]
```

OPTIONS 说明：

- **-o :**输出到的文件。

#### 12.5.6.2. 实例

将镜像 runoob/ubuntu:v3 生成 my_ubuntu_v3.tar 文档

```
runoob@runoob:~$ docker save -o my_ubuntu_v3.tar runoob/ubuntu:v3
runoob@runoob:~$ ll my_ubuntu_v3.tar
-rw------- 1 runoob runoob 142102016 Jul 11 01:37 my_ubuntu_v3.ta
```

### 12.5.7. load

**docker load :** 导入使用 **docker save** 命令导出的镜像。

#### 12.5.7.1. 语法

```
docker load [OPTIONS]
```

OPTIONS 说明：

- **--input , -i :** 指定导入的文件，代替 STDIN。
- **--quiet , -q :** 精简输出信息。

#### 12.5.7.2. 实例

导入镜像：

```
$ docker image ls
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE

$ docker load < busybox.tar.gz
Loaded image: busybox:latest

$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
busybox             latest              769b9341d937        7 weeks ago         2.489 MB

$ docker load --input fedora.tar
Loaded image: fedora:rawhide
Loaded image: fedora:20

$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
busybox             latest              769b9341d937        7 weeks ago         2.489 MB
fedora              rawhide             0d20aec6529d        7 weeks ago         387 MB
fedora              20                  58394af37342        7 weeks ago         385.5 MB
fedora              heisenbug           58394af37342        7 weeks ago         385.5 MB
fedora              latest              58394af37342        7 weeks ago         385.5 MB
```

### 12.5.8. import

**docker import :** 从归档文件中创建镜像。

#### 12.5.8.1. 语法

```
docker import [OPTIONS] file|URL|- [REPOSITORY[:TAG]]
```

OPTIONS说明：

- **-c :**应用docker 指令创建镜像；
- **-m :**提交时的说明文字；

#### 12.5.8.2. 实例

从镜像归档文件my_ubuntu_v3.tar创建镜像，命名为runoob/ubuntu:v4

```
runoob@runoob:~$ docker import my_ubuntu_v3.tar runoob/ubuntu:v4  
sha256:63ce4a6d6bc3fabb95dbd6c561404a309b7bdfc4e21c1d59fe9fe4299cbfea39
runoob@runoob:~$ docker images runoob/ubuntu:v4
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
runoob/ubuntu       v4                  63ce4a6d6bc3        20 seconds ago      142.1 MB
```

## 12.6. info|version

### 12.6.1. info

**docker info** : 显示 Docker 系统信息，包括镜像和容器数。。

#### 12.6.1.1. 语法

```
docker info [OPTIONS]
```

#### 12.6.1.2. 实例

查看docker系统信息。

```
$ docker info
Containers: 12
Images: 41
Storage Driver: aufs
 Root Dir: /var/lib/docker/aufs
 Backing Filesystem: extfs
 Dirs: 66
 Dirperm1 Supported: false
Execution Driver: native-0.2
Logging Driver: json-file
Kernel Version: 3.13.0-32-generic
Operating System: Ubuntu 14.04.1 LTS
CPUs: 1
Total Memory: 1.954 GiB
Name: iZ23mtq8bs1Z
ID: M5N4:K6WN:PUNC:73ZN:AONJ:AUHL:KSYH:2JPI:CH3K:O4MK:6OCX:5OYW
```

### 12.6.2. version

docker version :显示 Docker 版本信息。

#### 12.6.2.1. 语法

```
docker version [OPTIONS]
```

OPTIONS说明：

- **-f :**指定返回值的模板文件。

#### 12.6.2.2. 实例

显示 Docker 版本信息。

```
$ docker version
Client:
 Version:      1.8.2
 API version:  1.20
 Go version:   go1.4.2
 Git commit:   0a8c2e3
 Built:        Thu Sep 10 19:19:00 UTC 2015
 OS/Arch:      linux/amd64

Server:
 Version:      1.8.2
 API version:  1.20
 Go version:   go1.4.2
 Git commit:   0a8c2e3
 Built:        Thu Sep 10 19:19:00 UTC 2015
 OS/Arch:      linux/amd64
```



