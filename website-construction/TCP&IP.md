<h2>目录</h2>

<details open>
  <summary><a href="#1-tcpip-介绍">1. TCP/IP 介绍</a></summary>
  <ul>
    <a href="#11-计算机通信协议（computer-communication-protocol）">1.1. 计算机通信协议（Computer Communication Protocol）</a><br>
    <a href="#12-什么是-tcpip？">1.2. 什么是 TCP/IP？</a><br>
    <a href="#13-在-tcpip-内部">1.3. 在 TCP/IP 内部</a><br>
    <a href="#14-tcp-使用固定的连接">1.4. TCP 使用固定的连接</a><br>
    <a href="#15-ip-是无连接的">1.5. IP 是无连接的</a><br>
    <a href="#16-ip-路由器">1.6. IP 路由器</a><br>
    <a href="#17-tcpip">1.7. TCP/IP</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#2-tcpip-寻址">2. TCP/IP 寻址</a></summary>
  <ul>
    <a href="#21-ip地址">2.1. IP地址</a><br>
    <a href="#22-ip-地址包含-4-组数字：">2.2. IP 地址包含 4 组数字：</a><br>
    <a href="#23-32-比特--4-字节">2.3. 32 比特 = 4 字节</a><br>
    <a href="#24-ip-v6">2.4. IP V6</a><br>
    <a href="#25-域名">2.5. 域名</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#3-tcpip-协议">3. TCP/IP 协议</a></summary>
  <ul>
    <a href="#31-协议族">3.1. 协议族</a><br>
    <a href="#32-tcp---传输控制协议">3.2. TCP - 传输控制协议</a><br>
    <a href="#33-ip---网际协议（internet-protocol）">3.3. IP - 网际协议（Internet Protocol）</a><br>
    <a href="#34-http---超文本传输协议hyper-text-transfer-protocol">3.4. HTTP - 超文本传输协议(Hyper Text Transfer Protocol)</a><br>
    <a href="#35-https---安全的-http（http-secure）">3.5. HTTPS - 安全的 HTTP（HTTP Secure）</a><br>
    <a href="#36-ssl---安全套接字层（secure-sockets-layer）">3.6. SSL - 安全套接字层（Secure Sockets Layer）</a><br>
    <a href="#37-smtp---简易邮件传输协议（simple-mail-transfer-protocol）">3.7. SMTP - 简易邮件传输协议（Simple Mail Transfer Protocol）</a><br>
    <a href="#38-mime---多用途因特网邮件扩展（multi-purpose-internet-mail-extensions）">3.8. MIME - 多用途因特网邮件扩展（Multi-purpose Internet Mail Extensions）</a><br>
    <a href="#39-imap---因特网消息访问协议（internet-message-access-protocol）">3.9. IMAP - 因特网消息访问协议（Internet Message Access Protocol）</a><br>
    <a href="#310-pop---邮局协议（post-office-protocol）">3.10. POP - 邮局协议（Post Office Protocol）</a><br>
    <a href="#311-ftp---文件传输协议（file-transfer-protocol）">3.11. FTP - 文件传输协议（File Transfer Protocol）</a><br>
    <a href="#312-ntp---网络时间协议（network-time-protocol）">3.12. NTP - 网络时间协议（Network Time Protocol）</a><br>
    <a href="#313-dhcp---动态主机配置协议（dynamic-host-configuration-protocol）">3.13. DHCP - 动态主机配置协议（Dynamic Host Configuration Protocol）</a><br>
    <a href="#314-snmp---简单网络管理协议（simple-network-management-protocol）">3.14. SNMP - 简单网络管理协议（Simple Network Management Protocol）</a><br>
    <a href="#315-ldap---轻量级的目录访问协议（lightweight-directory-access-protocol）">3.15. LDAP - 轻量级的目录访问协议（Lightweight Directory Access Protocol）</a><br>
    <a href="#316-icmp---因特网消息控制协议（internet-control-message-protocol）">3.16. ICMP - 因特网消息控制协议（Internet Control Message Protocol）</a><br>
    <a href="#317-arp---地址解析协议（address-resolution-protocol）">3.17. ARP - 地址解析协议（Address Resolution Protocol）</a><br>
    <a href="#318-rarp---反向地址转换协议（reverse-address-resolution-protocol）">3.18. RARP - 反向地址转换协议（Reverse Address Resolution Protocol）</a><br>
    <a href="#319-bootp---自举协议（boot-protocol）">3.19. BOOTP - 自举协议（Boot Protocol）</a><br>
    <a href="#320-pptp---点对点隧道协议（point-to-point-tunneling-protocol）">3.20. PPTP - 点对点隧道协议（Point to Point Tunneling Protocol）</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#4-tcpip-邮件">4. TCP/IP 邮件</a></summary>
  <ul>
    <a href="#41-您不会用到">4.1. 您不会用到...</a><br>
    <a href="#42-邮件程序会用到">4.2. 邮件程序会用到...</a><br>
    <a href="#43-smtp---简单邮件传输协议">4.3. SMTP - 简单邮件传输协议</a><br>
    <a href="#44-pop---邮局协议">4.4. POP - 邮局协议</a><br>
    <a href="#45-imap---因特网消息访问协议">4.5. IMAP - 因特网消息访问协议</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#5-http-与-https-的区别">5. HTTP 与 HTTPS 的区别</a></summary>
  <ul>
    <a href="#51-基本概念">5.1. 基本概念</a><br>
    <a href="#52-http-与-https-区别">5.2. HTTP 与 HTTPS 区别</a><br>
    <a href="#53-tcp-三次握手">5.3. TCP 三次握手</a><br>
    <a href="#54-https-的工作原理">5.4. HTTPS 的工作原理</a><br>
  </ul>
</details>


<h1>TCP/IP</h1>

# 1. TCP/IP 介绍

------

TCP/IP 是用于因特网 (Internet) 的通信协议。

------

## 1.1. 计算机通信协议（Computer Communication Protocol）

计算机通信协议是对那些计算机必须遵守以便彼此通信的的规则的描述。

------

## 1.2. 什么是 TCP/IP？

TCP/IP 是供已连接因特网的计算机进行通信的通信协议。

TCP/IP 指传输控制协议/网际协议（**Transmission Control Protocol** / **Internet Protocol**）。

TCP/IP 定义了电子设备（比如计算机）如何连入因特网，以及数据如何在它们之间传输的标准。

------

## 1.3. 在 TCP/IP 内部

在 TCP/IP 中包含一系列用于处理数据通信的协议：

- TCP (传输控制协议) - 应用程序之间通信
- UDP (用户数据报协议) - 应用程序之间的简单通信
- IP (网际协议) - 计算机之间的通信
- ICMP (因特网消息控制协议) - 针对错误和状态
- DHCP (动态主机配置协议) - 针对动态寻址

------

## 1.4. TCP 使用固定的连接

TCP 用于应用程序之间的通信。

当应用程序希望通过 TCP 与另一个应用程序通信时，它会发送一个通信请求。这个请求必须被送到一个确切的地址。在双方"握手"之后，TCP 将在两个应用程序之间建立一个全双工 (full-duplex) 的通信。

这个全双工的通信将占用两个计算机之间的通信线路，直到它被一方或双方关闭为止。

UDP 和 TCP 很相似，但是更简单，同时可靠性低于 TCP。

------

## 1.5. IP 是无连接的

IP 用于计算机之间的通信。

IP 是无连接的通信协议。它不会占用两个正在通信的计算机之间的通信线路。这样，IP 就降低了对网络线路的需求。每条线可以同时满足许多不同的计算机之间的通信需要。

通过 IP，消息（或者其他数据）被分割为小的独立的包，并通过因特网在计算机之间传送。

IP 负责将每个包路由至它的目的地。

------

## 1.6. IP 路由器

当一个 IP 包从一台计算机被发送，它会到达一个 IP 路由器。

IP 路由器负责将这个包路由至它的目的地，直接地或者通过其他的路由器。

在一个相同的通信中，一个包所经由的路径可能会和其他的包不同。而路由器负责根据通信量、网络中的错误或者其他参数来进行正确地寻址。

------

## 1.7. TCP/IP

TCP/IP 意味着 TCP 和 IP 在一起协同工作。

TCP 负责应用软件（比如您的浏览器）和网络软件之间的通信。

IP 负责计算机之间的通信。

TCP 负责将数据分割并装入 IP 包，然后在它们到达的时候重新组合它们。

IP 负责将包发送至接受者。

# 2. TCP/IP 寻址

------

TCP/IP 使用 32 个比特或者 4 组 0 到 255 之间的数字来为计算机编址。

------

## 2.1. IP地址

每个计算机必须有一个 IP 地址才能够连入因特网。

每个 IP 包必须有一个地址才能够发送到另一台计算机。

在本教程下一节，您会学习到更多关于 IP 地址和 IP 名称的知识。

------

## 2.2. IP 地址包含 4 组数字：

TCP/IP 使用 4 组数字来为计算机编址。每个计算机必须有一个唯一的 4 组数字的地址。

每组数字必须在 0 到 255 之间，并由点号隔开，比如：192.168.1.60。

------

## 2.3. 32 比特 = 4 字节

TCP/IP 使用 32 个比特来编址。一个计算机字节是 8 比特。所以 TCP/IP 使用了 4 个字节。

一个计算机字节可以包含 256 个不同的值：

00000000、00000001、00000010、00000011、00000100、00000101、00000110、00000111、00001000 ....... 直到 11111111。

现在，您应该知道了为什么 TCP/IP 地址是介于 0 到 255 之间的 4 组数字。

------

## 2.4. IP V6

IPv6 是 "Internet Protocol Version 6" 的缩写，也被称作下一代互联网协议，它是由 IETF 小组（Internet 工程任务组Internet Engineering Task Force）设计的用来替代现行的 IPv4（现行的）协议的一种新的 IP 协议。

我们知道，Internet 的主机都有一个唯一的 IP 地址，IP 地址用一个 32 位二进制的数表示一个主机号码，但 32 位地址资源有限，已经不能满足用户的需求了，因此 Internet 研究组织发布新的主机标识方法，即 IPv6。

在 RFC1884 中（RFC 是 Request for Comments document 的缩写。RFC 实际上就是 Internet 有关服务的一些标准），规定的标准语法建议把 IPv6 地址的 128 位（16 个字节）写成 8 个 16 位的无符号整数，每个整数用 4 个十六进制位表示，这些数之间用冒号（:）分开，例如：

```
686E：8C64：FFFF：FFFF：0：1180：96A：FFFF
```

冒号十六进制记法允许零压缩，即一串连续的0可以用一对冒号取代，例如：

```Text
FF05：0：0：0：0：0：0：B3可以定成：FF05：：B3
```

为了保证零压缩有一个清晰的解释，建议中规定，在任一地址中，只能使用一次零压缩。该技术对已建议的分配策略特别有用，因为会有许多地址包含连续的零串。

冒号十六进制记法结合有点十进制记法的后缀。这种结合在IPv4向IPv6换阶段特别有用。例如，下面的串是一个合法的冒号十六进制记法：

```text
0：0：0：0：0：0：128.10.1.1
```

这种记法中，虽然冒号所分隔的每一个值是一个16位的量，但每个分点十进制部分的值则指明一个字节的值。再使用零压缩即可得出：

```
：：128.10.1.1
```

------

## 2.5. 域名

12 个阿拉伯数字很难记忆。使用一个名称更容易。

用于 TCP/IP 地址的名字被称为域名。runoob.com 就是一个域名。

当你键入一个像 http://www.runoob.com 这样的域名，域名会被一种 DNS 程序翻译为数字。

在全世界，数量庞大的 DNS 服务器被连入因特网。DNS 服务器负责将域名翻译为 TCP/IP 地址，同时负责使用新的域名信息更新彼此的系统。

当一个新的域名连同其 TCP/IP 地址一起注册后，全世界的 DNS 服务器都会对此信息进行更新。

# 3. TCP/IP 协议

------

TCP/IP 是不同的通信协议的大集合。

------

## 3.1. 协议族

TCP/IP 是基于 TCP 和 IP 这两个最初的协议之上的不同的通信协议的大集合。

------

## 3.2. TCP - 传输控制协议

TCP 用于从应用程序到网络的数据传输控制。

TCP 负责在数据传送之前将它们分割为 IP 包，然后在它们到达的时候将它们重组。

------

## 3.3. IP - 网际协议（Internet Protocol）

IP 负责计算机之间的通信。

IP 负责在因特网上发送和接收数据包。

------

## 3.4. HTTP - 超文本传输协议(Hyper Text Transfer Protocol)

HTTP 负责 web 服务器与 web 浏览器之间的通信。

HTTP 用于从 web 客户端（浏览器）向 web 服务器发送请求，并从 web 服务器向 web 客户端返回内容（网页）。

------

## 3.5. HTTPS - 安全的 HTTP（HTTP Secure）

HTTPS 负责在 web 服务器和 web 浏览器之间的安全通信。

作为有代表性的应用，HTTPS 会用于处理信用卡交易和其他的敏感数据。

------

## 3.6. SSL - 安全套接字层（Secure Sockets Layer）

SSL 协议用于为安全数据传输加密数据。

------

## 3.7. SMTP - 简易邮件传输协议（Simple Mail Transfer Protocol）

SMTP 用于电子邮件的传输。

------

## 3.8. MIME - 多用途因特网邮件扩展（Multi-purpose Internet Mail Extensions）

MIME 协议使 SMTP 有能力通过 TCP/IP 网络传输多媒体文件，包括声音、视频和二进制数据。

------

## 3.9. IMAP - 因特网消息访问协议（Internet Message Access Protocol）

IMAP 用于存储和取回电子邮件。

------

## 3.10. POP - 邮局协议（Post Office Protocol）

POP 用于从电子邮件服务器向个人电脑下载电子邮件。

------

## 3.11. FTP - 文件传输协议（File Transfer Protocol）

FTP 负责计算机之间的文件传输。

------

## 3.12. NTP - 网络时间协议（Network Time Protocol）

NTP 用于在计算机之间同步时间（钟）。

------

## 3.13. DHCP - 动态主机配置协议（Dynamic Host Configuration Protocol）

DHCP 用于向网络中的计算机分配动态 IP 地址。

------

## 3.14. SNMP - 简单网络管理协议（Simple Network Management Protocol）

SNMP 用于计算机网络的管理。

------

## 3.15. LDAP - 轻量级的目录访问协议（Lightweight Directory Access Protocol）

LDAP 用于从因特网搜集关于用户和电子邮件地址的信息。

------

## 3.16. ICMP - 因特网消息控制协议（Internet Control Message Protocol）

ICMP 负责网络中的错误处理。

------

## 3.17. ARP - 地址解析协议（Address Resolution Protocol）

ARP - 用于通过 IP 来查找基于 IP 地址的计算机网卡的硬件地址。

------

## 3.18. RARP - 反向地址转换协议（Reverse Address Resolution Protocol）

RARP 用于通过 IP 查找基于硬件地址的计算机网卡的 IP 地址。

------

## 3.19. BOOTP - 自举协议（Boot Protocol）

BOOTP 用于从网络启动计算机。

------

## 3.20. PPTP - 点对点隧道协议（Point to Point Tunneling Protocol）

PPTP 用于私人网络之间的连接（隧道）。

# 4. TCP/IP 邮件

------

电子邮件是 TCP/IP 最重要的应用之一。

------

## 4.1. 您不会用到...

当您写邮件时，您不会用到 TCP/IP。

当您写邮件时，您用到的是电子邮件程序，例如莲花软件的 Notes，微软公司出品的 Outlook，或者 Netscape Communicator 等等。

------

## 4.2. 邮件程序会用到...

您的电子邮件程序使用不同的 TCP/IP 协议：

- 使用 SMTP 来发送邮件
- 使用 POP 从邮件服务器下载邮件
- 使用 IMAP 连接到邮件服务器

------

## 4.3. SMTP - 简单邮件传输协议

SMTP 协议用于传输电子邮件。SMTP 负责把邮件发送到另一台计算机。

通常情况下，邮件会被送到一台邮件服务器（SMTP 服务器），然后被送到另一台（或几台）服务器，然后最终被送到它的目的地。

SMTP 也可以传送纯文本，但是无法传输诸如图片、声音或者电影之类的二进制数据。

SMTP 使用 MIME 协议通过 TCP/IP 网络来发送二进制数据。MIME 协议会将二进制数据转换为纯文本。

------

## 4.4. POP - 邮局协议

POP 协议被邮件程序用来取回邮件服务器上面的邮件。

假如您的邮件程序使用 POP，那么一旦它连接上邮件服务器，您的所有的邮件都会被下载到邮件程序中（或者称之为邮件客户端）。

------

## 4.5. IMAP - 因特网消息访问协议

与 POP 类似，IMAP 协议同样被邮件程序使用。

IMAP 协议与 POP 协议之间的主要差异是：如果 IMAP 连上了邮件服务器，它不会自动地将邮件下载到邮件程序之中。

IMAP 使您有能力在下载邮件之前先通过邮件服务器端查看他们。通过 IMAP，您可以选择下载这些邮件或者仅仅是删除它们。比方说您需要从不同的位置访问邮件服务器，但是仅仅希望回到办公室的时候再下载邮件，IMAP 在这种情况下会很有用。

# 5. HTTP 与 HTTPS 的区别

## 5.1. 基本概念

**HTTP**（HyperText Transfer Protocol：超文本传输协议）是一种用于分布式、协作式和超媒体信息系统的应用层协议。 简单来说就是一种发布和接收 HTML 页面的方法，被用于在 Web 浏览器和网站服务器之间传递信息。

HTTP 默认工作在 TCP 协议 80 端口，用户访问网站 **http://** 打头的都是标准 HTTP 服务。

HTTP 协议以明文方式发送内容，不提供任何方式的数据加密，如果攻击者截取了Web浏览器和网站服务器之间的传输报文，就可以直接读懂其中的信息，因此，HTTP协议不适合传输一些敏感信息，比如：信用卡号、密码等支付信息。

**HTTPS**（Hypertext Transfer Protocol Secure：超文本传输安全协议）是一种透过计算机网络进行安全通信的传输协议。HTTPS 经由 HTTP 进行通信，但利用 SSL/TLS（SSL“安全套接层”协议，TLS“安全传输层”协议） 来加密数据包。HTTPS 开发的主要目的，是提供对网站服务器的身份认证，保护交换数据的隐私与完整性。

HTTPS 默认工作在 TCP 协议**443**端口，它的工作流程一般如以下方式：

- 1、TCP 三次同步握手
- 2、客户端验证服务器数字证书
- 3、DH 算法协商对称加密算法的密钥、hash 算法的密钥。Diffie-Hellman:一种确保共享KEY安全[穿越](https://baike.baidu.com/item/穿越/5205129)不安全网络的方法，称为Diffie-Hellman密钥交换协议/算法(Diffie-Hellman Key Exchange/Agreement Algorithm)。
- 4、SSL 安全加密隧道协商完成
- 5、网页以加密的方式传输，用协商的对称加密算法和密钥加密，保证数据机密性；用协商的hash算法进行数据完整性保护，保证数据不被篡改。

> 截至 2018 年 6 月，Alexa 排名前 100 万的网站中有 34.6% 使用 HTTPS 作为默认值，互联网 141387 个最受欢迎网站的 43.1% 具有安全实施的 HTTPS，以及 45% 的页面加载（透过Firefox纪录）使用HTTPS。2017 年3 月，中国注册域名总数的 0.11％使用 HTTPS。
>
> 根据 Mozilla 统计，自 2017 年 1 月以来，超过一半的网站流量被加密。

## 5.2. HTTP 与 HTTPS 区别

- HTTP 明文传输，数据都是未加密的，安全性较差，HTTPS（SSL+HTTP） 数据传输过程是加密的，安全性较好。
- 使用 HTTPS 协议需要到 CA（Certificate Authority，数字证书认证机构） 申请证书，一般免费证书较少，因而需要一定费用。证书颁发机构如：Symantec、Comodo、GoDaddy 和 GlobalSign 等。
- HTTP 页面响应速度比 HTTPS 快，主要是因为 HTTP 使用 TCP 三次握手建立连接，客户端和服务器需要交换 3 个包，而 HTTPS除了 TCP 的三个包，还要加上 ssl 握手需要的 9 个包，所以一共是 12 个包。
- http 和 https 使用的是完全不同的连接方式，用的端口也不一样，前者是 80，后者是 443。
- HTTPS 其实就是建构在 SSL/TLS 之上的 HTTP 协议，所以，要比较 HTTPS 比 HTTP 要更耗费服务器资源。

## 5.3. TCP 三次握手

在TCP/IP协议中，TCP协议通过三次握手建立一个可靠的连接

- 第一次握手：客户端尝试连接服务器，向服务器发送 syn 包（同步序列编号Synchronize Sequence Numbers），syn=j，客户端进入 SYN_SEND 状态等待服务器确认
- 第二次握手：服务器接收客户端syn包并确认（ack=j+1），同时向客户端发送一个 SYN包（syn=k），即 SYN+ACK 包，此时服务器进入 SYN_RECV 状态
- 第三次握手：第三次握手：客户端收到服务器的SYN+ACK包，向服务器发送确认包ACK(ack=k+1），此包发送完毕，客户端和服务器进入ESTABLISHED状态，完成三次握手

## 5.4. HTTPS 的工作原理

我们都知道 HTTPS 能够加密信息，以免敏感信息被第三方获取，所以很多银行网站或电子邮箱等等安全级别较高的服务都会采用 HTTPS 协议。

**1、客户端发起 HTTPS 请求**

这个没什么好说的，就是用户在浏览器里输入一个 https 网址，然后连接到 server 的 443 端口。

**2、服务端的配置**

采用 HTTPS 协议的服务器必须要有一套数字证书，可以自己制作，也可以向组织申请，区别就是自己颁发的证书需要客户端验证通过，才可以继续访问，而使用受信任的公司申请的证书则不会弹出提示页面(startssl 就是个不错的选择，有 1 年的免费服务)。

这套证书其实就是一对公钥和私钥，如果对公钥和私钥不太理解，可以想象成一把钥匙和一个锁头，只是全世界只有你一个人有这把钥匙，你可以把锁头给别人，别人可以用这个锁把重要的东西锁起来，然后发给你，因为只有你一个人有这把钥匙，所以只有你才能看到被这把锁锁起来的东西。

**3、传送证书**

这个证书其实就是公钥，只是包含了很多信息，如证书的颁发机构，过期时间等等。

**4、客户端解析证书**

这部分工作是有客户端的TLS来完成的，首先会验证公钥是否有效，比如颁发机构，过期时间等等，如果发现异常，则会弹出一个警告框，提示证书存在问题。

如果证书没有问题，那么就生成一个随机值，然后用证书对该随机值进行加密，就好像上面说的，把随机值用锁头锁起来，这样除非有钥匙，不然看不到被锁住的内容。

**5、传送加密信息**

这部分传送的是用证书加密后的随机值，目的就是让服务端得到这个随机值，以后客户端和服务端的通信就可以通过这个随机值来进行加密解密了。

**6、服务端解密信息**

服务端用私钥解密后，得到了客户端传过来的随机值(私钥)，然后把内容通过该值进行对称加密，所谓对称加密就是，将信息和私钥通过某种算法混合在一起，这样除非知道私钥，不然无法获取内容，而正好客户端和服务端都知道这个私钥，所以只要加密算法够彪悍，私钥够复杂，数据就够安全。

**7、传输加密后的信息**

这部分信息是服务段用私钥加密后的信息，可以在客户端被还原。

**8、客户端解密信息**

客户端用之前生成的私钥解密服务段传过来的信息，于是获取了解密后的内容，整个过程第三方即使监听到了数据，也束手无策。



