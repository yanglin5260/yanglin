<h2>目录</h2>

<details open>
  <summary><a href="#1-www-指南-万维网联盟world-wide-web">1. WWW 指南-万维网联盟(World Wide Web)</a></summary>
  <ul>
    <a href="#11-什么是www？">1.1. 什么是WWW？</a><br>
    <a href="#12-万维网如何工作？">1.2. 万维网如何工作？</a><br>
    <a href="#13-浏览器如何获取网页？">1.3. 浏览器如何获取网页？</a><br>
    <a href="#14-浏览器如何显示页面？">1.4. 浏览器如何显示页面？</a><br>
    <a href="#15-是谁在做web标准？">1.5. 是谁在做Web标准？</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#2-html-指南">2. HTML 指南</a></summary>
  <ul>
    <a href="#21-html---超文本标记语言-hyper-text-markup-language">2.1. HTML - 超文本标记语言 (Hyper Text Markup Language)</a><br>
    <a href="#22-html-实例">2.2. HTML 实例</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#3-css-指南">3. CSS 指南</a></summary>
  <ul>
    <a href="#31-css---层叠样式表cascading-style-sheets">3.1. CSS - 层叠样式表(Cascading Style Sheets)</a><br>
    <a href="#32-css-实例">3.2. CSS 实例</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#4-javascript-指南">4. JavaScript 指南</a></summary>
  <ul>
    <a href="#41-javascript---客户端脚本">4.1. JavaScript - 客户端脚本</a><br>
    <a href="#42-什么是-javascript">4.2. 什么是 JavaScript?</a><br>
    <a href="#43-客户端脚本">4.3. 客户端脚本</a><br>
    <a href="#44-javascript可以做什么？">4.4. JavaScript可以做什么？</a><br>
    <a href="#45-什么是html-dom？">4.5. 什么是HTML DOM？</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#5-xml-指南">5. XML 指南</a></summary>
  <ul>
    <a href="#51-xml---可扩展标记语言extensible-markup-language">5.1. XML - 可扩展标记语言(EXtensible Markup Language)</a><br>
    <a href="#52-什么是xml？">5.2. 什么是XML？</a><br>
    <a href="#53-xml不会做任何事情">5.3. XML不会做任何事情</a><br>
    <a href="#54-xml标签不是预定义">5.4. XML标签不是预定义</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#6-服务端脚本-指南">6. 服务端脚本 指南</a></summary>
  <ul>
    <a href="#61-asp-和-php---服务端脚本">6.1. ASP 和 PHP - 服务端脚本</a><br>
    <a href="#62-服务器脚本能做什么呢？">6.2. 服务器脚本能做什么呢？</a><br>
    <a href="#63-asp-和-php">6.3. ASP 和 PHP</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#7-sql-指南">7. SQL 指南</a></summary>
  <ul>
    <a href="#71-sql---结构化查询语言-structured-query-language">7.1. SQL - 结构化查询语言 (Structured Query Language)</a><br>
    <a href="#72-什么是-sql">7.2. 什么是 SQL?</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#8-web-创建设计">8. Web 创建设计</a></summary>
  <ul>
    <a href="#81-用户是浏览者">8.1. 用户是浏览者</a><br>
    <a href="#82-少即是多">8.2. 少即是多</a><br>
    <a href="#83-导航">8.3. 导航</a><br>
    <a href="#84-加载速度">8.4. 加载速度</a><br>
    <a href="#85-用户反馈">8.5. 用户反馈</a><br>
    <a href="#86-访问者的显示器">8.6. 访问者的显示器</a><br>
    <a href="#87-他们使用什么浏览器？">8.7. 他们使用什么浏览器？</a><br>
    <a href="#88-客户端使用的插件">8.8. 客户端使用的插件</a><br>
    <a href="#89-关于残疾人呢？">8.9. 关于残疾人呢？</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#9-web-标准">9. Web 标准</a></summary>
  <ul>
    <a href="#91-为什么要web标准？">9.1. 为什么要Web标准？</a><br>
    <a href="#92-无障碍">9.2. 无障碍</a><br>
    <a href="#93-w3c---万维网联盟">9.3. W3C - 万维网联盟</a><br>
    <a href="#94-ecma---欧洲计算机制造商协会（european-computer-manufacturers-association）">9.4. ECMA - 欧洲计算机制造商协会（European Computer Manufacturers Association）</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#10-web-网页-验证">10. Web 网页 验证</a></summary>
  <ul>
    <a href="#101-web-网页验证器">10.1. Web 网页验证器</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#11-web-语义化">11. Web 语义化</a></summary>
  <ul>
    <a href="#111-什么是-web-语义化？">11.1. 什么是 Web 语义化？</a><br>
    <a href="#112-资源描述框架">11.2. 资源描述框架</a><br>
    <a href="#113-实现">11.3. 实现</a><br>
    <a href="#114-前景">11.4. 前景</a><br>
    <a href="#115-语义网体系结构">11.5. 语义网体系结构</a><br>
  </ul>
</details>
  <a href="#12-web-词汇表">12. Web 词汇表</a><br>
<details open>
  <summary><a href="#13-seo---搜索引擎优化">13. SEO - 搜索引擎优化</a></summary>
  <ul>
    <a href="#131-seo---搜索引擎优化（search-engine-optimization）">13.1. SEO - 搜索引擎优化（Search Engine Optimization）</a><br>
  </ul>
</details>


<h1>Website Construction Guide</h1>

# 1. WWW 指南-万维网联盟(World Wide Web)

WWW - 万维网联盟WWW通常称为网络。web是一个世界各地的计算机网络。电脑在Web上使用标准语言沟通。万维网联盟（W3C）制定了Web标准

## 1.1. 什么是WWW？

- WWW 代表 **World Wide Web(万维网)**
- 万维网常常被称为 **网络**
- 网络是世界各地的计算机网络
- 网络中的所有电脑可以相互沟通
- 所有的计算机使用HTTP的通信标准

------

## 1.2. 万维网如何工作？

- 信息存储的文件称为网页
- Web页面存储在Web服务器上。
- 阅读网页的计算机被称为Web客户端
- Web客户端查看网页的程序称为Web浏览器
- 流行的浏览器有Internet Explorer，Chrome和火狐等。

------

## 1.3. 浏览器如何获取网页？

- 一个浏览器通过请求从服务器上获取的网页数据
- 标准的HTTP请求包含一个网页地址
- 网页地址实例: **http://www.w3cschool.cc**

------

## 1.4. 浏览器如何显示页面？

- 所有的网页包含如何显示的说明
- 浏览器通过过阅读这些说明显示页面。
- 最常见的显示指令被称为HTML标签
- HTML中段落的标签为： **<p>**
- 在HTML中，段落是这样定义的: **<p>这是段落。**</p>

------

## 1.5. 是谁在做Web标准？

- Web标准并不是谷歌或微软在做
- 制定Web规则的机构是W3C
- W3C表示万维网联盟
- W3C 制定的web标准规范。
- 最重要的Web标准是HTML，CSS和XML

# 2. HTML 指南

------

## 2.1. HTML - 超文本标记语言 (Hyper Text Markup Language)

HTML是建设网站/网页制作主要语言。

HTML是一种易于学习的标记语言。

HTML使用像 <p> 尖括号内标记标签来定义网页的内容：

## 2.2. HTML 实例

```html
<html>
<body>

<h1>My First Heading</h1>

<p>My first paragraph.</p>

</body>
</html>
```

HTML使用开始标记和结束标记来标记一个网页元素，：在上面的例子，<p>标签标志着一个段落开始，</p>标志着该段末尾。

通过使用简单的HTML标签，网页设计师可以为一个网页（HTML文档）添加标题，段落，文字，表格，图片，列表，编程代码等。

Web浏览器（IE浏览器，火狐，Chrome等）读取HTML文档，解释HTML标记，并显示正确用户可读的内容。

# 3. CSS 指南

------

## 3.1. CSS - 层叠样式表(Cascading Style Sheets)

CSS定义如何显示HTML元素。

CSS 描述了HTML元素的可视化样式(外观，布局，颜色，字体)。

CSS是单独设计的文件（从而大大提高HTML的灵活性和减少HTML的复杂性）。

CSS简单易学。你可以把一个HTML元素当作选择器，并在大括号内的列出样式属性

## 3.2. CSS 实例

```css
body
{
background-color:#d0e4fe;
}
h1
{
color:orange;
text-align:center;
}
p
{
font-family:"Times New Roman";
font-size:20px;
}
```

样式表极大地提高了工作效率

样式表定义如何显示 HTML 元素，诸如 HTML 3.2 的样式中的字体标签和颜色属性通常被保存在外部的 .css 文件中。

通过仅仅编辑一个简单的 CSS 文档，外部样式表使你有能力同时改变站点中所有页面布局的外观。

由于允许同时控制多重页面的样式和布局，CSS 可以称得上 WEB 设计领域的一个突破。作为网站开发者，你可以为每个 HTML 元素定义样式，并将之应用于你希望的任意多的页面中。如需进行全局变换，只需简单地改变样式，然后网站中的所有元素均会被自动地更新。

# 4. JavaScript 指南

------

## 4.1. JavaScript - 客户端脚本

JavaScript 是属于网络的脚本语言!

JavaScript 被数百万计的网页用来改进设计、验证表单、检测浏览器、创建cookies,以及更多的应用。

JavaScript 学习简单

## 4.2. 什么是 JavaScript?

- JavaScript 被设计用来向 HTML 页面添加交互行为。
- JavaScript 是一种脚本语言（脚本语言是一种轻量级的编程语言）。
- JavaScript 由数行可执行计算机代码组成。
- JavaScript 通常被直接嵌入 HTML 页面。
- JavaScript 是一种解释性语言（就是说，代码执行不进行预编译）。
- 所有的人无需购买许可证均可使用 JavaScript。

------

## 4.3. 客户端脚本

JavaScript "制定" 浏览器行为。这就是所谓的客户端脚本（或浏览器的脚本）。

服务器端脚本是"制定"服务器的行为（见本站的ASP/PHP教程）。

------

## 4.4. JavaScript可以做什么？

- **JavaScript 为 HTML 设计师提供了一种编程工具**
  HTML 创作者往往都不是程序员，但是 JavaScript 却是一种只拥有极其简单的语法的脚本语言！几乎每个人都有能力将短小的代码片断放入他们的 HTML 页面当中。
- **JavaScript 可以将动态的文本放入 HTML 页面**
  类似于这样的一段 JavaScript 声明可以将一段可变的文本放入 HTML 页面：document.write("<h1>" + name + "</h1>")
- **JavaScript 可以对事件作出响应**
  可以将 JavaScript 设置为当某事件发生时才会被执行，例如页面载入完成或者当用户点击某个 HTML 元素时。
- **JavaScript 可以读写 HTML 元素**
  JavaScript 可以读取及改变 HTML 元素的内容。
- **JavaScript 可被用来验证数据**
  在数据被提交到服务器之前，JavaScript 可被用来验证这些数据。
- **JavaScript 可被用来检测访问者的浏览器**
  JavaScript 可被用来检测访问者的浏览器，并根据所检测到的浏览器，为这个浏览器载入相应的页面。
- **JavaScript 可被用来创建 cookies**
  JavaScript 可被用来存储和取回位于访问者的计算机中的信息。

------

## 4.5. 什么是HTML DOM？

HTML DOM 定义了访问和操作 HTML 文档的标准方法。

DOM 将 HTML 文档表达为树结构。

# 5. XML 指南

------

## 5.1. XML - 可扩展标记语言(EXtensible Markup Language)

XML 是跨平台的、用于传输信息且独立于软件和硬件的工具。

```xml
<?xml version="1.0"?>
<note>
    <to>Tove</to>
    <from>Jani</from>
    <heading>Reminder</heading>
    <body>Don't forget me this weekend!</body>
</note>
```

------

## 5.2. 什么是XML？

- XML 指*可扩展标记语言*（EXtensible Markup Language）
- XML 是一种*标记语言*，很类似 HTML
- XML 被设计用来*描述数据*
- XML 标签没有被预定义。您需要*自行定义标签*。
- XML 使用*文件类型声明*（DTD）或者 *XML Schema* 来描述数据。
- 带有 DTD 或者 XML Schema 的 XML 被设计为具有*自我描述性*。
- XML 是一个 W3C 标准

------

## 5.3. XML不会做任何事情

ML是不做任何事情。 XML创建结构，存储和携带信息。

上面的XML文档的例子是XML编写的从Jani到Tove的一张纸条。注意标题和邮件正文。它还具有来自哪里的信息。但是，这个XML文档并没有做任何事情。只是纯粹的信息包裹在XML标记中。

------

## 5.4. XML标签不是预定义

XML标签不是预定义，您必须"发明"自己的标签。

用来标记HTML文档的标签是预定义的的HTML文件作者只能使用在HTML标准（如<P>，<H1>等）定义的标签。

XML允许作者来定义他/她自己的标签和他/她自己的文档结构。

在上面的例子（像<to>和<from>）标签没有在任何XML标准定义。这些标签是XML文档作者"发明"的。

# 6. 服务端脚本 指南

------

## 6.1. ASP 和 PHP - 服务端脚本

HTML 文件可以包含文本、HTML 标签以及脚本。

服务器端脚本是对服务器行为的编程。这被称为服务器端脚本或服务器脚本。

客户端脚本是对浏览器行为的编程。

通常，当浏览器请求某个 HTML 文件时，服务器会返回此文件，但是假如此文件含有服务器端的脚本，那么在此 HTML 文件作为纯 HTML 被返回浏览器之前，首先会执行 HTML 文件中的脚本。

------

## 6.2. 服务器脚本能做什么呢？

- 动态地向 web 页面编辑、改变或添加任何的内容
- 对由 HTML 表单提交的用户请求或数据进行响应
- 访问数据或数据库，并向浏览器返回结果
- 为不同的用户定制页面
- 提高网页安全性，使您的网页代码不会通过浏览器被查看到

**重要提醒：** 由于脚本在服务器上执行，因此浏览器在不支持脚本的情况下就可以显示服务器端的文件！

------

## 6.3. ASP 和 PHP

我们通过使用活动服务器页面（ASP）和超文本预处理器（PHP）来演示服务器端的脚本编程。

# 7. SQL 指南

------

## 7.1. SQL - 结构化查询语言 (Structured Query Language)

SQL 是用于访问和处理数据库的标准的计算机语言。

常用的数据库管理系统： MySQL, SQL Server, Access, Oracle, Sybase, 和 DB2

对于那些希望在数据库中存储数据并从中获取数据的人来说，SQL 的知识是价值无法衡量的。

------

## 7.2. 什么是 SQL?

- SQL 指结构化查询语言 (*S*tructured *Q*uery *L*anguage)
- SQL 使我们有能力访问数据库
- SQL 是一种 ANSI（AMERICAN NATIONAL STANDARDS INSTITUTE，美国国家标准学会）的标准计算机语言
- SQL 面向数据库执行查询
- SQL 可从数据库取回数据
- SQL 可在数据库中插入新的记录
- SQL 可从数据库删除记录
- SQL 很容易学习

# 8. Web 创建设计

------

设计一个网站，需要认真思考和规划。

最重要的是要知道你的访问用户。

------

## 8.1. 用户是浏览者

**一个典型的访问者将无法读取您的网页的全部内容！**

无论您在网页中发布了多么有用的信息，一个访问者在决定是否继续阅读之前仅仅会花几秒钟的时间进行浏览。

请确保使你的观点，在页面的第一句！另外，您还需要在整个页面中使用简短的段落以及有趣的标题。

## 8.2. 少即是多

保持段落尽可能短。

保持章节尽可能短。

冗长文字的页面不利于用户体验。

如果你的网页内容很多，您将页面信息分解成小的模块，并放置在不同的页面！

------

## 8.3. 导航

在您网站的所有页面使用一致的导航结构。

不要在文本段落内使用超链，超链接会把访问者带到别的页面，这样做会破坏导航结构一致性。

如果您必须使用超链接，你可以将链接添加到一个段落的底部或菜单中。

------

## 8.4. 加载速度

有时开发人员不知道一些网页需要很长的时间来加载。

据统计，大多数用户会留在加载时间不超过7秒的网页。

测试您的网页在一个低速的调制解调器中打开。如果您的网页需要很长时间加载，可以考虑删除图片或多媒体等内容。

------

## 8.5. 用户反馈

反馈是一件非常好的事情！

你的访问者是你的"客户"。通常他们会给你的网站提供很好的改善建议。

如果您提供良好的反馈途径，您将得到来自很多来自不同领域人的反馈意见。

------

## 8.6. 访问者的显示器

在互联网上不是每个人的显示器尺寸都是一样的。

如果你设计一个网站，是用高分辨率的显示器上显示，当分辨率低的显示器（如800 × 600）访问你的网页时就可能会出现问题。

请在不同的显示器上测试您的网站。

------

## 8.7. 他们使用什么浏览器？

请在不同的浏览器测试你的网站。

目前最流行的浏览器有：Internet Explorer，Firefox和Google Chrome。

设计网页时，一个明智的做法是使用正确的HTML。正确的编码将帮助浏览器正确显示您的网页。

------

## 8.8. 客户端使用的插件

声音，视频剪辑，或其他多媒体内容可能需要使用单独的程序（插件）来播放。

请确保您的访问者能在你的网页上正常使用他们所需要的软件。

------

## 8.9. 关于残疾人呢？

有些人的视力或听力有障碍。

他们可能会尝试使用盲文或语音浏览器浏览您的网页。所以你应该在你的网页添加图像和图形元素的替代文本。

# 9. Web 标准

------

Web标准，使得Web开发更加容易。

Web标准由万维网联盟（W3C）制定。

------

## 9.1. 为什么要Web标准？

对于浏览器开发商和 Web 程序开发人员在开发新的应用程序时遵守指定的标准更有利于 Web 更好地发展。

开发人员按照 Web 标准制作网页，这样对于开发者来说就更加简单了，因为他们可以很容易了解彼此的编码。

使用Web标准，将确保所有浏览器正确显示您的网站而无需费时重写。

遵守标准的Web页面可以使得搜索引擎更容易访问并收入网页，也可以更容易转换为其他格式，并更易于访问程序代码（如JavaScript和DOM）。

**提示：** 你可以使用网页验证服务器验证页面的标准性。

------

## 9.2. 无障碍

无障碍环境是一个HTML标准的重要组成部分。

Web标准，使其更易于为残疾人士使用Web。

Web标准使得残疾人士也可以很容易地使用互联网。盲人可使用程序为他们读出网页。而弱视的人群可通过重新排列并放大网页来访问网站。

------

## 9.3. W3C - 万维网联盟

W3C 创建和维护Web标准。

蒂姆·伯纳斯·李（Tim Berners-Lee）是万维网联盟创始人发明者被称为互联网之父：

*"The dream behind the Web is of a common information space in which we communicate by sharing information."*

万维网联盟，建立于 1994 年，是一个国际性的联盟，其宗旨是投身于"引领 web 以激发其全部潜能"。

- W3C表示**万维网联盟**
- W3C创建于**1994年10月**
- W3C被**Web发明者蒂姆·伯纳斯·李（Tim Berners-Lee）**创建
- W3C是作为**成员国机构**组织
- W3C的工作是进行**标准化网络**
- W3C创建和维护的**WWW标准**
- W3C标准有**W3C建议**

最重要W3C标准有：

- [HTML](https://www.runoob.com/html/default.html)
- [CSS](https://www.runoob.com/html/html-tutorial.html)
- [XML](https://www.runoob.com/xml/xml-tutorial.html)
- [XSL](https://www.runoob.com/xsl/xsl-tutorial.html)
- [DOM](https://www.runoob.com/dom/dom-tutorial.html)

------

## 9.4. ECMA - 欧洲计算机制造商协会（European Computer Manufacturers Association）

ECMA于1960年在布鲁塞尔由一些欧洲最大的计算机和技术公司成立。到1961年5月，他们成立了一个正式的组织，这个组织的目标是评估，开发和认可电信和计算机标准。

大家决定把ECMA的总部设在日内瓦是因为这样能够让它与其它与之协同工作的标准制定组织更接近一些，比方说国际标准化组织（ISO）和国际电子技术协会（IEC）。

ECMA是"European Computer Manufactures Association"的缩写，中文称欧洲计算机制造联合会。是1961年成立的旨在建立统一的电脑操作格式标准--包括程序语言和输入输出的组织。

最新ECMAScript规范就是ECMA- 262：

http://www.ecma-international.org/publications/standards/Ecma-262.htm

# 10. Web 网页 验证

------

## 10.1. Web 网页验证器

验证器是一种软件程序，可以检查对Web网页是否标准。

当使用验证器来检查HTML,XHTML或CSS文件时，验证器会根据您所选择的标准返回一个错误列表。

在发布网页之前请确保你验证过所有的网页都符合W3C标准。

# 11. Web 语义化

单词语义化表示了它的意义。

事物的语义化意味着事物。

Web 语义化 = Web的意义。

------

## 11.1. 什么是 Web 语义化？

什么是语义化？其实简单说来就是让机器可以读懂内容。

- 甲壳虫乐队是一个来自利物浦受欢迎的乐队。
- 约翰列侬是披头士乐队的成员。
- "Hey Jude"是由披头士的代表作。

我们可以很容易理解上面的句子的意义。但这些语句怎么 被计算机理解呢？

语句由语法规则创建。语言的语法定义了创建语言语句的规则。但是如何让语法变为语义呢？

语义网是让机器可以理解数据。语义网技术，它包括一套描述语言和推理逻辑。它通过一些格式对本体（Ontology）进行描述。

语义网并不是网页之间的链接。

语义网描述了事物之间的关联(（如 A 是 B的一部分，Y 是 Z 的成员)及事物的属性（如大小，高度，年龄，价格等）。

语义网的实现是基于XML（可扩展标记语言eXtensible Markup Langauge）语言和资源描述框架（RDF）来完成的。XML是一种用于定义标记语言的工具，其内容包括XML声明、用以定义语言语法的DTD (document type declaration文档类型定义）、描述标记的详细说明以及文档本身。而文档本身又包含有标记和内容。RDF则用以表达网页的内容。

------

## 11.2. 资源描述框架

RDF（Resource Description Framework），即资源描述框架，是W3C推荐的用来描述WWW上的信息资源及其之间关系的语言规范。

RDF（S）是语义网的重要组成部分，它使用URI来标识不同的对象（包括资源节点、属性类或属性值）并可将不同的URI连接起来，清楚表达对象间的关系。

------

## 11.3. 实现

语义网虽然是一种更加美好的网络，但实现起来却是一项复杂而浩大的工程。 目前语义网的体系结构正在建设中，主要需要以下两方面的支持：

**（1） 数据网络的实现**

即：通过一套统一的完善的数据标准对网络信息进行更彻底更详细的标记，使得语义网能够精准的识别信息，区分信息的作用和含义 要使语义网搜索更精确彻底，更容易判断信息的真假，从而达到实用的目标，首先需要制订标准，该标准允许用户给网络内容添加元数据（即解释详尽的标记），并能让用户精确地指出他们正在寻找什么；然后，还需要找到一种方法，以确保不同的程序都能分享不同网站的内容；最后，要求用户可以增加其他功能，如添加应用软件等。

语义网的实现是基于XML（可扩展标记语言eXtensible Markup Langauge）语言和资源描述框架（RDF）来完成的。XML是一种用于定义标记语言的工具，其内容包括XML声明、用以定义语言语法的DTD (document type declaration文档类型定义）、描述标记的详细说明以及文档本身。而文档本身又包含有标记和内容。RDF则用以表达网页的内容。

**（2）具有语义分析能力的搜索引擎**

如果说数据网络能够短时间通过亿万的个体实现，那么网络的语义化智能化就要通过人类尖端智慧群体的努力实现。研发一种具有语义分析能力的信息搜索引擎将成为语义网的最重要一步，这种引擎能够理解人类的自然语言，并且具有一定的推理和判断能力。

语义搜索引擎（semantic search engine）和具有语义分析能力的搜索引擎(semantically enabled search engine)是两码事。前者不过是语义网络的利用，一种信息搜索方式，而具有语义分析能力的搜索引擎是一种能够理解自然语言，通过计算机的推理而进一步提供更符合用户心理的答案。

------

## 11.4. 前景

语义网的体系结构正在建设中，当前国际范围内对此体系结构的研究还没有形成一个令人满意的严密的逻辑描述与理论体系，中国学者对该体系结构也只是在国外研究的基础上做简要的介绍，还没有形成系统的阐述。

语义网的实现需要三大关键技术的支持：XML、RDF和Ontology。

XML(eXtensible Marked Language，即可扩展标记语言）可以让信息提供者根据需要，自行定义标记及属性名，从而使XML文件的结构可以复杂到任意程度。

它具有良好的数据存储格式和可扩展性、高度结构化以及便于网络传输等优点，再加上其特有的NS机制及XML Schema所支持的多种数据类型与校验机制，使其成为语义网的关键技术之一。

目前关于语义网关键技术的讨论主要集中在RDF和Ontology身上。

RDF是W3C组织推荐使用的用来描述资源及其之间关系的语言规范，具有简单、易扩展、开放性、易交换和易综合等特点。

值得注意的是，RDF 只定义了资源的描述方式，却没有定义用哪些数据描述资源。RDF由三个部分组成：RDF Data Model、RDF Schema和RDF Syntax。

**附上：**

1.语义网通过扩展现有的互联网，在信息中加入表示其含义的内容，使计算机可以自动与人协同工作。也就是说，语义网中的各种资源不再只是各种相连的信息，还包括其信息的真正含义，从而提高计算机处理信息的自动化和智能化。当然，计算机并不具有真正的智能，语义网的建立需要研究者们对信息进行有效的表示，制定统一的标准，使计算机可以对信息进行有效的自动处理。

（来源：何斌 张立厚《信息管理原理与方法》 清华大学出版社 2007年7月第二版）

## 11.5. 语义网体系结构

- 第一层：Unicode与URI，是整个体系结构的基础。

- 第二层：XML+NS+XMLSchema,负责语法上表示数据的内容和结构，通过使用标准的格式语言将网络信息的表现形式、数据结构和内容分离。

- 第三层：RDF+RDF Schema,它提供语义模型用于描述网上的信息和类型。其中，RDF（Resource Description Framework），即资源描述框架，是W3C推荐的用来描述WWW上的信息资源及其之间关系的语言规范。RDF（S）是语义网的重要组成部分，它使用URI来标识不同的对象（包括资源节点、属性类或属性值）并可将不同的URI连接起来，清楚表达对象间的关系。

- 第四层：本体词汇层，本体是关于领域知识的概念化、形式化的明确规范。在语义网体系结构中，本体的作用主要表现在：

  （1）.概念描述，即通过概念描述揭示领域知识；

  （2）.语义揭示，本体具有比RDF更强的表达能力，可以揭示更为丰富的语义关系；

  （3）.一致性，本体作为领域知识的明确规范，可以保证语义的一致性，从而彻底解决一词多义、多词一义和词义含糊现象；

  （4）. 推理支持，本体在概念描述上的确定性及其强大的语义揭示能力在数据层面有力地保证了推理的有效性。

- 第五层：逻辑层，负责提供公理和推理原则，为智能服务提供基础。其中，描述逻辑(DescriptionLogic)是基于对象的知识表示的形式化，它吸取了KL-ONE的主要思想，是一阶谓词逻辑的一个可判定子集。它与一阶谓词逻辑不同的是，描述逻辑系统能提供可判定的推理服务。除了知识表示以外，描述逻辑还用在其它许多领域，它被认为是以对象为中心的表示语言的最为重要的归一形式。描述逻辑的重要特征是很强的表达能力和可判定性，它能保证推理算法总能停止，并返回正确的结果。在众多知识表示的形式化方法中，描述逻辑在十多年来受到人们的特别关注，主要原因在于：它们有清晰的模型-理论机制；很适合于通过概念分类学来表示应用领域；并提供了很用的推理服务。

- 第六层证明层和第七层信任层负责提供认证和信任机制。

# 12. Web 词汇表

------

以下词汇按字母顺序排列

**Access (Microsoft Access)**

一个微软开发的数据库系统。 Microsoft Office专业版一部分。主要用于在Windows平台上运行低流量的Web网站。

**ActiveMovie**

微软公司推出的用于多媒体程序设计的控件

**ActiveX**

允许Web浏览器下载并执行Windows程序编程接口（API）。

**Address**

请参阅网址。

**AdSense**

由Google提供的网络广告系统。

**AJAX (Asynchronous JavaScript and XML)**

使用JavaScript和XML来创建交互式Web应用程序的"艺术"。使用Ajax，Web应用程序可以在后台Web服务器（异步）交换数据和更新一个网页部分无需重新加载页面。

**Anchor**

在网络方面：起点或结束的超链接。

**Adobe Air**

一个Adobe集成运行时（AIR）系统，使开发人员可以使用Web技术（HTML，JAVASCRIPT，FLASH）来创建桌面应用程序。

**Android**

一款手机操作系统Android公司开发，后来被谷歌收购。

**匿名 FTP**

请参阅FTP服务器。

**ANSI (American National Standards Institute)**

为计算机行业创建标准的组织。 ANSIC标准负责。

**ANSI C**

C编程语言国际标准。

**ADO (ActiveX Data Object)**

微软的任何类型的数据存储技术，提供数据访问。

**ADSL (Asymmetric Digital Subscriber Line)**

一个特殊类型的DSL线路，上传速度和下载速度是不同的。

**Agent**

请参阅搜索代理/搜索引擎

**Amaya**

来自W3C的一个开放源码的Web浏览器编辑器，用于推动在浏览器设计领先的想法。

**Animation**

一组照片播放时模拟运动串联。

**反病毒程序**

一种计算机程序，发现并摧毁所有类型的计算机病毒。

**Apache**

一个开源的Web服务器。大多用于Unix，Linux和Solaris平台。

**Applet**

请参阅web小程序。

**Archie**

计算机程序来定位公共FTP服务器上文件。

**API (Application Programming Interface)**

让程序与另一个程序通信接口。在web方面：让网页浏览器或Web服务器与其他程序通信的接口。

**ARPAnet**

网络测试实验，在20世纪70年代互联网发展时开始。

**Authentication**

在web方面：用什么方法在网络上或计算机程序上来验证用户的身份。

**ASCII (American Standard 兼容 Information Interchange)**

128位字母数字和特殊控制字符用于计算机存储和打印的文字。 HTML中通过web传输的数据。

**ASF (Advanced Streaming Format)**

一个多媒体数据流的格式。微软的Windows Media开发。

**ASP (Active Server Pages)**

微软的技术，使服务器可执行的脚本在网页中插入。

**ASX (ASF Streaming Redirector)**

关于ASF文件存储信息的XML格式。微软的Windows Media开发。

**AVI (Audio Video Interleave)**

文件格式的视频文件。微软开发的视频压缩技术。

**Banner Ad**

（最常见的图形）广告放置在网页上作为广告客户的网站超链接行为。

**Bandwidth**

您可以通过发送Internet连接速度（数据量）测量。更多的带宽更快的连接

**Baud**

通道上每秒钟发送符号数。

**BBS (Bulletin Board System)**

基于网络的公共分享讨论，文件和公告系统。

**Binary Data**

于机器可读的形式数据。

**Bit (Binary Digit)**

于计算机中存储的数据的最小单位。一个位值为0或1。一台计算机使用8位来存储一个文本字符。

**Blog (Web Log)**

一个网站类型（通常是由个人维护）日志评论（大多数通常是个人）的意见，含义说明事件等

**Blogger**

一个人维持或写入内容到网络日志（博客）。

**Blogging**

编写或内容添加到网络日志（博客）。

**BMP (Bitmap)**

用于存储图像的格式。

**Bookmark**

在web方面：一个特定的网站链接存储（书签）以备将来使用Web用户方便地访问。

**Bounce Rate**

网站访问者查看只有一个网页在他们离开（弹起）的比例。

**Browse**

术语来描述整个网络用户的运动通过超链接从一页一页地移动使用网页浏览器。 （请参阅Web浏览器）。

**BPS (Bits Per Second)**

用术语来描述在网上进行数据传输速度。

**Browser**

请参阅Web浏览器。

**Byte (Binary Term)**

计算机存储单元包含8位。每个字节可以存储一个文本字符。

**C**

一种先进的编程语言用于先进的计算机应用编程。

**C++ (C Plus Plus)**

同样的c补充面向对象的功能。

**C# (C Sharp)**

微软的版本C ++的补充类似Java的功能。

**Case Sensitive**

用来描述使用大写或小写字母敏感

**Cache**

在web方面：一个Web浏览器或Web服务器计算机的硬盘上存储的网页副本功能。

**Chat**

on-line基于文本的互联网用户之间通信。

**CGI (Common Gateway Interface)**

描述一个CGI程序与Web服务器如何通信的规则。

**CGI Bin**

在Web服务器上存储CGI程序文件夹（或目录）。

**CGI Program**

一个小程序，从Web服务器处理输入和输出。 CGI程序通常用于处理表单输入或数据库查询。

**Cinepac**

计算机视频编解码器

**Client**

请参阅Web客户端。

**Client/Server**

在web方面：和Web客户端和Web服务器之间工作量分离的通信。

**Click**

在web方面：鼠标点击一个超链接元素（如文字或图片）在网页上创建一个事件如要到另一个网页访问或访问同一页其他部分。

**Clickthrough Rate**

访问者点击页面上的超链接（或广告）页面已经显示时间数的比例。

**Cloud Computing**

在互联网上存储的应用程序和数据（而不是用户的计算机上）。

**Codec (Compressor / Decompressor)**

用于数据压缩和解压技术通用术语。

**Communication Protocol**

标准（语言和一套规则），让电脑用一个标准方式进行交互。例如IP，FTP和HTTP。

**Compression**

更快通过网络交付Web文档或图形大小（压缩）方法。

**Computer Virus**

一种计算机程序，可能损害消息显示，删除文件甚至摧毁计算机操作系统的计算机。

**Cookie**

Web浏览器由Web服务器，在您的计算机上存储的信息。 Cookie的目的就是提供有关您访问网站的信息，在随后的访问服务器使用。

**ColdFusion**

Web开发软件的大多数平台（LINUX，UNIX，Solaris和Windows）。

**CSS (Cascading Style Sheets)**

一个W3C推荐定义Web文档样式（如字体，大小，颜色，间距等）语言。

**Database**

以这样的方式，一个计算机程序可以轻松地检索和操纵数据计算机存储数据。

**Database System**

操纵数据库中的数据的计算机程序（如MS ACCESS，Oracle和MySQL）。

**DB2**

来自IBM数据库系统。大多用于Unix和Solaris平台。

**DBA (Data Base Administrator)**

人（或软件）谁管理数据库。典型的任务：备份，维护和执行。

**DHCP (Dynamic Host Configuration Protocol)**

互联网标准协议，为NEE用户分配新的IP地址。

**DHTML (Dynamic HTML)**

常用的一个术语来描述HTML内容可能动态改变。

**Dial-up Connection**

在web方面：一个通过电话和调制解调器连接到Internet。

**Discussion Group**

请参阅Newsgroup。

**DNS (Domain Name Service)**

计算机程序运行在Web服务器上域名翻译成IP地址。

**DNS Server**

Web服务器执行DNS。

**DOM (Document Object Model)**

一个网页对象编程模型。

**Domain Name**

一个网站的名称标识。 （如：runoob.com）

**DOS (Disk Operating System)**

一个通用基于磁盘的计算机操作系统（见操作系统）。最初是微软IBM个人电脑。通常用于为MS - DOS简写。

**Download**

从远程计算机传输文件到本地计算机。在web方面：Web客户端从Web服务器传输文件。

**DSL (Digital Subscriber Line)**

基于普通电话线的宽带接入技术

**DTD (Document Type Definition)**

一个定义类似HTML或XMLweb文件合法构建模块的规则（一种语言）。

**Dynamic IP**

每次连接到互联网的IP地址的变化。

**E-mail (Electronic Mail)**

由一个人到另一个通过互联网发送消息。

**E-mail Address**

发送电子邮件给一个人或一个组织使用的地址。典型格式就是用户名@主机名。

**E-mail Server**

一个Web服务器专用电子邮件服务任务。

**Encryption**

要从原来的形式转换的数据，一个只能读取人可以逆转的加密形式。加密的目的是为了防止未经授权的读取数据。

**Error**

请参阅Web服务器错误。

**Ethernet**

一个局域网络类型（见局域网）。

**Firewall**

作为一个安全过滤器，可以限制类型的网络通信行为软件。最常用于个人计算机（或LAN）和互联网之间。

**Flash**

基于矢量的多媒体格式在Web上的使用，由Adobe开发

**Form**

请参阅HTML表单。

**Forum**

在web方面：同样的作为Newsgroup。

**Frame**

在web方面：浏览器中显示屏幕一部分特定内容。帧通常用于显示不同的网页内容。

**FrontPage**

Windows平台Web开发软件。由微软开发。

**FTP (File Transfer Protocol)**

两台计算机之间发送文件最常用的方法之一。

**FTP Server**

一个Web服务器，您可以登录，并下载文件（或文件上传到）。匿名FTP是一种不使用登录帐户从FTP服务器下载文件方法。

**Gateway**

一个计算机程序之间不兼容的应用程序或网络传输（格式化）数据。

**GIF (Graphics Interchange Format)**

一个由CompuServe开发的存储图像压缩格式。互联网上最常见的图像格式之一。

**GB**

Gigabyte，千兆字节，电脑的一种存储单位。

**Gigabyte**

1024兆字节。通常向下舍入到10亿字节。

**Graphics**

在web方面介绍图片（相对文本）。

**Graphic Monitor**

一个显示屏，可以显示图形。

**Graphic Printer**

一台打印机，可以打印图形。

**Graphical Banner**

请参阅Banner Ad。

**Helper application**

在web方面：一个方案帮助浏览器显示，视图或工作，浏览器本身不能处理。（请参阅插件）。

**Hits**

Web对象（网页或图片）已被浏览或下载的数量。（请参阅页点击）。

**Home Page**

一个网站的顶层（主）页。当你访问某个网站显示的默认页。

**Host**

请参阅Web主机。

**Hosting**

请参阅虚拟主机

**Hotlink**

请参阅超链接。

**HTML (Hypertext Markup Language)**

HTML就是web语言。HTML是一种用来定义内容，布局和网页文件格式的标签设置。 Web浏览器使用HTML标签来定义如何显示文本。

**HTML Document**

在HTML编写的文件。

**HTML DOM (HTML Document Object Model)**

一个HTML文档编程接口。

**HTML Editor**

一个软件程序，用于编辑HTML页面。有了一个HTML编辑器你可以像使用字处理器，向HTML文档添加元素如列表表格布局，字体大小，和颜色。正在编辑时它会在网页上显示（所见即所得）HTML编辑器将正在编辑的内容显示在页面。

**HTML Form**

用户输入的形式传递回服务器。

**HTML Tags**

代码以识别文档的的不同部分使网页浏览器会知道如何显示。

**HTTP (Hyper Text Transfer Protocol)**

通过Internet发送文本文件的规则标准设置。它需要一端HTTP客户端程序，并在另一端HTTP服务器程序。

**HTTP Client**

一种计算机程序，从Web服务器请求服务。

**HTTP Server**

一种计算机程序，从Web服务器提供服务。

**HTTPS (Hyper Text Transfer Protocol Secure)**

和HTTP相同但提供安全的互联网的通信的SSL。 （另请参阅SSL）

**Hyperlink**

网页中链接其它网页的文本串称为HYPERLINK。

**Hypermedia**

扩展超文本，图形和音频。

**Hypertext**

超文本是交叉链接的，以这样的方式，读者可以阅读相关文件通过点击一个高亮显示的单词或符号或其他文件文本。（另请参阅超链接）

**IAB (Internet Architecture Board)**

一个理事会为Internet标准决策（另请参阅w3c）。

**IE (Internet Explorer)**

请参阅Internet Explorer。

**IETF (Internet Engineering Task Force)**

一个侧重于解决技术问题，在互联网上对IAB分组。

**IIS (Internet Information Server)**

一个适用于Windows操作系统的Web服务器。由微软开发。

**IMAP (Internet Message Access Protocol)**

一个电子邮件服务器检索电子邮件标准通信协议。 IMAP是很像POP，但更先进的。

**Indeo**

由英特尔公司开发的计算机视频编解码器。

**Internet**

一个世界性的连接数以百万计的电脑的网络。 （请参阅也万维网）

**Internet Browser**

请参阅Web浏览器。

**Internet Explorer**

微软浏览器。最常用浏览器。

**Internet Server**

请参见Web服务器

**Intranet**

私有（封闭）互联网内部的LAN（局域网）运行。

**IP (Internet Protocol)**

请参阅TCP / IP协议。

**IP Address (Internet Protocol Address)**

在互联网上的每一台计算机的一个独特的识别号码（如197.123.22.240）

**IP Number (Internet Protocol Number)**

一个IP地址。

**IP Packet**

请参阅TCP / IP包。

**IRC (Internet Relay Chat)**

互联网系统，使用户可以在网上讨论。

**IRC Client**

一个计算机程序，使用户能够连接到IRC。

**IRC Server**

Internet服务器专用的IRC连接服务任务。

**ISAPI (Internet Server API)**

Internet Information Server应用程序编程接口（API）（参阅IIS）。

**ISDN (Integrated Services Digital Network)**

使用数字传输的电信标准，支持通过普通电话线数据通信。

**ISP (Internet Service Provider)**

提供访问互联网和网站托管。

**Java**

由Sun开发的一种编程语言。大多用于编程Web服务器和Web小程序。

**Java Applet**

请参阅网页的Applet。

**JavaScript**

互联网上最流行的脚本语言由Netscape开发。

**JPEG (Joint Photographic Expert Group)**

旨在促进JPG和JPEG图形格式存储压缩图像。

**JPEG and JPG**

图形格式存储压缩图像。

**JScript**

微软版本的JavaScript。

**JSP (Java Server Pages)**

一个基于Java技术允许在网页中插入服务器可执行的脚本。主要用于在Linux，Unix和Solaris平台。

**K**

千字节10K同样是十千字节..

**KB**

千字节10K同样是十千字节..

**Keyword**

在web方面：搜索引擎来搜索相关网络信息的一个词。 在数据库术语：一个字（或指数）用于识别数据库中的记录。

**Kilobyte**

1024字节。通常被称为1K，向下调整至1000个字节。

**LAN (Local Area Network)**

在局部地区（如建筑物内）的计算机之间的网络，通常是通过当地的电缆连接。请参阅广域网。

**Link**

和超链接相同。

**Linux**

开放源码计算机操作系统，基于UNIX的。主要用于服务器和Web服务器。

**Mail**

在网络方面：和电子邮件相同。

**Mail Server**

请参阅e - mail服务器。

**MB**

和兆字节相同。 10MB是10兆字节。

**Megabyte**

1024千字节。通常向下舍入到一百万字节。

**Meta Data**

说明其他数据的数据。（meta标签）。

**Meta Search**

搜索文件中的元数据的方法。

**Meta Tags**

标签插入到文档中描述的文件。

**MIDI (Musical Instrument Digital Interface)**

一个计算机和乐器之间的沟通的标准协议。

**MIME (Multipurpose Internet Mail Extensions)**

定义文档类型的Internet标准。 MIME类型的例子：文本/纯文本，文本/图像/ GIF，HTML，图像/ JPG。

**MIME Types**

根据MIME文件类型定义。

**Modem**

硬件设备将计算机连接到电话网络，通常用于通过电话线连接到互联网。

**Mosaic**

第一个常用的Web浏览器。Mosaic是在1993年发布，并开始普及Web。

**MOV**

由苹果公司开发的计算机视频编解码器。 QuickTime多媒体文件的通用文件扩展名。

**MP3 (MPEG-1 Audio Layer-3)**

专为方便下载的Internet.c设计的一种音频压缩格式

**MP3 File**

文件中包含音频压缩MP3。最常见的音乐曲目。

**MPEG (Moving Picture Expert Group)**

一个ISO标准计算机的音频和视频编解码器。

**MPG**

MPEG文件的通用文件扩展名。

**MS-DOS (Microsoft Disk Operating System)**

一个通用的基于磁盘的计算机操作系统（操作系统）。最初由微软开发为IBM电脑，然后由微软开发的第一个版本的Windows的基础。

**Multimedia**

在网络方面：结合文字与图片，视频或声音的演示文稿。

**MySQL**

经常被用来在网络上免费的开源数据库软件。

**NetBEUI (Net Bios Extended User Interface)**

一个增强版的NetBIOS。

**NetBIOS (Network Basic Input Output System)**

一个应用程序编程接口（API），在局域网功能（LAN）上。用于DOS和Windows。

**Navigate**

在网络方面：和浏览相同。

**Netscape**

该公司的Netscape浏览器。是多年来最流行的浏览器。今天，带领着IE浏览器。

**Newsgroup**

一个在线讨论小组（新闻服务器上的部分），专用于感兴趣的某个主题。

**News Reader**

一个计算机程序，可以读取（和POST消息）从Internet新闻组。

**News Server**

Internet服务器专用的互联网新闻组服务的任务。

**Node**

在连接到互联网的网络方面：一台电脑，最经常被用来描述一个Web服务器。

**Opera**

来自挪威 Opera Software ASA公司的 Opera 的浏览器。

**OS (Operating System)**

管理软件的电脑基本操作。

**Packet**

请参阅参阅TCP / IP包。

**Page Hits**

网页由用户访问的次数数量。

**Page Impressions**

点击次数相同。

**Page Views**

点击次数相同。

**PDF (Portable Document Format)**

一个文档文件格式，由Adobe开发。最常用的文本文件。

**Perl (Practical Extraction and Reporting Language)**

一个Web服务器的脚本语言。最常用在UNIX服务器上。

**PHP (PHP: Hypertext Preprocessor)**

一个技术，允许在网页中插入服务器可执行脚本。多用在Unix，Linux和Solaris平台。

**Ping**

一个方法用来检查两台计算机之间的沟通。一个"ping"发送到远程计算机，看它是否响应。

**Platform**

在网络方面：电脑的作业系统，如Windows，Linux或OS X

**Plug-In**

到另一个应用程序构建的应用程序。在网络方面：（或添加）内置Web浏览器来处理像电子邮件，声音，或电影文件的数据的一个特殊类型的程序。（另见ActiveX）

**PNG (Portable Network Graphics)**

图像文件存储格式，其目的是试图替代GIF和TIFF文件格式，同时增加一些GIF文件格式所不具备的特性

**POP (Post Office Protocol)**

一个电子邮件服务器检索电子邮件的标准通信协议。（另见IMAP）。

**Port**

一个标识一台计算机的IO（输入/输出）通道。在网络方面：一个标识Internet应用程序（Web服务器通常使用80端口）所使用的I / O通道。

**Protocol**

请参阅通信协议。

**PPP (Point to Point Protocol)**

一个用于两台计算机之间的直接连接的通信协议。

**Proxy Server**

Internet服务器致力于改善互联网性能。

**QuickTime**

由苹果公司创建多媒体文件的格式。

**RAID (Redundant Array of Independent Disks)**

一个更高的安全性，速度和性能相同的服务器连接多个磁盘标准。通常用于Web服务器上。

**RDF (Resource Description Framework)**

用于描述Web资源建设的框架的语言。

**Real Audio**

一个常见的多媒体音频格式，由Real Networks公司创建。

**Real Video**

一个常见的多媒体视频格式，由Real Networks公司创建。

**Redirect**

在网络方面：行动时，网页自动转发（重定向）到另一个网页的用户。

**RGB (Red Green Blue)**

可以代表全彩光谱的三原色组合。

**Robot**

请参阅网络Robot。

**Router**

一个硬件（或软件）系统（路线），指示数据在不同的计算机上网络传输。

**Schema**

参见XML Schema。

**Script**

脚本语言编写的语句的集合。

**Scripting Language**

在网络方面：一个简单的编程语言，可以通过Web浏览器或Web服务器执行。 参考JavaScript和VBScript。

**Scripting**

编写一个脚本。

**Search Agent**

和搜索引擎相同。

**Search Engine**

计算机程序用于搜索和目录（索引）数以百万计的网页在网页上提供的信息。常见的搜索引擎Google和AltaVista。

**Semantic Web**

一个网站的意义，在这个意义上，计算机程序可以了解足够的数据与处理数据。

**Server**

请参阅Web服务器。

**Server Errors**

参考Web服务器错误。

**Shareware**

软件，你可以尝试免费的，并支付一定的费用，继续合法使用。

**Shockwave**

在网页中嵌入多媒体内容由Adobe开发的一种格式（技术）。

**SGML (Standard Generalized Markup Language)**

用于标记语言的国际标准。 HTML和XML的基础。

**SMIL (Synchronized Multimedia Integration Language)**

一个W3C推荐语言创建的多媒体演示。

**SMTP (Simple Mail Transfer Protocol)**

一个发送电子邮件的计算机之间的标准通信协议。

**SOAP (Simple Object Access Protocol)**

让应用程序进行通信相互使用XML的一个标准协议。

**Solaris**

来自Sun的计算机操作系统。

**SPAM**

在网络方面：多个不受欢迎的邮件发送到新闻组或邮件列表的行动。

**Spider**

请参阅Web Spider。

**Spoofing**

网页或虚假引荐的电子邮件的寻址。像假地址发送电子邮件。

**Spyware**

计算机软件隐藏在一台计算机与使用计算机收集信息的目的。

**SQL (Structured Query Language)**

访问和操作数据库的一个ANSI标准的计算机语言。

**SQL Server**

来自微软的数据库系统。主要用于在高流量的网站，在Windows平台上运行的网站。

**SSI (Server Side Include)**

HTML注释类型插入到网页指示Web服务器生成动态内容。最常见的用途是包含标准页眉或页脚的页面。

**SSL (Secure Socket Layer)**

软件安全和保护网站沟通，采用加密数据传输。

**Static IP (address)**

静态ip,对应于动态ip。

**Streaming**

这样一种方式，用户可以查看正在传输的文件，同时通过互联网发送视频和音频文件的方法。

**Streaming Format**

在互联网上使用的文件格式流媒体。（请参阅Windows Media，Real视频和QuickTime）。

**SVG (Scalable Vector Graphics)**

一个W3C推荐的语言，在XML中定义图形。

**Tag**

在网络方面：书面通知或进入网页文件命令。

**TCP (Transmission Control Protocol)**

请参阅TCP / IP协议。

**TCP/IP (Transmission Control Protocol / Internet Protocol)**

两台计算机之间的互联网通信协议的集合。 TCP协议是两台计算机之间的自由连接，而IP协议负责通过网络发送的数据包。

**TCP/IP Address**

请参阅IP地址。

**TCP/IP Packet**

一个"包"一个TCP / IP网络上传送的数据。 （通过互联网发送的数据被分成从40到32000字节长的小"包"）

**Trojan Horse**

计算机程序，隐藏在另一台计算机破坏程序软件或使用计算机收集信息的目的。

**UDDI (Universal Description Discovery and Integration)**

独立于平台的框架，用于描述服务，探索业务，并使用互联网的集成业务服务。

**Unix**

计算机操作系统，由贝尔实验室开发。大多用于服务器和Web服务器。

**UNZIP**

要解压压缩文件。请参阅zip。

**Upload**

从本地计算机传输文件到远程计算机。在网络方面：从Web客户端传输到Web服务器的文件。（见下载）。

**URI (Uniform Resource Identifier)**

用来确定在互联网上的资源。 URL是一种类型的URI。

**URL (Uniform Resource Locator)**

Web地址。标准的办法来解决互联网上的网页文件（页）（如：http://www.runoob.com/）

**USENET**

一个世界性的新闻系统，通过互联网访问。（参阅新闻组）

**User Agent**

和网页浏览器相同。

**VB (Visual Basic)**

请参阅Visual Basic。

**VBScript**

来自微软的脚本语言。 VBScript是ASP中的默认脚本语言。也可用于程序的Internet Explorer。

**Virus**

和计算机病毒相同

**Visit**

在网络方面：对一个网站的访问。常用来形容一个Web站点的访问者的活动。

**Visitor**

在网络方面：Web站点的访问者。常用来形容一个人访问Web站点（观赏）。

**Visual Basic**

来自微软的编程语言

**科学上网 (Virtual Private Network)**

两个远程站点之间的专用网络，通过一个安全加密的虚拟互联网连接（隧道）。

**VRML (Virtual Reality Modeling Language)**

一种编程语言，允许被添加到HTML文档中的3D效果。

**W3C (World Wide Web Consortium)**

该组织用于管理标准的WWW。

**WAN (Wide Area Network)**

在一个广泛的网络连接在一起的计算机，比局域网大，通常是通过电话线连接。另见局域网。

**WAP (Wireless Application Protocol)**

一个旧标准的数字移动电话等无线终端上的信息服务。

**Web Address**

和一个URL或URI相同。参考网址。

**Web Applet**

一个可以在网上下载，并在用户的计算机上运行的程序。最经常用Java编写的。

**Web Client**

一个软件程序，用于访问网页。有时和一个Web浏览器相同，但经常被用来作为一个广义的术语。

**Web Browser**

一个软件程序，用于显示网页。

**Web Document**

一个文件格式在网上进行传播。最常见的网页文件是标记语言如HTML或XML格式。

**Web Error**

请参阅Web服务器错误。

**Web Form**

请参阅HTML表单。

**Web Host**

一个Web服务器，如公司或个人提供网站空间的"主机"Web服务。

**Web Hosting**

提供虚拟主机服务的动作。

**Web Page**

旨在通过Web分发的一份文件（通常是一个HTML文件）。

**Web Log**

请参阅Blog。

**Web Robot**

请参阅Web Spider。

**Web Server**

服务器是为一种计算机到其他计算机提供服务或信息。在网络方面：一个服务器提供Web内容的Web浏览器。

**Web Server Error**

从Web服务器，显示一个错误的讯息。最常见的Web服务器错误"404文件未找到"。

**Web Services**

软件组件和Web服务器上运行的应用程序。服务器到其他计算机上，浏览器或个人提供这些服务，使用标准的通讯协议。

**Web Site**

属于公司或个人的网页的集合的相关的网站。

**Web Spider**

一种计算机程序，搜索互联网网页。常见的web spiders是一个像谷歌搜索引擎索引的网页。web spiders也被称为网络机器人或漫游者。

**Web Wanderer**

请参阅Web Spider。

**Wildcard**

一个字符用来代替任何字符（S）。最常用的作为一个星号（*）的搜索工具。

**Windows 2000, Windows NT, Windows 95/98, Windows XP**

来自Microsoft的电脑操作系统。

**Windows Media**

音频和视频格式，由Microsoft在互联网上开发。

**WINZIP**

一个压缩和解压文件的计算机程序。请参阅zip。

**WMA**

互联网的音频文件格式，由Microsoft开发。

**WMV**

互联网的视频文件格式，由Microsoft开发。 

**WML (Wireless Markup Language)**

旧标准用于无线终端，数字移动电话，从HTML继承，但基于XML的，远远比HTML更严格的信息服务。

**WML Script**

用于WML脚本语言（编程语言）。

**Worm**

计算机病毒，可以使自己的副本，并通过互联网传播到其他计算机。

**WSDL (Web Services Description Language)**

一个基于XML的语言，用于描述Web服务以及如何访问它们。

**WWW (World Wide Web)**

一个利用互联网进行交流的Web文档的计算机的全球网络。（请参阅互联网）

**WWW Server**

和Web Server相同。

**WYSIWYG (What You See Is What You Get)**

在Web方面：若要显示正在编辑的网页，以完全相同的方式显示在网页上。

**XForms**

替代版本的HTML表单，基于XML和XHTML。来自HTML表单的不同分离数据定义和数据显示。提供更丰富，更独立于设备的用户输入。

**XHTML (Extensible Hypertext Markup Language)**

以XML格式重新制定HTML。由W3C开发。

**XPath**

XPath是一种用于定义XML文档的部分（语言）的语法规则的设置。 XPath是一个W3C的XSL标准的重要组成部分。

**XQuery**

XQuery是一种用于从XML文档中提取信息（语言）的语法规则的设置。XQuery是有关XPath的基础。 XQuery是由W3C开发。

**XML (Extensible Markup Language)**

由W3C开发的Web文件，是专为SGML的一个简化版本。

**XML Document**

XML编写的一个文件。

**XML DOM (XML Document Object Model)**

一个由W3C制定的XML文档的编程接口。

**XMLHttpRequest**

编程接口（对象），所有现代Web浏览器对JavaScript都支持，使用网络浏览器和幕后（AJAX）的Web服务器之间交换数据。

**XML Schema**

XML Schema是以XML语言为基础的，它用于可替代DTD。XML schema文件描述了XML文档的结构。

**XSD (XML Schema Definition)**

和XML Schema大致相同。

**XSL (Extensible Stylesheet Language)**

一套由W3C开发的XML语言，包括XSLT，XSL - FO和XPath。

**XSL-FO (XSL Formatting Objects)**

用于格式化XML文档的XML语言。W3C XSL的一部分。

**XSLT (XSL Transformations)**

用于转换XML文档的XML语言。W3C XSL的一部分。

**ZIP**

一个计算机上的文件压缩格式。通常用于压缩文件。 使用象WinZip的计算机程序ZIP文件可以压缩（zipped）并解压缩。

# 13. SEO - 搜索引擎优化

------

您是否曾经使用过搜索引擎？你是否知道目前主要的搜索引擎有哪些？

本文列出了目前流行的搜索引擎列表。

------

## 13.1. SEO - 搜索引擎优化（Search Engine Optimization）

搜索引擎优化 (SEO) 是提高一个网站在搜索引擎中的排名（能见度）的过程。如果网站能够在搜索引擎中有良好的排名，有助于网站获得更多的流量。

SEO主要研究搜索引擎是工作的原理，是什么人搜索，输入什么搜索关键字。优化一个网站，可能涉及内容的编辑，增加关键字的相关性。推广一个网站，可以增加网站的外链数量。

搜索引擎优化需要修改一个网站的HTML源代码和网站内容。搜索引擎优化策略应在网站建设之前就纳入网站的发展，尤其是网站的菜单和导航结构。

笼统的说，所有使用作弊手段或可疑手段的，都可以称为黑帽SEO。比如说垃圾链接，隐藏网页，桥页，关键词堆砌等等。

黑帽seo就是作弊的意思，黑帽seo手法不符合主流搜索引擎发行方针规定。黑帽SEO获利主要的特点就是短平快，为了短期内的利益而采用的作弊方法。同时随时因为搜索引擎算法的改变而面临惩罚。

不论是白帽seo还是黑帽seo没有一个精准的定义。笼统来说所有使用作弊手段或一些可疑手段的都可称为黑帽SEO。例如隐藏网页，关键词堆砌，垃圾链接，桥页等等。

