<h2>目录</h2>

<details open>
  <summary><a href="#1-概览">1. 概览</a></summary>
  <ul>
    <a href="#11-简介">1.1. 简介</a><br>
    <a href="#12-markdown-应用">1.2. Markdown 应用</a><br>
    <a href="#13-编辑器">1.3. 编辑器</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#2-markdown-标题">2. Markdown 标题</a></summary>
  <ul>
    <a href="#21-使用--和---标记一级和二级标题">2.1. 使用 = 和 - 标记一级和二级标题</a><br>
    <a href="#22-使用--号标记">2.2. 使用 # 号标记</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#3-段落格式">3. 段落格式</a></summary>
  <ul>
    <a href="#31-字体">3.1. 字体</a><br>
    <a href="#32-分隔线">3.2. 分隔线</a><br>
    <a href="#33-删除线">3.3. 删除线</a><br>
    <a href="#34-下划线">3.4. 下划线</a><br>
    <a href="#35-脚注">3.5. 脚注</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#4-列表">4. 列表</a></summary>
  <ul>
    <a href="#41-无序列表">4.1. 无序列表</a><br>
    <a href="#42-有序列表">4.2. 有序列表</a><br>
    <a href="#43-列表嵌套">4.3. 列表嵌套</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#5-区块">5. 区块</a></summary>
  <ul>
    <a href="#51-区块常规使用">5.1. 区块常规使用</a><br>
    <a href="#52-区块中使用列表">5.2. 区块中使用列表</a><br>
    <a href="#53-列表中使用区块">5.3. 列表中使用区块</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#6-代码">6. 代码</a></summary>
  <ul>
    <a href="#61-常规代码">6.1. 常规代码</a><br>
    <a href="#62-代码区块">6.2. 代码区块</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#7-链接">7. 链接</a></summary>
  <ul>
    <a href="#71-普通链接">7.1. 普通链接</a><br>
    <a href="#72-高级链接">7.2. 高级链接</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#8-图片">8. 图片</a></summary>
  <ul>
    <a href="#81-常规显示图片">8.1. 常规显示图片</a><br>
    <a href="#82-高级显示图片">8.2. 高级显示图片</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#9-表格">9. 表格</a></summary>
  <ul>
    <a href="#91-一般使用">9.1. 一般使用</a><br>
    <a href="#92-对齐方式">9.2. 对齐方式</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#10-高级技巧">10. 高级技巧</a></summary>
  <ul>
    <a href="#101-支持的-html-元素">10.1. 支持的 HTML 元素</a><br>
    <a href="#102-转义">10.2. 转义</a><br>
    <a href="#103-公式">10.3. 公式</a><br>
  <details open>
    <summary><a href="#104-uml（目前gitlab支持）">10.4. UML（目前GitLab支持）</a>  </summary>
    <ul>
    <details open>
      <summary><a href="#1041-流程图">10.4.1. 流程图</a>    </summary>
      <ul>
        <a href="#10411-横向流程图">10.4.1.1. 横向流程图</a><br>
        <a href="#10412-竖向流程图">10.4.1.2. 竖向流程图</a><br>
        <a href="#10413-标准流程图">10.4.1.3. 标准流程图</a><br>
        <a href="#10414-标准流程图（横向）">10.4.1.4. 标准流程图（横向）</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1042-时序图">10.4.2. 时序图</a>    </summary>
      <ul>
        <a href="#10421-uml时序图源码样例">10.4.2.1. UML时序图源码样例</a><br>
        <a href="#10422-uml时序图源码复杂样例">10.4.2.2. UML时序图源码复杂样例</a><br>
        <a href="#10423-uml标准时序图样例">10.4.2.3. UML标准时序图样例</a><br>
      </ul>
    </details>
    <details open>
      <summary><a href="#1043-甘特图">10.4.3. 甘特图</a>    </summary>
      <ul>
        <a href="#10431-甘特图样例">10.4.3.1. 甘特图样例</a><br>
      </ul>
    </details>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#11-利用vs-code在markdown上自动添加和删除多级标题数字">11. 利用VS Code在Markdown上自动添加和删除多级标题数字</a></summary>
  <ul>
    <a href="#111-准备">11.1. 准备</a><br>
    <a href="#112-简介">11.2. 简介</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#12-相关笔记">12. 相关笔记</a></summary>
  <ul>
    <a href="#121-markdown的父目录，上一级目录">12.1. Markdown的父目录，上一级目录</a><br>
  </ul>
</details>


<h1>Markdown</h1>

# 1. 概览

## 1.1. 简介

* Markdown 是一种轻量级标记语言，它允许人们使用易读易写的纯文本格式编写文档。
* Markdown 语言在 2004 由约翰·格鲁伯（英语：John Gruber）创建。
* Markdown 编写的文档可以导出 HTML 、Word、图像、PDF、Epub 等多种格式的文档。
* Markdown 编写的文档后缀为**.md**, **.markdown**。

## 1.2. Markdown 应用

&emsp;&emsp;Markdown能被使用来撰写电子书，如：Gitbook。
&emsp;&emsp;当前许多网站都广泛使用Markdown来撰写帮助文档或是用于论坛上发表消息。例如：GitHub、简书、reddit、Diaspora、Stack Exchange、OpenStreetMap 、SourceForge等。

## 1.3. 编辑器

&emsp;&emsp;本教程将使用 Typora 编辑器来讲解 Markdown 的语法，Typora 支持 MacOS 、Windows、Linux 平台，且包含多种主题，编辑后直接渲染出效果。
&emsp;&emsp;支持导出HTML、PDF、Word、图片等多种类型文件。
&emsp;&emsp;Typora 官网：https://typora.io/
&emsp;&emsp;你也可以使用我们的在线编辑器来测试：https://c.runoob.com/front-end/712。

# 2. Markdown 标题

Markdown 标题有两种格式。

## 2.1. 使用 = 和 - 标记一级和二级标题

= 和 - 标记语法格式如下：

```
我展示的是一级标题
=================

我展示的是二级标题
-----------------
```

显示效果如下图：

![img](./images/Markdown-1.jpg)

## 2.2. 使用 # 号标记

使用 **#** 号可表示 1-6 级标题，一级标题对应一个 **#** 号，二级标题对应两个 **#** 号，以此类推。

```
# 一级标题
## 二级标题
### 三级标题
#### 四级标题
##### 五级标题
###### 六级标题
```

显示效果如下图：

![img](./images/Markdown-1.gif)

# 3. 段落格式

## 3.1. 字体

&emsp;&emsp;Markdown 可以使用以下几种字体：

~~~markdown
*斜体文本*
_斜体文本_
**粗体文本**
__粗体文本__
***粗斜体文本***
___粗斜体文本___
~~~

&emsp;&emsp;显示效果如下图：

*斜体文本*
_斜体文本_
**粗体文本**
__粗体文本__
***粗斜体文本***
___粗斜体文本___

## 3.2. 分隔线

&emsp;&emsp;你可以在一行中用三个以上的星号、减号、底线来建立一个分隔线，行内不能有其他东西。你也可以在星号或是减号中间插入空格。下面每种写法都可以建立分隔线：

~~~markdown
***

* * *

*****

- - -

----------
~~~

&emsp;&emsp;显示效果如下所示：

***

* * *

*****

- - -

----------

## 3.3. 删除线

&emsp;&emsp;如果段落上的文字要添加删除线，只需要在文字的两端加上两个波浪线 ~~ 即可，实例如下：

~~~
RUNOOB.COM
GOOGLE.COM
~~BAIDU.COM~~
~~~
显示效果如下所示：
RUNOOB.COM
GOOGLE.COM
~~BAIDU.COM~~

## 3.4. 下划线

下划线可以通过 HTML 的 <u> 标签来实现：

~~~
<u>带下划线文本</u>
~~~

显示效果如下所示：

<u>带下划线文本</u>

## 3.5. 脚注

&emsp;&emsp; 脚注是对文本的补充说明。

&emsp;&emsp; Markdown 脚注的格式如下:

~~~
[^要注明的文本]
~~~

&emsp;&emsp; 以下实例演示了脚注的用法：

~~~
创建脚注格式类似这样 [^RUNOOB]。

[^RUNOOB]: 菜鸟教程 -- 学的不仅是技术，更是梦想！！！
~~~

&emsp;&emsp; 演示效果如下：

创建脚注格式类似这样 [^RUNOOB]。

[^RUNOOB]: 菜鸟教程 -- 学的不仅是技术，更是梦想！！！

# 4. 列表

&emsp;&emsp; Markdown 支持有序列表和无序列表。

## 4.1. 无序列表

&emsp;&emsp; 无序列表使用星号(\*)、加号(+)或是减号(-)作为列表标记:

~~~
* 第一项
* 第二项
* 第三项

+ 第一项
+ 第二项
+ 第三项


- 第一项
- 第二项
- 第三项
~~~

&emsp;&emsp; 显示结果如下：

* 第一项
* 第二项
* 第三项

+ 第一项
+ 第二项
+ 第三项


- 第一项
- 第二项
- 第三项

## 4.2. 有序列表

&emsp;&emsp; 有序列表使用数字并加上 . 号来表示，如：

~~~
1. 第一项
2. 第二项
3. 第三项
~~~

&emsp;&emsp; 显示结果如下：

1. 第一项
2. 第二项
3. 第三项

## 4.3. 列表嵌套

&emsp;&emsp; 列表嵌套只需在子列表中的选项添加四个空格即可：

~~~
1. 第一项：
    - 第一项嵌套的第一个元素
    - 第一项嵌套的第二个元素
2. 第二项：
    - 第二项嵌套的第一个元素
    - 第二项嵌套的第二个元素
~~~

&emsp;&emsp; 显示结果如下：

1. 第一项：
    - 第一项嵌套的第一个元素
    - 第一项嵌套的第二个元素
2. 第二项：
    - 第二项嵌套的第一个元素
    - 第二项嵌套的第二个元素

# 5. 区块

## 5.1. 区块常规使用

&emsp;&emsp; Markdown 区块引用是在段落开头使用 > 符号 ，然后后面紧跟一个空格符号：

~~~
> 区块引用
> 菜鸟教程
> 学的不仅是技术更是梦想
~~~

&emsp;&emsp; 显示结果如下：

> 区块引用
> 菜鸟教程
> 学的不仅是技术更是梦想

&emsp;&emsp; 另外区块是可以嵌套的，一个 > 符号是最外层，两个 > 符号是第一层嵌套，以此类推退：

~~~
> 最外层
> > 第一层嵌套
> > > 第二层嵌套
~~~

&emsp;&emsp; 显示结果如下：

> 最外层
> > 第一层嵌套
> >
> > > 第二层嵌套

## 5.2. 区块中使用列表

&emsp;&emsp; 区块中使用列表实例如下：

~~~
> 区块中使用列表
> 1. 第一项
> 2. 第二项
> + 第一项
> + 第二项
> + 第三项
~~~

&emsp;&emsp; 显示结果如下：

> 区块中使用列表
> 1. 第一项
> 2. 第二项
> + 第一项
> + 第二项
> + 第三项

## 5.3. 列表中使用区块
&emsp;&emsp; 如果要在列表项目内放进区块，那么就需要在 > 前添加四个空格的缩进。

&emsp;&emsp; 区块中使用列表实例如下：

~~~
* 第一项
    > 菜鸟教程
    > 学的不仅是技术更是梦想
* 第二项
~~~

&emsp;&emsp; 显示结果如下：

* 第一项
    > 菜鸟教程
    > 学的不仅是技术更是梦想
* 第二项

# 6. 代码

## 6.1. 常规代码

&emsp;&emsp; 如果是段落上的一个函数或片段的代码可以用反引号把它包起来（\`），例如：

~~~
`printf()` 函数
~~~

&emsp;&emsp; 显示结果如下：

`printf()` 函数

## 6.2. 代码区块

&emsp;&emsp; 代码区块使用 4 个空格或者一个制表符（Tab 键）。

&emsp;&emsp; 你也可以用\`\`\`包裹一段代码，并指定一种语言（也可以不指定）：

\`\`\`javascript
$(document).ready(function () {
    alert('RUNOOB');
});
\`\`\`

&emsp;&emsp; 显示结果如下：

```javascript
$(document).ready(function () {
    alert('RUNOOB');
});
```

# 7. 链接

## 7.1. 普通链接

&emsp;&emsp; 链接使用方法如下：

```
[链接名称](链接地址)

或者

<链接地址>
```
例如：

```
这是一个链接 [菜鸟教程](https://www.runoob.com)
```

## 7.2. 高级链接

```
链接也可以用变量来代替，文档末尾附带变量地址：
这个链接用 1 作为网址变量 [Google][1]
这个链接用 runoob 作为网址变量 [Runoob][runoob]
然后在文档的结尾为变量赋值（网址）

  [1]: http://www.google.com/
  [runoob]: http://www.runoob.com/
```

&emsp;&emsp; 显示结果如下：

链接也可以用变量来代替，文档末尾附带变量地址：
这个链接用 1 作为网址变量 [Google][1]
这个链接用 runoob 作为网址变量 [Runoob][runoob]
然后在文档的结尾为变量赋值（网址）

[1]: http://www.google.com/
[runoob]: http://www.runoob.com/

# 8. 图片

## 8.1. 常规显示图片

&emsp;&emsp; Markdown 图片语法格式如下：

```
![alt 属性文本](图片地址)

![alt 属性文本](图片地址 "可选标题")
```

* 开头一个感叹号 !
* 接着一个方括号，里面放上图片的替代文字
* 接着一个普通括号，里面放上图片的网址，最后还可以用引号包住并加上选择性的 'title' 属性的文字。

&emsp;&emsp; 当然，你也可以像网址那样对图片网址使用变量:

```
这个链接用 1 作为网址变量 [RUNOOB][1].
然后在文档的结尾位变量赋值（网址）

[1]: http://static.runoob.com/images/runoob-logo.png
```

&emsp;&emsp; Markdown还没有办法指定图片的高度与宽度，如果你需要的话，你可以使用普通的 <img> 标签。

```
<img src="http://static.runoob.com/images/runoob-logo.png" width="50%">
```

## 8.2. 高级显示图片

&emsp;&emsp; 用base64转码工具把图片转成一段字符串，然后把字符串填到基础格式中链接的那个位置。

* 基础用法：

&emsp;&emsp; 这个时候会发现插入的这一长串字符串会把整个文章分割开，非常影响编写文章时的体验。如果能够把大段的base64字符串放在文章末尾，然后在文章中通过一个id来调用，文章就不会被分割的这么乱了。
* 高级用法

```
![avatar][base64str]
[base64str]:data:image/png;base64,iVBORw0......
```

# 9. 表格

## 9.1. 一般使用

&emsp;&emsp; Markdown 制作表格使用 | 来分隔不同的单元格，使用 - 来分隔表头和其他行。

&emsp;&emsp; 语法格式如下：

```
|  表头   | 表头  |
|  ----  | ----  |
| 单元格  | 单元格 |
| 单元格  | 单元格 |
```

&emsp;&emsp; 以上代码显示结果如下：

|  表头   | 表头  |
|  ----  | ----  |
| 单元格  | 单元格 |
| 单元格  | 单元格 |

## 9.2. 对齐方式

&emsp;&emsp; 我们可以设置表格的对齐方式：

* -: 设置内容和标题栏居右对齐。
* :- 设置内容和标题栏居左对齐。
* :-: 设置内容和标题栏居中对齐。

&emsp;&emsp; 实例如下：

```
| 左对齐 | 右对齐 | 居中对齐 |
| :-----| ----: | :----: |
| 单元格 | 单元格 | 单元格 |
| 单元格 | 单元格 | 单元格 |
```

&emsp;&emsp; 以上代码显示结果如下：

| 左对齐 | 右对齐 | 居中对齐 |
| :-----| ----: | :----: |
| 单元格 | 单元格 | 单元格 |
| 单元格 | 单元格 | 单元格 |

# 10. 高级技巧

## 10.1. 支持的 HTML 元素

&emsp;&emsp; 不在 Markdown 涵盖范围之内的标签，都可以直接在文档里面用 HTML 撰写。

&emsp;&emsp; 目前支持的 HTML 元素有：<kbd> <b> <i> <em> <sup> <sub> <br>等 ，如：

```
使用 <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>Del</kbd> 重启电脑
```

&emsp;&emsp; 输出结果为：

使用 <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>Del</kbd> 重启电脑

## 10.2. 转义
&emsp;&emsp; Markdown 使用了很多特殊符号来表示特定的意义，如果需要显示特定的符号则需要使用转义字符，Markdown 使用反斜杠转义特殊字符：

```
**文本加粗** 
\*\* 正常显示星号 \*\*
```

&emsp;&emsp; 输出结果为：

**文本加粗** 
\*\* 正常显示星号 \*\*

&emsp;&emsp; Markdown 支持以下这些符号前面加上反斜杠来帮助插入普通的符号：

```
\   反斜线
`   反引号
*   星号
_   下划线
{}  花括号
[]  方括号
()  小括号
/#   井字号
+   加号
-   减号
.   英文句点
!   感叹号
```

## 10.3. 公式

&emsp;&emsp; 当你需要在编辑器中插入数学公式时，可以使用两个美元符 $$ 包裹TeX 或 LaTeX格式的数学公式来实现。提交后，问答和文章页会根据需要加载 Mathjax 对数学公式进行渲染。如：

```
$$
\mathbf{V}_1 \times \mathbf{V}_2 =  \begin{vmatrix} 
\mathbf{i} & \mathbf{j} & \mathbf{k} \\
\frac{\partial X}{\partial u} &  \frac{\partial Y}{\partial u} & 0 \\
\frac{\partial X}{\partial v} &  \frac{\partial Y}{\partial v} & 0 \\
\end{vmatrix}
{\style{visibility:hidden}{(x+1)(x+1)}}
$$
```

&emsp;&emsp; 输出结果为：

$$
\mathbf{V}_1 \times \mathbf{V}_2 =  \begin{vmatrix} 
\mathbf{i} & \mathbf{j} & \mathbf{k} \\
\frac{\partial X}{\partial u} &  \frac{\partial Y}{\partial u} & 0 \\
\frac{\partial X}{\partial v} &  \frac{\partial Y}{\partial v} & 0 \\
\end{vmatrix}
{\style{visibility:hidden}{(x+1)(x+1)}}
$$


## 10.4. UML（目前GitLab支持）

### 10.4.1. 流程图

#### 10.4.1.1. 横向流程图

&emsp;&emsp; 源码格式：

\`\`\`mermaid
graph LR
A[方形] -->B(圆角)
    B --> C{条件a}
    C -->|a=1| D[结果1]
    C -->|a=2| E[结果2]
    F[横向流程图]
\`\`\`

&emsp;&emsp; 显示效果如下：

```mermaid
graph LR
A[方形] -->B(圆角)
    B --> C{条件a}
    C -->|a=1| D[结果1]
    C -->|a=2| E[结果2]
    F[横向流程图]
```

#### 10.4.1.2. 竖向流程图

&emsp;&emsp; 源码格式：

\`\`\`mermaid
graph TD
A[方形] --> B(圆角)
    B --> C{条件a}
    C --> |a=1| D[结果1]
    C --> |a=2| E[结果2]
    F[竖向流程图]
\`\`\`

&emsp;&emsp; 显示效果如下：

```mermaid
graph TD
A[方形] --> B(圆角)
    B --> C{条件a}
    C --> |a=1| D[结果1]
    C --> |a=2| E[结果2]
    F[竖向流程图]
```

#### 10.4.1.3. 标准流程图

&emsp;&emsp; 源码格式：

\`\`\`flow
st=>start: 开始框
op=>operation: 处理框
cond=>condition: 判断框(是或否?)
sub1=>subroutine: 子流程
io=>inputoutput: 输入输出框
e=>end: 结束框
st->op->cond
cond(yes)->io->e
cond(no)->sub1(right)->op
\`\`\`

&emsp;&emsp; 显示效果如下：

```flow
st=>start: 开始框
op=>operation: 处理框
cond=>condition: 判断框(是或否?)
sub1=>subroutine: 子流程
io=>inputoutput: 输入输出框
e=>end: 结束框
st->op->cond
cond(yes)->io->e
cond(no)->sub1(right)->op
```

#### 10.4.1.4. 标准流程图（横向）

&emsp;&emsp; 源码格式：

\`\`\`flow
st=>start: 开始框
op=>operation: 处理框
cond=>condition: 判断框(是或否?)
sub1=>subroutine: 子流程
io=>inputoutput: 输入输出框
e=>end: 结束框
st(right)->op(right)->cond
cond(yes)->io(bottom)->e
cond(no)->sub1(right)->op
\`\`\`

&emsp;&emsp; 显示效果如下：

```flow
st=>start: 开始框
op=>operation: 处理框
cond=>condition: 判断框(是或否?)
sub1=>subroutine: 子流程
io=>inputoutput: 输入输出框
e=>end: 结束框
st(right)->op(right)->cond
cond(yes)->io(bottom)->e
cond(no)->sub1(right)->op
```

### 10.4.2. 时序图

#### 10.4.2.1. UML时序图源码样例

&emsp;&emsp; 源码格式：

\`\`\`sequence
对象A->对象B: 对象B你好吗?（请求）
Note right of 对象B: 对象B的描述
Note left of 对象A: 对象A的描述(提示)
对象B-->对象A: 我很好(响应)
对象A->对象B: 你真的好吗？
\`\`\`

&emsp;&emsp; 显示效果如下：

```sequence
对象A->对象B: 对象B你好吗?（请求）
Note right of 对象B: 对象B的描述
Note left of 对象A: 对象A的描述(提示)
对象B-->对象A: 我很好(响应)
对象A->对象B: 你真的好吗？
```

#### 10.4.2.2. UML时序图源码复杂样例

&emsp;&emsp; 源码格式：

\`\`\`sequence
Title: 标题：复杂使用
对象A->对象B: 对象B你好吗?（请求）
Note right of 对象B: 对象B的描述
Note left of 对象A: 对象A的描述(提示)
对象B-->对象A: 我很好(响应)
对象B->小三: 你好吗
小三-->>对象A: 对象B找我了
对象A->对象B: 你真的好吗？
Note over 小三,对象B: 我们是朋友
participant C
Note right of C: 没人陪我玩
\`\`\`

&emsp;&emsp; 显示效果如下：

```sequence
Title: 标题：复杂使用
对象A->对象B: 对象B你好吗?（请求）
Note right of 对象B: 对象B的描述
Note left of 对象A: 对象A的描述(提示)
对象B-->对象A: 我很好(响应)
对象B->小三: 你好吗
小三-->>对象A: 对象B找我了
对象A->对象B: 你真的好吗？
Note over 小三,对象B: 我们是朋友
participant C
Note right of C: 没人陪我玩
```


#### 10.4.2.3. UML标准时序图样例

&emsp;&emsp; 源码格式：

\`\`\`mermaid
%% 时序图例子,-> 直线，-->虚线，->>实线箭头
  sequenceDiagram
    participant 张三
    participant 李四
    张三->王五: 王五你好吗？
    loop 健康检查
        王五->王五: 与疾病战斗
    end
    Note right of 王五: 合理 食物 <br/>看医生...
    李四-->>张三: 很好!
    王五->李四: 你怎么样?
    李四-->王五: 很好!
\`\`\`

&emsp;&emsp; 显示效果如下：

```mermaid
%% 时序图例子,-> 直线，-->虚线，->>实线箭头
  sequenceDiagram
    participant 张三
    participant 李四
    张三->王五: 王五你好吗？
    loop 健康检查
        王五->王五: 与疾病战斗
    end
    Note right of 王五: 合理 食物 <br/>看医生...
    李四-->>张三: 很好!
    王五->李四: 你怎么样?
    李四-->王五: 很好!
```


### 10.4.3. 甘特图

#### 10.4.3.1. 甘特图样例

&emsp;&emsp; 源码格式：

\`\`\`mermaid
%% 语法示例
        gantt
        dateFormat  YYYY-MM-DD
        title 软件开发甘特图
        section 设计
        需求                      :done,    des1, 2014-01-06,2014-01-08
        原型                      :active,  des2, 2014-01-09, 3d
        UI设计                     :         des3, after des2, 5d
    未来任务                     :         des4, after des3, 5d
        section 开发
        学习准备理解需求                      :crit, done, 2014-01-06,24h
        设计框架                             :crit, done, after des2, 2d
        开发                                 :crit, active, 3d
        未来任务                              :crit, 5d
        耍                                   :2d
        section 测试
        功能测试                              :active, a1, after des3, 3d
        压力测试                               :after a1  , 20h
        测试报告                               : 48h
\`\`\`

&emsp;&emsp; 显示效果如下：

```mermaid
%% 语法示例
        gantt
        dateFormat  YYYY-MM-DD
        title 软件开发甘特图
        section 设计
        需求                      :done,    des1, 2014-01-06,2014-01-08
        原型                      :active,  des2, 2014-01-09, 3d
        UI设计                     :         des3, after des2, 5d
    未来任务                     :         des4, after des3, 5d
        section 开发
        学习准备理解需求                      :crit, done, 2014-01-06,24h
        设计框架                             :crit, done, after des2, 2d
        开发                                 :crit, active, 3d
        未来任务                              :crit, 5d
        耍                                   :2d
        section 测试
        功能测试                              :active, a1, after des3, 3d
        压力测试                               :after a1  , 20h
        测试报告                               : 48h
```

# 11. 利用VS Code在Markdown上自动添加和删除多级标题数字

## 11.1. 准备

* VSCode编辑器
* Markdown文件

## 11.2. 简介

1. 在电脑上安装VSCode，在VSCode中打开要添加动态标题序号的Markdown文件。
2. 在应用市场安装名为**Markdown Header**的插件。
3. 在VSCode按**F1**或**Ctrl+Shift+P(俗称万能键)**打开命令面板（在Mac上的快捷键是**Command+Shift+P**）。
4. 在命令面板输入**Markdown generate header number** --> 生成多级标题，输入**Markdown remove header number** --> 移除多级标题。

# 12. 相关笔记

## 12.1. Markdown的父目录，上一级目录

```text
[搜索](./../../Server/Linux/Linux.md#211-vmware-fusion安装centos-8，配置静态网关（mac）)

上面是一个相对路径跳转例子，可以得到：
当前目录是./
上一级目录是./../
上上以及目录是./../../
以此类推。。。

'#'可以访问具体的章节和锚点，这样的锚点用h6标签标记，标题改变

```



