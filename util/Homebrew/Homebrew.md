<h2>目录</h2>

  <a href="#1-最全面的安装repository">1. 最全面的安装Repository</a><br>
  <a href="#2-修改了哪些文件">2. 修改了哪些文件</a><br>
  <a href="#3-安装">3. 安装</a><br>
  <a href="#4-卸载">4. 卸载</a><br>


<h1>Homebrew-Mac的包管理工具</h1>

# 1. 最全面的安装Repository

大佬的：

https://gitee.com/cunkai/HomebrewCN

https://gitee.com/cunkai/homebrew-services

我fork大佬的（可能随时更新）：

https://gitee.com/yanglin5260/HomebrewCN

https://gitee.com/yanglin5260/homebrew-services

# 2. 修改了哪些文件

下载了HomebrewCN中的关键文件，并修改了其中的内容，主要是修改成了从国内清华镜像下载资源：

[HomebrewInstall-Tsinghua.sh](./HomebrewInstall-Tsinghua.sh)

[HomebrewUninstall.sh](./HomebrewUninstall.sh)

# 3. 安装

在当前文件夹下通过命令终端执行**安装**，安装中可能需要输入很多次密码，并且下载时间较长（因为我选用了其中比较全面的安装），安装完成后不需要运行`source /Users/yanglin/.zprofile`命令（因为我已经修改在了脚本当中），**安装完成后使用`brew -v`命令可以验证是否安装成功**：

```
/bin/zsh -c ./HomebrewInstall-Tsinghua.sh 或者 ./HomebrewInstall-Tsinghua.sh
```

# 4. 卸载

在当前文件夹下通过命令终端执行**卸载**：

```
/bin/zsh -c ./HomebrewUninstall.sh
```



