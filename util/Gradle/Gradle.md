<h2>目录</h2>

  <a href="#1-gradle-简介">1. Gradle 简介</a><br>
<details open>
  <summary><a href="#2-gradle-概述">2. Gradle 概述</a></summary>
  <ul>
  <details open>
    <summary><a href="#21-特性说明">2.1. 特性说明</a>  </summary>
    <ul>
      <a href="#211-基于声明的构建和基于约定的构建">2.1.1. 基于声明的构建和基于约定的构建</a><br>
      <a href="#212-为以依赖为基础的编程方式提供语言支持">2.1.2. 为以依赖为基础的编程方式提供语言支持</a><br>
      <a href="#213-构建结构化">2.1.3. 构建结构化</a><br>
      <a href="#214-深度-api">2.1.4. 深度 API</a><br>
      <a href="#215-gradle-的扩展">2.1.5. Gradle 的扩展</a><br>
      <a href="#216-多项目构建">2.1.6. 多项目构建</a><br>
      <a href="#217-多种方式管理依赖">2.1.7. 多种方式管理依赖</a><br>
      <a href="#218-gradle-是第一个构建集成工具">2.1.8. Gradle 是第一个构建集成工具</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#22-易于移植">2.2. 易于移植</a>  </summary>
    <ul>
      <a href="#221-groovy">2.2.1. Groovy</a><br>
      <a href="#222-the-gradle-wrapper">2.2.2. The Gradle wrapper</a><br>
      <a href="#223-自由和开源">2.2.3. 自由和开源</a><br>
    </ul>
  </details>
    <a href="#23-为什么使用-groovy">2.3. 为什么使用 Groovy?</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#3-gradle-构建基础">3. Gradle 构建基础</a></summary>
  <ul>
    <a href="#31-projects-和-tasks">3.1. Projects 和 tasks</a><br>
  <details open>
    <summary><a href="#32-hello-world">3.2. Hello world</a>  </summary>
    <ul>
      <a href="#321-第一个构建脚本">3.2.1. 第一个构建脚本</a><br>
      <a href="#322-执行脚本">3.2.2. 执行脚本</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#33-快速定义任务">3.3. 快速定义任务</a>  </summary>
    <ul>
      <a href="#331-快速定义任务">3.3.1. 快速定义任务</a><br>
      <a href="#332-代码即脚本">3.3.2. 代码即脚本</a><br>
      <a href="#333-在-gradle-任务中采用-groovy">3.3.3. 在 gradle 任务中采用 groovy</a><br>
      <a href="#334-在-gradle-任务中采用-groovy">3.3.4. 在 gradle 任务中采用 groovy</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#34-任务依赖">3.4. 任务依赖</a>  </summary>
    <ul>
      <a href="#341-在两个任务之间指明依赖关系">3.4.1. 在两个任务之间指明依赖关系</a><br>
    </ul>
  </details>
    <a href="#35-延迟依赖">3.5. 延迟依赖</a><br>
  <details open>
    <summary><a href="#36-动态任务">3.6. 动态任务</a>  </summary>
    <ul>
      <a href="#361-创建动态任务">3.6.1. 创建动态任务</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#37-任务操纵">3.7. 任务操纵</a>  </summary>
    <ul>
      <a href="#371-通过-api-进行任务之间的通信---增加依赖">3.7.1. 通过 API 进行任务之间的通信 - 增加依赖</a><br>
      <a href="#372-通过-api-进行任务之间的通信---增加任务行为">3.7.2. 通过 API 进行任务之间的通信 - 增加任务行为</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#38-短标记法">3.8. 短标记法</a>  </summary>
    <ul>
      <a href="#381-以属性的方式访问任务">3.8.1. 以属性的方式访问任务</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#39-增加自定义属性">3.9. 增加自定义属性</a>  </summary>
    <ul>
      <a href="#391-为任务增加自定义属性">3.9.1. 为任务增加自定义属性</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#310-调用-ant-任务">3.10. 调用 Ant 任务</a>  </summary>
    <ul>
      <a href="#3101-利用-antbuilder-执行-antloadfile">3.10.1. 利用 AntBuilder 执行 ant.loadfile</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#311-方法抽取">3.11. 方法抽取</a>  </summary>
    <ul>
      <a href="#3111-利用方法组织脚本逻辑">3.11.1. 利用方法组织脚本逻辑</a><br>
    </ul>
  </details>
    <a href="#312-定义默认任务">3.12. 定义默认任务</a><br>
    <a href="#313-定义默认任务">3.13. 定义默认任务</a><br>
  <details open>
    <summary><a href="#314-configure-by-dag">3.14. Configure by DAG</a>  </summary>
    <ul>
      <a href="#3141-依赖任务的不同输出">3.14.1. 依赖任务的不同输出</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#4-gradle-java-构建入门">4. Gradle Java 构建入门</a></summary>
  <ul>
    <a href="#41-java-插件">4.1. Java 插件</a><br>
  <details open>
    <summary><a href="#42-一个基本-java-项目">4.2. 一个基本 Java 项目</a>  </summary>
    <ul>
      <a href="#421-采用-java-插件">4.2.1. 采用 Java 插件</a><br>
      <a href="#422-构建项目">4.2.2. 构建项目</a><br>
    <details open>
      <summary><a href="#423-构建-java-项目">4.2.3. 构建 Java 项目</a>    </summary>
      <ul>
        <a href="#4231-clean">4.2.3.1. clean</a><br>
        <a href="#4232-assemble">4.2.3.2. assemble</a><br>
        <a href="#4233-check">4.2.3.3. check</a><br>
      </ul>
    </details>
      <a href="#424-外部依赖">4.2.4. 外部依赖</a><br>
      <a href="#425-添加-maven-仓库">4.2.5. 添加 Maven 仓库</a><br>
      <a href="#426-添加依赖">4.2.6. 添加依赖</a><br>
      <a href="#427-自定义项目">4.2.7. 自定义项目</a><br>
      <a href="#428-自定义-manifestmf">4.2.8. 自定义 MANIFEST.MF</a><br>
      <a href="#429-为-test-添加系统属性">4.2.9. 为 test 添加系统属性</a><br>
      <a href="#4210-发布-jar-包">4.2.10. 发布 jar 包</a><br>
      <a href="#4211-发布-jar-包">4.2.11. 发布 jar 包</a><br>
      <a href="#4212-创建-eclipse-文件">4.2.12. 创建 Eclipse 文件</a><br>
      <a href="#4213-eclipse-plugin">4.2.13. Eclipse plugin</a><br>
      <a href="#4214-示例汇总">4.2.14. 示例汇总</a><br>
      <a href="#4215-java-示例---一个完整构建脚本">4.2.15. Java 示例 - 一个完整构建脚本</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#43-多项目构建">4.3. 多项目构建</a>  </summary>
    <ul>
      <a href="#431-多项目构建-项目结构">4.3.1. 多项目构建-项目结构</a><br>
      <a href="#432-多项目构建定义">4.3.2. 多项目构建定义</a><br>
      <a href="#433-多项目构建中的-settingsgradle">4.3.3. 多项目构建中的 settings.gradle</a><br>
      <a href="#434-公共配置">4.3.4. 公共配置</a><br>
      <a href="#435-多项目构建-公共配置">4.3.5. 多项目构建-公共配置</a><br>
      <a href="#436-多项目构建-工程依赖">4.3.6. 多项目构建-工程依赖</a><br>
      <a href="#437-多项目构建-发布">4.3.7. 多项目构建-发布</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#5-gradle-依赖管理基础">5. Gradle 依赖管理基础</a></summary>
  <ul>
    <a href="#51-什么是依赖管理">5.1. 什么是依赖管理?</a><br>
  <details open>
    <summary><a href="#52-依赖声明">5.2. 依赖声明</a>  </summary>
    <ul>
      <a href="#521-声明依赖">5.2.1. 声明依赖</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#53-依赖配置">5.3. 依赖配置</a>  </summary>
    <ul>
      <a href="#531-compile">5.3.1. compile</a><br>
      <a href="#532-runtime">5.3.2. runtime</a><br>
      <a href="#533-testcompile">5.3.3. testCompile</a><br>
      <a href="#534-testruntime">5.3.4. testRuntime</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#54-外部依赖">5.4. 外部依赖</a>  </summary>
    <ul>
      <a href="#541-定义外部依赖">5.4.1. 定义外部依赖</a><br>
      <a href="#542-快速定义外部依赖">5.4.2. 快速定义外部依赖</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#55-仓库">5.5. 仓库</a>  </summary>
    <ul>
      <a href="#551-使用-maven-中央仓库">5.5.1. 使用 Maven 中央仓库</a><br>
      <a href="#552-使用-maven-远程仓库">5.5.2. 使用 Maven 远程仓库</a><br>
      <a href="#553-采用-ivy-远程仓库">5.5.3. 采用 Ivy 远程仓库</a><br>
      <a href="#554-采用本地-ivy-目录">5.5.4. 采用本地 Ivy 目录</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#56-打包发布">5.6. 打包发布</a>  </summary>
    <ul>
      <a href="#561-发布到-ivy-仓库">5.6.1. 发布到 Ivy 仓库</a><br>
      <a href="#562-发布到-maven-仓库">5.6.2. 发布到 Maven 仓库</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#6-gradle-groovy-快速入门">6. Gradle Groovy 快速入门</a></summary>
  <ul>
    <a href="#61-一个基本的-groovy-项目">6.1. 一个基本的 Groovy 项目</a><br>
    <a href="#62-总结">6.2. 总结</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#7-gradle-web-工程构建">7. Gradle Web 工程构建</a></summary>
  <ul>
  <details open>
    <summary><a href="#71-打-war-包">7.1. 打 War 包</a>  </summary>
    <ul>
      <a href="#711-war-plugin">7.1.1. War plugin</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#72-web-工程启动">7.2. Web 工程启动</a>  </summary>
    <ul>
      <a href="#721-采用-jetty-plugin-启动-web-工程">7.2.1. 采用 Jetty plugin 启动 web 工程</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#8-gradle-gradle-命令行的基本使用">8. Gradle Gradle 命令行的基本使用</a></summary>
  <ul>
  <details open>
    <summary><a href="#81-多任务调用">8.1. 多任务调用</a>  </summary>
    <ul>
      <a href="#811-任务依赖">8.1.1. 任务依赖</a><br>
      <a href="#812-多任务调用">8.1.2. 多任务调用</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#82-排除任务">8.2. 排除任务</a>  </summary>
    <ul>
      <a href="#821-排除任务">8.2.1. 排除任务</a><br>
    </ul>
  </details>
    <a href="#83-失败后继续执行">8.3. 失败后继续执行</a><br>
  <details open>
    <summary><a href="#84-简化任务名">8.4. 简化任务名</a>  </summary>
    <ul>
      <a href="#841-简化任务名">8.4.1. 简化任务名</a><br>
      <a href="#842-简化驼峰任务名">8.4.2. 简化驼峰任务名</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#85-选择构建位置">8.5. 选择构建位置</a>  </summary>
    <ul>
      <a href="#851-选择文件构建">8.5.1. 选择文件构建</a><br>
      <a href="#852-选择构建目录">8.5.2. 选择构建目录</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#86-获取构建信息">8.6. 获取构建信息</a>  </summary>
    <ul>
      <a href="#861-项目列表">8.6.1. 项目列表</a><br>
      <a href="#862-收集项目信息">8.6.2. 收集项目信息</a><br>
      <a href="#863-为项目添加描述信息">8.6.3. 为项目添加描述信息.</a><br>
      <a href="#864-任务列表">8.6.4. 任务列表</a><br>
      <a href="#865-获取任务信息">8.6.5. 获取任务信息</a><br>
      <a href="#866-更改任务报告内容">8.6.6. 更改任务报告内容</a><br>
      <a href="#867-obtaining-more-information-about-tasks">8.6.7. Obtaining more information about tasks</a><br>
      <a href="#868-获取任务帮助信息">8.6.8. 获取任务帮助信息</a><br>
      <a href="#869-获取任务帮助">8.6.9. 获取任务帮助</a><br>
      <a href="#8610-获取依赖列表">8.6.10. 获取依赖列表</a><br>
      <a href="#8611-获取依赖信息">8.6.11. 获取依赖信息</a><br>
      <a href="#8612-过滤依赖信息">8.6.12. 过滤依赖信息</a><br>
      <a href="#8613-查看特定依赖">8.6.13. 查看特定依赖</a><br>
      <a href="#8614-获取特定依赖">8.6.14. 获取特定依赖</a><br>
      <a href="#8615-获取项目属性列表">8.6.15. 获取项目属性列表</a><br>
      <a href="#8616-属性信息">8.6.16. 属性信息</a><br>
      <a href="#8617-构建日志">8.6.17. 构建日志</a><br>
    </ul>
  </details>
    <a href="#87-dry-run">8.7. Dry Run</a><br>
  </ul>
</details>
  <a href="#9-gradle-编写构建脚本">9. Gradle 编写构建脚本</a><br>




<h1>Gradle</h1>

# 1. Gradle 简介

很高兴能向大家介绍 Gradle，这是一个基于 JVM 的富有突破性构建工具。

它为您提供了:

- 一个像 ant 一样，通用的灵活的构建工具
- 一种可切换的，像 maven 一样的基于约定约定优于配置的构建框架
- 强大的多工程构建支持
- 强大的依赖管理(基于ApacheIvy)
- 对已有的 maven 和 ivy 仓库的全面支持
- 支持传递性依赖管理，而不需要远程仓库或者 pom.xml 或者 ivy 配置文件
- ant 式的任务和构建是 gradle 的第一公民
- 基于 groovy，其 build 脚本使用 groovy dsl 编写
- 具有广泛的领域模型支持你的构建

# 2. Gradle 概述

## 2.1. 特性说明

下面是一些 Gradle 特性的列表。

### 2.1.1. 基于声明的构建和基于约定的构建

Gradle 的核心在于基于 Groovy 的丰富而可扩展的域描述语言(DSL)。 Groovy 通过声明性的语言元素将基于声明的构建推向下层，你可以按你想要的方式进行组合。 这些元素同样也为支持 Java， Groovy，OSGi，Web 和 Scala 项目提供了基于约定的构建。 并且，这种声明性的语言是可以扩展的。你可以添加新的或增强现有的语言元素。 因此，它提供了简明、可维护和易理解的构建。

### 2.1.2. 为以依赖为基础的编程方式提供语言支持

声明性语言优点在于通用任务图，你可以将其充分利用在构建中. 它提供了最大限度的灵活性，以让 Gradle 适应你的特殊需求。

### 2.1.3. 构建结构化

Gradle 的灵活和丰富性最终能够支持在你的构建中应用通用的设计模式。 例如，它可以很容易地将你的构建拆分为多个可重用的模块，最后再进行组装，但不要强制地进行模块的拆分。 不要把原本在一起的东西强行分开（比如在你的项目结构里），从而避免让你的构建变成一场噩梦。 最后，你可以创建一个结构良好，易于维护，易于理解的构建。

### 2.1.4. 深度 API

Gradle 允许你在构建执行的整个生命周期，对它的核心配置及执行行为进行监视并自定义。

### 2.1.5. Gradle 的扩展

Gradle 有非常良好的扩展性。 从简单的单项目构建，到庞大的多项目构建，它都能显著地提升你的效率。 这才是真正的结构化构建。通过最先进的增量构建功能，它可以解决许多大型企业所面临的性能瓶颈问题。

### 2.1.6. 多项目构建

Gradle 对多项目构建的支持非常出色。项目依赖是首先需要考虑的问题。 我们允许你在多项目构建当中对项目依赖关系进行建模，因为它们才是你真正的问题域。 Gradle 遵守你的布局。

Gradle 提供了局部构建的功能。 如果你在构建一个单独的子项目，Gradle 也会帮你构建它所依赖的所有子项目。 你也可以选择重新构建依赖于特定子项目的子项目。 这种增量构建将使得在大型构建任务中省下大量时间。

### 2.1.7. 多种方式管理依赖

不同的团队喜欢用不同的方式来管理他们的外部依赖。 从 Maven 和 Ivy 的远程仓库的传递依赖管理，到本地文件系统的 jar 包或目录，Gradle 对所有的管理策略都提供了方便的支持。

### 2.1.8. Gradle 是第一个构建集成工具

Ant tasks 是最重要的。而更有趣的是，Ant projects 也是最重要的。 Gradle 对任意的 Ant 项目提供了深度导入，并在运行时将 Ant 目标(target)转换为原生的 Gradle 任务(task)。 你可以从 Gradle 上依赖它们(Ant targets)，增强它们，甚至在你的 build.xml 上定义对 Gradle tasks 的依赖。Gradle 为属性、路径等等提供了同样的整合。

Gradle 完全支持用于发布或检索依赖的 Maven 或 Ivy 仓库。 Gradle 同样提供了一个转换器，用于将一个 Maven pom.xml 文件转换为一个 Gradle 脚本。Maven 项目的运行时导入的功能将很快会有。

## 2.2. 易于移植

Gradle 能适应你已有的任何结构。因此，你总可以在你构建项目的同一个分支当中开发你的 Gradle 构建脚本，并且它们能够并行进行。 我们通常建议编写测试，以保证生成的文件是一样的。 这种移植方式会尽可能的可靠和减少破坏性。这也是重构的最佳做法。

### 2.2.1. Groovy

Gradle 的构建脚本是采用 Groovy 写的，而不是用 XML。 但与其他方法不同，它并不只是展示了由一种动态语言编写的原始脚本的强大。 那样将导致维护构建变得很困难。 Gradle 的整体设计是面向被作为一门语言，而不是一个僵化的框架。 并且 Groovy 是我们允许你通过抽象的 Gradle 描述你个人的 story 的黏合剂。 Gradle 提供了一些标准通用的 story。这是我们相比其他声明性构建系统的主要特点。 我们的 Groovy 支持也不是简单的糖衣层，整个 Gradle 的 API 都是完全 groovy 化的。只有通过 Groovy才能去运用它并对它提高效率。

### 2.2.2. The Gradle wrapper

Gradle Wrapper 允许你在没有安装 Gradle 的机器上执行 Gradle 构建。 这一点是非常有用的。比如，对一些持续集成服务来说。 它对一个开源项目保持低门槛构建也是非常有用的。 Wrapper 对企业来说也很有用，它使得对客户端计算机零配置。 它强制使用指定的版本，以减少兼容支持问题。

### 2.2.3. 自由和开源

Gradle 是一个开源项目，并遵循 ASL 许可。

## 2.3. 为什么使用 Groovy?

我们认为内部 DSL（基于一种动态语言）相比 XML 在构建脚本方面优势非常大。它们是一对动态语言。 为什么使用 Groovy？答案在于 Gradle 内部的运行环境。 虽然 Gradle 核心目的是作为通用构建工具，但它还是主要面向 Java 项目。 这些项目的团队成员显然熟悉 Java。我们认为一个构建工具应该尽可能地对所有团队成员透明。

你可能会想说，为什么不能使用 Java 来作为构建脚本的语言。 我认为这是一个很有意义的问题。对你们的团队来讲，它确实会有最高的透明度和最低的学习曲线。 但由于 Java 本身的局限性，这种构建语言可能就不会那样友善、 富有表现力和强大。 [1] 这也是为什么像 Python，Groovy 或者 Ruby 这样的语言在这方面表现得更好的原因。 我们选择了 Groovy，因为它向 Java 人员提供了目前为止最大的透明度。 其基本的语法，类型，包结构和其他方面都与 Java 一样，Groovy 在这之上又增加了许多东西。但是和 Java 也有着共同点。

对于那些分享和乐于去学习 Python 知识的 Java 团队而言，上述论点并不适用。 Gradle 的设计非常适合在 JRuby 或 Jython 中创建另一个构建脚本引擎。 那时候，对我们而言，它只是不再是最高优先级的了。我们很高兴去支持任何社区努力创建其他的构建脚本引擎。

# 3. Gradle 构建基础

## 3.1. Projects 和 tasks

projects 和 tasks是 Gradle 中最重要的两个概念。

任何一个 Gradle 构建都是由一个或多个 projects 组成。每个 project 包括许多可构建组成部分。 这完全取决于你要构建些什么。一个 project 不必描述它只能进行构建操作。它也可以部署你的应用或搭建你的环境。不要担心它像听上去的那样庞大。 Gradle 的 build-by-convention 可以让您来具体定义一个 project 到底该做什么。

每个 project 都由多个 tasks 组成。每个 task 都代表了构建执行过程中的一个原子性操作。如编译，打包，生成 javadoc，发布到某个仓库等操作。

## 3.2. Hello world

你可以通过在命令行运行 gradle 命令来执行构建，gradle 命令会从当前目录下寻找 build.gradle 文件来执行构建。我们称 build.gradle 文件为构建脚本。严格来说这其实是一个构建配置脚本，后面你会了解到这个构建脚本定义了一个 project 和一些默认的 task。

你可以创建如下脚本到 build.gradle 中 To try this out，create the following build script named build.gradle。

### 3.2.1. 第一个构建脚本

build.gradle

```
task hello {
    doLast {
        println 'Hello world!'
    }
}
```

然后在该文件所在目录执行 gradle -q hello

> -q 参数的作用是什么?
>
> 该文档的示例中很多地方在调用 gradle 命令时都加了 -q 参数。该参数用来控制 gradle 的日志级别，可以保证只输出我们需要的内容。具体可参阅本文档第十八章日志来了解更多参数和信息。

### 3.2.2. 执行脚本

```
Output of gradle -q hello
> gradle -q hello
Hello world!
```

上面的脚本定义了一个叫做 hello 的 task，并且给它添加了一个动作。当执行 gradle hello 的时候, Gralde 便会去调用 hello 这个任务来执行给定操作。这些操作其实就是一个用 groovy 书写的闭包。

## 3.3. 快速定义任务

用一种更简洁的方式来定义上面的 hello 任务。

### 3.3.1. 快速定义任务

build.gradle

```
task hello {
    println 'Hello world!'
}
```

上面的脚本又一次采用闭包的方式来定义了一个叫做 hello 的任务，本文档后续章节中我们将会更多的采用这种风格来定义任务。

>  注：在Gradle4.x中被弃用，在Gradle 5.0中被移除，详情见：[Gradle 4.x官网](https://docs.gradle.org/4.0/release-notes.html#setting-the-compiler-executable-is-no-longer-deprecated)
>
> 例：task { println 'Hello world!'}  解决方法：直接去掉或使用doLast解决。

### 3.3.2. 代码即脚本

Gradle 脚本采用 Groovy 书写，作为开胃菜，看下下面这个例子。

### 3.3.3. 在 gradle 任务中采用 groovy

build.gradle

```
task upper << {
    String someString = 'mY_nAmE'
    println "Original: " + someString
    println "Upper case: " + someString.toUpperCase()
}
Output of gradle -q upper
> gradle -q upper
Original: mY_nAmE
Upper case: MY_NAME
```

或者

### 3.3.4. 在 gradle 任务中采用 groovy

build.gradle

```
task count << {
    4.times { print "$it " }
}
Output of gradle -q count
> gradle -q count
0 1 2 3
```

## 3.4. 任务依赖

你可以按如下方式创建任务间的依赖关系

### 3.4.1. 在两个任务之间指明依赖关系

build.gradle

```
task hello << {
    println 'Hello world!'
}
task intro(dependsOn: hello) << {
    println "I'm Gradle"
}
```

gradle -q intro 的输出结果

```
Output of gradle -q intro
\> gradle -q intro
Hello world!
I'm Gradle
```

添加依赖 task 也可以不必首先声明被依赖的 task。

## 3.5. 延迟依赖

build.gradle

```
task taskX(dependsOn: 'taskY') << {
    println 'taskX'
}
task taskY << {
    println 'taskY'
}
```

Output of gradle -q taskX

```
 \> gradle -q taskX
taskY
taskX
```

可以看到，taskX 是 在 taskY 之前定义的，这在多项目构建中非常有用。

**注意**：当引用的任务尚未定义的时候不可使用短标记法来运行任务。

## 3.6. 动态任务

借助 Groovy 的强大不仅可以定义简单任务还能做更多的事。例如，可以动态定义任务。

### 3.6.1. 创建动态任务

build.gradle

```
4.times { counter ->
    task "task$counter" << {
        println "I'm task number $counter"
    }
}
```

gradle -q task1 的输出结果。

```
\> gradle -q task1
I'm task number 1
```

## 3.7. 任务操纵

一旦任务被创建后，任务之间可以通过 API 进行相互访问。这也是与 Ant 的不同之处。比如可以增加一些依赖。

### 3.7.1. 通过 API 进行任务之间的通信 - 增加依赖

build.gradle

```
4.times { counter ->
    task "task$counter" << {
        println "I'm task number $counter"
    }
}
task0.dependsOn task2, task3
```

gradle -q task0的输出结果。

```
Output of gradle -q task0
\> gradle -q task0
I'm task number 2
I'm task number 3
I'm task number 0
```

为已存在的任务增加行为。

### 3.7.2. 通过 API 进行任务之间的通信 - 增加任务行为

build.gradle

```
task hello << {
    println 'Hello Earth'
}
hello.doFirst {
    println 'Hello Venus'
}
hello.doLast {
    println 'Hello Mars'
}
hello << {
    println 'Hello Jupiter'
}
Output of gradle -q hello
> gradle -q hello
Hello Venus
Hello Earth
Hello Mars
Hello Jupiter
```

doFirst 和 doLast 可以进行多次调用。他们分别被添加在任务的开头和结尾。当任务开始执行时这些动作会按照既定顺序进行。其中 << 操作符 是 doLast 的简写方式。

## 3.8. 短标记法

你早就注意到了吧，没错，每个任务都是一个脚本的属性，你可以访问它:

### 3.8.1. 以属性的方式访问任务

build.gradle

```
task hello << {
    println 'Hello world!'
}
hello.doLast {
    println "Greetings from the $hello.name task."
}
```

gradle -q hello 的输出结果

```
Output of gradle -q hello
\> gradle -q hello
Hello world!
Greetings from the hello task.
```

对于插件提供的内置任务。这尤其方便(例如:complie)

## 3.9. 增加自定义属性

你可以为一个任务添加额外的属性。例如，新增一个叫做 myProperty 的属性，用 ext.myProperty 的方式给他一个初始值。这样便增加了一个自定义属性。

### 3.9.1. 为任务增加自定义属性

build.gradle

```
task myTask {
    ext.myProperty = "myValue"
}

task printTaskProperties << {
    println myTask.myProperty
}
```

gradle -q printTaskProperties 的输出结果

```
Output of gradle -q printTaskProperties
\> gradle -q printTaskProperties
myValue
```

自定义属性不仅仅局限于任务上，还可以做更多事情。

## 3.10. 调用 Ant 任务

Ant 任务是 Gradle 中的一等公民。Gradle 借助 Groovy 对 Ant 任务进行了优秀的整合。Gradle 自带了一个 AntBuilder，在 Gradle 中调用 Ant 任务比在 build.xml 中调用更加的方便和强大。 通过下面的例子你可以学到如何调用一个 Ant 任务以及如何与 Ant 中的属性进行通信。

### 3.10.1. 利用 AntBuilder 执行 ant.loadfile

build.gradle

```
task loadfile << {
    def files = file('../antLoadfileResources').listFiles().sort()
    files.each { File file ->
        if (file.isFile()) {
            ant.loadfile(srcFile: file, property: file.name)
            println " *** $file.name ***"
            println "${ant.properties[file.name]}"
        }
    }
}
```

gradle -q loadfile 的输出结果

```
Output of gradle -q loadfile
\> gradle -q loadfile
*** agile.manifesto.txt ***
Individuals and interactions over processes and tools
Working software over comprehensive documentation
Customer collaboration  over contract negotiation
Responding to change over following a plan
 *** gradle.manifesto.txt ***
Make the impossible possible, make the possible easy and make the easy elegant.
(inspired by Moshe Feldenkrais)
```

在你脚本里还可以利用 Ant 做更多的事情。

## 3.11. 方法抽取

Gradle 的强大要看你如何编写脚本逻辑。针对上面的例子，首先要做的就是要抽取方法。

### 3.11.1. 利用方法组织脚本逻辑

build.gradle

```
task checksum << {
    fileList('../antLoadfileResources').each {File file ->
        ant.checksum(file: file, property: "cs_$file.name")
        println "$file.name Checksum: ${ant.properties["cs_$file.name"]}"
    }
}
task loadfile << {
    fileList('../antLoadfileResources').each {File file ->
        ant.loadfile(srcFile: file, property: file.name)
        println "I'm fond of $file.name"
    }
}
File[] fileList(String dir) {
    file(dir).listFiles({file -> file.isFile() } as FileFilter).sort()
}
```

gradle -q loadfile 的输出结果

```
Output of gradle -q loadfile
\> gradle -q loadfile
I'm fond of agile.manifesto.txt
I'm fond of gradle.manifesto.txt
```

在后面的章节你会看到类似出去出来的方法可以在多项目构建中的子项目中调用。无论构建逻辑多复杂，Gradle 都可以提供给你一种简便的方式来组织它们。

## 3.12. 定义默认任务

Gradle 允许在脚本中定义多个默认任务。

## 3.13. 定义默认任务

build.gradle

```
defaultTasks 'clean', 'run'
task clean << {
    println 'Default Cleaning!'
}
task run << {
    println 'Default Running!'
}
task other << {
    println "I'm not a default task!"
}
```

gradle -q 的输出结果。

```
Output of gradle -q
\> gradle -q
Default Cleaning!
Default Running!
```

这与直接调用 gradle clean run 效果是一样的。在多项目构建中，每个子项目都可以指定单独的默认任务。如果子项目未进行指定将会调用父项目指定的的默认任务。

## 3.14. Configure by DAG

稍后会对 Gradle 的配置阶段和运行阶段进行详细说明 配置阶段后，Gradle 会了解所有要执行的任务 Gradle 提供了一个钩子来捕获这些信息。一个例子就是可以检查已经执行的任务中有没有被释放。借由此，你可以为一些变量赋予不同的值。

在下面的例子中，为 distribution 和 release 任务赋予了不同的 version 值。

### 3.14.1. 依赖任务的不同输出

build.gradle

```
task distribution << {
    println "We build the zip with version=$version"
}
task release(dependsOn: 'distribution') << {
    println 'We release now'
}
gradle.taskGraph.whenReady {taskGraph ->
    if (taskGraph.hasTask(release)) {
        version = '1.0'
    } else {
        version = '1.0-SNAPSHOT'
    }
}
```

gradle -q distribution 的输出结果

```
Output of gradle -q distribution
\> gradle -q distribution
We build the zip with version=1.0-SNAPSHOT
```

gradle -q release 的输出结果

```
Output of gradle -q release
\> gradle -q release
We build the zip with version=1.0
We release now
```

whenReady 会在已发布的任务之前影响到已发布任务的执行。即使已发布的任务不是主要任务(也就是说，即使这个任务不是通过命令行直接调用)

# 4. Gradle Java 构建入门

## 4.1. Java 插件

如你所见，Gradle 是一个通用工具。它可以通过脚本构建任何你想要实现的东西，真正实现开箱即用。但前提是你需要在脚本中编写好代码才行。

大部分 Java 项目基本流程都是相似的：编译源文件，进行单元测试，创建 jar 包。使用 Gradle 做这些工作不必为每个工程都编写代码。Gradle 已经提供了完美的插件来解决这些问题。插件就是 Gradle 的扩展，简而言之就是为你添加一些非常有用的默认配置。Gradle 自带了很多插件，并且你也可以很容易的编写和分享自己的插件。

Java plugin 作为其中之一，为你提供了诸如编译，测试，打包等一些功能。

Java 插件为工程定义了许多默认值，如Java源文件位置。如果你遵循这些默认规则，那么你无需在你的脚本文件中书写太多代码。当然，Gradle 也允许你自定义项目中的一些规则，实际上，由于对 Java 工程的构建是基于插件的，那么你也可以完全不用插件自己编写代码来进行构建。

## 4.2. 一个基本 Java 项目

来看一下下面这个小例子，想用 Java 插件，只需增加如下代码到你的build.gradle脚本里。

### 4.2.1. 采用 Java 插件

```
apply plugin: 'java'
```

备注:示例代码可以在 Gralde 发行包中的 samples/java/quickstart 下找到。

定义一个 Java 项目只需如此而已。这将会为你添加 Java 插件及其一些内置任务。

> 添加了哪些任务?
>
> 你可以运行 gradle tasks 列出任务列表。这样便可以看到 Java 插件为你添加了哪些任务。

标准目录结构如下:

```
project  
    +build  
    +src/main/java  
    +src/main/resources  
    +src/test/java  
    +src/test/resources  
```

Gradle 默认会从 `src/main/java` 搜寻打包源码，在 `src/test/java` 下搜寻测试源码。并且 `src/main/resources` 下的所有文件按都会被打包，所有 `src/test/resources` 下的文件 都会被添加到类路径用以执行测试。所有文件都输出到 build 下，打包的文件输出到 build/libs 下。

### 4.2.2. 构建项目

Java 插件为你添加了众多任务。但是它们只是在你需要构建项目的时候才能发挥作用。最常用的就是 build 任务，这会构建整个项目。当你执行 gradle build 时，Gralde 会编译并执行单元测试，并且将 `src/main/*` 下面 class 和资源文件打包。

### 4.2.3. 构建 Java 项目

运行 gradle build 的输出结果

```
Output of gradle build
> gradle build
:compileJava
:processResources
:classes
:jar
:assemble
:compileTestJava
:processTestResources
:testClasses
:test
:check
:build
BUILD SUCCESSFUL
Total time: 1 secs
```

其余一些较常用的任务有如下几个:

#### 4.2.3.1. clean

删除 build 目录以及所有构建完成的文件。

#### 4.2.3.2. assemble

编译并打包 jar 文件，但不会执行单元测试。一些其他插件可能会增强这个任务的功能。例如，如果采用了 War 插件，这个任务便会为你的项目打出 War 包。

#### 4.2.3.3. check

编译并测试代码。一些其他插件也可能会增强这个任务的功能。例如，如果采用了 Code-quality 插件，这个任务会额外执行 Checkstyle。

### 4.2.4. 外部依赖

通常，一个 Java 项目拥有许多外部依赖。你需要告诉 Gradle 如何找到并引用这些外部文件。在 Gradle 中通常 Jar 包都存在于仓库中。仓库可以用来搜寻依赖或发布项目产物。下面是一个采用 Maven 仓库的例子。

### 4.2.5. 添加 Maven 仓库

```
build.gradle
repositories {
    mavenCentral()
}
```

### 4.2.6. 添加依赖

添加依赖。这里声明了编译期所需依赖 commons-collections 和测试期所需依赖 junit。

```
build.gradle
dependencies {
    compile group: 'commons-collections', name: 'commons-collections', version: '3.2'
    testCompile group: 'junit', name: 'junit', version: '4.+'
}
```

### 4.2.7. 自定义项目

Java 插件为你的项目添加了众多默认配置。这些默认值通常对于一个普通项目来说已经足够了。但如果你觉得不适用修改起来也很简单。看下面的例子，我们为 Java 项目指定了版本号以及所用的 JDK 版本，并且添加一些属性到 mainfest 中。

### 4.2.8. 自定义 MANIFEST.MF

```
build.gradle
sourceCompatibility = 1.5
version = '1.0'
jar {
    manifest {
        attributes 'Implementation-Title': 'Gradle Quickstart', 'Implementation-Version': version
    }
}
```

> 都有哪些可用属性?
>
> 可以执行 gradle properties来得到项目的属性列表。用这条命令可以看到插件添加的属性以及默认值。

Java 插件添加的都是一些普通任务，如同他们写在 Build 文件中一样。这意味着前面章节展示的机制都可以用来修改这些任务的行为。

### 4.2.9. 为 test 添加系统属性

```
test {
    systemProperties 'property': 'value'
}
```

### 4.2.10. 发布 jar 包

如何发布 jar 包？你需要告诉 Gradle 发布到到哪。在 Gradle 中 jar 包通常被发布到某个仓库中。在下面的例子中，我们会将 jar 包发布到本地目录。当然你也可以发布到远程仓库或多个远程仓库中。

### 4.2.11. 发布 jar 包

```
uploadArchives {
    repositories {
       flatDir {
           dirs 'repos'
       }
    }
}
```

执行 gradle uploadArchives 以发布 jar 包。

### 4.2.12. 创建 Eclipse 文件

若要把项目导入 Eclipse 中，你需要添加另外一个插件到你的脚本文件中。

### 4.2.13. Eclipse plugin

```
build.gradle
apply plugin: 'eclipse'
```

执行 gradle eclipse 来生成 Eclipse 项目文件。

### 4.2.14. 示例汇总

这是示例代码汇总得到的一个完整脚本：

### 4.2.15. Java 示例 - 一个完整构建脚本

```
build.gradle
apply plugin: 'java'
apply plugin: 'eclipse'
sourceCompatibility = 1.5
version = '1.0'
jar {
    manifest {
        attributes 'Implementation-Title': 'Gradle Quickstart', 'Implementation-Version': version
    }
}
repositories {
    mavenCentral()
}
dependencies {
    compile group: 'commons-collections', name: 'commons-collections', version: '3.2'
    testCompile group: 'junit', name: 'junit', version: '4.+'
}
test {
    systemProperties 'property': 'value'
}
uploadArchives {
    repositories {
       flatDir {
           dirs 'repos'
       }
    }
}
```

## 4.3. 多项目构建

现在来看一个典型的多项目构建的例子。项目结构如下：

### 4.3.1. 多项目构建-项目结构

```
Build layout
multiproject/
  api/
  services/webservice/
  shared/
备注: 本示例代码可在 Gradle 发行包的 samples/java/multiproject 位置找到
```

此处有三个工程。

- api 工程用来生成给客户端用的 jar 文件，这个 jar 文件可以为 XML webservice 提供 Java 客户端。
- webservice 是一个 web 应用，生成 XML。
- shared 工程包含的是前述两个工程共用的代码。

### 4.3.2. 多项目构建定义

定义一个多项目构建工程需要在根目录(本例中与 multiproject 同级)创建一个*setting* 配置文件来指明构建包含哪些项目。并且这个文件必需叫 settings.gradle 本例的配置文件如下:

### 4.3.3. 多项目构建中的 settings.gradle

```
settings.gradle
include "shared", "api", "services:webservice", "services:shared"
```

### 4.3.4. 公共配置

对多项目构建而言，总有一些共同的配置.在本例中，我们会在根项目上采用配置注入的方式定义一些公共配置。根项目就像一个容器，子项目会迭代访问它的配置并注入到自己的配置中。这样我们就可以简单的为所有工程定义主配置单了：

### 4.3.5. 多项目构建-公共配置

```
build.gradle
subprojects {
    apply plugin: 'java'
    apply plugin: 'eclipse-wtp'
    repositories {
       mavenCentral()
    }
    dependencies {
        testCompile 'junit:junit:4.11'
    }
    version = '1.0'
    jar {
        manifest.attributes provider: 'gradle'
    }
}
```

值得注意的是我们为每个子项目都应用了 Java 插件。这意味着我们在前面章节学习的内容在子项目中也都是可用的。所以你可以在根项目目录进行编译，测试，打包等所有操作。

### 4.3.6. 多项目构建-工程依赖

同一个构建中可以建立工程依赖，一个工程的 jar 包可以提供给另外一个工程使用。例如我们可以让 api 工程以依赖于 shared 工程的 jar 包。这样 Gradle 在构建 api 之前总是会先构建 shared 工程。

```
api/build.gradle
dependencies {
    compile project(':shared')
}
```

### 4.3.7. 多项目构建-发布

```
api/build.gradle
task dist(type: Zip) {
    dependsOn spiJar
    from 'src/dist'
    into('libs') {
        from spiJar.archivePath
        from configurations.runtime
    }
}
artifacts {
   archives dist
}
```

# 5. Gradle 依赖管理基础

## 5.1. 什么是依赖管理?

通俗来讲，依赖管理由如下两部分组成。首先，Gradle 需要知道项目构建或运行所需要的一些文件，以便于找到这些需要的文件。我们称这些输入的文件为项目的依赖。其次，你可能需要构建完成后自动上传到某个地方。我们称这些输出为发布。下面来仔细介绍一下这两部分：

大部分工程都不太可能完全自给自足，一般你都会用到其他工程的文件。比如我工程需要 Hibernate 就得把它的类库加进来，比如测试的时候可能需要某些额外 jar 包，例如 JDBC 驱动或 Ehcache 之类的 Jar 包。

这些文件就是工程的依赖。Gradle 需要你告诉它工程的依赖是什么，它们在哪，然后帮你加入构建中。依赖可能需要去远程库下载，比如 Maven 或者 Ivy 库。也可以是本地库，甚至可能是另一个工程。我们称这个过程叫*依赖解决*。

通常，依赖的自身也有依赖。例如，Hibernate 核心类库就依赖于一些其他的类库。所以，当 Gradle 构建你的工程时，会去找到这些依赖。我们称之为*依赖传递*。

大部分工程构建的主要目的是脱离工程使用。例如，生成 jar 包，包括源代码、文档等，然后发布出去。

这些输出的文件构成了项目的发布内容。Gralde 也会为你分担这些工作。你声明了发布到到哪，Gradle 就会发布到哪。“发布”的意思就是你想做什么。比如，复制到某个目录，上传到 Maven 或 Ivy 仓库。或者在其它项目里使用，这些都可以称之为*发行*。

## 5.2. 依赖声明

来看一下这个脚本里声明依赖的部分：

### 5.2.1. 声明依赖

build.gradle

```
apply plugin: 'java'
repositories {
    mavenCentral()
}
dependencies {
    compile group: 'org.hibernate', name: 'hibernate-core', version: '3.6.7.Final'
    testCompile group: 'junit', name: 'junit', version: '4.+'
}
```

这是什么意思呢？这段脚本是这么个意思。首先，Hibernate-core.3.6.7.final.jar 这货是编译期必需的依赖。并且这货相关的依赖也会一并被加载进来，该段脚本同时还声明项目测试阶段需要 4.0 版本以上的 Junit。同时告诉 Gradle 可以去 Maven 中央仓库去找这些依赖。下面的章节会进行更详细的描述。

## 5.3. 依赖配置

Gradle 中依赖以组的形式来划分不同的*配置*。每个配置都只是一组指定的依赖。我们称之为*依赖配置* 。你也可以借由此声明外部依赖。后面我们会了解到，这也可用用来声明项目的发布。

Java 插件定义了许多标准配置项。这些配置项形成了插件本身的 classpath。比如下面罗列的这一些，并且你可以从 [“Java 插件 - 依赖配置”](https://www.w3cschool.cn/gradle/5vdr1hug.html#tab:configurations)了解到更多详细内容.。

### 5.3.1. compile

编译范围依赖在所有的 classpath 中可用，同时它们也会被打包

### 5.3.2. runtime

runtime 依赖在运行和测试系统的时候需要，但在编译的时候不需要。比如，你可能在编译的时候只需要 JDBC API JAR，而只有在运行的时候才需要 JDBC 驱动实现

### 5.3.3. testCompile

测试期编译需要的附加依赖

### 5.3.4. testRuntime

测试运行期需要

不同的插件提供了不同的标准配置，你甚至也可以定义属于自己的配置项。

## 5.4. 外部依赖

依赖的类型有很多种，其中有一种类型称之为*外部依赖*。这种依赖由外部构建或者在不同的仓库中，例如 Maven 中央仓库或 Ivy 仓库中抑或是本地文件系统的某个目录中。

定义外部依赖需要像下面这样进行依赖配置

### 5.4.1. 定义外部依赖

build.gradle

```
dependencies {
    compile group: 'org.hibernate', name: 'hibernate-core', version: '3.6.7.Final'
}
```

外部依赖包含 group，name 和 version 几个属性。根据选取仓库的不同，group 和 version 也可能是可选的。

当然，也有一种更加简洁的方式来声明外部依赖。采用：将三个属性拼接在一起即可。"group:name:version"

### 5.4.2. 快速定义外部依赖

build.gradle

```
dependencies {
    compile 'org.hibernate:hibernate-core:3.6.7.Final'
}
```

## 5.5. 仓库

Gradle 是在一个被称之为*仓库*的地方找寻所需的外部依赖。仓库即是一个按 group，name 和 version 规则进行存储的一些文件。Gradle 可以支持不同的仓库存储格式，如 Maven 和 Ivy，并且还提供多种与仓库进行通信的方式，如通过本地文件系统或 HTTP。

默认情况下，Gradle 没有定义任何仓库，你需要在使用外部依赖之前至少定义一个仓库，例如 Maven 中央仓库。

### 5.5.1. 使用 Maven 中央仓库

build.gradle

```
repositories {
    mavenCentral()
}
```

或者其它远程 Maven 仓库：

### 5.5.2. 使用 Maven 远程仓库

build.gradle

```
repositories {
    maven {
        url "http://repo.mycompany.com/maven2"
    }
}
```

或者采用 Ivy 远程仓库

### 5.5.3. 采用 Ivy 远程仓库

build.gradle

```
repositories {
    ivy {
        url "http://repo.mycompany.com/repo"
    }
}
```

或者在指定本地文件系统构建的库。

### 5.5.4. 采用本地 Ivy 目录

build.gradle

```
repositories {
    ivy {
        // URL can refer to a local directory
        url "../local-repo"
    }
}
```

一个项目可以采用多个库。Gradle 会按照顺序从各个库里寻找所需的依赖文件，并且一旦找到第一个便停止搜索。

了解更多库相关的信息请参阅章节 50.6，“仓库”。

## 5.6. 打包发布

依赖配置也被用于发布文件[[3\]](https://www.w3cschool.cn/gradle/sh8k1htf.html#footname)我们称之为*打包发布*或*发布*。

插件对于打包提供了完美的支持，所以通常而言无需特别告诉 Gradle 需要做什么。但是你需要告诉 Gradle 发布到哪里。这就需要在 uploadArchives 任务中添加一个仓库。下面的例子是如何发布到远程 Ivy 仓库的：

### 5.6.1. 发布到 Ivy 仓库

build.gradle

```
uploadArchives {
    repositories {
        ivy {
            credentials {
                username "username"
                password "pw"
            }
            url "http://repo.mycompany.com"
        }
    }
}
```

执行 **gradle uploadArchives**，Gradle 便会构建并上传你的 jar 包，同时会生成一个 ivy.xml 一起上传到目标仓库。

当然你也可以发布到 Maven 仓库中。语法只需稍微一换就可以了。[[4\]](https://www.w3cschool.cn/gradle/sh8k1htf.html#footname)

p.s：发布到 Maven 仓库你需要 Maven 插件的支持，当然，Gradle 也会同时产生 pom.xml 一起上传到目标仓库。

### 5.6.2. 发布到 Maven 仓库

build.gradle

```
apply plugin: 'maven'
uploadArchives {
    repositories {
        mavenDeployer {
            repository(url: "file://localhost/tmp/myRepo/")
        }
    }
}
```

# 6. Gradle Groovy 快速入门

## 6.1. 一个基本的 Groovy 项目

让我们来看一个例子。要使用 Groovy 插件，你需要在构建脚本文件当中添加以下内容：

例子 Groovy plugin

build.gradle

```
apply plugin: 'groovy'   
```

注意： 此例子的代码可以在 Gradle 的二进制文件或源码中的 `samples/groovy/quickstart` 里看到。

这段代码同时会将 Java 插件应用到 project 中，如果 Java 插件还没被应用的话。Groovy 插件继承了 compile 任务 ，在 `src/main/groovy` 目录中查找源文件；且继承了 compileTest 任务，在 `src/test/groovy` 目录中查找测试的源文件。这些编译任务对这些目录使用了联合编译，这意味着它们可以同时包含 java 和 groovy 源文件。

要使用 groovy 编译任务，还必须声明要使用的 Groovy 版本以及从哪里获取 Groovy 库。你可以通过在 groovy 配置中添加依赖来完成。compile 配置继承了这个依赖，从而在编译 Groovy和 Java 源代码时，groovy 库也会被包含在类路径中。下面例子中，我们会使用 Maven 中央仓库中的 Groovy 2.2.0 版本。

例子 Dependency on Groovy 2.2.0

build.gradle

```
repositories {
    mavenCentral()
}
dependencies {
    compile 'org.codehaus.groovy:groovy-all:2.2.0'
}  
```

这里是我们写好的构建文件：

例子 Groovy example - complete build file

build.gradle

```
apply plugin: 'eclipse'
apply plugin: 'groovy'
repositories {
    mavenCentral()
}
dependencies {
    compile 'org.codehaus.groovy:groovy-all:2.2.0'
    testCompile 'junit:junit:4.11'
}
```

运行 gradle build 将会对你的项目进行编译，测试和打成jar包。

## 6.2. 总结

这一章描述了一个很简单的 Groovy 项目。通常情况下，一个真实的项目所需要的不止于此。因为一个 Groovy 项目也是一个 Java 项目， 由于 Groovy 工程也是一个 Java 工程，因此你能用 Java 做的事情 Groovy 也能做。

# 7. Gradle Web 工程构建

## 7.1. 打 War 包

需要打包 War 文件，需要在脚本中使用 War plugin：

### 7.1.1. War plugin

build.gradle

```
apply plugin: 'war'
```

备注：本示例代码可以在 Gradle 发行包中的 samples/webApplication/quickstart 路径下找到。

由于继承自 Java 插件，当你执行 **gradle build** 时，将会编译、测试、打包你的工程。Gradle 会在 `src/main/webapp` 下寻找 Web 工程文件。编译后的 classes 文件以及运行时依赖也都会被包含在 War 包中。

> Groovy web构建
>
> 在一个工程中你可以采用多个插件。比如你可以在 web 工程中同时使用 War plugin 和 Groovy plugin。插件会将 Gradle 依赖添加到你的 War 包中。

## 7.2. Web 工程启动

要启动 Web 工程，只需使用 Jetty plugin 即可：

### 7.2.1. 采用 Jetty plugin 启动 web 工程

build.gradle

```
apply plugin: 'jetty'
```

由于 Jetty plugin 继承自 War plugin。调用 gradle jettyRun 将会把你的工程启动部署到 jetty 容器中。调用 gradle jettyRunWar 会打包并启动部署到 jetty 容器中。

待添加：使用哪个 URL，配置端口，使用源文件的地方，可编辑你的文件，以及重新加载的内容。

# 8. Gradle Gradle 命令行的基本使用

## 8.1. 多任务调用

你可以以列表的形式在命令行中一次调用多个任务。例如 gradle compile test 命令会依次调用，并且每个任务仅会被调用一次。compile 和 test 任务以及它们的依赖任务。无论它们是否被包含在脚本中：即无论是以命令行的形式定义的任务还是依赖于其它任务都会被调用执行。来看下面的例子。

下面定义了四个任务。dist 和 test 都依赖于 compile，只用当 compile 被调用之后才会调用 gradle dist test 任务。

### 8.1.1. 任务依赖

![img](./images/Gradle-1.png)

### 8.1.2. 多任务调用

build.gradle

```
task compile << {
    println 'compiling source'
}
task compileTest(dependsOn: compile) << {
    println 'compiling unit tests'
}
task test(dependsOn: [compile, compileTest]) << {
    println 'running unit tests'
}
task dist(dependsOn: [compile, test]) << {
    println 'building the distribution'
}
```

**gradle dist test** 的输出结果。

```
\> gradle dist test
:compile
compiling source
:compileTest
compiling unit tests
:test
running unit tests
:dist
building the distribution
BUILD SUCCESSFUL
Total time: 1 secs
```

由于每个任务仅会被调用一次，所以调用 **gradle test test** 与调用 **gradle test** 效果是相同的。

## 8.2. 排除任务

你可以用命令行选项 -x 来排除某些任务，让我们用上面的例子来示范一下。

### 8.2.1. 排除任务

**gradle dist -x test** 的输出结果。

```
\> gradle dist -x test
:compile
compiling source
:dist
building the distribution
BUILD SUCCESSFUL
Total time: 1 secs
```

可以看到，test 任务并没有被调用，即使他是 dist 任务的依赖。同时 test 任务的依赖任务 compileTest 也没有被调用，而像 compile 被 test 和其它任务同时依赖的任务仍然会被调用。

## 8.3. 失败后继续执行

默认情况下只要有任务调用失败 Gradle 就是中断执行。这可能会使调用过程更快，但那些后面隐藏的错误不会被发现。所以你可以使用--continue 在一次调用中尽可能多的发现所有问题。

采用了--continue 选项，Gradle会调用*每一个*任务以及它们依赖的任务。而不是一旦出现错误就会中断执行。所有错误信息都会在最后被列出来。

一旦某个任务执行失败,那么所有依赖于该任务的子任务都不会被调用。例如由于 test 任务依赖于 complie 任务，所以如果 compile 调用出错，test 便不会被直接或间接调用。

## 8.4. 简化任务名

当你试图调用某个任务的时候，无需输入任务的全名。只需提供足够的可以唯一区分出该任务的字符即可。例如，上面的例子你也可以这么写。用 **gradle di** 来直接调用 dist 任务。

### 8.4.1. 简化任务名

**gradle di** 的输出结果

```
\> gradle di
:compile
compiling source
:compileTest
compiling unit tests
:test
running unit tests
:dist
building the distribution
BUILD SUCCESSFUL
Total time: 1 secs
```

你也可以用驼峰命名的任务中每个单词的首字母进行调用。例如，可以执行 **gradle compTest** or even **gradle cT** 来调用 compileTest 任务。

### 8.4.2. 简化驼峰任务名

**gradle cT** 的输出结果。

```
\> gradle cT
:compile
compiling source
:compileTest
compiling unit tests
BUILD SUCCESSFUL
Total time: 1 secs
```

简化后你仍然可以使用 -x 参数。

## 8.5. 选择构建位置

调用 gradle 时，默认情况下总是会构建当前目录下的文件，可以使用-b 参数选择构建的文件，并且当你使用此参数时settings。gradle 将不会生效,看下面的例子:

### 8.5.1. 选择文件构建

subdir/myproject.gradle

```
task hello << {
    println "using build file '$buildFile.name' in '$buildFile.parentFile.name'."
}
```

**gradle -q -b subdir/myproject.gradle hello** 的输出结果

```
Output of gradle -q -b subdir/myproject.gradle hello
\> gradle -q -b subdir/myproject.gradle hello
using build file 'myproject.gradle' in 'subdir'.
```

另外，你可以使用 -p 参数来指定构建的目录，例如在多项目构建中你可以用 -p 来替代 -b 参数。

### 8.5.2. 选择构建目录

**gradle -q -p subdir hello** 的输出结果

```
\> gradle -q -p subdir hello
using build file 'build.gradle' in 'subdir'.
```

## 8.6. 获取构建信息

Gradle 提供了许多内置任务来收集构建信息。这些内置任务对于了解依赖结构以及解决问题都是很有帮助的。

了解更多，可以参阅项目报告插件以为你的项目添加构建报告。

### 8.6.1. 项目列表

执行 **gradle projects** 会为你列出子项目名称列表。如下例。

### 8.6.2. 收集项目信息

**gradle -q projects** 的输出结果

```
\> gradle -q projects
\------------------------------------------------------------
Root project
\------------------------------------------------------------
Root project 'projectReports'
+--- Project ':api' - The shared API for the application
\--- Project ':webapp' - The Web application implementation
To see a list of the tasks of a project, run gradle <project-path>:tasks
For example, try running gradle :api:tasks
```

这份报告展示了每个项目的描述信息。当然你可以在项目中用 description 属性来指定这些描述信息。

### 8.6.3. 为项目添加描述信息.

build.gradle

```
description = 'The shared API for the application'
```

### 8.6.4. 任务列表

执行 gradle tasks 会列出项目中所有任务。这会显示项目中所有的默认任务以及每个任务的描述。如下例

### 8.6.5. 获取任务信息

**gradle -q tasks**的输出结果：

```
\> gradle -q tasks
\------------------------------------------------------------
All tasks runnable from root project
\------------------------------------------------------------
Default tasks: dists
Build tasks
\-----------
clean - Deletes the build directory (build)
dists - Builds the distribution
libs - Builds the JAR
Build Setup tasks
\-----------------
init - Initializes a new Gradle build. [incubating]
wrapper - Generates Gradle wrapper files. [incubating]
Help tasks
\----------
dependencies - Displays all dependencies declared in root project 'projectReports'.
dependencyInsight - Displays the insight into a specific dependency in root project 'projectReports'.
help - Displays a help message
projects - Displays the sub-projects of root project 'projectReports'.
properties - Displays the properties of root project 'projectReports'.
tasks - Displays the tasks runnable from root project 'projectReports' (some of the displayed tasks may belong to subprojects).
To see all tasks and more detail, run with --all.
```

默认情况下，这只会显示那些被分组的任务。你可以通过为任务设置 group 属性和 description 来把 这些信息展示到结果中。

### 8.6.6. 更改任务报告内容

build.gradle

```
dists {
    description = 'Builds the distribution'
    group = 'build'
}
```

当然你也可以用--all 参数来收集更多任务信息。这会列出项目中所有任务以及任务之间的依赖关系。

### 8.6.7. Obtaining more information about tasks

**gradle -q tasks --all**的输出结果：

```
\> gradle -q tasks --all
\------------------------------------------------------------
All tasks runnable from root project
\------------------------------------------------------------
Default tasks: dists
Build tasks
\-----------
clean - Deletes the build directory (build)
api:clean - Deletes the build directory (build)
webapp:clean - Deletes the build directory (build)
dists - Builds the distribution [api:libs, webapp:libs]
    docs - Builds the documentation
api:libs - Builds the JAR
    api:compile - Compiles the source files
webapp:libs - Builds the JAR [api:libs]
    webapp:compile - Compiles the source files
Build Setup tasks
\-----------------
init - Initializes a new Gradle build. [incubating]
wrapper - Generates Gradle wrapper files. [incubating]
Help tasks
\----------
dependencies - Displays all dependencies declared in root project 'projectReports'.
dependencyInsight - Displays the insight into a specific dependency in root project 'projectReports'.
help - Displays a help message
projects - Displays the sub-projects of root project 'projectReports'.
properties - Displays the properties of root project 'projectReports'.
tasks - Displays the tasks runnable from root project 'projectReports' (some of the displayed tasks may belong to subprojects).
```

### 8.6.8. 获取任务帮助信息

执行 **gradle help --task someTask** 可以显示指定任务的详细信息。或者多项目构建中相同任务名称的所有任务的信息。如下例。

### 8.6.9. 获取任务帮助

**gradle -q help --task libs** 的输出结果

```
\> gradle -q help --task libs
Detailed task information for libs
Paths
     :api:libs
     :webapp:libs
Type
     Task (org.gradle.api.Task)
Description
     Builds the JAR
```

这些结果包含了任务的路径、类型以及描述信息等。

### 8.6.10. 获取依赖列表

执行 **gradle dependencies** 会列出项目的依赖列表，所有依赖会根据任务区分，以树型结构展示出来。如下例。

### 8.6.11. 获取依赖信息

**gradle -q dependencies api:dependencies webapp:dependencies** 的输出结果

```
\> gradle -q dependencies api:dependencies webapp:dependencies
\------------------------------------------------------------
Root project
\------------------------------------------------------------
No configurations
\------------------------------------------------------------
Project :api - The shared API for the application
\------------------------------------------------------------
compile
\--- org.codehaus.groovy:groovy-all:2.2.0
testCompile
\--- junit:junit:4.11
     \--- org.hamcrest:hamcrest-core:1.3
\------------------------------------------------------------
Project :webapp - The Web application implementation
\------------------------------------------------------------
compile
+--- project :api
|    \--- org.codehaus.groovy:groovy-all:2.2.0
\--- commons-io:commons-io:1.2
testCompile
No dependencies
```

虽然输出结果很多，但这对于了解构建任务十分有用，当然你可以通过**--configuration** 参数来查看 指定构建任务的依赖情况。

### 8.6.12. 过滤依赖信息

**gradle -q api:dependencies --configuration testCompile** 的输出结果

```
\> gradle -q api:dependencies --configuration testCompile
\------------------------------------------------------------
Project :api - The shared API for the application
\------------------------------------------------------------
testCompile
\--- junit:junit:4.11
     \--- org.hamcrest:hamcrest-core:1.3
```

### 8.6.13. 查看特定依赖

执行 Running gradle dependencyInsight 可以查看指定的依赖情况。如下例。

### 8.6.14. 获取特定依赖

**gradle -q webapp:dependencyInsight --dependency groovy --configuration compile** 的输出结果

```
\> gradle -q webapp:dependencyInsight --dependency groovy --configuration compile
org.codehaus.groovy:groovy-all:2.2.0
\--- project :api
     \--- compile
```

这对于了解依赖关系、了解为何选择此版本作为依赖十分有用。了解更多请参阅[依赖检查报告](http://gradledoc.qiniudn.com/1.12/dsl/org.gradle.api.tasks.diagnostics.DependencyInsightReportTask.html)。

dependencyInsight 任务是'Help'任务组中的一个。这项任务需要进行配置才可以。如果用了 Java 相关的插件，那么 dependencyInsight 任务已经预先被配置到'Compile'下了。你只需要通过'--dependency'参数来制定所需查看的依赖即可。如果你不想用默认配置的参数项你可以通过 '--configuration' 参数来进行指定。了解更多请参阅[依赖检查报告](http://gradledoc.qiniudn.com/1.12/dsl/org.gradle.api.tasks.diagnostics.DependencyInsightReportTask.html)。

### 8.6.15. 获取项目属性列表

执行 **gradle properties** 可以获取项目所有属性列表。如下例。

### 8.6.16. 属性信息

**gradle -q api:properties** 的输出结果

```
\> gradle -q api:properties
\------------------------------------------------------------
Project :api - The shared API for the application
\------------------------------------------------------------
allprojects: [project ':api']
ant: org.gradle.api.internal.project.DefaultAntBuilder@12345
antBuilderFactory: org.gradle.api.internal.project.DefaultAntBuilderFactory@12345
artifacts: org.gradle.api.internal.artifacts.dsl.DefaultArtifactHandler@12345
asDynamicObject: org.gradle.api.internal.ExtensibleDynamicObject@12345
buildDir: /home/user/gradle/samples/userguide/tutorial/projectReports/api/build
buildFile: /home/user/gradle/samples/userguide/tutorial/projectReports/api/build.gradle
```

### 8.6.17. 构建日志

**--profile** 参数可以收集一些构建期间的信息并保存到 build/reports/profile 目录下并且以构建时间命名这些文件。

下面这份日志记录了总体花费时间以及各过程花费的时间，并以时间大小倒序排列，并且记录了任务的执行情况。

如果采用了 buildSrc，那么在 buildSrc/build 下同时也会生成一份日志记录记录。

![img](./images/Gradle-2.png)

## 8.7. Dry Run

有时可能你只想知道某个任务在一个任务集中按顺序执行的结果，但并不想实际执行这些任务。那么你可以用-m 参数。例如 **gradle -m clean compile** 会调用 clean 和 compile，这与 tasks 可以形成互补，让你知道哪些任务可以用于执行。

# 9. Gradle 编写构建脚本



