<h2>目录</h2>

  <a href="#1-笔记和代码云备份">1. 笔记和代码云备份</a><br>
<details open>
  <summary><a href="#2-网页书签bookmarks云备份">2. 网页书签Bookmarks云备份</a></summary>
  <ul>
    <a href="#21-cubox介绍">2.1. Cubox介绍</a><br>
    <a href="#22-cubox下载安装">2.2. Cubox下载安装</a><br>
    <a href="#23-cubox使用">2.3. Cubox使用</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#3-密码password云备份">3. 密码Password云备份</a></summary>
  <ul>
    <a href="#31-bitwarden介绍">3.1. Bitwarden介绍</a><br>
    <a href="#32-bitwarden下载安装">3.2. Bitwarden下载安装</a><br>
    <a href="#33-搭建自己的密码管理服务器">3.3. 搭建自己的密码管理服务器</a><br>
  </ul>
</details>


<h1>Cloud Backup</h1>

# 1. 笔记和代码云备份

都保存在Gitee上，可以备份到本地，或者上传到其他Git环境，例如GitLab，GitHub

Gitee笔记地址：https://gitee.com/yanglin5260/yanglin

# 2. 网页书签Bookmarks云备份

## 2.1. Cubox介绍

Cubox 是一个现代化书签收藏夹，帮助你高效收藏并管理有价值的网络信息。

Cubox 的功能不仅限于书签收藏夹，它其实是一个集「收藏夹 + 稍后读 + 搜索工具 + 个人信息管理」四个功能于一身的 App。

我更愿意把 Cubox 称为私人网络信息助手。它能搜集我们几乎在网上浏览到的一切信息，包括但不限于文章、图片、视频等内容。当发现了自己在意的信息后，可以通过 Cubox 提供的快捷收藏工具，将他们快速保存。在之后工作和生活中，当你需要它们时，能够随时在 Cubox 中浏览或搜索。

## 2.2. Cubox下载安装

Cubox可以下载安装到各种设备极其浏览器上，下载地址如下：

https://cubox.pro/#download

![image-20211220111139646](./images/CloudBackup-1.png)

## 2.3. Cubox使用

可以使用网页端查看收藏的书签和其他资料

https://cubox.pro/web/save/inbox



也可以使用在浏览器插件快捷按钮添加收藏到默认的收集箱中，后面可以访问网页端整理到具体的文件夹

![image-20211220111943478](./images/CloudBackup-2.png)

# 3. 密码Password云备份



## 3.1. Bitwarden介绍



[Bitwarden](https://bitwarden.com/)是一个跨平台的密码管理软件，类似于1Password、EnPass、LastPass 等，支持几乎目前所有的平台（Mac、PC、Linux、iOS、Android、网页浏览器、浏览器扩展）。Bitwarden 是开源的，可以将服务端部署在自己的服务器上，比如群晖的 Docker 中。

提供个人账户（免费版和高级会员版）和组织账户（免费版、家庭版、团队版和企业版）。查看[各版本的区别](https://help.bitwarden.in/plans-and-pricing/about-bitwarden-plans)。

Bitwarden 个人账户的免费版不限制使用时间、不限制密码条目数、不限制设备数量、不限制文件夹数量、支持常见的验证器应用程序，对于大部分人日常使用已经足够。如果你要使用高级功能（如 Yubikey、TOTP、文件附件），可以以 $10/年购买高级会员版，相对 1Password 已经是非常良心的价格了。

**Bitwarden 的优点：**

- 客户端界面简洁（我觉得是优点）
- 大部分功能免费，价格良心
- 可以使用 Bitwarden 的云也可以自己本地部署
- 有支持高级功能的免费的第三方开源 bitwarden_rs 项目

**Bitwarden 的缺点：**

- 不能离线编辑（[以后应该也不会支持此功能](https://community.bitwarden.com/t/offline-management-of-writeable-vault-items/107)）
- 不能自动点击登录按钮

相比较，1Passwords 更为方便，界面也更华丽。

Bitwarden 的介绍、配置和使用请参阅 [Bitwarden Help Center](https://help.bitwarden.com/)，或者我翻译的[中文版](https://help.bitwarden.in/)。

## 3.2. Bitwarden下载安装

https://bitwarden.com/download/

## 3.3. 搭建自己的密码管理服务器

[docker-compose部署文件](./Bitwarden/docker-compose.yaml)，还需要修饰



参考文章：

[自建超级安全密码管理服务Bitwarden保姆级使用教程](https://www.linjoey.cn/index.php/archives/365/)

[利用Bitwarden自建个人私有密码管理系统服务 – 配置和使用](https://www.inuhang.xyz/index.php/2021/04/07/%E5%88%A9%E7%94%A8bitwarden%E8%87%AA%E5%BB%BA%E4%B8%AA%E4%BA%BA%E7%A7%81%E6%9C%89%E5%AF%86%E7%A0%81%E7%AE%A1%E7%90%86%E7%B3%BB%E7%BB%9F%E6%9C%8D%E5%8A%A1-%E9%85%8D%E7%BD%AE%E5%92%8C%E4%BD%BF/)

[搭建自己的密码管理服务器 Bitwarden](https://cloud.tencent.com/developer/article/1578102)

[Docker-compose Bitwarden](https://github.com/Fretwin/bitwardenrs)





