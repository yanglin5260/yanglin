package com.design.pattern04;

public class Wrapper implements Packing {

    @Override
    public String pack() {
        return "Wrapper";
    }
}