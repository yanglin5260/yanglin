package com.design.pattern15;

public interface Order {
    void execute();
}