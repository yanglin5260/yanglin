package com.design.pattern08;

import java.util.List;

public interface Criteria {
    List<Person> meetCriteria(List<Person> persons);
}