package com.design.pattern03;

/**
 * <p>
 * <b>饿汉式</b><br>
 * <b>是否 Lazy 初始化：</b>否<br>
 * <b>是否多线程安全：</b>是<br>
 * <b>实现难度：</b>易<br>
 * <b>描述：</b>这种方式比较常用，但容易产生垃圾对象。<br>
 * 优点：没有加锁，执行效率会提高。<br>
 * 缺点：类加载时就初始化，浪费内存。<br>
 * 它基于 classloader
 * 机制避免了多线程的同步问题，不过，instance在类装载时就实例化，虽然导致类装载的原因有很多种，在单例模式中大多数都是调用 getInstance
 * 方法，但是也不能确定有其他的方式（或者其他的静态方法）导致类装载，这时候初始化 instance 显然没有达到 lazy loading 的效果。<br>
 *
 * @version V1.0
 * @ClassName:SingletonStarvingSafe
 * @author:yanglin
 * @time:2018年8月27日 下午8:36:15
 */
public class SingletonStarvingSafe {
    private static final SingletonStarvingSafe instance = new SingletonStarvingSafe();

    private SingletonStarvingSafe() {
    }

    public static SingletonStarvingSafe getInstance() {
        return instance;
    }
}
