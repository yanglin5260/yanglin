package com.design.pattern02;

public interface Color {
    void fill();
}