package com.design.pattern22;

public abstract class AbstractCustomer {
    protected String name;

    public abstract boolean isNil();

    public abstract String getName();
}