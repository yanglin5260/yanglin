package com.design.pattern25;

public interface ComputerPart {
    void accept(ComputerPartVisitor computerPartVisitor);
}