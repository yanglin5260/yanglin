package com.design.pattern28;

/**
 * <p>
 * 组合实体模式（Composite Entity Pattern）用在 EJB 持久化机制中。一个组合实体是一个 EJB 实体
 * bean，代表了对象的图解。当更新一个组合实体时，内部依赖对象 beans 会自动更新，因为它们是由 EJB 实体 bean 管理的。以下是组合实体
 * bean 的参与者。</b>
 * <ol>
 * <li><b>组合实体（Composite-Entity）</b>它是主要的实体bean。它可以是粗粒的，或者可以包含一个粗粒度对象，用于持续生命周期。</li>
 * <li><b>粗粒度对象（Coarse-Grained-Object）</b>该对象包含依赖对象。它有自己的生命周期，也能管理依赖对象的生命周期。</li>
 * <li><b>依赖对象（Dependent-Object）</b>依赖对象是一个持续生命周期依赖于粗粒度对象的对象。</li>
 * <li><b>策略（Strategies）</b>策略表示如何实现组合实体。</li>
 * </ol>
 * </p>
 *
 * @version V1.0
 * @ClassName:CompositeEntityPatternDemo
 * @author:yanglin
 * @time:2018年10月11日 上午10:04:28
 */
public class CompositeEntityPatternDemo {
    public static void main(String[] args) {
        Client client = new Client();
        client.setData("Test", "Data");
        client.printData();
        client.setData("Second Test", "Data1");
        client.printData();
    }
}