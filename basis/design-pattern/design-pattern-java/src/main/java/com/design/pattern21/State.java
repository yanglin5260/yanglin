package com.design.pattern21;

public interface State {
    void doAction(Context context);
}