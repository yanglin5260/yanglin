package com.design.pattern01;

public interface Shape {
    void draw();
}