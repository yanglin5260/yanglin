package com.design.pattern10;

public interface Shape {
    void draw();
}