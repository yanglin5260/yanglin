package com.design.pattern03;

/**
 * <b>单例模式：懒汉式，线程不安全 </b><br>
 * 是否 Lazy初始化：是 <br>
 * 是否多线程安全：否 <br>
 * 实现难度：易 <br>
 * <p>
 * 描述：这种方式是最基本的实现方式，这种实现最大的问题就是不支持多线程。因为没有加锁 synchronized，所以严格意义上它并不算单例模式。 这种方式
 * lazy loading 很明显，不要求线程安全，在多线程不能正常工作。
 * </p>
 *
 * @Description:SingletonLazyNoSafe
 * @author:yanglin
 * @time:2018年8月24日 下午7:20:24
 */
public class SingletonLazyNoSafe {
    private static SingletonLazyNoSafe instance;

    private SingletonLazyNoSafe() {
    }

    public static SingletonLazyNoSafe getInstance() {
        if (instance == null) {
            instance = new SingletonLazyNoSafe();
        }
        return instance;
    }
}
