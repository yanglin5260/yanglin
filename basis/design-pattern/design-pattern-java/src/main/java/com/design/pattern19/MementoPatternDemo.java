package com.design.pattern19;

/**
 * <p>
 * 备忘录模式（Memento Pattern）保存一个对象的某个状态，以便在适当的时候恢复对象。备忘录模式属于行为型模式。
 * <p>
 * <b>意图：</b>在不破坏封装性的前提下，捕获一个对象的内部状态，并在该对象之外保存这个状态。
 * <p>
 * <b>主要解决：</b>所谓备忘录模式就是在不破坏封装的前提下，捕获一个对象的内部状态，并在该对象之外保存这个状态，这样可以在以后将对象恢复到原先保存的状态。
 * <p>
 * <b>何时使用：</b>很多时候我们总是需要记录一个对象的内部状态，这样做的目的就是为了允许用户取消不确定或者错误的操作，能够恢复到他原先的状态，使得他有"后悔药"可吃。
 * <p>
 * <b>如何解决：</b>通过一个备忘录类专门存储对象状态。
 * <p>
 * <b>关键代码：</b>客户不与备忘录类耦合，与备忘录管理类耦合。
 * <p>
 * <b>应用实例：</b>
 * <ol>
 * <li>后悔药。</li>
 * <li>打游戏时的存档。</li>
 * <li>Windows 里的 ctri + z。</li>
 * <li>IE 中的后退。</li>
 * <li>数据库的事务管理。</li>
 * </ol>
 * <p>
 * <b>优点：</b>
 * <ol>
 * <li>给用户提供了一种可以恢复状态的机制，可以使用户能够比较方便地回到某个历史的状态。</li>
 * <li>实现了信息的封装，使得用户不需要关心状态的保存细节。</li>
 * </ol>
 * <p>
 * <b>缺点：</b>消耗资源。如果类的成员变量过多，势必会占用比较大的资源，而且每一次保存都会消耗一定的内存。
 * <p>
 * <b>使用场景：</b>
 * <ol>
 * <li>需要保存/恢复数据的相关状态场景。</li>
 * <li>提供一个可回滚的操作。</li>
 * </ol>
 * <p>
 * <b>注意事项：</b>
 * <ol>
 * <li>为了符合迪米特原则，还要增加一个管理备忘录的类。</li>
 * <li>为了节约内存，可使用原型模式+备忘录模式。</li>
 * </ol>
 *
 * @version V1.0
 * @ClassName:MementoPatternDemo
 * @author:yanglin
 * @time:2018年10月8日 下午3:13:28
 */
public class MementoPatternDemo {
    public static void main(String[] args) {
        Originator originator = new Originator();
        CareTaker careTaker = new CareTaker();
        originator.setState("State #1");
        originator.setState("State #2");
        careTaker.add(originator.saveStateToMemento());
        originator.setState("State #3");
        careTaker.add(originator.saveStateToMemento());
        originator.setState("State #4");

        System.out.println("Current State: " + originator.getState());
        originator.getStateFromMemento(careTaker.get(0));
        System.out.println("First saved State: " + originator.getState());
        originator.getStateFromMemento(careTaker.get(1));
        System.out.println("Second saved State: " + originator.getState());
    }
}