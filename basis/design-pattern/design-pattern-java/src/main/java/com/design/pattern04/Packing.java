package com.design.pattern04;

public interface Packing {
    String pack();
}