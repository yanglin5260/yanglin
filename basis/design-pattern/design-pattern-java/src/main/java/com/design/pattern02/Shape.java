package com.design.pattern02;

public interface Shape {
    void draw();
}