package com.design.pattern27;

public interface BusinessService {
    void doProcessing();
}