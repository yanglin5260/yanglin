package com.design.pattern32;

public interface Service {
    String getName();

    void execute();
}