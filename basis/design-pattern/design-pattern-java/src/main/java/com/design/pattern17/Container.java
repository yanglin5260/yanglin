package com.design.pattern17;

public interface Container {
    Iterator getIterator();
}