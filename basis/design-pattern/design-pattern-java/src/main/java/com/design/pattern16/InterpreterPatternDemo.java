package com.design.pattern16;

/**
 * <p>
 * 解释器模式（Interpreter
 * Pattern）提供了评估语言的语法或表达式的方式，它属于行为型模式。这种模式实现了一个表达式接口，该接口解释一个特定的上下文。这种模式被用在 SQL
 * 解析、符号处理引擎等。
 * <p>
 * <b>意图：</b>给定一个语言，定义它的文法表示，并定义一个解释器，这个解释器使用该标识来解释语言中的句子。
 * <p>
 * <b>主要解决：</b>对于一些固定文法构建一个解释句子的解释器。
 * <p>
 * <b>何时使用：</b>如果一种特定类型的问题发生的频率足够高，那么可能就值得将该问题的各个实例表述为一个简单语言中的句子。这样就可以构建一个解释器，该解释器通过解释这些句子来解决该问题。
 * <p>
 * <b>如何解决：</b>构件语法树，定义终结符与非终结符。
 * <p>
 * <b>关键代码：</b>构件环境类，包含解释器之外的一些全局信息，一般是 HashMap。
 * <p>
 * <b>应用实例：</b>编译器、运算表达式计算。
 * <p>
 * <b>优点：</b>
 * <ol>
 * <li>可扩展性比较好，灵活。</li>
 * <li>增加了新的解释表达式的方式。</li>
 * <li>易于实现简单文法。</li>
 * </ol>
 * <p>
 * <b>缺点：</b>
 * <ol>
 * <li>可利用场景比较少。</li>
 * <li>对于复杂的文法比较难维护。</li>
 * <li>解释器模式会引起类膨胀。</li>
 * <li>解释器模式采用递归调用方法。</li>
 * </ol>
 * <p>
 * <b>使用场景：</b>
 * <ol>
 * <li>可以将一个需要解释执行的语言中的句子表示为一个抽象语法树。</li>
 * <li>一些重复出现的问题可以用一种简单的语言来进行表达。</li>
 * <li>一个简单语法需要解释的场景。</li>
 * </ol>
 * <p>
 * <b>注意事项：</b>可利用场景比较少，JAVA 中如果碰到可以用 expression4J 代替。
 *
 * @version V1.0
 * @ClassName:InterpreterPatternDemo
 * @author:yanglin
 * @time:2018年9月26日 下午7:31:23
 */
public class InterpreterPatternDemo {

    // 规则：Robert 和 John 是男性
    public static Expression getMaleExpression() {
        Expression robert = new TerminalExpression("Robert");
        Expression john = new TerminalExpression("John");
        return new OrExpression(robert, john);
    }

    // 规则：Julie 是一个已婚的女性
    public static Expression getMarriedWomanExpression() {
        Expression julie = new TerminalExpression("Julie");
        Expression married = new TerminalExpression("Married");
        return new AndExpression(julie, married);
    }

    public static void main(String[] args) {
        Expression isMale = getMaleExpression();
        Expression isMarriedWoman = getMarriedWomanExpression();

        System.out.println("John is male? " + isMale.interpret("John"));
        System.out.println("Julie is a married women? " + isMarriedWoman.interpret("Married Julie"));
    }
}