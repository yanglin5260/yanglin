package com.design.pattern03;

/**
 * <p>
 * <b>双检锁/双重校验锁（DCL，即 double-checked locking）</b><br>
 * <b>JDK 版本：</b>JDK1.5 起<br>
 * <b>是否 Lazy 初始化：</b>是<br>
 * <b>是否多线程安全：</b>是<br>
 * <b>实现难度：</b>较复杂<br>
 * <b>描述：</b>这种方式采用双锁机制，安全且在多线程情况下能保持高性能。 getInstance() 的性能对应用程序很关键。<br>
 *
 * @version V1.0
 * @ClassName:SingletonDoubleCheckedLocking
 * @author:yanglin
 * @time:2018年8月27日 下午8:43:18
 */
public class SingletonDoubleCheckedLocking {
    private volatile static SingletonDoubleCheckedLocking singleton;

    private SingletonDoubleCheckedLocking() {
    }

    public static SingletonDoubleCheckedLocking getInstance() {
        if (singleton == null) {
            synchronized (SingletonDoubleCheckedLocking.class) {
                if (singleton == null) {
                    singleton = new SingletonDoubleCheckedLocking();
                }
            }
        }
        return singleton;
    }
}
