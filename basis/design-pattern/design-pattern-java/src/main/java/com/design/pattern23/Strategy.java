package com.design.pattern23;

public interface Strategy {
    int doOperation(int num1, int num2);
}