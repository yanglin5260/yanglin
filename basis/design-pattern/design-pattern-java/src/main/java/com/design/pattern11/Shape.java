package com.design.pattern11;

public interface Shape {
    void draw();
}