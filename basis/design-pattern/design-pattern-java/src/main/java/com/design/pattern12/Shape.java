package com.design.pattern12;

public interface Shape {
    void draw();
}