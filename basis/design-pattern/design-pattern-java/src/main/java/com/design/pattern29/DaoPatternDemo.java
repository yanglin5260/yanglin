package com.design.pattern29;

/**
 * <p>
 * 数据访问对象模式（Data-Access-Object-Pattern）或DAO模式用于把低级的数据访问API或操作从高级的业务服务中分离出来。以下是数据访问对象模式的参与者。</b>
 * <ol>
 * <li><b>数据访问对象接口（Data-Access-Object-Interface）</b>该接口定义了在一个模型对象上要执行的标准操作。</li>
 * <li><b>数据访问对象实体类（Data-Access-Object-concrete-class）</b>该类实现了上述的接口。该类负责从数据源获取数据，数据源可以是数据库，也可以是xml，或者是其他的存储机制。</li>
 * <li><b>模型对象/数值对象（Model-Object/Value-Object）</b>该对象是简单的POJO，包含了get/set方法来存储通过使用DAO类检索到的数据。</li>
 * </ol>
 * </p>
 *
 * @version V1.0
 * @ClassName:DaoPatternDemo
 * @author:yanglin
 * @time:2018年10月11日 下午1:49:45
 */
public class DaoPatternDemo {
    public static void main(String[] args) {
        StudentDao studentDao = new StudentDaoImpl();

        // 输出所有的学生
        for (Student student : studentDao.getAllStudents()) {
            System.out.println("Student: [RollNo : " + student.getRollNo() + ", Name : " + student.getName() + " ]");
        }

        // 更新学生
        Student student = studentDao.getAllStudents().get(0);
        student.setName("Michael");
        studentDao.updateStudent(student);

        // 获取学生
        studentDao.getStudent(0);
        System.out.println("Student: [RollNo : " + student.getRollNo() + ", Name : " + student.getName() + " ]");
    }
}