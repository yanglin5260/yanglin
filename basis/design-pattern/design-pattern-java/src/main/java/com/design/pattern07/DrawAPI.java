package com.design.pattern07;

public interface DrawAPI {
    void drawCircle(int radius, int x, int y);
}