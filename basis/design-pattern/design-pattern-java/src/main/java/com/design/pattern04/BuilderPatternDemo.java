package com.design.pattern04;

/**
 * <p>
 * 建造者模式（Builder
 * Pattern）使用多个简单的对象一步一步构建成一个复杂的对象。这种类型的设计模式属于创建型模式，它提供了一种创建对象的最佳方式。 一个 Builder
 * 类会一步一步构造最终的对象。该 Builder 类是独立于其他对象的。
 * <p>
 * <b>意图：</b>将一个复杂的构建与其表示相分离，使得同样的构建过程可以创建不同的表示。
 * <p>
 * <b>主要解决：</b>主要解决在软件系统中，有时候面临着"一个复杂对象"的创建工作，其通常由各个部分的子对象用一定的算法构成；由于需求的变化，这个复杂对象的各个部分经常面临着剧烈的变化，但是将它们组合在一起的算法却相对稳定。
 * <p>
 * <b>何时使用：</b>一些基本部件不会变，而其组合经常变化的时候。
 * <p>
 * <b>如何解决：</b>将变与不变分离开。
 * <p>
 * <b>关键代码：</b>建造者：创建和提供实例，导演：管理建造出来的实例的依赖关系。
 * <p>
 * <b>应用实例：</b>
 * <ol>
 * <li>去肯德基，汉堡、可乐、薯条、炸鸡翅等是不变的，而其组合是经常变化的，生成出所谓的"套餐"。</li>
 * <li>JAVA 中的 StringBuilder。</li>
 * </ol>
 * <p>
 * <b>优点：</b>
 * <ol>
 * <li>建造者独立，易扩展。</li>
 * <li>便于控制细节风险。</li>
 * </ol>
 * <p>
 * <b>缺点：</b>
 * <ol>
 * <li>产品必须有共同点，范围有限制。</li>
 * <li>如内部变化复杂，会有很多的建造类。</li>
 * </ol>
 * <p>
 * <b>使用场景： </b>
 * <ol>
 * <li>需要生成的对象具有复杂的内部结构。</li>
 * <li>需要生成的对象内部属性本身相互依赖。</li>
 * </ol>
 * <p>
 * <b>注意事项：</b>与工厂模式的区别是：建造者模式更加关注与零件装配的顺序。
 *
 * @version V1.0
 * @ClassName:BuilderPatternDemo
 * @author:yanglin
 * @time:2018年8月29日 上午11:18:21
 */
public class BuilderPatternDemo {
    public static void main(String[] args) {
        MealBuilder mealBuilder = new MealBuilder();

        Meal vegMeal = mealBuilder.prepareVegMeal();
        System.out.println("Veg Meal");
        vegMeal.showItems();
        System.out.println("Total Cost: " + vegMeal.getCost());

        Meal nonVegMeal = mealBuilder.prepareNonVegMeal();
        System.out.println("\n\nNon-Veg Meal");
        nonVegMeal.showItems();
        System.out.println("Total Cost: " + nonVegMeal.getCost());
    }
}