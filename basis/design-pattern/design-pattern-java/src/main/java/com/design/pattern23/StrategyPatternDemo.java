package com.design.pattern23;

/**
 * <p>
 * 在策略模式（Strategy Pattern）中，一个类的行为或其算法可以在运行时更改。这种类型的设计模式属于行为型模式。
 * 在策略模式中，我们创建表示各种策略的对象和一个行为随着策略对象改变而改变的 context 对象。策略对象改变 context 对象的执行算法。
 * <p>
 * <b>意图：</b>定义一系列的算法,把它们一个个封装起来, 并且使它们可相互替换。
 * <p>
 * <b>主要解决：</b>在有多种算法相似的情况下，使用 if...else 所带来的复杂和难以维护。
 * <p>
 * <b>何时使用：</b>一个系统有许多许多类，而区分它们的只是他们直接的行为。
 * <p>
 * <b>如何解决：</b>将这些算法封装成一个一个的类，任意地替换。
 * <p>
 * <b>关键代码：</b>实现同一个接口。
 * <p>
 * <b>应用实例：</b>
 * <ol>
 * <li>诸葛亮的锦囊妙计，每一个锦囊就是一个策略。</li>
 * <li>旅行的出游方式，选择骑自行车、坐汽车，每一种旅行方式都是一个策略。</li>
 * <li>JAVA AWT 中的 LayoutManager。</li>
 * </ol>
 * <p>
 * <b>优点：</b>
 * <ol>
 * <li>算法可以自由切换。</li>
 * <li>避免使用多重条件判断。</li>
 * <li>扩展性良好。</li>
 * </ol>
 * <p>
 * <b>缺点：</b>
 * <ol>
 * <li>策略类会增多。</li>
 * <li>所有策略类都需要对外暴露。</li>
 * </ol>
 * <p>
 * <b>使用场景：</b>
 * <ol>
 * <li>如果在一个系统里面有许多类，它们之间的区别仅在于它们的行为，那么使用策略模式可以动态地让一个对象在许多行为中选择一种行为。</li>
 * <li>一个系统需要动态地在几种算法中选择一种。</li>
 * <li>如果一个对象有很多的行为，如果不用恰当的模式，这些行为就只好使用多重的条件选择语句来实现。</li>
 * </ol>
 * <p>
 * <b>注意事项：</b>如果一个系统的策略多于四个，就需要考虑使用混合模式，解决策略类膨胀的问题。
 *
 * @version V1.0
 * @ClassName:StrategyPatternDemo
 * @author:yanglin
 * @time:2018年10月8日 下午4:35:05
 */
public class StrategyPatternDemo {
    public static void main(String[] args) {
        Context context = new Context(new OperationAdd());
        System.out.println("10 + 5 = " + context.executeStrategy(10, 5));

        context = new Context(new OperationSubstract());
        System.out.println("10 - 5 = " + context.executeStrategy(10, 5));

        context = new Context(new OperationMultiply());
        System.out.println("10 * 5 = " + context.executeStrategy(10, 5));
    }
}