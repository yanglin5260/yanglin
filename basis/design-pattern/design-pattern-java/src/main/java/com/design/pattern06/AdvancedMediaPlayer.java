package com.design.pattern06;

public interface AdvancedMediaPlayer {
    void playVlc(String fileName);

    void playMp4(String fileName);
}