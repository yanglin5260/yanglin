package com.design.pattern06;

public interface MediaPlayer {
    void play(String audioType, String fileName);
}