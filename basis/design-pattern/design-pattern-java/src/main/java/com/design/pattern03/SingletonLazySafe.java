package com.design.pattern03;

/**
 * <p>
 * <b>单例模式：懒汉式，线程安全</b><br>
 * 是否 Lazy 初始化：是<br>
 * 是否多线程安全：是<br>
 * 实现难度：易<br>
 * <p>
 * 描述：这种方式具备很好的 lazy loading，能够在多线程中很好的工作，但是，效率很低，99% 情况下不需要同步。 <br>
 * 优点：第一次调用才初始化，避免内存浪费。 <br>
 * 缺点：必须加锁 synchronized 才能保证单例，但加锁会影响效率。 getInstance() 的性能对应用程序不是很关键（该方法使用不太频繁）。
 *
 * @Description:SingletonLazySafe
 * @author:yanglin
 * @time:2018年8月24日 下午7:27:16
 */
public class SingletonLazySafe {

    private static SingletonLazySafe instance;

    private SingletonLazySafe() {
    }

    public static synchronized SingletonLazySafe getInstance() {
        if (instance == null) {
            instance = new SingletonLazySafe();
        }
        return instance;
    }
}
