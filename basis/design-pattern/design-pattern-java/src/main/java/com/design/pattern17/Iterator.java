package com.design.pattern17;

public interface Iterator {
    boolean hasNext();

    Object next();
}