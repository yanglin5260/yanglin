package com.design.pattern16;

public interface Expression {
    boolean interpret(String context);
}