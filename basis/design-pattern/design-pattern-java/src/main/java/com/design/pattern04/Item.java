package com.design.pattern04;

public interface Item {
    String name();

    Packing packing();

    float price();
}