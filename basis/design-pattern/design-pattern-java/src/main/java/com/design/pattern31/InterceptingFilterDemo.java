package com.design.pattern31;

/**
 * <p>
 * 拦截过滤器模式（Intercepting-Filter-Pattern）用于对应用程序的请求或响应做一些预处理/后处理。定义过滤器，并在把请求传给实际目标应用程序之前应用在请求上。过滤器可以做认证/授权/记录日志，或者跟踪请求，然后把请求传给相应的处理程序。以下是这种设计模式的实体。</b>
 * <ol>
 * <li><b>过滤器（Filter）</b>过滤器在请求处理程序执行请求之前或之后，执行某些任务。</li>
 * <li><b>过滤器链（Filter Chain）</b>过滤器链带有多个过滤器，并在 Target 上按照定义的顺序执行这些过滤器。</li>
 * <li><b>处理对象（Target）</b>Target对象是请求处理程序。</li>
 * <li><b>过滤管理器（Filter Manager）</b>过滤管理器管理过滤器和过滤器链。</li>
 * <li><b>客户端（Client）</b>Client是向Target对象发送请求的对象。</li>
 * </ol>
 * </p>
 *
 * @version V1.0
 * @ClassName:InterceptingFilterDemo
 * @author:yanglin
 * @time:2018年10月12日 下午3:10:41
 */
public class InterceptingFilterDemo {
    public static void main(String[] args) {
        FilterManager filterManager = new FilterManager(new Target());
        filterManager.setFilter(new AuthenticationFilter());
        filterManager.setFilter(new DebugFilter());

        Client client = new Client();
        client.setFilterManager(filterManager);
        client.sendRequest("HOME");
    }
}