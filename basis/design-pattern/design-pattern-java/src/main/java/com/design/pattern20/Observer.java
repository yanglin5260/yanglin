package com.design.pattern20;

public abstract class Observer {
    protected Subject subject;

    public abstract void update();
}