package com.design.pattern32;

/**
 * <p>
 * 服务定位器模式（Service-Locator-Pattern）用在我们想使用JNDI查询定位各种服务的时候。考虑到为某个服务查找JNDI的代价很高，服务定位器模式充分利用了缓存技术。在首次请求某个服务时，服务定位器在JNDI中查找服务，并缓存该服务对象。当再次请求相同的服务时，服务定位器会在它的缓存中查找，这样可以在很大程度上提高应用程序的性能。以下是这种设计模式的实体。</b>
 * <ol>
 * <li><b>服务（Service）</b>实际处理请求的服务。对这种服务的引用可以在JNDI服务器中查找到。</li>
 * <li><b>Context/初始的Context</b>JNDI-Context带有对要查找的服务的引用。</li>
 * <li><b>服务定位器（Service-Locator）</b>服务定位器是通过JNDI查找和缓存服务来获取服务的单点接触。</li>
 * <li><b>缓存（Cache）</b>缓存存储服务的引用，以便复用它们。</li>
 * <li><b>客户端（Client）</b>Client是通过 ServiceLocator调用服务的对象。</li>
 * </ol>
 * </p>
 *
 * @version V1.0
 * @ClassName:ServiceLocatorPatternDemo
 * @author:yanglin
 * @time:2018年10月12日 下午4:15:31
 */
public class ServiceLocatorPatternDemo {
    public static void main(String[] args) {
        Service service = ServiceLocator.getService("Service1");
        service.execute();
        service = ServiceLocator.getService("Service2");
        service.execute();
        service = ServiceLocator.getService("Service1");
        service.execute();
        service = ServiceLocator.getService("Service2");
        service.execute();
    }
}