package com.design.pattern05;

/**
 * <p>
 * 原型模式（Prototype Pattern）是用于创建重复的对象，同时又能保证性能。这种类型的设计模式属于创建型模式，它提供了一种创建对象的最佳方式。
 * 这种模式是实现了一个原型接口，该接口用于创建当前对象的克隆。当直接创建对象的代价比较大时，则采用这种模式。例如，一个对象需要在一个高代价的数据库操作之后被创建。我们可以缓存该对象，在下一个请求时返回它的克隆，在需要的时候更新数据库，以此来减少数据库调用。
 * <p>
 * <b>意图：</b>用原型实例指定创建对象的种类，并且通过拷贝这些原型创建新的对象。
 * <p>
 * <b>主要解决：</b>在运行期建立和删除原型。
 * <p>
 * <b>何时使用：</b>
 * <ol>
 * <li>当一个系统应该独立于它的产品创建，构成和表示时。</li>
 * <li>当要实例化的类是在运行时刻指定时，例如，通过动态装载。</li>
 * <li>为了避免创建一个与产品类层次平行的工厂类层次时。</li>
 * <li>当一个类的实例只能有几个不同状态组合中的一种时。建立相应数目的原型并克隆它们可能比每次用合适的状态手工实例化该类更方便一些。</li>
 * </ol>
 * <p>
 * <b>如何解决：</b>利用已有的一个原型对象，快速地生成和原型对象一样的实例。
 * <p>
 * <b>关键代码：</b>
 * <ol>
 * <li>实现克隆操作，在 JAVA 继承 Cloneable，重写 clone()，在 .NET 中可以使用 Object 类的
 * MemberwiseClone() 方法来实现对象的浅拷贝或通过序列化的方式来实现深拷贝。</li>
 * <li>原型模式同样用于隔离类对象的使用者和具体类型（易变类）之间的耦合关系，它同样要求这些"易变类"拥有稳定的接口。</li>
 * </ol>
 * <p>
 * <b>应用实例：</b>
 * <ol>
 * <li>细胞分裂。</li>
 * <li>JAVA 中的 Object clone() 方法。</li>
 * </ol>
 * <p>
 * <b>优点：</b>
 * <ol>
 * <li>性能提高。</li>
 * <li>逃避构造函数的约束。</li>
 * </ol>
 * <p>
 * <b>缺点：</b>
 * <ol>
 * <li>配备克隆方法需要对类的功能进行通盘考虑，这对于全新的类不是很难，但对于已有的类不一定很容易，特别当一个类引用不支持串行化的间接对象，或者引用含有循环结构的时候。</li>
 * <li>必须实现 Cloneable 接口。</li>
 * </ol>
 * <p>
 * <b>使用场景： </b>
 * <ol>
 * <li>资源优化场景。</li>
 * <li>类初始化需要消化非常多的资源，这个资源包括数据、硬件资源等。</li>
 * <li>性能和安全要求的场景。</li>
 * <li>通过 new 产生一个对象需要非常繁琐的数据准备或访问权限，则可以使用原型模式。</li>
 * <li>一个对象多个修改者的场景。</li>
 * <li>一个对象需要提供给其他对象访问，而且各个调用者可能都需要修改其值时，可以考虑使用原型模式拷贝多个对象供调用者使用。</li>
 * <li>在实际项目中，原型模式很少单独出现，一般是和工厂方法模式一起出现，通过 clone 的方法创建一个对象，然后由工厂方法提供给调用者。</li>
 * </ol>
 * <p>
 * <b>注意事项：</b>与通过对一个类进行实例化来构造新对象不同的是，原型模式是通过拷贝一个现有对象生成新对象的。浅拷贝实现
 * Cloneable，重写，深拷贝是通过实现 Serializable 读取二进制流。
 *
 * @version V1.0
 * @ClassName:PrototypePatternDemo
 * @author:yanglin
 * @time:2018年8月30日 下午6:34:37
 */
public class PrototypePatternDemo {
    public static void main(String[] args) {
        ShapeCache.loadCache();

        Shape clonedShape = ShapeCache.getShape("1");
        System.out.println("Shape : " + clonedShape.getType());

        Shape clonedShape2 = ShapeCache.getShape("2");
        System.out.println("Shape : " + clonedShape2.getType());

        Shape clonedShape3 = ShapeCache.getShape("3");
        System.out.println("Shape : " + clonedShape3.getType());
    }
}