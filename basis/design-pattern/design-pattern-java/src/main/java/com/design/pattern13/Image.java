package com.design.pattern13;

public interface Image {
    void display();
}