package com.design.pattern31;

public interface Filter {
    void execute(String request);
}