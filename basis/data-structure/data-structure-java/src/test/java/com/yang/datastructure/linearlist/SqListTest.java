package com.yang.datastructure.linearlist;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SqListTest {
    SqList sl = null;

    @BeforeEach
    void before() {
        int n = 10;
        System.out.println("----初始化" + (2 * n) + "个空间，填充" + n + "个元素----");
        sl = new SqList(2 * n);
        for (int i = 0; i < n; i++) {
            try {
                sl.insert(i, new Object());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @AfterEach
    void after() {
        sl.display();
    }

    /**
     * <p>
     * 测试插入元素操作
     * </p>
     *
     * @author yanglin
     * @date 2020-12-22 00:04:30
     */
    @Test
    void insert() {
        System.out.println("-----------------------");
        System.out.println("测试插入元素操作");
        try {
            sl.insert(10, new Object());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * <p>
     * 测试删除元素操作
     * </p>
     *
     * @author yanglin
     * @date 2020-12-22 00:05:40
     */
    @Test
    void remove() {
        System.out.println("-----------------------");
        System.out.println("测试删除元素操作");
        try {
            sl.remove(2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * <p>
     * 测试按值位置查找（顺序查找），返回其元素
     * 测试按值查找（顺序查找），返回其下标位置
     * </p>
     *
     * @author yanglin
     * @date 2020-12-22 00:15:52
     */
    @Test
    void getAndIndexOf() {
        System.out.println("-----------------------");
        System.out.println("测试按值位置查找（顺序查找）");
        System.out.println("测试按值查找（顺序查找）");
        try {
            Object s = sl.get(6);
            System.out.println("测试按值位置查找到的元素是：" + s);
            System.out.println("测试按值查找到的元素的下标位置是：" + sl.indexOf(s));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
