package com.yang.datastructure.linearlist;

import com.yang.datastructure.linearlist.Linked.DoubleLinkedList;
import com.yang.datastructure.linearlist.Linked.HeroNode;
import com.yang.datastructure.linearlist.Linked.HeroNodeDouble;
import com.yang.datastructure.linearlist.Linked.SingleLinkedList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LinkedTest {

    @BeforeEach
    public void before() {
        System.out.println("******************************");
    }

    /**
     * <p>
     * 测试采用尾插法建立单链表
     * </p>
     *
     * @author yanglin
     * @date 2020-12-27 15:37:59
     */
    @Test
    public void testAddTailer() {
        System.out.println("***************测试采用尾插法建立单链表***************");
        // 先创建节点
        HeroNode hero1 = new HeroNode("宋江", "及时雨");
        HeroNode hero2 = new HeroNode("卢俊义", "玉麒麟");
        HeroNode hero3 = new HeroNode("吴用", "智多星");
        HeroNode hero4 = new HeroNode("林冲", "豹子头");

        // 创建要给链表
        SingleLinkedList singleLinkedList = new SingleLinkedList();

        // 加入
        singleLinkedList.addTailer(hero1);
        singleLinkedList.addTailer(hero2);
        singleLinkedList.addTailer(hero3);
        singleLinkedList.addTailer(hero4);

        singleLinkedList.list();
    }

    /**
     * <p>
     * 测试采用头插法建立单链表
     * </p>
     *
     * @author yanglin
     * @date 2020-12-27 16:09:10
     */
    @Test
    public void testAddHeader() {
        System.out.println("***************测试采用头插法建立单链表***************");
        // 先创建节点
        HeroNode hero1 = new HeroNode("宋江", "及时雨");
        HeroNode hero2 = new HeroNode("卢俊义", "玉麒麟");
        HeroNode hero3 = new HeroNode("吴用", "智多星");
        HeroNode hero4 = new HeroNode("林冲", "豹子头");

        // 创建要给链表
        SingleLinkedList singleLinkedList = new SingleLinkedList();

        // 加入
        singleLinkedList.addHeader(hero1);
        singleLinkedList.addHeader(hero2);
        singleLinkedList.addHeader(hero3);
        singleLinkedList.addHeader(hero4);

        singleLinkedList.listHeader();
    }

    /**
     * <p>
     * 测试按序号查找结点值(序号以1开始)
     * </p>
     *
     * @author yanglin
     * @date 2020-12-27 16:33:55
     */
    @Test
    public void testGetByIndex() {
        //利用尾插法建立列表
        testAddTailer();
        System.out.println("***************按序号查找结点值(序号以1开始)***************");
        int index = 3;
        System.out.println("查询结果是：" + SingleLinkedList.getByIndex(index));
    }

    /**
     * <p>
     * 测试按值查找表结点
     * 测试按值(name作为value)查找表结点
     * </p>
     *
     * @author yanglin
     * @date 2020-12-27 16:46:05
     */
    @Test
    public void testGetByValue() {
        //利用尾插法建立列表
        testAddTailer();
        System.out.println("***************测试按值查找表结点***************");
        String value = "吴用";
        System.out.println("查询结果是：" + SingleLinkedList.getByValue(value));
    }

    /**
     * <p>
     * 插入结点操作
     * </p>
     *
     * @author yanglin
     * @date 2020-12-27 17:11:05
     */
    @Test
    public void testInsertNode() {
        //利用尾插法建立列表
        testAddTailer();
        System.out.println("***************测试插入节点***************");
        int index = 3;
        HeroNode hero = new HeroNode("鲁智深", "花和尚");
        SingleLinkedList.insertNode(index, hero);
        SingleLinkedList.list();
    }

    /**
     * <p>
     * 测试删除结点操作
     * </p>
     *
     * @author yanglin
     * @date 2020-12-27 17:42:05
     */
    @Test
    public void testDeleteNode() {
        //利用尾插法建立列表
        testAddTailer();
        System.out.println("***************测试删除结点操作***************");
        int index = 3;
        SingleLinkedList.deleteNode(index);
        SingleLinkedList.list();
    }

    /**
     * <p>
     * 测试求表长的操作
     * </p>
     *
     * @author yanglin
     * @date 2020-12-27 17:48:36
     */
    @Test
    public void testListSize() {
        //利用尾插法建立列表
        testAddTailer();
        System.out.println("***************测试求表长的操作***************");
        System.out.println("表长为--" + SingleLinkedList.listSize());
    }

    /**
     * <p>
     * 测试双向链表尾插法构造链表
     * </p>
     *
     * @author yanglin
     * @date 2020-12-27 15:37:59
     */
    @Test
    public void testDoubleAddTailer() {
        System.out.println("***************测试双向链表尾插法构造链表***************");
        // 先创建节点
        HeroNodeDouble hero5 = new HeroNodeDouble("宋江", "及时雨");
        HeroNodeDouble hero6 = new HeroNodeDouble("卢俊义", "玉麒麟");
        HeroNodeDouble hero7 = new HeroNodeDouble("吴用", "智多星");
        HeroNodeDouble hero8 = new HeroNodeDouble("林冲", "豹子头");

        // 创建链表
        DoubleLinkedList doubleLinkedList = new DoubleLinkedList();

        // 加入
        doubleLinkedList.addTailer(hero5);
        doubleLinkedList.addTailer(hero6);
        doubleLinkedList.addTailer(hero7);
        doubleLinkedList.addTailer(hero8);

        doubleLinkedList.list();
    }

    /**
     * <p>
     * 测试双链表的删除操作(序号从1开始)
     * 从双向链表中删除一个节点(序号从1开始)
     * </p>
     *
     * @author yanglin
     * @date 2020-12-27 19:45:48
     */
    @Test
    public void testDeleteNodeDouble() {
        testDoubleAddTailer();
        System.out.println("***************测试双链表的删除操作(序号从1开始)***************");
        int index = 3;
        DoubleLinkedList.deleteNode(index);
        DoubleLinkedList.list();
    }

    /**
     * <p>
     * 测试双链表的插入操作
     * </p>
     *
     * @author yanglin
     * @date 2020-12-27 20:05:37
     */
    @Test
    public void testInsertNodeDouble() {
        // 利用尾插法建立链表
        testDoubleAddTailer();
        System.out.println("***************测试双链表的插入操作***************");
        int index = 3;
        HeroNodeDouble hero = new HeroNodeDouble("鲁智深", "花和尚");
        DoubleLinkedList.insertNode(index, hero);
        DoubleLinkedList.list();
    }

}