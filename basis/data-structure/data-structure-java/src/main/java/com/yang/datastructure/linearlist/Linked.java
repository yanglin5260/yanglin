package com.yang.datastructure.linearlist;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * <p>
 * 线性表的链式表示
 * </p>
 *
 * @author yanglin
 * @date 2020-12-27 15:04:18
 */
public class Linked {

    @Data
    @ToString(exclude = {"next"})
    @RequiredArgsConstructor
    static class HeroNode {
        @NonNull
        public String name;

        @NonNull
        public String nickName;

        public HeroNode next; // 指向下一个节点

    }

    @Data
    @ToString(exclude = {"pre", "next"})
    @RequiredArgsConstructor
    static class HeroNodeDouble {
        @NonNull
        public String name;
        @NonNull
        public String nickname;
        public HeroNodeDouble next; // 指向下一个节点, 默认为null
        public HeroNodeDouble pre; // 指向前一个节点, 默认为null
    }

    @Data
    static class DoubleLinkedList {
        // 先初始化一个头节点, 头节点不要动, 不存放具体的数据
        private static HeroNodeDouble head = new HeroNodeDouble("", "");

        /**
         * <p>
         * 双链表的删除操作
         * 从双向链表中删除一个节点,
         * 说明
         * 1 对于双向链表，我们可以直接找到要删除的这个节点
         * 2 找到后，自我删除即可
         * </p>
         *
         * @param index
         * @author yanglin
         * @date 2020-12-27 19:43:21
         */
        public static void deleteNode(int index) {
            if (index <= 0) {
                System.out.println("index不能小于或者等于0，删除节点失败");
                return;
            }
            // 查找删除位置的前驱结点
            HeroNodeDouble temp = getByIndex(index);
            if (temp == null) {
                System.out.println("index越界，删除节点失败");
                return;
            }
            // 删除节点
            temp.pre.next = temp.next;
            temp.next = temp.pre;

            System.out.println("删除节点成功---" + temp);

        }

        /**
         * <p>
         * 双链表的插入操作
         * </p>
         *
         * @param index
         * @param heroNode
         * @author yanglin
         * @date 2020-12-27 20:00:00
         */
        public static void insertNode(int index, HeroNodeDouble heroNode) {
            if (index <= 0) {
                System.out.println("index不能小于或者等于0，插入节点失败");
                return;
            }
            // 查找插入位置的前驱结点
            HeroNodeDouble preNode = getByIndex(index - 1);
            if (preNode == null) {
                System.out.println("index越界，插入节点失败");
                return;
            }
            // 插入节点
            heroNode.next = preNode.next;
            preNode.next.pre = heroNode;
            heroNode.pre = preNode;
            preNode.next = heroNode;
            System.out.println("插入节点成功---" + heroNode);
        }

        /**
         * <p>
         * 根据下标获取双向链表
         * </p>
         *
         * @param index
         * @return com.yang.datastructure.linearlist.Linked.HeroNodeDouble
         * @author yanglin
         * @date 2020-12-27 19:58:42
         */
        public static HeroNodeDouble getByIndex(int index) {
            HeroNodeDouble temp = head;
            for (int i = 0; i < index; i++) {
                if (temp.next != null) {
                    temp = temp.next;
                } else {
                    return null;
                }
            }
            return temp;
        }

        // 遍历双向链表的方法
        // 显示链表[遍历]
        public static void list() {
            System.out.println("***************打印双向链表***************");
            // 判断链表是否为空
            if (head.next == null) {
                System.out.println("链表为空");
                return;
            }
            // 因为头节点，不能动，因此我们需要一个辅助变量来遍历
            HeroNodeDouble temp = head.next;
            while (true) {
                // 判断是否到链表最后
                if (temp == null) {
                    break;
                }
                // 输出节点的信息
                System.out.println(temp);
                // 将temp后移， 一定小心
                temp = temp.next;
            }
        }

        /**
         * <p>
         * TODO
         * </p>
         *
         * @param heroNode
         * @author yanglin
         * @date 2020-12-27 18:55:59
         */
        public void addTailer(HeroNodeDouble heroNode) {
            // 因为head节点不能动，因此我们需要一个辅助遍历 temp
            HeroNodeDouble temp = head;
            // 遍历链表，找到最后
            while (true) {
                // 找到链表的最后
                if (temp.next == null) {//
                    break;
                }
                // 如果没有找到最后, 将将temp后移
                temp = temp.next;
            }
            // 当退出while循环时，temp就指向了链表的最后
            // 形成一个双向链表
            temp.next = heroNode;
            heroNode.pre = temp;
        }
    }

    /**
     * <p>
     * 定义SingleLinkedList 管理我们的英雄（定义头结点）
     * </p>
     *
     * @author yanglin
     * @date 2020-12-27 15:16:53
     */
    @Data
    static class SingleLinkedList {
        // 先初始化一个头节点, 头节点不要动, 不存放具体的数据
        private static HeroNode head = new HeroNode("", "");

        /**
         * <p>
         * 按序号查找结点值(序号以1开始)
         * </p>
         *
         * @param index
         * @return com.yang.datastructure.linearlist.Linked.HeroNode
         * @author yanglin
         * @date 2020-12-27 16:50:32
         */
        public static HeroNode getByIndex(int index) {
            HeroNode temp = head;
            for (int i = 0; i < index; i++) {
                if (temp.next != null) {
                    temp = temp.next;
                } else {
                    return null;
                }
            }
            return temp;
        }

        /**
         * <p>
         * 按值查找表结点
         * </p>
         *
         * @param value
         * @return com.yang.datastructure.linearlist.Linked.HeroNode
         * @author yanglin
         * @date 2020-12-27 16:50:10
         */
        public static HeroNode getByValue(String value) {
            HeroNode temp = head.next;
            do {
                if (temp.getName() == value) {
                    return temp;
                } else {
                    continue;
                }
            } while ((temp = temp.next) != null);
            return null;

        }

        /**
         * <p>
         * 打印头结点为空，头插法
         * </p>
         *
         * @author yanglin
         * @date 2020-12-27 15:17:54
         */
        public static void list() {
            System.out.println("***************打印单链表***************");
            // 判断链表是否为空
            if (head.next == null) {
                System.out.println("链表为空");
                return;
            }
            // 因为头节点，不能动，因此我们需要一个辅助变量来遍历
            HeroNode temp = head.next;
            while (true) {
                // 判断是否到链表最后
                if (temp == null) {
                    break;
                }
                // 输出节点的信息
                System.out.println(temp);
                // 将temp后移， 一定小心
                temp = temp.next;
            }
        }

        /**
         * <p>
         * 插入结点操作（前插）
         * 插入操作是将值为x的新结点插入到单链表的第index个位置上。
         * </p>
         *
         * @param heroNode
         * @author yanglin
         * @date 2020-12-27 17:05:12
         */
        public static void insertNode(int index, HeroNode heroNode) {
            if (index <= 0) {
                System.out.println("index不能小于或者等于0，插入节点失败");
                return;
            }
            // 查找插入位置的前驱结点
            HeroNode preNode = getByIndex(index - 1);
            if (preNode == null) {
                System.out.println("index越界，插入节点失败");
                return;
            }
            // 插入节点
            heroNode.next = preNode.next;
            preNode.next = heroNode;

            System.out.println("插入节点成功---" + heroNode);
        }

        /**
         * <p>
         * 删除结点操作
         * </p>
         *
         * @param index
         * @author yanglin
         * @date 2020-12-27 20:17:14
         */
        public static void deleteNode(int index) {
            if (index <= 0) {
                System.out.println("index不能小于或者等于0，插入节点失败");
                return;
            }
            // 查找插入位置的前驱结点
            HeroNode preNode = getByIndex(index - 1);
            if (preNode == null) {
                System.out.println("index越界，插入节点失败");
                return;
            }
            // 删除节点
            HeroNode deleteNode = preNode.next;
            preNode.next = deleteNode.next;

            System.out.println("删除节点成功---" + deleteNode);
        }

        /**
         * <p>
         * 求表长的操作
         * </p>
         *
         * @return int
         * @author yanglin
         * @date 2020-12-27 17:45:41
         */
        public static int listSize() {
            int size = 0;
            HeroNode temp = head;
            while ((temp = temp.next) != null) {
                size++;
            }
            return size;
        }

        /**
         * <p>
         * 打印尾结点为空，头插法
         * </p>
         *
         * @author yanglin
         * @date 2020-12-27 16:22:20
         */
        public void listHeader() {
            // 判断链表是否为空
            if (head.next == null) {
                System.out.println("链表为空");
                return;
            }
            // 因为头节点，不能动，因此我们需要一个辅助变量来遍历
            HeroNode temp = head;
            while (true) {
                // 判断是否到链表最后
                if (temp.next == null) {
                    break;
                }
                // 输出节点的信息
                System.out.println(temp);
                // 将temp后移， 一定小心
                temp = temp.next;
            }
        }

        /**
         * <p>
         * 采用尾插法建立单链表
         * 尾插法（尾部插入）
         * 添加节点到单向链表
         * 思路，当不考虑编号顺序时
         * 1. 找到当前链表的最后节点
         * 2. 将最后这个节点的next 指向 新的节点
         * </p>
         *
         * @param heroNode
         * @author yanglin
         * @date 2020-12-27 15:19:25
         */
        public void addTailer(HeroNode heroNode) {
            // 因为head节点不能动，因此我们需要一个辅助遍历 temp
            HeroNode temp = head;
            // 遍历链表，找到最后
            while (true) {
                // 找到链表的最后
                if (temp.next == null) {//
                    break;
                }
                // 如果没有找到最后, 将将temp后移
                temp = temp.next;
            }
            // 当退出while循环时，temp就指向了链表的最后
            // 将最后这个节点的next 指向 新的节点
            temp.next = heroNode;
        }

        /**
         * <p>
         * 采用头插法建立单链表
         * 头插法的实现相对简单 思路是将新形成的节点的下一个赋值为header
         * 再把新形成的节点地址传给header即将header向前移动
         * </p>
         *
         * @param heroNode
         * @author yanglin
         * @date 2020-12-27 15:19:25
         */
        public void addHeader(HeroNode heroNode) {
            //将新节点连接到链表的头部
            heroNode.next = head;
            //header永远存储第一个节点的地址
            head = heroNode;
        }

    }


}
