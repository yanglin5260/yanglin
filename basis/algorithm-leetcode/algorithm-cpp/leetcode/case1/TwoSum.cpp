//
// Created by yanglin on 12/17/20.
//

#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

///**
// * <p>
// * 方法一：暴力枚举
// * </p>
// *
// * @author yanglin
// * @date 2020-12-17 12:34:56
// */
//class Solution {
//public:
//    vector<int> twoSum(vector<int> &nums, int target) {
//        int n = nums.size();
//        for (int i = 0; i < n; ++i) {
//            for (int j = i + 1; j < n; ++j) {
//                if (nums[i] + nums[j] == target) {
//                    return {i, j};
//                }
//            }
//        }
//        return {};
//    }
//};

/**
 * <p>
 * 方法二：哈希表
 * </p>
 *
 * @author yanglin
 * @date 2020-12-17 12:31:05
 */
class Solution {
public:
    vector<int> twoSum(vector<int> &nums, int target) {
        unordered_map<int, int> hashtable;
        for (int i = 0; i < nums.size(); ++i) {
            auto it = hashtable.find(target - nums[i]);
            if (it != hashtable.end()) {
                return {it->second, i};
            }
            hashtable[nums[i]] = i;
        }
        return {};
    }
};

int main() {
    // 初始化Solution
    Solution s;
    // 准备测试数据
    vector<int> ret = {-1, -1};
    vector<int> nums = {2, 7, 11, 15};
    // 调用方法
    ret = s.twoSum(nums, 9);
    // 打印输出
    cout << ret[0] << "," << ret[1] << endl;
    return 0;
}
