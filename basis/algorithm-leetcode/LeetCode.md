<h2>目录</h2>

  <a href="#1-数组">1. 数组</a><br>
  <a href="#2-动态规划">2. 动态规划</a><br>
<details open>
  <summary><a href="#3-字符串">3. 字符串</a></summary>
  <ul>
    <a href="#31-编辑距离72-100-手动编写">3.1. 编辑距离#72-100%-手动编写</a><br>
  </ul>
</details>
  <a href="#4-数学">4. 数学</a><br>
<details open>
  <summary><a href="#5-树">5. 树</a></summary>
  <ul>
    <a href="#51-二叉树的右视图199-100-手动编写">5.1. 二叉树的右视图#199-100%-手动编写</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#6-深度优先搜索">6. 深度优先搜索</a></summary>
  <ul>
    <a href="#61-移除盒子546-100-手动编写">6.1. 移除盒子#546-100%-手动编写</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#7-哈希表">7. 哈希表</a></summary>
  <ul>
    <a href="#71-两数之和1-100-手动编写">7.1. 两数之和#1-100%-手动编写</a><br>
  </ul>
</details>
  <a href="#8-贪心算法">8. 贪心算法</a><br>
  <a href="#9-二分查找">9. 二分查找</a><br>
  <a href="#10-广度优先搜索">10. 广度优先搜索</a><br>
  <a href="#11-双指针">11. 双指针</a><br>
  <a href="#12-排序">12. 排序</a><br>
  <a href="#13-回溯算法">13. 回溯算法</a><br>
  <a href="#14-设计">14. 设计</a><br>
  <a href="#15-栈">15. 栈</a><br>
  <a href="#16-位运算">16. 位运算</a><br>
<details open>
  <summary><a href="#17-链表">17. 链表</a></summary>
  <ul>
    <a href="#171-反转链表206-50-手动编写">17.1. 反转链表#206-50%-手动编写</a><br>
  </ul>
</details>
  <a href="#18-图">18. 图</a><br>
  <a href="#19-堆">19. 堆</a><br>
  <a href="#20-并查集">20. 并查集</a><br>
  <a href="#21-递归">21. 递归</a><br>
  <a href="#22-sliding-window">22. Sliding Window</a><br>
  <a href="#23-分治算法">23. 分治算法</a><br>
  <a href="#24-字典树">24. 字典树</a><br>
  <a href="#25-线段树">25. 线段树</a><br>
  <a href="#26-ordered">26. Ordered</a><br>
  <a href="#27-几何">27. 几何</a><br>
  <a href="#28-队列">28. 队列</a><br>
  <a href="#29-极小化极大">29. 极小化极大</a><br>
  <a href="#30-树状数组">30. 树状数组</a><br>
  <a href="#31-脑筋急转弯">31. 脑筋急转弯</a><br>
  <a href="#32-line-sweep">32. Line Sweep</a><br>
  <a href="#33-random">33. Random</a><br>
  <a href="#34-拓扑排序">34. 拓扑排序</a><br>
  <a href="#35-二叉搜索树">35. 二叉搜索树</a><br>
  <a href="#36-记忆化">36. 记忆化</a><br>
  <a href="#37-rejection-sampling">37. Rejection Sampling</a><br>
  <a href="#38-蓄水池抽样">38. 蓄水池抽样</a><br>
<details open>
  <summary><a href="#39-面试题">39. 面试题</a></summary>
  <ul>
    <a href="#391-面试题-0106-字符串压缩-100-手动编写">39.1. 面试题 01.06. 字符串压缩-100%-手动编写</a><br>
  </ul>
</details>



<h1>Algorithms-LeetCode</h1>

**根据标签分类**

# 1. 数组

[数组](https://leetcode-cn.com/tag/array/)

![](./images/LeetCode-1.png)

# 2. 动态规划

[动态规划](https://leetcode-cn.com/tag/dynamic-programming/)

![](./images/LeetCode-2.png)

# 3. 字符串

[字符串](https://leetcode-cn.com/tag/string/)

![](./images/LeetCode-3.png)

## 3.1. 编辑距离#72-100%-手动编写

[本地笔记](./algorithm-java/src/main/java/com/yang/algorithms/leetcode/case72/README.md)

# 4. 数学

[数学](https://leetcode-cn.com/tag/math/)

![](./images/LeetCode-4.png)

# 5. 树

[树](https://leetcode-cn.com/tag/tree/)

![](./images/LeetCode-5.png)

## 5.1. 二叉树的右视图#199-100%-手动编写

[本地笔记](./algorithm-java/src/main/java/com/yang/algorithms/leetcode/case199/README.md)

# 6. 深度优先搜索

[深度优先搜索](https://leetcode-cn.com/tag/depth-first-search/)

![](./images/LeetCode-6.png)

## 6.1. 移除盒子#546-100%-手动编写

[本地笔记](./algorithm-java/src/main/java/com/yang/algorithms/leetcode/case546/README.md)

# 7. 哈希表

[哈希表](https://leetcode-cn.com/tag/hash-table/)

![](./images/LeetCode-7.png)

## 7.1. 两数之和#1-100%-手动编写

[本地笔记](./algorithm-java/src/main/java/com/yang/algorithms/leetcode/case1/README.md)

# 8. 贪心算法

[贪心算法](https://leetcode-cn.com/tag/greedy/)

![](./images/LeetCode-8.png)

# 9. 二分查找

[二分查找](https://leetcode-cn.com/tag/binary-search/)

![](./images/LeetCode-9.png)

# 10. 广度优先搜索

[广度优先搜索](https://leetcode-cn.com/tag/breadth-first-search/)

![](./images/LeetCode-10.png)

# 11. 双指针

[双指针](https://leetcode-cn.com/tag/two-pointers/)

![](./images/LeetCode-11.png)

# 12. 排序

[排序](https://leetcode-cn.com/tag/sort/)

![](./images/LeetCode-12.png)

# 13. 回溯算法

[回溯算法](https://leetcode-cn.com/tag/backtracking/)

![](./images/LeetCode-13.png)

# 14. 设计

[设计](https://leetcode-cn.com/tag/design/)

![](./images/LeetCode-14.png)

# 15. 栈

[栈](https://leetcode-cn.com/tag/stack/)

![](./images/LeetCode-15.png)

# 16. 位运算

[位运算](https://leetcode-cn.com/tag/bit-manipulation/)

![](./images/LeetCode-16.png)

# 17. 链表

[链表](https://leetcode-cn.com/tag/linked-list/)

![](./images/LeetCode-17.png)

## 17.1. 反转链表#206-50%-手动编写

[本地笔记](./algorithm-java/src/main/java/com/yang/algorithms/leetcode/case206/README.md)

# 18. 图

[图](https://leetcode-cn.com/tag/graph/)

![](./images/LeetCode-18.png)

# 19. 堆

[堆](https://leetcode-cn.com/tag/heap/)

![](./images/LeetCode-19.png)

# 20. 并查集

[并查集](https://leetcode-cn.com/tag/union-find/)

![](./images/LeetCode-20.png)

# 21. 递归

[递归](https://leetcode-cn.com/tag/recursion/)

![](./images/LeetCode-21.png)

# 22. Sliding Window

[Sliding Window](https://leetcode-cn.com/tag/sliding-window/)

![](./images/LeetCode-22.png)

# 23. 分治算法

[分治算法](https://leetcode-cn.com/tag/divide-and-conquer/)

![](./images/LeetCode-23.png)

# 24. 字典树

[字典树](https://leetcode-cn.com/tag/trie/)

![](./images/LeetCode-24.png)

# 25. 线段树

[线段树](https://leetcode-cn.com/tag/segment-tree/)

![](./images/LeetCode-25.png)

# 26. Ordered

[Ordered](https://leetcode-cn.com/tag/ordered-map/)

![](./images/LeetCode-26.png)

# 27. 几何

[几何](https://leetcode-cn.com/tag/geometry/)

![](./images/LeetCode-27.png)

# 28. 队列

[队列](https://leetcode-cn.com/tag/queue/)

![](./images/LeetCode-28.png)

# 29. 极小化极大

[极小化极大](https://leetcode-cn.com/tag/minimax/)

![](./images/LeetCode-29.png)

# 30. 树状数组

[树状数组](https://leetcode-cn.com/tag/binary-indexed-tree/)

![](./images/LeetCode-30.png)

# 31. 脑筋急转弯

[脑筋急转弯](https://leetcode-cn.com/tag/brainteaser/)

![](./images/LeetCode-31.png)

# 32. Line Sweep

[Line Sweep](https://leetcode-cn.com/tag/line-sweep/)

![](./images/LeetCode-32.png)

# 33. Random

[Random](https://leetcode-cn.com/tag/random/)

![](./images/LeetCode-33.png)

# 34. 拓扑排序

[拓扑排序](https://leetcode-cn.com/tag/topological-sort/)

![](./images/LeetCode-34.png)

# 35. 二叉搜索树

[二叉搜索树](https://leetcode-cn.com/tag/binary-search-tree/)

![](./images/LeetCode-35.png)

# 36. 记忆化

[记忆化](https://leetcode-cn.com/tag/memoization/)

![](./images/LeetCode-36.png)

# 37. Rejection Sampling

[Rejection Sampling](https://leetcode-cn.com/tag/rejection-sampling/)

![](./images/LeetCode-37.png)

# 38. 蓄水池抽样

[蓄水池抽样](https://leetcode-cn.com/tag/reservoir-sampling/)

![](./images/LeetCode-38.png)

# 39. 面试题

## 39.1. 面试题 01.06. 字符串压缩-100%-手动编写

[本地笔记](./algorithm-java/src/main/java/com/yang/algorithms/leetcode/case_interview_01_06/README.md)



