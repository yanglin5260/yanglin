package com.yang.algorithms.leetcode.case199;

import com.yang.algorithms.leetcode.CommonTest;
import org.junit.jupiter.api.Test;


class SolutionTest extends CommonTest<Solution> {

    @Test
    void test() {
        TreeNode treeNode = TreeNodeUtil.arrayToTreeNode(new Integer[]{1, 2, 3, null, 5, null, 4});
        TreeNodeUtil.pirnt(treeNode);
        argument.put("input", treeNode);
    }

}
