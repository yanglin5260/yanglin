package com.yang.algorithms.leetcode.case72;

import com.yang.algorithms.leetcode.CommonTest;
import org.junit.jupiter.api.Test;

/**
 * <p>
 * SolutionTest
 * </p>
 *
 * @author yanglin
 * @date 2020-12-18 21:07:19
 */
class SolutionTest extends CommonTest<Solution> {

    @Test
    void test() {
        argument.put("word1", "horse");
        argument.put("word2", "ros");
    }

}
