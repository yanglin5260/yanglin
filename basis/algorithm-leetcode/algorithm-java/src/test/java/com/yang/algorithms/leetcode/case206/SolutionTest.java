package com.yang.algorithms.leetcode.case206;

import com.yang.algorithms.leetcode.CommonTest;
import org.junit.jupiter.api.Test;


class SolutionTest extends CommonTest<Solution> {

    @Test
    void test() {
        ListNode s =  ListNode.arrayToListNode(new int[]{1,2, 3, 4, 5});
        argument.put("inout", s);
    }

}
