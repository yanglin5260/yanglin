package com.yang.algorithms.leetcode.case_interview_01_06;

import com.yang.algorithms.leetcode.CommonTest;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class SolutionTest extends CommonTest<Solution> {

    @ParameterizedTest
    @CsvSource({"S,abcd", "S,aabcccccaaa"})
    void test(String name, String value) {
        argument.put(name, value);
    }

}
