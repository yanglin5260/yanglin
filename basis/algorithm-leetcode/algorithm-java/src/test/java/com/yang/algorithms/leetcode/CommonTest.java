package com.yang.algorithms.leetcode;

import com.yang.algorithms.util.AlgorithmUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * CommonTest
 * </p>
 *
 * @author yanglin
 * @date 2021-01-10 18:13:38
 */
public abstract class CommonTest<T> {
    public Map<String, Object> argument = new HashMap<>();

    @BeforeEach
    void before() {
        System.out.println("-----------------------Start-----------------------");
    }

    @AfterEach
    void after() throws Exception {
        Constructor constructor = ((Class) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getDeclaredConstructor();
        constructor.setAccessible(true);
        constructor.newInstance();
        AlgorithmUtil.execute(constructor.newInstance(), argument);
        System.out.println("-----------------------End-----------------------");
    }

}
