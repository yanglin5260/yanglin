package com.yang.algorithms.leetcode.case1;

import com.yang.algorithms.leetcode.CommonTest;
import org.junit.jupiter.api.Test;

/**
 * <p>
 * SolutionTest
 * </p>
 *
 * @author yanglin
 * @date 2020-12-18 21:07:19
 */
class SolutionTest extends CommonTest<Solution> {

    @Test
    void test() {
        argument.put("nums", new int[]{2, 7, 11, 15});
        argument.put("target", 9);
    }

}
