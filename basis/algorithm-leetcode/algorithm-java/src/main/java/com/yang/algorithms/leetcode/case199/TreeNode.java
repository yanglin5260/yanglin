package com.yang.algorithms.leetcode.case199;

/**
 * <p>
 * Definition for a binary tree node.
 * </p>
 *
 * @author yanglin
 * @date 2021-01-06 02:21:33
 */
public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }

    @Override
    public String toString() {
        return Integer.toString(val);
    }
}
