<h2>目录</h2>

<details open>
  <summary><a href="#1-问题描述">1. 问题描述</a></summary>
  <ul>
    <a href="#11-[199-二叉树的右视图]httpsleetcode-cncomproblemsbinary-tree-right-side-view">1.1. [199. 二叉树的右视图](https://leetcode-cn.com/problems/binary-tree-right-side-view/)</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#2-思考和解决">2. 思考和解决</a></summary>
  <ul>
  <details open>
    <summary><a href="#21-宽度优先搜索-breadth-first-search">2.1. 宽度优先搜索-Breadth First Search</a>  </summary>
    <ul>
      <a href="#211-思路">2.1.1. 思路</a><br>
      <a href="#212-复杂度分析">2.1.2. 复杂度分析</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#22-深度优先搜索-depth-first-search">2.2. 深度优先搜索-Depth First Search</a>  </summary>
    <ul>
      <a href="#221-思路">2.2.1. 思路</a><br>
      <a href="#222-复杂度分析">2.2.2. 复杂度分析</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#3-源码路径">3. 源码路径</a></summary>
  <ul>
    <a href="#31-java">3.1. Java</a><br>
  </ul>
</details>


# 1. 问题描述

## 1.1. [199. 二叉树的右视图](https://leetcode-cn.com/problems/binary-tree-right-side-view/)

给定一棵二叉树，想象自己站在它的右侧，按照从顶部到底部的顺序，返回从右侧所能看到的节点值。

**示例:**

```
输入: [1,2,3,null,5,null,4]
输出: [1, 3, 4]
解释:

   1            <---
 /   \
2     3         <---
 \     \
  5     4       <---
```

# 2. 思考和解决

## 2.1. 宽度优先搜索-Breadth First Search

### 2.1.1. 思路

利用 BFS 进行层次遍历，记录下每层的最后一个元素。

### 2.1.2. 复杂度分析

时间复杂度：O(n)。深度优先搜索最多访问每个结点一次，因此是线性复杂度。

空间复杂度：O(n)。最坏情况下，栈内会包含接近树高度的结点数量，占用O(n) 的空间。

## 2.2. 深度优先搜索-Depth First Search

### 2.2.1. 思路

我们按照 「根结点 -> 右子树 -> 左子树」 的顺序访问，就可以保证每层都是最先访问最右边的节点的。

（与先序遍历 「根结点 -> 左子树 -> 右子树」 正好相反，先序遍历每层最先访问的是最左边的节点）

### 2.2.2. 复杂度分析

- 时间复杂度： O(N)，每个节点都访问了 1 次。 
- 空间复杂度： O(N)，因为这不是一棵平衡二叉树，二叉树的深度最少是 logN, 最坏的情况下会退化成一条链表，深度就是 N，因此递归时使用的栈空间是 O(N) 的。

# 3. 源码路径

## 3.1. Java

[源码路径](./Solution.java)



