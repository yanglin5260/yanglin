package com.yang.algorithms.util;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Date;

/**
 * <p>
 * 文件操作工具类
 * </p>
 *
 * @author yanglin
 * @date 2020-12-17 23:35:26
 */
public class FileUtil {

    /**
     * <p>
     * 追加文件内容，文件不存在的话会自动创建，用UTF-8编码追加
     * </p>
     *
     * @param file    文件实体
     * @param content 追加的内容
     * @author yanglin
     * @date 2020-12-18 12:29:36
     */
    public static void appendContentInFile(File file, String content) {
        content = new Date().toString() + " --- " + content;
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try (FileOutputStream fos = new FileOutputStream(file, true);) {
            fos.write(content.getBytes(StandardCharsets.UTF_8)); //写入内容
            fos.write(System.lineSeparator().getBytes(StandardCharsets.UTF_8));  //换行
            System.out.println(content);// 打印到控制台
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * <p>
     * 追加文件内容，文件不存在的话会自动创建，用UTF-8编码追加
     * </p>
     *
     * @param fileName 文件名
     * @param content  追加的内容
     * @author yanglin
     * @date 2020-12-18 12:29:36
     */
    public static void appendContentInFile(String fileName, String content) {
        File file = new File(fileName);
        appendContentInFile(file, content);

    }

}
