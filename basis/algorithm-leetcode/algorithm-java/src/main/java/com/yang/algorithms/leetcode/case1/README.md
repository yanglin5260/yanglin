<h2>目录</h2>

<details open>
  <summary><a href="#1-问题描述">1. 问题描述</a></summary>
  <ul>
    <a href="#11-[1-两数之和]httpsleetcode-cncomproblemstwo-sum">1.1. [1. 两数之和](https://leetcode-cn.com/problems/two-sum/)</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#2-思考和解决">2. 思考和解决</a></summary>
  <ul>
  <details open>
    <summary><a href="#21-暴力枚举">2.1. 暴力枚举</a>  </summary>
    <ul>
      <a href="#211-思路及算法">2.1.1. 思路及算法</a><br>
    <details open>
      <summary><a href="#212-代码">2.1.2. 代码</a>    </summary>
      <ul>
        <a href="#2121-java">2.1.2.1. Java</a><br>
      </ul>
    </details>
      <a href="#213-复杂度分析">2.1.3. 复杂度分析</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#22-哈希表">2.2. 哈希表</a>  </summary>
    <ul>
      <a href="#221-思路及算法">2.2.1. 思路及算法</a><br>
    <details open>
      <summary><a href="#222-代码">2.2.2. 代码</a>    </summary>
      <ul>
        <a href="#2221-java">2.2.2.1. Java</a><br>
      </ul>
    </details>
      <a href="#223-复杂度分析">2.2.3. 复杂度分析</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#3-源码路径">3. 源码路径</a></summary>
  <ul>
    <a href="#31-java">3.1. Java</a><br>
    <a href="#32-c">3.2. C++</a><br>
  </ul>
</details>


# 1. 问题描述

## 1.1. [1. 两数之和](https://leetcode-cn.com/problems/two-sum/)

给定一个整数数组 `nums` 和一个目标值 `target`，请你在该数组中找出和为目标值的那 **两个** 整数，并返回他们的数组下标。

你可以假设每种输入只会对应一个答案。但是，数组中同一个元素不能使用两遍。

**示例:**

```
给定 nums = [2, 7, 11, 15], target = 9

因为 nums[0] + nums[1] = 2 + 7 = 9
所以返回 [0, 1]
```

# 2. 思考和解决

## 2.1. 暴力枚举

### 2.1.1. 思路及算法

最容易想到的方法是枚举数组中的每一个数 x，寻找数组中是否存在 target - x。

当我们使用遍历整个数组的方式寻找 target - x 时，需要注意到每一个位于 x 之前的元素都已经和 x 匹配过，因此不需要再进行匹配。**而每一个元素不能被使用两次，所以我们只需要在 x 后面的元素中寻找 target - x**。

### 2.1.2. 代码

#### 2.1.2.1. Java

```java
class Solution {
    public int[] twoSum(int[] nums, int target) {
        for (int i = 0, len = nums.length; i < len; i++) {
            for (int j = i + 1; j < len; j++) {
                // target - nums[i] == nums[j] 比nums[j] == target - nums[i]消耗的内存小一些，因为压栈操作
                if (target - nums[i] == nums[j]) {
                    return new int[]{i, j};
                }
            }
        }
        return null;
    }
}
```

### 2.1.3. 复杂度分析

时间复杂度：O(N)，其中 N是数组中的元素数量。最坏情况下数组中任意两个数都要被匹配一次。

空间复杂度：O(1)。

## 2.2. 哈希表

### 2.2.1. 思路及算法

注意到方法一的时间复杂度较高的原因是寻找 target - x 的时间复杂度过高。因此，我们需要一种更优秀的方法，能够快速寻找数组中是否存在目标元素。如果存在，我们需要找出它的索引。

使用哈希表，可以将寻找 target - x 的时间复杂度降低到从 O(N)降低到 O(1)。

这样我们创建一个哈希表，对于每一个 x，我们首先查询哈希表中是否存在 target - x，然后将 x 插入到哈希表中，即可保证不会让 x 和自己匹配。

### 2.2.2. 代码

#### 2.2.2.1. Java

```java
class Solution {
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> hashtable = new HashMap<Integer, Integer>();
        for (int i = 0; i < nums.length; ++i) {
            if (hashtable.containsKey(target - nums[i])) {
                return new int[]{hashtable.get(target - nums[i]), i};
            }
            hashtable.put(nums[i], i);
        }
        return new int[0];
    }
}
```

### 2.2.3. 复杂度分析

时间复杂度：O(N)，其中N是数组中的元素数量。对于每一个元素 x，我们可以O(1)地寻找 target - x。

空间复杂度：O(N)，其中N是数组中的元素数量。主要为哈希表的开销。

# 3. 源码路径

## 3.1. Java

[源码路径](./Solution.java)

## 3.2. C++

[源码路径](../algorithm-cpp/leetcode/case1/TwoSum.cpp)



