package com.yang.algorithms.util;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * AlgorithmUtil
 * </p>
 *
 * @author yanglin
 * @date 2020-12-18 17:48:40
 */
public class AlgorithmUtil {

    public static void execute(Object s, Map<String, Object> argument) {
        // 设置记录文件
//        File file = Path.of(URLDecoder.decode(s.getClass().getResource("").getPath(), StandardCharsets.UTF_8), s.getClass().getSimpleName() + ".txt").toFile();
        // 打印参数
        argumentPrint(argument);
        Object result = AlgorithmUtil.commonExecute(s, argument);
        // 打印返回结果
        resultPrint(result);
    }

    public static Object commonExecute(Object s, Map<String, Object> cond) {
        List<Object> c = new ArrayList<>();
        for (String o : cond.keySet()) {
            c.add(cond.get(o));
        }
        Method method = s.getClass().getMethods()[0];
        try {
            method.setAccessible(true);
            return method.invoke(s, c.toArray());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * <p>
     * 参数打印并规范参数
     * </p>
     *
     * @param cond
     * @author yanglin
     * @date 2020-12-17 22:39:22
     */
    public static void argumentPrint(Map<String, Object> cond) {
        List<String> as = new ArrayList<>();
        for (String o : cond.keySet()) {
            as.add(o + ": " + commonObjectString(cond.get(o)));
        }
        System.out.println("Input: " + as.stream().collect(Collectors.joining(", ")));
//        FileUtil.appendContentInFile(file, "Input: " + as.stream().collect(Collectors.joining(", ")));
    }

    /**
     * <p>
     * 结果打印
     * </p>
     *
     * @param o
     * @author yanglin
     * @date 2020-12-17 22:36:17
     */
    public static void resultPrint(Object o) {
        System.out.println("Output: " + AlgorithmUtil.commonObjectString(o));
//        FileUtil.appendContentInFile(file, "Output: " + AlgorithmUtil.commonObjectString(o));
    }

    /**
     * <p>
     * 通用对象字符串返回
     * </p>
     *
     * @param o 任一对象
     * @author yanglin
     * @date 2020-12-17 19:58:02
     */
    private static String commonObjectString(Object o) {
        if (o == null) {
            return "null";
        }
        Class<?> aClass = o.getClass();
        //是否是数组
        boolean isArray = aClass.isArray();
        if (isArray == true) {
            //是否是int[]类型
            if (o instanceof int[]) {
                return Arrays.toString((int[]) o);
            }
        } else {
            return o.toString();
        }
        return "";
    }

}
