package com.yang.algorithms.leetcode.case199;


import java.util.ArrayList;
import java.util.List;

///**
// * <p>
// * 宽度优先搜索（BFS）
// * </p>
// *
// * @author yanglin
// * @date 2021-01-07 02:04:50
// */
//class Solution {
//    public List<Integer> rightSideView(TreeNode root) {
//        List<Integer> result = new ArrayList<>();
//        if (root == null) return result;
//        Queue<TreeNode> queue = new LinkedList<>();
//        queue.offer(root);
//        while (!queue.isEmpty()) {
//            int size = queue.size();
//            for (int i = 0; i < size; i++) {
//                TreeNode t = queue.poll();
//                if (t.left != null) queue.offer(t.left);
//                if (t.right != null) queue.offer(t.right);
//                // 将当前队列中最后一个节点存放到结果集合中
//                if (size - 1 == i) result.add(t.val);
//            }
//        }
//        return result;
//    }
//}

/**
 * <p>
 * 深度优先搜索（DFS）
 * </p>
 *
 * @author yanglin
 * @date 2021-01-10 21:32:36
 */
class Solution {
    List<Integer> result = new ArrayList<>();

    public List<Integer> rightSideView(TreeNode root) {
        dfs(root, 0);
        return result;
    }

    private void dfs(TreeNode t, int depth) {
        if (t==null) return;
        // 先访问 当前节点，再递归地访问 右子树 和 左子树。
        // 如果当前节点所在深度还没有出现在res里，说明在该深度下当前节点是第一个被访问的节点，因此将当前节点加入res中。
        if (depth == result.size())  result.add(t.val);
        depth++;
        dfs(t.right, depth);
        dfs(t.left, depth);
    }
}

