package com.yang.algorithms.leetcode.case206;


/**
 * <p>
 * 迭代
 * </p>
 *
 * @author yanglin
 * @date 2021-01-10 23:02:42
 */
class Solution {
    public ListNode reverseList(ListNode head) {
        ListNode prev = null;
        ListNode curr = head;
        while (curr != null) {
            // 重点理解链表的移动
            ListNode next = curr.next;
            curr.next = prev;
            prev = curr;
            curr = next;
        }
        return prev;
    }
}


