package com.yang.algorithms.leetcode.case72;


//class Solution {
//    public int minDistance(String word1, String word2) {
//        int l1 = word1.length();
//        int l2 = word2.length();
//
//        // 有一个字符串为空串，所以，距离就为其中一个字段的长度
//        if (l1 * l2 == 0) {
//            return l1 + l2;
//        }
//        // DP 数组
//        int len1 = l1 + 1;
//        int len2 = l2 + 1;
//
//        int[][] D = new int[len1][len2];
//        // 边界状态初始化
//        for (int i = 0; i < len1; i++) {
//            D[i][0] = i;
//        }
//        for (int j = 0; j < len2; j++) {
//            D[0][j] = j;
//        }
//        //计算所有DP值
//        for (int i = 1; i < len1; i++) {
//            for (int j = 1; j < len2; j++) {
//                int left = D[i - 1][j] + 1;
//                int right = D[i][j - 1] + 1;
//                int equal = D[i - 1][j - 1];
//                if (word1.charAt(i - 1) != word2.charAt(j - 1)) {
//                    equal += 1;
//                }
//                D[i][j] = Math.min(left, Math.min(right, equal));
//                System.out.println(D[i][j]);
//            }
//
//        }
//        return D[l1][l2];
//    }
//}

/**
 * <p>
 * 优化代码
 * </p>
 *
 * @author yanglin
 * @date 2020-12-30 01:33:41
 */
class Solution {
    public int minDistance(String word1, String word2) {
        int wl1 = word1.length();
        int wl2 = word2.length();
        // 有一个字符串为空串，所以，距离就为其中一个字段的长度
        if (wl1 * wl2 == 0) return wl1 + wl2;
        // 边界状态初始化
        int len1 = wl1 + 1;
        int len2 = wl2 + 1;
        int[][] D = new int[len1][len2];
        // 初始化边界请注意
        for (int i = 1; i <= wl1; i++) D[i][0] = i + 1;
        for (int j = 1; j <= wl2; j++) D[0][j] = j + 1;
        //计算所有DP值
        for (int i = 1; i < len1; i++) {
            for (int j = 1; j < len2; j++) {
                /*如果 A 的第 i 个字符和 B 的第 j 个字符原本就相同，那么我们实际上不需要进行修改操作。在这种情况下，D[i][j] 最小可以为 D[i-1][j-1]*/
                if (word1.charAt(i - 1) == word2.charAt(j - 1)) D[i][j] = D[i - 1][j - 1];
                else D[i][j] = 1 + Math.min(Math.min(D[i][j - 1], D[i - 1][j]), D[i - 1][j - 1]);
            }
        }
        return D[wl1][wl2];
    }
}
