package com.yang.algorithms.leetcode.case546;


/**
 * <p>
 * 动态规划：递归方式
 * </p>
 *
 * @author yanglin
 * @date 2021-01-05 01:43:25
 */
class Solution {
    public int removeBoxes(int[] boxes) {
        int n = boxes.length;
        if (n == 0)
            return 0;
        int[][][] dp = new int[n][n][n];
        return calculatePoints(boxes, dp, 0, n - 1, 0);
    }

    private int calculatePoints(int[] boxes, int[][][] dp, int l, int r, int k) {
        /* 左右边界反转了，只能拿到0分。 */
        if (l > r) return 0;
        /* 最少也会有一个盒子，获得积分为1。 如果dp数组中该位置不为0,则表明已经计算过了，可以直接使用，不用重新计算。 */
        if (dp[l][r][k] != 0) return dp[l][r][k];
        // 优化:从第一个盒子开始连续计算的所有相同颜色的盒子应该组合在一起，向左移动，直到相邻方块颜色不等
        while (r > l && boxes[r] == boxes[r - 1]) {
            r--;
            k++;
        }
        /* 计算初始值 */
        /* 1.单独删除的情况dp[l][r][k] = dp[l][r-1][0] + (k+1)*(k+1) 后一项dp的k为0是因为我们仅考虑[l:r-1]之后没有和r-1处字符相同的字符 */
        dp[l][r][k] = calculatePoints(boxes, dp, l, r - 1, 0) + (k + 1) * (k + 1);
        /* 2.如果[l:r-1]之间存在某个位置p和r处字符相同的元素，则可以考虑先把[i+1:r-1]处的字符删除，然后剩下dp[l][i][k+1] */
        for (int i = l; i < r; i++) {
            if (boxes[i] == boxes[r]) {
                dp[l][r][k] = Math.max(dp[l][r][k], calculatePoints(boxes, dp, l, i, k + 1) + calculatePoints(boxes, dp, i + 1, r - 1, 0));
            }
        }
        return dp[l][r][k];
    }
}
