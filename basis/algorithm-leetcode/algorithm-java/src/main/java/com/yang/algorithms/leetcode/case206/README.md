<h2>目录</h2>

<details open>
  <summary><a href="#1-问题描述">1. 问题描述</a></summary>
  <ul>
    <a href="#11-[206-反转链表]httpsleetcode-cncomproblemsreverse-linked-list">1.1. [206. 反转链表](https://leetcode-cn.com/problems/reverse-linked-list/)</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#2-思考和解决">2. 思考和解决</a></summary>
  <ul>
  <details open>
    <summary><a href="#21-迭代">2.1. 迭代</a>  </summary>
    <ul>
      <a href="#211-思路">2.1.1. 思路</a><br>
      <a href="#212-复杂度分析">2.1.2. 复杂度分析</a><br>
    </ul>
  </details>
  <details open>
    <summary><a href="#22-递归">2.2. 递归</a>  </summary>
    <ul>
      <a href="#221-思路">2.2.1. 思路</a><br>
      <a href="#222-复杂度分析">2.2.2. 复杂度分析</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#3-源码路径">3. 源码路径</a></summary>
  <ul>
    <a href="#31-java">3.1. Java</a><br>
  </ul>
</details>


# 1. 问题描述

## 1.1. [206. 反转链表](https://leetcode-cn.com/problems/reverse-linked-list/)

反转一个单链表。

**示例:**

```
输入: 1->2->3->4->5->NULL
输出: 5->4->3->2->1->NULL
```

**进阶:**
你可以迭代或递归地反转链表。你能否用两种方法解决这道题？

# 2. 思考和解决

## 2.1. 迭代

### 2.1.1. 思路

在遍历列表时，将当前节点的next指针改为指向前一个元素。由于节点没有引用其上一个节点，因此必须事先存储其前一个元素。在更改引用之前，还需要另一个指针来存储下一个节点。不要忘记在最后返回新的头引用！

![img](./images/README-1.gif)

### 2.1.2. 复杂度分析

- 时间复杂度：O(n)，假设n是列表的长度，时间复杂度是O(n)。
- 空间复杂度：O(1)。

## 2.2. 递归

### 2.2.1. 思路

- 使用递归函数，一直递归到链表的最后一个结点，该结点就是反转后的头结点，记作 retret .
- 此后，每次函数在返回的过程中，让当前结点的下一个结点的 nextnext 指针指向当前节点。
- 同时让当前结点的 nextnext 指针指向 NULLNULL ，从而实现从链表尾部开始的局部反转
- 当递归函数全部出栈后，链表反转完成。

![img](./images/README-2.gif)

### 2.2.2. 复杂度分析

- 时间复杂度：O(n)，假设n是列表的长度，那么时间复杂度为 O(n)。
- 空间复杂度：O(n)，由于使用递归，将会使用隐式栈空间。递归深度可能会达到n层。

# 3. 源码路径

## 3.1. Java

[源码路径](./Solution.java)





