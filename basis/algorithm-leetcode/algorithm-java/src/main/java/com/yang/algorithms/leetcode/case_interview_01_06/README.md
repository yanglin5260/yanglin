<h2>目录</h2>

<details open>
  <summary><a href="#1-问题描述">1. 问题描述</a></summary>
  <ul>
    <a href="#11-[面试题-0106-字符串压缩]httpsleetcode-cncomproblemscompress-string-lcci">1.1. [面试题 01.06. 字符串压缩](https://leetcode-cn.com/problems/compress-string-lcci/)</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#2-思考和解决">2. 思考和解决</a></summary>
  <ul>
  <details open>
    <summary><a href="#21-方法一：模拟">2.1. 方法一：模拟</a>  </summary>
    <ul>
      <a href="#211-思路及算法">2.1.1. 思路及算法</a><br>
      <a href="#212-复杂度分析">2.1.2. 复杂度分析</a><br>
    </ul>
  </details>
  </ul>
</details>
<details open>
  <summary><a href="#3-源码路径">3. 源码路径</a></summary>
  <ul>
    <a href="#31-java">3.1. Java</a><br>
  </ul>
</details>


# 1. 问题描述

## 1.1. [面试题 01.06. 字符串压缩](https://leetcode-cn.com/problems/compress-string-lcci/)

难度简单73收藏分享切换为英文接收动态反馈

字符串压缩。利用字符重复出现的次数，编写一种方法，实现基本的字符串压缩功能。比如，字符串`aabcccccaaa`会变为`a2b1c5a3`。若“压缩”后的字符串没有变短，则返回原先的字符串。你可以假设字符串中只包含大小写英文字母（a至z）。

**示例1:**

```
 输入："aabcccccaaa"
 输出："a2b1c5a3"
```

**示例2:**

```
 输入："abbccd"
 输出："abbccd"
 解释："abbccd"压缩后为"a1b2c2d1"，比原字符串长度更长。
```

**提示：**

1. 字符串长度在[0, 50000]范围内。

# 2. 思考和解决

## 2.1. 方法一：模拟

### 2.1.1. 思路及算法

字符串压缩的方式就是将连续出现的相同字符按照 `字符 + 出现次数` 压缩。如果压缩后的字符串长度变短，则返回压缩后的字符串，否则保留原来的字符串，所以我们模拟这个过程构建字符串即可。

我们从左往右遍历字符串，用 `ch` 记录当前要压缩的字符，`cnt`记录 `ch` 出现的次数，如果当前枚举到的字符 `s[i]` 等于 `ch` ，我们就更新 `cnt` 的计数，即 `cnt = cnt + 1`，否则我们按题目要求将 `ch` 以及 `cnt` 更新到答案字符串 `ans` 里，即 `ans = ans + ch + cnt`，完成对 ch 字符的压缩。随后更新 chch 为 `s[i]`，`cnt` 为 11，表示将压缩的字符更改为`s[i]`。

在遍历结束之后，我们就得到了压缩后的字符串`ans`，并将其长度与原串长度进行比较。如果长度没有变短，则返回原串，否则返回压缩后的字符串。

### 2.1.2. 复杂度分析

时间复杂度：O(n)，其中 n 为字符串的长度，即遍历一次字符串的复杂度。

空间复杂度：O(1)，只需要常数空间（不包括存储答案 ans 的空间）存储变量。

# 3. 源码路径

## 3.1. Java

[源码路径](./Solution.java)




