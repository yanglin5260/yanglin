package com.yang.algorithms.leetcode.case1;

import java.util.HashMap;
import java.util.Map;

/**
 * 方法二：一遍哈希表
 * 事实证明，我们可以一次完成。在进行迭代并将元素插入到表中的同时，我们还会回过头来检查表中是否已经存在当前元素所对应的目标元素。如果它存在，那我们已经找到了对应解，并立即将其返回。
 * <p>
 * 复杂度分析：
 * <p>
 * 时间复杂度：O(n)，
 * 我们只遍历了包含有 n 个元素的列表一次。在表中进行的每次查找只花费 O(1) 的时间。
 * <p>
 * 空间复杂度：O(n)，
 * 所需的额外空间取决于哈希表中存储的元素数量，该表最多需要存储 n 个元素。
 *
 * @author yanglin
 * @date 2020-07-30 16:47:11
 */
class Solution {
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0, len = nums.length; i < len; i++) {
            if (map.containsKey(target - nums[i])) {
                return new int[]{i, map.get(target - nums[i])};
            }
            map.put(nums[i], i);
        }
        return null;
    }
}

///**
// * 方法一：暴力法
// * 暴力法很简单，遍历每个元素 xx，并查找是否存在一个值与 target - x相等的目标元素。
// * <p>
// * 复杂度分析：
// * <p>
// * 时间复杂度：O(n^2)
// * 对于每个元素，我们试图通过遍历数组的其余部分来寻找它所对应的目标元素，这将耗费 O(n)O(n) 的时间。因此时间复杂度为 O(n^2)。
// * <p>
// * 空间复杂度：O(1)。
// *
// * @author yanglin
// * @date 2020-07-30 16:41:21
// */
//class Solution {
//    public int[] twoSum(int[] nums, int target) {
//        for (int i = 0, len = nums.length; i < len; i++) {
//            for (int j = i + 1; j < len; j++) {
//                // target - nums[i] == nums[j] 比nums[j] == target - nums[i]消耗的内存小一些，因为压栈操作
//                if (target - nums[i] == nums[j]) {
//                    return new int[]{i, j};
//                }
//            }
//        }
//        return null;
//    }
//}
