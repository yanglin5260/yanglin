package com.yang.algorithms.leetcode.case206;

//创建一个链表的类
class ListNode {
    int val;    //数值 data
    ListNode next;    // 结点 node

    ListNode(int x) {    //可以定义一个有参构造方法，也可以定义一个无参构造方法
        val = x;
    }

    // 添加新的结点
    public void add(int newval) {
        ListNode newNode = new ListNode(newval);
        if (this.next == null)
            this.next = newNode;
        else
            this.next.add(newval);
    }

    // 打印链表
    public void print() {
        System.out.print(this.val);
        if (this.next != null) {
            System.out.print("-->");
            this.next.print();
        }
    }

    public static ListNode arrayToListNode(int[] s) {
        ListNode root = new ListNode(s[0]);
        ListNode other = root;
        for (int i = 1; i < s.length; i++) {
            ListNode temp = new ListNode(s[i]);
            other.next = temp;
            other = temp;
        }
        return root;
    }

    @Override
    public String toString() {
        return printNode(this);
    }

    public String printNode(ListNode n) {
        if (n == null) return "NULL";
        return n.val + "->" + printNode(n.next);
    }
}
