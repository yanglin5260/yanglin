<h2>目录</h2>

<details open>
  <summary><a href="#1-问题描述">1. 问题描述</a></summary>
  <ul>
    <a href="#11-[546-移除盒子]httpsleetcode-cncomproblemsremove-boxes">1.1. [546. 移除盒子](https://leetcode-cn.com/problems/remove-boxes/)</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#2-思考和解决">2. 思考和解决</a></summary>
  <ul>
    <a href="#21-动态规划">2.1. 动态规划</a><br>
    <a href="#22-复杂度分析">2.2. 复杂度分析</a><br>
  </ul>
</details>
<details open>
  <summary><a href="#3-源码路径">3. 源码路径</a></summary>
  <ul>
    <a href="#31-java">3.1. Java</a><br>
  </ul>
</details>


# 1. 问题描述

## 1.1. [546. 移除盒子](https://leetcode-cn.com/problems/remove-boxes/)

给出一些不同颜色的盒子，盒子的颜色由数字表示，即不同的数字表示不同的颜色。
你将经过若干轮操作去去掉盒子，直到所有的盒子都去掉为止。每一轮你可以移除具有相同颜色的连续 k 个盒子（k >= 1），这样一轮之后你将得到 `k*k` 个积分。
当你将所有盒子都去掉之后，求你能获得的最大积分和。

**示例：**

```
输入：boxes = [1,3,2,2,2,3,4,3,1]
输出：23
解释：
[1, 3, 2, 2, 2, 3, 4, 3, 1] 
----> [1, 3, 3, 4, 3, 1] (3*3=9 分) 
----> [1, 3, 3, 3, 1] (1*1=1 分) 
----> [1, 1] (3*3=9 分) 
----> [] (2*2=4 分)
```

**提示：**

- `1 <= boxes.length <= 100`
- `1 <= boxes[i] <= 100`

# 2. 思考和解决

## 2.1. 动态规划

```
  集合定义： dp[i][j][k]， 从i~j中移除颜色相同的盒子， j右边有k个和a[j]颜色相同的盒子
  策略1：
      从i ~ j， 我们可以先移除右边的颜色相同的k + 1个盒子, 再移除i ~ j - 1的盒子
      dp[i][j][k] = dp[i][j - 1][0] + (k + 1) ^ 2
  策略2：
      从i ~ j， 把a[j] 归属到 k个右边颜色相同的盒子里， 从 i ~ j - 1
      枚举 t， i <= t <= j - 1
          先移除右半部分t + 1 ~ j - 1中的盒子， 为dp[t + 1][j - 1][0]
          在移除左半部分i ~ t中的盒子， 为dp[i][t][k + 1]
```

![消消乐.png](./images/README-1.png)

## 2.2. 复杂度分析

- 时间复杂度：O(n<sup>4</sup>)。
- 空间复杂度：O(n<sup>3</sup>)。

# 3. 源码路径

## 3.1. Java

[源码路径](./Solution.java)



